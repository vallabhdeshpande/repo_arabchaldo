﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class PostTypes
    {
        public PostTypes()
        {
            PostDetails = new HashSet<PostDetails>();
        }

        public int PostTypeId { get; set; }
        public string PostType { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public ICollection<PostDetails> PostDetails { get; set; }
    }
}
