﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class LoginDetails
    {
        public long LoginDetailId { get; set; }
        public long UserId { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }
        public string ActivationCode { get; set; }
        public bool? IsActivated { get; set; }

        public UserDetails User { get; set; }
    }
}
