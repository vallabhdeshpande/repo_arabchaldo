﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class BannerStatusDetails
    {
        public long BannerStatusDetailsId { get; set; }
        public long BannerId { get; set; }
        public long WorkflowStatusId { get; set; }
        public string Remark { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public BannerDetails Banner { get; set; }
        public WorkflowStatus WorkflowStatus { get; set; }
    }
}
