﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class State
    {
        public State()
        {
            City = new HashSet<City>();
        }

        public long StateId { get; set; }
        public string StateName { get; set; }
        public string Description { get; set; }
        public long CountryId { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public Country Country { get; set; }
        public ICollection<City> City { get; set; }
    }
}
