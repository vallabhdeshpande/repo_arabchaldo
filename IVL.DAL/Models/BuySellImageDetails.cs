﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class BuySellImageDetails
    {
        public long BuySellImageDetailId { get; set; }
        public string BuySellImageUrl { get; set; }
        public long BuySellId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public BuySellDetails BuySell { get; set; }
    }
}
