﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class Country
    {
        public Country()
        {
            State = new HashSet<State>();
        }

        public long CountryId { get; set; }
        public string CountryName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public ICollection<State> State { get; set; }
    }
}
