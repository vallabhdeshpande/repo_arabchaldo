﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class PaymentPlan
    {
        public PaymentPlan()
        {
            TransactionDetails = new HashSet<TransactionDetails>();
        }

        public long PlanId { get; set; }
        public string PlanName { get; set; }
        public long UserId { get; set; }
        public long? AdImageCount { get; set; }
        public decimal? AdImagePrice { get; set; }
        public long? BannerImageCount { get; set; }
        public decimal? BannerImagePrice { get; set; }
        public long? EventImageCount { get; set; }
        public decimal? EventImagePrice { get; set; }
        public long? LogoCount { get; set; }
        public decimal? LogoPrice { get; set; }
        public long? JobsAdvCount { get; set; }
        public decimal? JobsAdvPrice { get; set; }
        public long? SmallAdsCount { get; set; }
        public decimal? SmallAdsPrice { get; set; }
        public long? BuySellCount { get; set; }
        public decimal? BuySellPrice { get; set; }
        public decimal? MiscellaneiousTotal { get; set; }
        public decimal Amount { get; set; }
        public decimal? Discount { get; set; }
        public decimal FinalAmount { get; set; }
        public bool IsActive { get; set; }
        public bool? IsPaid { get; set; }
        public bool? IsVisible { get; set; }
        public string Description { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTill { get; set; }

        public ICollection<TransactionDetails> TransactionDetails { get; set; }
    }
}
