﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class UserDetails
    {
        public UserDetails()
        {
            LoginDetails = new HashSet<LoginDetails>();
        }

        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public byte Gender { get; set; }
        public string ProfilePicUrl { get; set; }
        public string ContactNumber { get; set; }
        public string AlternateContactNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Email { get; set; }
        public string AddressStreet1 { get; set; }
        public string AddressStreet2 { get; set; }
        public long? CountryId { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }
        public string RoleName { get; set; }
        public string ZipCode { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }
        public string CountryCodeContact { get; set; }
        public bool? IsSocialMedia { get; set; }
        public string Provider { get; set; }

        public City City { get; set; }
        public ICollection<LoginDetails> LoginDetails { get; set; }
    }
}
