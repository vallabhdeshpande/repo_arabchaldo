﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class MessageBoardDetails
    {
        public long MessageBoardDetailsId { get; set; }
        public long MessageBoardId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
