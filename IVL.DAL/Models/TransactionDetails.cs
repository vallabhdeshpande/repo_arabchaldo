﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class TransactionDetails
    {
        public long TransactionId { get; set; }
        public long PlanId { get; set; }
        public string PaymentMode { get; set; }
        public string StripeTokenId { get; set; }
        public string StripeChargeId { get; set; }
        public string ReceiptId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public decimal Amount { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string StripeCountry { get; set; }

        public PaymentPlan Plan { get; set; }
    }
}
