﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class Category
    {
        public Category()
        {
            AdDetails = new HashSet<AdDetails>();
            BuySellDetails = new HashSet<BuySellDetails>();
            EventDetails = new HashSet<EventDetails>();
            SubCategory = new HashSet<SubCategory>();
        }

        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string CategoryImageUrl { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }
        public string ArabicName { get; set; }

        public ICollection<AdDetails> AdDetails { get; set; }
        public ICollection<BuySellDetails> BuySellDetails { get; set; }
        public ICollection<EventDetails> EventDetails { get; set; }
        public ICollection<SubCategory> SubCategory { get; set; }
    }
}
