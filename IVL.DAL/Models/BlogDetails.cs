﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class BlogDetails
    {
        public long BlogId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public bool? IsVisible { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? ValidTillDate { get; set; }
        public DateTime? ValidFromDate { get; set; }
        public long? VisitorCount { get; set; }
    }
}
