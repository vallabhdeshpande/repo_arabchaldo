﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class SmallAdImageDetails
    {
        public long SmallAdImageDetailId { get; set; }
        public string SmallAdImageUrl { get; set; }
        public long SmallAdId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public SmallAdDetails SmallAd { get; set; }
    }
}
