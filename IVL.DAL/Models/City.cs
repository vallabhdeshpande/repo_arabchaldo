﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class City
    {
        public City()
        {
            UserDetails = new HashSet<UserDetails>();
        }

        public long CityId { get; set; }
        public string CityName { get; set; }
        public string Description { get; set; }
        public long StateId { get; set; }
        public string StateName { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public State State { get; set; }
        public ICollection<UserDetails> UserDetails { get; set; }
    }
}
