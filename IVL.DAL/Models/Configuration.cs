﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class Configuration
    {
        public int ConfigurationId { get; set; }
        public decimal? LogoCharge { get; set; }
        public decimal? ImageCharge { get; set; }
        public decimal? BannerCharge { get; set; }
        public string Smtp { get; set; }
        public int? Port { get; set; }
        public string FromMailId { get; set; }
        public string Password { get; set; }
        public string RegistrationMailTemplate { get; set; }
        public string ResetPasswordMailTemplate { get; set; }
        public string ChangePasswordMailTemplate { get; set; }
        public string ForgotPasswordMailTemplate { get; set; }
        public string PostMailTemplate { get; set; }
        public string ApproveMailTemplate { get; set; }
        public string RejectMailTemplate { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Domain { get; set; }
        public string ContactMailTemplate { get; set; }
        public string FeedbackMailTemplate { get; set; }
        public string JobsMailTemplate { get; set; }
        public string AboutUstemplate { get; set; }
        public string ContactUstemplate { get; set; }
        public string PrivacyPolicyTemplate { get; set; }
        public string TermsOfUseTemplate { get; set; }
        public string Faqtemplate { get; set; }
        public string DisclaimerTemplate { get; set; }
        public long JobCategoryId { get; set; }
        public long BuySellCategoryId { get; set; }
        public string ContactUsTemplateArabic { get; set; }
        public string AboutUstemplateArabic { get; set; }
        public string PrivacyPolicyTemplateArabic { get; set; }
        public string TermsOfUseTemplateArabic { get; set; }
        public string FaqtemplateArabic { get; set; }
        public string DisclaimerTemplateArabic { get; set; }
    }
}
