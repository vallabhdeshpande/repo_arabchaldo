﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class WorkflowStatus
    {
        public WorkflowStatus()
        {
            AdStatusDetails = new HashSet<AdStatusDetails>();
            BannerStatusDetails = new HashSet<BannerStatusDetails>();
            PostStatusDetails = new HashSet<PostStatusDetails>();
        }

        public long WorkflowStatusId { get; set; }
        public string Status { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public ICollection<AdStatusDetails> AdStatusDetails { get; set; }
        public ICollection<BannerStatusDetails> BannerStatusDetails { get; set; }
        public ICollection<PostStatusDetails> PostStatusDetails { get; set; }
    }
}
