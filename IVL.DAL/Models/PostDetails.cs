﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class PostDetails
    {
        public PostDetails()
        {
            PostStatusDetails = new HashSet<PostStatusDetails>();
        }

        public long PostDetailsId { get; set; }
        public int PostTypeId { get; set; }
        public long PostId { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        // keep the below property as public PostTypes PostTypes { get; set; }
        public PostTypes PostTypes { get; set; }
        public ICollection<PostStatusDetails> PostStatusDetails { get; set; }
    }
}
