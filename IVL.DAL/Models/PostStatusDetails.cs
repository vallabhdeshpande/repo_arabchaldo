﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class PostStatusDetails
    {
        public long PostStatusDetailsId { get; set; }
        public long PostDetailsId { get; set; }
        public long WorkflowStatusId { get; set; }
        public string Remark { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public PostDetails PostDetails { get; set; }
        public WorkflowStatus WorkflowStatus { get; set; }
    }
}
