﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class Offshore_ArabchaldoContext : DbContext
    {
        public Offshore_ArabchaldoContext()
        {
        }

        public Offshore_ArabchaldoContext(DbContextOptions<Offshore_ArabchaldoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AdDetails> AdDetails { get; set; }
        public virtual DbSet<AdImageDetails> AdImageDetails { get; set; }
        public virtual DbSet<AdStatusDetails> AdStatusDetails { get; set; }
        public virtual DbSet<BannerDetails> BannerDetails { get; set; }
        public virtual DbSet<BannerStatusDetails> BannerStatusDetails { get; set; }
        public virtual DbSet<BlogDetails> BlogDetails { get; set; }
        public virtual DbSet<BuySellDetails> BuySellDetails { get; set; }
        public virtual DbSet<BuySellImageDetails> BuySellImageDetails { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<Configuration> Configuration { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<EventDetails> EventDetails { get; set; }
        public virtual DbSet<LoginDetails> LoginDetails { get; set; }
        public virtual DbSet<MessageBoard> MessageBoard { get; set; }
        public virtual DbSet<MessageBoardDetails> MessageBoardDetails { get; set; }
        public virtual DbSet<PaymentPlan> PaymentPlan { get; set; }
        public virtual DbSet<Permissions> Permissions { get; set; }
        public virtual DbSet<PostDetails> PostDetails { get; set; }
        public virtual DbSet<PostStatusDetails> PostStatusDetails { get; set; }
        public virtual DbSet<PostTypes> PostTypes { get; set; }
        public virtual DbSet<RolePermissions> RolePermissions { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<SmallAdDetails> SmallAdDetails { get; set; }
        public virtual DbSet<SmallAdImageDetails> SmallAdImageDetails { get; set; }
        public virtual DbSet<State> State { get; set; }
        public virtual DbSet<SubCategory> SubCategory { get; set; }
        public virtual DbSet<TransactionDetails> TransactionDetails { get; set; }
        public virtual DbSet<UserDetails> UserDetails { get; set; }
        public virtual DbSet<UserRoles> UserRoles { get; set; }
        public virtual DbSet<WorkflowStatus> WorkflowStatus { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                // optionsBuilder.UseSqlServer("Server=HIJ-SQLSRV01;Database=Offshore_Arabchaldo;Persist Security Info=True;User ID=arabchaldo_user;Password=admin@123; Pooling = true; Max Pool Size = 100; ");
                // Local Demo Instance
                // optionsBuilder.UseSqlServer("Server=HIJ-SQLSRV01;Database=DemoDB;Persist Security Info=True;User ID=arabchaldo_user;Password=admin@123; Pooling = true; Max Pool Size = 100; ");
                // optionsBuilder.UseSqlServer("Server=HIJ-SQLSRV01;Database=Offshore_ArabchaldoDemo;Persist Security Info=True;User ID=arabchaldo_user;Password=admin@123; Pooling = true; Max Pool Size = 100; ");
                // LOCAL UAT DB
                // optionsBuilder.UseSqlServer("Server=HIJ-SQLSRV01;Database=RaviUATDB;Persist Security Info=True;User ID=arabchaldo_user;Password=admin@123; Pooling = true; Max Pool Size = 100; ");

                // Ravi UAT DB
                // optionsBuilder.UseSqlServer("Server=MYLOWFARESERVER;Database=Test_Arabchaldo;Integrated Security=False;User ID=Ivl_Arabchaldo;Password=Arab@1234;Pooling = true; Max Pool Size = 100;");

                // Ravi Production DB
                 optionsBuilder.UseSqlServer("Server=MYLOWFARESERVER;Database=Prod_Arabchaldo;Integrated Security=False;User ID=Ivl_Arabchaldo;Password=Arab@1234;Pooling = true; Max Pool Size = 100;");
                // Deepak Azure
                // FULL--> optionsBuilder.UseSqlServer("Server=tcp:uatarabchaldo.database.windows.net,1433;Initial Catalog=UAT_Arabchaldo;Persist Security Info=False;User ID=adminarabchaldo;Password=Ivl@1234;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
               // optionsBuilder.UseSqlServer("Server=tcp:uatarabchaldo.database.windows.net;Initial Catalog=UAT_Arabchaldo;User ID=adminarabchaldo;Password=Ivl@1234");
                //optionsBuilder.UseSqlServer("Server=tcp:uatarabchaldo.database.windows.net;Initial Catalog=UAT_Arabchaldo;Persist Security Info=True;User ID=adminarabchaldo;Password=Ivl@1234");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdDetails>(entity =>
            {
                entity.HasKey(e => e.AdId);

                entity.Property(e => e.AdLogoUrl).HasMaxLength(500);

                entity.Property(e => e.AddressStreet1)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AddressStreet2)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AlternateContactNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CityName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPersonName)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CountryCodeContact).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FaxNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ImageUrl)
                    .HasColumnName("ImageURL")
                    .HasMaxLength(900);

                entity.Property(e => e.OtherCity)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PostedForName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ServicesOffered)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.StateName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubCategoryName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TagLine)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.ValidFromDate).HasColumnType("datetime");

                entity.Property(e => e.ValidTillDate).HasColumnType("datetime");

                entity.Property(e => e.Website).HasMaxLength(150);

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.AdDetails)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdDetails_Category");
            });

            modelBuilder.Entity<AdImageDetails>(entity =>
            {
                entity.HasKey(e => e.AdImageDetailId);

                entity.Property(e => e.AdImageUrl)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Ad)
                    .WithMany(p => p.AdImageDetails)
                    .HasForeignKey(d => d.AdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdImageDetails_AdDetails");
            });

            modelBuilder.Entity<AdStatusDetails>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Remark)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Ad)
                    .WithMany(p => p.AdStatusDetails)
                    .HasForeignKey(d => d.AdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdStatusDetails_AdDetails");

                entity.HasOne(d => d.WorkflowStatus)
                    .WithMany(p => p.AdStatusDetails)
                    .HasForeignKey(d => d.WorkflowStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdStatusDetails_WorkflowStatus");
            });

            modelBuilder.Entity<BannerDetails>(entity =>
            {
                entity.HasKey(e => e.BannerId);

                entity.Property(e => e.AddressStreet1)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AddressStreet2)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AlternateContactNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CityName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPersonName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CountryCodeContact).HasMaxLength(50);

                entity.Property(e => e.CountryName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FaxNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ImageUrl).HasMaxLength(500);

                entity.Property(e => e.OtherCity).HasMaxLength(50);

                entity.Property(e => e.PostedForName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.ValidFromDate).HasColumnType("datetime");

                entity.Property(e => e.ValidTillDate).HasColumnType("datetime");

                entity.Property(e => e.Website).HasMaxLength(150);

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BannerStatusDetails>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Remark)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Banner)
                    .WithMany(p => p.BannerStatusDetails)
                    .HasForeignKey(d => d.BannerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BannerStatusDetails_BannerDetails");

                entity.HasOne(d => d.WorkflowStatus)
                    .WithMany(p => p.BannerStatusDetails)
                    .HasForeignKey(d => d.WorkflowStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BannerStatusDetails_WorkflowStatus");
            });

            modelBuilder.Entity<BlogDetails>(entity =>
            {
                entity.HasKey(e => e.BlogId);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ImageUrl).HasMaxLength(500);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.ValidFromDate).HasColumnType("datetime");

                entity.Property(e => e.ValidTillDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<BuySellDetails>(entity =>
            {
                entity.HasKey(e => e.BuySellId);

                entity.Property(e => e.AddressStreet1)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AddressStreet2)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AlternateContactNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CityName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPersonName)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CountryCodeContact).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FaxNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ImageUrl)
                    .HasColumnName("ImageURL")
                    .HasMaxLength(900);

                entity.Property(e => e.OtherCity)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PostedForName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubCategoryName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.ValidFromDate).HasColumnType("datetime");

                entity.Property(e => e.ValidTillDate).HasColumnType("datetime");

                entity.Property(e => e.Website).HasMaxLength(150);

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.BuySellDetails)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuySellDetails_Category");
            });

            modelBuilder.Entity<BuySellImageDetails>(entity =>
            {
                entity.HasKey(e => e.BuySellImageDetailId);

                entity.Property(e => e.BuySellImageUrl)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.BuySell)
                    .WithMany(p => p.BuySellImageDetails)
                    .HasForeignKey(d => d.BuySellId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuySellImageDetails_BuySellDetails");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.ArabicName).HasMaxLength(150);

                entity.Property(e => e.CategoryImageUrl)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.Property(e => e.CityName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.StateName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.City)
                    .HasForeignKey(d => d.StateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_City_State");
            });

            modelBuilder.Entity<Configuration>(entity =>
            {
                entity.Property(e => e.AboutUstemplate).HasColumnName("AboutUSTemplate");

                entity.Property(e => e.AboutUstemplateArabic).HasColumnName("AboutUSTemplateArabic");

                entity.Property(e => e.ContactUstemplate).HasColumnName("ContactUSTemplate");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Domain)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Faqtemplate).HasColumnName("FAQTemplate");

                entity.Property(e => e.FaqtemplateArabic).HasColumnName("FAQTemplateArabic");

                entity.Property(e => e.FromMailId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password).HasMaxLength(50);

                entity.Property(e => e.Smtp)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.Property(e => e.CountryName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<EventDetails>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.Property(e => e.EventId).ValueGeneratedOnAdd();

                entity.Property(e => e.AddressStreet1)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AddressStreet2)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AlternateContactNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CityName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPersonName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CountryCodeContact).HasMaxLength(50);

                entity.Property(e => e.CountryName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EventDate).HasColumnType("datetime");

                entity.Property(e => e.FaxNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ImageUrl).HasMaxLength(500);

                entity.Property(e => e.OtherCity).HasMaxLength(50);

                entity.Property(e => e.PostedForName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.ValidFromDate).HasColumnType("datetime");

                entity.Property(e => e.ValidTillDate).HasColumnType("datetime");

                entity.Property(e => e.Website).HasMaxLength(150);

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.EventDetails)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_Event_Category");

                entity.HasOne(d => d.Event)
                    .WithOne(p => p.InverseEvent)
                    .HasForeignKey<EventDetails>(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EventDetails_EventDetails");
            });

            modelBuilder.Entity<LoginDetails>(entity =>
            {
                entity.HasKey(e => e.LoginDetailId);

                entity.Property(e => e.ActivationCode).HasMaxLength(350);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.LoginDetails)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LoginDetails_UserDetails");
            });

            modelBuilder.Entity<MessageBoard>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(1500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Topic)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<MessageBoardDetails>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(1500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<PaymentPlan>(entity =>
            {
                entity.HasKey(e => e.PlanId);

                entity.Property(e => e.AdImagePrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.BannerImagePrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.BuySellPrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EventImagePrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.JobsAdvPrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.LogoPrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.MiscellaneiousTotal).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.PlanName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SmallAdsPrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.ValidFrom).HasColumnType("datetime");

                entity.Property(e => e.ValidTill).HasColumnType("datetime");
            });

            modelBuilder.Entity<Permissions>(entity =>
            {
                entity.HasKey(e => e.PermissionId);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PermissionName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<PostDetails>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                // Keep entity.HasOne(d => d.PostTypes) and  entity.HasOne(d => d.PostType) as per DB structure.
                entity.HasOne(d => d.PostTypes)
                    .WithMany(p => p.PostDetails)
                    .HasForeignKey(d => d.PostTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PostDetails_PostType");
            });

            modelBuilder.Entity<PostStatusDetails>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Remark)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.PostDetails)
                    .WithMany(p => p.PostStatusDetails)
                    .HasForeignKey(d => d.PostDetailsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PostStatusDetails_PostDetails");

                entity.HasOne(d => d.WorkflowStatus)
                    .WithMany(p => p.PostStatusDetails)
                    .HasForeignKey(d => d.WorkflowStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PostStatusDetails_WorkflowStatus");
            });

            modelBuilder.Entity<PostTypes>(entity =>
            {
                entity.HasKey(e => e.PostTypeId);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.PostType)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<RolePermissions>(entity =>
            {
                entity.HasKey(e => e.RolePermissionId);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.RolePermissions)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RolePermissions_Permissions");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RolePermissions)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RolePermissions_Roles");
            });

            modelBuilder.Entity<Roles>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<SmallAdDetails>(entity =>
            {
                entity.HasKey(e => e.SmallAdId);

                entity.Property(e => e.AdLogoUrl).HasMaxLength(500);

                entity.Property(e => e.AddressStreet1)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AddressStreet2)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AlternateContactNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CityName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPersonName)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CountryCodeContact).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FaxNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ImageUrl)
                    .HasColumnName("ImageURL")
                    .HasMaxLength(900);

                entity.Property(e => e.OtherCity)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PostedForName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TagLine)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.ValidFromDate).HasColumnType("datetime");

                entity.Property(e => e.ValidTillDate).HasColumnType("datetime");

                entity.Property(e => e.Website).HasMaxLength(150);

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SmallAdImageDetails>(entity =>
            {
                entity.HasKey(e => e.SmallAdImageDetailId);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.SmallAdImageUrl)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.SmallAd)
                    .WithMany(p => p.SmallAdImageDetails)
                    .HasForeignKey(d => d.SmallAdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SmallAdImageDetails_SmallAdDetails");
            });

            modelBuilder.Entity<State>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.StateName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.State)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_State_Country");
            });

            modelBuilder.Entity<SubCategory>(entity =>
            {
                entity.Property(e => e.ArabicName).HasMaxLength(150);

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SubCategoryName)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.SubCategory)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SubCategory_Category");
            });

            modelBuilder.Entity<TransactionDetails>(entity =>
            {
                entity.HasKey(e => e.TransactionId);

                entity.Property(e => e.Address1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentMode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiptId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StripeChargeId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StripeCountry)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StripeTokenId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionDate).HasColumnType("datetime");

                entity.Property(e => e.Zip)
                    .HasColumnName("ZIP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Plan)
                    .WithMany(p => p.TransactionDetails)
                    .HasForeignKey(d => d.PlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TransactionDetails_PaymentPlan");
            });

            modelBuilder.Entity<UserDetails>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.AddressStreet1)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AddressStreet2)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AlternateContactNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CityName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CountryCodeContact)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FaxNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ProfilePicUrl)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Provider)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.UserDetails)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK_UserDetails_City");
            });

            modelBuilder.Entity<UserRoles>(entity =>
            {
                entity.HasKey(e => e.UserRoleId);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<WorkflowStatus>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });
        }
    }
}
