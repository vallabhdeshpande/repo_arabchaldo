﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Models;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface ITransactionDetails
    {
        bool InsertTransaction(TransactionDetails transaction);
    }
}
