﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.CommonDTO.Models;
using UserDetails = IVL.ArabChaldo.DAL.Models.UserDetails;
using System.Data;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface IUserDetails
    {
        bool InsertUser(UserDetails userdetails);
        bool CheckUserExits(string EmailID);
        UserDetails GetUserbyId(long ID);
        UserDetails GetUserbyEmail(string Email);
        bool DeleteUser(long ID);

        bool UpdateUsers(long ID, UserDetails userdetails); 

        DataTable GetAllUser();

      //  List<UserDetails> GetUsersForThirdParty();

        string ThirdPartyUserName(long id);
    }
}
