﻿using IVL.ArabChaldo.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface IWebsiteDetails
    {
        DataTable GetAllAdDetails(string imgurl);
        DataTable GetAllJobDetails(string imgurl);
        List<AdDetails> GetAdsAdListBycategoryId(long categoryId);

        List<AdDetails> GetAdsAdListBySubcategoryId(long SubcategoryId);
    
        AdDetails GetAdDetailsById(long AdDetailsID);

        BannerDetails GetBannerById(long id);

        DataTable GetAllBanner(string imgurl);

        DataTable getPremiumBannerList(string imgurl);

        BlogDetails GetBlogById(long id);

        DataTable GetAllBlog();
        EventDetails GetEventById(long id);

        List<EventDetails> GetAllEvent();
        DataTable GetEventsByCategoryId(long categoryId);
        DataTable GetAllCategoryData();
        DataTable GetAllCityData();
        DataTable GetAdListBySearchCriteria(string searchCriteria);

        DataTable GetAboutUSData();
        DataTable GetContactUSData();
        DataTable GetTermsofUseData();
        DataTable GetPrivacyPolicyData();
        DataTable GetFAQData();
        DataTable GetDisclaimerData();

        DataTable GetAllBuysellData(string imgurl);

        DataTable GetAllBuysellRecentData(string imgurl);
        BuySellDetails GetBuysellById(long id);

        SmallAdDetails GetSmallAdDetailsById(long smallAdId);
        DataTable GetAllSmallAdDetails(string imgurl);
        DataTable GetCitySearchDDL();
        DataTable GetCategorySearchDDL();
    }
}
