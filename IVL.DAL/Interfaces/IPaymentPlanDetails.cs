﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using System.Data;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface IPaymentPlanDetails
    {
        long InsertPlan(PaymentPlan paymentPlan);
        DataTable GetAllPlan();
        List<PaymentPlan> GetPlanListForUser(int userId, string location);

        PaymentPlan getPlanIdDetails(int userId);
        void UpdatePlanAfterPayment(long planId);

        void UpdatePlanDetails(long id, PaymentPlan paymentPlan);
        //void UpdatePlanAfterPayment(long planId);
        void UpdatePlanDetailsForBanner(long id, long bannerImageCount);
        void UpdatePlanDetailsForEvent(long id, long eventImageCount);
        void UpdatePlanDetailsForAds(long id, long logoImageCount, long adsImageCount, long jobsCount);
        void UpdatePlanDetailsSmallAds(long planId, long logoImageCount, long adsImageCount);
        void UpdatePlanDetailsForBuySell(long planId, long BuySellCount);


    }
}
