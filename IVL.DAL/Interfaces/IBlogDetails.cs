﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
namespace IVL.ArabChaldo.DAL.Interfaces
{
   public interface IBlogDetails
    {
        bool Insert(BlogDetails blogDetails);
        bool Update(long id, BlogDetails blogDetails);
        bool Delete(long id);

        bool BlogStatus(long BlogId, string remarks, string Action,long CreatedBy);
        BlogDetails GetBlogById(long id);



        DataTable GetAllBlog(long loggedInUserId, string loggedInRole);
    }
}

