﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using PostDetails = IVL.ArabChaldo.DAL.Models.PostDetails;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface IPostDetails
    {
        bool InsertPostDetails(PostDetails country);
        bool CheckPostDetailsExits(string name);
        PostDetails GetPostDetailsById(int PostDetailsID);
        bool AdStatus(int PostDetailsID, string Action);

        bool UpdatePostDetails(int PostDetailsID, PostDetails PostDetails);

        List<PostDetails> GetAllPostDetails();
    }
}
