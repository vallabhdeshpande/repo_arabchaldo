﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using State = IVL.ArabChaldo.DAL.Models.State;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface IState
    {
        //bool Insert(State State);
        //bool Update(State State);
        //bool Delete(int id);

        bool InsertState(State state);
        bool CheckStateExits(string StateName, long StateId);
        State GetStateById(int id);
        bool DeleteState(int id);

        bool UpdateState(int StateID, State State);

        List<State> GetAllState();
        List<State> GetAllStatesByCountryId(int countryId);
        DataTable FillStateDDLByCountryId(int countryId);


        DataTable FillStateList();
    }
}
