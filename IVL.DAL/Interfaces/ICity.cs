﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using City = IVL.ArabChaldo.DAL.Models.City;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface ICity
    {
        //bool Insert(City City);
        //bool Update(City City);
        //bool Delete(int id);

        bool InsertCity(City city);
        bool CheckCityExits(string name, long StateId, long CityId);
        City GetCityById(int id);
        bool DeleteCity(int id);

        bool UpdateCity(int CityID, City City);

        List<City> GetAllCity();
        List<City> GetAllCitiesByStateId(int stateId);
        //List<City> GetCityWithPagination(string getCityWithPagination);
        DataTable FillCityList();
    }
}
