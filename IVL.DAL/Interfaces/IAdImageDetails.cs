﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using AdImageDetails = IVL.ArabChaldo.DAL.Models.AdImageDetails;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface IAdImageDetails
    {
        bool InsertAdImageDetails(AdImageDetails country);
        bool CheckAdImageDetailsExits(string name);
        AdImageDetails GetAdImageDetailsById(int AdImageDetailsID);
        bool DeleteAdImageDetails(int AdImageDetailsID);

        bool UpdateAdImageDetails(int AdImageDetailsID, AdImageDetails AdImageDetails);

        List<AdImageDetails> GetAllAdImageDetails();
    }
}
