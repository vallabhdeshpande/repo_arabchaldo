﻿using IVL.ArabChaldo.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using Microsoft.EntityFrameworkCore.Internal;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface IConfiguration
    {
        bool DeleteConfig(int ID);
        Configuration GetConfigDetailsbyId(int Id);
        Configuration GetExistingConfigDetails();
        bool UpdateConfig(int ID, Configuration userdetails);

        List<Configuration> GetAllConfigDetails();

    }
}
