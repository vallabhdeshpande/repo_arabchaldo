﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.CommonDTO.Models;
using Category = IVL.ArabChaldo.DAL.Models.Category;
using System.Data;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface IManageCategory
    {
        bool InsertCategory(Category catgorydetails);
        bool CheckCategoryExits(string CategoryName);
        Category GetCategorybyId(int CategoryID);
        bool DeleteCategory(int CategoryID);

        bool UpdateCategory(int CategoryID, Category Categorydetails);

        List<Category> GetAllCategory();

        DataTable FillCategoryList();
    }
}
