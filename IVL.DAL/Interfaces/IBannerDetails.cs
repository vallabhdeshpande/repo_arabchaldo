﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.DAL.Models;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface IBannerDetails
    {
        bool Insert(BannerDetails bannerDetails);
        bool Update(long id, BannerDetails bannerDetails);
        bool Delete(long id);

        bool BannerStatus(long bannerId, string remarks, string Action, long CreatedBy);
        BannerDetails GetBannerById(long id);

        DataTable GetAllBanner(int id);
        DataTable FillBannerList();
    }
}
