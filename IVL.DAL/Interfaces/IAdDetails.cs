﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using AdDetails = IVL.ArabChaldo.DAL.Models.AdDetails;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface IAdDetails
    {
        bool InsertAdDetails(AdDetails country);
        bool CheckAdDetailsExits(string Title, long CategoryId,long AdId);
        AdDetails GetAdDetailsById(long AdDetailsID);
        bool AdStatus(long AdDetailsID,string remarks,string Action, long CreatedBy);

        bool UpdateAdDetails(long AdDetailsID, AdDetails AdDetails);

        List<AdDetails> GetAllAdDetails(long loggedInUserId, string loggedInRole);

        List<AdDetails> GetAllJobDetails(long loggedInUserId, string loggedInRole);
        List<AdDetails> GetAdsAdListBycategoryId(long categoryId);

        List<AdDetails> GetAdsAdListBySubcategoryId(long SubcategoryId);
        DataTable GetAllAdsDetails(long loggedInUserId, string loggedInRole, string categoryName);
    }
}
