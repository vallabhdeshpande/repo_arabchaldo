﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace IVL.ArabChaldo.DAL.Interfaces
{
   public interface IDashboard
    {
         DataTable GetotalCount();
         DataTable GetBargraphdata( long year);
         DataTable GeBUtotalCount(long UserId);
    }
}
