﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.DAL.Models;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface IEventDetails
    {
        bool Insert(EventDetails eventDetails);
        bool Update(long id, EventDetails eventDetails);
        bool Delete(long id);

        bool EventStatus(long EventId, string remarks, string Action, long CreatedBy);
        EventDetails GetEventById(long id);

        DataTable GetAllEvent(long loggedInUserId, string loggedInRole);
        List<EventDetails> GetEventsByCategoryId(long categoryId);
    }
}
