﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using MessageBoard = IVL.ArabChaldo.DAL.Models.MessageBoard;
using MessageBoardDetails = IVL.ArabChaldo.DAL.Models.MessageBoardDetails;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface IMessageBoard   
        {
            //bool Insert(MessageBoard MessageBoard);
            //bool Update(MessageBoard MessageBoard);
            //bool Delete(int id);

            bool InsertMessageBoard(MessageBoard messageBoard);
            bool CheckMessageBoardExits(string Name, long CategoryId);
            MessageBoard GetMessageBoardById(long messageBoardId);
            bool DeleteMessageBoard(long messageBoardId);

            bool UpdateMessageBoard(long MessageBoardID, MessageBoard messageBoard);

           DataTable GetAllMessageBoard();
        DataTable FillMessageBoardList();


        // Message Board Details
        List<MessageBoardDetails> GetMessageBoardReplyByMessageBoardId(long messageBoardId);
        bool DeleteMessageBoardDetails(long messageBoardId);
        bool InsertMessageBoardDetails(MessageBoardDetails messageBoardDetails);
        DataTable FillMessageBoardReplyList(long MessageBoardId);


        }
    }
