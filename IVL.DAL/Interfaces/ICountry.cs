﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using Country = IVL.ArabChaldo.DAL.Models.Country;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface ICountry
    {
        bool InsertCountry(Country country);
        bool CheckCountryExits(string name);
        Country GetCountryById(int CountryID);
        bool DeleteCountry(int CountryID);

        bool UpdateCountry(int CountryID, Country Country);

        List<Country> GetAllCountry();
    }
}
