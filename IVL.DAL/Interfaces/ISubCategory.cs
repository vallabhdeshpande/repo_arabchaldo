﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using SubCategory = IVL.ArabChaldo.DAL.Models.SubCategory;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface ISubCategory
    {
       

        bool InsertSubCategory(SubCategory subCategory);
        bool CheckSubCategoryExits(string subCategoryName,long categoryId);
        SubCategory GetSubCategoryById(int id);
        bool DeleteSubCategory(int id);

        bool UpdateSubCategory(int SubCategoryID, SubCategory SubCategory);

        bool GetSubCategoryCountForCategory(int categoryId);
        bool UpdateSubcategoryWhenCategoryUpdated(int categoryId, string categoryName);

        List<SubCategory> GetAllSubCategory();
        List<SubCategory> GetAllSubCategoriesByCategoryId(int CategoryId);


        DataTable FillSubcategoryList();
    }
}

