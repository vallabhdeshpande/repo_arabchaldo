﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.DAL.Models;


namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface ICommon
    {
        #region Dropdowns
        DataTable FillCountryDDL();
        DataTable FillStateDDLByCountryId(long countryId);
        DataTable FillCityDDLByStateId(long stateId);
        DataTable FillRoleDDL(string roleName);
        DataTable FillUserDDL();
        DataTable FillUserByRoleDDL(long roleId);
        DataTable FillCategoryDDL();
        DataTable FillSubCategoryDDLByCategoryId(long CategoryId);

        DataTable FillPopularCategory();
        DataTable FillPopularEventsCategory();
        DataTable FillPopularJobSubCategory();
        DataTable JobCategoryDDL();
        #endregion


        #region Grid
        //DataTable FillCityList();
        #endregion

        DataTable DisplayWorkFlow(long Id, long PostTypeId);
        DataTable FillBUCityDDLByStateId(long stateId);
    }
}
