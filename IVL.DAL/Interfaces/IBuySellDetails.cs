﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using BuySellDetails = IVL.ArabChaldo.DAL.Models.BuySellDetails;
namespace IVL.ArabChaldo.DAL.Interfaces
{
   public interface IBuySellDetails
    {
        bool CheckbuySellDetailsExits(string Title, long CategoryId, long buysellId);
        bool InsertBuySellDetails(BuySellDetails buyselldetails);
        DataTable GetAllBuySellDetails(long loggedInUserId, string loggedInRole);

        BuySellDetails GetbuySellDetailsById(long buySellId);

        bool UpdateBuySellDetails(long buysellId, BuySellDetails buySellDetails);

        bool buySellAdStatus(long buySellId, string remarks, string Action, long CreatedBy);
        List<BuySellDetails> GetAllBuySellsDetails(long loggedInUserId, string loggedInRole);


    }
}
