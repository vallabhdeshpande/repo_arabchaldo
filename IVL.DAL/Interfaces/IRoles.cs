﻿using IVL.ArabChaldo.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.DAL.Interfaces
{
   public interface IRoles
    {
        List<Roles> GetAllUserRoles();
    }
}
