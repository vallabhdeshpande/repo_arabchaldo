﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface IUnitOfWork 
    {
        // property 
        void Commit();
        IBannerDetails BannerDetails { get; }
        IBlogDetails BlogDetails { get; }
        IEventDetails EventDetails { get; }
        IUserDetails userDetails { get; }
        IManageCategory categoryDetails { get; }
        ISubCategory SubCategory { get; }
        ICountry Country { get; }
        IRoles Roles { get; }
        IState State { get; }
        ICity City { get; }
        IAdDetails AdDetails { get; }
        IAdImageDetails AdImageDetails { get; }
        IAdStatusDetails AdStatusDetails { get; }
        ILoginDetails LoginDetails { get; }
        IWebsiteDetails WebsiteDetails { get; }
        IConfiguration ConfigDetails { get; }
     
        ICommon CommonDetails { get; }
        IPaymentPlanDetails PaymentPlan { get; }
        IMessageBoard MessageBoard { get; }
        ITransactionDetails TransactionDetails { get; }
        IBuySellDetails BuySellDetails { get; }
        IDashboard Dashboard { get; }
        ISmallAdDetails SmallAdDetails { get; }
        void Dispose();

    }
}
