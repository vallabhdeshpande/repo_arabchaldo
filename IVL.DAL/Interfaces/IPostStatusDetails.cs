﻿using System;
using System.Collections.Generic;
using System.Text;
using PostStatusDetails = IVL.ArabChaldo.DAL.Models.PostStatusDetails;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface IPostStatusDetails
    {
        bool InsertPostStatusDetails(PostStatusDetails PostStatusDetails);
        // bool CheckPostStatusDetailsExits(long WorkflowStatusId);
        // PostStatusDetails GetPostStatusDetailsById(int PostStatusDetailsID);
        // bool DeletePostStatusDetails(int PostStatusDetailsID);

        // bool UpdatePostStatusDetails(int PostStatusDetailsID, PostStatusDetails PostStatusDetails);

        // List<PostStatusDetails> GetAllPostStatusDetails();
    }
}
