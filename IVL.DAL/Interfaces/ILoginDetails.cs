﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using LoginDetails = IVL.ArabChaldo.DAL.Models.LoginDetails;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface ILoginDetails
    {
        LoginDetails GetLogin(string UserName,string Password);
        string GetLoginInfo(string userName, string password);
        LoginDetails GetLoginByUserID(long userId);

        bool ResetPassword(long userId, LoginDetails loginDetails);
        bool CheckUserRegistered(string userEmail);
        bool RegisterUser(LoginDetails registerNewUser);
        bool ActivateUserAccount(string info);
        DataTable GetUserInfoByEmail(string email);
    }
}
