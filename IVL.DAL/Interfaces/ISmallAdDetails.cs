﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using SmallAdDetails = IVL.ArabChaldo.DAL.Models.SmallAdDetails;

namespace IVL.ArabChaldo.DAL.Interfaces
{
    public interface ISmallAdDetails
    {
        bool InsertSmallAdDetails(SmallAdDetails SmallAdDetails);
        bool CheckSmallAdDetailsExits(string Title, long SmallAdId);
        SmallAdDetails GetSmallAdDetailsById(long SmallAdDetailsID);
        bool SmallAdStatus(long SmallAdDetailsID, string remarks, string Action, long CreatedBy);

        bool UpdateSmallAdDetails(long SmallAdDetailsID, SmallAdDetails SmallAdDetails);

        List<SmallAdDetails> GetAllSmallAdDetails(long loggedInUserId, string loggedInRole);
      
        DataTable GetAllSmallAdsDetails(long loggedInUserId, string loggedInRole);
    }
}
