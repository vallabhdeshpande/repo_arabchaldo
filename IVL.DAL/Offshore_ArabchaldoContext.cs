﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace IVL.ArabChaldo.DAL.Models
{
    public partial class Offshore_ArabchaldoContext
    {

        public class SearchAdList
        {
            public long adId { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            public string CategoryName { get; set; }
            public string ImageURL { get; set; }
            public string ContactNumber { get; set; }
            public string Email { get; set; }
            public DateTime CreatedDate { get; set; }
            public long CreatedBy { get; set; }
            public long? VisitorCount { get; set; }
            public bool? IsPaidAd { get; set; }

            public string UserName { get; set; }

            public bool? IsFeatured { get; set; }

            [NotMapped]
            public string SubCategoryName { get; set; }
        }

        public class DashBoardList
        {
            public string Adsresult { get; set; }
            public string BannerResult { get; set; }
            public string EventResult { get; set; }
            public string PlanResult { get; set; }
            public string RevenueResult { get; set; }

            public string SmallAdsResult { get; set; }
            public string BuySellResults { get; set; }
            public string JobsResults { get; set; }
            public string BlogsResults { get; set; }

        }
        public class CategoryList
        {
            public long categoryId { get; set; }
            public string categoryName { get; set; }
            public string CategoryImageUrl { get; set; }
            public int SubCategoryCount { get; set; }
            public string arabicName { get; set; }

        }
        public class CategorySearchDDL
        {
            public long categoryId { get; set; }
            public string categoryName { get; set; }
            public string arabicName { get; set; }
        }
        public class CitySearchDDL
        {
            public long cityId { get; set; }
            public string cityName { get; set; }
            public string stateName { get; set; }
        }

        public class SubCategoryList
        {
            public long subcategoryId { get; set; }
            public string subcategoryName { get; set; }
            public int SubCategoryCount { get; set; }
            public string arabicName { get; set; }

        }
        public class DashBoardCountList
        {

            public long ActiveBusiLitingCount { get; set; }
            public long PendingBusiLitingCount { get; set; }
            public long ActiveSmallAdsCount { get; set; }
            public long PendingSmallAdsCount { get; set; }
            public long ActiveBannerCount { get; set; }
            public long PendingBannerCount { get; set; }
            public long ActiveEventCount { get; set; }
            public long PendingEventCount { get; set; }
            public long ActiveJobsCount { get; set; }
            public long PendingJobsCount { get; set; }
            public long ActiveBlogsCount { get; set; }
            public long PendingBlogsCount { get; set; }
            public long ActiveBuysellCount { get; set; }
            public long PendingBuysellCount { get; set; }

            public long UserCount { get; set; }
            public long AdsCount { get; set; }
            public long BannerCount { get; set; }
            public long EventCount { get; set; }
            public long JobsCount { get; set; }
            public long BlogsCount { get; set; }
            public long buysellCount { get; set; }
            public long smallAdsCount { get; set; }

            public long TotalRevenueAmount { get; set; }

            public long OnlineAmount { get; set; }
            public long OfflineAmount { get; set; }
            public long paidUser { get; set; }

            public long FreeUser { get; set; }
            public long TotalAdvertimentCount { get; set; }
            public long paidAdvertisementCount { get; set; }
            public long freeAdvertisementCount { get; set; }
            public long MessageTopicCount { get; set; }
            public long MessageReplyCount { get; set; }
        }

        public class BUDashBoardCountList
        {
            public long ActiveBusiLitingCount { get; set; }
            public long PendingBusiLitingCount { get; set; }
            public long ActiveSmallAdsCount { get; set; }
            public long PendingSmallAdsCount { get; set; }
            public long ActiveBannerCount { get; set; }
            public long PendingBannerCount { get; set; }
            public long ActiveEventCount { get; set; }
            public long PendingEventCount { get; set; }
            public long ActiveJobsCount { get; set; }
            public long PendingJobsCount { get; set; }
            public long ActiveBlogsCount { get; set; }
            public long PendingBlogsCount { get; set; }
            public long ActiveBuysellCount { get; set; }
            public long PendingBuysellCount { get; set; }
            public long MessageTopicCount { get; set; }
            public long MessageReplyCount { get; set; }
            //public long? FreeAds { get; set; }
            //public long? PaidAds { get; set; }
            //public long? ApprovedAds { get; set; }
            //public long? RejectedAds { get; set; }
            //public long? PendingAds { get; set; }
            //public long? FreeBanner { get; set; }
            //public long? PaidBanner { get; set; }

            //public long? ApprovedBanner { get; set; }
            //public long? RejectedBanner { get; set; }
            //public long? PendingBanner { get; set; }

            //public long? FreeEvents { get; set; }
            //public long? PaidEvents { get; set; }
            //public long? ApprovedEvents { get; set; }
            //public long? RejectedEvents { get; set; }
            //public long? PendingEvents { get; set; }
            //public long? FreeJobs { get; set; }
            //public long? PaidJobs { get; set; }

            //public long? ApprovedJobs { get; set; }
            //public long? RejectedJobs { get; set; }
            //public long? PendingJobs { get; set; }
            public long? AdsImageCount { get; set; }
            public long? BannerImageCount { get; set; }

            public long? EventImageCount { get; set; }
            public long? LogoCount { get; set; }
            public long? JobsAdvCount { get; set; }

            public long? BuySellCount { get; set; }
            public long? SmallAdsCount { get; set; }
        }

        public class AdDetailsList
        {
            public long AdId { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string CategoryName { get; set; }
            public string ImageURL { get; set; }
            public string ContactNumber { get; set; }
            public string Email { get; set; }
            public string CreatedDate { get; set; }
            public long CreatedBy { get; set; }
            public long? VisitorCount { get; set; }
            public bool? IsPaidAd { get; set; }

            public string UserName { get; set; }

            public bool? IsFeatured { get; set; }

            [NotMapped]
            public string SubCategoryName { get; set; }
        }

        public class SmallAdDetailsList
        {
            public long SmallAdId { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string ImageURL { get; set; }
            public string ContactNumber { get; set; }
            public string Email { get; set; }
            public string CreatedDate { get; set; }
            public long CreatedBy { get; set; }
            public long? VisitorCount { get; set; }
            public bool? IsPaidAd { get; set; }

            public string UserName { get; set; }

            public bool? IsPremium { get; set; }
        }
        public DbQuery<SearchAdList> ssp_GetAdListResult { get; set; }

        public DbQuery<DashBoardList> ssp_GetDashboradList { get; set; }

        public DbQuery<DashBoardCountList> ssp_GetDashboradCountList { get; set; }

        public DbQuery<BUDashBoardCountList> ssp_GetBUDashboradCountList { get; set; }

        public DbQuery<CategoryList> ssp_GetCategoryList { get; set; }
        public DbQuery<SubCategoryList> ssp_GetSubCategoryList { get; set; }
        public DbQuery<CategoryList> ssp_GetPopularCategoriesList { get; set; }
        public DbQuery<SubCategoryList> ssp_GetPopularJobSubCategoriesList { get; set; }
        public DbQuery<AdDetailsList> ssp_GetAllAdsList { get; set; }
        public DbQuery<SmallAdDetailsList> ssp_GetAllSmallAdsList { get; set; }
        public DbQuery<AllMessageBoardlist> ssp_GetMessageBoardData { get; set; }
        public DbQuery<AllCategorylist> ssp_GetAllCategoryList { get; set; }
        public DbQuery<AllSubCategorylist> ssp_GetAllSubCategoryList { get; set; }
        public DbQuery<AllStatelist> ssp_GetAllStateList { get; set; }
        public DbQuery<AllCitylist> ssp_GetAllCityList { get; set; }

        public List<SearchAdList> ssp_GetAdListBySearchCriteria(string searchText, long categoryId, long cityId)
        {
            SqlParameter paramSearchText = new SqlParameter("@searchText", searchText);
            SqlParameter paramCategoryId = new SqlParameter("@categoryId", categoryId);
            SqlParameter paramCityId = new SqlParameter("@cityId", cityId);
            try
            {
                return ssp_GetAdListResult.FromSql("Execute ssp_GetAdListBySearchCriteria @searchText,@categoryId,@cityId", paramSearchText, paramCategoryId, paramCityId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<DashBoardList> ssp_GetDashBoardData(long year)
        {
            try
            {
                SqlParameter paramYear = new SqlParameter("@Year", year);
                return ssp_GetDashboradList.FromSql("Execute ssp_GetDashboardData @Year", paramYear).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public List<DashBoardCountList> ssp_GetDashBoardCountList()
        {
            try
            {
                return ssp_GetDashboradCountList.FromSql("Execute ssp_GetDashBoardCountList").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public List<BUDashBoardCountList> ssp_GetBUDashBoardCountList(long UserId)
        {
            try
            {
                SqlParameter paramUserId = new SqlParameter("@UserId", UserId);
                return ssp_GetBUDashboradCountList.FromSql("Execute ssp_GetBUDashBoardCountList @UserId", paramUserId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CategoryList> ssp_GetCategoriesList()
        {
            try
            {

                return ssp_GetCategoryList.FromSql("Execute GetCategoryList").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public List<CategorySearchDDL> ssp_GetCategorySearchDDL(string listType)
        {
            try
            {
                SqlParameter paramListType = new SqlParameter("@ListType", listType);
                return ssp_GetCategorySearchDDLData.FromSql("Execute ssp_CityCategoryListForSearch @ListType", paramListType).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CitySearchDDL> ssp_GetCitySearchDDL(string listType)
        {
            try
            {
                SqlParameter paramListType = new SqlParameter("@ListType", listType);
                return ssp_GetCitySearchDDLData.FromSql("Execute ssp_CityCategoryListForSearch @ListType", paramListType).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public List<SubCategoryList> ssp_GetSubCategoriesList()
        {
            try
            {

                return ssp_GetSubCategoryList.FromSql("Execute GetSubCategoryList").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public List<CategoryList> ssp_GetAllPopularCategoriesList(string AdType)
        {
            try
            {
                SqlParameter paramAdType = new SqlParameter("@AdType", AdType);
                return ssp_GetPopularCategoriesList.FromSql("Execute GetPopularCategoryList @AdType", paramAdType).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public List<SubCategoryList> ssp_GetPopularSubCategoriesList(string AdType)
        {
            try
            {
                SqlParameter paramAdType = new SqlParameter("@AdType", AdType);
                return ssp_GetPopularJobSubCategoriesList.FromSql("Execute GetPopularCategoryList @AdType", paramAdType).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public List<AdDetailsList> ssp_GetAllAdsDetailsList(long categoryId, long subcategoryId, string categoryName)
        {
            try
            {
                SqlParameter paramCategoryId = new SqlParameter("@CategoryId", categoryId);
                SqlParameter paramSubCategoryId = new SqlParameter("@SubCategoryId", subcategoryId);
                SqlParameter paramCategoryName = new SqlParameter("@CategoryName", categoryName);
                return ssp_GetAllAdsList.FromSql("Execute GetAllAdsListing @CategoryId,@SubCategoryId,@CategoryName", paramCategoryId, paramSubCategoryId, paramCategoryName).ToList();
                
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public List<AdDetailsList> ssp_GetAllJobsDetailsList(long subcategoryId, string categoryName)
        {
            try
            {
               SqlParameter paramSubCategoryId = new SqlParameter("@SubCategoryId", subcategoryId);
                SqlParameter paramCategoryName = new SqlParameter("@CategoryName", categoryName);
                return ssp_GetAllAdsList.FromSql("Execute GetAllJobsListing @SubCategoryId,@CategoryName", paramSubCategoryId, paramCategoryName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public List<SmallAdDetailsList> ssp_GetAllSmallAdsDetailsList(string type)
        {
            try
            {
                SqlParameter paramType = new SqlParameter("@Type", type);
                return ssp_GetAllSmallAdsList.FromSql("Execute GetAllSmallAdsListing @Type", paramType).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #region All Ads
        // Select All Ads Data 

        public class AllAdslist
        {
            public long AdId { get; set; }
            public string Title { get; set; }
            public string CategoryName { get; set; }
            public string AdLogoUrl { get; set; }
            public string CityName { get; set; }
            public string Status { get; set; }
            public bool? IsFeatured { get; set; }
            public bool? IsVisible { get; set; }
            public bool? IsPaidAd { get; set; }
            public bool IsActive { get; set; }
            public string CreatedDate { get; set; }
            public long CreatedBy { get; set; }
            public string UpdatedDate { get; set; }
            public string ValidFromDate { get; set; }
            public string ValidTillDate { get; set; }
            public bool Activity { get; set; }
            public string IsPremium { get; set; }
            public string SubCategoryName { get; set; }


        }

        public class PaymentPlanList
        {
            public long PlanId { get; set; }
            public string PlanName { get; set; }
            public long UserId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public bool IsPaid { get; set; }
            public string ValidFrom { get; set; }
            public string ValidTill { get; set; }
            public decimal FinalAmount { get; set; }
        }

        public class BannerList
        {
            public long BannerId { get; set; }
            public long PostedFor { get; set; }
            public string Title { get; set; }
            public string CityName { get; set; }
            public bool? IsFeatured { get; set; }
            public bool? IsVisible { get; set; }
            public bool? IsActive { get; set; }
            public string CreatedDate { get; set; }
            public string ValidFromDate { get; set; }
            public string ValidTillDate { get; set; }
        }

        public class PremiumBannerList
        {
            public long BannerId { get; set; }
            public string imageUrl { get; set; }            
        }

        public class EventList
        {
            public long EventId { get; set; }
            public string ImageUrl { get; set; }
            public string Title { get; set; }
            public bool? IsVisible { get; set; }
            public bool? IsActive { get; set; }
            public string CityName { get; set; }
            public string Status { get; set; }
            public string IsPremium { get; set; }
            public string CreatedDate { get; set; }
            public string EventDate { get; set; }
            public long CreatedBy { get; set; }
            public string UpdatedDate { get; set; }
            public string ValidFromDate { get; set; }
            public string ValidTillDate { get; set; }
            public bool Activity { get; set; }
        }

        public DbQuery<AllAdslist> ssp_GetAllAdDetails { get; set; }

        public List<AllAdslist> ssp_GetAllAds(string LoggedInRole, string CategoryName, long LoggedInUserId)
        {
            try
            {
                SqlParameter paramLoggedInRole = new SqlParameter("@loggedInRole", LoggedInRole);
                SqlParameter paramCategoryName = new SqlParameter("@CategoryName", CategoryName);
                SqlParameter paramLoggedInUserId = new SqlParameter("@loggedInUserId", LoggedInUserId);
                return ssp_GetAllAdDetails.FromSql("Execute ssp_GetAllAdDetails @loggedInRole,@CategoryName,@loggedInUserId", paramLoggedInRole, paramCategoryName, paramLoggedInUserId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion

        #region All Small Ads
        // Select All Small Ads Data 

        public class AllSmallAdslist
        {
            public long SmallAdId { get; set; }
            public string Title { get; set; }

            public string AdLogoUrl { get; set; }
            public string CityName { get; set; }
            public string Status { get; set; }
            //public bool? IsPremium { get; set; }
            public bool? IsVisible { get; set; }
            public bool? IsPaidAd { get; set; }
            public bool IsActive { get; set; }
            public string CreatedDate { get; set; }
            public long CreatedBy { get; set; }
            public string UpdatedDate { get; set; }
            public string ValidFromDate { get; set; }
            public string ValidTillDate { get; set; }
            public bool Activity { get; set; }
            public string IsPremium { get; set; }


        }
        public DbQuery<AllSmallAdslist> ssp_GetAllSmallAdDetails { get; set; }
        public List<AllSmallAdslist> ssp_GetAllSmallAds(string LoggedInRole, long LoggedInUserId)
        {
            try
            {
                SqlParameter paramLoggedInRole = new SqlParameter("@loggedInRole", LoggedInRole);
                SqlParameter paramLoggedInUserId = new SqlParameter("@loggedInUserId", LoggedInUserId);
                return ssp_GetAllSmallAdDetails.FromSql("Execute ssp_GetAllSmallAdDetails @loggedInRole,@loggedInUserId", paramLoggedInRole, paramLoggedInUserId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion

        #region Events
        public class WebsiteEventList
        {
            public long eventId { get; set; }
            public string title { get; set; }
            public string imageUrl { get; set; }
            public bool? isVisible { get; set; }
            public bool? isActive { get; set; }
            public long categoryId { get; set; }
            public bool? isPaidAd { get; set; }
            public long visitorCount { get; set; }
            public bool? isPremium { get; set; }
            public string eventDate { get; set; }
        }

        public DbQuery<WebsiteEventList> ssp_GetEventListForWebsite { get; set; }

        public List<WebsiteEventList> ssp_GetAllEventForWebsite(long catId)
        {
            try
            {
                SqlParameter categoryId = new SqlParameter("@categoryId", catId);
                return ssp_GetEventListForWebsite.FromSql("Execute ssp_GetEventListForWebsite @categoryId", categoryId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region All buysell
        public class AllBuySelllist
        {
            public long BuySellId { get; set; }
            public string ImageUrl { get; set; }
            public string Title { get; set; }
            public string SubCategoryName { get; set; }
            public string CityName { get; set; }
            public string Status { get; set; }
            public string IsPremium { get; set; }
            public bool? IsVisible { get; set; }
            public bool? IsPaidAd { get; set; }
            public bool IsActive { get; set; }
            public string CreatedDate { get; set; }
            public long CreatedBy { get; set; }
            public string UpdatedDate { get; set; }
            public string ValidFromDate { get; set; }
            public string ValidTillDate { get; set; }
            public bool Activity { get; set; }

        }

        public class BuySelllist
        {
            public long BuySellId { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string ImageUrl { get; set; }
            public string CategoryName { get; set; }

            public string ContactNumber { get; set; }

            public string Email { get; set; }
            public string IsPremium { get; set; }
            public string UserName { get; set; }
            public bool? IsVisible { get; set; }
            public bool? IsPaidAd { get; set; }
            public bool IsActive { get; set; }
            public string CreatedDate { get; set; }
            public long CreatedBy { get; set; }
            public long? VisitorCount { get; set; }

            public string SubCategoryName { get; set; }

        }
        public DbQuery<CategorySearchDDL> ssp_GetCategorySearchDDLData { get; set; }

        public DbQuery<CitySearchDDL> ssp_GetCitySearchDDLData { get; set; }
        public DbQuery<AllBuySelllist> ssp_GetAllBuySellDetails { get; set; }
        public DbQuery<BuySelllist> ssp_GetAllBuySellListing { get; set; }
        public DbQuery<PaymentPlanList> ssp_GetAllPaymentPlan { get; set; }
        public DbQuery<BannerList> ssp_GetAllBannerList{get; set;}
        public DbQuery<PremiumBannerList> ssp_GetWebHomePageBanner { get; set; }
        public DbQuery<EventList> ssp_GetAllEventList { get; set; }

        public List<AllBuySelllist> ssp_GetBuySellList(string LoggedInRole, long LoggedInUserId)
        {
            try
            {
                SqlParameter paramLoggedInRole = new SqlParameter("@loggedInRole", LoggedInRole);
                SqlParameter paramLoggedInUserId = new SqlParameter("@loggedInUserId", LoggedInUserId);
                return ssp_GetAllBuySellDetails.FromSql("Execute ssp_GetAllBuySellDetails @loggedInRole,@loggedInUserId", paramLoggedInRole, paramLoggedInUserId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public List<BuySelllist> ssp_GetAllBuySellList()
        {
            try
            {
                return ssp_GetAllBuySellListing.FromSql("Execute GetAllBuySellListing").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public List<BuySelllist> ssp_GetAllBuySellRecentList()
        {
            try
            {
                return ssp_GetAllBuySellListing.FromSql("Execute GetAllBuySellRecentListing").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PaymentPlanList> ssp_GetAllPlanListDetails()
        {

            try
            {
                return ssp_GetAllPaymentPlan.FromSql("EXEC ssp_GetAllPaymentPlan").ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public  List<BannerList> ssp_GetAllBannerListDetails(long userId)
        {
            try
            {
                SqlParameter paramLoggedInRole = new SqlParameter("@loggedInUserId", userId);
                List<BannerList> bannerListDetails = ssp_GetAllBannerList.FromSql("EXEC ssp_GetAllBannerList @loggedInUserId", paramLoggedInRole).ToList();
                return bannerListDetails;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PremiumBannerList> ssp_GetWebSiteHomePageBannerList(string imgUrl, string location)
        {
            try
            {
                //string location = "Premium";
                SqlParameter imageUrl = new SqlParameter("@imgUrl", imgUrl);
                SqlParameter LocationDetails = new SqlParameter("@location", location);
                List<PremiumBannerList> bannerListDetails = ssp_GetWebHomePageBanner.FromSql("EXEC ssp_GetWebHomePageBanner @imgUrl, @location", imageUrl, LocationDetails).ToList();
                return bannerListDetails;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EventList> ssp_GetAllEventListDetails(long loggedInUserId, string loggedInRole)
        {
            try
            {
                SqlParameter paramLoggedInUserId = new SqlParameter("@loggedInUserId", loggedInUserId);
                SqlParameter paramLoggedInRole = new SqlParameter("@loggedInRole", loggedInRole);
                var eventListDetails = ssp_GetAllEventList.FromSql("EXEC ssp_GetAllEventList @loggedInRole, @loggedInUserId", paramLoggedInRole, paramLoggedInUserId).ToList();
                List<EventList> check = eventListDetails;
                return eventListDetails;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region MessageBoard
        public class AllMessageBoardlist
        {
            public long messageBoardId { get; set; }
            public string topic { get; set; }
            public string description { get; set; }
            public string name { get; set; }
            public long categoryId { get; set; }
            public long views { get; set; }          
            public bool IsActive { get; set; }
            public string CreatedDate { get; set; }
            public long CreatedBy { get; set; }
            public string UpdatedDate { get; set; }
            public long UpdatedBy { get; set; }
        }

        public List<AllMessageBoardlist> ssp_MessageBoardData()
        {
            try
            {
               
                return ssp_GetMessageBoardData.FromSql("Execute ssp_GetMessageBoardData").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion

        #region CategoryList
        public class AllCategorylist
        {
            public long CategoryId { get; set; }
            public string CategoryName { get; set; }    
            public string ArabicName { get; set; }
            public string Description { get; set; }
            public bool IsActive { get; set; }  
        }

        public List<AllCategorylist> ssp_GetAllCategoryListDetails()
        {
            try
            {
                return ssp_GetAllCategoryList.FromSql("Execute ssp_GetAllCategoryList").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SubCategoryList
        public class AllSubCategorylist
        {
            public long subCategoryId { get; set; }
            public string subCategoryName { get; set; }
            public string arabicName { get; set; }
            public string description { get; set; }
            public long categoryId { get; set; }
            public string categoryName { get; set; }
            public bool IsActive { get; set; }
        }

        public List<AllSubCategorylist> ssp_GetAllSubCategoryListDetails()
        {
            try
            {
                return ssp_GetAllSubCategoryList.FromSql("Execute ssp_GetAllSubCategoryList").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region StateList
        public class AllStatelist
        {
            public long stateId { get; set; }
            public string stateName { get; set; }
            public string description { get; set; }
            public long countryId { get; set; }
            public string countryName { get; set; }
            public bool isActive { get; set; }
        }

        public List<AllStatelist> ssp_GetAllStateListDetails()
        {
            try
            {
                return ssp_GetAllStateList.FromSql("Execute ssp_GetAllStateList").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region CityList
        public class AllCitylist
        {
            public long cityId { get; set; }
            public long stateId { get; set; }
            public string cityName { get; set; }
            public string stateName { get; set; }
            public string description { get; set; }
            public string countryName { get; set; }
            public bool isActive { get; set; }
        }

        public List<AllCitylist> ssp_GetAllCityListDetails()
        {
            try
            {
                return ssp_GetAllCityList.FromSql("Execute ssp_GetAllCityList").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region All Blogs
        public class AllBlogDetails
        {
            public long BlogId { get; set; }
            public string Title { get; set; }
            public string Status { get; set; }
            public bool? IsVisible { get; set; }
            public bool IsActive { get; set; }
            public string CreatedDate { get; set; }
            public long CreatedBy { get; set; }
            public string UpdatedDate { get; set; }
            public string ValidFromDate { get; set; }
            public string ValidTillDate { get; set; }
            public bool Activity { get; set; }

        }

        public class AllBlogList
        {
            public long BlogId { get; set; }
            public string Title { get; set; }
            public string ImageUrl { get; set; }
            public bool? IsVisible { get; set; }
            public bool IsActive { get; set; }
            public string CreatedDate { get; set; }
            public long CreatedBy { get; set; }
            public long? VisitorCount { get; set; }
        }
        public DbQuery<AllBlogDetails> ssp_GetAllBlogDetails { get; set; }

        public DbQuery<AllBlogList> ssp_GetAllBlogListing { get; set; }

        public List<AllBlogDetails> ssp_GetBlogDetails(string LoggedInRole, long LoggedInUserId)
        {
            try
            {
                SqlParameter paramLoggedInRole = new SqlParameter("@loggedInRole", LoggedInRole);
                SqlParameter paramLoggedInUserId = new SqlParameter("@loggedInUserId", LoggedInUserId);
                return ssp_GetAllBlogDetails.FromSql("Execute ssp_GetAllBlogDetails @loggedInRole,@loggedInUserId", paramLoggedInRole, paramLoggedInUserId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public List<AllBlogList> ssp_GetAllBlogList()
        {
            try
            {
                return ssp_GetAllBlogListing.FromSql("Execute GetAllBlogListing").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion



    }
}
