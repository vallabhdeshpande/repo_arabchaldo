﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.DAL.Interfaces;
using System.Linq;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class PaymentPlanRepository :IPaymentPlanDetails
    {
        private Offshore_ArabchaldoContext _Offshore_ArabchaldoContext;

        public PaymentPlanRepository(Offshore_ArabchaldoContext context)
        {
            _Offshore_ArabchaldoContext = context;
        }

        public class DisplayAllPlan
        {
            public long PlanId { get; set; }
            public string PlanName { get; set; }
            public long UserId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public bool IsPaid { get; set; }
            public DateTime? ValidFrom { get; set; }
            public DateTime? ValidTill { get; set; }
            public decimal FinalAmount { get; set; }
        }



        public DataTable GetAllPlan()
        {
            List<DisplayAllPlan> getAllPlans = new List<DisplayAllPlan>();

            //getAllPlans = (from pp in _Offshore_ArabchaldoContext.PaymentPlan
            //               join ud in _Offshore_ArabchaldoContext.UserDetails
            //               on pp.UserId equals ud.UserId
            //               orderby pp.UpdatedDate ascending
            //               select new DisplayAllPlan { PlanId = pp.PlanId, PlanName = pp.PlanName, UserId = ud.UserId, FirstName = ud.FirstName, LastName = ud.LastName, Email = ud.Email, ValidFrom = pp.ValidFrom, ValidTill = pp.ValidTill, FinalAmount = pp.FinalAmount }
            //               ).ToList();

            //getAllPlans = (from pp in _Offshore_ArabchaldoContext.PaymentPlan
            //               join ud in _Offshore_ArabchaldoContext.UserDetails
            //               on pp.UserId equals ud.UserId
            //               where (ud.IsActive == true)
            //               orderby pp.UpdatedDate ascending
            //               select new DisplayAllPlan { PlanId = pp.PlanId, PlanName = pp.PlanName, UserId = ud.UserId, FirstName = ud.FirstName, LastName = ud.LastName, Email = ud.Email, IsPaid = pp.IsPaid, ValidFrom = pp.ValidFrom, ValidTill = pp.ValidTill, FinalAmount = pp.FinalAmount }
            //               ).ToList();

            //getAllPlans = (from pp in _Offshore_ArabchaldoContext.PaymentPlan
            //               join ud in _Offshore_ArabchaldoContext.UserDetails
            //               on pp.UserId equals ud.UserId
            //               where ((ud.IsActive == true ) && (pp.ValidTill >= DateTime.Now))
            //               orderby pp.UpdatedDate ascending
            //               select new DisplayAllPlan { PlanId = pp.PlanId, PlanName = pp.PlanName, UserId = ud.UserId, FirstName = ud.FirstName, LastName = ud.LastName, Email = ud.Email, IsPaid= (bool)pp.IsPaid, ValidFrom = pp.ValidFrom, ValidTill= pp.ValidTill, FinalAmount = pp.FinalAmount }
            //               ).ToList();

            List<Offshore_ArabchaldoContext.PaymentPlanList> paymentPlanListsSSP = new List<Offshore_ArabchaldoContext.PaymentPlanList>();
            paymentPlanListsSSP = _Offshore_ArabchaldoContext.ssp_GetAllPlanListDetails();

            //List<DisplayAllPlan> 

            DataTable allPaymentPlan = new DataTable();

            allPaymentPlan.Columns.Add("PlanId", typeof(long));
            allPaymentPlan.Columns.Add("PlanName", typeof(string));
            allPaymentPlan.Columns.Add("UserId", typeof(long));
            allPaymentPlan.Columns.Add("FirstName", typeof(string));
            allPaymentPlan.Columns.Add("LastName", typeof(string));
            allPaymentPlan.Columns.Add("Email", typeof(string));
            allPaymentPlan.Columns.Add("IsPaid", typeof(bool));
            allPaymentPlan.Columns.Add("ValidFrom", typeof(string));
            allPaymentPlan.Columns.Add("ValidTill", typeof(string));
            allPaymentPlan.Columns.Add("FinalAmount", typeof(decimal));

            //string inputFormat = "dd-MM-yyyy HH:mm:ss";
            //string outputFormat = "dd-MMM-yyyy";
            //DateTime? datetime = DateTime.Now;

            foreach (var plan in paymentPlanListsSSP)
            {
                //plan.ValidFrom = ((DateTime)plan.ValidFrom).Date;
                //plan.ValidTill = ((DateTime)plan.ValidTill).Date;
                allPaymentPlan.Rows.Add(plan.PlanId, plan.PlanName, plan.UserId, plan.FirstName, plan.LastName, plan.Email, plan.IsPaid, plan.ValidFrom, plan.ValidTill, plan.FinalAmount);
            }
            allPaymentPlan.AcceptChanges();

            return allPaymentPlan;            
        }      

        public List<PaymentPlan> GetPlanListForUser(int userId, string location)
        {
            List<PaymentPlan> paymentPlans = new List<PaymentPlan>();
            if (location == "BUDashboard")
            {
                paymentPlans = _Offshore_ArabchaldoContext.PaymentPlan.Where(e => e.UserId == userId && e.ValidTill >= DateTime.Today && e.ValidFrom <= DateTime.Today).ToList();
            }
            else
            {
                paymentPlans = _Offshore_ArabchaldoContext.PaymentPlan.Where(e => e.UserId == userId && e.ValidTill >= DateTime.Today && e.ValidFrom <= DateTime.Today && e.IsPaid == true).ToList();
            }
            return paymentPlans;
        }

        public PaymentPlan getPlanIdDetails(int planId)
        {
            PaymentPlan paymentPlan = new PaymentPlan();
            paymentPlan = _Offshore_ArabchaldoContext.PaymentPlan.Where(e => e.PlanId == planId).SingleOrDefault();
            return paymentPlan;
        }

        public long InsertPlan(PaymentPlan paymentPlan)
        {
            _Offshore_ArabchaldoContext.PaymentPlan.Add(paymentPlan);
            _Offshore_ArabchaldoContext.SaveChanges();
            long planId = paymentPlan.PlanId; 

            return planId;
        }

        public void UpdatePlanDetailsForBanner(long planId, long bannerImageCount)
        {
            PaymentPlan isPlanExist = IsPlanExist(planId);
            if (isPlanExist != null)
            {
                isPlanExist.UpdatedDate = DateTime.Now;
                isPlanExist.BannerImageCount = bannerImageCount;
            }

            _Offshore_ArabchaldoContext.SaveChanges();
        }

        public void UpdatePlanDetailsForEvent(long planId, long eventImageCount)
        {
            PaymentPlan isPlanExist = IsPlanExist(planId);
            if (isPlanExist != null)
            {
                isPlanExist.UpdatedDate = DateTime.Now;
                isPlanExist.EventImageCount = eventImageCount;
            }

            _Offshore_ArabchaldoContext.SaveChanges();
        }

        public void UpdatePlanDetailsForAds(long planId, long logoImageCount, long adsImageCount, long jobsCount)
        {
            PaymentPlan isPlanExist = IsPlanExist(planId);
            if (isPlanExist != null)
            {
                isPlanExist.UpdatedDate = DateTime.Now;
                isPlanExist.AdImageCount = adsImageCount;
                isPlanExist.LogoCount = logoImageCount;
                isPlanExist.JobsAdvCount = jobsCount;
            }

            _Offshore_ArabchaldoContext.SaveChanges();
        }

        public void UpdatePlanDetailsbuySell(long planId, long buysellCount)
        {
            PaymentPlan isPlanExist = IsPlanExist(planId);
            if (isPlanExist != null)
            {
                isPlanExist.UpdatedDate = DateTime.Now;
                isPlanExist.BuySellCount = buysellCount;
            }

            _Offshore_ArabchaldoContext.SaveChanges();
        }
        public void UpdatePlanDetailsSmallAds(long planId, long logoImageCount, long smalladsImageCount)
        {
            PaymentPlan isPlanExist = IsPlanExist(planId);
            if (isPlanExist != null)
            {
                isPlanExist.UpdatedDate = DateTime.Now;
                isPlanExist.SmallAdsCount = smalladsImageCount;
                isPlanExist.LogoCount = logoImageCount;
            }

            _Offshore_ArabchaldoContext.SaveChanges();
        }

        public void UpdatePlanDetailsForBuySell(long planId, long BuySellCount)
        {
            PaymentPlan isPlanExist = IsPlanExist(planId);
            if (isPlanExist != null)
            {
                isPlanExist.UpdatedDate = DateTime.Now;
                isPlanExist.BuySellCount = BuySellCount;
            }

            _Offshore_ArabchaldoContext.SaveChanges();
        }

        public void UpdatePlanDetails(long planId, PaymentPlan paymentPlan)
        {
            //PaymentPlan isPlanExist = new PaymentPlan();
            PaymentPlan isPlanExist = IsPlanExist(planId);
            if(isPlanExist!= null)
            {
                isPlanExist.UpdatedDate = DateTime.Now;
                isPlanExist.AdImageCount = paymentPlan.AdImageCount;
                isPlanExist.BannerImageCount = paymentPlan.BannerImageCount;
                isPlanExist.EventImageCount = paymentPlan.EventImageCount;
                isPlanExist.LogoCount = paymentPlan.LogoCount;
            }

            _Offshore_ArabchaldoContext.SaveChanges();
        }

        public void UpdatePlanAfterPayment(long planId)
        {
            //PaymentPlan isPlanExist = new PaymentPlan();
            PaymentPlan isPlanExist = IsPlanExist(planId);
            if (isPlanExist != null)
            {
                isPlanExist.UpdatedDate = DateTime.Now;
                isPlanExist.IsPaid = true;
            }

            _Offshore_ArabchaldoContext.SaveChanges();
        }

        public PaymentPlan IsPlanExist(long planId)
        {
            PaymentPlan paymentPlan = new PaymentPlan();
            paymentPlan = _Offshore_ArabchaldoContext.PaymentPlan.Where(e => e.PlanId == planId).SingleOrDefault();
            return paymentPlan;
        }

        //bool IPaymentPlanDetails.UpdatePlanAfterPayment(long planId)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
