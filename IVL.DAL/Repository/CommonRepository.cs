﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class CommonRepository:ICommon
    {
        private Offshore_ArabchaldoContext Acdcontext;


        public CommonRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;
        }


        #region Dropdowns

        // Country
        public class CountryDdl
        {
            public long countryId { get; set; }
            public string countryName { get; set; }
        }
        public DataTable FillCountryDDL()
        {
            List<CountryDdl> allcountryDdlList = new List<CountryDdl>();
            allcountryDdlList = Acdcontext.Country.Where(e => e.IsActive == true).Select(s => new CountryDdl()
            { countryId = s.CountryId, countryName = s.CountryName }).ToList();

            DataTable dtcountry = new DataTable();
            dtcountry.Columns.Add("countryId", typeof(long));
            dtcountry.Columns.Add("countryName", typeof(string));
            foreach (var item in allcountryDdlList)
            {
                dtcountry.Rows.Add(item.countryId, item.countryName);
            }
            dtcountry.AcceptChanges();
            return dtcountry;
        }


        // State
        public class StateDdl
        {
            public long stateId { get; set; }
            public string stateName { get; set; }
        }
        public DataTable FillStateDDLByCountryId(long countryId)
        {
            List<StateDdl> allStateDdlList = new List<StateDdl>();
            allStateDdlList = Acdcontext.State.Where(e => e.CountryId == countryId && e.IsActive == true).OrderBy(e => e.StateName).Select(s => new StateDdl()
            { stateId = s.StateId,stateName = s.StateName}).ToList();

            DataTable dtState = new DataTable();
            dtState.Columns.Add("stateId", typeof(long));
            dtState.Columns.Add("stateName", typeof(string));
            foreach (var item in allStateDdlList)
            {
                dtState.Rows.Add(item.stateId, item.stateName);
            }
            dtState.AcceptChanges();
            return dtState;
        }


        // City
        public class CityDdl
        {
            public long cityId { get; set; }
            public string cityName { get; set; }
        }
        public DataTable FillCityDDLByStateId(long stateId)
        {
            List<CityDdl> allcityDdlList = new List<CityDdl>();
            allcityDdlList = Acdcontext.City.Where(e => e.StateId == stateId && e.IsActive == true).OrderBy(e => e.CityName).Select(s => new CityDdl()
            { cityId = s.CityId, cityName = s.CityName }).ToList();

            DataTable dtcity = new DataTable();
            dtcity.Columns.Add("cityId", typeof(long));
            dtcity.Columns.Add("cityName", typeof(string));
            foreach (var item in allcityDdlList)
            {
                dtcity.Rows.Add(item.cityId, item.cityName);
            }
            dtcity.AcceptChanges();
            return dtcity;
        }

        // For BU City DDl with OTHER Option
        public DataTable FillBUCityDDLByStateId(long stateId)
        {
            List<CityDdl> allcityDdlList = new List<CityDdl>();
            allcityDdlList = Acdcontext.City.Where(e => e.StateId == stateId && e.IsActive == true).OrderBy(e => e.CityName).Select(s => new CityDdl()
            { cityId = s.CityId, cityName = s.CityName }).ToList();

            DataTable dtcity = new DataTable();
            dtcity.Columns.Add("cityId", typeof(long));
            dtcity.Columns.Add("cityName", typeof(string));
            foreach (var item in allcityDdlList)
            {
                dtcity.Rows.Add(item.cityId, item.cityName);
            }
            dtcity.Rows.Add(9999, "Other");
            dtcity.AcceptChanges();
            return dtcity;
        }

        // Role
        public class RoleDdl
        {
            public long roleId { get; set; }
            public string roleName { get; set; }
        }
        public DataTable FillRoleDDL(string roleName)
        {
            List<RoleDdl> allroleDdlList = new List<RoleDdl>();
            if(roleName== "SuperAdmin")
            {
                allroleDdlList = Acdcontext.Roles.Where(e => e.IsActive == true && e.RoleName != "SuperAdmin").Select(s => new RoleDdl()
                { roleId = s.RoleId, roleName = s.RoleName }).ToList();
            }
           else if (roleName == "Admin")
            {
                allroleDdlList = Acdcontext.Roles.Where(e => e.IsActive == true && (e.RoleName != "Admin" && e.RoleName != "SuperAdmin")).Select(s => new RoleDdl()
                { roleId = s.RoleId, roleName = s.RoleName }).ToList();
            }

            DataTable dtrole = new DataTable();
            dtrole.Columns.Add("roleId", typeof(long));
            dtrole.Columns.Add("roleName", typeof(string));
            foreach (var item in allroleDdlList)
            {
                dtrole.Rows.Add(item.roleId, item.roleName);
            }
            dtrole.AcceptChanges();
            return dtrole;
        }


        // User
        public class UserDdl
        {
            public long userId { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string email { get; set; }
            public string userName { get; set; }
            public string userFullName { get; set; }
            public long postedFor { get; set; }
        }
        public DataTable FillUserDDL()
        {
            List<UserDdl> alluserDdlList = new List<UserDdl>();
            alluserDdlList = Acdcontext.UserDetails.Where(e =>  e.IsActive == true && e.Email != "sa@sa.com" && e.Email != "admin@admin.com").OrderBy(e => e.FirstName).Select(s => new UserDdl()
            { userId = s.UserId, firstName = s.FirstName,lastName=s.LastName,email=s.Email, userName= s.FirstName+' '+ s.LastName+'<'+ s.Email+'>',userFullName= s.FirstName + ' ' + s.LastName, postedFor=s.UserId }).ToList();

            DataTable dtuser = new DataTable();
            dtuser.Columns.Add("userId", typeof(long));
            dtuser.Columns.Add("firstName", typeof(string));
            dtuser.Columns.Add("lastName", typeof(string));
            dtuser.Columns.Add("email", typeof(string));
            dtuser.Columns.Add("userName", typeof(string));
            dtuser.Columns.Add("userFullName", typeof(string));
            dtuser.Columns.Add("postedFor", typeof(long));
            foreach (var item in alluserDdlList)
            {
                dtuser.Rows.Add(item.userId, item.firstName, item.lastName, item.email,item.userName,item.userFullName,item.postedFor);
            }
            dtuser.AcceptChanges();
            return dtuser;
        }



        // UserByRoleId
        public class UserByRoleIdDdl
        {
            public long roleId { get; set; }
            public long userId { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
        }
        public DataTable FillUserByRoleDDL(long roleId)
        {
            List<UserByRoleIdDdl> alluserByRoleIdDdlList = new List<UserByRoleIdDdl>();
            alluserByRoleIdDdlList = (from u in Acdcontext.UserDetails join ur in Acdcontext.UserRoles
                                      on u.UserId equals ur.UserId where u.IsActive==true && ur.RoleId==roleId orderby u.FirstName
                                      select new UserByRoleIdDdl {roleId=ur.RoleId,userId=ur.UserId,firstName=u.FirstName,lastName=u.LastName }).ToList();

            DataTable dtuserByRoleId = new DataTable();
            dtuserByRoleId.Columns.Add("roleId", typeof(long));
            dtuserByRoleId.Columns.Add("userId", typeof(long));
            dtuserByRoleId.Columns.Add("firstName", typeof(string));
            dtuserByRoleId.Columns.Add("lastName", typeof(string));
            foreach (var item in alluserByRoleIdDdlList)
            {
                dtuserByRoleId.Rows.Add(item.roleId, item.userId, item.firstName, item.lastName);
            }
            dtuserByRoleId.AcceptChanges();
            return dtuserByRoleId;
        }

        // Category
        public class CategoryDdl
        {
            public long categoryId { get; set; }
            public string categoryName { get; set; }

            public string CategoryImageUrl { get; set; }
            public string arabicName { get; set; }
        }
        public DataTable FillCategoryDDL()
        {
            List<CategoryDdl> allcategoryDdlList = new List<CategoryDdl>();
            allcategoryDdlList = Acdcontext.Category.Where(e => e.IsActive == true).OrderBy(e=>e.CategoryName).Select(s => new CategoryDdl()
            { categoryId = s.CategoryId, categoryName = s.CategoryName , CategoryImageUrl = s.CategoryImageUrl, arabicName = s.ArabicName }).ToList();

            DataTable dtcategory = new DataTable();
            dtcategory.Columns.Add("categoryId", typeof(long));
            dtcategory.Columns.Add("categoryName", typeof(string));
            dtcategory.Columns.Add("CategoryImageUrl", typeof(string));
            dtcategory.Columns.Add("arabicName", typeof(string));
            foreach (var item in allcategoryDdlList)
            {
                dtcategory.Rows.Add(item.categoryId, item.categoryName, "Category/" + item.categoryId + "/Image/" +item.CategoryImageUrl, item.arabicName);
            }
            dtcategory.AcceptChanges();
            return dtcategory;
        }
        public DataTable JobCategoryDDL()
        {
            List<CategoryDdl> allcategoryDdlList = new List<CategoryDdl>();
            allcategoryDdlList = Acdcontext.Category.Where(e => e.IsActive == true && e.CategoryName=="Jobs").OrderBy(e => e.CategoryName).Select(s => new CategoryDdl()
            { categoryId = s.CategoryId, categoryName = s.CategoryName, CategoryImageUrl = s.CategoryImageUrl, arabicName = s.ArabicName }).ToList();

            DataTable dtcategory = new DataTable();
            dtcategory.Columns.Add("categoryId", typeof(long));
            dtcategory.Columns.Add("categoryName", typeof(string));
            dtcategory.Columns.Add("CategoryImageUrl", typeof(string));
            dtcategory.Columns.Add("arabicName", typeof(string));
            foreach (var item in allcategoryDdlList)
            {
                dtcategory.Rows.Add(item.categoryId, item.categoryName, item.CategoryImageUrl, item.arabicName);
            }
            dtcategory.AcceptChanges();
            return dtcategory;
        }

        // SubCategory
        public class SubCategoryDdl
        {
            public long CategoryId { get; set; }
            public long subCategoryId { get; set; }
            public string subCategoryName { get; set; }
            public string arabicName { get; set; }
        }
        public DataTable FillSubCategoryDDLByCategoryId(long CategoryId)
        {
            List<SubCategoryDdl> allsubCategoryDdlList = new List<SubCategoryDdl>();
            if(CategoryId==0)
                allsubCategoryDdlList = Acdcontext.SubCategory.Where(e => e.IsActive == true && e.CategoryName == "Jobs").OrderBy(e => e.CategoryName).Select(s => new SubCategoryDdl()
                {CategoryId=s.CategoryId, subCategoryId = s.SubCategoryId, subCategoryName = s.SubCategoryName, arabicName=s.ArabicName }).ToList();
            else if (CategoryId == -1)
                allsubCategoryDdlList = Acdcontext.SubCategory.Where(e => e.IsActive == true).OrderBy(e => e.CategoryName).Select(s => new SubCategoryDdl()
                { CategoryId = s.CategoryId, subCategoryId = s.SubCategoryId, subCategoryName = s.SubCategoryName, arabicName = s.ArabicName }).ToList();
            else
                allsubCategoryDdlList = Acdcontext.SubCategory.Where(e => e.IsActive == true && e.CategoryId==CategoryId).OrderBy(e => e.CategoryName).Select(s => new SubCategoryDdl()
                { CategoryId = s.CategoryId, subCategoryId = s.SubCategoryId, subCategoryName = s.SubCategoryName, arabicName = s.ArabicName }).ToList();
            DataTable dtsubCategory = new DataTable();
            dtsubCategory.Columns.Add("categoryId", typeof(long));
            dtsubCategory.Columns.Add("subCategoryId", typeof(long));
            dtsubCategory.Columns.Add("subCategoryName", typeof(string));
            dtsubCategory.Columns.Add("arabicName", typeof(string));
            foreach (var item in allsubCategoryDdlList)
            {
                dtsubCategory.Rows.Add(item.CategoryId,item.subCategoryId, item.subCategoryName, item.arabicName);
            }
            dtsubCategory.AcceptChanges();
            return dtsubCategory;
        }
        public DataTable FillPopularCategory()
        {
            //List<CategoryDdl> allcategoryDdlList = new List<CategoryDdl>();
            string AdType = "Ads";
            List<Offshore_ArabchaldoContext.CategoryList> allcategoryDdlList = new List<Offshore_ArabchaldoContext.CategoryList>();
            allcategoryDdlList = Acdcontext.ssp_GetAllPopularCategoriesList(AdType);           

            DataTable popularcategorylist = new DataTable();
            popularcategorylist.Columns.Add("categoryId", typeof(long));
            popularcategorylist.Columns.Add("categoryName", typeof(string));
            popularcategorylist.Columns.Add("arabicName", typeof(string));
            foreach (var item in allcategoryDdlList)
            {
                popularcategorylist.Rows.Add(item.categoryId, item.categoryName,item.arabicName);
            }
            popularcategorylist.AcceptChanges();
            return popularcategorylist;
        }

        public DataTable FillPopularEventsCategory()
        {
            string AdType = "Events";
            List<Offshore_ArabchaldoContext.CategoryList> allcategoryDdlList = new List<Offshore_ArabchaldoContext.CategoryList>();
            allcategoryDdlList = Acdcontext.ssp_GetAllPopularCategoriesList(AdType);

            DataTable popularcategorylist = new DataTable();
            popularcategorylist.Columns.Add("categoryId", typeof(long));
            popularcategorylist.Columns.Add("categoryName", typeof(string));
            popularcategorylist.Columns.Add("arabicName", typeof(string));
            foreach (var item in allcategoryDdlList)
            {
                popularcategorylist.Rows.Add(item.categoryId, item.categoryName, item.arabicName);
            }
            popularcategorylist.AcceptChanges();
            return popularcategorylist;
        }

        public DataTable FillPopularJobSubCategory()
        {
            string AdType = "Jobs";
            List<Offshore_ArabchaldoContext.SubCategoryList> allcategoryDdlList = new List<Offshore_ArabchaldoContext.SubCategoryList>();
            allcategoryDdlList = Acdcontext.ssp_GetPopularSubCategoriesList(AdType);

            DataTable popularjobsubcategorylist = new DataTable();
            popularjobsubcategorylist.Columns.Add("subcategoryId", typeof(long));
            popularjobsubcategorylist.Columns.Add("subcategoryName", typeof(string));
            popularjobsubcategorylist.Columns.Add("arabicName", typeof(string));
            foreach (var item in allcategoryDdlList)
            {
                popularjobsubcategorylist.Rows.Add(item.subcategoryId, item.subcategoryName, item.arabicName);
            }
            popularjobsubcategorylist.AcceptChanges();
            return popularjobsubcategorylist;
        }


        #endregion


        #region Grid

        // City
        public class CityList
        {
            public long cityId { get; set; }
            public long StateId { get; set; }
            public string cityName { get; set; }
            public string stateName { get; set; }
            public string countryName { get; set; }
            public string description { get; set; }
        }
        public DataTable FillCityList()
        {
            List<CityList> cityList = new List<CityList>();
            cityList = (from c in Acdcontext.City
                        join s in Acdcontext.State
                        on c.StateId equals s.StateId
                        join cnt in Acdcontext.Country
                        on s.CountryId equals cnt.CountryId
                        where c.IsActive == true
                        orderby c.CityName
                        select new CityList { cityId = c.CityId,StateId=s.StateId,cityName=c.CityName,stateName=s.StateName,countryName=cnt.CountryName,description=c.Description}).ToList();

            DataTable dtcityList = new DataTable();
            dtcityList.Columns.Add("cityId", typeof(long));
            dtcityList.Columns.Add("StateId", typeof(long));
            dtcityList.Columns.Add("cityName", typeof(string));
            dtcityList.Columns.Add("stateName", typeof(string));
            dtcityList.Columns.Add("countryName", typeof(string));
            dtcityList.Columns.Add("description", typeof(string));
            foreach (var item in cityList)
            {
                dtcityList.Rows.Add(item.cityId, item.StateId,item.cityName,item.stateName,item.countryName,item.description);
            }
            dtcityList.AcceptChanges();
            return dtcityList;
        }


        #endregion


        #region Display Workflow
        public class WorkflowDisplay
        {
            public long id { get; set; }    //postId(AdId,BannerId,)
            public long postTypeId { get; set; }
            public string createdBy { get; set; }
            public DateTime? createdDate { get; set; }
            public string status { get; set; }
            public string remark { get; set; }
        }

        public DataTable DisplayWorkFlow(long Id, long PostTypeId)
        {
            List<WorkflowDisplay> displayWorkFlowDetails = new List<WorkflowDisplay>();
            displayWorkFlowDetails =(from psd in Acdcontext.PostStatusDetails
                                     join pd in Acdcontext.PostDetails on psd.PostDetailsId equals pd.PostDetailsId
                                     join pt in Acdcontext.PostTypes on pd.PostTypeId equals pt.PostTypeId
                                    join ud in Acdcontext.UserDetails on psd.CreatedBy equals ud.UserId 
                                     join ws in Acdcontext.WorkflowStatus on psd.WorkflowStatusId equals ws.WorkflowStatusId
                                     where pd.IsActive==true && pd.PostId==Id && pd.PostTypeId==PostTypeId
                                     orderby  psd.PostStatusDetailsId descending
                                     select new WorkflowDisplay  {id=pd.PostId,postTypeId=pd.PostTypeId,createdBy=ud.FirstName+" "+ud.LastName,createdDate=psd.CreatedDate,status=ws.Status,remark=psd.Remark }).ToList();

            DataTable dtdisplayWorkFlowDetails = new DataTable();
            dtdisplayWorkFlowDetails.Columns.Add("id", typeof(long));
            dtdisplayWorkFlowDetails.Columns.Add("postTypeId", typeof(long));
            dtdisplayWorkFlowDetails.Columns.Add("createdBy", typeof(string));
            dtdisplayWorkFlowDetails.Columns.Add("createdDate", typeof(DateTime));
            dtdisplayWorkFlowDetails.Columns.Add("status", typeof(string));
            dtdisplayWorkFlowDetails.Columns.Add("remark", typeof(string));
            foreach (var item in displayWorkFlowDetails)
            {
                dtdisplayWorkFlowDetails.Rows.Add(item.id,item.postTypeId,item.createdBy,item.createdDate,item.status,item.remark);
            }
            dtdisplayWorkFlowDetails.AcceptChanges();
            return dtdisplayWorkFlowDetails;
        }

        #endregion


        #region ManageWorkFlow




        #endregion
    }
}
