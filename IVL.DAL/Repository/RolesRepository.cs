﻿using IVL.ArabChaldo.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore.Internal;
using System.Linq;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class RolesRepository : IRoles
    {
        private Offshore_ArabchaldoContext Acdcontext;
        public RolesRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }
        public List<Roles> GetAllUserRoles()
        {
            List<Roles> allUserRoleList = new List<Roles>();
            return Acdcontext.Roles.Where(e => e.IsActive == true).ToList();
        }        
    }
}
