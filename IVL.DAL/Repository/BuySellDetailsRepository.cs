﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class BuySellDetailsRepository : IBuySellDetails
    {
        private Offshore_ArabchaldoContext Acdcontext;
        public BuySellDetailsRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }

        public bool CheckbuySellDetailsExits(string Title, long CategoryId, long BuysellId)
        {
            BuySellDetails buysellDetailsDetails = Acdcontext.BuySellDetails.AsNoTracking().FirstOrDefault(e => e.Title == Title && e.CategoryId == CategoryId && e.BuySellId != BuysellId && e.IsActive == true);
            //Acdcontext.AdDetails.ExecuteSqlCommand("updateAdDetails");
            // var cmd = Acdcontext.CreateCommand();
            if (buysellDetailsDetails != null)
            {
                return true;
            }

            return false;
        }
        public DataTable GetAllBuySellDetails(long loggedInUserId, string loggedInRole)
        {
            BuySellDetails buySellDetails = new BuySellDetails();
            List<BuySellDetails> allbuySellDetailsList = new List<BuySellDetails>();
            


            List<Offshore_ArabchaldoContext.AllBuySelllist> allbuySell = new List<Offshore_ArabchaldoContext.AllBuySelllist>();
            allbuySell = Acdcontext.ssp_GetBuySellList(loggedInRole, loggedInUserId);
            DataTable allbuySelldata = new DataTable();
            allbuySelldata.Columns.Add("BuySellId", typeof(long));
            allbuySelldata.Columns.Add("ImageUrl", typeof(string));
            allbuySelldata.Columns.Add("title", typeof(string));
            allbuySelldata.Columns.Add("subCategoryName", typeof(string));
            allbuySelldata.Columns.Add("cityName", typeof(string));
            allbuySelldata.Columns.Add("status", typeof(string));
            allbuySelldata.Columns.Add("isPremium", typeof(string));
            allbuySelldata.Columns.Add("isVisible", typeof(bool));
            allbuySelldata.Columns.Add("isPaidAd", typeof(bool));
            allbuySelldata.Columns.Add("isActive", typeof(bool));
            allbuySelldata.Columns.Add("createdDate", typeof(string));
            allbuySelldata.Columns.Add("createdBy", typeof(long));
            allbuySelldata.Columns.Add("updatedDate", typeof(string));
            allbuySelldata.Columns.Add("validFromDate", typeof(string));
            allbuySelldata.Columns.Add("validTillDate", typeof(string));
            allbuySelldata.Columns.Add("activity", typeof(bool));

            foreach (var item in allbuySell)
            {
                try
                {
                    if (item.IsVisible != null)
                        item.Activity = true;
                    else
                        item.Activity = false;

                    allbuySelldata.Rows.Add(item.BuySellId, item.ImageUrl, item.Title, item.SubCategoryName, item.CityName, item.Status, item.IsPremium, item.IsVisible, item.IsPaidAd, item.IsActive, item.CreatedDate, item.CreatedBy, item.UpdatedDate, item.ValidFromDate, item.ValidTillDate, item.Activity);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            allbuySelldata.AcceptChanges();


            return allbuySelldata;
        }
        public BuySellDetails GetbuySellDetailsById(long buySellId)
        {
            return Acdcontext.BuySellDetails.AsNoTracking().FirstOrDefault(e => e.BuySellId == buySellId && e.IsActive == true);
        }

        public List<BuySellDetails> GetAllBuySellsDetails(long loggedInUserId, string loggedInRole)
        {
            BuySellDetails buySellsDetails = new BuySellDetails();
            List<BuySellDetails> buySellsDetailsList = new List<BuySellDetails>();
            try
            {
                Acdcontext.Database.ExecuteSqlCommand("update BuySellDetails  Set CategoryName=ct.CategoryName, PostedForName = u.FirstName + ' ' + u.LastName FROM SmallAdDetails ad INNER JOIN  Category ct ON ad.CategoryId = ct.CategoryId INNER JOIN UserDetails u on ad.postedfor = u.UserId");
                Acdcontext.Database.ExecuteSqlCommand("exec [dbo].[updateSmallAdDetails]");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //  allSmallAdDetailsList = Acdcontext.ssp_GetAllAds(loggedInRole,"", loggedInUserId);


            if (loggedInRole == "SuperAdmin" || loggedInRole == "Admin")  //&& e.ValidTillDate >= DateTime.Today
            {
                //allSmallAdDetailsList = Acdcontext.SmallAdDetails.Where(e => e.IsActive == true && e.CategoryName != "Jobs").OrderBy(n => n.UpdatedDate).ToList();
                buySellsDetailsList = (Acdcontext.BuySellDetails.AsNoTracking().Where(e => e.IsActive == true && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
                    (Acdcontext.BuySellDetails.AsNoTracking().Where(e => e.IsActive == true && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).Concat
                    (Acdcontext.BuySellDetails.AsNoTracking().Where(e => e.IsActive == true && e.IsVisible == false).ToList()).OrderBy(n => n.UpdatedDate).ToList());
            }

            else
            {
                buySellsDetailsList = (Acdcontext.BuySellDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
                    (Acdcontext.BuySellDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).Concat
                    (Acdcontext.BuySellDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.IsVisible == false).ToList()).OrderBy(n => n.UpdatedDate).ToList());
            }


            return buySellsDetailsList;





        }



        public bool InsertBuySellDetails(BuySellDetails buysellDetails)
        {
            if (buysellDetails.CityId == 9999)
                buysellDetails.CityName = "Other";
            if (buysellDetails.CityId == 0 || buysellDetails.CityId == null)
                buysellDetails.CityName = "";
            if (buysellDetails.StateId == 0 || buysellDetails.StateId == null)
                buysellDetails.StateName = "";
            if (buysellDetails.SubCategoryId == 0 || buysellDetails.SubCategoryId == null)
                buysellDetails.SubCategoryName = "";
            if (buysellDetails.ZipCode == null || buysellDetails.ZipCode == null)
                buysellDetails.ZipCode = "";
            if(buysellDetails.ImageUrl=="")
                buysellDetails.ImageUrl = null;
            if (buysellDetails.AddressStreet1 == null)
                buysellDetails.AddressStreet1 = "";
            if (buysellDetails.AddressStreet2 == null)
                buysellDetails.AddressStreet2 = "";
            if (buysellDetails.IsPaidAd == true)
                buysellDetails.IsPremium = true;
            buysellDetails.IsActive = true;
            buysellDetails.VisitorCount = 0;
            buysellDetails.UpdatedDate = DateTime.Now;
            buysellDetails.CreatedDate = DateTime.Now;
            Acdcontext.BuySellDetails.Add(buysellDetails);
            try
            {
                Acdcontext.SaveChanges();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            long BuysellId = buysellDetails.BuySellId;
            Acdcontext.Entry(buysellDetails).State = EntityState.Detached;
            //Save image in directory
            if (buysellDetails.ImageUrl != null & buysellDetails.IsPaidAd==false)
                ManageUploads(BuysellId.ToString(), buysellDetails.ImageUrl, "Free");

            // Add images
            if (buysellDetails.ImageUrl != null & buysellDetails.IsPaidAd == true)
            {
                var Images = new List<string>(buysellDetails.ImageUrl.Split(',')).ToList();
                foreach (var item in Images)
                {
                    BuySellImageDetails adimages = new BuySellImageDetails();
                    adimages.BuySellId = buysellDetails.BuySellId;
                    adimages.BuySellImageUrl = item;
                    adimages.IsActive = true;
                    adimages.CreatedBy = buysellDetails.CreatedBy;
                    adimages.CreatedDate = DateTime.Now;
                    Acdcontext.BuySellImageDetails.Add(adimages);
                    Acdcontext.SaveChanges();
                    if (item != null)
                        ManageUploads(buysellDetails.BuySellId.ToString(), item, "Premium");
                }
            }
            // workflow management for Created Status
            ManageWorkflowDetails(BuysellId, 1, "", buysellDetails.CreatedBy);

            UpdateForeignKeyConstraints(buysellDetails.BuySellId, buysellDetails.SubCategoryId);

            return true;
        }
        // Delete, Approve and Reject
        public bool buySellAdStatus(long buySellId, string Remark, string Action, long CreatedBy)
        {
            BuySellDetails buySellDetails = Acdcontext.BuySellDetails.FirstOrDefault(e => e.BuySellId == buySellId && e.IsActive == true);
            int StatusId = 0;
            bool isActive = true;
            bool isVisible = true;
            string remark = "";
            if (buySellDetails != null && Action == "Delete")
            {
                if (buySellDetails.IsVisible == true)
                    return false;
                else
                {
                    isActive = false;
                    isVisible = false;
                    StatusId = 6;
                }

                //remark = Remark.Split(',')[2];
            }
            else if (buySellDetails != null && Action == "Approve")
            {
                isActive = true;
                isVisible = true;
                StatusId = 3;
                remark = Remark.Split(',')[2];
                buySellDetails.ValidFromDate = Convert.ToDateTime(Remark.Split(',')[0]);
                buySellDetails.ValidTillDate = Convert.ToDateTime(Remark.Split(',')[1]);
                buySellDetails.IsPremium = Convert.ToBoolean(Remark.Split(',')[3]);
            }
            else if (buySellDetails != null && Action == "Reject")
            {
                isActive = true;
                isVisible = false;
                StatusId = 4;
                remark = Remark.Split(',')[2];
            }

            buySellDetails.IsActive = isActive;
            buySellDetails.IsVisible = isVisible;
            buySellDetails.UpdatedDate = DateTime.Now;
            buySellDetails.UpdatedBy = CreatedBy;
            Acdcontext.BuySellDetails.Update(buySellDetails);
            Acdcontext.SaveChanges();
            ManageWorkflowDetails(buySellId, StatusId, remark, CreatedBy);
            Acdcontext.SaveChanges();
            return true;


        }

        public bool UpdateBuySellDetails(long buySellId, BuySellDetails updateBuysellDetails)
        {

            Acdcontext.Entry(updateBuysellDetails).State = EntityState.Detached;

            BuySellDetails buySellDetailsDetails = Acdcontext.BuySellDetails.FirstOrDefault(e => e.BuySellId == buySellId && e.IsActive == true);

            if (buySellDetailsDetails != null)
            {
                buySellDetailsDetails.Title = updateBuysellDetails.Title;
                buySellDetailsDetails.Description = updateBuysellDetails.Description;
                //buySellDetailsDetails.PostedForName = updateBuysellDetails.PostedForName;
               // buySellDetailsDetails.PostedFor = updateBuysellDetails.PostedFor;
                buySellDetailsDetails.CategoryName = updateBuysellDetails.CategoryName;
                buySellDetailsDetails.CategoryId = updateBuysellDetails.CategoryId;
                buySellDetailsDetails.SubCategoryName = updateBuysellDetails.SubCategoryName;
                buySellDetailsDetails.SubCategoryId = updateBuysellDetails.SubCategoryId;
                buySellDetailsDetails.ImageUrl = updateBuysellDetails.ImageUrl;
                buySellDetailsDetails.ContactPersonName = updateBuysellDetails.ContactPersonName;
                buySellDetailsDetails.ContactNumber = updateBuysellDetails.ContactNumber;
                buySellDetailsDetails.AlternateContactNumber = updateBuysellDetails.AlternateContactNumber;
                buySellDetailsDetails.FaxNumber = updateBuysellDetails.FaxNumber;
                buySellDetailsDetails.Email = updateBuysellDetails.Email;
                buySellDetailsDetails.ExpectedPrice = updateBuysellDetails.ExpectedPrice;
                if (updateBuysellDetails.AddressStreet1 == null)
                    buySellDetailsDetails.AddressStreet1 = "";
                else
                    buySellDetailsDetails.AddressStreet1 = updateBuysellDetails.AddressStreet1;
                if (updateBuysellDetails.AddressStreet2 == null)
                    buySellDetailsDetails.AddressStreet2 = "";
                else
                    buySellDetailsDetails.AddressStreet2 = updateBuysellDetails.AddressStreet2;
                buySellDetailsDetails.CountryId = updateBuysellDetails.CountryId;
                buySellDetailsDetails.StateId = updateBuysellDetails.StateId;
                buySellDetailsDetails.CityId = updateBuysellDetails.CityId;
                buySellDetailsDetails.StateName = updateBuysellDetails.StateName;

                if (updateBuysellDetails.CityId == 9999)
                    buySellDetailsDetails.CityName = "Other";
                else
                    buySellDetailsDetails.CityName = updateBuysellDetails.CityName;
                buySellDetailsDetails.OtherCity = updateBuysellDetails.OtherCity;
                buySellDetailsDetails.AssignedTo = updateBuysellDetails.AssignedTo;
                if (updateBuysellDetails.ZipCode == null)
                    buySellDetailsDetails.ZipCode = "";
                else
                    buySellDetailsDetails.ZipCode = updateBuysellDetails.ZipCode;
                buySellDetailsDetails.ValidFromDate = null;
                buySellDetailsDetails.ValidTillDate = null;
                buySellDetailsDetails.CountryCodeContact = updateBuysellDetails.CountryCodeContact;
                buySellDetailsDetails.UpdatedDate = DateTime.Now;
                buySellDetailsDetails.UpdatedBy = updateBuysellDetails.CreatedBy;
                buySellDetailsDetails.IsVisible = null;

                if (updateBuysellDetails.CityId == 0 || updateBuysellDetails.CityId == null)
                    buySellDetailsDetails.CityName = "";
                if (updateBuysellDetails.StateId == 0 || updateBuysellDetails.StateId == null)
                    buySellDetailsDetails.StateName = "";
                if (updateBuysellDetails.SubCategoryId == 0 || updateBuysellDetails.SubCategoryId == null)
                    buySellDetailsDetails.SubCategoryName = "";
                if (buySellDetailsDetails.IsPaidAd == true)
                    buySellDetailsDetails.IsPremium = true;
                buySellDetailsDetails.StateName = updateBuysellDetails.StateName;
                buySellDetailsDetails.CityName = updateBuysellDetails.CityName;
                try
                {
                    Acdcontext.BuySellDetails.Update(buySellDetailsDetails);
                }
                catch(Exception ex)
                {
                    throw ex;
                }
                Acdcontext.SaveChanges();
                // Update images

                if (buySellDetailsDetails.ImageUrl != null && buySellDetailsDetails.IsPaidAd !=true)
                    ManageUploads(buySellId.ToString(), buySellDetailsDetails.ImageUrl, "Free");
                // Delete all images for a AdId
                if (buySellDetailsDetails.ImageUrl != null && buySellDetailsDetails.IsPaidAd == true)
                {
                    var Images = new List<string>(buySellDetailsDetails.ImageUrl.Split(',')).ToList();
                    //  Insert updated Images
                    foreach (var item in Images)
                    {

                        BuySellImageDetails chkimgexist = Acdcontext.BuySellImageDetails.FirstOrDefault(e => e.BuySellImageUrl == item && e.BuySellId == buySellDetailsDetails.BuySellId && e.IsActive == true);
                        if (chkimgexist == null)
                        {
                            BuySellImageDetails adimages = new BuySellImageDetails();
                            adimages.BuySellId = buySellDetailsDetails.BuySellId;
                            adimages.BuySellImageUrl = item;
                            adimages.IsActive = true;
                            adimages.CreatedBy = buySellDetailsDetails.CreatedBy;
                            adimages.CreatedDate = DateTime.Now;
                            Acdcontext.BuySellImageDetails.Add(adimages);
                            Acdcontext.SaveChanges();
                            if (item != null)
                                ManageUploads(buySellDetailsDetails.BuySellId.ToString(), item, "Premium");
                        }
                    }


                }
                // Checking the current state is Rejected
                long PostDetailsId = GetPostDetailsId(buySellId, 4, 1, buySellDetailsDetails.CreatedBy); // 1 == posttype Ads
                PostStatusDetails CheckRejected = Acdcontext.PostStatusDetails.FirstOrDefault(e => e.PostDetailsId == PostDetailsId && e.IsActive == true && e.WorkflowStatusId == 4);
                if (CheckRejected != null)
                {
                    ManageWorkflowDetails(buySellId, 5, "", updateBuysellDetails.CreatedBy); // Rectified
                }
                else
                    ManageWorkflowDetails(buySellId, 2, "", updateBuysellDetails.CreatedBy); //Modified
                Acdcontext.SaveChanges();
                UpdateForeignKeyConstraints(buySellDetailsDetails.BuySellId, buySellDetailsDetails.SubCategoryId);

                return true;
            }

            return false;
        }
        //Create Directory If not exist
        public void ManageUploads(string path, string FileUrl, string UploadType)
        {
            string AdDirectory = "StaticFiles\\BuySell\\" + path + "\\" + UploadType;
            if (!Directory.Exists(AdDirectory))
            {
                Directory.CreateDirectory(AdDirectory);
            }
            var SourcefolderName = Path.Combine("StaticFiles", "Images");
            var Sourcepath = Path.Combine(Directory.GetCurrentDirectory(), SourcefolderName);
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), AdDirectory);
            var SourceFile = Sourcepath + "\\" + FileUrl;
            var DestinationFile = pathToSave + "\\" + FileUrl;
            if (!File.Exists(pathToSave + FileUrl) && File.Exists(SourceFile))
            {
                File.Move(SourceFile, DestinationFile);
            }
        }
        // Insert Workflow Details
        public void ManageWorkflowDetails(long buysellId, int status, string Remark, long CreatedBy)
        {
            //AdId, StatusId, remark, CreatedBy

            // CommonRepository cr = new CommonRepository();

            long PostDetailsId = GetPostDetailsId(buysellId, status, 5, CreatedBy); // 1 == posttype Ads
            if (PostDetailsId > 0)
            {


                PostStatusDetails postStatusDetails = new PostStatusDetails();
                postStatusDetails.PostDetailsId = PostDetailsId;
                postStatusDetails.IsActive = true;
                postStatusDetails.CreatedBy = CreatedBy;
                postStatusDetails.CreatedDate = DateTime.Now;
                postStatusDetails.Remark = Remark;
                if (status == 1)
                {
                    postStatusDetails.WorkflowStatusId = status;
                }

                else
                {
                    Acdcontext.PostStatusDetails.Where(x => x.PostDetailsId == postStatusDetails.PostDetailsId && x.IsActive == true).ToList().ForEach(x =>
                    {
                        x.IsActive = false; x.UpdatedDate = DateTime.Now;
                    });
                    postStatusDetails.WorkflowStatusId = status;
                }

                Acdcontext.PostStatusDetails.Add(postStatusDetails);
                Acdcontext.SaveChanges();
            }
            //InsertPostStatusDetails(postStatusDetails);
        }

        public void UpdateForeignKeyConstraints(long buySellId, long? subCategoryId)
        {
            // Execute SQL
            if (subCategoryId == 0)
            {
                Acdcontext.Database.ExecuteSqlCommand("update BuySellDetails  Set SubCategoryName='',CategoryName=ct.CategoryName,CityName = c.CityName, StateName = s.StateName, PostedForName = u.FirstName + ' ' + u.LastName FROM BuySellDetails ad INNER JOIN  Category ct ON ad.CategoryId = ct.CategoryId  INNER JOIN  city c ON ad.CityId = c.CityId INNER JOIN state s on ad.StateId = s.StateId INNER JOIN UserDetails u on ad.postedfor = u.UserId");
            }
            else
                
                Acdcontext.Database.ExecuteSqlCommand("update BuySellDetails  Set CategoryName=ct.CategoryName,CityName = c.CityName, StateName = s.StateName, PostedForName = u.FirstName + ' ' + u.LastName FROM BuySellDetails ad INNER JOIN  Category ct ON ad.CategoryId = ct.CategoryId  INNER JOIN  city c ON ad.CityId = c.CityId INNER JOIN state s on ad.StateId = s.StateId INNER JOIN UserDetails u on ad.postedfor = u.UserId ");
        }

        public long GetPostDetailsId(long buysellId, int status, int PostTypeId, long CreatedBy)
        {
            long PostDetailsId = 0;
            PostDetails postDetails = new PostDetails();
            postDetails = Acdcontext.PostDetails.FirstOrDefault(e => e.PostId == buysellId && e.PostTypeId == PostTypeId && e.IsActive == true);
            if (postDetails == null)
            {
                PostDetails addpostDetails = new PostDetails();
                addpostDetails.PostId = buysellId;
                addpostDetails.PostTypeId = PostTypeId;
                addpostDetails.IsActive = true;
                addpostDetails.CreatedBy = CreatedBy;
                addpostDetails.CreatedDate = DateTime.Now;
                Acdcontext.PostDetails.Add(addpostDetails);
                Acdcontext.SaveChanges();
                PostDetailsId = addpostDetails.PostDetailsId;
            }
            else
                PostDetailsId = postDetails.PostDetailsId;
            return PostDetailsId;
        }

        public bool InsertAdStatusDetails(PostStatusDetails postStatusDetails)
        {

            return true;
        }

    }
}
