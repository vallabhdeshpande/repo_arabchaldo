﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.DAL.Interfaces;
using System.Linq;


namespace IVL.ArabChaldo.DAL.Repository
{
    public class TransactionDetailsRepository : ITransactionDetails
    {
        private Offshore_ArabchaldoContext _Offshore_ArabchaldoContext;

        public TransactionDetailsRepository(Offshore_ArabchaldoContext context)
        {
            _Offshore_ArabchaldoContext = context;
        }
        public bool InsertTransaction(TransactionDetails transaction)
        {
            _Offshore_ArabchaldoContext.TransactionDetails.Add(transaction);
            _Offshore_ArabchaldoContext.SaveChanges();
            return true;
        }
    }
}
