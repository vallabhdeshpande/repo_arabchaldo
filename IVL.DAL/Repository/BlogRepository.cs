﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using System.Linq;
using System.IO;
using System.Data;

namespace IVL.ArabChaldo.DAL.Repository

{
    public class BlogRepository : IBlogDetails
{
    private Offshore_ArabchaldoContext _Offshore_ArabchaldoContext;

   
    public BlogRepository(Offshore_ArabchaldoContext context)
    {
        _Offshore_ArabchaldoContext = context;
    }


        public bool IsBlogNewIdExist(long id)
        {
            BlogDetails BlogDetails = _Offshore_ArabchaldoContext.BlogDetails.FirstOrDefault(e => e.BlogId == id);
            if (BlogDetails == null)
            {
                return true;
            }

            return false;
        }

        public bool Delete(long id)
    {
        BlogDetails blogAvailable = IsBlogIdExist(id);
        if (blogAvailable != null)
        {
            blogAvailable.IsActive = false;
            return true;
        }

        return false;
    }

    public DataTable GetAllBlog(long loggedInUserId, string loggedInRole)
    {
            //List<BlogDetails> blogDetails = new List<BlogDetails>();
            //    // blogDetails = _Offshore_ArabchaldoContext.BlogDetails.ToList().FindAll(e=>e.IsActive == true);

            //    // added for order by descending
            //    //if (loggedInRole == "SuperAdmin" || loggedInRole == "Admin")
            //    //    blogDetails = _Offshore_ArabchaldoContext.BlogDetails.Where(e => e.IsActive == true ).OrderBy(d => d.UpdatedDate).ToList();
            //    //else
            //    //    blogDetails = _Offshore_ArabchaldoContext.BlogDetails.Where(e => e.IsActive == true && e.CreatedBy == loggedInUserId ).OrderBy(d => d.UpdatedDate).ToList();

            //    if (loggedInRole == "SuperAdmin" || loggedInRole == "Admin")  //&& e.ValidTillDate >= DateTime.Today
            //    {

            //        blogDetails = (_Offshore_ArabchaldoContext.BlogDetails.Where(e => e.IsActive == true  && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
            //            (_Offshore_ArabchaldoContext.BlogDetails.Where(e => e.IsActive == true  && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).Concat
            //            (_Offshore_ArabchaldoContext.BlogDetails.Where(e => e.IsActive == true && e.IsVisible == false).ToList()).OrderBy(n => n.UpdatedDate).ToList());
            //    }

            //    else
            //    {

            //        blogDetails = (_Offshore_ArabchaldoContext.BlogDetails.Where(e => e.IsActive == true && e.CreatedBy == loggedInUserId  && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
            //             (_Offshore_ArabchaldoContext.BlogDetails.Where(e => e.IsActive == true && e.CreatedBy == loggedInUserId  && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).Concat
            //             (_Offshore_ArabchaldoContext.BlogDetails.Where(e => e.IsActive == true && e.CreatedBy == loggedInUserId && e.IsVisible == false).ToList()).OrderBy(n => n.UpdatedDate).ToList());
            //    }

            BlogDetails blogDetails = new BlogDetails();
            List<BlogDetails> allBlogDetailsList = new List<BlogDetails>();



            List<Offshore_ArabchaldoContext.AllBlogDetails> allBlogs = new List<Offshore_ArabchaldoContext.AllBlogDetails>();
            allBlogs = _Offshore_ArabchaldoContext.ssp_GetBlogDetails(loggedInRole, loggedInUserId);
            DataTable allBlogsdata = new DataTable();
            allBlogsdata.Columns.Add("blogId", typeof(long));
            allBlogsdata.Columns.Add("title", typeof(string));
            allBlogsdata.Columns.Add("status", typeof(string));
            allBlogsdata.Columns.Add("isVisible", typeof(bool));
            allBlogsdata.Columns.Add("isActive", typeof(bool));
            allBlogsdata.Columns.Add("createdDate", typeof(string));
            allBlogsdata.Columns.Add("createdBy", typeof(long));
            allBlogsdata.Columns.Add("updatedDate", typeof(string));
            allBlogsdata.Columns.Add("validFromDate", typeof(string));
            allBlogsdata.Columns.Add("validTillDate", typeof(string));
            allBlogsdata.Columns.Add("activity", typeof(bool));

            foreach (var item in allBlogs)
            {
                try
                {
                    if (item.IsVisible != null)
                        item.Activity = true;
                    else
                        item.Activity = false;

                    allBlogsdata.Rows.Add(item.BlogId, item.Title, item.Status, item.IsVisible, item.IsActive, item.CreatedDate, item.CreatedBy, item.UpdatedDate, item.ValidFromDate, item.ValidTillDate, item.Activity);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            allBlogsdata.AcceptChanges();


            return allBlogsdata;

    }

    public BlogDetails GetBlogById(long id)
    {
        BlogDetails blogIdExist = _Offshore_ArabchaldoContext.BlogDetails.FirstOrDefault(e => e.BlogId == id && e.IsActive == true);
        if (blogIdExist != null)
        {
            return blogIdExist;
        }
        return null;
    }

    public bool Insert(BlogDetails blogDetails)
    {
            bool isExist = IsBlogNewIdExist(blogDetails.BlogId);
            if (isExist)
            {
                blogDetails.VisitorCount = 0;
                blogDetails.UpdatedDate = DateTime.Now;
                blogDetails.UpdatedBy = blogDetails.CreatedBy;
                _Offshore_ArabchaldoContext.BlogDetails.Add(blogDetails);
            _Offshore_ArabchaldoContext.SaveChanges();
            long BlogId = blogDetails.BlogId;
            //Save image in directory
            if (blogDetails.ImageUrl != null)
                ManageUploads(BlogId.ToString(), blogDetails.ImageUrl, "Image");
            // Workflow for Created state
            ManageWorkflowDetails(blogDetails.BlogId, 1, "", blogDetails.CreatedBy);
                return true;
            }
            return true;
    }

    public bool Update(long blogId, BlogDetails blogDetails)
    {
        BlogDetails isExist = IsBlogIdExist(blogId);
            if (isExist != null)
            {
                isExist.BlogId = blogDetails.BlogId;
            isExist.Title = blogDetails.Title;
            isExist.Description = blogDetails.Description;
            long BlogId = blogDetails.BlogId;
            if (blogDetails.ImageUrl != null && (isExist.ImageUrl != blogDetails.ImageUrl))
            {

                ManageUploads(BlogId.ToString(), blogDetails.ImageUrl, "Image");

            }
            isExist.ImageUrl = blogDetails.ImageUrl;
            isExist.IsActive = blogDetails.IsActive;
            isExist.IsVisible = null;
            isExist.UpdatedBy = blogDetails.CreatedBy;
            isExist.UpdatedDate = DateTime.Now;
                isExist.ValidFromDate = null;
                isExist.ValidTillDate = null;
                _Offshore_ArabchaldoContext.SaveChanges();
                // Checking the current state is Rejected
                long PostDetailsId = GetPostDetailsId(blogId, 4, 4, blogDetails.CreatedBy); // 4 == Blog
                PostStatusDetails CheckRejected = _Offshore_ArabchaldoContext.PostStatusDetails.FirstOrDefault(e => e.PostDetailsId == PostDetailsId && e.IsActive == true && e.WorkflowStatusId == 4);
                if (CheckRejected != null)
                {
                    ManageWorkflowDetails(blogId, 5, "", blogDetails.CreatedBy); // Rectified
                }
                else
                    ManageWorkflowDetails(blogId, 2, "", blogDetails.CreatedBy); //Modified
                _Offshore_ArabchaldoContext.SaveChanges();

               return true;
            //Save image in directory

        }
        return false;
    }

    private BlogDetails IsBlogIdExist(long id)
    {
        BlogDetails blogIdExist = _Offshore_ArabchaldoContext.BlogDetails.FirstOrDefault(e => e.BlogId == id && e.IsActive == true);
        if (blogIdExist != null)
        {
            return blogIdExist;
        }
        return null;
    }

    // Delete, Approve and Reject
    public bool BlogStatus(long id, string Remark, string Action, long CreatedBy)
    {
        BlogDetails blogDetails = _Offshore_ArabchaldoContext.BlogDetails.FirstOrDefault(e => e.BlogId == id);
        int StatusId = 0;
        bool isActive = true;
        bool isVisible = true;
        string remark = "";
        if (blogDetails != null && Action == "Delete" && blogDetails.IsVisible == true)
        {
            return false;
        }

        else if (blogDetails != null && Action == "Delete" && blogDetails.IsVisible != true)
        {
            isActive = false;
            isVisible = false;
            StatusId = 6;
           // remark = Remark;
        }
        else if (blogDetails != null && Action == "Approve")
        {
                isActive = true;
                isVisible = true;
                StatusId = 3;
                remark = Remark.Split(',')[2];
                blogDetails.ValidFromDate = Convert.ToDateTime(Remark.Split(',')[0]);
                blogDetails.ValidTillDate = Convert.ToDateTime(Remark.Split(',')[1]);
            }
        else if (blogDetails != null && Action == "Reject")
        {
            isActive = true;
            isVisible = false;
            StatusId = 4;
                remark = Remark.Split(',')[2];
            }
            blogDetails.UpdatedDate = DateTime.Now;
            blogDetails.UpdatedBy = CreatedBy;
            blogDetails.IsActive = isActive;
            blogDetails.IsVisible = isVisible;
        _Offshore_ArabchaldoContext.BlogDetails.Update(blogDetails);
        _Offshore_ArabchaldoContext.SaveChanges();
        ManageWorkflowDetails(id, StatusId, remark, CreatedBy);
        _Offshore_ArabchaldoContext.SaveChanges();
        return true;


    }


        #region Workflow&Image

        //Create Directory If not exist
        public void ManageUploads(string path, string FileUrl, string UploadType)
        {
            if (FileUrl!="")
            {
                string AdDirectory = "StaticFiles\\Blogs\\" + path + "\\" + UploadType;
                if (!Directory.Exists(AdDirectory))
                {
                    Directory.CreateDirectory(AdDirectory);
                }
                var SourcefolderName = Path.Combine("StaticFiles", "Images");
                var Sourcepath = Path.Combine(Directory.GetCurrentDirectory(), SourcefolderName);
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), AdDirectory);
                var SourceFile = Sourcepath + "\\" + FileUrl;
                var DestinationFile = pathToSave + "\\" + FileUrl;
                if (!File.Exists(pathToSave + FileUrl))
                {
                    File.Move(SourceFile, DestinationFile);
                }
            }
            
        }


        // Insert Workflow Details
        public void ManageWorkflowDetails(long BlogId, int status, string Remark, long CreatedBy)
        {
            long PostDetailsId = GetPostDetailsId(BlogId, status, 4, CreatedBy); // 4 == posttype Blog

            if (PostDetailsId > 0)
            {


                PostStatusDetails postStatusDetails = new PostStatusDetails();
                postStatusDetails.PostDetailsId = PostDetailsId;
                postStatusDetails.IsActive = true;
                postStatusDetails.CreatedBy = CreatedBy;
                postStatusDetails.CreatedDate = DateTime.Now;
                postStatusDetails.Remark = Remark;
                if (status == 1)
                {
                    postStatusDetails.WorkflowStatusId = status;
                }

                else
                {
                    _Offshore_ArabchaldoContext.PostStatusDetails.Where(x => x.PostDetailsId == postStatusDetails.PostDetailsId && x.IsActive == true).ToList().ForEach(x =>
                    {
                        x.IsActive = false; x.UpdatedDate = DateTime.Now;
                    });
                    postStatusDetails.WorkflowStatusId = status;
                }

                _Offshore_ArabchaldoContext.PostStatusDetails.Add(postStatusDetails);
                _Offshore_ArabchaldoContext.SaveChanges();
            }
        }


        // Insert PostDetails
        public long GetPostDetailsId(long BlogId, int status, int PostTypeId,long CreatedBy)
        {
            long PostDetailsId = 0;
            PostDetails postDetails = new PostDetails();
            postDetails = _Offshore_ArabchaldoContext.PostDetails.FirstOrDefault(e => e.PostId == BlogId && e.PostTypeId == PostTypeId && e.IsActive == true);
            if (postDetails == null)
            {
                PostDetails addpostDetails = new PostDetails();
                addpostDetails.PostId = BlogId;
                addpostDetails.PostTypeId = PostTypeId;
                addpostDetails.IsActive = true;
                addpostDetails.CreatedBy = CreatedBy;
                addpostDetails.CreatedDate = DateTime.Now;
                _Offshore_ArabchaldoContext.PostDetails.Add(addpostDetails);
                _Offshore_ArabchaldoContext.SaveChanges();
                PostDetailsId = addpostDetails.PostDetailsId;
            }
            else
                PostDetailsId = postDetails.PostDetailsId;
            return PostDetailsId;
        }

        #endregion


        #region Grid

        // Blog
        public class BlogList
        {
            public long BlogId { get; set; }
            public string BlogTitle { get; set; }
            public string Description { get; set; }
            public long CreatedBy { get; set; }
            public string postedfor { get; set; }
            public string ImageUrl { get; set; }
            public DateTime validTill { get; set; }
        }
        public DataTable FillBlogList()
        {
            List<BlogList> blogList = new List<BlogList>();
            blogList = (from b in _Offshore_ArabchaldoContext.BlogDetails
                        join u in _Offshore_ArabchaldoContext.UserDetails
                        on b.CreatedBy equals u.UserId
                  
                        where b.IsActive == true orderby b.UpdatedDate descending
                        select new BlogList { BlogId = b.BlogId, BlogTitle = b.Title, Description = b.Description,  CreatedBy=b.CreatedBy,  postedfor = u.FirstName, validTill= b.CreatedDate }).ToList();

            DataTable dtblogList = new DataTable();
            dtblogList.Columns.Add("BlogId", typeof(long));
            dtblogList.Columns.Add("BlogTitle", typeof(long));
            dtblogList.Columns.Add("Description", typeof(string));
            dtblogList.Columns.Add("CreatedBy", typeof(long));
            dtblogList.Columns.Add("postedfor", typeof(string));
            dtblogList.Columns.Add("validTill", typeof(DateTime));
            foreach (var item in blogList)
            {
                dtblogList.Rows.Add(item.BlogId, item.BlogTitle, item.Description, item.CreatedBy, item.postedfor, item.validTill);
            }
            dtblogList.AcceptChanges();
            return dtblogList;
        }


        #endregion




    }
}