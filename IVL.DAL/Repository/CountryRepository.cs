﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore.Internal;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class CountryRepository : ICountry
    {
        private Offshore_ArabchaldoContext Acdcontext;
        public CountryRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }

        public bool CheckCountryExits(string CountryName)
        {
            Country CountryDetails = Acdcontext.Country.FirstOrDefault(e => e.CountryName == CountryName);
            if (CountryDetails != null)
            {
                return true;
            }

            return false;
        }
        public bool DeleteCountry(int CountryID)
        {
            Country CountryDetails = Acdcontext.Country.FirstOrDefault(e => e.CountryId == CountryID);

            if (CountryDetails != null)
            {
                CountryDetails.IsActive = false;
                Acdcontext.Country.Update(CountryDetails);
                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }
        public bool UpdateCountry(int CountryID, Country updateCountryDetails)
        {
            Country CountryDetails = Acdcontext.Country.FirstOrDefault(e => e.CountryId == CountryID);

            if (updateCountryDetails != null)
            {

                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }
        public List<Country> GetAllCountry()
        {
            List<Country> allCountryList = new List<Country>();
            return Acdcontext.Country.ToList();
        }

        public Country GetCountryById(int CountryId)
        {
            return Acdcontext.Country.FirstOrDefault(e => e.CountryId == CountryId);
        }

        public bool InsertCountry(Country Countrydeatils)
        {
            Acdcontext.Country.Add(Countrydeatils);
            Acdcontext.SaveChanges();

            return true;
        }
    }
}
