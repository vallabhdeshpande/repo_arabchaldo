﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using System.Linq;
using System.IO;
using System.Data;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class EventRepository : IEventDetails
    {
        private Offshore_ArabchaldoContext _Offshore_ArabchaldoContext;

        public EventRepository(Offshore_ArabchaldoContext context)
        {
            _Offshore_ArabchaldoContext = context;
        }

        public bool Delete(long id)
        {
            //EventDetails eventAvailable = IsEventIdExist(id);
            //if (eventAvailable != null)
            //{
            //EventDetails eventAvailable = new EventDetails();
            EventDetails eventAvailable = GetEventById(id);

             eventAvailable.IsActive = false;
            _Offshore_ArabchaldoContext.SaveChanges();

                return true;
            //}

            //return false;
        }

        public class EventList
        {
            public long EventId { get; set; }
            public string Title { get; set; }
            public long PostedFor { get; set; }
            public bool? IsVisible { get; set; }
            public bool? IsActive { get; set; }
            public string CityName { get; set; }
            public bool? IsPremium { get; set; }
            public string CreatedDate { get; set; }
            public string ValidFromDate { get; set; }
            public string ValidTillDate { get; set; }
            public string EventDate { get; set; }
        }

        public DataTable GetAllEvent(long loggedInUserId, string loggedInRole)
        {
            List<Offshore_ArabchaldoContext.EventList> EventList = new List<Offshore_ArabchaldoContext.EventList>();
            EventList = _Offshore_ArabchaldoContext.ssp_GetAllEventListDetails(loggedInUserId,loggedInRole);

            DataTable dteventList = new DataTable();
            dteventList.Columns.Add("eventId", typeof(long));
            dteventList.Columns.Add("ImageUrl", typeof(string));
            dteventList.Columns.Add("title", typeof(string));
            dteventList.Columns.Add("cityName", typeof(string));
            dteventList.Columns.Add("status", typeof(string));
            dteventList.Columns.Add("isPremium", typeof(string));
            dteventList.Columns.Add("isVisible", typeof(bool));
            dteventList.Columns.Add("isActive", typeof(bool));
            dteventList.Columns.Add("createdDate", typeof(string));
            dteventList.Columns.Add("createdBy", typeof(long));
            dteventList.Columns.Add("updatedDate", typeof(string));
            dteventList.Columns.Add("validFromDate", typeof(string));
            dteventList.Columns.Add("validTillDate", typeof(string));
            dteventList.Columns.Add("eventDate", typeof(string));
            dteventList.Columns.Add("activity", typeof(bool));


            foreach (var item in EventList)
            {
                dteventList.Rows.Add(item.EventId, item.ImageUrl, item.Title, item.CityName, item.Status, item.IsPremium, item.IsVisible, item.IsActive, item.CreatedDate, item.CreatedBy,item.UpdatedDate, item.ValidFromDate,
                    item.ValidTillDate, item.EventDate,item.Activity);
            }
            dteventList.AcceptChanges();

            return dteventList;
            //if (userId == 0)
            //{
            //    List<EventDetails> eventDetails = new List<EventDetails>();
            //    // eventDetails = _Offshore_ArabchaldoContext.EventDetails.ToList().FindAll(e=>e.IsActive == true);

            //    // added for order by descending
            //    //eventDetails = _Offshore_ArabchaldoContext.EventDetails.Where(e => e.IsActive == true ).OrderBy(n => n.UpdatedDate).ToList();
            //    eventDetails = (_Offshore_ArabchaldoContext.EventDetails.Where(e => e.IsActive == true && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
            //        (_Offshore_ArabchaldoContext.EventDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).Concat
            //        (_Offshore_ArabchaldoContext.EventDetails.Where(e => e.IsActive == true && e.IsVisible == false).ToList()).OrderBy(n => n.UpdatedDate).ToList());

            //    return eventDetails;
            //}

            //else
            //{
            //    List<EventDetails> eventDetails = new List<EventDetails>();
            //    // eventDetails = _Offshore_ArabchaldoContext.EventDetails.ToList().FindAll(e=>e.IsActive == true);

            //    // added for order by descending
            //    // eventDetails = _Offshore_ArabchaldoContext.EventDetails.Where(e => e.PostedFor == userId && e.IsActive == true ).OrderBy(n => n.UpdatedDate).ToList();
            //    eventDetails = (_Offshore_ArabchaldoContext.EventDetails.Where(e => e.PostedFor == userId && e.IsActive == true && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
            //         (_Offshore_ArabchaldoContext.EventDetails.Where(e => e.PostedFor == userId && e.IsActive == true && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).Concat
            //         (_Offshore_ArabchaldoContext.EventDetails.Where(e => e.PostedFor == userId && e.IsActive == true && e.IsVisible == false).ToList()).OrderBy(n => n.UpdatedDate).ToList());

            //    return eventDetails;
            //}
        }

        public EventDetails GetEventById(long id)
        {
            // EventDetails eventIdExist = _Offshore_ArabchaldoContext.EventDetails.FirstOrDefault(e => e.EventId == id);
            EventDetails eventDetails = new EventDetails(); 

            eventDetails = _Offshore_ArabchaldoContext.EventDetails.FirstOrDefault(e => e.EventId == id);
            if (eventDetails != null)
            {
                return eventDetails;
            }
            return null;
        }

        public bool Insert(EventDetails eventDetails)
        {
            EventDetails isExist = IsEventIdExist(eventDetails.EventId, eventDetails.Title);
            if (isExist == null)
            {
             
                eventDetails.UpdatedDate = DateTime.Now;
                eventDetails.IsPremium = eventDetails.IsPaidAd;
                eventDetails.UpdatedBy = eventDetails.CreatedBy;
                eventDetails.VisitorCount = 0;
                _Offshore_ArabchaldoContext.EventDetails.Add(eventDetails);
                _Offshore_ArabchaldoContext.SaveChanges();
                long EventId = eventDetails.EventId;
                //Save image in directory
                if (eventDetails.ImageUrl != null)
                    ManageUploads(EventId.ToString(), eventDetails.ImageUrl, "Image");
                // Workflow for Created state
                ManageWorkflowDetails(eventDetails.EventId, 1, "", eventDetails.CreatedBy);
                return true;
            }
            return false;
        }

        public bool Update(long eventId, EventDetails eventDetails)
        {
            EventDetails isExist = IsEventIdExist(eventId, eventDetails.Title);

            if (isExist != null)
            {
                isExist.EventId = eventDetails.EventId;
                isExist.Title = eventDetails.Title;
                isExist.Description = eventDetails.Description;
                isExist.PostedForName = eventDetails.ContactPersonName;
                isExist.EventDate = eventDetails.EventDate;
                if (eventDetails.ImageUrl != null && (isExist.ImageUrl != eventDetails.ImageUrl))
                {

                    ManageUploads(eventId.ToString(), eventDetails.ImageUrl, "Image");

                }

                isExist.AddressStreet1 = eventDetails.AddressStreet1;
                isExist.AddressStreet2 = eventDetails.AddressStreet2;
                isExist.CityId = eventDetails.CityId;
                isExist.CityName = eventDetails.CityName;
                isExist.CountryId = eventDetails.CountryId;
                isExist.StateId = eventDetails.StateId;
                isExist.StateName = eventDetails.StateName;
                isExist.ImageUrl = eventDetails.ImageUrl;
                isExist.IsVisible = null;
                isExist.IsActive = eventDetails.IsActive;
                isExist.UpdatedBy = eventDetails.CreatedBy;
                isExist.UpdatedDate = DateTime.Now;
                isExist.PostedFor = eventDetails.PostedFor;
                isExist.CategoryId = eventDetails.CategoryId;
                isExist.CategoryName = eventDetails.CategoryName;
                isExist.AlternateContactNumber = eventDetails.AlternateContactNumber;
                isExist.ContactNumber = eventDetails.ContactNumber;
                isExist.FaxNumber = eventDetails.FaxNumber;
                isExist.Website = eventDetails.Website;
                isExist.ZipCode = eventDetails.ZipCode;
                isExist.ValidFromDate = null;
                isExist.ValidTillDate = null;
                isExist.CountryCodeContact = eventDetails.CountryCodeContact;
                isExist.Email = eventDetails.Email;
                isExist.OtherCity = eventDetails.OtherCity;


                // Checking the current state is Rejected
                long PostDetailsId = GetPostDetailsId(eventId, 4, 3, eventDetails.CreatedBy); // 3 == posttype Event
                PostStatusDetails CheckRejected = _Offshore_ArabchaldoContext.PostStatusDetails.FirstOrDefault(e => e.PostDetailsId == PostDetailsId && e.IsActive == true && e.WorkflowStatusId == 4);
                if (CheckRejected != null)
                {
                    ManageWorkflowDetails(eventId, 5, "", eventDetails.CreatedBy); // Rectified
                }
                else
                    ManageWorkflowDetails(eventId, 2, "", eventDetails.CreatedBy); //Modified
                return true;
            }
            return false;
        }

        private EventDetails IsEventIdExist(long id,string Title)
        {
            EventDetails eventIdExist = _Offshore_ArabchaldoContext.EventDetails.FirstOrDefault(e => e.EventId == id);
            if (eventIdExist != null)
            {
                return eventIdExist;
            }
            return null;
        }

        // Delete, Approve and Reject
        public bool EventStatus(long id, string Remark, string Action, long CreatedBy)
        {
            EventDetails eventDetails = _Offshore_ArabchaldoContext.EventDetails.FirstOrDefault(e => e.EventId == id);
            int StatusId = 0;
            bool isActive = true;
            bool isVisible = true;
            string remark = "";

            if (eventDetails != null && Action == "Delete" && eventDetails.IsVisible == true)
            {
                return false;
            }

            else if (eventDetails != null && Action == "Delete")
            {
                isActive = false;
                isVisible = false;
                StatusId = 6;
               // remark = Remark;
            }
            else if (eventDetails != null && Action == "Approve")
            {
                isActive = true;
                isVisible = true;
                StatusId = 3;
                remark = Remark.Split(',')[2];
                eventDetails.ValidFromDate = Convert.ToDateTime(Remark.Split(',')[0]);
                eventDetails.ValidTillDate = Convert.ToDateTime(Remark.Split(',')[1]);
       
            }
            else if (eventDetails != null && Action == "Reject")
            {
                isActive = true;
                isVisible = false;
                StatusId = 4;
                remark = Remark.Split(',')[2];
            }
            eventDetails.UpdatedDate = DateTime.Now;
            eventDetails.UpdatedBy = CreatedBy;
            eventDetails.IsActive = isActive;
            eventDetails.IsVisible = isVisible;
            _Offshore_ArabchaldoContext.EventDetails.Update(eventDetails);
            _Offshore_ArabchaldoContext.SaveChanges();
            ManageWorkflowDetails(eventDetails.EventId, StatusId, remark, CreatedBy);
            _Offshore_ArabchaldoContext.SaveChanges();
            return true;


        }
        public List<EventDetails> GetEventsByCategoryId(long categoryId)
        {
            List<EventDetails> alleventDdlList = new List<EventDetails>();
             alleventDdlList = _Offshore_ArabchaldoContext.EventDetails.Where(e => e.IsActive == true && e.CategoryId== categoryId).OrderByDescending(n => n.EventId).ToList();
            return alleventDdlList;
        }
        #region Workflow&Image

        //Create Directory If not exist
        public void ManageUploads(string path, string FileUrl, string UploadType)
        {
            string AdDirectory = "StaticFiles\\Events\\" + path + "\\" + UploadType;
            if (!Directory.Exists(AdDirectory))
            {
                Directory.CreateDirectory(AdDirectory);
            }
            var SourcefolderName = Path.Combine("StaticFiles", "Images");
            var Sourcepath = Path.Combine(Directory.GetCurrentDirectory(), SourcefolderName);
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), AdDirectory);
            var SourceFile = Sourcepath + "\\" + FileUrl;
            var DestinationFile = pathToSave + "\\" + FileUrl;
            if (!File.Exists(pathToSave + FileUrl))
            {
                File.Move(SourceFile, DestinationFile);
            }
        }

        // Insert Workflow Details
        public void ManageWorkflowDetails(long eventId, int status, string Remark, long CreatedBy)
        {
            long PostDetailsId = GetPostDetailsId(eventId, status, 3, CreatedBy); // 3 == posttype Event
            if (PostDetailsId > 0)
            {
                PostStatusDetails postStatusDetails = new PostStatusDetails();
                postStatusDetails.PostDetailsId = PostDetailsId;
                postStatusDetails.IsActive = true;
                postStatusDetails.CreatedBy = CreatedBy;
                postStatusDetails.CreatedDate = DateTime.Now;
                postStatusDetails.Remark = Remark;
                if (status == 1)
                {
                    postStatusDetails.WorkflowStatusId = status;
                }

                else
                {
                    _Offshore_ArabchaldoContext.PostStatusDetails.Where(x => x.PostDetailsId == postStatusDetails.PostDetailsId && x.IsActive == true).ToList().ForEach(x =>
                    {
                        x.IsActive = false; x.UpdatedDate = DateTime.Now;
                    });
                    postStatusDetails.WorkflowStatusId = status;
                }

                _Offshore_ArabchaldoContext.PostStatusDetails.Add(postStatusDetails);
                _Offshore_ArabchaldoContext.SaveChanges();
                //InsertPostStatusDetails(postStatusDetails);
            }
        }


        // Insert PostDetails
        public long GetPostDetailsId(long eventId, int status, int PostTypeId, long CreatedBy)
        {
            long PostDetailsId = 0;
            PostDetails postDetails = new PostDetails();
            postDetails = _Offshore_ArabchaldoContext.PostDetails.FirstOrDefault(e => e.PostId == eventId && e.PostTypeId == PostTypeId && e.IsActive == true);
            if (postDetails == null)
            {
                PostDetails addpostDetails = new PostDetails();
                addpostDetails.PostId = eventId;
                addpostDetails.PostTypeId = PostTypeId;
                addpostDetails.IsActive = true;
                addpostDetails.CreatedBy = CreatedBy;
                addpostDetails.CreatedDate = DateTime.Now;
                _Offshore_ArabchaldoContext.PostDetails.Add(addpostDetails);
                _Offshore_ArabchaldoContext.SaveChanges();
                PostDetailsId = addpostDetails.PostDetailsId;
            }
            else
                PostDetailsId = postDetails.PostDetailsId;
            return PostDetailsId;
        }



        #endregion
    }
}
