﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore.Internal;
using System.IO;
using System.Data;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class ManageCategoryRepository : IManageCategory
    {
        private Offshore_ArabchaldoContext Acdcontext;
        public ManageCategoryRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }

        public bool CheckCategoryExits(string CategoryName)
        {
            Category CategoryDetails = Acdcontext.Category.FirstOrDefault(e => e.CategoryName == CategoryName && e.IsActive == true);

            if (CategoryDetails != null)
            {
                return true;
            }

            return false;
        }

        public bool DeleteCategory(int CategoryID)
        {
            Category CategoryDetails = Acdcontext.Category.FirstOrDefault(e => e.CategoryId == CategoryID && e.IsActive == true);

           AdDetails addtls= Acdcontext.AdDetails.FirstOrDefault(e => e.CategoryId == CategoryID && e.IsActive == true && e.IsVisible == true);
            

            if (CategoryDetails != null && addtls == null && CategoryDetails.SubCategory.Count ==0)
            {
                CategoryDetails.IsActive = false;
                Acdcontext.Category.Update(CategoryDetails);
                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }
        public bool UpdateCategory(int CategoryID, Category updateCatDetails)
        {
            Category CategoryDetails = Acdcontext.Category.FirstOrDefault(e => e.CategoryId == CategoryID);
            Category CheckExistCategoryDetails = Acdcontext.Category.FirstOrDefault(e => e.CategoryName == updateCatDetails.CategoryName && e.CategoryId != CategoryID && e.IsActive == true);
            if (CheckExistCategoryDetails == null)
            {
                CategoryDetails.CategoryId = updateCatDetails.CategoryId;
                CategoryDetails.CategoryName = updateCatDetails.CategoryName;
                CategoryDetails.ArabicName = updateCatDetails.ArabicName;
                CategoryDetails.CategoryImageUrl = updateCatDetails.CategoryImageUrl;
                CategoryDetails.Description = updateCatDetails.Description;
                CategoryDetails.CreatedBy = updateCatDetails.CreatedBy;
                CategoryDetails.CreatedDate = updateCatDetails.CreatedDate;
                CategoryDetails.UpdatedBy = updateCatDetails.UpdatedBy;
                CategoryDetails.UpdatedDate = DateTime.Now;

                Acdcontext.SaveChanges();

                Int64 categoryId = updateCatDetails.CategoryId;

                //Save image in directory
                if (updateCatDetails.CategoryImageUrl != null)
                {
                    ManageUploads(categoryId.ToString(), updateCatDetails.CategoryImageUrl, "Image");
                }

                return true;
            }

            return false;
        }
        public List<Category> GetAllCategory()
        {
            List<Category> allCategoryList = new List<Category>();
            return Acdcontext.Category.Where(e => e.IsActive == true).OrderBy(e => e.UpdatedDate).ToList();
        }

        public Category GetCategorybyId(int CategoryId)
        {
            return Acdcontext.Category.FirstOrDefault(e => e.CategoryId == CategoryId);
        }

        public bool InsertCategory(Category Categorydeatils)
        {
            Categorydeatils.IsActive = true;
            Categorydeatils.UpdatedDate = DateTime.Now;
            Acdcontext.Category.Add(Categorydeatils);
            Acdcontext.SaveChanges();
            Int64 categoryId = Categorydeatils.CategoryId;

            //Save image in directory
            if (Categorydeatils.CategoryImageUrl != null)
            {
                ManageUploads(categoryId.ToString(), Categorydeatils.CategoryImageUrl, "Image");
            }
            return true;
        }

        //Create Directory If not exist
        public void ManageUploads(string path, string FileUrl, string UploadType)
        {
            string AdDirectory = "StaticFiles\\Category\\" + path + "\\" + UploadType;
            if (!Directory.Exists(AdDirectory))
            {
                Directory.CreateDirectory(AdDirectory);
            }
            var SourcefolderName = Path.Combine("StaticFiles", "Images");
            var Sourcepath = Path.Combine(Directory.GetCurrentDirectory(), SourcefolderName);
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), AdDirectory);
            var SourceFile = Sourcepath + "\\" + FileUrl;
            var DestinationFile = pathToSave + "\\" + FileUrl;
            if (!File.Exists(pathToSave + FileUrl) && File.Exists(SourceFile))
            {
                File.Move(SourceFile, DestinationFile);
            }
        }

        #region Grid

        // Category
        public class CategoryList
        {
            public long categoryId { get; set; }
            public string CategoryName { get; set; }
            public string ArabicName { get; set; }
            public string Description { get; set; }
        }
        public DataTable FillCategoryList()
        {
            List<Offshore_ArabchaldoContext.AllCategorylist> CategoryList = new List<Offshore_ArabchaldoContext.AllCategorylist>();
            CategoryList = Acdcontext.ssp_GetAllCategoryListDetails();

            //List<CategoryList>categoryList = new List<CategoryList>();
            //categoryList = (from c in Acdcontext.Category

            //                where c.IsActive == true
            //                orderby c.UpdatedDate ascending
            //                select new CategoryList { categoryId = c.CategoryId, Description = c.Description, CategoryName = c.CategoryName, ArabicName = c.ArabicName }).ToList();

            DataTable dtcategoryList = new DataTable();
            dtcategoryList.Columns.Add("CategoryId", typeof(long));
            dtcategoryList.Columns.Add("CategoryName", typeof(string));
            dtcategoryList.Columns.Add("ArabicName", typeof(string));
            dtcategoryList.Columns.Add("Description", typeof(string));
            dtcategoryList.Columns.Add("IsActive", typeof(bool));
        
            foreach (var item in CategoryList)
            {
                dtcategoryList.Rows.Add(item.CategoryId, item.CategoryName, item.ArabicName, item.Description,item.IsActive);
            }
            dtcategoryList.AcceptChanges();
            return dtcategoryList;
        }


        #endregion



        // need to be modified and should be implemented.
        //public bool CheckCategoryExits(int CategoryID)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
