﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class SmallAdDetailsRepository : ISmallAdDetails
    {
        private Offshore_ArabchaldoContext Acdcontext;
        public SmallAdDetailsRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }

        public bool CheckSmallAdDetailsExits(string Title, long SmallAdId)
        {
            //SmallAdDetails SmallAdDetailsDetails = Acdcontext.SmallAdDetails.AsNoTracking().FirstOrDefault(e => e.Title == Title && e.SmallAdId != SmallAdId && e.IsActive == true);
            ////Acdcontext.SmallAdDetails.ExecuteSqlCommand("updateSmallAdDetails");
            //// var cmd = Acdcontext.CreateCommand();
            //if (SmallAdDetailsDetails != null)
            //{
            //    return true;
            //}

            return false;
        }

        // Delete, Approve and Reject
        public bool SmallAdStatus(long SmallAdId, string Remark, string Action, long CreatedBy)
        {
            SmallAdDetails SmallAdDetails = Acdcontext.SmallAdDetails.FirstOrDefault(e => e.SmallAdId == SmallAdId && e.IsActive == true);
            int StatusId = 0;
            bool isActive = true;
            bool isVisible = true;
            string remark = "";
            if (SmallAdDetails != null && Action == "Delete")
            {
                if (SmallAdDetails.IsVisible == true)
                    return false;
                else
                {
                    isActive = false;
                    isVisible = false;
                    StatusId = 6;
                }

                //remark = Remark.Split(',')[2];
            }
            else if (SmallAdDetails != null && Action == "Approve")
            {
                isActive = true;
                isVisible = true;
                StatusId = 3;
                remark = Remark.Split(',')[2];
                SmallAdDetails.ValidFromDate = Convert.ToDateTime(Remark.Split(',')[0]);
                SmallAdDetails.ValidTillDate = Convert.ToDateTime(Remark.Split(',')[1]);
                SmallAdDetails.IsPremium = Convert.ToBoolean(Remark.Split(',')[3]);
            }
            else if (SmallAdDetails != null && Action == "Reject")
            {
                isActive = true;
                isVisible = false;
                StatusId = 4;
                remark = Remark.Split(',')[2];
            }

            SmallAdDetails.IsActive = isActive;
            SmallAdDetails.IsVisible = isVisible;
            SmallAdDetails.UpdatedDate = DateTime.Now;
            SmallAdDetails.UpdatedBy = CreatedBy;
            Acdcontext.SmallAdDetails.Update(SmallAdDetails);
            Acdcontext.SaveChanges();
            //SmallAdDetails.CreatedBy = CreatedBy;
            //ManageWorkflowDetails(SmallAdDetails, StatusId, remark);
            ManageWorkflowDetails(SmallAdId, StatusId, remark, CreatedBy);
            Acdcontext.SaveChanges();
            return true;


        }

        public bool UpdateSmallAdDetails(long SmallAdId, SmallAdDetails updateSmallAdDetails)
        {

            Acdcontext.Entry(updateSmallAdDetails).State = EntityState.Detached;

            SmallAdDetails SmallAdDetailsDetails = Acdcontext.SmallAdDetails.FirstOrDefault(e => e.SmallAdId == SmallAdId && e.IsActive == true);
            // SmallAdDetails SmallAdDetailsDetails = new SmallAdDetails();

            if (SmallAdDetailsDetails != null)
            {
                SmallAdDetailsDetails.Title = updateSmallAdDetails.Title;
                SmallAdDetailsDetails.TagLine = updateSmallAdDetails.TagLine;
                SmallAdDetailsDetails.Description = updateSmallAdDetails.Description;
               
                SmallAdDetailsDetails.PostedForName = updateSmallAdDetails.PostedForName;
                //SmallAdDetailsDetails.PostedFor = updateSmallAdDetails.PostedFor;
               
                SmallAdDetailsDetails.AdLogoUrl = updateSmallAdDetails.AdLogoUrl;
                SmallAdDetailsDetails.ImageUrl = updateSmallAdDetails.ImageUrl;
                SmallAdDetailsDetails.ContactPersonName = updateSmallAdDetails.ContactPersonName;
                SmallAdDetailsDetails.ContactNumber = updateSmallAdDetails.ContactNumber;
                SmallAdDetailsDetails.AlternateContactNumber = updateSmallAdDetails.AlternateContactNumber;
                SmallAdDetailsDetails.FaxNumber = updateSmallAdDetails.FaxNumber;
                SmallAdDetailsDetails.Email = updateSmallAdDetails.Email;
                if (updateSmallAdDetails.AddressStreet1 == null)
                    SmallAdDetailsDetails.AddressStreet1 = "";
                else
                    SmallAdDetailsDetails.AddressStreet1 = updateSmallAdDetails.AddressStreet1;
                if (updateSmallAdDetails.AddressStreet2 == null)
                    SmallAdDetailsDetails.AddressStreet2 = "";
                else
                    SmallAdDetailsDetails.AddressStreet2 = updateSmallAdDetails.AddressStreet2;
                SmallAdDetailsDetails.CountryId = updateSmallAdDetails.CountryId;
                SmallAdDetailsDetails.StateId = updateSmallAdDetails.StateId;
                SmallAdDetailsDetails.CityId = updateSmallAdDetails.CityId;
                SmallAdDetailsDetails.StateName = updateSmallAdDetails.StateName;

                if (updateSmallAdDetails.CityId == 9999)
                    SmallAdDetailsDetails.CityName = "Other";
                else
                    SmallAdDetailsDetails.CityName = updateSmallAdDetails.CityName;
                SmallAdDetailsDetails.OtherCity = updateSmallAdDetails.OtherCity;
                SmallAdDetailsDetails.AssignedTo = updateSmallAdDetails.AssignedTo;
                SmallAdDetailsDetails.IsPremium = updateSmallAdDetails.IsPaidAd;
                SmallAdDetailsDetails.IsPaidAd = updateSmallAdDetails.IsPaidAd;
                if (updateSmallAdDetails.ZipCode == null)
                    SmallAdDetailsDetails.ZipCode = "";
                else
                    SmallAdDetailsDetails.ZipCode = updateSmallAdDetails.ZipCode;
                SmallAdDetailsDetails.ValidFromDate = null;
                SmallAdDetailsDetails.ValidTillDate = null;
                SmallAdDetailsDetails.CountryCodeContact = updateSmallAdDetails.CountryCodeContact;
                SmallAdDetailsDetails.UpdatedDate = DateTime.Now;
                SmallAdDetailsDetails.UpdatedBy = updateSmallAdDetails.CreatedBy;
                SmallAdDetailsDetails.IsVisible = null;

                if (updateSmallAdDetails.CityId == 0 || updateSmallAdDetails.CityId == null)
                    SmallAdDetailsDetails.CityName = "";
                if (updateSmallAdDetails.StateId == 0 || updateSmallAdDetails.StateId == null)
                    SmallAdDetailsDetails.StateName = "";
                
                SmallAdDetailsDetails.StateName = updateSmallAdDetails.StateName;
                SmallAdDetailsDetails.CityName = updateSmallAdDetails.CityName;
                Acdcontext.SmallAdDetails.Update(SmallAdDetailsDetails);
                Acdcontext.SaveChanges();
                // Update images

                if (SmallAdDetailsDetails.AdLogoUrl != null)
                    ManageUploads(SmallAdId.ToString(), SmallAdDetailsDetails.AdLogoUrl, "Logo");
                // Delete all images for a SmallAdId
                if (SmallAdDetailsDetails.ImageUrl != null)
                {
                    //var adimgdtls = Acdcontext.SmallAdImageDetails.Where(x => x.SmallAdId == SmallAdDetailsDetails.SmallAdId && x.IsActive == true).ToList();

                    //List<string> imglist = new List<string>();

                    //foreach (var item in adimgdtls)
                    //{
                    //    imglist.Add(item.AdImageUrl);
                    //}


                    //
                    //var matchImages = Images.Intersect(imglist);
                    var Images = new List<string>(SmallAdDetailsDetails.ImageUrl.Split(',')).ToList();
                    //  Insert updated Images
                    foreach (var item in Images)
                    {

                        SmallAdImageDetails chkimgexist = Acdcontext.SmallAdImageDetails.FirstOrDefault(e => e.SmallAdImageUrl == item && e.SmallAdId == SmallAdDetailsDetails.SmallAdId && e.IsActive == true);
                        if (chkimgexist == null)
                        {
                            SmallAdImageDetails adimages = new SmallAdImageDetails();
                            adimages.SmallAdId = SmallAdDetailsDetails.SmallAdId;
                            adimages.SmallAdImageUrl = item;
                            adimages.IsActive = true;
                            adimages.CreatedBy = SmallAdDetailsDetails.CreatedBy;
                            adimages.CreatedDate = DateTime.Now;
                            Acdcontext.SmallAdImageDetails.Add(adimages);
                            Acdcontext.SaveChanges();
                            if (item != null)
                                ManageUploads(SmallAdDetailsDetails.SmallAdId.ToString(), item, "Images");
                        }
                    }


                }
                // Checking the current state is Rejected
                long PostDetailsId = GetPostDetailsId(SmallAdId, 4, 6, SmallAdDetailsDetails.CreatedBy); // 6 == posttype Small Ads
                PostStatusDetails CheckRejected = Acdcontext.PostStatusDetails.FirstOrDefault(e => e.PostDetailsId == PostDetailsId && e.IsActive == true && e.WorkflowStatusId == 4);
                if (CheckRejected != null)
                {
                    ManageWorkflowDetails(SmallAdId, 5, "", updateSmallAdDetails.CreatedBy); // Rectified
                }
                else
                    ManageWorkflowDetails(SmallAdId, 2, "", updateSmallAdDetails.CreatedBy); //Modified
                Acdcontext.SaveChanges();
               // UpdateForeignKeyConstraints(SmallAdDetailsDetails.SmallAdId, SmallAdDetailsDetails.SubCategoryId);

                return true;
            }

            return false;
        }
        public List<SmallAdDetails> GetAllSmallAdDetails(long loggedInUserId, string loggedInRole)
        {
            SmallAdDetails SmallAdDetails = new SmallAdDetails();
            List<SmallAdDetails> allSmallAdDetailsList = new List<SmallAdDetails>();
            //try
            //{
            //    Acdcontext.Database.ExecuteSqlCommand("update SmallAdDetails  Set CategoryName=ct.CategoryName, PostedForName = u.FirstName + ' ' + u.LastName FROM SmallAdDetails ad INNER JOIN  Category ct ON ad.CategoryId = ct.CategoryId INNER JOIN UserDetails u on ad.postedfor = u.UserId");
            //    Acdcontext.Database.ExecuteSqlCommand("exec [dbo].[updateSmallAdDetails]");
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            //  allSmallAdDetailsList = Acdcontext.ssp_GetAllAds(loggedInRole,"", loggedInUserId);


            if (loggedInRole == "SuperAdmin" || loggedInRole == "Admin")  //&& e.ValidTillDate >= DateTime.Today
            {
                //allSmallAdDetailsList = Acdcontext.SmallAdDetails.Where(e => e.IsActive == true && e.CategoryName != "Jobs").OrderBy(n => n.UpdatedDate).ToList();
                allSmallAdDetailsList = (Acdcontext.SmallAdDetails.AsNoTracking().Where(e => e.IsActive == true  && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
                    (Acdcontext.SmallAdDetails.AsNoTracking().Where(e => e.IsActive == true  && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).Concat
                    (Acdcontext.SmallAdDetails.AsNoTracking().Where(e => e.IsActive == true  && e.IsVisible == false).ToList()).OrderBy(n => n.UpdatedDate).ToList());
            }

            else
            {
                allSmallAdDetailsList = (Acdcontext.SmallAdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId  && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
                    (Acdcontext.SmallAdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId  && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).Concat
                    (Acdcontext.SmallAdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId  && e.IsVisible == false).ToList()).OrderBy(n => n.UpdatedDate).ToList());
                  }


            return allSmallAdDetailsList;



          

        }

   

        public DataTable GetAllSmallAdsDetails(long loggedInUserId, string loggedInRole)
        {
            SmallAdDetails SmallAdDetails = new SmallAdDetails();
            List<SmallAdDetails> allSmallAdDetailsList = new List<SmallAdDetails>();
            //try
            //{
            //    Acdcontext.Database.ExecuteSqlCommand("update SmallAdDetails  Set CategoryName=ct.CategoryName, PostedForName = u.FirstName + ' ' + u.LastName FROM SmallAdDetails ad INNER JOIN  Category ct ON ad.CategoryId = ct.CategoryId INNER JOIN UserDetails u on ad.postedfor = u.UserId");
            //    Acdcontext.Database.ExecuteSqlCommand("exec [dbo].[updateSmallAdDetails]");
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

          

            List<Offshore_ArabchaldoContext.AllSmallAdslist> allSmallAds = new List<Offshore_ArabchaldoContext.AllSmallAdslist>();
            allSmallAds = Acdcontext.ssp_GetAllSmallAds(loggedInRole, loggedInUserId);
            DataTable dtAllSmallAds = new DataTable();
            dtAllSmallAds.Columns.Add("SmallAdId", typeof(long));
            dtAllSmallAds.Columns.Add("adLogoUrl", typeof(string));
            dtAllSmallAds.Columns.Add("title", typeof(string));          
            dtAllSmallAds.Columns.Add("cityName", typeof(string));
            dtAllSmallAds.Columns.Add("status", typeof(string));
            //dtAllSmallAds.Columns.Add("isFeatured", typeof(bool));
            dtAllSmallAds.Columns.Add("isVisible", typeof(bool));
            dtAllSmallAds.Columns.Add("isPaidAd", typeof(bool));
            dtAllSmallAds.Columns.Add("isActive", typeof(bool));
            dtAllSmallAds.Columns.Add("createdDate", typeof(string));
            dtAllSmallAds.Columns.Add("createdBy", typeof(long));
            dtAllSmallAds.Columns.Add("updatedDate", typeof(string));
            dtAllSmallAds.Columns.Add("validFromDate", typeof(string));
            dtAllSmallAds.Columns.Add("validTillDate", typeof(string));
            dtAllSmallAds.Columns.Add("activity", typeof(bool));
            dtAllSmallAds.Columns.Add("isPremium", typeof(string));

            foreach (var item in allSmallAds)
            {
                try
                {
                    if (item.IsVisible != null)
                        item.Activity = true;
                    else
                        item.Activity = false;

                    dtAllSmallAds.Rows.Add(item.SmallAdId, item.AdLogoUrl, item.Title, item.CityName, item.Status //,item.IsFeatured
                        , item.IsVisible, item.IsPaidAd, item.IsActive, item.CreatedDate, item.CreatedBy, item.UpdatedDate, item.ValidFromDate, item.ValidTillDate, item.Activity, item.IsPremium);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            dtAllSmallAds.AcceptChanges();


            return dtAllSmallAds;
        }

        
        public SmallAdDetails GetSmallAdDetailsById(long SmallAdId)
        {
            return Acdcontext.SmallAdDetails.AsNoTracking().FirstOrDefault(e => e.SmallAdId == SmallAdId && e.IsActive == true);
        }

        public bool InsertSmallAdDetails(SmallAdDetails SmallAdDetails)
        {
            if (SmallAdDetails.CityId == 9999)
                SmallAdDetails.CityName = "Other";
            if (SmallAdDetails.CityId == 0 || SmallAdDetails.CityId == null)
                SmallAdDetails.CityName = "";
            if (SmallAdDetails.StateId == 0 || SmallAdDetails.StateId == null)
                SmallAdDetails.StateName = "";
            
            if (SmallAdDetails.ZipCode == null || SmallAdDetails.ZipCode == null)
                SmallAdDetails.ZipCode = "";
            if (SmallAdDetails.AddressStreet1 == null)
                SmallAdDetails.AddressStreet1 = "";
            if (SmallAdDetails.AddressStreet2 == null)
                SmallAdDetails.AddressStreet2 = "";
            SmallAdDetails.IsActive = true;
            SmallAdDetails.VisitorCount = 0;
            SmallAdDetails.UpdatedDate = DateTime.Now;
            SmallAdDetails.CreatedDate = DateTime.Now;
            SmallAdDetails.IsPremium = SmallAdDetails.IsPaidAd;
            Acdcontext.SmallAdDetails.Add(SmallAdDetails);
            Acdcontext.SaveChanges();
            long SmallAdId = SmallAdDetails.SmallAdId;
            Acdcontext.Entry(SmallAdDetails).State = EntityState.Detached;
            //Save image in directory
            if (SmallAdDetails.AdLogoUrl != null)
                ManageUploads(SmallAdId.ToString(), SmallAdDetails.AdLogoUrl, "Logo");

            // Add images
            if (SmallAdDetails.ImageUrl != null)
            {
                var Images = new List<string>(SmallAdDetails.ImageUrl.Split(',')).ToList();
                foreach (var item in Images)
                {
                    SmallAdImageDetails adimages = new SmallAdImageDetails();
                    adimages.SmallAdId = SmallAdDetails.SmallAdId;
                    adimages.SmallAdImageUrl = item;
                    adimages.IsActive = true;
                    adimages.CreatedBy = SmallAdDetails.CreatedBy;
                    adimages.CreatedDate = DateTime.Now;
                    Acdcontext.SmallAdImageDetails.Add(adimages);
                    Acdcontext.SaveChanges();
                    if (item != null)
                        ManageUploads(SmallAdDetails.SmallAdId.ToString(), item, "Images");
                }
            }
            // workflow management for Created Status
            ManageWorkflowDetails(SmallAdId, 1, "", SmallAdDetails.CreatedBy);

          //  UpdateForeignKeyConstraints(SmallAdDetails.SmallAdId);

            return true;
        }

        public void UpdateForeignKeyConstraints(long SmallAdId)
        {
            // Execute SQL
           
                Acdcontext.Database.ExecuteSqlCommand("update SmallAdDetails  Set CityName = c.CityName, StateName = s.StateName, PostedForName = u.FirstName + ' ' + u.LastName FROM SmallAdDetails ad INNER JOIN    city c ON ad.CityId = c.CityId INNER JOIN state s on ad.StateId = s.StateId INNER JOIN UserDetails u on ad.postedfor = u.UserId");
             }

        //Create Directory If not exist
        public void ManageUploads(string path, string FileUrl, string UploadType)
        {
            string AdDirectory = "StaticFiles\\SmallAds\\" + path + "\\" + UploadType;
            if (!Directory.Exists(AdDirectory))
            {
                Directory.CreateDirectory(AdDirectory);
            }
            var SourcefolderName = Path.Combine("StaticFiles", "Images");
            var Sourcepath = Path.Combine(Directory.GetCurrentDirectory(), SourcefolderName);
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), AdDirectory);
            var SourceFile = Sourcepath + "\\" + FileUrl;
            var DestinationFile = pathToSave + "\\" + FileUrl;
            if (!File.Exists(pathToSave + FileUrl) && File.Exists(SourceFile))
            {
                File.Move(SourceFile, DestinationFile);
            }
        }
        // Insert Workflow Details
        public void ManageWorkflowDetails(long SmallAdId, int status, string Remark, long CreatedBy)
        {
            //SmallAdId, StatusId, remark, CreatedBy

            // CommonRepository cr = new CommonRepository();

            long PostDetailsId = GetPostDetailsId(SmallAdId, status, 6, CreatedBy); // 6 == posttype Small Ads
            if (PostDetailsId > 0)
            {


                PostStatusDetails postStatusDetails = new PostStatusDetails();
                postStatusDetails.PostDetailsId = PostDetailsId;
                postStatusDetails.IsActive = true;
                postStatusDetails.CreatedBy = CreatedBy;
                postStatusDetails.CreatedDate = DateTime.Now;
                postStatusDetails.Remark = Remark;
                if (status == 1)
                {
                    postStatusDetails.WorkflowStatusId = status;
                }

                else
                {
                    Acdcontext.PostStatusDetails.Where(x => x.PostDetailsId == postStatusDetails.PostDetailsId && x.IsActive == true).ToList().ForEach(x =>
                    {
                        x.IsActive = false; x.UpdatedDate = DateTime.Now;
                    });
                    postStatusDetails.WorkflowStatusId = status;
                }

                Acdcontext.PostStatusDetails.Add(postStatusDetails);
                Acdcontext.SaveChanges();
            }
            //InsertPostStatusDetails(postStatusDetails);
        }


        // Insert PostDetails
        public long GetPostDetailsId(long SmallAdId, int status, int PostTypeId, long CreatedBy)
        {
            long PostDetailsId = 0;
            PostDetails postDetails = new PostDetails();
            postDetails = Acdcontext.PostDetails.FirstOrDefault(e => e.PostId == SmallAdId && e.PostTypeId == PostTypeId && e.IsActive == true);
            if (postDetails == null)
            {
                PostDetails addpostDetails = new PostDetails();
                addpostDetails.PostId = SmallAdId;
                addpostDetails.PostTypeId = PostTypeId;
                addpostDetails.IsActive = true;
                addpostDetails.CreatedBy = CreatedBy;
                addpostDetails.CreatedDate = DateTime.Now;
                Acdcontext.PostDetails.Add(addpostDetails);
                Acdcontext.SaveChanges();
                PostDetailsId = addpostDetails.PostDetailsId;
            }
            else
                PostDetailsId = postDetails.PostDetailsId;
            return PostDetailsId;
        }

        public bool InsertAdStatusDetails(PostStatusDetails postStatusDetails)
        {

            return true;
        }







    }
}
