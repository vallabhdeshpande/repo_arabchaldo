﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore.Internal;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class AdImageDetailsRepository : IAdImageDetails
    {
        private Offshore_ArabchaldoContext Acdcontext;
        public AdImageDetailsRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }

        public bool CheckAdImageDetailsExits(string AdImageDetailsName)
        {
            //AdImageDetails AdImageDetailsDetails = Acdcontext.AdImageDetails.FirstOrDefault(e => e.AdImageDetailsName == AdImageDetailsName);
            //if (AdImageDetailsDetails != null)
            //{
            //    return true;
            //}

            return false;
        }
        public bool DeleteAdImageDetails(int AdImageDetailsID)
        {
            AdImageDetails AdImageDetailsDetails = Acdcontext.AdImageDetails.FirstOrDefault(e => e.AdImageDetailId == AdImageDetailsID);

            if (AdImageDetailsDetails != null)
            {
                AdImageDetailsDetails.IsActive = false;
                Acdcontext.AdImageDetails.Update(AdImageDetailsDetails);
                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }
        public bool UpdateAdImageDetails(int AdImageDetailsID, AdImageDetails updateAdImageDetailsDetails)
        {
            AdImageDetails AdImageDetailsDetails = Acdcontext.AdImageDetails.FirstOrDefault(e => e.AdImageDetailId == AdImageDetailsID);

            if (updateAdImageDetailsDetails != null)
            {

                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }
        public List<AdImageDetails> GetAllAdImageDetails()
        {
            List<AdImageDetails> allAdImageDetailsList = new List<AdImageDetails>();
            return Acdcontext.AdImageDetails.ToList();
        }

        public AdImageDetails GetAdImageDetailsById(int AdImageDetailsId)
        {
            return Acdcontext.AdImageDetails.FirstOrDefault(e => e.AdImageDetailId == AdImageDetailsId);
        }

        public bool InsertAdImageDetails(AdImageDetails AdImageDetailsdeatils)
        {
            Acdcontext.AdImageDetails.Add(AdImageDetailsdeatils);
            Acdcontext.SaveChanges();

            return true;
        }
    }
}
