﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore.Internal;
using System.IO;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class UserDetailsRepository : IUserDetails
    {
        private Offshore_ArabchaldoContext Acdcontext;

        public UserDetailsRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }

        public bool CheckUserExits(string EmailID)
        {
            try
            {
                UserDetails userDetails = Acdcontext.UserDetails.FirstOrDefault(e => e.Email == EmailID && e.IsActive==true);
                if (userDetails != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public string ThirdPartyUserName(long userId)
        {
            UserDetails userDetails = Acdcontext.UserDetails.SingleOrDefault(e => e.UserId == userId && e.IsActive == true);
            string name = userDetails.FirstName + " " + userDetails.LastName;
            return name;
        }

        public List<UserDetails> UsersForPostedFor(long id)
        {
            List<UserDetails> allUserList = new List<UserDetails>();

            //allUserList = Acdcontext.User.ToList();

            var abc = Acdcontext.UserDetails.Join(Acdcontext.UserRoles, ur => ur.UserId, ud => ud.UserId, (ur, ud) => new { ur });

            foreach (var items in abc)
            {
                //allUserList.Add(items);
            }

            return Acdcontext.UserDetails.Where(e => e.IsActive == true).OrderByDescending(n => n.UserId).ToList();
        }


        public bool DeleteUser(long Id)
        {
            UserDetails userDetails = Acdcontext.UserDetails.FirstOrDefault(e => e.UserId == Id && e.IsActive == true);
            UserRoles userRoleDetails = Acdcontext.UserRoles.FirstOrDefault(e => e.UserId == Id && e.IsActive == true);
            LoginDetails userLoginDetails = Acdcontext.LoginDetails.FirstOrDefault(e => e.UserId == Id && e.IsActive == true);

            if (userDetails != null)
            {
                userDetails.IsActive = false;
                userRoleDetails.IsActive = false;
                userLoginDetails.IsActive = false;
                userLoginDetails.IsActivated= false;

                Acdcontext.UserDetails.Update(userDetails);
                Acdcontext.UserRoles.Update(userRoleDetails);
                Acdcontext.LoginDetails.Update(userLoginDetails);
                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }
        public bool UpdateUsers(long Id, UserDetails updateDetails)
        {
            UserDetails userdetails = Acdcontext.UserDetails.FirstOrDefault(e => e.UserId == Id && e.IsActive == true);
            updateDetails.IsActive = true;
            if (userdetails != null)
            {
                userdetails.FirstName = updateDetails.FirstName;
                userdetails.LastName = updateDetails.LastName;
                userdetails.Gender = updateDetails.Gender;
                if (updateDetails.ProfilePicUrl != null && (userdetails.ProfilePicUrl != updateDetails.ProfilePicUrl))
                {
                    ManageUploads(userdetails.UserId.ToString(), updateDetails.ProfilePicUrl, "ProfilePic");

                }
                userdetails.ProfilePicUrl = updateDetails.ProfilePicUrl;
                userdetails.ContactNumber = updateDetails.ContactNumber;
                userdetails.AlternateContactNumber = updateDetails.AlternateContactNumber;
                userdetails.FaxNumber = updateDetails.FaxNumber;
                userdetails.Email = updateDetails.Email;
                userdetails.RoleName = updateDetails.RoleName;
                userdetails.AddressStreet1 = updateDetails.AddressStreet1;
                userdetails.AddressStreet2 = updateDetails.AddressStreet2;
                userdetails.CountryId = updateDetails.CountryId;
                userdetails.StateId = updateDetails.StateId;
                userdetails.StateName = updateDetails.StateName;
                userdetails.CityName = updateDetails.CityName;
               
                if (updateDetails.CityId != 0)
                {
                    userdetails.CityId = updateDetails.CityId;
                    Acdcontext.Database.ExecuteSqlCommand("update UserDetails Set CityName=c.CityName,statename=s.StateName from UserDetails u Inner join dbo.city c on u.CityId=c.CityId and  u.CityId is not NULL Inner join dbo.State s on u.StateId=s.StateId where u.CityName is null");
                }
                if (updateDetails.CityId == 0 || updateDetails.CityId == null)
                    userdetails.CityName = "";
                if (updateDetails.StateId == 0 || updateDetails.StateId == null)
                    userdetails.StateName = "";
                userdetails.ZipCode = updateDetails.ZipCode;
                userdetails.IsActive = updateDetails.IsActive;
                userdetails.CreatedDate = updateDetails.CreatedDate;
                userdetails.CreatedBy = updateDetails.CreatedBy;
                userdetails.UpdatedDate = updateDetails.UpdatedDate;
                userdetails.Description = updateDetails.Description;
                userdetails.UpdatedBy = updateDetails.UpdatedBy;
                //Acdcontext.UserDetails.Update(updateDetails);
                try
                {
                    Acdcontext.SaveChanges();
                    
                }
        catch(Exception ex)
                {
                    throw ex;
                }

                return true;
            }

            return false;
        }


        public DataTable GetAllUser()
        {
            List<UserDetails> allUserList = new List<UserDetails>();
            Acdcontext.Database.ExecuteSqlCommand("update UserDetails Set CityName=c.CityName,statename=s.StateName from UserDetails u Inner join dbo.city c on u.CityId=c.CityId and  u.CityId is not NULL Inner join dbo.State s on u.StateId=s.StateId where u.CityName is null");
            //allUserList = Acdcontext.UserDetails.Where(e => e.IsActive == true && e.Email != "sa@sa.com" && e.Email != "admin@admin.com").ToList();
            //allUserList = Acdcontext.UserDetails.Where(e => e.IsActive == true && e.Email != "sa@sa.com" && e.Email != "admin@admin.com").Select(s => new UserDetails()
            //{ UserId = s.UserId, FirstName = s.FirstName, LastName = s.LastName,ProfilePicUrl=s.ProfilePicUrl,Email=s.Email,RoleName=s.RoleName,
            //CityId=s.CityId,CityName=s.CityName}).ToList();
            allUserList =(from u in Acdcontext.UserDetails
                          join ur in Acdcontext.UserRoles
                          on u.UserId equals ur.UserId 
                          join r in Acdcontext.Roles
                          on ur.RoleId equals r.RoleId where u.IsActive==true && u.Email != "sa@sa.com" && u.Email != "admin@admin.com"
                          select new UserDetails
                          {UserId = u.UserId,FirstName = u.FirstName,LastName = u.LastName,ProfilePicUrl = u.ProfilePicUrl,Email = u.Email,RoleName = r.RoleName,CityId =u.CityId,CityName=u.CityName}).ToList();
            DataTable dtuserDetails = new DataTable();
            dtuserDetails.Columns.Add("userId", typeof(long));
            dtuserDetails.Columns.Add("firstName", typeof(string));
            dtuserDetails.Columns.Add("lastName", typeof(string));
            dtuserDetails.Columns.Add("roleName", typeof(string));
            dtuserDetails.Columns.Add("email", typeof(string));
            dtuserDetails.Columns.Add("cityId", typeof(long));
            dtuserDetails.Columns.Add("cityName", typeof(string));
            dtuserDetails.Columns.Add("profilePicUrl", typeof(string));
            foreach (var item in allUserList)
            {
                try
                {
                    dtuserDetails.Rows.Add(item.UserId, item.FirstName, item.LastName, item.RoleName, item.Email, item.CityId, item.CityName, item.ProfilePicUrl);
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }
            dtuserDetails.AcceptChanges();
            return dtuserDetails;
        }


        public List<UserDetails> GetUsersForThirdParty()
        {
            // update with lambda expression
            List<UserDetails> allUserList = new List<UserDetails>();
            return Acdcontext.UserDetails.Where(e => e.IsActive == true).ToList();

            //    var dropdownList = from users in _Offshore_ArabchaldoContext.UserDetails
            //                        join role in _Offshore_ArabchaldoContext.UserRoles
            //                        on users.UserId equals role.UserId
            //                        where role.RoleId == 4
            //                        select new
            //                        {
            //                            users.FirstName,
            //                            users.LastName,
            //                            role.RoleId
            //                        };
        }

        public UserDetails GetUserbyId(long UserId)
        {
            return Acdcontext.UserDetails.FirstOrDefault(e => e.UserId == UserId && e.IsActive == true);
        }

        public UserDetails GetUserbyEmail(string Email)
        {
            return Acdcontext.UserDetails.FirstOrDefault(e => e.Email == Email && e.IsActive == true);
        }
        //private Roles roles;
        public bool InsertUser(UserDetails userDetails)
        {
            if(userDetails.CityId != 0)
            {
                Acdcontext.Database.ExecuteSqlCommand("update UserDetails Set CityName=c.CityName,statename=s.StateName from UserDetails u Inner join dbo.city c on u.CityId=c.CityId and  u.CityId is not NULL Inner join dbo.State s on u.StateId=s.StateId where u.CityName is null");
            }
            else
            {
                userDetails.CityId = null;
            }
            if (userDetails.Provider != null)
            {
                userDetails.IsSocialMedia = true;
                userDetails.Provider = userDetails.Provider;
            }
            userDetails.CreatedDate = DateTime.Now;
            userDetails.UpdatedDate = DateTime.Now;
            userDetails.IsActive = true;
            Acdcontext.UserDetails.Add(userDetails);            
            Acdcontext.SaveChanges();



           
            long UserId = userDetails.UserId;
            // updating Created By in UserDetails
            userDetails.CreatedBy = UserId;
            userDetails.UpdatedBy = UserId;
            userDetails.UserId = UserId;
            Acdcontext.UserDetails.Update(userDetails);
            Acdcontext.SaveChanges();



            //Save image in directory
            if (userDetails.ProfilePicUrl != null && userDetails.IsSocialMedia==null)
                ManageUploads(UserId.ToString(), userDetails.ProfilePicUrl, "ProfilePic");
            //save login details 
            if (userDetails.IsSocialMedia !=null)
            {
                ManageUserLogInDetails(userDetails, UserId);
            }
            UserRoles userRoles = new UserRoles();
            if(userRoles.RoleId == 0)
            {
                userRoles.RoleId = 8;
            }            
            userRoles.UserId =UserId;
            userRoles.IsActive = true;
            userRoles.CreatedDate = DateTime.Now;
            userRoles.CreatedBy = UserId;
            userRoles.UpdatedDate = DateTime.Now;
            userRoles.UpdatedBy = UserId;
            Acdcontext.UserRoles.Add(userRoles);
            Acdcontext.SaveChanges();



            return true;

        }

        // Insert Workflow Details
        public void ManageUserLogInDetails(UserDetails userDetails, long userId)
        {
            LoginDetails LoginExist = Acdcontext.LoginDetails.FirstOrDefault(e => e.Email == userDetails.Email && e.IsActive==true);
            if (LoginExist==null)
            {
                LoginDetails loginDetails = new LoginDetails();
                loginDetails.UserId = userId;
                loginDetails.IsActive = true;
                loginDetails.Email = userDetails.Email;

                loginDetails.Password = EncryptText("Admin1234");
                loginDetails.CreatedBy = userId;
                loginDetails.CreatedDate = DateTime.Now;
                loginDetails.UpdatedBy = userId;
                loginDetails.UpdatedDate = DateTime.Now;
                loginDetails.ActivationCode = EncryptText(loginDetails.Password);
                Acdcontext.LoginDetails.Add(loginDetails);
                Acdcontext.SaveChanges();
            }
            
            //InsertPostStatusDetails(postStatusDetails);
        }
        // Insert Workflow Details
        public void ManageUserRolesDetails(UserDetails userDetails, long userId)
        {
            UserRoles userRoleDetails = new UserRoles();
            userRoleDetails.UserId = userId;
            userRoleDetails.IsActive = true;
            // userRoleDetails.RoleId = userDetails.RoleName;
            userRoleDetails.CreatedBy = userId;
            userRoleDetails.CreatedDate = DateTime.Now;
            userRoleDetails.UpdatedBy = userDetails.UpdatedBy;
            userRoleDetails.UpdatedDate = DateTime.Now;
            Acdcontext.UserRoles.Add(userRoleDetails);
            Acdcontext.SaveChanges();
            //InsertPostStatusDetails(postStatusDetails);
        }
        #region Delete




        //public bool InsertEmployee(Employee employee)
        //{


        //    return true;
        //}

        //public bool CheckEmployeeExits(string Name)
        //{
        //    return Acdcontext.Employee.Where(o => o.Name == Name).Any();

        //}

        //public Employee GetEmployeebyId(int ID)
        //{

        //    return Acdcontext.Employee.Where(o => o.Id == ID).FirstOrDefault();

        //}

        //public bool InsertEmployee()
        //{
        //    throw new NotImplementedException();
        //}

        //IEmployee IEmployee.GetEmployeebyId(int ID)
        //{
        //    throw new NotImplementedException();
        //}

        //bool CheckEmployeeExits(string Name)
        //{
        //    return Acdcontext.Employee.Where(o => o.Name == Name).Any();
        //}

        #endregion

        #region Imageupload

        //Create Directory If not exist
        public void ManageUploads(string path, string FileUrl, string UploadType)
        {
            if (FileUrl!="")
            {
                string AdDirectory = "StaticFiles\\UserProfile\\" + path + "\\" + UploadType;
                if (!Directory.Exists(AdDirectory))
                {
                    Directory.CreateDirectory(AdDirectory);
                }
                var SourcefolderName = Path.Combine("StaticFiles", "Images");
                var Sourcepath = Path.Combine(Directory.GetCurrentDirectory(), SourcefolderName);
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), AdDirectory);
                var SourceFile = Sourcepath + "\\" + FileUrl;
                var DestinationFile = pathToSave + "\\" + FileUrl;
                try
                {
                    if (!File.Exists(pathToSave + FileUrl))
                    {
                        File.Move(SourceFile, DestinationFile);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            
            
        }
         public string EncryptText(string password)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(password);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        //public bool SocialloginInfo(UserDetails userDetails)
        //{
        //    try
        //    {
        //        if (Acdcontext.LoginDetails.SingleOrDefault(e => e.Email == userDetails.Email) == null)
        //        {
        //            return false;
        //        }
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }


        //}
        #endregion




    }
}
