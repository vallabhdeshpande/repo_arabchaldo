﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class StateRepository : IState
    {
        private Offshore_ArabchaldoContext Acdcontext;
        public StateRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }

        public bool CheckStateExits(string StateName,long StateId)
        {
            State StateDetails = Acdcontext.State.FirstOrDefault(e => e.StateName == StateName && e.IsActive==true && e.StateId!= StateId);
            if (StateDetails != null)
            {
                return true;
            }

            return false;
        }
        public bool DeleteState(int StateID)
        {
            State StateDetails = Acdcontext.State.FirstOrDefault(e => e.StateId == StateID);
            City CityDetails= Acdcontext.City.FirstOrDefault(e => e.StateId == StateID && e.IsActive==true);

            if (StateDetails != null)
            {
                if (CityDetails == null)
                {
                                 StateDetails.IsActive = false;
                                Acdcontext.State.Update(StateDetails);
                                Acdcontext.SaveChanges();
                                return true;
                }
                else
                    return false;

            }

            return false;
        }
        public bool UpdateState(int StateID, State updateStateDetails)
        {
            State StateDetails = Acdcontext.State.FirstOrDefault(e => e.StateId == StateID);

            if (updateStateDetails != null)
            {

                StateDetails.StateId = updateStateDetails.StateId;
                StateDetails.StateName = updateStateDetails.StateName;
                StateDetails.City = updateStateDetails.City;
                StateDetails.Country = updateStateDetails.Country;
                StateDetails.Description = updateStateDetails.Description;
                StateDetails.StateId = updateStateDetails.StateId;
                StateDetails.IsActive = true;
                StateDetails.CreatedDate = updateStateDetails.CreatedDate;
                StateDetails.CreatedBy = updateStateDetails.CreatedBy;
                StateDetails.UpdatedDate = updateStateDetails.UpdatedDate;
                StateDetails.UpdatedDate = DateTime.Now;

                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }

       


           

      
        public List<State> GetAllState()
        {   
            List<State> allStateList = new List<State>();
            return Acdcontext.State.Where(e => e.IsActive == true).OrderBy(e => e.UpdatedDate).ToList();
         
        }

        public List<State> GetAllStatesByCountryId(int countryId)
        {
            List<State> allStateList = new List<State>();
            FillStateDDLByCountryId(countryId);
            return Acdcontext.State.Where(e => e.CountryId == countryId).Where(e => e.IsActive == true).ToList();

        }

        public DataTable FillStateDDLByCountryId(int countryId)
        {

            List<StateDdl> allStateDdlList = new List<StateDdl>();
    
            //var dataResult = (from s in Acdcontext.State
            //                  where s.IsActive == true && s.CountryId == countryId
            //                  select new { StateId = s.StateId, StateName = s.StateName }).ToList();

            allStateDdlList =Acdcontext.State.Where(e => e.CountryId == countryId && e.IsActive == true).Select(s => new StateDdl()
            {
                 
                stateId = s.StateId,
                stateName = s.StateName
            }).ToList();



            DataTable dtState = new DataTable();
            dtState.Columns.Add("stateId", typeof(long));
            dtState.Columns.Add("stateName", typeof(string));
            foreach (var item in allStateDdlList)
            {
                dtState.Rows.Add(item.stateId, item.stateName);
            }
            dtState.AcceptChanges();
            return dtState;

        }

        public class StateDdl
        {
            public long stateId { get; set; }
            public string stateName { get; set; }
        }


        public State GetStateById(int StateId)
        {
            return Acdcontext.State.FirstOrDefault(e => e.StateId == StateId);
        }

        public bool InsertState(State Statedeatils)
        {
            Statedeatils.IsActive = true;
            Statedeatils.UpdatedDate = DateTime.Now;
            Statedeatils.CreatedBy = 1;
            Statedeatils.CreatedDate = DateTime.Now;
            Acdcontext.State.Add(Statedeatils);
            Acdcontext.SaveChanges();
            
            return true;
        }

        #region Grid

        // State
        public class StateList
        {
            public long stateId { get; set; }
            public long countryId { get; set; }
            public string stateName { get; set; }
            public string countryName { get; set; }
            public string description { get; set; }
        }
        public DataTable FillStateList()
        {
            List<Offshore_ArabchaldoContext.AllStatelist> stateList = new List<Offshore_ArabchaldoContext.AllStatelist>();
            stateList = Acdcontext.ssp_GetAllStateListDetails();


            //List<StateList> stateList = new List<StateList>();
            //stateList = (from s in Acdcontext.State
            //             join cnt in Acdcontext.Country
            //             on s.CountryId equals cnt.CountryId
            //             where s.IsActive == true
            //             orderby s.UpdatedDate ascending
            //             select new StateList { stateId = s.StateId, stateName = s.StateName,countryName = cnt.CountryName, description = s.Description }).ToList();

            DataTable dtstateList = new DataTable();
            dtstateList.Columns.Add("stateId", typeof(long));
            dtstateList.Columns.Add("countryId", typeof(long));
            dtstateList.Columns.Add("stateName", typeof(string));
            dtstateList.Columns.Add("countryName", typeof(string));
            dtstateList.Columns.Add("description", typeof(string));
            dtstateList.Columns.Add("isActive", typeof(bool));
            foreach (var item in stateList)
            {
                dtstateList.Rows.Add(item.stateId, item.countryId, item.stateName, item.countryName,item.description,item.isActive);
            }
            dtstateList.AcceptChanges();
            return dtstateList;
        }


        #endregion



    }
}
