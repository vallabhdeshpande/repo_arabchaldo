﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore;


using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class LoginDetailsRepository : ILoginDetails
    {
        private Offshore_ArabchaldoContext Acdcontext;
        private EncryptDecrypt encdec;

        string userPassword = "";
        public LoginDetailsRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }

        public LoginDetails GetLogin(string UserName, string Password)
        {
            string encpwd = EncryptText(Password);
            

            return Acdcontext.LoginDetails.FirstOrDefault(e => e.Email ==  UserName && e.Password== encpwd && e.IsActive == true );  
        }

        public bool RegisterUser(LoginDetails registerNewUser)
        {
            LoginDetails LoginExist = Acdcontext.LoginDetails.FirstOrDefault(e => e.Email == registerNewUser.Email && e.IsActive == true);
            if (LoginExist == null)
            {
                registerNewUser.Password = EncryptText(registerNewUser.Password);
                //registerNewUser.Password = registerNewUser.Password;
                Acdcontext.LoginDetails.Add(registerNewUser);
                Acdcontext.SaveChanges();
                return true;
            }
            else
                return false;

           
        }

        


        public string GetLoginInfo(string UserName, string Password)
        {
            string encpwd = EncryptText(Password);
            // return Acdcontext.LoginDetails.FirstOrDefault(e => e.Email == UserName && e.IsActive == true);

            var LoggedInUserInfo = (from u in Acdcontext.UserDetails
                                   join ur in Acdcontext.UserRoles on u.UserId equals ur.UserId
                                   join l in Acdcontext.LoginDetails on u.UserId equals l.UserId
                                   join r in Acdcontext.Roles on ur.RoleId equals r.RoleId
                                   where l.Email == UserName && u.IsActive == true && l.Password == encpwd 
                                    select new {FirstName=u.FirstName,LastName=u.LastName,Email=l.Email,RoleId=r.RoleId,RoleName=r.RoleName,UserId=u.UserId,ProfilePic=u.ProfilePicUrl,VerifiedUser=l.IsActivated }).ToList();


            //LoginDetails ld = new LoginDetails();
            //ld = Acdcontext.LoginDetails.FirstOrDefault(e => e.Email == UserName && e.IsActive == true);
            string LoggedInUserInfoJson = "";
            if (LoggedInUserInfo!=null)
            {
                foreach (var item in LoggedInUserInfo)
                {
                    LoggedInUserInfoJson = item.FirstName + ":" + item.LastName + ":" + item.UserId + ":" + item.Email + ":" + item.RoleId + ":" + item.RoleName + ":" + item.ProfilePic + ":" + item.VerifiedUser;
                }
            }


            return LoggedInUserInfoJson;


        }
        public List<Roles> GetAllRole()
        {
            return Acdcontext.Roles.Where(e => e.IsActive == true).ToList();
        }

        public UserRoles GetUserRole(long UserId)
        {
            return Acdcontext.UserRoles.FirstOrDefault(e => e.IsActive == true && e.UserId == UserId);
        }

        public UserDetails GetUserDetailsById(long UserId)
        {
            return Acdcontext.UserDetails.FirstOrDefault(e => e.IsActive == true && e.UserId == UserId);
        }

        


        // Encrypt String
        public string EncryptText(string password)
        {
            if (password==null)
            {
                password = "123456";
            }
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(password);
            return System.Convert.ToBase64String(plainTextBytes);
        }


        // Decrypt String

        public string DecryptText(string encryptedText)
        {            
            var base64EncodedBytes = System.Convert.FromBase64String(encryptedText);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public LoginDetails GetLoginByUserID(long UserId)
        {
            return Acdcontext.LoginDetails.FirstOrDefault(e => e.UserId == UserId);

        }
       
        public bool ResetPassword(long userId, LoginDetails updatedDetails)
        {
            LoginDetails userdetails = Acdcontext.LoginDetails.FirstOrDefault(e => e.UserId == userId);
            updatedDetails.IsActive = true;
            if(userdetails.Password== EncryptText(updatedDetails.Password))
            {
                return false;
            }
            else
            {
                if (userdetails != null)
                {
                    userdetails.Email = updatedDetails.Email;
                    userdetails.Password = EncryptText(updatedDetails.Password);
                    userdetails.CreatedBy = updatedDetails.CreatedBy;
                    //Acdcontext.UserDetails.Update(updateDetails);
                    Acdcontext.SaveChanges();

                   
                }

                return true;
            }

        }

        public bool ActivateUserAccount(string Info)

        {
            if (Info!="undefined")
            {
                string userEmail = Info.Split(',')[0];
                string activationCode = Info.Split(',')[1];
                LoginDetails logindetails = Acdcontext.LoginDetails.FirstOrDefault(e => e.Email == userEmail && e.IsActive == true && e.ActivationCode == activationCode);

                if (logindetails != null)
                {

                    logindetails.IsActivated = true;

                    Acdcontext.LoginDetails.Update(logindetails);
                    Acdcontext.SaveChanges();

                    return true;
                }
                else
                    return false;

            }
            else
            return false;
        }
        //public LoginDetails GetLogin(string UserName,string Password)
        //{
        //   return  Acdcontext.LoginDetails.FirstOrDefault(e => e.Email == UserName);

        //}

        public bool CheckUserRegistered(string registerEmail)
        {
            if (Acdcontext.LoginDetails.SingleOrDefault(e => e.Email == registerEmail) == null)
            {
                return false;
            }
            return true;
        }

        #region ForgotPassword

        public class UserInfoList
        {       
          
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public long UserId { get; set; }
            public string EncUserId { get; set; }
        }

        public DataTable GetUserInfoByEmail(string email)
        {

            // return Acdcontext.LoginDetails.FirstOrDefault(e => e.Email == UserName && e.IsActive == true);
            List<UserInfoList> UserInfo = new List<UserInfoList>();
            UserInfo = (from u in Acdcontext.UserDetails
                                   join l in Acdcontext.LoginDetails on u.UserId equals l.UserId
                                   where l.Email == email && u.IsActive == true
                                   select new UserInfoList { FirstName = u.FirstName, LastName = u.LastName, Email = l.Email, UserId = u.UserId, EncUserId = u.UserId.ToString() }).ToList();


            DataTable dtUserInfoList = new DataTable();
            dtUserInfoList.Columns.Add("FirstName", typeof(string));
            dtUserInfoList.Columns.Add("LastName", typeof(string));
            dtUserInfoList.Columns.Add("Email", typeof(string));
            dtUserInfoList.Columns.Add("UserId", typeof(long));
            dtUserInfoList.Columns.Add("EncUserId", typeof(string));
           
           
           

         //   return UserInfoByEmailJson;
            foreach (var item in UserInfo)
            {
                dtUserInfoList.Rows.Add(item.FirstName, item.LastName, item.Email, item.UserId, item.EncUserId);
            }
            dtUserInfoList.AcceptChanges();
            return dtUserInfoList;


        }

#endregion

    }
}
