﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class AdDetailsRepository : IAdDetails
    {
        private Offshore_ArabchaldoContext Acdcontext;
        public AdDetailsRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }

        public bool CheckAdDetailsExits(string Title, long CategoryId, long AdId)
        {
            //AdDetails AdDetailsDetails = Acdcontext.AdDetails.AsNoTracking().FirstOrDefault(e => e.Title == Title && e.CategoryId == CategoryId && e.AdId != AdId && e.IsActive == true);
            ////Acdcontext.AdDetails.ExecuteSqlCommand("updateAdDetails");
            //// var cmd = Acdcontext.CreateCommand();
            //if (AdDetailsDetails != null)
            //{
            //    return true;
            //}

            return false;
        }

        // Delete, Approve and Reject
        public bool AdStatus(long AdId, string Remark, string Action, long CreatedBy)
        {
            AdDetails AdDetails = Acdcontext.AdDetails.FirstOrDefault(e => e.AdId == AdId && e.IsActive == true);
            int StatusId = 0;
            bool isActive = true;
            bool isVisible = true;
            string remark = "";
            if (AdDetails != null && Action == "Delete")
            {
                if (AdDetails.IsVisible == true)
                    return false;
                else
                {
                    isActive = false;
                    isVisible = false;
                    StatusId = 6;
                }

                //remark = Remark.Split(',')[2];
            }
            else if (AdDetails != null && Action == "Approve")
            {
                isActive = true;
                isVisible = true;
                StatusId = 3;
                remark = Remark.Split(',')[2];
                AdDetails.ValidFromDate = Convert.ToDateTime(Remark.Split(',')[0]);
                AdDetails.ValidTillDate = Convert.ToDateTime(Remark.Split(',')[1]);
                AdDetails.IsFeatured = Convert.ToBoolean(Remark.Split(',')[3]);
            }
            else if (AdDetails != null && Action == "Reject")
            {
                isActive = true;
                isVisible = false;
                StatusId = 4;
                remark = Remark.Split(',')[2];
            }

            AdDetails.IsActive = isActive;
            AdDetails.IsVisible = isVisible;
            AdDetails.UpdatedDate = DateTime.Now;
            AdDetails.UpdatedBy = CreatedBy;
            Acdcontext.AdDetails.Update(AdDetails);
            Acdcontext.SaveChanges();
            //AdDetails.CreatedBy = CreatedBy;
            //ManageWorkflowDetails(AdDetails, StatusId, remark);
            ManageWorkflowDetails(AdId, StatusId, remark, CreatedBy);
            Acdcontext.SaveChanges();
            return true;


        }

        public bool UpdateAdDetails(long AdId, AdDetails updateAdDetails)
        {

            Acdcontext.Entry(updateAdDetails).State = EntityState.Detached;

            AdDetails AdDetailsDetails = Acdcontext.AdDetails.FirstOrDefault(e => e.AdId == AdId && e.IsActive == true);
            // AdDetails AdDetailsDetails = new AdDetails();

            if (AdDetailsDetails != null)
            {
                AdDetailsDetails.Title = updateAdDetails.Title;
                AdDetailsDetails.TagLine = updateAdDetails.TagLine;
                AdDetailsDetails.Description = updateAdDetails.Description;
                AdDetailsDetails.ServicesOffered = updateAdDetails.ServicesOffered;
                AdDetailsDetails.PostedForName = updateAdDetails.PostedForName;
                AdDetailsDetails.PostedFor = updateAdDetails.PostedFor;
                AdDetailsDetails.CategoryName = updateAdDetails.CategoryName;
                AdDetailsDetails.CategoryId = updateAdDetails.CategoryId;
                AdDetailsDetails.SubCategoryName = updateAdDetails.SubCategoryName;
                AdDetailsDetails.SubCategoryId = updateAdDetails.SubCategoryId;
                AdDetailsDetails.AdLogoUrl = updateAdDetails.AdLogoUrl;
                AdDetailsDetails.ImageUrl = updateAdDetails.ImageUrl;
                AdDetailsDetails.ContactPersonName = updateAdDetails.ContactPersonName;
                AdDetailsDetails.ContactNumber = updateAdDetails.ContactNumber;
                AdDetailsDetails.AlternateContactNumber = updateAdDetails.AlternateContactNumber;
                AdDetailsDetails.FaxNumber = updateAdDetails.FaxNumber;
                AdDetailsDetails.Email = updateAdDetails.Email;
                if(updateAdDetails.AddressStreet1==null)
                AdDetailsDetails.AddressStreet1 = "";
                else
                    AdDetailsDetails.AddressStreet1 = updateAdDetails.AddressStreet1;
                if (updateAdDetails.AddressStreet2 == null)
                    AdDetailsDetails.AddressStreet2 = "";
                else
                    AdDetailsDetails.AddressStreet2 = updateAdDetails.AddressStreet2;
                AdDetailsDetails.CountryId = updateAdDetails.CountryId;
                AdDetailsDetails.StateId = updateAdDetails.StateId;
                AdDetailsDetails.CityId = updateAdDetails.CityId;
                AdDetailsDetails.StateName = updateAdDetails.StateName;

                if (updateAdDetails.CityId == 9999)
                    AdDetailsDetails.CityName = "Other";
                else
                    AdDetailsDetails.CityName = updateAdDetails.CityName;
                AdDetailsDetails.OtherCity = updateAdDetails.OtherCity;
                AdDetailsDetails.AssignedTo = updateAdDetails.AssignedTo;
                AdDetailsDetails.IsFeatured = updateAdDetails.IsFeatured;
                AdDetailsDetails.IsPaidAd = updateAdDetails.IsPaidAd;
                if (updateAdDetails.ZipCode == null)
                    AdDetailsDetails.ZipCode = "";
                else
                    AdDetailsDetails.ZipCode = updateAdDetails.ZipCode;
                AdDetailsDetails.ValidFromDate = null;
                AdDetailsDetails.ValidTillDate = null;
                AdDetailsDetails.CountryCodeContact = updateAdDetails.CountryCodeContact;
                AdDetailsDetails.UpdatedDate = DateTime.Now;
                AdDetailsDetails.UpdatedBy = updateAdDetails.CreatedBy;
                AdDetailsDetails.IsVisible = null;
                
                if (updateAdDetails.CityId == 0 || updateAdDetails.CityId == null)
                    AdDetailsDetails.CityName = "";
                if (updateAdDetails.StateId == 0 || updateAdDetails.StateId == null)
                    AdDetailsDetails.StateName = "";
                if (updateAdDetails.SubCategoryId == 0 || updateAdDetails.SubCategoryId == null)
                    AdDetailsDetails.SubCategoryName = "";
                //AdDetailsDetails.StateName = updateAdDetails.StateName;
                //AdDetailsDetails.CityName = updateAdDetails.CityName;
                Acdcontext.AdDetails.Update(AdDetailsDetails);
                Acdcontext.SaveChanges();
                // Update images

                if (AdDetailsDetails.AdLogoUrl != null)
                    ManageUploads(AdId.ToString(), AdDetailsDetails.AdLogoUrl, "Logo");
                // Delete all images for a AdId
                if (AdDetailsDetails.ImageUrl != null)
                {
                    //var adimgdtls = Acdcontext.AdImageDetails.Where(x => x.AdId == AdDetailsDetails.AdId && x.IsActive == true).ToList();

                    //List<string> imglist = new List<string>();

                    //foreach (var item in adimgdtls)
                    //{
                    //    imglist.Add(item.AdImageUrl);
                    //}


                    //
                    //var matchImages = Images.Intersect(imglist);
                    var Images = new List<string>(AdDetailsDetails.ImageUrl.Split(',')).ToList();
                    //  Insert updated Images
                    foreach (var item in Images)
                    {

                        AdImageDetails chkimgexist = Acdcontext.AdImageDetails.FirstOrDefault(e => e.AdImageUrl == item && e.AdId == AdDetailsDetails.AdId && e.IsActive == true);
                        if (chkimgexist == null)
                        {
                            AdImageDetails adimages = new AdImageDetails();
                            adimages.AdId = AdDetailsDetails.AdId;
                            adimages.AdImageUrl = item;
                            adimages.IsActive = true;
                            adimages.CreatedBy = AdDetailsDetails.CreatedBy;
                            adimages.CreatedDate = DateTime.Now;
                            Acdcontext.AdImageDetails.Add(adimages);
                            Acdcontext.SaveChanges();
                            if (item != null)
                                ManageUploads(AdDetailsDetails.AdId.ToString(), item, "Images");
                        }
                    }


                }
                // Checking the current state is Rejected
                long PostDetailsId = GetPostDetailsId(AdId, 4, 1, AdDetailsDetails.CreatedBy); // 1 == posttype Ads
                PostStatusDetails CheckRejected = Acdcontext.PostStatusDetails.FirstOrDefault(e => e.PostDetailsId == PostDetailsId && e.IsActive == true && e.WorkflowStatusId == 4);
                if (CheckRejected != null)
                {
                    ManageWorkflowDetails(AdId, 5, "", updateAdDetails.CreatedBy); // Rectified
                }
                else
                    ManageWorkflowDetails(AdId, 2, "", updateAdDetails.CreatedBy); //Modified
                Acdcontext.SaveChanges();
                UpdateForeignKeyConstraints(AdDetailsDetails.AdId, AdDetailsDetails.SubCategoryId);

                return true;
            }

            return false;
        }
        public List<AdDetails> GetAllAdDetails(long loggedInUserId, string loggedInRole)
        {
            AdDetails adDetails = new AdDetails();
            List<AdDetails> allAdDetailsList = new List<AdDetails>();
            try
            {
                Acdcontext.Database.ExecuteSqlCommand("update AdDetails  Set CategoryName=ct.CategoryName, PostedForName = u.FirstName + ' ' + u.LastName FROM AdDetails ad INNER JOIN  Category ct ON ad.CategoryId = ct.CategoryId INNER JOIN UserDetails u on ad.postedfor = u.UserId");
                Acdcontext.Database.ExecuteSqlCommand("exec [dbo].[updateAdDetails]");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //  allAdDetailsList = Acdcontext.ssp_GetAllAds(loggedInRole,"", loggedInUserId);


            if (loggedInRole == "SuperAdmin" || loggedInRole == "Admin")  //&& e.ValidTillDate >= DateTime.Today
            {
                //allAdDetailsList = Acdcontext.AdDetails.Where(e => e.IsActive == true && e.CategoryName != "Jobs").OrderBy(n => n.UpdatedDate).ToList();
                allAdDetailsList = (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.CategoryName != "Jobs" && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
                    (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.CategoryName != "Jobs" && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).Concat
                    (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.CategoryName != "Jobs" && e.IsVisible == false).ToList()).OrderBy(n => n.UpdatedDate).ToList());
            }

            else
            {
                allAdDetailsList = (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName != "Jobs" && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
                    (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName != "Jobs" && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).Concat
                    (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName != "Jobs" && e.IsVisible == false).ToList()).OrderBy(n => n.UpdatedDate).ToList());
                // allAdDetailsList = Acdcontext.AdDetails.Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName != "Jobs").OrderBy(n => n.UpdatedDate).ToList();
                //allAdDetailsList = (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName == "Jobs" && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
                //     (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName == "Jobs" && e.IsVisible != null && e.ValidTillDate >= DateTime.Today).ToList()).OrderBy(n => n.UpdatedDate).ToList());
            }


            return allAdDetailsList;



            //var query = (from ad in Acdcontext.AdDetails
            //             join cnt in Acdcontext.Country on ad.CountryId equals cnt.CountryId
            //             join st in Acdcontext.State on ad.StateId equals st.StateId
            //             join ct in Acdcontext.City on ad.CityId equals ct.CityId
            //             join ctg in Acdcontext.Category on ad.CategoryId equals ctg.CategoryId
            //             join usd in Acdcontext.UserDetails on ad.PostedFor equals usd.UserId

            //             orderby ad.AdId descending
            //             select new
            //             {
            //                 ad.AdId,
            //                 ad.Title,
            //                 ad.TagLine,
            //                 ad.AdLogoUrl,
            //                 ad.PostedFor,
            //                 ad.ServicesOffered,
            //                 ad.Website,
            //                 ad.ZipCode,
            //                 ad.AddressStreet1,
            //                 ad.AddressStreet2,
            //                 ad.CityId,
            //                 ad.CountryId,
            //                 ad.StateId,
            //                 ad.FaxNumber,
            //                 ad.ContactNumber,
            //                 ad.AlternateContactNumber,
            //                 ad.ContactPersonName,
            //                 ad.CategoryId,
            //                 ad.SubCategoryId,
            //                 ad.CreatedBy,
            //                 ad.IsActive,
            //                 ad.IsVisible,
            //                  cnt.CountryName,
            //                 st.StateName,
            //                 ct.CityName,
            //                 ctg.CategoryName,
            //                 usd.FirstName,
            //                 usd.LastName
            //             }).AsEnumerable().ToList();

            //foreach (var item in Acdcontext.AdDetails.ToList())
            //{
            //    var country = Acdcontext.Country.Where(x => x.CountryId == item.CountryId);
            //    foreach (var ctry in country)
            //    {
            //        ctry.
            //    }
            //}

            //var query = Acdcontext.AdDetails.Include(c => c.City).Include(ctg => ctg.Category).Include(u => u.UserDetails).ToList();


            //return query;

        }

        public List<AdDetails> GetAllJobDetails(long loggedInUserId, string loggedInRole)
        {
            AdDetails adDetails = new AdDetails();
            List<AdDetails> allAdDetailsList = new List<AdDetails>();
            try
            {
                Acdcontext.Database.ExecuteSqlCommand("update AdDetails  Set CategoryName=ct.CategoryName, PostedForName = u.FirstName + ' ' + u.LastName FROM AdDetails ad INNER JOIN  Category ct ON ad.CategoryId = ct.CategoryId INNER JOIN UserDetails u on ad.postedfor = u.UserId");
                Acdcontext.Database.ExecuteSqlCommand("exec [dbo].[updateAdDetails]");
            }
            catch (Exception ex)
            {
                throw ex;
            }



            //if (loggedInRole == "SuperAdmin" || loggedInRole == "Admin")
            //    allAdDetailsList = Acdcontext.AdDetails.Where(e => e.IsActive == true  && e.CategoryName == "Jobs").OrderBy(n => n.UpdatedDate).ToList();
            //else allAdDetailsList = Acdcontext.AdDetails.Where(e => e.IsActive == true  && e.PostedFor == loggedInUserId &&  e.CategoryName == "Jobs").OrderBy(n => n.UpdatedDate).ToList();

            if (loggedInRole == "SuperAdmin" || loggedInRole == "Admin")  //&& e.ValidTillDate >= DateTime.Today
            {
                //allAdDetailsList = Acdcontext.AdDetails.Where(e => e.IsActive == true && e.CategoryName != "Jobs").OrderBy(n => n.UpdatedDate).ToList();
                allAdDetailsList = (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.CategoryName == "Jobs" && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
                    (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.CategoryName == "Jobs" && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).Concat
                    (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.CategoryName == "Jobs" && e.IsVisible == false).ToList()).OrderBy(n => n.UpdatedDate).ToList());
            }

            else
            {
                allAdDetailsList = (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName == "Jobs" && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
                    (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName == "Jobs" && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).Concat
                    (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName == "Jobs" && e.IsVisible == false).ToList()).OrderBy(n => n.UpdatedDate).ToList());
                // allAdDetailsList = Acdcontext.AdDetails.Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName != "Jobs").OrderBy(n => n.UpdatedDate).ToList();
                //allAdDetailsList = (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName == "Jobs" && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
                //     (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName == "Jobs" && e.IsVisible != null && e.ValidTillDate >= DateTime.Today).ToList()).OrderBy(n => n.UpdatedDate).ToList());
            }




            return allAdDetailsList;
        }

        public DataTable GetAllAdsDetails(long loggedInUserId, string loggedInRole, string CategoryName)
        {
            AdDetails adDetails = new AdDetails();
            List<AdDetails> allAdDetailsList = new List<AdDetails>();
            try
            {
                Acdcontext.Database.ExecuteSqlCommand("update AdDetails  Set CategoryName=ct.CategoryName, PostedForName = u.FirstName + ' ' + u.LastName FROM AdDetails ad INNER JOIN  Category ct ON ad.CategoryId = ct.CategoryId INNER JOIN UserDetails u on ad.postedfor = u.UserId");
                Acdcontext.Database.ExecuteSqlCommand("exec [dbo].[updateAdDetails]");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //  allAdDetailsList = Acdcontext.ssp_GetAllAds(loggedInRole,"", loggedInUserId);


            //if (loggedInRole == "SuperAdmin" || loggedInRole == "Admin")  //&& e.ValidTillDate >= DateTime.Today
            //{
            //    //allAdDetailsList = Acdcontext.AdDetails.Where(e => e.IsActive == true && e.CategoryName != "Jobs").OrderBy(n => n.UpdatedDate).ToList();
            //    allAdDetailsList = (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.CategoryName != "Jobs" && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
            //        (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.CategoryName != "Jobs" && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).OrderBy(n => n.UpdatedDate).ToList());
            //}

            //else
            //{
            //    // allAdDetailsList = Acdcontext.AdDetails.Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName != "Jobs").OrderBy(n => n.UpdatedDate).ToList();
            //    allAdDetailsList = (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName != "Jobs" && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
            //         (Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.PostedFor == loggedInUserId && e.CategoryName != "Jobs" && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).OrderBy(n => n.UpdatedDate).ToList());
            //}






            List<Offshore_ArabchaldoContext.AllAdslist> allAds = new List<Offshore_ArabchaldoContext.AllAdslist>();
            allAds = Acdcontext.ssp_GetAllAds(loggedInRole, CategoryName, loggedInUserId);
            DataTable dtAllAds = new DataTable();
            dtAllAds.Columns.Add("adId", typeof(long));
            dtAllAds.Columns.Add("adLogoUrl", typeof(string));
            dtAllAds.Columns.Add("title", typeof(string));
            dtAllAds.Columns.Add("categoryName", typeof(string));
            dtAllAds.Columns.Add("cityName", typeof(string));
            dtAllAds.Columns.Add("status", typeof(string));
            dtAllAds.Columns.Add("isFeatured", typeof(bool));
            dtAllAds.Columns.Add("isVisible", typeof(bool));
            dtAllAds.Columns.Add("isPaidAd", typeof(bool));
            dtAllAds.Columns.Add("isActive", typeof(bool));
            dtAllAds.Columns.Add("createdDate", typeof(string));
            dtAllAds.Columns.Add("createdBy", typeof(long));
            dtAllAds.Columns.Add("updatedDate", typeof(string));
            dtAllAds.Columns.Add("validFromDate", typeof(string));
            dtAllAds.Columns.Add("validTillDate", typeof(string));
            dtAllAds.Columns.Add("activity", typeof(bool));
            dtAllAds.Columns.Add("isPremium", typeof(string));
            dtAllAds.Columns.Add("subCategoryName", typeof(string));

            foreach (var item in allAds)
            {
                try
                {
                    if (item.IsVisible != null)
                        item.Activity = true;
                    else
                        item.Activity = false;

                    dtAllAds.Rows.Add(item.AdId, item.AdLogoUrl, item.Title, item.CategoryName, item.CityName, item.Status,item.IsFeatured, item.IsVisible, item.IsPaidAd, item.IsActive, item.CreatedDate, item.CreatedBy, item.UpdatedDate, item.ValidFromDate, item.ValidTillDate,item.Activity,item.IsPremium,item.SubCategoryName);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            dtAllAds.AcceptChanges();


            return dtAllAds;
        }

        public List<AdDetails> GetAdsAdListBycategoryId(long categoryId)
        {
            AdDetails adDetails = new AdDetails();
            List<AdDetails> allAdDetailsList = new List<AdDetails>();
            Acdcontext.Database.ExecuteSqlCommand("exec [dbo].[updateAdDetails]");
            allAdDetailsList = Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.CategoryId == categoryId && e.ValidTillDate >= DateTime.Today).OrderBy(n => n.AdId).ToList();
            return allAdDetailsList;
        }

        public List<AdDetails> GetAdsAdListBySubcategoryId(long SubcategoryId)
        {
            AdDetails adDetails = new AdDetails();
            List<AdDetails> allAdDetailsList = new List<AdDetails>();
            Acdcontext.Database.ExecuteSqlCommand("exec [dbo].[updateAdDetails]");
            allAdDetailsList = Acdcontext.AdDetails.AsNoTracking().Where(e => e.IsActive == true && e.SubCategoryId == SubcategoryId).OrderBy(n => n.AdId).ToList();
            return allAdDetailsList;
        }
        public AdDetails GetAdDetailsById(long AdId)
        {
            return Acdcontext.AdDetails.AsNoTracking().FirstOrDefault(e => e.AdId == AdId && e.IsActive == true);
        }

        public bool InsertAdDetails(AdDetails AdDetails)
        {
            if (AdDetails.CityId == 9999)
                AdDetails.CityName = "Other";
            if (AdDetails.CityId == 0 || AdDetails.CityId==null)
                AdDetails.CityName = "";
            if (AdDetails.StateId == 0 || AdDetails.StateId == null)
                AdDetails.StateName = "";
            if (AdDetails.SubCategoryId == 0 || AdDetails.SubCategoryId == null)
                AdDetails.SubCategoryName = "";
            if (AdDetails.ZipCode == null || AdDetails.ZipCode == null)
                AdDetails.ZipCode = "";
            if (AdDetails.AddressStreet1 == null)
                AdDetails.AddressStreet1 = "";
            if (AdDetails.AddressStreet2 == null)
                AdDetails.AddressStreet2 = "";
            AdDetails.IsActive = true;
            AdDetails.VisitorCount = 0;
            AdDetails.IsFeatured = AdDetails.IsPaidAd;
               AdDetails.UpdatedDate = DateTime.Now;
                AdDetails.CreatedDate = DateTime.Now;
                Acdcontext.AdDetails.Add(AdDetails);
                Acdcontext.SaveChanges();
                long AdId = AdDetails.AdId;
                Acdcontext.Entry(AdDetails).State = EntityState.Detached;
            //Save image in directory
            if (AdDetails.AdLogoUrl != null)
                ManageUploads(AdId.ToString(), AdDetails.AdLogoUrl, "Logo");

            // Add images
            if (AdDetails.ImageUrl != null)
            {
                var Images = new List<string>(AdDetails.ImageUrl.Split(',')).ToList();
                foreach (var item in Images)
                {
                    AdImageDetails adimages = new AdImageDetails();
                    adimages.AdId = AdDetails.AdId;
                    adimages.AdImageUrl = item;
                    adimages.IsActive = true;
                    adimages.CreatedBy = AdDetails.CreatedBy;
                    adimages.CreatedDate = DateTime.Now;
                    Acdcontext.AdImageDetails.Add(adimages);
                    Acdcontext.SaveChanges();
                    if (item != null)
                        ManageUploads(AdDetails.AdId.ToString(), item, "Images");
                }
            }
            // workflow management for Created Status
            ManageWorkflowDetails(AdId, 1, "", AdDetails.CreatedBy);

            UpdateForeignKeyConstraints(AdDetails.AdId, AdDetails.SubCategoryId);

            return true;
        }

        public void UpdateForeignKeyConstraints(long adId, long? subCategoryId)
        {
            // Execute SQL
            if (subCategoryId == 0)
            {
                Acdcontext.Database.ExecuteSqlCommand("update AdDetails  Set SubCategoryName='',CategoryName=ct.CategoryName,CityName = c.CityName, StateName = s.StateName, PostedForName = u.FirstName + ' ' + u.LastName FROM AdDetails ad INNER JOIN  Category ct ON ad.CategoryId = ct.CategoryId  INNER JOIN  city c ON ad.CityId = c.CityId INNER JOIN state s on ad.StateId = s.StateId INNER JOIN UserDetails u on ad.postedfor = u.UserId");
            }
            else
                //Acdcontext.Database.ExecuteSqlCommand("exec [dbo].[updateAdDetails]");
                Acdcontext.Database.ExecuteSqlCommand("update AdDetails  Set CategoryName=ct.CategoryName,CityName = c.CityName, StateName = s.StateName, PostedForName = u.FirstName + ' ' + u.LastName FROM AdDetails ad INNER JOIN  Category ct ON ad.CategoryId = ct.CategoryId  INNER JOIN  city c ON ad.CityId = c.CityId INNER JOIN state s on ad.StateId = s.StateId INNER JOIN UserDetails u on ad.postedfor = u.UserId ");
        }

        //Create Directory If not exist
        public void ManageUploads(string path, string FileUrl, string UploadType)
        {
            string AdDirectory = "StaticFiles\\Ads\\" + path + "\\" + UploadType;
            if (!Directory.Exists(AdDirectory))
            {
                Directory.CreateDirectory(AdDirectory);
            }
            var SourcefolderName = Path.Combine("StaticFiles", "Images");
            var Sourcepath = Path.Combine(Directory.GetCurrentDirectory(), SourcefolderName);
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), AdDirectory);
            var SourceFile = Sourcepath + "\\" + FileUrl;
            var DestinationFile = pathToSave + "\\" + FileUrl;
            if (!File.Exists(pathToSave + FileUrl) && File.Exists(SourceFile))
            {
                File.Move(SourceFile, DestinationFile);
            }
        }
        // Insert Workflow Details
        public void ManageWorkflowDetails(long AdId, int status, string Remark, long CreatedBy)
        {
            //AdId, StatusId, remark, CreatedBy

            // CommonRepository cr = new CommonRepository();

            long PostDetailsId = GetPostDetailsId(AdId, status, 1, CreatedBy); // 1 == posttype Ads
            if (PostDetailsId > 0)
            {


                PostStatusDetails postStatusDetails = new PostStatusDetails();
                postStatusDetails.PostDetailsId = PostDetailsId;
                postStatusDetails.IsActive = true;
                postStatusDetails.CreatedBy = CreatedBy;
                postStatusDetails.CreatedDate = DateTime.Now;
                postStatusDetails.Remark = Remark;
                if (status == 1)
                {
                    postStatusDetails.WorkflowStatusId = status;
                }

                else
                {
                    Acdcontext.PostStatusDetails.Where(x => x.PostDetailsId == postStatusDetails.PostDetailsId && x.IsActive == true).ToList().ForEach(x =>
                    {
                        x.IsActive = false; x.UpdatedDate = DateTime.Now;
                    });
                    postStatusDetails.WorkflowStatusId = status;
                }

                Acdcontext.PostStatusDetails.Add(postStatusDetails);
                Acdcontext.SaveChanges();
            }
            //InsertPostStatusDetails(postStatusDetails);
        }


        // Insert PostDetails
        public long GetPostDetailsId(long AdId, int status, int PostTypeId, long CreatedBy)
        {
            long PostDetailsId = 0;
            PostDetails postDetails = new PostDetails();
            postDetails = Acdcontext.PostDetails.FirstOrDefault(e => e.PostId == AdId && e.PostTypeId == PostTypeId && e.IsActive == true);
            if (postDetails == null)
            {
                PostDetails addpostDetails = new PostDetails();
                addpostDetails.PostId = AdId;
                addpostDetails.PostTypeId = PostTypeId;
                addpostDetails.IsActive = true;
                addpostDetails.CreatedBy = CreatedBy;
                addpostDetails.CreatedDate = DateTime.Now;
                Acdcontext.PostDetails.Add(addpostDetails);
                Acdcontext.SaveChanges();
                PostDetailsId = addpostDetails.PostDetailsId;
            }
            else
                PostDetailsId = postDetails.PostDetailsId;
            return PostDetailsId;
        }

        public bool InsertAdStatusDetails(PostStatusDetails postStatusDetails)
        {

            return true;
        }







    }
}
