﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore.Internal;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class AdStatusDetailsRepository : IAdStatusDetails
    {
        private Offshore_ArabchaldoContext Acdcontext;
        public AdStatusDetailsRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }

        public bool CheckAdStatusDetailsExits(long AdStatusDetailsName)
        {
            //AdStatusDetails AdStatusDetailsDetails = Acdcontext.AdStatusDetails.FirstOrDefault(e => e.AdStatusDetailsName == AdStatusDetailsName);
            //if (AdStatusDetailsDetails != null)
            //{
            //    return true;
            //}

            return false;
        }
        public bool DeleteAdStatusDetails(int AdStatusDetailsID)
        {
            AdStatusDetails AdStatusDetailsDetails = Acdcontext.AdStatusDetails.FirstOrDefault(e => e.AdStatusDetailsId == AdStatusDetailsID);

            if (AdStatusDetailsDetails != null)
            {
                AdStatusDetailsDetails.IsActive = false;
                Acdcontext.AdStatusDetails.Update(AdStatusDetailsDetails);
                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }
        public bool UpdateAdStatusDetails(int AdStatusDetailsID, AdStatusDetails updateAdStatusDetailsDetails)
        {
            AdStatusDetails AdStatusDetailsDetails = Acdcontext.AdStatusDetails.FirstOrDefault(e => e.AdStatusDetailsId == AdStatusDetailsID);

            if (updateAdStatusDetailsDetails != null)
            {

                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }
        public List<AdStatusDetails> GetAllAdStatusDetails()
        {
            List<AdStatusDetails> allAdStatusDetailsList = new List<AdStatusDetails>();
            return Acdcontext.AdStatusDetails.ToList();
        }

        public AdStatusDetails GetAdStatusDetailsById(int AdStatusDetailsId)
        {
            return Acdcontext.AdStatusDetails.FirstOrDefault(e => e.AdStatusDetailsId == AdStatusDetailsId);
        }

        public bool InsertAdStatusDetails(AdStatusDetails AdStatusDetailsdeatils)
        {
            Acdcontext.AdStatusDetails.Add(AdStatusDetailsdeatils);
            Acdcontext.SaveChanges();

            return true;
        }
    }
}
