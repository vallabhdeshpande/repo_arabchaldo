﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore.Internal;
using System.IO;
using Microsoft.EntityFrameworkCore;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;
using System.Data;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class WebsiteDetailsRepository: IWebsiteDetails
    {
        private Offshore_ArabchaldoContext Acdcontext;

        public WebsiteDetailsRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }
        #region BusinessListing 
        public class AdsDetailsDdl
        {
            public long AdId { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string categoryName { get; set; }

        }

        public List<AdDetails> GetAdsAdListBycategoryId(long categoryId)
        {
            AdDetails adDetails = new AdDetails();
            List<AdDetails> allAdDetailsList = new List<AdDetails>();
            Acdcontext.Database.ExecuteSqlCommand("exec [dbo].[updateAdDetails]");
            allAdDetailsList = Acdcontext.AdDetails.Where(e => e.IsActive == true && e.CategoryId == categoryId && e.IsVisible == true && e.ValidTillDate >= DateTime.Today && e.ValidFromDate <= DateTime.Today).OrderByDescending(n => n.UpdatedDate).ToList();
            return allAdDetailsList;
        }
        public List<AdDetails> GetAdsAdListBySubcategoryId(long SubcategoryId)
        {
            AdDetails adDetails = new AdDetails();
            List<AdDetails> allAdDetailsList = new List<AdDetails>();
            Acdcontext.Database.ExecuteSqlCommand("exec [dbo].[updateAdDetails]");
            allAdDetailsList = Acdcontext.AdDetails.Where(e => e.IsActive == true && e.SubCategoryId == SubcategoryId && e.IsVisible == true && e.ValidTillDate >= DateTime.Today && e.ValidFromDate <= DateTime.Today).OrderByDescending(n => n.UpdatedDate).ToList();
            return allAdDetailsList;
        }
        public AdDetails GetAdDetailsById(long AdId)
        {
            List<AdDetails> allAdDetailsList = new List<AdDetails>();
            allAdDetailsList = Acdcontext.AdDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.AdId == AdId)
                                              .Select(s => new AdDetails() { VisitorCount = s.VisitorCount }).ToList();
          
                long counter = Convert.ToInt64(allAdDetailsList[0].VisitorCount) + 1;
                Acdcontext.Database.ExecuteSqlCommand("update AdDetails  Set VisitorCount=" + counter + "where AdId=" + AdId + ";");
           
           
            return Acdcontext.AdDetails.FirstOrDefault(e => e.AdId == AdId && e.IsActive == true);
        }
        public DataTable GetAllAdDetails(string imgurl)
        {
            var paramValue = imgurl.Split(",");
            string imgpath = paramValue[0];
            long categoryId = Convert.ToInt64(paramValue[1]);
            long subcategoryId = Convert.ToInt64(paramValue[2]);
            string categoryName = "";
            List<Offshore_ArabchaldoContext.AdDetailsList> allAdDetailsDdlList = new List<Offshore_ArabchaldoContext.AdDetailsList>();
            allAdDetailsDdlList = Acdcontext.ssp_GetAllAdsDetailsList(categoryId, subcategoryId,categoryName);
            //allcategoryDdlList = Acdcontext.AdDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.CategoryName != "Buy & Sell" && e.IsPaidAd==true && e.ValidTillDate >= DateTime.Now && e.ValidFromDate <= DateTime.Now).OrderByDescending(n => n.UpdatedDate).Select(s => new AdDetails()
            //{ AdId = s.AdId, Title = s.Title, Description = s.Description, CreatedDate=s.CreatedDate,CreatedBy=s.CreatedBy,VisitorCount=s.VisitorCount,ContactNumber=s.ContactNumber
            //,Email=s.Email,   CategoryName = s.CategoryName,  AdLogoUrl = s.AdLogoUrl}).ToList();
            DataTable dtAdList = new DataTable();
            dtAdList.Columns.Add("AdId", typeof(long));
            dtAdList.Columns.Add("Title", typeof(string));
            dtAdList.Columns.Add("Description", typeof(string));
            dtAdList.Columns.Add("CreatedDate", typeof(string));
            dtAdList.Columns.Add("CreatedBy", typeof(string));
            dtAdList.Columns.Add("VisitorCount", typeof(string));
            dtAdList.Columns.Add("ContactNumber", typeof(string));
            dtAdList.Columns.Add("Email", typeof(string));
            dtAdList.Columns.Add("categoryName", typeof(string));
            dtAdList.Columns.Add("ImageUrl", typeof(string));
            dtAdList.Columns.Add("IsPaidAd", typeof(Boolean));
            dtAdList.Columns.Add("UserName", typeof(string));
            dtAdList.Columns.Add("IsPremium", typeof(Boolean));
            dtAdList.Columns.Add("subCategoryName",typeof(string));

            foreach (var item in allAdDetailsDdlList)
            {
                try
                {
                    if(item.ImageURL!=null)
                    {
                        var adImgurl = item.ImageURL.Split(",");
                        item.ImageURL = adImgurl[0];
                    }
                    dtAdList.Rows.Add(item.AdId, item.Title, item.Description, item.CreatedDate, item.CreatedBy, item.VisitorCount, item.ContactNumber, item.Email, item.CategoryName, imgpath + "Ads/" + item.AdId + "/Logo/" + item.ImageURL, item.IsPaidAd, item.UserName,item.IsPaidAd,item.SubCategoryName);
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }
            dtAdList.AcceptChanges();
            return dtAdList;


            //AdDetails adDetails = new AdDetails();
            //List<AdDetails> allAdDetailsList = new List<AdDetails>();
            //Acdcontext.Database.ExecuteSqlCommand("exec [dbo].[updateAdDetails]");
            //allAdDetailsList = Acdcontext.AdDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.ValidTillDate >= DateTime.Now && e.ValidFromDate <= DateTime.Now).OrderByDescending(n => n.UpdatedDate).ToList();
            //return allAdDetailsList;
        }

        public DataTable GetAllJobDetails(string imgurl)
        {
            var paramValue = imgurl.Split(",");
            string imgpath = paramValue[0];
            long categoryId = Convert.ToInt64(paramValue[1]);
            long subcategoryId = Convert.ToInt64(paramValue[2]);
            string categoryName = "Jobs";
            List<Offshore_ArabchaldoContext.AdDetailsList> allAdDetailsDdlList = new List<Offshore_ArabchaldoContext.AdDetailsList>();
            allAdDetailsDdlList = Acdcontext.ssp_GetAllJobsDetailsList(subcategoryId, categoryName);
            DataTable dtAdList = new DataTable();
            dtAdList.Columns.Add("AdId", typeof(long));
            dtAdList.Columns.Add("Title", typeof(string));
            dtAdList.Columns.Add("Description", typeof(string));
            dtAdList.Columns.Add("CreatedDate", typeof(string));
            dtAdList.Columns.Add("CreatedBy", typeof(string));
            dtAdList.Columns.Add("VisitorCount", typeof(string));
            dtAdList.Columns.Add("ContactNumber", typeof(string));
            dtAdList.Columns.Add("Email", typeof(string));
            dtAdList.Columns.Add("categoryName", typeof(string));
            dtAdList.Columns.Add("ImageUrl", typeof(string));
            dtAdList.Columns.Add("IsPaidAd", typeof(Boolean));
            dtAdList.Columns.Add("UserName", typeof(string));
            dtAdList.Columns.Add("IsPremium", typeof(Boolean));

            foreach (var item in allAdDetailsDdlList)
            {
                try
                {
                    if (item.ImageURL != null)
                    {
                        var adImgurl = item.ImageURL.Split(",");
                        item.ImageURL = adImgurl[0];
                    }
                    dtAdList.Rows.Add(item.AdId, item.Title, item.Description, item.CreatedDate, item.CreatedBy, item.VisitorCount, item.ContactNumber, item.Email, item.CategoryName, imgpath + "Ads/" + item.AdId + "/Logo/" + item.ImageURL, item.IsPaidAd, item.UserName, item.IsFeatured);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            dtAdList.AcceptChanges();
            return dtAdList;


            //AdDetails adDetails = new AdDetails();
            //List<AdDetails> allAdDetailsList = new List<AdDetails>();
            //Acdcontext.Database.ExecuteSqlCommand("exec [dbo].[updateAdDetails]");
            //allAdDetailsList = Acdcontext.AdDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.ValidTillDate >= DateTime.Now && e.ValidFromDate <= DateTime.Now).OrderByDescending(n => n.UpdatedDate).ToList();
            //return allAdDetailsList;
        }
        #endregion

        #region SmallAds 
        //public class AdsDetailsDdl
        //{
        //    public long AdId { get; set; }
        //    public string Title { get; set; }
        //    public string Description { get; set; }
        //    public string categoryName { get; set; }

        //}

       
        public SmallAdDetails GetSmallAdDetailsById(long SmallAdId)
        {
            List<SmallAdDetails> allAdDetailsList = new List<SmallAdDetails>();
            allAdDetailsList = Acdcontext.SmallAdDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.SmallAdId == SmallAdId)
                                              .Select(s => new SmallAdDetails() { VisitorCount = s.VisitorCount }).ToList();

            long counter = Convert.ToInt64(allAdDetailsList[0].VisitorCount) + 1;
            Acdcontext.Database.ExecuteSqlCommand("update SmallAdDetails  Set VisitorCount=" + counter + "where SmallAdId=" + SmallAdId + ";");


            return Acdcontext.SmallAdDetails.FirstOrDefault(e => e.SmallAdId == SmallAdId && e.IsActive == true);
        }
        public DataTable GetAllSmallAdDetails(string imgurl)
        {
            var paramValue = imgurl.Split(",");
            string imgpath = paramValue[0];
            string type = paramValue[1];
            List<Offshore_ArabchaldoContext.SmallAdDetailsList> allSmallAdDetailsDdlList = new List<Offshore_ArabchaldoContext.SmallAdDetailsList>();
            allSmallAdDetailsDdlList = Acdcontext.ssp_GetAllSmallAdsDetailsList(type);
           DataTable dtSmallAdList = new DataTable();
            dtSmallAdList.Columns.Add("SmallAdId", typeof(long));
            dtSmallAdList.Columns.Add("Title", typeof(string));
            dtSmallAdList.Columns.Add("Description", typeof(string));
            dtSmallAdList.Columns.Add("CreatedDate", typeof(string));
            dtSmallAdList.Columns.Add("CreatedBy", typeof(string));
            dtSmallAdList.Columns.Add("VisitorCount", typeof(string));
            dtSmallAdList.Columns.Add("ContactNumber", typeof(string));
            dtSmallAdList.Columns.Add("Email", typeof(string));
            dtSmallAdList.Columns.Add("ImageUrl", typeof(string));
            dtSmallAdList.Columns.Add("IsPaidAd", typeof(Boolean));
            dtSmallAdList.Columns.Add("UserName", typeof(string));
            dtSmallAdList.Columns.Add("IsPremium", typeof(Boolean));

            foreach (var item in allSmallAdDetailsDdlList)
            {
                try
                {
                    if (item.ImageURL != null)
                    {
                        var adImgurl = item.ImageURL.Split(",");
                        item.ImageURL = adImgurl[0];
                    }
                    dtSmallAdList.Rows.Add(item.SmallAdId, item.Title, item.Description, item.CreatedDate, item.CreatedBy, item.VisitorCount, item.ContactNumber, item.Email, imgpath + "SmallAds/" + item.SmallAdId + "/Logo/" + item.ImageURL, item.IsPaidAd, item.UserName, item.IsPremium);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            dtSmallAdList.AcceptChanges();
            return dtSmallAdList;


        }

      
        #endregion






        #region  Banners
        public DataTable GetAllBanner(string imgurl)
        {

            List<Offshore_ArabchaldoContext.PremiumBannerList> FeaturedBannerList = new List<Offshore_ArabchaldoContext.PremiumBannerList>();
            FeaturedBannerList = Acdcontext.ssp_GetWebSiteHomePageBannerList(imgurl, "Featured");

            //List<BannerDetails> bannerDetails = new List<BannerDetails>();

            //bannerDetails = Acdcontext.BannerDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.IsFeatured != true && e.ValidTillDate >= DateTime.Today && e.ValidFromDate <= DateTime.Today).OrderByDescending(n => n.UpdatedDate).Select(s => new BannerDetails()
            //{ BannerId = s.BannerId, ImageUrl = s.ImageUrl }).ToList();

            DataTable dtbannerList = new DataTable();
            dtbannerList.Columns.Add("BannerId", typeof(long));
            dtbannerList.Columns.Add("ImageUrl", typeof(string));
            foreach (var item in FeaturedBannerList)
            {
                dtbannerList.Rows.Add(item.BannerId, item.imageUrl);
            }
            dtbannerList.AcceptChanges();
            return dtbannerList;

        }
        public DataTable getPremiumBannerList(string imgurl)
        {
            List<Offshore_ArabchaldoContext.PremiumBannerList> PremiumBannerList = new List<Offshore_ArabchaldoContext.PremiumBannerList>();
            PremiumBannerList = Acdcontext.ssp_GetWebSiteHomePageBannerList(imgurl, "Premium");


            //List<BannerDetails> bannerDetails = new List<BannerDetails>();

            //bannerDetails = Acdcontext.BannerDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.IsFeatured == true && e.ValidTillDate >= DateTime.Today && e.ValidFromDate <= DateTime.Today).OrderByDescending(n => n.UpdatedDate).Select(s => new BannerDetails()
            //{ BannerId = s.BannerId, ImageUrl = s.ImageUrl }).ToList();
            DataTable dtbannerList = new DataTable();
            dtbannerList.Columns.Add("BannerId", typeof(long));
            dtbannerList.Columns.Add("ImageUrl", typeof(string));
            foreach (var item in PremiumBannerList)
            {
                dtbannerList.Rows.Add(item.BannerId, item.imageUrl);
            }
            dtbannerList.AcceptChanges();
            return dtbannerList;

        }
        public BannerDetails GetBannerById(long BannerId)
        {
            List<BannerDetails> allBannerList = new List<BannerDetails>();
            allBannerList = Acdcontext.BannerDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.BannerId == BannerId)
                                              .Select(s => new BannerDetails() { VisitorCount = s.VisitorCount }).ToList();
            long counter = Convert.ToInt64(allBannerList[0].VisitorCount) + 1;
            Acdcontext.Database.ExecuteSqlCommand("update BannerDetails  Set VisitorCount=" + counter + "where BannerId=" + BannerId + ";");
            BannerDetails bannerIdExist = Acdcontext.BannerDetails.FirstOrDefault(e => e.BannerId == BannerId);
            if (bannerIdExist != null)
            {
                return bannerIdExist;
            }
            return null;
        }

        #endregion

        #region Blogs
        public DataTable GetAllBlog()
        {
            //BlogDetails BlogDetails = new BlogDetails();
            //List<BlogDetails> blogDetails = new List<BlogDetails>();
            //try {
            //    blogDetails = Acdcontext.BlogDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.ValidTillDate >= DateTime.Today && e.ValidFromDate <= DateTime.Today).OrderByDescending(n => n.BlogId).ToList();
            //}
            //catch(Exception ex)
            //{ throw ex; }
            //return blogDetails;

            List<Offshore_ArabchaldoContext.AllBlogList> allBlogsList = new List<Offshore_ArabchaldoContext.AllBlogList>();
            allBlogsList = Acdcontext.ssp_GetAllBlogList();
            DataTable allBlogsdata = new DataTable();
            allBlogsdata.Columns.Add("blogId", typeof(long));
            allBlogsdata.Columns.Add("title", typeof(string));
            allBlogsdata.Columns.Add("ImageUrl", typeof(string));
            allBlogsdata.Columns.Add("isVisible", typeof(bool));
            allBlogsdata.Columns.Add("isActive", typeof(bool));
            allBlogsdata.Columns.Add("createdDate", typeof(string));
            allBlogsdata.Columns.Add("createdBy", typeof(long));
            allBlogsdata.Columns.Add("visitorCount", typeof(long));

            foreach (var item in allBlogsList)
            {
                try
                {
                    
                    allBlogsdata.Rows.Add(item.BlogId, item.Title, item.ImageUrl, item.IsVisible, item.IsActive, item.CreatedDate, item.CreatedBy, item.VisitorCount);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            allBlogsdata.AcceptChanges();


            return allBlogsdata;
        }

        public BlogDetails GetBlogById(long BlogId)
        {
            List<BlogDetails> allBlogList = new List<BlogDetails>();
            allBlogList = Acdcontext.BlogDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.BlogId == BlogId)
                                              .Select(s => new BlogDetails() { VisitorCount = s.VisitorCount }).ToList();
            long counter = Convert.ToInt64(allBlogList[0].VisitorCount) + 1;
            Acdcontext.Database.ExecuteSqlCommand("update BlogDetails  Set VisitorCount=" + counter + "where BlogId=" + BlogId + ";");
            BlogDetails blogIdExist = Acdcontext.BlogDetails.FirstOrDefault(e => e.BlogId == BlogId);
            if (blogIdExist != null)
            {
                return blogIdExist;
            }
            return null;
        }

        #endregion

        #region Events
        public List<EventDetails> GetAllEvent()
        {
                List<EventDetails> eventDetails = new List<EventDetails>();
                eventDetails = Acdcontext.EventDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.ValidTillDate >= DateTime.Today && e.ValidFromDate <= DateTime.Today).OrderByDescending(n => n.EventId).ToList();
                return eventDetails;           
        }

        public EventDetails GetEventById(long EventId)
        {
            List<EventDetails> eventDetails = new List<EventDetails>();
            eventDetails = Acdcontext.EventDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.EventId == EventId)
                                  .Select(s => new EventDetails() { VisitorCount = s.VisitorCount }).ToList();
            long counter = Convert.ToInt64(eventDetails[0].VisitorCount) + 1;
            Acdcontext.Database.ExecuteSqlCommand("update EventDetails  Set VisitorCount=" + counter + "where EventId=" + EventId + ";");
            EventDetails eventIdExist = Acdcontext.EventDetails.FirstOrDefault(e => e.EventId == EventId);
            if (eventIdExist != null)
            {
                return eventIdExist;
            }
            return null;
        }

        public DataTable GetEventsByCategoryId(long categoryId)
        {
            List<Offshore_ArabchaldoContext.WebsiteEventList> WebEventList = new List<Offshore_ArabchaldoContext.WebsiteEventList>();
            WebEventList = Acdcontext.ssp_GetAllEventForWebsite(categoryId);

            DataTable dtEvent = new DataTable();

            dtEvent.Columns.Add("eventId", typeof(long));
            dtEvent.Columns.Add("title", typeof(string));
            dtEvent.Columns.Add("imageUrl", typeof(string));
            dtEvent.Columns.Add("isVisible", typeof(bool));
            dtEvent.Columns.Add("isActive", typeof(bool));
            dtEvent.Columns.Add("categoryId", typeof(long));
            dtEvent.Columns.Add("isPaidAd", typeof(bool));
            dtEvent.Columns.Add("visitorCount", typeof(long));
            dtEvent.Columns.Add("isPremium", typeof(bool));
            dtEvent.Columns.Add("eventDate", typeof(string));

            foreach (var item in WebEventList)
            {
                dtEvent.Rows.Add(item.eventId, item.title, item.imageUrl, item.isVisible, item.isActive, item.categoryId, item.isPaidAd, 
                    item.visitorCount, item.isPremium, item.eventDate);
            }
            dtEvent.AcceptChanges();
            return dtEvent;

            //List<EventDetails> alleventDdlList = new List<EventDetails>();
            //if(categoryId!=0)
            //alleventDdlList = Acdcontext.EventDetails.Where(e => e.IsActive == true && e.CategoryId == categoryId && e.IsVisible == true && e.ValidTillDate >= DateTime.Today && e.ValidFromDate <= DateTime.Today).OrderBy(n => n.EventDate).ToList();
            //else
            //    alleventDdlList = Acdcontext.EventDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.ValidTillDate >= DateTime.Today && e.ValidFromDate <= DateTime.Today).OrderBy(n => n.EventDate).ToList();

            //return alleventDdlList;
        }
        #endregion

        #region GlobalSearch

        // Category
        public class CategoryDdl
        {
            public long categoryId { get; set; }
            public string categoryName { get; set; }

            public string CategoryImageUrl { get; set; }

            public int  SubCategoryCount { get; set; }
            public string arabicName { get; set; }
        }
        public DataTable GetAllCategoryData()
        {
            //List<CategoryDdl> allcategoryDdlList = new List<CategoryDdl>();
            //allcategoryDdlList = Acdcontext.Category.Where(e => e.IsActive == true).Select(s => new CategoryDdl()
            //{ categoryId = s.CategoryId, categoryName = s.CategoryName, CategoryImageUrl = s.CategoryImageUrl }).ToList();
            List<Offshore_ArabchaldoContext.CategoryList> allcategoryDdlList = new List<Offshore_ArabchaldoContext.CategoryList>();
            allcategoryDdlList = Acdcontext.ssp_GetCategoriesList();
            DataTable dtcategory = new DataTable();
            dtcategory.Columns.Add("categoryId", typeof(long));
            dtcategory.Columns.Add("categoryName", typeof(string));
            dtcategory.Columns.Add("CategoryImageUrl", typeof(string));
            dtcategory.Columns.Add("SubCategoryCount", typeof(int));
            dtcategory.Columns.Add("arabicName", typeof(string));
            foreach (var item in allcategoryDdlList)
            {
                dtcategory.Rows.Add(item.categoryId, item.categoryName, item.CategoryImageUrl,item.SubCategoryCount,item.arabicName);
            }
            dtcategory.AcceptChanges();
            return dtcategory;
        }

        // Category Search DDL
        public class CategorySearchDDL
        {
            public long categoryId { get; set; }
            public string categoryName { get; set; }
            public string arabicName { get; set; }
        }
        public DataTable GetCategorySearchDDL()
        {
            List<Offshore_ArabchaldoContext.CategorySearchDDL> categorySearchDDL = new List<Offshore_ArabchaldoContext.CategorySearchDDL>();
            categorySearchDDL = Acdcontext.ssp_GetCategorySearchDDL("Category");
            DataTable dtcategory = new DataTable();
            dtcategory.Columns.Add("categoryId", typeof(long));
            dtcategory.Columns.Add("categoryName", typeof(string));
            dtcategory.Columns.Add("arabicName", typeof(string));
            foreach (var item in categorySearchDDL)
            {
                dtcategory.Rows.Add(item.categoryId, item.categoryName, item.arabicName);
            }
            dtcategory.AcceptChanges();
            return dtcategory;
        }


        // City
        public class CityList
        {
            public long cityId { get; set; }
            public long StateId { get; set; }
            public string cityName { get; set; }
            public string stateName { get; set; }
            public string countryName { get; set; }
            public string description { get; set; }
        }
        public DataTable GetAllCityData()
        {
            List<CityList> cityList = new List<CityList>();
            cityList = (from c in Acdcontext.City
                        join s in Acdcontext.State
                        on c.StateId equals s.StateId
                        join cnt in Acdcontext.Country
                        on s.CountryId equals cnt.CountryId
                        where c.IsActive == true orderby c.CityName
                        select new CityList { cityId = c.CityId, StateId = s.StateId, cityName = c.CityName, stateName = s.StateName, countryName = cnt.CountryName, description = c.Description }).ToList();

            DataTable dtcityList = new DataTable();
            dtcityList.Columns.Add("cityId", typeof(long));
            dtcityList.Columns.Add("StateId", typeof(long));
            dtcityList.Columns.Add("cityName", typeof(string));
            dtcityList.Columns.Add("stateName", typeof(string));
            dtcityList.Columns.Add("countryName", typeof(string));
            dtcityList.Columns.Add("description", typeof(string));
            foreach (var item in cityList)
            {
                dtcityList.Rows.Add(item.cityId, item.StateId, item.cityName, item.stateName, item.countryName, item.description);
            }
            dtcityList.AcceptChanges();
            return dtcityList;
        }

        // City
        public class CitySearchDDL
        {
            public long cityId { get; set; }
            public string cityName { get; set; }
            public string stateName { get; set; }
        }
        public DataTable GetCitySearchDDL()
        {
            List<Offshore_ArabchaldoContext.CitySearchDDL> citySearchDDL = new List<Offshore_ArabchaldoContext.CitySearchDDL>();
            citySearchDDL = Acdcontext.ssp_GetCitySearchDDL("City");
            DataTable dtcityList = new DataTable();
            dtcityList.Columns.Add("cityId", typeof(long));
            dtcityList.Columns.Add("cityName", typeof(string));
            dtcityList.Columns.Add("stateName", typeof(string));
            foreach (var item in citySearchDDL)
            {
                dtcityList.Rows.Add(item.cityId, item.cityName, item.stateName);
            }
            dtcityList.AcceptChanges();
            return dtcityList;
        }

        //public class SearchAdList
        //{
        //    public long adId { get; set; }
        //    public string title { get; set; }
        //    public string description { get; set; }
        //}
        public DataTable GetAdListBySearchCriteria(string searchCriteria)
        {
            long categoryId = 0;
            long cityId = 0;
            string searchText = "";
            string imgpath = "";
            searchText = searchCriteria.Split(',')[0];
            if(searchText.Contains(' '))
            {
                string searchText1 = searchText.Split(' ')[0];
                string searchText2 = searchText.Split(' ')[1];
                searchText = searchText1;
            }
            categoryId = Convert.ToInt64(searchCriteria.Split(',')[1]);
            cityId = Convert.ToInt64(searchCriteria.Split(',')[2]);
            imgpath = searchCriteria.Split(',')[3];
            //searchCriteria = searchText;
            List<Offshore_ArabchaldoContext.SearchAdList> AdList = new List<Offshore_ArabchaldoContext.SearchAdList>();
            //AdList = (from ad in Acdcontext.ssp_GetAdListBySearchCriteria                        
            //            where ad.IsActive == true && ad.IsVisible==true && ad.CategoryId==categoryId && ad.CityId==cityId && ad.Title==searchText
            //            select new SearchAdList { adId = ad.AdId,title=ad.Title ,description = ad.Description }).ToList();

            AdList = Acdcontext.ssp_GetAdListBySearchCriteria(searchText, categoryId, cityId);
            DataTable dtAdList = new DataTable();
            dtAdList.Columns.Add("adId", typeof(long));
            dtAdList.Columns.Add("title", typeof(string));
            dtAdList.Columns.Add("description", typeof(string));
            dtAdList.Columns.Add("CreatedDate", typeof(string));
            dtAdList.Columns.Add("CreatedBy", typeof(string));
            dtAdList.Columns.Add("VisitorCount", typeof(string));
            dtAdList.Columns.Add("ContactNumber", typeof(string));
            dtAdList.Columns.Add("Email", typeof(string));
            dtAdList.Columns.Add("categoryName", typeof(string));
            dtAdList.Columns.Add("ImageURL", typeof(string));
            dtAdList.Columns.Add("IsPaidAd", typeof(Boolean));
            dtAdList.Columns.Add("UserName", typeof(string));
            dtAdList.Columns.Add("IsPremium", typeof(Boolean));
            dtAdList.Columns.Add("subCategoryName", typeof(string));

            foreach (var item in AdList)
            {
                if (item.ImageURL != null)
                {
                    var adImgurl = item.ImageURL.Split(",");
                    item.ImageURL = adImgurl[0];
                }
                    dtAdList.Rows.Add(item.adId, item.title, item.description, item.CreatedDate, item.CreatedBy, item.VisitorCount, item.ContactNumber, item.Email, item.CategoryName, imgpath + "Ads/" + item.adId + "/Logo/" + item.ImageURL, item.IsPaidAd, item.UserName,item.IsPaidAd,item.SubCategoryName); 
            }
            dtAdList.AcceptChanges();
            return dtAdList;
        }





        #endregion

        EmailSender em = new EmailSender();
        #region Template records of website

        public class ConfigrationDdl
        {
            public string AboutUs { get; set; }
            public string ContactUs { get; set; }
            public string ContactUsArabic { get; set; }
            public string TermsofUse { get; set; }
            public string TermsofUseArabic { get; set; }
            public string PrivacyPolicy { get; set; }
            public string PrivacyPolicyArabic { get; set; }
            public string FAQs { get; set; }
            public string FAQsArabic { get; set; }
            public string Disclaimer { get; set; }
            public string DisclaimerArabic { get; set; }
        }
        public DataTable GetAboutUSData()
        {

            List<ConfigrationDdl> configrationList = new List<ConfigrationDdl>();
            configrationList = Acdcontext.Configuration.Where(e => e.IsActive == true).Select(s => new ConfigrationDdl()
            { AboutUs=s.AboutUstemplate}).ToList();
            DataTable ConfigList = new DataTable();
            ConfigList.Columns.Add("AboutUs", typeof(string));
            foreach (var item in configrationList)
            {
                ConfigList.Rows.Add(item.AboutUs);
            }
            ConfigList.AcceptChanges();
            return ConfigList;
        }
        public DataTable GetContactUSData()
        {

            List<ConfigrationDdl> configrationList = new List<ConfigrationDdl>();
            configrationList = Acdcontext.Configuration.Where(e => e.IsActive == true).Select(s => new ConfigrationDdl()
            { ContactUs = s.ContactUstemplate, ContactUsArabic = s.ContactUsTemplateArabic }).ToList();
            DataTable ConfigList = new DataTable();
            ConfigList.Columns.Add("ContactUs", typeof(string));
            ConfigList.Columns.Add("ContactUsArabic", typeof(string));
            foreach (var item in configrationList)
            {
                ConfigList.Rows.Add(item.ContactUs,item.ContactUsArabic);
            }
            ConfigList.AcceptChanges();
            return ConfigList;
        }
        public DataTable GetTermsofUseData()
        {

            List<ConfigrationDdl> configrationList = new List<ConfigrationDdl>();
            configrationList = Acdcontext.Configuration.Where(e => e.IsActive == true).Select(s => new ConfigrationDdl()
            { TermsofUse = s.TermsOfUseTemplate, TermsofUseArabic=s.TermsOfUseTemplateArabic }).ToList();
            DataTable ConfigList = new DataTable();
            ConfigList.Columns.Add("TermsofUse", typeof(string));
            ConfigList.Columns.Add("TermsofUseArabic", typeof(string));
            foreach (var item in configrationList)
            {
                ConfigList.Rows.Add(item.TermsofUse,item.TermsofUseArabic);
            }
            ConfigList.AcceptChanges();
            return ConfigList;
        }
        public DataTable GetPrivacyPolicyData()
        {

            List<ConfigrationDdl> configrationList = new List<ConfigrationDdl>();
            configrationList = Acdcontext.Configuration.Where(e => e.IsActive == true).Select(s => new ConfigrationDdl()
            { PrivacyPolicy = s.PrivacyPolicyTemplate, PrivacyPolicyArabic=s.PrivacyPolicyTemplateArabic }).ToList();
            DataTable ConfigList = new DataTable();
            ConfigList.Columns.Add("PrivacyPolicy", typeof(string));
            ConfigList.Columns.Add("PrivacyPolicyArabic", typeof(string));
            foreach (var item in configrationList)
            {
                ConfigList.Rows.Add(item.PrivacyPolicy, item.PrivacyPolicyArabic);
            }
            ConfigList.AcceptChanges();
            return ConfigList;
        }







        public DataTable GetFAQData()
        {

            List<ConfigrationDdl> configrationList = new List<ConfigrationDdl>();
            configrationList = Acdcontext.Configuration.Where(e => e.IsActive == true).Select(s => new ConfigrationDdl()
            { FAQs = s.Faqtemplate, FAQsArabic = s.FaqtemplateArabic }).ToList();
            DataTable ConfigList = new DataTable();
            ConfigList.Columns.Add("FAQs", typeof(string));
            ConfigList.Columns.Add("FAQsArabic", typeof(string));
            foreach (var item in configrationList)
            {
                ConfigList.Rows.Add(item.FAQs, item.FAQsArabic);
            }
            ConfigList.AcceptChanges();
            return ConfigList;
        }

        public DataTable GetDisclaimerData()
        {

            List<ConfigrationDdl> configrationList = new List<ConfigrationDdl>();
            configrationList = Acdcontext.Configuration.Where(e => e.IsActive == true).Select(s => new ConfigrationDdl()
            { Disclaimer = s.DisclaimerTemplate,DisclaimerArabic=s.DisclaimerTemplateArabic }).ToList();
            DataTable ConfigList = new DataTable();
            ConfigList.Columns.Add("Disclaimer", typeof(string));
            ConfigList.Columns.Add("DisclaimerArabic", typeof(string));
            foreach (var item in configrationList)
            {
                ConfigList.Rows.Add(item.Disclaimer,item.DisclaimerArabic);
            }
            ConfigList.AcceptChanges();
            return ConfigList;
        }
        #endregion

        #region buysell 
        public DataTable GetAllBuysellData(string imgurl)
        {
            List<Offshore_ArabchaldoContext.BuySelllist> BuySellList = new List<Offshore_ArabchaldoContext.BuySelllist>();
            //BuySellList = (from ad in Acdcontext.BuySellDetails
            //               join ur in Acdcontext.UserDetails
            //               on ad.PostedFor equals ur.UserId
            //               join bdi in Acdcontext.BuySellImageDetails
            //               on ad.BuySellId equals bdi.BuySellId
            //               where ad.IsActive == true && ad.IsVisible == true // && ad.ValidTillDate >=DateTime.Today && ad.ValidFromDate <=DateTime.Today
            //               orderby ad.UpdatedDate descending
            //               select new Offshore_ArabchaldoContext.BuySelllist
            //               {
            //                   BuySellId = ad.BuySellId,
            //                   Title = ad.Title,
            //                   Description = ad.Description,
            //                   CreatedDate = ad.CreatedDate,
            //                   CreatedBy = ad.CreatedBy,
            //                   VisitorCount = ad.VisitorCount,
            //                   ContactNumber = ad.ContactNumber,
            //                   Email = ad.Email,
            //                   CategoryName = ad.CategoryName,
            //                   ImageUrl = ad.ImageUrl,
            //                   IsPaidAd = ad.IsPaidAd,
            //                   PostedForName = ur.FirstName + " " + ur.LastName,
            //                   IsPremium = ad.IsPremium, imageUrls= bdi.BuySellImageUrl
            //               }).ToList();
            BuySellList = Acdcontext.ssp_GetAllBuySellList();
            DataTable dtbuysell = new DataTable();
            dtbuysell.Columns.Add("BuySellId", typeof(long));
            dtbuysell.Columns.Add("Title", typeof(string));
            dtbuysell.Columns.Add("Description", typeof(string));
            dtbuysell.Columns.Add("CreatedDate", typeof(string));
            dtbuysell.Columns.Add("CreatedBy", typeof(string));
            dtbuysell.Columns.Add("VisitorCount", typeof(string));
            dtbuysell.Columns.Add("ContactNumber", typeof(string));
            dtbuysell.Columns.Add("Email", typeof(string));
            dtbuysell.Columns.Add("categoryName", typeof(string));
            dtbuysell.Columns.Add("ImageUrl", typeof(string));
            dtbuysell.Columns.Add("IsPaidAd", typeof(Boolean));
            dtbuysell.Columns.Add("UserName", typeof(string));
            dtbuysell.Columns.Add("IsPremium", typeof(string));
            dtbuysell.Columns.Add("subCategoryName", typeof(string));
            foreach (var item in BuySellList)
            {
  
                if (item.IsPaidAd == true && item.ImageUrl != null)
                {
                    var adImgurl = item.ImageUrl.Split(",");
                    item.ImageUrl = adImgurl[0];
                    dtbuysell.Rows.Add(item.BuySellId, item.Title, item.Description, item.CreatedDate, item.CreatedBy, item.VisitorCount, item.ContactNumber, item.Email, item.CategoryName, imgurl + "BuySell/" + item.BuySellId + "/Premium/" + item.ImageUrl, item.IsPaidAd, item.UserName, item.IsPremium,item.SubCategoryName);
                }
                else if (item.IsPaidAd != true && item.ImageUrl != null)

                {
                    dtbuysell.Rows.Add(item.BuySellId, item.Title, item.Description, item.CreatedDate, item.CreatedBy, item.VisitorCount, item.ContactNumber, item.Email, item.CategoryName, imgurl + "BuySell/" + item.BuySellId + "/Free/" + item.ImageUrl, item.IsPaidAd, item.UserName, item.IsPremium, item.SubCategoryName);
                }
                else
                {
                    item.ImageUrl = "assets/img/default/no_image_placeholder.jpg";
                    dtbuysell.Rows.Add(item.BuySellId, item.Title, item.Description, item.CreatedDate, item.CreatedBy, item.VisitorCount, item.ContactNumber, item.Email, item.CategoryName, item.ImageUrl, item.IsPaidAd, item.UserName, item.IsPremium, item.SubCategoryName);
                }
     
            }
            dtbuysell.AcceptChanges();
            return dtbuysell;
        }

        public DataTable GetAllBuysellRecentData(string imgpath)
        {
            var paramValue = imgpath.Split(",");
            string imgurl = paramValue[0];
            string type = paramValue[1];
            List<Offshore_ArabchaldoContext.BuySelllist> BuySellList = new List<Offshore_ArabchaldoContext.BuySelllist>();
            BuySellList = Acdcontext.ssp_GetAllBuySellRecentList();
            DataTable dtbuysell = new DataTable();
            dtbuysell.Columns.Add("BuySellId", typeof(long));
            dtbuysell.Columns.Add("Title", typeof(string));
            dtbuysell.Columns.Add("Description", typeof(string));
            dtbuysell.Columns.Add("CreatedDate", typeof(string));
            dtbuysell.Columns.Add("CreatedBy", typeof(string));
            dtbuysell.Columns.Add("VisitorCount", typeof(string));
            dtbuysell.Columns.Add("ContactNumber", typeof(string));
            dtbuysell.Columns.Add("Email", typeof(string));
            dtbuysell.Columns.Add("categoryName", typeof(string));
            dtbuysell.Columns.Add("ImageUrl", typeof(string));
            dtbuysell.Columns.Add("IsPaidAd", typeof(Boolean));
            dtbuysell.Columns.Add("UserName", typeof(string));
            dtbuysell.Columns.Add("IsPremium", typeof(string));
            dtbuysell.Columns.Add("subCategoryName", typeof(string));
            foreach (var item in BuySellList)
            {
                    if ( item.ImageUrl != null)
                    {
                        var adImgurl = item.ImageUrl.Split(",");
                        item.ImageUrl = adImgurl[0];
                        dtbuysell.Rows.Add(item.BuySellId, item.Title, item.Description, item.CreatedDate, item.CreatedBy, item.VisitorCount, item.ContactNumber, item.Email, item.CategoryName, imgurl + "BuySell/" + item.BuySellId + "/Premium/" + item.ImageUrl, item.IsPaidAd, item.UserName, item.IsPremium, item.SubCategoryName);
                    }               

            }
            dtbuysell.AcceptChanges();
            return dtbuysell;
        }
        public BuySellDetails GetBuysellById(long buysellId)
        {
            List<BuySellDetails> allBuyellList = new List<BuySellDetails>();
            allBuyellList = Acdcontext.BuySellDetails.Where(e => e.IsActive == true && e.IsVisible == true && e.BuySellId == buysellId)
                                              .Select(s => new BuySellDetails() { VisitorCount = s.VisitorCount }).ToList();
            long counter = Convert.ToInt64(allBuyellList[0].VisitorCount) + 1;
            Acdcontext.Database.ExecuteSqlCommand("update BuySellDetails  Set VisitorCount=" + counter + "where BuySellId=" + buysellId + ";");
            BuySellDetails buysellIdExist = Acdcontext.BuySellDetails.FirstOrDefault(e => e.BuySellId == buysellId);
            if (buysellIdExist != null)
            {
                return buysellIdExist;
            }
            return null;
        }
        #endregion


    }
}
