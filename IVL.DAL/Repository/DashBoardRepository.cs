﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class DashBoardRepository : IDashboard
    {
        private Offshore_ArabchaldoContext Acdcontext;
        public DashBoardRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }
        #region SuperAdmin Dashboard
        public class DashBoardDal
        {
            public long userCount { get; set; }
            public long AdsCount { get; set; }
            public long BannerCount { get; set; }
            public long EventCount { get; set; }
            public long jobsCount { get; set; }
            public long BlogsCount { get; set; }
            public string AdscountList { get; set; }

            public string BannerCountList { get; set; }
            public string EventsCountList { get; set; }
            public long MessageTopicCount { get; set; }
            public long MessageReplyCount { get; set; }

        }
        public DataTable GetotalCount()
        {
            List<Offshore_ArabchaldoContext.DashBoardCountList> dahsboardCountList = new List<Offshore_ArabchaldoContext.DashBoardCountList>();
            dahsboardCountList = Acdcontext.ssp_GetDashBoardCountList();
            DataTable DashBoardCount = new DataTable();
            DashBoardCount.Columns.Add("ActiveBusiLitingCount", typeof(int));
            DashBoardCount.Columns.Add("PendingBusiLitingCount", typeof(int));
            DashBoardCount.Columns.Add("ActiveSmallAdsCount", typeof(int));
            DashBoardCount.Columns.Add("PendingSmallAdsCount", typeof(int));
            DashBoardCount.Columns.Add("ActiveBannerCount", typeof(int));
            DashBoardCount.Columns.Add("PendingBannerCount", typeof(int));
            DashBoardCount.Columns.Add("ActiveEventCount", typeof(int));
            DashBoardCount.Columns.Add("PendingEventCount", typeof(int));
            DashBoardCount.Columns.Add("ActiveJobsCount", typeof(int));
            DashBoardCount.Columns.Add("PendingJobsCount", typeof(int));
            DashBoardCount.Columns.Add("ActiveBlogsCount", typeof(int));
            DashBoardCount.Columns.Add("PendingBlogsCount", typeof(int));
            DashBoardCount.Columns.Add("ActiveBuysellCount", typeof(int));
            DashBoardCount.Columns.Add("PendingBuysellCount", typeof(int));
            DashBoardCount.Columns.Add("UserCount", typeof(int)); 
            DashBoardCount.Columns.Add("AdsCount", typeof(int));
            DashBoardCount.Columns.Add("BannerCount", typeof(int));
            DashBoardCount.Columns.Add("EventCount", typeof(int));
            DashBoardCount.Columns.Add("JobsCount", typeof(int));
            DashBoardCount.Columns.Add("BlogsCount", typeof(int));
            DashBoardCount.Columns.Add("buysellCount", typeof(int));
            DashBoardCount.Columns.Add("smallAdsCount", typeof(int));
            DashBoardCount.Columns.Add("TotalRevenueAmount", typeof(int));
            DashBoardCount.Columns.Add("OnlineAmount", typeof(int));
            DashBoardCount.Columns.Add("OfflineAmount", typeof(int));
            DashBoardCount.Columns.Add("paidUser", typeof(int));
            DashBoardCount.Columns.Add("FreeUser", typeof(int));
            DashBoardCount.Columns.Add("TotalAdvertimentCount", typeof(int));
            DashBoardCount.Columns.Add("paidAdvertisementCount", typeof(int));
            DashBoardCount.Columns.Add("freeAdvertisementCount", typeof(int));
            DashBoardCount.Columns.Add("MessageTopicCount", typeof(int));
            DashBoardCount.Columns.Add("MessageReplyCount", typeof(int));
            foreach (var item in dahsboardCountList)
            {
                DashBoardCount.Rows.Add(
                    item.ActiveBusiLitingCount, item.PendingBusiLitingCount, item.ActiveSmallAdsCount, item.PendingSmallAdsCount, item.ActiveBannerCount, item.PendingBannerCount,
                    item.ActiveEventCount, item.PendingEventCount, item.ActiveJobsCount, item.PendingJobsCount, item.ActiveBlogsCount, item.PendingBlogsCount,
                    item.ActiveBuysellCount, item.PendingBuysellCount,
                    item.UserCount, item.AdsCount, item.BannerCount, item.EventCount, item.JobsCount, item.BlogsCount, item.buysellCount, item.smallAdsCount,
                    item.TotalRevenueAmount, item.OnlineAmount, item.OfflineAmount, item.paidUser, item.FreeUser, item.TotalAdvertimentCount,
                    item.paidAdvertisementCount, item.freeAdvertisementCount,item.MessageTopicCount,item.MessageReplyCount);
            }

            DashBoardCount.AcceptChanges();
            return DashBoardCount;
        }

        public DataTable GetBargraphdata(long year)
        {
           
                List<Offshore_ArabchaldoContext.DashBoardList> BarGraphRecords = new List<Offshore_ArabchaldoContext.DashBoardList>();
            try {
                BarGraphRecords = Acdcontext.ssp_GetDashBoardData(year);

            }
            catch(Exception ex)
            {
                throw ex;
            }
            string AdscountList = "";
            if (BarGraphRecords[0].Adsresult != null)
            {
                AdscountList = (BarGraphRecords[0].Adsresult).ToString();
                AdscountList = AdscountList.Replace("{", "");
                AdscountList = AdscountList.Replace("}", "");
                AdscountList = AdscountList.Replace("\"", string.Empty).Trim();
                AdscountList = AdscountList.Replace("data:", string.Empty).Trim();
            }
            else
                AdscountList ="[0, 0]";

            //AdscountList = "{data:" + AdscountList + "," + "label:'Ads'}";
            string BannerCountList = "";
            if (BarGraphRecords[0].BannerResult != null)
            {
                BannerCountList = (BarGraphRecords[0].BannerResult).ToString();
                BannerCountList = BannerCountList.Replace("{", "");
                BannerCountList = BannerCountList.Replace("}", "");
                BannerCountList = BannerCountList.Replace("\"", string.Empty).Trim();
                BannerCountList = BannerCountList.Replace("data:", string.Empty).Trim();
                //      BannerCountList = "{data:" + BannerCountList + "," + "label:'Banners'}";
            }
            else
                BannerCountList = "[0,0]";

            string EventsCountList = "";
            if(BarGraphRecords[0].EventResult != null)
            {
                EventsCountList=(BarGraphRecords[0].EventResult).ToString();
                EventsCountList = EventsCountList.Replace("{", "");
                EventsCountList = EventsCountList.Replace("}", "");
                EventsCountList = EventsCountList.Replace("\"", string.Empty).Trim();
                EventsCountList = EventsCountList.Replace("data:", string.Empty).Trim();
                //      EventsCountList = "{data:" + EventsCountList + "," + "label:'Events'}";
            }
            else
                EventsCountList = "[0,0]";

            string PlanCountList = "";
            if (BarGraphRecords[0].PlanResult != null)
            {
                PlanCountList = (BarGraphRecords[0].PlanResult).ToString();
                PlanCountList = PlanCountList.Replace("{", "");
                PlanCountList = PlanCountList.Replace("}", "");
                PlanCountList = PlanCountList.Replace("\"", string.Empty).Trim();
                PlanCountList = PlanCountList.Replace("data:", string.Empty).Trim();
            }
            else
                PlanCountList = "[0,0]";
            //
            string RevenuCountList = "";
            if (BarGraphRecords[0].RevenueResult != null)
            {
                RevenuCountList = (BarGraphRecords[0].RevenueResult).ToString();
                RevenuCountList = RevenuCountList.Replace("{}", "0");
                RevenuCountList = RevenuCountList.Replace("{", "");
                RevenuCountList = RevenuCountList.Replace("}", "");
                RevenuCountList = RevenuCountList.Replace("\"", string.Empty).Trim();
                RevenuCountList = RevenuCountList.Replace("data:", string.Empty).Trim();
            }
            else
                RevenuCountList = "[0,0]";

            string smallAdsCountList=""; 
            if (BarGraphRecords[0].SmallAdsResult != null)
            {
                smallAdsCountList = (BarGraphRecords[0].SmallAdsResult).ToString();
                smallAdsCountList = smallAdsCountList.Replace("{", "");
                smallAdsCountList = smallAdsCountList.Replace("}", "");
                smallAdsCountList = smallAdsCountList.Replace("\"", string.Empty).Trim();
                smallAdsCountList = smallAdsCountList.Replace("data:", string.Empty).Trim();
            }
            else
                smallAdsCountList = "[0,0]";

            string BuySellCountList = "";
            if (BarGraphRecords[0].BuySellResults != null)
            {
                BuySellCountList  =(BarGraphRecords[0].BuySellResults).ToString();
                BuySellCountList = BuySellCountList.Replace("{", "");
                BuySellCountList = BuySellCountList.Replace("}", "");
                BuySellCountList = BuySellCountList.Replace("\"", string.Empty).Trim();
                BuySellCountList = BuySellCountList.Replace("data:", string.Empty).Trim();
            }
            else
                BuySellCountList = "[0,0]";

            string JobsCountList = "";
            if (BarGraphRecords[0].JobsResults != null)
            {
                JobsCountList=(BarGraphRecords[0].JobsResults).ToString();
                JobsCountList = JobsCountList.Replace("{", "");
                JobsCountList = JobsCountList.Replace("}", "");
                JobsCountList = JobsCountList.Replace("\"", string.Empty).Trim();
                JobsCountList = JobsCountList.Replace("data:", string.Empty).Trim();
            }
            else
                JobsCountList = "[0,0]";

            string BlogsCountList = ""; 
            if (BarGraphRecords[0].BlogsResults != null)
            {
                BlogsCountList=(BarGraphRecords[0].BlogsResults).ToString();
                BlogsCountList = BlogsCountList.Replace("{", "");
                BlogsCountList = BlogsCountList.Replace("}", "");
                BlogsCountList = BlogsCountList.Replace("\"", string.Empty).Trim();
                BlogsCountList = BlogsCountList.Replace("data:", string.Empty).Trim();
            }
            else
                BlogsCountList = "[0,0]";
            DataTable CountList = new DataTable();
            CountList.Columns.Add("BusinessAdscountList", typeof(string));
            CountList.Columns.Add("BannerCountList", typeof(string));
            CountList.Columns.Add("EventsCountList", typeof(string));
            CountList.Columns.Add("PlanCountList", typeof(string));
            CountList.Columns.Add("RevenuCountList", typeof(string));
            CountList.Columns.Add("smallAdsCountList", typeof(string));
            CountList.Columns.Add("BuySellCountList", typeof(string));
            CountList.Columns.Add("JobsCountList", typeof(string));
            CountList.Columns.Add("BlogsCountList", typeof(string));
            CountList.Rows.Add(AdscountList, BannerCountList, EventsCountList,PlanCountList,RevenuCountList,smallAdsCountList,BuySellCountList,JobsCountList,BlogsCountList);

             CountList.AcceptChanges();
             return CountList;          
        }

        #endregion
        #region BU Dashboard
        public class BUDashBoardDal
        {
            public int FreeAds { get; set; }
            public int PaidAds { get; set; }
            public int ApprovedAds { get; set; }
            public int RejectedAds { get; set; }
            public int PendingAds { get; set; }
            public int FreeBanner { get; set; }
            public int PaidBanner { get; set; }

            public int ApprovedBanner { get; set; }
            public int RejectedBanner { get; set; }
            public int PendingBanner { get; set; }

            public int FreeEvents { get; set; }
            public int PaidEvents { get; set; }
            public int ApprovedEvents { get; set; }
            public int RejectedEvents { get; set; }
            public int PendingEvents { get; set; }
            public int FreeJobs { get; set; }
            public int PaidJobs { get; set; }

            public int ApprovedJobs { get; set; }
            public int RejectedJobs { get; set; }
            public int PendingJobs { get; set; }
            public int AdsImageCount { get; set; }
            public int BannerImageCount { get; set; }

            public int EventImageCount { get; set; }
            public int LogoCount { get; set; }
            public int JobsAdvCount { get; set; }

        }

        public DataTable GeBUtotalCount(long UserId)
        {
            List<Offshore_ArabchaldoContext.BUDashBoardCountList> BUdahsboardCountList = new List<Offshore_ArabchaldoContext.BUDashBoardCountList>();
            BUdahsboardCountList = Acdcontext.ssp_GetBUDashBoardCountList(UserId);
            DataTable BUDashBoardCount = new DataTable();
            BUDashBoardCount.Columns.Add("ActiveBusiLitingCount", typeof(int));
            BUDashBoardCount.Columns.Add("PendingBusiLitingCount", typeof(int));
            BUDashBoardCount.Columns.Add("ActiveSmallAdsCount", typeof(int));
            BUDashBoardCount.Columns.Add("PendingSmallAdsCount", typeof(int));
            BUDashBoardCount.Columns.Add("ActiveBannerCount", typeof(int));
            BUDashBoardCount.Columns.Add("PendingBannerCount", typeof(int));
            BUDashBoardCount.Columns.Add("ActiveEventCount", typeof(int));
            BUDashBoardCount.Columns.Add("PendingEventCount", typeof(int));
            BUDashBoardCount.Columns.Add("ActiveJobsCount", typeof(int));
            BUDashBoardCount.Columns.Add("PendingJobsCount", typeof(int));
            BUDashBoardCount.Columns.Add("ActiveBlogsCount", typeof(int));
            BUDashBoardCount.Columns.Add("PendingBlogsCount", typeof(int));
            BUDashBoardCount.Columns.Add("ActiveBuysellCount", typeof(int));
            BUDashBoardCount.Columns.Add("PendingBuysellCount", typeof(int));
            BUDashBoardCount.Columns.Add("MessageTopicCount", typeof(int));
            BUDashBoardCount.Columns.Add("MessageReplyCount", typeof(int));
            //BUDashBoardCount.Columns.Add("FreeAds", typeof(int));
            //BUDashBoardCount.Columns.Add("PaidAds", typeof(int));
            //BUDashBoardCount.Columns.Add("ApprovedAds", typeof(int));
            //BUDashBoardCount.Columns.Add("RejectedAds", typeof(int));
            //BUDashBoardCount.Columns.Add("PendingAds", typeof(int));
            //BUDashBoardCount.Columns.Add("FreeBanner", typeof(int));
            //BUDashBoardCount.Columns.Add("PaidBanner", typeof(int));
            //BUDashBoardCount.Columns.Add("ApprovedBanner", typeof(int));
            //BUDashBoardCount.Columns.Add("RejectedBanner", typeof(int));
            //BUDashBoardCount.Columns.Add("PendingBanner", typeof(int));
            //BUDashBoardCount.Columns.Add("FreeEvents", typeof(int));
            //BUDashBoardCount.Columns.Add("PaidEvents", typeof(int));
            //BUDashBoardCount.Columns.Add("ApprovedEvents", typeof(int));
            //BUDashBoardCount.Columns.Add("RejectedEvents", typeof(int));
            //BUDashBoardCount.Columns.Add("PendingEvents", typeof(int));
            //BUDashBoardCount.Columns.Add("FreeJobs", typeof(int));
            //BUDashBoardCount.Columns.Add("PaidJobs", typeof(int));
            //BUDashBoardCount.Columns.Add("ApprovedJobs", typeof(int));
            //BUDashBoardCount.Columns.Add("RejectedJobs", typeof(int));
            //BUDashBoardCount.Columns.Add("PendingJobs", typeof(int));
            BUDashBoardCount.Columns.Add("AdsImageCount", typeof(int));
            BUDashBoardCount.Columns.Add("BannerImageCount", typeof(int));
            BUDashBoardCount.Columns.Add("EventImageCount", typeof(int));
            BUDashBoardCount.Columns.Add("LogoCount", typeof(int));
            BUDashBoardCount.Columns.Add("JobsAdvCount", typeof(int));
            BUDashBoardCount.Columns.Add("BuySellCount", typeof(int));
            BUDashBoardCount.Columns.Add("SmallAdsCount", typeof(int));
            foreach (var item in BUdahsboardCountList)
            {
                BUDashBoardCount.Rows.Add(
                    item.ActiveBusiLitingCount, item.PendingBusiLitingCount, item.ActiveSmallAdsCount, item.PendingSmallAdsCount, item.ActiveBannerCount, item.PendingBannerCount,
                    item.ActiveEventCount, item.PendingEventCount, item.ActiveJobsCount, item.PendingJobsCount, item.ActiveBlogsCount, item.PendingBlogsCount,
                    item.ActiveBuysellCount, item.PendingBuysellCount, item.MessageTopicCount, item.MessageReplyCount,
                    //item.FreeAds, item.PaidAds, item.ApprovedAds, item.RejectedAds, item.PendingAds, item.FreeBanner,
                    //item.PaidBanner, item.ApprovedBanner, item.RejectedBanner, item.PendingBanner, item.FreeEvents, item.PaidEvents,
                    //item.ApprovedEvents, item.RejectedEvents,item.PendingEvents,item.FreeJobs,item.PaidJobs,item.ApprovedJobs,item.RejectedJobs,
                    //item.PendingJobs,
                    item.AdsImageCount,item.BannerImageCount,item.EventImageCount,item.LogoCount,item.JobsAdvCount,item.BuySellCount,item.SmallAdsCount);
            }

            BUDashBoardCount.AcceptChanges();
            return BUDashBoardCount;
        }
        #endregion
    }
}
