﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore.Internal;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class SubCategoryRepository : ISubCategory
    {
        private Offshore_ArabchaldoContext Acdcontext;
        public SubCategoryRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }

        public bool CheckSubCategoryExits(string SubCategoryName,long CategoryId)
        {
            SubCategory SubCategoryDetails = Acdcontext.SubCategory.FirstOrDefault(e => e.SubCategoryName == SubCategoryName && e.CategoryId== CategoryId && e.IsActive == true);
            if (SubCategoryDetails != null)
            {
                return true;
            }

            return false;
        }


        public bool DeleteSubCategory(int SubCategoryID)

        {
            SubCategory SubCategoryDetails = Acdcontext.SubCategory.FirstOrDefault(e => e.SubCategoryId == SubCategoryID && e.IsActive == true);

        AdDetails addtls = Acdcontext.AdDetails.FirstOrDefault(e => e.SubCategoryId == SubCategoryID && e.IsActive == true && e.IsVisible == true);

            if (SubCategoryDetails != null && addtls == null)
            {
                SubCategoryDetails.IsActive = false;
                Acdcontext.SubCategory.Update(SubCategoryDetails);
                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }




  
public bool UpdateSubCategory(int SubCategoryID, SubCategory updateSubCategoryDetails)
        {
            SubCategory SubCategoryDetails = Acdcontext.SubCategory.FirstOrDefault(e => e.SubCategoryId == SubCategoryID);
            SubCategory CheckSubCategoryExits = Acdcontext.SubCategory.FirstOrDefault(e => e.SubCategoryName == updateSubCategoryDetails.SubCategoryName && e.CategoryId == updateSubCategoryDetails.CategoryId && e.SubCategoryId!=SubCategoryID && e.IsActive == true);

            if (CheckSubCategoryExits == null)
            {
                SubCategoryDetails.SubCategoryId = updateSubCategoryDetails.SubCategoryId;
                SubCategoryDetails.SubCategoryName = updateSubCategoryDetails.SubCategoryName;
                SubCategoryDetails.Description = updateSubCategoryDetails.Description;
                SubCategoryDetails.CategoryName = updateSubCategoryDetails.CategoryName;
                SubCategoryDetails.ArabicName = updateSubCategoryDetails.ArabicName;
                SubCategoryDetails.CategoryId = updateSubCategoryDetails.CategoryId;
     
                SubCategoryDetails.UpdatedDate = updateSubCategoryDetails.UpdatedDate;
                SubCategoryDetails.UpdatedBy = updateSubCategoryDetails.UpdatedBy;
                SubCategoryDetails.UpdatedDate = DateTime.Now;
                Acdcontext.SaveChanges();
                Int64 subCategoryId = SubCategoryDetails.SubCategoryId;

          
         

                return true;
            }
            return false;
        }


        

        public bool UpdateSubcategoryWhenCategoryUpdated(int categoryId, string updatedCategoryName)
        {
            List<SubCategory> GetAllSubCategory = new List<SubCategory>();
            GetAllSubCategory = this.GetAllSubCategory();

            GetAllSubCategory=  Acdcontext.SubCategory.Where(e => e.CategoryId == categoryId).ToList();

            foreach (var item in GetAllSubCategory)
            {
                item.CategoryName = updatedCategoryName;
            }
            Acdcontext.SaveChanges();           
            return true;
        }

     
        public List<SubCategory> GetAllSubCategory()
        {
            List<SubCategory> allSubCategoryList = new List<SubCategory>();            
            return Acdcontext.SubCategory.Where(e => e.IsActive == true).OrderBy(e => e.UpdatedDate).ToList();
        }

        public List<SubCategory> GetAllSubCategoriesByCategoryId(int categoryId)
        {
            List<SubCategory> allSubCategoryList = new List<SubCategory>();
            return Acdcontext.SubCategory.Where(e => e.CategoryId == categoryId).Where(e => e.IsActive == true).ToList();

        }

        public SubCategory GetSubCategoryById(int SubCategoryId)
        {
            return Acdcontext.SubCategory.FirstOrDefault(e => e.SubCategoryId == SubCategoryId);
        }

        public bool GetSubCategoryCountForCategory(int categoryId)
        {
            SubCategory subCategory = Acdcontext.SubCategory.Where(e => e.CategoryId == categoryId).Where(e => e.IsActive == true).FirstOrDefault();
            if(subCategory == null)
            {
                return false;
            }
            return true;   
        }

        public bool InsertSubCategory(SubCategory SubCategorydeatils)
        {
            SubCategory CheckSubCategoryExits = Acdcontext.SubCategory.FirstOrDefault(e => e.SubCategoryName == SubCategorydeatils.SubCategoryName && e.CategoryId == SubCategorydeatils.CategoryId && e.IsActive == true);
            if (CheckSubCategoryExits == null)
            {
                SubCategorydeatils.IsActive = true;
                //SubCategorydeatils.CreatedDate = DateTime.Now;
                SubCategorydeatils.UpdatedDate = DateTime.Now;
                Acdcontext.SubCategory.Add(SubCategorydeatils);
                Acdcontext.SaveChanges();
                return true;
            }
            else
                return false;


        }


        // need to be modified and should be implemented.
        public bool CheckSubCategoryExits(int SubCategoryID)
        {
            throw new NotImplementedException();
        }



        #region Grid

        // Subcategory
        public class SubcategoryList
        {
            public long subCategoryId { get; set; }
            public string subCategoryName { get; set; }
            public string arabicName { get; set; }
            public string categoryName { get; set; }
            public long categoryId { get; set; }
            public string description { get; set; }
          
        }
        public DataTable FillSubcategoryList()
        {
            List<Offshore_ArabchaldoContext.AllSubCategorylist> subCategoryList = new List<Offshore_ArabchaldoContext.AllSubCategorylist>();
            subCategoryList = Acdcontext.ssp_GetAllSubCategoryListDetails();


            //List<SubcategoryList> subcategoryList = new List<SubcategoryList>();
            //subcategoryList = (from s in Acdcontext.SubCategory
            //             join cnt in Acdcontext.Category
            //             on s.CategoryId equals cnt.CategoryId
            //                   where s.IsActive == true
            //                   orderby s.UpdatedDate ascending
            //                   select new SubcategoryList { subCategoryId = s.SubCategoryId, categoryId = s.CategoryId, subCategoryName = s.SubCategoryName, arabicName = s.ArabicName, categoryName = cnt.CategoryName, description = s.Description }).ToList();

            DataTable dtsubcategoryList = new DataTable();
            dtsubcategoryList.Columns.Add("subCategoryId", typeof(long));
            dtsubcategoryList.Columns.Add("categoryId", typeof(long));
            dtsubcategoryList.Columns.Add("subCategoryName", typeof(string));
            dtsubcategoryList.Columns.Add("arabicName", typeof(string));
            dtsubcategoryList.Columns.Add("categoryName", typeof(string));
            dtsubcategoryList.Columns.Add("description", typeof(string));
            dtsubcategoryList.Columns.Add("IsActive", typeof(bool));
            foreach (var item in subCategoryList)
            {
                dtsubcategoryList.Rows.Add(item.subCategoryId, item.categoryId, item.subCategoryName, item.arabicName, item.categoryName, item.description,item.IsActive);
            }
            dtsubcategoryList.AcceptChanges();
            return dtsubcategoryList;
        }


        #endregion




    }
}
