﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Repository;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        protected Offshore_ArabchaldoContext Acdcontext;
    
        private IUserDetails _userdetails;
        private IManageCategory _categorydetails;
        private IBannerDetails _bannerDetails;
        private IBlogDetails _blogDetails;
        private IEventDetails _eventDetails;
        private ICountry _country;
        private IState _state;
        private ICity _city;
        private IAdDetails _adDetails;
        private ISmallAdDetails _smallAdDetails;
        private IAdImageDetails _adImageDetails;
        private IAdStatusDetails _adStatusDetails;
        private ISubCategory _subCategory;
        private ILoginDetails _loginDetails;
        private IConfiguration _ConfigDetails;
        private IRoles _roles;
        private ICommon _CommonDetails;
        private IWebsiteDetails _webdetails;
        private IPaymentPlanDetails _paymentPlanDetails;
        private ITransactionDetails _transactionDetails;
        private IMessageBoard _MessageBoard;
        private IDashboard _DashBoardDetails;
        private IBuySellDetails _BuySellDetails;

        public UnitOfWork()
        {
            Acdcontext = new Offshore_ArabchaldoContext();
        }

        //
        public void Commit()
        {
            Acdcontext.SaveChanges();
        }
     
        public IUserDetails userDetails
        {
            get
            {
                if (_userdetails == null) _userdetails = new UserDetailsRepository(Acdcontext);
                return _userdetails;
            }
        }
        public IManageCategory categoryDetails
        {
            get
            {
                if (_categorydetails == null) _categorydetails = new ManageCategoryRepository(Acdcontext);
                return _categorydetails;
            }
        }

        public IBannerDetails BannerDetails
        {
            get
            {
                if (_bannerDetails == null) _bannerDetails = new BannerRepository(Acdcontext);
                return _bannerDetails;
            }
        }


        public IBlogDetails BlogDetails
        {
            get
            {
                if (_blogDetails == null) _blogDetails = new BlogRepository(Acdcontext);
                return _blogDetails;
            }
        }
        public IEventDetails EventDetails
        {
            get
            {
                if (_eventDetails == null) _eventDetails = new EventRepository(Acdcontext);
                return _eventDetails;
            }
        }
        public ICountry Country
        {
            get
            {
                if (_country == null) _country = new CountryRepository(Acdcontext);
                return _country;
            }
        }
        public IState State
        {
            get
            {
                if (_state == null) _state = new StateRepository(Acdcontext);
                return _state;
            }
        }

        public ICity City
        {
            get
            {
                if (_city == null) _city = new CityRepository(Acdcontext);
                return _city;
            }
        }

        public IAdDetails AdDetails
        {
            get
            {
                if (_adDetails == null) _adDetails = new AdDetailsRepository(Acdcontext);
                return _adDetails;
            }
        }

        public IAdImageDetails AdImageDetails
        {
            get
            {
                if (_adImageDetails == null) _adImageDetails = new AdImageDetailsRepository(Acdcontext);
                return _adImageDetails;
            }
        }

        public ISmallAdDetails SmallAdDetails
        {
            get
            {
                if (_smallAdDetails == null) _smallAdDetails = new SmallAdDetailsRepository(Acdcontext);
                return _smallAdDetails;
            }
        }
        public IAdStatusDetails AdStatusDetails
        {
            get
            {
                if (_adStatusDetails == null) _adStatusDetails = new AdStatusDetailsRepository(Acdcontext);
                return _adStatusDetails;
            }
        }

        public ISubCategory SubCategory
        {
            get
            {
                if (_subCategory == null) _subCategory = new SubCategoryRepository(Acdcontext);
                return _subCategory;
            }
        }
        public IRoles Roles
        {
            get
            {
                if (_roles == null) _roles = new RolesRepository(Acdcontext);
                return _roles;
            }
        }

        public ILoginDetails LoginDetails
        {
            get
            {
                if (_loginDetails == null) _loginDetails = new LoginDetailsRepository(Acdcontext);
                return _loginDetails;
            }
        }
        public IConfiguration ConfigDetails
        {
            get
            {
                if (_ConfigDetails == null) _ConfigDetails = new ConfigurationRepository(Acdcontext);
                return _ConfigDetails;
            }
        }

        public ICommon CommonDetails
        {
            get
            {
                if (_CommonDetails == null) _CommonDetails = new CommonRepository(Acdcontext);
                return _CommonDetails;
            }
        }
        public IWebsiteDetails WebsiteDetails
        {
            get
            {
                if (_webdetails == null) _webdetails = new WebsiteDetailsRepository(Acdcontext);
                return _webdetails;
            }
        }
        public IPaymentPlanDetails PaymentPlan
        {
            get
            {
                if (_paymentPlanDetails == null) _paymentPlanDetails = new PaymentPlanRepository(Acdcontext);
                return _paymentPlanDetails;
            }
        }

        public IMessageBoard MessageBoard
        {
            get
            {
                if (_MessageBoard == null) _MessageBoard = new MessageBoardRepository(Acdcontext);
                return _MessageBoard;
            }
        }

        public ITransactionDetails TransactionDetails
        {
            get
            {
                if (_transactionDetails == null) _transactionDetails = new TransactionDetailsRepository(Acdcontext);
                return _transactionDetails;
            }
        }


       
        public IDashboard Dashboard
        {
            get
            {
                if (_DashBoardDetails == null) _DashBoardDetails = new DashBoardRepository(Acdcontext);
                return _DashBoardDetails;
            }
        }

        public IBuySellDetails BuySellDetails
        {
            get
            {
                if (_BuySellDetails == null) _BuySellDetails = new BuySellDetailsRepository(Acdcontext);
                return _BuySellDetails;
            }
        }
        public void Dispose()
        {
            if (Acdcontext != null)
            {
                this.Acdcontext.Dispose();
            }
        }
    }
}
