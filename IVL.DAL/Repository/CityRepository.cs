﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class CityRepository : ICity
    {
        private Offshore_ArabchaldoContext Acdcontext;
        public CityRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }

        public bool CheckCityExits(string CityName,long StateId, long CityId)
        {
            City CityDetails = Acdcontext.City.FirstOrDefault(e => e.CityName == CityName && e.CityId!=CityId && e.StateId== StateId && e.IsActive==true);
            if (CityDetails != null)
            {
                return true;
            }

            return false;
        }
        public bool DeleteCity(int CityID)
        {
            City CityDetails = Acdcontext.City.FirstOrDefault(e => e.CityId == CityID);

            if (CityDetails != null)
            {
                CityDetails.IsActive = false;
                Acdcontext.City.Update(CityDetails);
                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }
        public bool UpdateCity(int CityID, City updateCityDetails)
        {
            City CityDetails = Acdcontext.City.FirstOrDefault(e => e.CityId == CityID);

            if (updateCityDetails != null)
            {
                CityDetails.CityId = updateCityDetails.CityId;
                CityDetails.CityName = updateCityDetails.CityName;
                CityDetails.CreatedBy = updateCityDetails.CreatedBy;
                CityDetails.CreatedDate = updateCityDetails.CreatedDate;
                CityDetails.Description = updateCityDetails.Description;
                CityDetails.State = updateCityDetails.State;
                CityDetails.StateId = updateCityDetails.StateId;
                CityDetails.IsActive = true;
                CityDetails.CreatedDate = updateCityDetails.CreatedDate;
                CityDetails.CreatedBy = updateCityDetails.CreatedBy;
                CityDetails.UpdatedBy = updateCityDetails.UpdatedBy;
                CityDetails.UpdatedDate = updateCityDetails.UpdatedDate;
                CityDetails.UpdatedDate = DateTime.Now;
                Acdcontext.SaveChanges();
                Acdcontext.Database.ExecuteSqlCommand("update City Set Statename=S.Statename from Dbo.City c Inner join dbo.state s on c.stateId=s.StateId where c.statename is null");

               
                return true;
            }

            return false;
        }
        public List<City> GetAllCity()
        {
            List<City> allCityList = new List<City>();
            return Acdcontext.City.Where(e => e.IsActive == true).OrderBy(e => e.UpdatedDate).ToList();
        }
        public List<City> GetCityWithPagination(string paginationFilter)
        {

            var paginatFilter = paginationFilter.Split(",");

           int pageSize = Convert.ToInt32(paginatFilter[0]);
            int pageNumber = Convert.ToInt32(paginatFilter[1]);
            List<City> allCityList = new List<City>();
            return Acdcontext.City.Where(e => e.IsActive == true).ToList();
           //return Acdcontext.City.FromSql("Select * from 	(SELECT  ROW_NUMBER() OVER ( ORDER BY CityId desc ) AS RowNum,CityId, CityName, Description, StateId, StateName, IsActive, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy From [dbo].[City] with (Nolock)) As MatchedRecords 	where IsActive=1 order by RowNum	OFFSET (" + pageNumber+" -1) * "+pageSize+ " ROWS	FETCH NEXT " + pageSize + " ROWS ONLY").ToList();


        }

        public List<City> GetAllCitiesByStateId(int stateId)
        {
            List<City> allCityList = new List<City>();
            return Acdcontext.City.Where(e => e.StateId == stateId).Where(e => e.IsActive == true).ToList();

        }
        public City GetCityById(int CityId)
        {
            return Acdcontext.City.FirstOrDefault(e => e.CityId == CityId);
        }

        public bool InsertCity(City Citydeatils)
        {
           Citydeatils.IsActive = true;
            Citydeatils.UpdatedDate = DateTime.Now;
            Acdcontext.City.Add(Citydeatils);
            Acdcontext.SaveChanges();
            Acdcontext.Database.ExecuteSqlCommand("update City Set Statename=S.Statename from Dbo.City c Inner join dbo.state s on c.stateId=s.StateId where c.statename is null");
            
          


            return true;
        }

        #region Grid

        // City
        public class CityList
        {
            public long cityId { get; set; }
            public long StateId { get; set; }
            public string cityName { get; set; }
            public string stateName { get; set; }
            public string countryName { get; set; }
            public string description { get; set; }
        }
        public DataTable FillCityList()
        {
            List<Offshore_ArabchaldoContext.AllCitylist> cityList = new List<Offshore_ArabchaldoContext.AllCitylist>();
            cityList = Acdcontext.ssp_GetAllCityListDetails();


            //List<CityList> cityList = new List<CityList>();
            //cityList = (from c in Acdcontext.City
            //            join s in Acdcontext.State
            //            on c.StateId equals s.StateId
            //            join cnt in Acdcontext.Country
            //            on s.CountryId equals cnt.CountryId
            //            where c.IsActive == true
            //            orderby c.UpdatedDate ascending
            //            select new CityList { cityId = c.CityId, StateId = s.StateId, cityName = c.CityName, stateName = s.StateName, countryName = cnt.CountryName, description = c.Description }).ToList();

            DataTable dtcityList = new DataTable();
            dtcityList.Columns.Add("cityId", typeof(long));
            dtcityList.Columns.Add("StateId", typeof(long));
            dtcityList.Columns.Add("cityName", typeof(string));
            dtcityList.Columns.Add("stateName", typeof(string));
            dtcityList.Columns.Add("countryName", typeof(string));
            dtcityList.Columns.Add("description", typeof(string));
            dtcityList.Columns.Add("isActive", typeof(string));
            foreach (var item in cityList)
            {
                dtcityList.Rows.Add(item.cityId, item.stateId, item.cityName, item.stateName, item.countryName, item.description,item.isActive);
            }
            dtcityList.AcceptChanges();
            return dtcityList;
        }


        #endregion
    }
}
