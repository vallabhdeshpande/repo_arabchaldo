﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class MessageBoardRepository:IMessageBoard
    {
        private Offshore_ArabchaldoContext Acdcontext;
        public MessageBoardRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }

        public bool CheckMessageBoardExits(string Name,long CategoryId)
        {
            MessageBoard MessageBoard = Acdcontext.MessageBoard.FirstOrDefault(e => e.Name == Name && e.CategoryId != CategoryId && e.IsActive == true);
            if (MessageBoard != null)
            {
                return true;
            }

            return false;
        }
        public bool DeleteMessageBoard(long messageboardId)
        {
            MessageBoard MessageBoard = Acdcontext.MessageBoard.FirstOrDefault(e => e.MessageBoardId == messageboardId);

            if (MessageBoard != null)
            {
                MessageBoard.IsActive = false;
                Acdcontext.MessageBoard.Update(MessageBoard);
                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }



        public bool DeleteMessageBoardDetails(long messageBoardDetailsId)
        {
            MessageBoardDetails MessageBoardDetails = Acdcontext.MessageBoardDetails.FirstOrDefault(e =>e.MessageBoardDetailsId == messageBoardDetailsId);

            if (MessageBoardDetails != null)
            {
                MessageBoardDetails.IsActive = false;
                Acdcontext.MessageBoardDetails.Update(MessageBoardDetails);
                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }


        public bool UpdateMessageBoard(long MessageBoardID, MessageBoard updateMessageBoard)
        {
            MessageBoard MessageBoard = Acdcontext.MessageBoard.FirstOrDefault(e => e.MessageBoardId == MessageBoardID);

            if (updateMessageBoard != null)
            {
                //MessageBoard.MessageBoardId = updateMessageBoard.MessageBoardId;
                MessageBoard.Name = updateMessageBoard.Name;                          
                MessageBoard.Description = updateMessageBoard.Description;               
                MessageBoard.IsActive = true;
                MessageBoard.Name = updateMessageBoard.Name;               
                MessageBoard.UpdatedBy = updateMessageBoard.CreatedBy;
                MessageBoard.UpdatedDate = updateMessageBoard.UpdatedDate;
                MessageBoard.Views = updateMessageBoard.Views;
                Acdcontext.SaveChanges();
               
                return true;
            }

            return false;
        }
        //public List<MessageBoard> GetAllMessageBoard()
        //{
        //    List<MessageBoard> allMessageBoardList = new List<MessageBoard>();
        //    return Acdcontext.MessageBoard.Where(e => e.IsActive == true).OrderBy(n => n.UpdatedDate).ToList();
        //}

        public DataTable GetAllMessageBoard()
        {
            List<Offshore_ArabchaldoContext.AllMessageBoardlist> allMessageBoard = new List<Offshore_ArabchaldoContext.AllMessageBoardlist>();
            allMessageBoard = Acdcontext.ssp_MessageBoardData();
            DataTable dtallMessageBoard = new DataTable();
            dtallMessageBoard.Columns.Add("messageBoardId", typeof(long));
            dtallMessageBoard.Columns.Add("topic", typeof(string));
            dtallMessageBoard.Columns.Add("description", typeof(string));
            dtallMessageBoard.Columns.Add("name", typeof(string));
            dtallMessageBoard.Columns.Add("categoryId", typeof(long));
            dtallMessageBoard.Columns.Add("views", typeof(long));
            dtallMessageBoard.Columns.Add("isActive", typeof(bool));
            dtallMessageBoard.Columns.Add("createdBy", typeof(long));
            dtallMessageBoard.Columns.Add("createdDate", typeof(string));
            dtallMessageBoard.Columns.Add("updatedBy", typeof(long));
            dtallMessageBoard.Columns.Add("updatedDate", typeof(string));
            foreach (var item in allMessageBoard)
            {
                try
                {
                    dtallMessageBoard.Rows.Add(item.messageBoardId, item.topic, item.description, item.name, item.categoryId, item.views, item.IsActive, item.CreatedBy, item.CreatedDate, item.UpdatedBy, item.UpdatedDate);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            dtallMessageBoard.AcceptChanges();
            return dtallMessageBoard;
        }


            public MessageBoard GetMessageBoardById(long MessageBoardId)
        {
            return Acdcontext.MessageBoard.FirstOrDefault(e => e.MessageBoardId == MessageBoardId && e.IsActive == true);
        }

        public bool InsertMessageBoard(MessageBoard MessageBoard)
        {
            MessageBoard.IsActive = true;

            MessageBoard.UpdatedDate = DateTime.Now;
            MessageBoard.UpdatedBy = MessageBoard.CreatedBy;
            MessageBoard.CreatedDate = DateTime.Now;
           // DateTime time = DateTime.UtcNow.ToTimeZoneTime("Pacific Standard Time");
            var tz = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
           // DateTime time2 = DateTime.UtcNow.ToTimeZoneTime(tz);
            MessageBoard.Views = 0;
            Acdcontext.MessageBoard.Add(MessageBoard);
            Acdcontext.SaveChanges();
            return true;
        }


        #region MessageBoardGrid

        // MessageBoard
        public class MessageBoardList
        {
            public long MessageBoardId { get; set; }
            public long CategoryId { get; set; }
            public string Name { get; set; }
            public string CategoryName { get; set; }
            public long Views { get; set; }
            public long Replies { get; set; }
            public string Description { get; set; }
            public string PostedOn { get; set; }
        }
        public DataTable FillMessageBoardList()
        {
            List<MessageBoardList> cityList = new List<MessageBoardList>();
            //cityList = (from c in Acdcontext.MessageBoard
            //            join s in Acdcontext.State
            //            on c.StateId equals s.StateId
            //            join cnt in Acdcontext.Country
            //            on s.CountryId equals cnt.CountryId
            //            where c.IsActive == true
            //            select new MessageBoardList { cityId = c.MessageBoardId, StateId = s.StateId, cityName = c.Name, stateName = s.StateName, countryName = cnt.CountryName, description = c.Description }).ToList();

            DataTable dtcityList = new DataTable();
            //dtcityList.Columns.Add("cityId", typeof(long));
            //dtcityList.Columns.Add("StateId", typeof(long));
            //dtcityList.Columns.Add("cityName", typeof(string));
            //dtcityList.Columns.Add("stateName", typeof(string));
            //dtcityList.Columns.Add("countryName", typeof(string));
            //dtcityList.Columns.Add("description", typeof(string));
            //foreach (var item in cityList)
            //{
            //    dtcityList.Rows.Add(item.cityId, item.StateId, item.cityName, item.stateName, item.countryName, item.description);
            //}
            //dtcityList.AcceptChanges();
            return dtcityList;
        }


        #endregion






        // Message Board Details
        public List<MessageBoardDetails> GetMessageBoardReplyByMessageBoardId(long MessageBoardId)
        {
            List<MessageBoardDetails> allMessageBoardList = new List<MessageBoardDetails>();
            return Acdcontext.MessageBoardDetails.Where(e => e.MessageBoardId == MessageBoardId && e.IsActive == true).OrderBy(n => n.UpdatedDate).ToList();

        }
        public bool InsertMessageBoardDetails(MessageBoardDetails MessageBoardDetails)
        {
            MessageBoardDetails.IsActive = true;
            MessageBoardDetails.UpdatedDate = DateTime.Now;
            MessageBoardDetails.UpdatedBy = MessageBoardDetails.CreatedBy;
            MessageBoardDetails.CreatedDate = DateTime.Now;
            Acdcontext.MessageBoardDetails.Add(MessageBoardDetails);
            Acdcontext.SaveChanges();
            return true;
        }


        #region MessageBoardGrid

        // MessageBoard
        public class MessageBoardReplyList
        {
            public long MessageBoardId { get; set; }          
            public string Topic { get; set; }
            public string Description { get; set; }
            public string Name { get; set; }
            public DateTime CreatedDate { get; set; }
            public string ProfilePicUrl { get; set; }
            public long CreatedBy { get; set; }

        }
        public DataTable FillMessageBoardReplyList(long MessageBoardId)
        {
            List<MessageBoard> mbView = new List<MessageBoard>();
           // MessageBoard updateMBViewCount = new MessageBoard();
            mbView = Acdcontext.MessageBoard.Where(e => e.IsActive == true  && e.MessageBoardId == MessageBoardId)
                                              .Select(s => new MessageBoard() { Views = s.Views }).ToList();
            long counter = Convert.ToInt64(mbView[0].Views) + 1;
           // updateMBViewCount.Views = counter;
            MessageBoard updateMBViewCount = Acdcontext.MessageBoard.Single(e => e.MessageBoardId== MessageBoardId && e.IsActive==true);
            updateMBViewCount.Views = counter;
            Acdcontext.SaveChanges();
            List<MessageBoardReplyList> messageBoardReplyList = new List<MessageBoardReplyList>();
            List<MessageBoardReplyList> messageBoardDetailsReplyList = new List<MessageBoardReplyList>();
            messageBoardReplyList = ((from mb in Acdcontext.MessageBoard
                                     // join u in Acdcontext.UserDetails  on mb.CreatedBy equals u.UserId
                                      where mb.IsActive == true && mb.MessageBoardId== MessageBoardId orderby mb.UpdatedDate
                                      select new MessageBoardReplyList { MessageBoardId = mb.MessageBoardId, Topic = mb.Topic,Name = mb.Name, Description = mb.Description, CreatedDate = mb.CreatedDate, ProfilePicUrl="", CreatedBy=0}).Concat

                        (from mbd in Acdcontext.MessageBoardDetails
                        // join u in Acdcontext.UserDetails   on mbd.CreatedBy equals u.UserId
                         where mbd.IsActive == true && mbd.MessageBoardId == MessageBoardId
                         orderby mbd.UpdatedDate
                         select new MessageBoardReplyList { MessageBoardId = mbd.MessageBoardDetailsId,  Topic = "", Name = mbd.Name, Description = mbd.Description, CreatedDate = mbd.CreatedDate, ProfilePicUrl = "", CreatedBy = 0 }).ToList())
                                      .ToList();

            DataTable dtmessageBoardReplyList = new DataTable();
            dtmessageBoardReplyList.Columns.Add("MessageBoardId", typeof(long));
       
            dtmessageBoardReplyList.Columns.Add("Topic", typeof(string));
            dtmessageBoardReplyList.Columns.Add("Name", typeof(string));
            dtmessageBoardReplyList.Columns.Add("Description", typeof(string));
            dtmessageBoardReplyList.Columns.Add("CreatedDate", typeof(DateTime));
            dtmessageBoardReplyList.Columns.Add("ProfilePicUrl", typeof(string));
            dtmessageBoardReplyList.Columns.Add("CreatedBy", typeof(long));
            foreach (var item in messageBoardReplyList)
            {
                dtmessageBoardReplyList.Rows.Add(item.MessageBoardId, item.Topic, item.Name, item.Description, item.CreatedDate,item.ProfilePicUrl,item.CreatedBy);
            }
            dtmessageBoardReplyList.AcceptChanges();
            return dtmessageBoardReplyList;
        }


        #endregion



    }
}

