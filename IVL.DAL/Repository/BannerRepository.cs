﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using System.Linq;
using System.IO;
using System.Data;

namespace IVL.ArabChaldo.DAL.Repository
{
    public class BannerRepository : IBannerDetails
    {
        private Offshore_ArabchaldoContext _Offshore_ArabchaldoContext;

        public BannerRepository(Offshore_ArabchaldoContext context)
        {
            _Offshore_ArabchaldoContext = context;
        }

        public bool Delete(long id)
        {
            BannerDetails bannerAvailable = IsBannerIdExist(id);
            if (bannerAvailable != null)
            {
                bannerAvailable.IsActive = false;
                return true;
            }

            return false;
        }

        public DataTable GetAllBanner( int userId)
        {
            //List<Offshore_ArabchaldoContext.PaymentPlanList> paymentPlanListsSSP = new List<Offshore_ArabchaldoContext.PaymentPlanList>();
            //paymentPlanListsSSP = _Offshore_ArabchaldoContext.ssp_GetAllPlanListDetails();

            //BannerList = _Offshore_ArabchaldoContext.ssp_GetAllBannerListDetails(userId);
            List<Offshore_ArabchaldoContext.BannerList> BannerList = new List<Offshore_ArabchaldoContext.BannerList>();
            BannerList = _Offshore_ArabchaldoContext.ssp_GetAllBannerListDetails(userId);

            

            DataTable dtbannerList = new DataTable();
            dtbannerList.Columns.Add("bannerId", typeof(long));
            dtbannerList.Columns.Add("postedFor", typeof(long));
            dtbannerList.Columns.Add("title", typeof(string));
            dtbannerList.Columns.Add("cityName", typeof(string));
            dtbannerList.Columns.Add("createdDate", typeof(string));
            dtbannerList.Columns.Add("validFromDate", typeof(string));
            dtbannerList.Columns.Add("validTillDate", typeof(string));
            dtbannerList.Columns.Add("isFeatured", typeof(bool));
            dtbannerList.Columns.Add("isVisible", typeof(bool));
            dtbannerList.Columns.Add("isActive", typeof(bool));

            foreach (var item in BannerList)
            {
                dtbannerList.Rows.Add(item.BannerId, item.PostedFor, item.Title, item.CityName, item.CreatedDate, item.ValidFromDate,
                    item.ValidTillDate, item.IsFeatured, item.IsVisible, item.IsActive);
            }
            dtbannerList.AcceptChanges();
            
            return dtbannerList;

            //if (userId ==0)
            //{


            //    List<BannerDetails> bannerDetails = new List<BannerDetails>();
            //    // bannerDetails = _Offshore_ArabchaldoContext.BannerDetails.ToList().FindAll(e=>e.IsActive == true);

            //    // added for order by descending
            //    try
            //    {

            //        // bannerDetails = _Offshore_ArabchaldoContext.BannerDetails.Where(e => e.IsActive == true).OrderBy(d => d.UpdatedDate).ToList();

            //        bannerDetails = (_Offshore_ArabchaldoContext.BannerDetails.Where(e => e.IsActive == true  && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
            //        (_Offshore_ArabchaldoContext.BannerDetails.Where(e => e.IsActive == true  && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).Concat
            //        (_Offshore_ArabchaldoContext.BannerDetails.Where(e => e.IsActive == true && e.IsVisible == false ).ToList()).OrderBy(n => n.UpdatedDate).ToList());
            //    }
            //    catch( Exception ex)
            //    {
            //        throw ex;
            //    }
            //    return bannerDetails;
            //}

            //else
            //{
            //    List<BannerDetails> bannerDetails = new List<BannerDetails>();
            //    // bannerDetails = _Offshore_ArabchaldoContext.BannerDetails.ToList().FindAll(e=>e.IsActive == true);

            //    // added for order by descending
            //    // bannerDetails = _Offshore_ArabchaldoContext.BannerDetails.Where(e => e.PostedFor == userId && e.IsActive == true ).OrderBy(d => d.UpdatedDate).ToList();
            //    bannerDetails = (_Offshore_ArabchaldoContext.BannerDetails.Where(e => e.PostedFor == userId && e.IsActive == true && e.IsVisible == null && e.ValidFromDate == null && e.ValidTillDate == null).ToList().Concat
            //         (_Offshore_ArabchaldoContext.BannerDetails.Where(e => e.PostedFor == userId && e.IsActive == true && e.IsVisible == true && e.ValidTillDate >= DateTime.Today).ToList()).Concat
            //         (_Offshore_ArabchaldoContext.BannerDetails.Where(e => e.PostedFor == userId && e.IsActive == true && e.IsVisible == false ).ToList()).OrderBy(n => n.UpdatedDate).ToList());
            //    return bannerDetails;
            //}
        }

        public BannerDetails GetBannerById(long id) 
        {
            BannerDetails bannerIdExist = _Offshore_ArabchaldoContext.BannerDetails.FirstOrDefault(e => e.BannerId == id);
            if (bannerIdExist != null)
            {
                return bannerIdExist;
            }
            return null;
        }

        public bool Insert(BannerDetails bannerDetails)
        {
            BannerDetails isExist = IsBannerIdExist(bannerDetails.BannerId);
            if (isExist == null)
            {
                bannerDetails.UpdatedDate = DateTime.Now;
                bannerDetails.UpdatedBy = bannerDetails.CreatedBy;
                _Offshore_ArabchaldoContext.BannerDetails.Add(bannerDetails);
                _Offshore_ArabchaldoContext.SaveChanges();
                 long BannerId = bannerDetails.BannerId;
                //Save image in directory
                if (bannerDetails.ImageUrl != null)
                    ManageUploads(BannerId.ToString(), bannerDetails.ImageUrl, "Image");
                // Workflow for Created state
                ManageWorkflowDetails(BannerId, 1, "", bannerDetails.CreatedBy);
                return true;
            }
            return false;
        }

        public bool Update(long bannerId, BannerDetails bannerDetails)
        {
            BannerDetails isExist = IsBannerIdExist(bannerId);
            if (isExist != null)
            {
                isExist.BannerId = bannerDetails.BannerId;
                isExist.Title = bannerDetails.Title;
                isExist.Description = bannerDetails.Description;
                long BannerId = bannerDetails.BannerId;
                if (bannerDetails.ImageUrl != null && (isExist.ImageUrl != bannerDetails.ImageUrl))
                {
                    
                    ManageUploads(BannerId.ToString(), bannerDetails.ImageUrl, "Image");
                  
                }
                isExist.ImageUrl = bannerDetails.ImageUrl;
                isExist.AddressStreet1 = bannerDetails.AddressStreet1;
                isExist.AddressStreet2 = bannerDetails.AddressStreet2;
                isExist.CityId = bannerDetails.CityId;
                isExist.CityName = bannerDetails.CityName;
                isExist.CountryId = bannerDetails.CountryId;
                isExist.CountryName = "USA";
                isExist.StateId = bannerDetails.StateId;
                isExist.StateName = bannerDetails.StateName;
                isExist.IsActive = bannerDetails.IsActive;
                isExist.IsVisible = null;
                isExist.ValidFromDate = null;
                isExist.ValidTillDate = null;
                isExist.UpdatedBy = bannerDetails.CreatedBy;
                isExist.UpdatedDate = DateTime.Now; ;
                isExist.PostedFor = bannerDetails.PostedFor;
                isExist.PostedForName = bannerDetails.PostedForName;
                isExist.ValidTillDate = bannerDetails.ValidTillDate;
                isExist.ContactNumber = bannerDetails.ContactNumber;
                isExist.AlternateContactNumber = bannerDetails.AlternateContactNumber;
                isExist.ContactPersonName = bannerDetails.ContactPersonName;
                isExist.Website = bannerDetails.Website;
                isExist.FaxNumber = bannerDetails.FaxNumber;
                isExist.Email = bannerDetails.Email;
                isExist.CountryCodeContact = bannerDetails.CountryCodeContact;
                isExist.ZipCode = bannerDetails.ZipCode;
                isExist.OtherCity = bannerDetails.OtherCity;
                //isExist.FaxNumber= bannerDetails.faxNumber;
                _Offshore_ArabchaldoContext.SaveChanges();
                // Checking the current state is Rejected
                long PostDetailsId = GetPostDetailsId(BannerId, 4, 2, bannerDetails.CreatedBy); // 2 == Banner
                PostStatusDetails CheckRejected = _Offshore_ArabchaldoContext.PostStatusDetails.FirstOrDefault(e => e.PostDetailsId == PostDetailsId && e.IsActive == true && e.WorkflowStatusId == 4);
                if (CheckRejected != null)
                {
                    ManageWorkflowDetails(BannerId, 5, "", bannerDetails.CreatedBy); // Rectified
                }
                else
                    ManageWorkflowDetails(BannerId, 2, "", bannerDetails.CreatedBy); //Modified
                _Offshore_ArabchaldoContext.SaveChanges();

                return true;
                //Save image in directory

            }
            return false;
        }

        private BannerDetails IsBannerIdExist(long id)
        {
            BannerDetails bannerIdExist = _Offshore_ArabchaldoContext.BannerDetails.FirstOrDefault(e => e.BannerId == id);
            if (bannerIdExist != null)
            {
                return bannerIdExist;
            }
            return null;
        }

        // Delete, Approve and Reject
        public bool BannerStatus(long id, string Remark, string Action, long CreatedBy)
        {
            BannerDetails bannerDetails = _Offshore_ArabchaldoContext.BannerDetails.FirstOrDefault(e => e.BannerId == id);
            int StatusId = 0;
            bool isActive = true;
            bool isVisible = true;
            string remark = "";
            if (bannerDetails != null && Action == "Delete" && bannerDetails.IsVisible == true)
            {
                return false;
            }

            else if(bannerDetails != null && Action == "Delete" && bannerDetails.IsVisible != true)
            {
                isActive = false;
                isVisible = false;
                StatusId = 6;
                //remark = Remark;
            }
            else if (bannerDetails != null && Action == "Approve")
            {
                isActive = true;
                isVisible = true;
                StatusId = 3;
                remark = Remark.Split(',')[2];
                bannerDetails.ValidFromDate = Convert.ToDateTime(Remark.Split(',')[0]);
                bannerDetails.ValidTillDate = Convert.ToDateTime(Remark.Split(',')[1]);
                //bannerDetails.IsFeatured = Convert.ToBoolean(Remark.Split(',')[3]);
            }
            else if (bannerDetails != null && Action == "Reject")
            {
                isActive = true;
                isVisible = false;
                StatusId = 4;
                remark = Remark.Split(',')[2];
            }
            bannerDetails.UpdatedDate = DateTime.Now;
            bannerDetails.UpdatedBy = CreatedBy;
            bannerDetails.IsActive = isActive;
            bannerDetails.IsVisible = isVisible;
            _Offshore_ArabchaldoContext.BannerDetails.Update(bannerDetails);
            _Offshore_ArabchaldoContext.SaveChanges();
            ManageWorkflowDetails(id, StatusId, remark, CreatedBy);
            _Offshore_ArabchaldoContext.SaveChanges();
            return true;


        }




        #region Grid

        // Banner
        public class BannerList
        {
            public long BannerId { get; set; }
            public long PostedFor { get; set; }
            public string ImageUrl { get; set; }
            public DateTime ValidTill { get; set; }
            public string title { get; set; }
            public string Description { get; set; }
        }

        /// <summary>
        /// Banner List for SuperAdmin and BU. BE= BackEnd
        /// </summary>
        public class BannerListForBE
        {
            public long BannerId { get; set; }
            public long PostedFor { get; set; }
            public string Title { get; set; }
            public string CityName { get; set; }
            public bool IsFeatured { get; set; }
            public bool IsVisible { get; set; }
            public bool IsActive { get; set; }
            public string CreatedDate { get; set; }
            public string ValidFromDate { get; set; }
            public string ValidTillDate { get; set; }
        }

        public DataTable FillBannerList()
        {
            List<BannerList> bannerList = new List<BannerList>();
            bannerList = (from b in _Offshore_ArabchaldoContext.BannerDetails
                          join u in _Offshore_ArabchaldoContext.UserDetails
                           on b.PostedFor equals u.UserId
                          where b.IsActive == true
                            select new BannerList { BannerId = b.BannerId, PostedFor = b.PostedFor, ImageUrl = b.ImageUrl, title = b.Title, Description = b.Description }).ToList();

            DataTable dtbannerList = new DataTable();
            dtbannerList.Columns.Add("BannerId", typeof(long));
            dtbannerList.Columns.Add("ImageUrl", typeof(string));
            dtbannerList.Columns.Add("PostedFor", typeof(string));
            dtbannerList.Columns.Add("validTill", typeof(DateTime));
            dtbannerList.Columns.Add("title", typeof(string));
            dtbannerList.Columns.Add("Description", typeof(string));
            foreach (var item in bannerList)
            {
                dtbannerList.Rows.Add(item.BannerId, item.ImageUrl,item.PostedFor, item.ValidTill, item.title, item.Description);
            }
            dtbannerList.AcceptChanges();
            return dtbannerList;
        }


        #endregion



        #region Workflow&Image

        //Create Directory If not exist
        public void ManageUploads(string path, string FileUrl, string UploadType)
        {
            string AdDirectory = "StaticFiles\\Banners\\" + path + "\\" + UploadType;
            if (!Directory.Exists(AdDirectory))
            {
                Directory.CreateDirectory(AdDirectory);
            }
            var SourcefolderName = Path.Combine("StaticFiles", "Images");
            var Sourcepath = Path.Combine(Directory.GetCurrentDirectory(), SourcefolderName);
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), AdDirectory);
            var SourceFile = Sourcepath + "\\" + FileUrl;
            var DestinationFile = pathToSave + "\\" + FileUrl;
            if (!File.Exists(pathToSave + FileUrl))
            {
                File.Move(SourceFile, DestinationFile);
            }
        }

        // Insert Workflow Details
        public void ManageWorkflowDetails(long bannerId, int status, string Remark, long CreatedBy)
        {
            long PostDetailsId = GetPostDetailsId(bannerId, status, 2, CreatedBy); // 2 == posttype Banner

            if (PostDetailsId > 0)
            {


                PostStatusDetails postStatusDetails = new PostStatusDetails();
                postStatusDetails.PostDetailsId = PostDetailsId;
                postStatusDetails.IsActive = true;
                postStatusDetails.CreatedBy = CreatedBy;
                postStatusDetails.CreatedDate = DateTime.Now;
                postStatusDetails.Remark = Remark;
                if (status == 1)
                {
                    postStatusDetails.WorkflowStatusId = status;
                }

                else
                {
                    _Offshore_ArabchaldoContext.PostStatusDetails.Where(x => x.PostDetailsId == postStatusDetails.PostDetailsId && x.IsActive == true).ToList().ForEach(x =>
                    {
                        x.IsActive = false; x.UpdatedDate = DateTime.Now;
                    });
                    postStatusDetails.WorkflowStatusId = status;
                }

                _Offshore_ArabchaldoContext.PostStatusDetails.Add(postStatusDetails);
                _Offshore_ArabchaldoContext.SaveChanges();
            }
        }


        // Insert PostDetails
        public long GetPostDetailsId(long BannerId, int status, int PostTypeId, long CreatedBy)
        {
            long PostDetailsId = 0;
            PostDetails postDetails = new PostDetails();
            postDetails = _Offshore_ArabchaldoContext.PostDetails.FirstOrDefault(e => e.PostId == BannerId && e.PostTypeId == PostTypeId && e.IsActive == true);
            if (postDetails == null)
            {
                PostDetails addpostDetails = new PostDetails();
                addpostDetails.PostId = BannerId;
                addpostDetails.PostTypeId = PostTypeId;
                addpostDetails.IsActive = true;
                addpostDetails.CreatedBy = CreatedBy;
                addpostDetails.CreatedDate = DateTime.Now;
                _Offshore_ArabchaldoContext.PostDetails.Add(addpostDetails);
                _Offshore_ArabchaldoContext.SaveChanges();
                PostDetailsId = addpostDetails.PostDetailsId;
            }
            else
                PostDetailsId = postDetails.PostDetailsId;
            return PostDetailsId;
        }



        #endregion
    }
}
