﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace IVL.ArabChaldo.DAL.Repository
{
   public class ConfigurationRepository: IConfiguration
    {
        private Offshore_ArabchaldoContext Acdcontext;

        public ConfigurationRepository(Offshore_ArabchaldoContext context)
        {
            Acdcontext = context;

        }


   
        public bool DeleteConfig(int Id)
        {
            Configuration ConfigDetails = Acdcontext.Configuration.FirstOrDefault(e => e.ConfigurationId == Id);

            if (ConfigDetails != null)
            {
                ConfigDetails.IsActive = false;
                Acdcontext.Configuration.Update(ConfigDetails);
                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }
        public bool UpdateConfig(int Id, Configuration updateDetails)
        {
            Configuration Configdetails = Acdcontext.Configuration.FirstOrDefault(e => e.ConfigurationId == Id);
            updateDetails.IsActive = true;
            if (Configdetails != null)
            {
                Configdetails.ApproveMailTemplate = updateDetails.ApproveMailTemplate;
                Configdetails.BannerCharge = updateDetails.BannerCharge;
                Configdetails.ChangePasswordMailTemplate = updateDetails.ChangePasswordMailTemplate;
                Configdetails.CreatedBy = updateDetails.CreatedBy;
                Configdetails.CreatedDate = updateDetails.CreatedDate;
                Configdetails.FromMailId = updateDetails.FromMailId;
                Configdetails.ImageCharge = updateDetails.ImageCharge;
                Configdetails.IsActive = updateDetails.IsActive;
                Configdetails.LogoCharge = updateDetails.LogoCharge;
                Configdetails.Password = updateDetails.Password;
                Configdetails.Port = updateDetails.Port;
                Configdetails.Domain = updateDetails.Domain;
                Configdetails.ContactMailTemplate = updateDetails.ContactMailTemplate;
                Configdetails.FeedbackMailTemplate = updateDetails.FeedbackMailTemplate;
                Configdetails.PostMailTemplate = updateDetails.PostMailTemplate;
                Configdetails.JobsMailTemplate = updateDetails.JobsMailTemplate;
                Configdetails.RegistrationMailTemplate = updateDetails.RegistrationMailTemplate;
                Configdetails.RejectMailTemplate = updateDetails.RejectMailTemplate;
                Configdetails.ResetPasswordMailTemplate = updateDetails.ResetPasswordMailTemplate;
                Configdetails.Smtp = updateDetails.Smtp;
                Configdetails.UpdatedDate = updateDetails.UpdatedDate;
                Configdetails.UpdatedBy = updateDetails.UpdatedBy;
                Configdetails.AboutUstemplate = updateDetails.AboutUstemplate;
                Configdetails.ContactUstemplate = updateDetails.ContactUstemplate;
                Configdetails.PrivacyPolicyTemplate = updateDetails.PrivacyPolicyTemplate;
                Configdetails.TermsOfUseTemplate = updateDetails.TermsOfUseTemplate;
                Configdetails.Faqtemplate = updateDetails.Faqtemplate;
                Configdetails.DisclaimerTemplate = updateDetails.DisclaimerTemplate;
                Acdcontext.SaveChanges();
                return true;
            }

            return false;
        }
        public List<Configuration> GetAllConfigDetails()
        {
            List<Configuration> allConfigList = new List<Configuration>();

            //allUserList = Acdcontext.User.ToList();

            return Acdcontext.Configuration.Where(e => e.IsActive == true).OrderByDescending(n => n.ConfigurationId).ToList();
        }

        public Configuration GetConfigDetailsbyId(int ConfigurationId)
        {
            return Acdcontext.Configuration.FirstOrDefault(e => e.IsActive == true);
        }
        public Configuration GetExistingConfigDetails()
        {
            return Acdcontext.Configuration.FirstOrDefault(e => e.IsActive == true);
        }

    }
}
