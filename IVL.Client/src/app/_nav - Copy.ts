interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
 

  
  {
    title: true,
    name: 'Components'
  },
  {
    name: 'Manage User',
    url: '/theme/manageuser',
    icon: 'icon-star'
  },
  {
    name: 'Manage Ads',
    url: '/theme/managead',
    icon: 'icon-star'
  },
  {
    name: 'Add New User',
    url: '/theme/adduser',
    icon: 'icon-star'
  },

  {
    name: 'Post New Ad',
    url: '/theme/postad',
    icon: 'icon-star'
  },

  {
    name: 'Mange Category',
    url: '/theme/managecategory',
    icon: 'icon-star'
  },
  {
    name: 'Manage Sub Category',
    url: '/theme/managesubcategory',
    icon: 'icon-star'
  },

  {
    name: 'Add State',
    url: '/theme/addstate',
    icon: 'icon-star'
  },
  {
    name: 'Add City',
    url: '/theme/addcity',
    icon: 'icon-star'
  },

     
      
  {
    name: 'Reset Password',
    url: '/theme/resetpassword',
    icon: 'icon-star'
  }
  
     
     
     
     
    
  
  
  
  
     
      
      
    
  
 
 
];
