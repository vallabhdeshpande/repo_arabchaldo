interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Home',
    url: '/website',
    icon: 'icon-home'
  },
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    name: 'Users',
    url: '/theme/manageuser',
    icon: 'icon-people'
  }, 
  {
    name: 'Business Listings',
    url: '/theme/managead',
    icon: 'icon-doc' 
  },
  {
    name: 'Current Promotions Ad',
    url: '/theme/managesmallad',
    icon: 'icon-docs'
  },
  //{
  //  name: 'Post Advertisement',
  //  url: '/theme/postsmallad',
  //  icon: 'icon-docs'
  //},
  {
    name: 'Top Banner/Featured Ads',
    url: '/theme/managebanner',
    icon: 'icon-picture'
  },
  {
    name: 'Events',
    url: '/theme/manageevent',
    icon: 'icon-calendar'
  },
  
  {
    name: 'Blogs',
    url: '/theme/manageblog',
    icon: 'icon-note'
  },
  {
    name: 'Jobs',
    url: '/theme/managejobs',
    icon: 'icon-briefcase'
  },
  {
    name: 'Buy/Sell',
    url: '/theme/managebuysell',
    icon: 'icon-tag'
  },
  {
    name: 'Category',
    url: '/theme/managecategory',
    icon: 'icon-grid'
  },
  {
    name: 'Sub Category',
    url: '/theme/managesubcategory',
    icon: 'icon-list'
  }, 
  {
    name: 'State',
    url: '/theme/addstate',
    icon: 'icon-map'
  },
  {
    name: 'City',
    url: '/theme/addcity',
    icon: 'icon-location-pin'
  },
  
  {
    name: 'Settings',
    url: '/theme/setting',
    icon: 'icon-settings'
  },
  //{
  //  name: 'Create Plan',
  //  url: '/theme/createplan',
  //  icon: 'icon-tag'
  //},
 
  {
    name: 'Create & Assign Plan',
    url: '/theme/assignplan',
    icon: 'icon-user-following'
  },
  //{
  //  name: 'Stripe payment',
  //  url: '/theme/stripe',
  //  icon: 'icon-user-following'
  //},
  {
    name: 'Post Message',
    url: '/theme/inboxmessage',
    icon: 'icon-envelope-letter'
  },
  {
    name: 'Message Board',
    url: '/theme/message-board',
    icon: 'icon-bubbles'
  },
];


export const adminNavItems: NavData[] = [
  {
    name: 'Home',
    url: '/website',
    icon: 'icon-home'
  },
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    name: 'Users',
    url: '/theme/manageuser',
    icon: 'icon-people'
  }, 
  {
    name: 'Business Listings',
    url: '/theme/managead',
    icon: 'icon-docs'
  },
  {
    name: 'Current Promotions Ad',
    url: '/theme/managesmallad',
    icon: 'icon-docs'
  },
  {
    name: 'Top Banner/Featured Ads',
    url: '/theme/managebanner',
    icon: 'icon-picture'
  },
  {
    name: 'Events',
    url: '/theme/manageevent',
    icon: 'icon-picture'
  }, 
  {
    name: 'Blogs',
    url: '/theme/manageblog',
    icon: 'icon-note'
  },
  {
    name: 'Jobs',
    url: '/theme/managejobs',
    icon: 'icon-briefcase'
  },
  {
    name: 'Category',
    url: '/theme/managecategory',
    icon: 'icon-grid'
  },
  {
    name: 'Sub Category',
    url: '/theme/managesubcategory',
    icon: 'icon-list'
  },
  {
    name: 'State',
    url: '/theme/addstate',
    icon: 'icon-map'
  },
  {
    name: 'City',
    url: '/theme/addcity',
    icon: 'icon-location-pin'
  },
  {
    name: 'Create & Assign Plan',
    url: '/theme/assignplan',
    icon: 'icon-user-following'
  },
  {
    name: 'Message Board',
    url: '/theme/message-board',
    icon: 'icon-bubbles'
  },
  {
    name: 'Category',
    url: '/theme/managecategory',
    icon: 'icon-grid'
  },
  {
    name: 'Sub Category',
    url: '/theme/managesubcategory',
    icon: 'icon-list'
  },
  {
    name: 'State',
    url: '/theme/addstate',
    icon: 'icon-map'
  },
  {
    name: 'City',
    url: '/theme/addcity',
    icon: 'icon-location-pin'
  }
];
