// loader.interceptors.ts
import { Injectable } from '@angular/core';

import {
    HttpErrorResponse,
    HttpResponse,
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { LoaderService } from './loader.service';
import { AuthenticationService } from '../../app/views/login/auth/authentication.service';
import 'rxjs/add/operator/do';
import { ToastrService } from 'ngx-toastr';
import { Router } from "@angular/router";


@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
  private requests: HttpRequest<any>[] = [];

  constructor(private loaderService: LoaderService, private authService: AuthenticationService, private toastr: ToastrService, private router: Router) { }

  removeRequest(req: HttpRequest<any>) {
    const i = this.requests.indexOf(req);
    if (i >= 0) {
      this.requests.splice(i, 1);
    }
    this.loaderService.isLoading.next(this.requests.length > 0);   
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    //debugger;
    // add authorization header to request

    //Get Token data from local storage
    //let tokenInfo = localStorage.getItem('token');
    //if (tokenInfo!=null) {
    //  req = req.clone({
    //    setHeaders: {
    //      Authorization: `Bearer ${tokenInfo}`//,
    //      //'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
    //    }
    //  });
    //}
   
    this.requests.push(req);
    console.log("No of requests--->" + this.requests.length);
    this.loaderService.isLoading.next(true);
    //this.delay(3000);
    return Observable.create(observer => {
      const subscription = next.handle(req)
        .subscribe(
          event => {
            if (event instanceof HttpResponse) {
              this.removeRequest(req);
              observer.next(event);
            }
          },
          err => {
            console.log("error returned");
            console.log("Request:-  " + req);
            console.log("Error:-  " + err);
            this.removeRequest(req);
            observer.error(err);
            if (err.status == 401 && err.statusText == "Unauthorized") {
              debugger;
              this.authService.logout();
              this.toastr.error('Your session is expired');
              this.router.navigate(['/login']);
            }
          },
          () => {
            this.removeRequest(req);
            observer.complete();
          });
      // remove request from queue when cancelled
      return () => {
        this.removeRequest(req);
        subscription.unsubscribe();
      };
    });
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}
