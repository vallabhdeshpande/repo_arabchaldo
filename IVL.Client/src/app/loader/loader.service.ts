//loader.service.ts

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  public isLoading = new BehaviorSubject(false);

  constructor() { }

  show() {
    debugger;
    this.isLoading.next(true);
  }

  hide() {
    debugger;
    this.isLoading.next(false);
  }

  isVisible(): Observable<boolean> {
    return this.isLoading.asObservable().pipe(share());   
  }
}
