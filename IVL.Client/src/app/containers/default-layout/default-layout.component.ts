import { adminNavItems } from './../../_nav';
import { Component, OnDestroy, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { navItems } from '../../_nav';
import {Router, ActivatedRoute} from "@angular/router";
import { UserService } from '../../views/theme/user.service';
import { AuthenticationService } from '../../views/login/auth/authentication.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoggedInUser } from '../../views/shared/common.model';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnDestroy {
  public navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  profileImage:string;
  welcomeuser:string;
  currentLocation:string;
  imagepath = environment.imagepath;
  public currentUser: Observable<LoggedInUser>;

  constructor(private router: Router,private userService: UserService,private AuthenticationService:AuthenticationService,@Inject(DOCUMENT) _document?: any) 
  {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
  }

  onLogout() 
  {
    debugger;
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    var currentLangSelected = localStorage.getItem('language');
    window.localStorage.clear();
     window.sessionStorage.clear();
    // this.router.navigate(['/login']);
    this.AuthenticationService.logout();
    localStorage.setItem('language',currentLangSelected);
        this.router.navigate(['/userlogout']);
  }

  ngOnInit() 
			{
        // Get logged In UserInfo on the basis of Geolocation
        //debugger;

       // this.userService.getIpAddress().subscribe(data => {
       //   console.log(data);
          //debugger;
         // this.currentLocation=data.city;
       
       // });  
    if (localStorage.getItem('token') === null)
      this.router.navigate(['/login']);
    else {
      if (localStorage.getItem('profilePic') === null)
        this.profileImage = "../../../assets/img/default/ArabChaldoLogo.PNG";
    else  if (localStorage.getItem('profilePic') === "")
        this.profileImage = "../../../assets/img/default/ArabChaldoLogo.PNG";
      else
        this.profileImage = this.imagepath + "UserProfile/" + localStorage.getItem('userId') + "/ProfilePic/" + localStorage.getItem('profilePic');
      this.welcomeuser = localStorage.getItem('firstName') + " " + localStorage.getItem('lastName');

      if (localStorage.getItem('roleName') == "SuperAdmin") {
        this.navItems = navItems;
      }
      else if (localStorage.getItem('roleName') == "Admin") {
        this.navItems = adminNavItems;
      }
      else if (localStorage.getItem('roleName') == "Registered User") {

        this.router.navigate(['/budashboard']);
      }
            

    }

      
      }
    
  ngOnDestroy(): void 
  {
    this.changes.disconnect();
  }

  navigatetouserdetails() 
  {
    var userId = localStorage.getItem('userId');
    
   // this.router.navigate(['/theme/userdetailsview'], { queryParams: { userId:userId } });
    this.router.navigate(['/theme/adduser'], { queryParams: { userId: userId, UpdateProfile: "UpdateProfile" } });
  }
}
