import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../app/views/login/auth/auth.guard';
// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { LoginComponent } from './views/login/login.component';
import { ActivationComponent } from './views/activation/activation.component';
import { ForgotpasswordComponent } from './views/forgotpassword/forgotpassword.component';
import { ChangepasswordComponent } from './views/changepassword/changepassword.component';
import { RegisterComponent } from './views/register/register.component';

import { WebsiteComponent } from './Website/website.component';
import { MainheadComponent } from './Website/mainhead/mainhead.component';
import { BannerComponent } from './Website/banner/banner.component';
import { ContentComponent } from './Website/content/content.component';
import { MainfooterComponent } from './Website/mainfooter/mainfooter.component';
import { AboutComponent } from './Website/about/about.component';
import { EventsComponent } from './Website/events/events.component';
import { JobsComponent } from './Website/jobs/jobs.component';
import { AdDetailComponent } from './Website/ad-detail/ad-detail.component';
import { BannerDetailComponent } from './Website/banner-detail/banner-detail.component';
import { FaqComponent } from './Website/faq/faq.component';
import { BuyplanComponent } from './Website/buyplan/buyplan.component';
import { ContactComponent } from './Website/contact/contact.component';
import { PrivacyPolicyComponent } from './Website/privacy-policy/privacy-policy.component';
import { TermsUseComponent } from './Website/terms-use/terms-use.component';
import { DisclaimerComponent } from './Website/disclaimer/disclaimer.component';
import { BuaddetailsviewComponent } from './Website/buaddetailsview/buaddetailsview.component';
import { BudashboardComponent } from './Website/budashboard/budashboard.component';
import { BupostadComponent } from './Website/bupostad/bupostad.component';
import { BupostpaidadComponent } from './Website/bupostpaidad/bupostpaidad.component';
import { BumanageadComponent } from './Website/bumanagead/bumanagead.component';
import { BupostbannerComponent } from './Website/bupostbanner/bupostbanner.component';
import { BumanagebannerComponent } from './Website/bumanagebanner/bumanagebanner.component';
import { BuposteventComponent } from './Website/bupostevent/bupostevent.component';
import { EventDetailComponent } from './Website/event-detail/event-detail.component';
import { BumanageeventComponent } from './Website/bumanageevent/bumanageevent.component';
import { BuprofileComponent } from './Website/buprofile/buprofile.component';
import { BuresetpasswordComponent } from './Website/buresetpassword/buresetpassword.component';
import { AdsCategorywiseComponent } from './Website/ads-categorywise/ads-categorywise.component';
import { AdsSubcategorywiseComponent } from './Website/ads-subcategorywise/ads-subcategorywise.component';
import { BubannerdetailsviewComponent } from './Website/bubannerdetailsview/bubannerdetailsview.component';
import { BueventdetailviewComponent } from './Website/bueventdetailview/bueventdetailview.component';
//import { BueditbannerComponent } from './Website/bueditbanner/bueditbanner.component';
//import { BuediteventComponent } from './Website/bueditevent/bueditevent.component';
import { BlogComponent } from './Website/blog/blog.component';
import { BlogDetailComponent } from './Website/blog-detail/blog-detail.component';
import { BumanageblogComponent } from './Website/bumanageblog/bumanageblog.component'; 
import { BuadblogComponent } from './Website/buadblog/buadblog.component';
// import { UploadComponent } from './Website/upload/upload.component'
// import { MultiUploadComponent } from './Website/multiupload/multiupload.component';
import { BublogdetailsviewComponent } from './Website/bublogdetailsview/bublogdetailsview.component';
import { SearchresultComponent } from './Website/searchresult/searchresult.component';
import { UserlogoutComponent } from './Website/userlogout/userlogout.component';
import { MessageboardComponent } from './Website/messageboard/messageboard.component';
import { BumanagejobsComponent } from './Website/bumanagejobs/bumanagejobs.component';
import { BuysellComponent } from './Website/buysell/buysell.component';
import { BusmalladdetailsviewComponent } from './Website/busmalladdetailsview/busmalladdetailsview.component';
import { BupostsmalladComponent } from './Website/bupostsmallad/bupostsmallad.component';
import { BumanagesmalladComponent } from './Website/bumanagesmallad/bumanagesmallad.component';
import {BumanagebuysellComponent} from './Website/bumanagebuysell/bumanagebuysell.component';
import { BubuyselldetailsviewComponent } from './Website/bubuyselldetailsview/bubuyselldetailsview.component';

import { SmalladlistingComponent } from './Website/smalladlisting/smalladlisting.component';
import { SmalladdetailsComponent } from './Website/smalladdetails/smalladdetails.component';
import { BuaddbuysellComponent } from './Website/buaddbuysell/buaddbuysell.component';
import { BuyselldetailsComponent } from './Website/buyselldetails/buyselldetails.component';
import { BannerlistingComponent } from './Website/bannerlisting/bannerlisting.component';
import { AdvertiseusComponent } from './Website/advertiseus/advertiseus.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/website',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },

  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login'
    }
  },
  {
    path: 'activation',
    component: ActivationComponent,
    data: {
      title: 'Activation'
    }
  },
  {
    path: 'changepassword',
    component: ChangepasswordComponent,
    data: {
      title: 'Change Password'
    }
  },
  {
    path: 'messageboard',
    component: MessageboardComponent,
    data: {
      title: 'Message Board'
    }
  },
  {
    path: 'userlogout',
    component: UserlogoutComponent,
    data: {
      title: 'User Logout'
    }
  },
  {
    path: 'buaddetailsview',
    component: BuaddetailsviewComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Business Listing Details'
    }
  },

 
  {
    path: 'forgotpassword',
    component: ForgotpasswordComponent,
    data: {
      title: 'forgotpassword'
    }
  },

  
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },


  {
    path: 'mainhead',
    component: MainheadComponent,
    data: {
      title: 'Header'
    }
  },
  {
    path: 'mainfooter',
    component: MainfooterComponent,
    data: {
      title: 'Footer'
    }
  },
  {
    path: 'content',
    component: ContentComponent,
    data: {
      title: 'content'
    }
  },

  {
    path: 'ad-detail',
    component: AdDetailComponent,
    data: {
      title: 'ad-detail'
    }
  },

  {
    path: 'banner-detail',
    component: BannerDetailComponent,
    data: {
      title: 'banner-detail'
    }
  },

  {
    path: 'about',
    component: AboutComponent,
    data: {
      title: 'about'
    }
  },

  {
    path: 'events',
    component: EventsComponent,
    data: {
      title: 'events'
    }
  },

  {
    path: 'jobs',
    component: JobsComponent,
    data: {
      title: 'jobs'
    }
  },
  {
    path: 'event-detail',
    component: EventDetailComponent,
    data: {
      title: 'event-detail'
    }
  },

  {
    path: 'faq',
    component: FaqComponent,
    data: {
      title: 'faq'
    }
  },

  {
    path: 'buyplan',
    component: BuyplanComponent,
    data: {
      title: 'Buy Plan'
    }
  },

  {
    path: 'contact',
    component: ContactComponent,
    data: {
      title: 'contact'
    }
  },

  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent,
    data: {
      title: 'privacy-policy'
    }
  },

  {
    path: 'terms-use',
    component: TermsUseComponent,
    data: {
      title: 'terms-use'
    }
  },

  {
    path: 'disclaimer',
    component: DisclaimerComponent,
    data: {
      title: 'disclaimer'
    }
  },
  {
    path: 'website',
    //redirectTo: '/website',
    component: WebsiteComponent,
    data: {
      title: 'Website'
    }
  },

  {
    path: 'banner',
    component: BannerComponent,
    data: {
      title: 'banner'
    }
  },
  {
    path: 'forgotpassword',
    component: ForgotpasswordComponent,
    data: {
      title: 'forgotpassword'
    }
  },
  {
    path: 'budashboard',
    component: BudashboardComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Website Dashboard'
    }
  },
  {
    path: 'bupostad',
    component: BupostadComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Business Listing'
    }
  },
  {
    path: 'bupostpaidad',
    component: BupostpaidadComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Website Post Paid Ad'
    }
  },
  {
    path: 'bumanagead',
    component: BumanageadComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Manage Business Listing'
    }
  },
  {
    path: 'bumanagesmallad',
    component: BumanagesmalladComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Website Manage Advertisement'
    }
  },
  {
    path: 'busmalladdetailsview',
    component: BusmalladdetailsviewComponent,
    canActivate: [AuthGuard],
    data: {
      title: ' BU Advertisement details'
    }
  },
  {
    path: 'bupostsmallad',
    component: BupostsmalladComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Website Post Advertisement'
    }
  },

  {
    path: 'bupostbanner',
    component: BupostbannerComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Website Post Banner'
    }
  },
  {
    path: 'buadblog',
    component: BuadblogComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'buadblog'
    }
  },
  //{
  //  path: 'bueditbanner',
  //  component: BueditbannerComponent,
  //  data: {
  //    title: 'BU edit Banner'
  //  }
  //},

  {
    path: 'bubannerdetailsview',
    component: BubannerdetailsviewComponent,
    canActivate: [AuthGuard],
    data: {
      title: ' BU banner details'
    }
  },

  {
    path: 'bumanagebanner',
    component: BumanagebannerComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Website Manage Banner'
    }
  },

  {
    path: 'bueventdetailview',
    component: BueventdetailviewComponent,
    canActivate: [AuthGuard],
    data: {
      title: ' BU Event detail view'
    }
  },

  //{
  //  path: 'bueventdetail;view',
  //  component: BuediteventComponent,
  //  data: {
  //    title: ' BU edit Event'
  //  }
  //},

  {
    path: 'bupostevent',
    component: BuposteventComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Website Post Event'
    }
  },


  {
    path: 'bumanageevent',
    component: BumanageeventComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Website Manage Event'
    }
  },

  {
    path: 'buprofile',
    component: BuprofileComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Website Profile'
    }
  },

  {
    path: 'buresetpassword',
    component: BuresetpasswordComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Website Reset Password'
    }
  },
 
  {
    path: 'searchresult',
    component: SearchresultComponent,
    data: {
      title: 'Search Result'
    }
  },
  {
   path: 'ads-categorywise',
   component: AdsCategorywiseComponent,
   data: {
     title: 'Ads Categorywise'
   }
  },
  {
    path: 'blog-detail',
    component: BlogDetailComponent,
    data: {
      title: 'blog details'
    }
  },
    {
        path: 'bumanageblog',
      component: BumanageblogComponent,
      canActivate: [AuthGuard],
        data: {
          title: 'bumanageblog'
        }
      }, 
  {
    path: 'blog',
    component: BlogComponent,
    data: {
      title: 'blogs'
    }
  },
  {
    path: 'ads-subcategorywise',
    component: AdsSubcategorywiseComponent,
    data: {
      title: 'Ads Subcategorywise'
    }
  },
  {
    path: 'bublogdetailsview',
    component: BublogdetailsviewComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'bublogetailsview'
    }
  },
  {
    path: 'bumanagejobs',
    component: BumanagejobsComponent,
    data: {
      title: 'bumanagejobs'
    }
  }, 
  {
    path: 'buysell',
    component: BuysellComponent,
    data: {
      title: 'buysell'
    }
  }, 
  {
    path: 'bumanagebuysell',
    component: BumanagebuysellComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'bumanagebuysell'
    }
  }, 
  {
    path: 'bubuyselldetailsview',
    component: BubuyselldetailsviewComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'bubuyselldetailsview'
    }
  }, 
  {
    path: 'buaddbuysell',
    component: BuaddbuysellComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'buaddbuysell'
    }
  },
  {
    path: 'smalladlisting',
    component: SmalladlistingComponent,

    data: {
      title: 'smalladlisting'
    }
  },
  {
    path: 'smalladdetails',
    component: SmalladdetailsComponent,
    data: {
      title: 'smalladdetails'
    }
  }, 
  {
    path: 'buyselldetails',
    component: BuyselldetailsComponent,
   
    data: {
      title: 'buyselldetails'
    }
  }, 
  {
    path: 'bannerlisting',
    component: BannerlistingComponent,
    data: {
      title: 'bannerlisting'
    }
  },
  {
    path: 'advertiseus',
    component: AdvertiseusComponent,
    data: {
      title: 'advertiseus'
    }
  },

  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      // {
      //   path: 'base',
      //   loadChildren: () => import('./views/base/base.module').then(m => m.BaseModule)
      // },
      // {
      //   path: 'buttons',
      //   loadChildren: () => import('./views/buttons/buttons.module').then(m => m.ButtonsModule)
      // },
      // {
      //   path: 'charts',
      //   loadChildren: () => import('./views/chartjs/chartjs.module').then(m => m.ChartJSModule)
      // },
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      // {
      //   path: 'icons',
      //   loadChildren: () => import('./views/icons/icons.module').then(m => m.IconsModule)
      // },
      // {
      //   path: 'notifications',
      //   loadChildren: () => import('./views/notifications/notifications.module').then(m => m.NotificationsModule)
      // },
      {
        path: 'theme',
        loadChildren: () => import('./views/theme/theme.module').then(m => m.ThemeModule)
      }
       ,
       {
         path: 'widgets',
         loadChildren: () => import('./views/widgets/widgets.module').then(m => m.WidgetsModule)
       }
    ]
  },
  { //path: '**', component: P404Component
    path: '**', redirectTo:'/index.html'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
