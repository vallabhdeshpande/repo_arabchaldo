import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../views/shared/common.service';
import { Common,category,Subcategory} from '../../views/shared/common.model';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  imagepath = environment.imagepath;
 
  constructor(public CommonService: CommonService) { }

	status: boolean = false;
	showAll(){
	this.status = !this.status;       
	}

  categories: category[]=[]; 
  category = new category();
  Subcategories: Subcategory[]=[]; 
  Subcategory = new Subcategory();
  categoryId: string;
 //el: HTMLElement = document.getElementById('content');
 //el: HTMLElement = document.getElementById('content');
 //el: HTMLElement = document.getElementById('content');
 //el: HTMLElement = document.getElementById('content');

  ngOnInit() {
    
    this.getCategoryList();
    
  }
  
  getCategoryList(): void {
    debugger;
    this.CommonService.CategoryDDL().subscribe(categories => {
      if (categories != null && categories.length > 0)
        this.categories = categories;
      else
        this.categories = [];
      console.log(this.categories);

    });
  }
  getSubCategoryList(categoryId) {
    debugger;
    this.CommonService.SubCategoryDDL(categoryId).subscribe(Subcategories => {
     // this.Subcategories = Subcategories;
      if (Subcategories != null && Subcategories.length > 0)
        this.Subcategories = Subcategories;
      else
        this.Subcategories = [];
      console.log(this.Subcategories);
    });
  }
  getPicUrl(CategoryImageUrl:string,categoryId)
  {    
    if(CategoryImageUrl==null)
    return "assets/img/default/no_image_placeholder.jpg";
    else if (CategoryImageUrl=="")
    return "assets/img/default/no_image_placeholder.jpg";
    else 
    //return this.imagepath+'Banners/'+bannerId+'/Image/'+picurl;
    return  this.imagepath+'Category/'+categoryId+'/Image/'+ CategoryImageUrl;
  } 
}
