import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { UserService } from '../../shared/user.service';
import { AuthenticationService } from '../../views/login/auth/authentication.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthService,SocialUser,GoogleLoginProvider,FacebookLoginProvider,LinkedinLoginProvider} from 'ng4-social-login';
import { User } from '../../shared/user';
import { LoggedInUser } from '../../views/shared/common.model';

@Component({
  selector: 'app-mainfooter',
  templateUrl: './mainfooter.component.html',
  styleUrls: ['./mainfooter.component.scss']
})
export class MainfooterComponent implements OnInit {

  formModel = {
    Email: '',
    Password: ''
  }
  loggedInRole: string;
  returnUrl: string;
  public user: any = SocialUser
  UserName: [];

  exitModal: string = 'modal';
  closepopup: string = 'close';
  constructor(private service: AuthenticationService, private router: Router, private route: ActivatedRoute, private toastr: ToastrService, private SocialAuthserice: AuthService) {

    // redirect to home if already logged in
    //  if (this.service.currentUserValue) { 
    //   this.router.navigate(['/dashboard']);
    // }
  }





  ngOnInit() {

    // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    // if (localStorage.getItem('token') != null)
    //   this.router.navigateByUrl('/dashboard');
    //   else
    //   this.router.navigate(['/login']);
  }
  ngAfterViewInit() {
    //if

  }
  onSubmit(form: NgForm) {
    this.service.login(form.value).pipe(first()).subscribe(
      (res: any) => {
        debugger;
        this.closepopup = 'close';
        this.exitModal = 'modal';
        //  this.router.navigate([this.returnUrl]);//data-dismiss="modal"
        console.log(res);
        this.navigateToDashboard(res.loggedInUserInfo.split(":")[4]);

      },
      err => {
        console.log(err.status);
        if (err.status == 400) {
          this.toastr.error('Incorrect username or password.', 'Authentication failed.');

          // alert('Incorrect username or password');
        }
        else
          console.log(err);
      }
    );
  }

  exitModalPopup() {
    debugger;
    this.closepopup = 'close';
  }


  navigateToDashboard(roleId: number) {
    if (roleId == 2 || roleId == 1) {
      this.router.navigate(['/dashboard']);
    }
    else if (roleId == 8) {
      // this.router.navigate(['/budashboard'],{queryParams:{res}});
      this.router.navigate(['/budashboard']);
    }

  }
  

  Googlelogin() {
    debugger;
    this.SocialAuthserice.signIn(GoogleLoginProvider.PROVIDER_ID).then((userData) => {
      if (userData != null) {

        this.user = userData;
        this.SocialLogin(this.user);
      }
   
    });
    console.log('clicked');
  }
  facebooklogin() {
    debugger
    this.SocialAuthserice.signIn(FacebookLoginProvider.PROVIDER_ID).then((userData) => {
      if (userData != null) {

        this.user = userData;
        this.SocialLogin(this.user);
      }

    });
    //this.SocialLogin(this.user);
  }
  Linkdenlogin() {
    debugger
    this.SocialAuthserice.signIn(LinkedinLoginProvider.PROVIDER_ID).then((userData) => {
      if (userData != null) {

        this.user = userData;
        this.SocialLogin(this.user);
      }
    });
    //this.SocialLogin(this.user);
  }
  SocialLogin(user) {
    debugger;
    user.name = user.name.split(" ");
    user.FirstName = user.name[0];
    user.LastName = user.name[1];
    user.ProfilePicurl = user.photoUrl;
    this.service.Sociallogin(user).pipe(first()).subscribe(
      (res: any) => {
        debugger;
        this.router.navigate(['/budashboard']);
      });
  }
}

