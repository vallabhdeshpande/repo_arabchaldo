import { Component, OnInit } from '@angular/core';
import { EventDetailsService } from '../../views/theme/eventdetails.service';
import { Router } from "@angular/router";
import { EventDetails, WorkflowstatusDTO } from '../../views/theme/eventdetails.model';
import { IfStmt, debugOutputAstAsTypeScript } from '@angular/compiler';
import { OrderPipe } from 'ngx-order-pipe';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-bumanageevent',
  templateUrl: './bumanageevent.component.html',
  styleUrls: ['./bumanageevent.component.scss']
})
export class BumanageeventComponent implements OnInit {
  adevents: EventDetails[]=[];
  workflowdetails: string;
  statusMessage: string;
  deleteremark: string;
  activity: boolean;
  visibilitybutton: string;
  visibilityspan: string; 
  adevent = new EventDetails(); order: string;
  reverse: any;
  LoggedInUserId: string;
  modalRemark: string;
  public pageSize: number = 10;
  public p: number;
  remark: string;
  workflowstatusId: number;
  action: string = "";
  eventId = 0;
  imagepath = environment.imagepath;
  sortedCollection: any[];
  id: number;
  actionMessage: string;
  postingevent: number;
  setStatus: string = "";
  filter: string = "";
  IsPremium: boolean = false;
  eventtype: string;
  showPremium: string = "col-md-3";
  setType: string = "";
  statusText: string = "";
  constructor(private toastr: ToastrService,private orderPipe: OrderPipe, private EventDetailsService: EventDetailsService, private router: Router) {
    this.sortedCollection = orderPipe.transform(this.adevents, 'info.title');
    console.log(this.sortedCollection);
  }

  ngOnInit() {
    this.postingevent = 0;
    this.getEventList();
    this.LoggedInUserId = localStorage.getItem('userId');
  }


  onDelete(eventId: number,event) {
    this.eventId=eventId;
    this.actionMessage="Are you sure to delete this Event?"
  }

  ActionDelete()
  {
    this.action = "Delete";
    this.deleteremark = "";
    this.workflowdetails = this.eventId + "," + this.LoggedInUserId + "," + this.action + "," + this.deleteremark;
      this.EventDetailsService.updatewithWorkflow(this.workflowdetails).
        subscribe(response => {
          console.log(response);
          this.getEventList();
          debugger;
          if (response.status === 200) {
            debugger;
            console.log(response);
            if ((<any>response)._body === "true")
            this.toastr.success('Event successfully deleted.');
            else
            this.toastr.info('Event is active on website.');
            debugger

          }
          else {
            this.toastr.warning('Problem with service. Please try again later!');
          }
        });
  }

  // constructor(private EventDetailsService: EventDetailsService, private router: Router) { }
  ngAfterViewInit() {
    //  document.getElementById('#modalRemarkText').value = this.modalRemark;
  }

  updatePost(adevent: EventDetails): void {
    //debugger
    //this.router.navigate(['/theme/editbanner'],{queryParams:{bannerId:managebanner.bannerId}});  
    this.router.navigate(['/theme/updateevent'], { queryParams: { id: adevent.eventId } });
  };

  getEventList(): void
  {
debugger
    //this.EventDetailsService.getEventList(localStorage.userId).subscribe(adevents => {
    this.EventDetailsService.getEventList().subscribe(adevents => {
      this.adevents = adevents;

      console.log(this.adevents);
      var adlenth = 0;
       if (this.adevents != null) {
         adlenth = adevents.length;
         if (adevents.length > 0) {

           
           for (var i = 0; i < this.adevents.length; i++) {
             if (adevents[i].status == "") {
              adevents[i].status = "Pending";
             }
           }
           this.adevents = adevents;
         }
         else
           this.adevents = [];
       }
       else
         this.adevents = [];
      });
  }

  openModal() {
    debugger;
    //#modalRemarkText
    let modalRemarkText = (<HTMLInputElement>document.getElementById('modalRemarkText')).value;
    if (modalRemarkText != "")
      modalRemarkText = "";
    return "modal";

  }

  getPicUrl(picurl: string, eventId) {
    //debugger;
    if (picurl == null)
      return "assets/img/default/no_image_placeholder.jpg";
    else if (picurl == "")
      return "assets/img/default/no_image_placeholder.jpg";
    else
      return this.imagepath + 'Events/' + eventId + '/Image/' + picurl;

  }

  updateEvent(adevent: EventDetails): void {
    debugger;
    this.router.navigate(['/theme/postevent'], { queryParams: { eventId: adevent.eventId } });
  };

  gotoEventDetails(adevent: EventDetails): void {
    debugger
    this.router.navigate(['/theme/eventdetailsview'], { queryParams: { eventId: adevent.eventId } });
  };

  NavigateToAddEvent(): void {
    debugger
    this.router.navigate(['/theme/postevent']);
  };

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }

  ActionOnApprove(eventId, event: any) {

    debugger;
    this.remark = "";
    this.eventId = eventId;
    this.action = "Approve";
    this.actionMessage = "Are you sure to approve this record?";
  }

  getStatus(Status: string) {  
    // debugger;
     if (Status == "Active") {
       this.statusText = "Active";
       return this.setStatus = "badge badge-success";
     }
       

     if (Status == "InActive") {
       this.statusText = "InActive";
       return this.setStatus = "badge badge-danger";
     }
         

     if (Status == "Pending") {
       this.statusText = "Pending";
       return this.setStatus = "badge badge-warning";
     }
              

 } 

  ActionOnReject(eventId, event) {
    this.remark = "";
    this.eventId = eventId;
    this.action = "Reject";
    this.actionMessage = "Are you sure to reject this record?";
  }

  ActionOnPost(remark) {
    debugger;
    this.workflowdetails = this.eventId + "," + this.LoggedInUserId + "," + this.action + "," + remark;

    // string message = Are you sure to "remark" this record?;

    if (confirm(this.actionMessage)) {
      this.EventDetailsService.updatewithWorkflow(this.workflowdetails).subscribe(response => {
        console.log(response);
        console.log('Updated');
        this.getEventList();
      },

        (error) => {
          console.log(error);

          this.toastr.error('Problem with service. Please try again later!');
        });
    }
    //this.router.navigate(['/theme/postevent'],{queryParams:{eventId:adevent.eventId}});
  }

  DeleteEvent(eventId: number): void {
    debugger;
    this.eventId = eventId;
    this.action = "Delete";
    this.deleteremark = "";
    this.workflowdetails = this.eventId + "," + this.LoggedInUserId + "," + this.action + "," + this.deleteremark;
    if (confirm('Are you sure to delete this record?')) {
      this.EventDetailsService.updatewithWorkflow(this.workflowdetails).
        subscribe(response => {
          console.log(response);
          this.getEventList();
          debugger;
          if (response.status === 200) {
            debugger;
            console.log(response);
            if ((<any>response)._body === "true")
            this.toastr.success('Event successfully deleted.');
            else
            this.toastr.info('Event is active on website.');
            debugger

          }
          else {
            this.toastr.warning('Problem with service. Please try again later!');
          }

        },

          (error) => {
            console.log(error);

            this.toastr.error('Problem with service. Please try again later!');
          });
    }
  }

  eventDetailsView(eventId: number) {
    debugger;
    this.router.navigate(['/bueventdetailview'], { queryParams: { id: eventId } });
  }
}
