import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BumanageeventComponent } from './bumanageevent.component';

describe('BumanageeventComponent', () => {
  let component: BumanageeventComponent;
  let fixture: ComponentFixture<BumanageeventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BumanageeventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BumanageeventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
