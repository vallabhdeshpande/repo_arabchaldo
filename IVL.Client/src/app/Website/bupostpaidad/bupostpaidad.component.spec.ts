import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BupostpaidadComponent } from './bupostpaidad.component';

describe('BupostpaidadComponent', () => {
  let component: BupostpaidadComponent;
  let fixture: ComponentFixture<BupostpaidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BupostpaidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BupostpaidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
