import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { AuthenticationService } from '../../views/login/auth/authentication.service';
import {Router, ActivatedRoute} from "@angular/router";
import { environment } from '../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http } from '@angular/http';
import { ContentComponent } from '../content/content.component';

import { WebsiteComponent } from '../website.component';
@Component({
  selector: 'app-mainhead',
  templateUrl: './mainhead.component.html',
  styleUrls: ['./mainhead.component.scss']
})
export class MainheadComponent implements OnInit {

  constructor(private router: Router, private AuthenticationService: AuthenticationService, public translate: TranslateService) {
    // Arabic language integration
    // translate.addLangs(['en-US', 'ar-SA']);
    // translate.setDefaultLang('en-US');

    //translate.addLangs(['English', 'Arabic']);
    //debugger;
    if (localStorage.getItem('language') !== null) {
      translate.setDefaultLang(localStorage.getItem('language'));
      this.langselected = localStorage.getItem('language');
      this.translate.use(localStorage.getItem('language'));
      //this.contentCmpt.ngOnInit();
    }
    else {
      localStorage.setItem('language', "English");
      translate.setDefaultLang('English');
      this.langselected = "English";
      this.translate.use("English");

    }
      
    const browserLang = translate.getBrowserLang();
    
    //translate.use(browserLang.match(/Arabic/) ? 'Arabic' : 'English');
    //const browserLang = translate.getBrowserLang();
    //debugger;
    //translate.use(browserLang.match(/Arabic/) ? 'Arabic' : 'English');
    ////translate.use(browserLang.match(/ar|ar-SA/) ? 'ar-SA' : 'en-US');
    //alert("Current Language " + translate.currentLang);
    //alert('Browser Lang =' + browserLang);
    //alert('Navigator Lang ='+ navigator.language);
    //console.log('Browser Lang =', browserLang);
    //console.log('Navigator Lang =', navigator.language);
    //console.log('Current Lang =', translate.currentLang);

  }
  userName: string;
  loginshow: string;
  logoutshow:string;
  userpic: string;
  langselected: string = "English";
  imagepath = environment.imagepath;
  @ViewChild(ContentComponent, { static: false }) contentCmpt: ContentComponent;

  @ViewChild(WebsiteComponent, { static: false }) webcmpt: WebsiteComponent;
  @Output() changeLanguage = new EventEmitter<string>();
  ngOnInit() {
    if(localStorage.getItem('token') !=null)
    {
      debugger;
      this.loginshow = 'hidebutton';
      this.logoutshow = 'showbutton';
      console.log(localStorage.getItem);
      this.userName = localStorage.getItem('firstName') + ' ' + localStorage.getItem('lastName');
      if (localStorage.getItem('profilePic') != "")
        this.userpic = this.imagepath + 'UserProfile/' + localStorage.getItem('userId') + '/ProfilePic/' + localStorage.getItem('profilePic');
      else
        this.userpic = "assets/img/default/ArabChaldoLogo.PNG";

    }
    else  
    {
      this.loginshow='showbutton';
    this.logoutshow='hidebutton';

    }
  }

  switchLanguage(event: any) {
    debugger;
   
    this.translate.use(event.target.value);
    localStorage.setItem('language', event.target.value);
    //var langselected = document.getElementById('langSelect');
    if (event.target.value=="English") 
      this.langselected = "English";
    else
      this.langselected = "Arabic";
    localStorage.setItem('language', this.langselected);
    this.langselected = localStorage.getItem('language');
    // this.webcmpt.ngOnInit();
    this.changeLanguage.emit(this.langselected);
    
  }
  onLogout() 
  {
    debugger;

    localStorage.removeItem('token');
    localStorage.removeItem('role');
    var currentLangSelected = localStorage.getItem('language');
    window.localStorage.clear();
     window.sessionStorage.clear();
    // this.router.navigate(['/login']);
    this.AuthenticationService.logout();
    this.loginshow='showbutton';
    this.logoutshow = 'hidebutton';
    localStorage.setItem('language', currentLangSelected);
    this.router.navigate(['/userlogout']);

  }

  navigateToDashboard() {
debugger;

console.log(localStorage.getItem('roleName'));
    if ((localStorage.getItem('roleName') == "SuperAdmin") || (localStorage.getItem('roleName') == "Admin"))
      this.router.navigate(['/dashboard']);
    else
      this.router.navigate(['/budashboard']);
     
  }
  navigateToProfile() {
    debugger
    if ((localStorage.getItem('roleName') == "SuperAdmin") || (localStorage.getItem('roleName') == "Admin"))
     // this.router.navigate(['/theme/userdetailsview']);
      this.router.navigate(['/theme/adduser'], { queryParams: { userId: localStorage.getItem('userId'), UpdateProfile: "UpdateProfile" } });
    else
      this.router.navigate(['/buprofile']);

  }
  navigateToRegister() 
  {
    this.router.navigate(['/register']);
  }

  navigateToLogin() 
  {
    this.router.navigate(['/login']);
  }

  navigateToPostAd() {
    debugger
    if ((localStorage.getItem('roleName') == "SuperAdmin") || (localStorage.getItem('roleName') == "Admin"))
      // this.router.navigate(['/theme/userdetailsview']);
      this.router.navigate(['/dashboard']);
    else if (localStorage.getItem('roleName') == "Registered User")
      this.router.navigate(['/budashboard']);
    else
      this.router.navigate(['/login']);

  }


 
}
