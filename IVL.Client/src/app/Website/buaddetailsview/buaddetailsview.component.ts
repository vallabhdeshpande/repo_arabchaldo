import { Component, OnInit } from '@angular/core';
import { AdDetailsService } from '../../views/theme/addetails.service';
import {Router, ActivatedRoute} from "@angular/router";
import { AdDetails } from '../../views/theme/addetails.model';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Observable } from 'rxjs';
@Component({
  selector: 'app-buaddetailsview',
  templateUrl: './buaddetailsview.component.html',
  styleUrls: ['./buaddetailsview.component.scss']
})
export class BuaddetailsviewComponent implements OnInit {

  adposts: AdDetails[];
  workflow:string;
  statusMessage: string;
  logoUrl: string;
  logourls = [];
  workflowDetails = [];
  imageurls = [];
  multiimageurls = [];
  maxDate:string;
  validTill:string;
  showLogo:string ="col-md-4";
  showImages:string ="col-md-8";
  adpost = new AdDetails();;
  imagepath = environment.imagepath;
  addivshow = true;
  jobdivshow = false;
  detailsViewName: string = "Business Listing Details";
  detailsViewTitleCss: string = "nav-icon icon-docs";
  Job: string;
  showOtherCity: string = "d-none";
  adType: string = "";
  showAdType: string = "col-md-4";
  description: string = "";
  constructor(private AdDetailsService: AdDetailsService,private route: ActivatedRoute, private router: Router, private http: HttpClient) { }


  ngOnInit() {
    //debugger
    this.getAdDetailsById(this.route.snapshot.queryParams['adId']);
    this.gettoday();
    this.Job = this.route.snapshot.queryParams['Job']
    if (this.Job == "Jobs") {
      this.addivshow = false;
      this.jobdivshow = true;
      this.detailsViewName = "Job Details";
      this.detailsViewTitleCss = "nav-icon icon-docs";
      this.showAdType = "col-md-4 d-none";
    }
    else {
      this.addivshow = true;
      this.jobdivshow = false;
      this.detailsViewName = "Business Listing Details";
     // this.detailsViewTitleCss = "nav-icon icon-briefcase";
      this.showAdType = "col-md-4";
    }
  }

  manageAdsback() {
    if (this.Job == "Jobs") 
      this.router.navigate(['/bumanagejobs']);
    else
      this.router.navigate(['/bumanagead']);

  }
  gettoday(){ 
 
    this.maxDate =new Date().toJSON().split('T')[0];  
    }
  getAdDetailsById(adId) {
    debugger;
    this.workflow = adId + "," + "Ad";
    this.AdDetailsService.getAdDetailsById(adId).subscribe(adposts => {
      if(adposts.validTillDate!==null)
      this.validTill=adposts.validTillDate.split('T')[0];
      else
      this.validTill=this.maxDate;
      if(adposts.countryCodeContact===null ||adposts.countryCodeContact==="1")
      adposts.countryCodeContact="USA (+1)";
      console.log(adposts); 
      debugger ;
     // this.workflow=adId+","+"Ad";     
      this.adpost.PostedForName=adposts.postedForName;
      this.adpost.CategoryName=adposts.categoryName;
      if(adposts.subCategoryName==="null")
      this.adpost.SubCategoryName="";
      else
      this.adpost.SubCategoryName=adposts.subCategoryName;
      this.adpost.ContactPersonName=adposts.contactPersonName;
      this.adpost.Email=adposts.email;
      this.adpost.ContactNumber=adposts.contactNumber;
      this.adpost.AlternateContactNumber=adposts.alternateContactNumber;
      this.adpost.ContactPersonName=adposts.contactPersonName;
      this.adpost.Website=adposts.website;
      this.adpost.FaxNumber=adposts.faxNumber;  
      this.adpost.ServicesOffered=adposts.servicesOffered;
      this.adpost.Title=adposts.title;
      this.adpost.TagLine=adposts.tagLine;
      this.adpost.Description = adposts.description;
      this.description = adposts.description;
      this.adpost.AddressStreet1=adposts.addressStreet1;
      this.adpost.AddressStreet2 = adposts.addressStreet2;
      if (adposts.countryId == 0) {
        this.adpost.CountryName = "";
      }
      else
      this.adpost.CountryName=adposts.countryName;
      this.adpost.StateName=adposts.stateName;
      this.adpost.CityName=adposts.cityName;  
      this.adpost.ZipCode = adposts.zipCode;
      if (adposts.validFromDate !== null)
        this.adpost.ValidFromDate = adposts.validFromDate.split('T')[0];
      if (adposts.validTillDate !== null)
        this.adpost.ValidTillDate = adposts.validTillDate.split('T')[0];
      this.adpost.CountryCode=adposts.countryCodeContact;
      this.adpost.AdStatusForDetailsView = this.getStatus(adposts.isVisible);
      if (adposts.cityId === 9999) {
        this.showOtherCity = "col-md-4"
        this.adpost.OtherCity = adposts.otherCity;
      }
      else
        this.showOtherCity = "d-none";
      debugger;
      if(adposts.adLogoUrl===null)
      {
        if (adposts.adLogoUrl==="") {
          this.adpost.AdLogoUrl = "";
          this.showLogo = "col-md-4 d-none";
        }
        else
          this.showLogo = "col-md-4 d-none";
      
      }
      else
      {
        if (adposts.adLogoUrl!="") {
          this.adpost.AdLogoUrl = adposts.adLogoUrl;
          this.logoUrl = this.imagepath + 'Ads/' + adId + '/Logo/' + this.adpost.AdLogoUrl;
          this.logourls.push(this.logoUrl);
          this.showLogo = "col-md-4";
        }
        else
          this.showLogo = "col-md-4 d-none";
      }    
      //if(adposts.imageUrl==="")  
      //{
      //  this.adpost.AdImageUrl="";
      //  this.showLogo="col-md-8 d-none";

      //}
      if (adposts.imageUrl === null) {
        if (adposts.imageUrl === "") {
          this.adpost.AdImageUrl = "";
          this.showImages = "col-md-8 d-none";
        }
        else
          this.showImages = "col-md-8 d-none";

      }
      else {
        if (adposts.imageUrl!=="") {
          this.adpost.AdImageUrl = adposts.imageUrl;
          this.multiimageurls = adposts.imageUrl.split(',');
          for (let i = 0; i < this.multiimageurls.length; i++) {
            this.imageurls.push(this.imagepath + 'Ads/' + adId + '/Images/' + this.multiimageurls[i]);
          }
          this.showImages = "col-md-8";
        }
        else
          this.showImages = "col-md-8 d-none";
        //alert(this.imageurls);
       }
      if (adposts.isPaidAd)
        this.adType = "Premium";
      else
        this.adType = "Free";
    });
  }
  getStatus(status: boolean) {
    debugger;
  var currentStatus:string="";
  if(status==true)
  currentStatus="Active";
  else if(status==false)
  currentStatus="Inactive";
 else
  currentStatus="Pending";
return currentStatus;


  }
  

}
