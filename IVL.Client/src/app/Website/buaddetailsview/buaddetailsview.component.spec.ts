import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuaddetailsviewComponent } from './buaddetailsview.component';

describe('BuaddetailsviewComponent', () => {
  let component: BuaddetailsviewComponent;
  let fixture: ComponentFixture<BuaddetailsviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuaddetailsviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuaddetailsviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
