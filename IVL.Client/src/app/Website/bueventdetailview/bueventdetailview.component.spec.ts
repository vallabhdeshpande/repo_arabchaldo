import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BueventdetailviewComponent } from './bueventdetailview.component';

describe('BueventdetailviewComponent', () => {
  let component: BueventdetailviewComponent;
  let fixture: ComponentFixture<BueventdetailviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BueventdetailviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BueventdetailviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
