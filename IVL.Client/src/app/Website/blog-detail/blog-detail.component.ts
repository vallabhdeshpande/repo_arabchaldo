import { Component, OnInit } from '@angular/core';
import { WebsiteService} from '../../shared/website.service'
import { Manageblog } from '../../shared/manageblog'
import { from } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.scss']
})
export class BlogDetailComponent implements OnInit {
  blogpost= new Manageblog();;
  imageUrl: string;
  Imageurls = [];
  imagepath = environment.imagepath;
  desciption: string;
  userId: number;
  constructor(private WebsiteService: WebsiteService,private route: ActivatedRoute, private router:Router) { }

  ngOnInit() {
    debugger
    this.getblogbyblogId(this.route.snapshot.queryParams['blogId']);
  }
  getblogbyblogId(blogid)
  {
    debugger;
   this.WebsiteService.getBlogbyId(blogid).subscribe(blogpost => { 
    //var blogpost = JSON.parse(blogpost._body);
    console.log(blogpost);
    this.blogpost.Title=blogpost.title,
    this.blogpost.CreatedDate=blogpost.createdDate.split('T')[0];
    this.blogpost.Description=blogpost.description,
    this.desciption=blogpost.description,
    this.blogpost.CreatedBy=blogpost.createdBy,
    this.blogpost.ValidTill=blogpost.validTillDate.split('T')[0];
    this.blogpost.PostedForName=blogpost.postedForName,
    this.blogpost.ImageUrl=blogpost.imageUrl,
    this.imageUrl=this.imagepath+'Blogs/'+blogid+'/Image/'+ blogpost.imageUrl;
    this.Imageurls.push(this.imageUrl); 
    console.log(blogpost);
    debugger;
   });
  }

  btnPostNew() {
    console.log(localStorage.getItem('roleName'));
    if (localStorage.getItem('token') === null) {
      this.router.navigate(['/login']);
    }
    else {
      console.log(localStorage.getItem);
      if ((localStorage.getItem('roleName') === 'SuperAdmin') || (localStorage.getItem('roleName') === 'Admin')) {
        this.router.navigate(['/theme/manageblog']);
      }
      else {
        this.router.navigate(['/buadblog']);
      }
    }
  }

}
