import { Component, OnInit } from '@angular/core';
import { Managebanner } from '../../shared/managebanner'
import { AdDetails, SmallAdDetails } from '../../shared/addetails.model';
import { Buysell } from '../../views/theme/buysell.model';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CommonService } from '../../shared/common.service';
import { WebsiteService } from '../../shared/website.service';
import { FormBuilder, Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { LoggedInUser, State, City } from '../../views/shared/common.model';
import { User } from '../../shared/user';
import { Common, category, Subcategory } from '../../views/shared/common.model';
import { ToastrService } from 'ngx-toastr';
import { SubCategory } from '../../views/theme/subcategory.model';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  public _allState: Observable<State[]>;
  public _allCity: Observable<City[]>;
  public _allCategory: Observable<category[]>;
  categories: category[]=[];
  popularcategories: category[]=[];
  category = new category();
  Subcategories = [];
  states: any[];
  cities: any[];
  Subcategorylist: Subcategory[]=[];
  PopularSubcategories: Subcategory[]=[];
  Subcategory = new Subcategory();
  categoryId: string;
  SubcategoryId: string;
  StateId: string = "0";
  submitted = false;
  StateName: string = "";
  FeedbackForm: any;
  CityName: string = "";
  CityId: string = "";

  adposts: AdDetails[]=[];
  contents = [];
  buycontents = [];
  jobcontents = [];
  jobposts: AdDetails[]=[];
  buysellcontents = [];
  buysellposts: AdDetails[]=[];
  adpost = new AdDetails();
  bannerposts: Managebanner[]=[];
  bannerpost = new Managebanner();
  tempbannerposts: Managebanner[]=[];
  tempbannerpost = new Managebanner();
  BannerDetails = [];
  action: string;
  mailfor: string = "Feedback";
  Navigateurl: string;
  isVisible: boolean;
  Addetails = [];
  JobDetails = [];
  bannerId = 0;
  imagepath = environment.imagepath;
  BannerPicurl: string;
  ImageUrl: string;
  Imageurls = [];
  counter: number;
  counters: number
  pageTitle: string = "Recent Ads";
  catgoryhide: string;
  showCatEng: string = "";
  showCatArb: string = "d-none";
  showdropdownmenu = "dropdown-menu"
  showmaindropdown = "dropdown-toggle"
  searchCriteria: string;
  array_one = []; // your other array...
  premiumTag:string="ad-block premium";
  PremiumtagList:boolean=true;
  Verifiedtag:boolean=true;
  CategoryList=[];
  Categoriesdata=[];
  constructor(private toastr: ToastrService, private formbulider: FormBuilder, private _commonService: CommonService, private WebsiteService: WebsiteService, private route: ActivatedRoute, private router: Router) {
    this.counter = 0;
    this.counters = 0;
  }


  status: boolean = false;
  catstatus: boolean = false;
  showAll() {
    //debugger;
    this.status = !this.status;
  }
  viewAll() {
    //debugger;
    this.catstatus = !this.catstatus;
  }
  slideConfig = {
    "autoplay": true,
    "vertical": true,
    "slidesToShow": 12,
    "slidesToScroll": 1,
    "verticalSwiping": true,
    "arrows": false,
  };

  logos = [
    { img: "assets/website/images/clientele-logos/logo1.jpg" },
    { img: "assets/website/images/clientele-logos/logo2.jpg" },
    { img: "assets/website/images/clientele-logos/logo3.jpg" },
    { img: "assets/website/images/clientele-logos/logo4.jpg" },
    { img: "assets/website/images/clientele-logos/logo5.jpg" },
    { img: "assets/website/images/clientele-logos/logo1.jpg" },
    { img: "assets/website/images/clientele-logos/logo2.jpg" },
    { img: "assets/website/images/clientele-logos/logo3.jpg" },
    { img: "assets/website/images/clientele-logos/logo4.jpg" },
    { img: "assets/website/images/clientele-logos/logo5.jpg" },
  ];

  logoConfig = {
    "arrow": true,
    "autoplay": true,
    "slidesToShow": 5,
    "slidesToScroll": 1,
  };

  afterChange(event: any) { }
  public convertContent = (event) => { debugger; this.ngOnInit(); }
  getCategoryList(): void {

    //Popular Categories
    this._commonService.Popularcategories().subscribe(categories => {
      if (categories != null) {
        if (categories.length > 0)
          this.popularcategories = categories;
        else
          this.popularcategories = [];
      }
      else
        this.popularcategories = [];
     
      console.log(this.popularcategories);
    });
    //
    this._commonService.SubCategoryDDL('-1').subscribe(Subcategorylist => {
      if (Subcategorylist != null) {
        if (Subcategorylist.length >0)
        this.Subcategorylist = Subcategorylist;
        else
          this.Subcategorylist = [];
      }
      else
        this.Subcategorylist = [];
      
      console.log(this.Subcategorylist);
    });
    // All categories
    this._commonService.CategoryDDL().subscribe(categories => {
      if (categories != null) {
        if (categories.length>0) {
        this.categories = categories;
        this.CategoryList = categories;
        console.log(this.categories);
        for (let i = 0; i < this.CategoryList.length; i++) {
          if (this.CategoryList[i].categoryName == "Jobs" || this.CategoryList[i].categoryName == "Buy/Sell" || this.CategoryList[i].categoryName == "Other" ) {
            //this.categories[i].pop();            
          }
          else
            this.Categoriesdata.push(this.categories[i]);
        }
        this.categories = this.Categoriesdata;
      }
        else {
          this.categories = [];
          this.Categoriesdata = [];
          this.CategoryList = [];
        }
      }
      else {
        this.categories = [];
        this.Categoriesdata = [];
        this.CategoryList = [];
      }
      
    });
    //this.Subcategorylist = this.Subcategorylist;
  }

  getpremiumtag(IsPremium) {
    //debugger;
    if (IsPremium == "Premium") {
      this.premiumTag = "ad-block premium";
      this.PremiumtagList = true;
      this.Verifiedtag = false;
    }
    else {
      this.premiumTag = "ad-block premium d-none";
      this.PremiumtagList = false;
      this.Verifiedtag = true;
    }
  }
  //   getCatPicUrl(CategoryImageUrl: string, categoryId) {
  //     if (CategoryImageUrl == null)
  //       return "assets/img/default/no_image_placeholder.jpg";
  //     else if (CategoryImageUrl == "")
  //       return "assets/img/default/no_image_placeholder.jpg";
  //     else
  //       //return this.imagepath+'Banners/'+bannerId+'/Image/'+picurl;
  //       return this.imagepath + 'Category/' + categoryId + '/Image/' + CategoryImageUrl;
  //   } 


  getSubCategoryList(categoryId) {
    //debugger;
    this.Subcategories = [];
    if (this.Subcategorylist != null && this.Subcategorylist.length>0) {
      for (let i = 0; i < this.Subcategorylist.length; i++) {
        if (this.Subcategorylist[i].categoryId == categoryId)
          this.Subcategories.push(this.Subcategorylist[i]);
      }

      this.Subcategories = this.Subcategories;
    }
    
    console.log(this.Subcategories);
   
    if (this.Subcategories.length == 0) {
      this.showdropdownmenu = "dropdown-menu d-none"
      this.showmaindropdown = "dropdown-toggle d-none"
    }
    else {
      this.showmaindropdown = "dropdown-toggle"
      this.showdropdownmenu = "dropdown-menu"
    }
  }


  getPopularSubCategoryList(categoryId, categoryName) {
    //debugger;
    this._commonService.SubCategoryDDL(categoryId).subscribe(Subcategories => {
      if (Subcategories!=null && Subcategories.length>0)
      this.PopularSubcategories = Subcategories;
      if (Subcategories == null) {
        this.PopularSubcategories = [];
        this.router.navigate(['/ads-categorywise'], { queryParams: { categoryId: categoryId, categoryName: categoryName } });
      }
    });
  }

  changeCategoryContent() {

    if (localStorage.getItem('language') !== null) {
      if (localStorage.getItem('language') === "English") {
        this.showCatEng = "";
        this.showCatArb = "d-none";
      }
      else {
        this.showCatEng = "d-none";
        this.showCatArb = "";
      }

    }
  }

  ngOnInit() {
    //debugger;
    this.changeCategoryContent();
    this.FillStateDDL();
    this.getCategoryList();
    
    this.FeedbackForm = this.formbulider.group({
      mailfor: ['', ''],
      Name: ['', Validators.required],
      Email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      cityId: ['', Validators.required],
      stateId: ['', Validators.required],
      stateName: [''],
      cityName: [''],
      Phone: ['', [Validators.required, Validators.minLength(10)]],
      reason: ['', Validators.required],
      Comments: ['', Validators.required]
    });
    //debugger
    if (this.route.snapshot.queryParams['categoryId'] == undefined && this.route.snapshot.queryParams['SubcategoryId'] == undefined) {
      //this.getAdList();	
      this.getbuySellList();
      this.getJobList();
      this.getSmallAdList();

    }
    this.getBannerList();
    this.Navigateurl = '/ad-detail/adId=' + this.adpost.adId
  }


  Submit(): void {

    //debugger;
    this.submitted = true;

    // stop the process here if form is invalid
    if (this.FeedbackForm.invalid) {
      //debugger
      return;
    }
    //debugger
    console.log(this.FeedbackForm.value)
    //debugger;

   
    this.FeedbackForm.value.Name = this.FeedbackForm.value.Name;
    this.FeedbackForm.value.Phone = this.FeedbackForm.value.Phone;
    this.FeedbackForm.value.Email = this.FeedbackForm.value.Email;
    this.FeedbackForm.value.stateId = this.FeedbackForm.value.stateId;
    this.FeedbackForm.value.cityId = this.FeedbackForm.value.cityId;
    this.FeedbackForm.value.stateName = this.StateName;
    this.FeedbackForm.value.cityName = this.CityName;
    this.FeedbackForm.value.reason = this.FeedbackForm.value.reason;
    this.FeedbackForm.value.Comments = this.FeedbackForm.value.Comments;
    this.FeedbackForm.value.mailfor = this.mailfor;

    console.log(this.FeedbackForm.value)
    this.WebsiteService.add(this.FeedbackForm.value)
      .subscribe((response) => {
        console.log(response);
        this.toastr.success('Feedback form submitted successfully.');


        this.FeedbackForm.reset();

        this.submitted = false;
      },
        (error) => {
          console.log(error);
          this.toastr.error('Problem with service. Please try again later!');

        }

      );


  }

  get f() { return this.FeedbackForm.controls; }
  getAdList(): void {
    //debugger
    //this.WebsiteService.getAdList().subscribe(adposts => {
    //  this.contents = adposts;
    //});

    //console.log(this.contents);
  }
  getJobList(): void {
    this.WebsiteService.getJobsList().subscribe(jobposts => {
      if (jobposts!=null) {
        if (jobposts.length > 0) {
          let TenJobs: any[];
          TenJobs = jobposts;
          if (TenJobs.length > 0) {
            let recordLength = 0;
            if (TenJobs.length <= 5)
              recordLength = TenJobs.length;
            else
              recordLength = 5;
            for (var i = 0; i < recordLength; i++) {
              this.jobcontents.push(TenJobs[i]);
            }
          }
        }
        else
          this.jobcontents = [];
      }
      else
        this.jobcontents = [];




    //  this.jobcontents = jobposts;
    });
    console.log(this.jobcontents);
  }

  navigateToPostAd() {
    //debugger;

    console.log(localStorage.getItem('roleName'));
    if ((localStorage.getItem('roleName') == "SuperAdmin") || (localStorage.getItem('roleName') == "Admin"))
      this.router.navigate(['/dashboard']);
    else if (localStorage.getItem('roleName') == "Registered User")
      this.router.navigate(['/bupostad']);
    else
      this.router.navigate(['/login']);
  }

  //   getjobsData(){
  // 	 //debugger
  // 	console.log(this.counters + 'dat size'+this.jobposts.length)
  // 	for(let i=this.counters+1;i<=this.jobposts.length;i++)
  // 	{
  // 	this.jobcontents.push(this.jobposts[i-1]);
  // 	if(i%10==0) break;
  // 	}

  // 	this.counters+=10;
  // 	console.log(this.jobcontents);
  //   }



  //   getData(){
  // 	//debugger
  // 	console.log(this.counter + 'dat size'+this.adposts.length)
  // 	for(let i=this.counter+1;i<=this.adposts.length;i++)
  // 	{
  // 	this.contents.push(this.adposts[i-1]);
  // 	if(i%8==0) break;
  // 	}

  // 	this.counter+=8;
  // 	console.log(this.contents);
  //   }

  gotoAdDetails(adpost: AdDetails): void {
    //debugger
    this.router.navigate(['/ad-detail'], { queryParams: { adId: adpost.adId } });
  };
  gotoBannerDetails(bannerpost: Managebanner): void {
    //debugger
    this.router.navigate(['/banner-detail'], { queryParams: { bannerId: bannerpost } });
  };

  // banner listy for vertical view on homepage
  getBannerList(): void {
    //debugger
    this.WebsiteService.getBannerList().subscribe(bannerposts => {
      if (bannerposts!=null) {
        if (bannerposts.length>0) {
          this.bannerposts = bannerposts;
          const shuffle = arr => arr.sort(() => Math.random() - 0.5);
          shuffle(this.bannerposts);
          var prembannerLength = this.bannerposts.length;
          if (prembannerLength > 15) {
            this.tempbannerposts = this.bannerposts.slice(0, 15);
            this.bannerposts = this.tempbannerposts;
          }
        }
        else {
          this.bannerposts = [];
          this.tempbannerposts = [];
        }

      }
      else {
        this.bannerposts = [];
        this.tempbannerposts = [];
      }
      
    });
    console.log(this.bannerposts);
  }
  getPicUrl(picurl: string, bannerId) {
    if (picurl == null)
      return "assets/img/default/no_image_placeholder.jpg";
    else if (picurl == "")
      return "assets/img/default/no_image_placeholder.jpg";
    else
      return this.imagepath + 'Banners/' + bannerId + '/Image/' + picurl;
  }



  FillStateDDL() {
    //debugger;
    
    this._allState = this._commonService.StateDDL("1");
    //debugger;
    this._commonService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      if (data.length > 0)
        this.states = data;
      else
        this.states = [];
     
    });
   
    var x = document.getElementsByClassName("ng-placeholder");
    x[2].innerHTML = "-- Select State --";
    x[3].innerHTML = "-- Select City --";
  }

  FillCityDDL(event: any) {
    //debugger;
    var x = document.getElementsByClassName("ng-placeholder");
    var y = document.getElementsByClassName("ng-value-label");
    x[2].innerHTML = "";
    y[1].innerHTML = "";
    this.StateId = event.stateId;
    this.StateName = event.stateName;
    //this.StateName=event.target.options[this.StateId].innerHTML;
    this._allCity = this._commonService.CityDDL(this.StateId);
    this.FeedbackForm.stateName = event.stateName;


    this._commonService.CityDDL(this.StateId).subscribe(data => {
      console.log(data);
      //debugger;
     // this.cities = data;
      if (data.length > 0)
        this.cities = data;
      else
        this.cities = [];
    });
    var x = document.getElementsByClassName("ng-placeholder");
    var y = document.getElementsByClassName("ng-value-label");
    x[0].innerHTML = "--Select City--";
    x[1].innerHTML = "--Select Category--";
    x[3].innerHTML = "-- Select City --";
  }

  FillCityName(event: any) {
  //  debugger;
    var x = document.getElementsByClassName("ng-placeholder");
    x[3].innerHTML = "";
    this.CityId = event.cityId;
    this.CityName = event.cityName;
    //this.CityName=event.target.options[event.target.selectedIndex].innerHTML;	
    // this.FeedbackForm.cityName = this.CityName;
  }
  NavigateToBuySellisting() {

    this.router.navigate(['/buysell']);

  }

  NavigateToBannerlisting() {

    this.router.navigate(['/bannerlisting']);

  }
  NavigateToSearch(keyword) {
    //debugger;
    if (keyword === undefined)
      keyword = 'null'
    else
      keyword = keyword;
    this.searchCriteria = keyword + ',0,0:viewmore';
    this.router.navigate(['/searchresult'], { queryParams: { searchCriteria: this.searchCriteria } });

  }
  NavigateToJobs(): void {
    //debugger;
    this.router.navigate(['/jobs']);
  }


  NavigateToSmallAdsListing() {
    this.router.navigate(['/smalladlisting']);

  }
  gotoSmallAdDetails(adpost: SmallAdDetails): void {
    //debugger
    this.router.navigate(['/smalladdetails'], { queryParams: { smalladId: adpost.smalladId } });
  };
  getSmallAdList(): void {
    //debugger
    this.WebsiteService.getSmallAdList("RecentAds").subscribe(adposts => {
      if (adposts!=null) {
        if (adposts.length > 0) {
          const shuffle = arr => arr.sort(() => Math.random() - 0.5);
          let randomTenAds: any[];
          randomTenAds = adposts;
          shuffle(randomTenAds);
          if (randomTenAds.length > 0) {
            let recordLength = 0;
            if (randomTenAds.length <= 20)
              recordLength = randomTenAds.length;
            else
              recordLength = 20;
            for (var i = 0; i < recordLength; i++) {
              this.contents.push(randomTenAds[i]);
            }
          }
        }
        else
          this.contents = [];
      }
      else
        this.contents = [];

    });
  }
  getbuySellList(): void {
    //debugger
    this.WebsiteService.getbuySellrecentList("Recentbuysell").subscribe(buysellposts => {
      if (buysellposts!=null) {
        if (buysellposts.length > 0) {
          const shuffle = arr => arr.sort(() => Math.random() - 0.5);
          let randomTenbuysellAds: any[];
          randomTenbuysellAds = buysellposts;
          shuffle(randomTenbuysellAds);
          if (randomTenbuysellAds.length > 0) {
            let recordLength = 0;
            if (randomTenbuysellAds.length <= 10)
              recordLength = randomTenbuysellAds.length;
            else
              recordLength = 10;
            for (var i = 0; i < recordLength; i++) {
              this.buysellcontents.push(randomTenbuysellAds[i]);
            }
          }

        }
        else
          this.buysellcontents = [];
      }
      else
        this.buysellcontents = [];
    });
  }
}
