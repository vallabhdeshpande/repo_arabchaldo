import { Component, OnInit } from '@angular/core';
import { WebsiteService} from '../../shared/website.service';
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  FAQData: string="";
  FAQArabicData: string = "";
  showEng: string = "";
  showArb: string = "d-none";
  constructor(private WebsiteService: WebsiteService) { }
  ngOnInit() {
    this.GetFAQData();
    this.changeFaqData();
  }

  changeFaqData() {

    if (localStorage.getItem('language') !== null) {
      if (localStorage.getItem('language') === "English") {
        this.showEng = "";
        this.showArb = "d-none";
      }
      else {
        this.showEng = "d-none";
        this.showArb = "";
      }

    }
  }

  GetFAQData()
  {
    debugger;
   this.WebsiteService.GetFAQData().subscribe(disclaimer => {
     console.log(disclaimer);
     if (disclaimer != null && disclaimer.length>0) {
       this.FAQData = disclaimer[0].FAQs;
       this.FAQArabicData = disclaimer[0].FAQsArabic;
       if (localStorage.getItem('language') !== null) {
         if (localStorage.getItem('language') === "English") {

           this.FAQData = disclaimer[0].FAQs;
         }
         else {
           this.FAQArabicData = disclaimer[0].FAQsArabic;
         }

       }
       else
         this.FAQData = disclaimer[0].FAQs;
     }
     else {
       this.FAQData = "";
       this.FAQArabicData = "";
     }
    
 

   });
  }
}
