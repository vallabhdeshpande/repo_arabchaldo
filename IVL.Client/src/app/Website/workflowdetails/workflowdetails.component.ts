import { Component, OnInit,Input } from '@angular/core';
import { CommonService } from '../../views/shared/common.service';

@Component({
  selector: 'app-workflowdetails',
  templateUrl: './workflowdetails.component.html',
  styleUrls: ['./workflowdetails.component.scss']
})
export class WorkflowdetailsComponent implements OnInit {
  workflowDetails = [];
  @Input() workflow: string;
  postType: string;
  displayWFSection: string="form-bg-section d-none";
  displayTableSection: string="table-box d-none";

  constructor(private _commonService: CommonService) { }

  ngOnInit() {
    debugger;
    this.getWorkflowdetails(this.workflow);
  }


  // Display Workflow Details

  getWorkflowdetails(workflow:string) {
debugger;
   if(this.workflow.split(',')[1]=="Ad")
   this.postType="1";
   else if(this.workflow.split(',')[1]=="Banner")
   this.postType="2";
   else if(this.workflow.split(',')[1]=="Event")
   this.postType="3";
   else if(this.workflow.split(',')[1]=="Blog")
     this.postType = "4";
   else if (this.workflow.split(',')[1] == "BuySell")
     this.postType = "5";
   else if (this.workflow.split(',')[1] == "SmallAd")
     this.postType = "6";
    this._commonService.DisplayWorkFlow(this.workflow.split(',')[0], this.postType).subscribe(Workflowdata => {
      if (Workflowdata.length>0) {
        this.workflowDetails = Workflowdata;
        this.displayWFSection = "form-bg-section";
        this.displayTableSection = "table-box";
      }
      else {
        this.displayWFSection = "form-bg-section d-none";
        this.displayTableSection = "table-box d-none";
      }

    });
  }
}
