import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BumanageadComponent } from './bumanagead.component';

describe('BumanageadComponent', () => {
  let component: BumanageadComponent;
  let fixture: ComponentFixture<BumanageadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BumanageadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BumanageadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
