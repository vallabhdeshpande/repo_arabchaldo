import { Component, OnInit } from '@angular/core';

//import { AdDetailsService } from './addetails.service';
import { AdDetailsService } from '../../views/theme/addetails.service';
import { Router } from "@angular/router";
import { AdDetails, WorkflowstatusDTO, AdStatusDetailsDTO } from '../../views/theme/addetails.model';
import { IfStmt, debugOutputAstAsTypeScript } from '@angular/compiler';
import { OrderPipe } from 'ngx-order-pipe';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr'; 



@Component({
  selector: 'app-bumanagead',
  templateUrl: './bumanagead.component.html',
  styleUrls: ['./bumanagead.component.scss']
})
export class BumanageadComponent implements OnInit {

  adposts: AdDetails[];

 // workflowdetails: WorkflowstatusDTO[];
  adStatusdetails: AdStatusDetailsDTO[];
  workflowdetails:string;
  statusMessage: string;
  deleteremark:string;
  activity:boolean;
  visibilitybutton:string;
  visibilityspan:string;
  setStatusActive: string = "";
  setStatus: string = "";
  setStatusInactive:string="";
  adpost = new AdDetails();order: string;
  reverse: any ;
modalRemark:string;
  public pageSize: number = 10;
  public p: number;
  remark: string;
  workflowstatusId:number;
  action:string="";
  actionmessage:string="";
  adId=0;
imagepath=environment.imagepath;
loggedInUserId:string;
adStatus:string;
  setType: string;
  statusText: string = "";
  filter: string = "";
  searchCriteria: string;
  // constructor(private AdDetailsService: AdDetailsService, private router: Router) { }
  ngAfterViewInit(){   
  //  document.getElementById('#modalRemarkText').value = this.modalRemark;
     }
  getAdList(): void {
    
    this.AdDetailsService.getAdList().subscribe(adposts => {
      this.adposts = adposts;
      
      console.log(this.adposts);
      // Status
debugger;
      var adlenth = 0;
      if (this.adposts != null) {
        adlenth = adposts.length;
        if (adposts.length > 0) {
         
          for (var i = 0; i < adposts.length; i++) {
            if (adposts[i].status=="") {
              adposts[i].status = "Pending";
            }
          }
          this.adposts = adposts;
        }
        else
          this.adposts = [];
      }
      else
        this.adposts = [];

   
   
    });
  }

  getPicUrl(picurl:string,adId)
  {
//debugger;
    if(picurl==null)
    return "assets/img/default/no_image_placeholder.jpg";
    else if (picurl=="")
    return "assets/img/default/no_image_placeholder.jpg";
    else     
    return this.imagepath+'Ads/'+adId+'/Logo/'+picurl;

  }
 
// openModal()
// {
//  // debugger;
//   //#modalRemarkText
//   let modalRemarkText = (<HTMLInputElement>document.getElementById('modalRemarkText')).value;
//   if(modalRemarkText!="")
//   modalRemarkText="";
//   return "modal";
  
// }
  hideActivity(Status: string) {

    if (Status == "Active")
      return "d-none";

    if (Status == "InActive")
      return "d-none";

    if (Status == "Pending")
      return "";

  }


    getStatus(Status: string) {  
      debugger;
      if (Status == "Active") {
        this.statusText = "Active";
        return this.setStatus = "badge badge-success";
      }
        

      if (Status == "InActive") {
        this.statusText = "InActive";
        return this.setStatus = "badge badge-danger";
      }
          

      if (Status == "Pending") {
        this.statusText = "Pending";
        return this.setStatus = "badge badge-warning";
      }
               

  } 
  getType(IsFeatured) {  
    debugger;
    if(IsFeatured==="Premium")
    return this.setStatus="badge badge-warning";
    else
    return this.setStatus=" "; 
} 
  updatePost(adpost:AdDetails): void {
   // debugger;
    this.router.navigate(['/bupostad'],{queryParams:{adId:adpost.adId}});
    };
    gotoAdDetails(adpost:AdDetails): void {
      debugger
      this.router.navigate(['/buaddetailsview'],{queryParams:{adId:adpost.adId}});
      };
    NavigateToAddPost( ): void {
      debugger
       this.router.navigate(['/bupostad']);
       };
  
  sortedCollection: any[];

  constructor(private toastr: ToastrService,private orderPipe: OrderPipe, private AdDetailsService: AdDetailsService, private router: Router) {
    this.sortedCollection = orderPipe.transform(this.adposts, 'firstName');
    console.log(this.sortedCollection);
  }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }
  ActionOnApprove(adId,event)
  {
   
debugger;
//$("#modalRemarkText").value="";
// (<HTMLInputElement>document.getElementById('remark')).value="";
this.remark=null;
    this.adId=adId;
    this.action="Approve";
    this.actionmessage ="Are you sure to approve this business listing?";
  }
  ActionOnReject(adId,event)
  {
    this.remark=null;
    this.adId=adId;
    this.action="Reject";
    this.actionmessage ="Are you sure to reject this business listing?";
  }
  ActionOnPost(remark) {
debugger;
    this.workflowdetails=this.adId+","+remark+","+this.action;
    

    if (confirm(this.actionmessage)) {
      this.AdDetailsService.updatewithWorkflow(this.workflowdetails).subscribe(response => {
        console.log(response);
        console.log('Updated');
        this.getAdList();
      },

        (error) => {
          console.log(error);

          this.toastr.error('Problem with service. Please try again later!');
        });
    }
    //this.router.navigate(['/theme/postad'],{queryParams:{adId:adpost.adId}});
  }
  DeleteAd(adId :number,adStatus :string): void {
    this.adId=adId;
    this.adStatus=adStatus;
    this.actionmessage="Are you sure to delete this business listing?"


  }
  ActionDelete()
  {
   // debugger;
    if(this.adStatus!=="Active")
  {
    this.action="Delete";
    this.deleteremark="";
    this.workflowdetails=this.adId+","+this.loggedInUserId+","+this.action+","+this.deleteremark;
    //this.workflowdetails=this.adId+","+this.deleteremark+","+this.action;
        this.AdDetailsService.updatewithWorkflow(this.workflowdetails).subscribe(response => {
          if (response.status === 200) {
            debugger;
            console.log(response);
            if ((<any>response)._body === "true")
              this.toastr.success('Business Listing successfully deleted.');
            else
              this.toastr.info('Business Listing is active on website.');
            debugger

          }
          else {
            this.toastr.warning('Problem with service. Please try again later!');
          }
          this.getAdList();
        },

          (error) => {
            console.log(error);

            this.statusMessage = "Problem with service. Please try again later!";
          });      
    }
      else{
        this.toastr.info('This is active business listing, can not be deleted.');

      }
  }

  adDetailsView(adId: number) {

    this.router.navigate(['/buaddetailsview'], { queryParams: { adId: adId } });
  }




  NavigateToSearch(keyword) {
    debugger;
    if (keyword === undefined)
      keyword = 'null'
    else
      keyword = keyword;
    this.searchCriteria = keyword + ',0,0';
    this.router.navigate(['/searchresult'], { queryParams: { searchCriteria: this.searchCriteria } });

  }

  ngOnInit() {
	  this.loggedInUserId=localStorage.getItem('userId');
	  this.getAdList();
  }  
}

