import { Component, OnInit, ViewChild } from '@angular/core';
import { AdDetails } from '../../shared/addetails.model';
import { Router, ActivatedRoute } from '@angular/router';

import { WebsiteService } from '../../shared/website.service'
import { HttpEventType, HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { FormBuilder, Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from '../../views/shared/common.service';
import { Common, category, Subcategory } from '../../views/shared/common.model';
import { FileuploadComponent } from '../../../app/Website/fileupload/fileupload.component';
import { Input, ElementRef } from '@angular/core';
import { } from 'googlemaps';

@Component({
  selector: 'app-ad-detail',
  templateUrl: './ad-detail.component.html',
  styleUrls: ['./ad-detail.component.scss']
})


export class AdDetailComponent implements OnInit {
  @ViewChild(FileuploadComponent, { static: false }) FileUploadCmpt: FileuploadComponent;
  JobContactForm: any;
  adposts: AdDetails[];
  statusMessage: string;
  logoUrl: string;
  logourls = [];
  mailfor: string = "Jobs";
  submitted = false;
  imageurls = [];
  multiimageurls = [];
  addressforMap: string = "";
  adpost = new AdDetails();;
  imagepath = environment.imagepath;
  baseUrl = environment.baseUrl;
  showRreplyButton: string = "text-right d-none";
  jobAdEmailId: string;
  jobadId: number;
  jobadTitle: string;
  public fileresponse: string[];
  bannerSlickcss = "carousel ad-img-banner";
  categories: category[];
  category = new category();
  Subcategories: Subcategory[];
  Subcategory = new Subcategory();
  categoryId: string;
  SubcategoryId: string;
  showImage: string = "";
  showLogo: string = "";
  showDescription: string = "";
  showServiceOfferd: string = "";
  showtagline: string = "";
  showAddress: boolean = true;
  showMap: boolean = true;
  PremiumtagAd: boolean = false;
  mailto: string = "";
  map: google.maps.Map;
  adhidden:boolean = false;
  jobhidden: boolean = false;
  userId: number;
  navigateTo: string;
  businessButton: boolean = false;
  jobsButton: boolean = false;
  description: string="";
  subCatName: string;
  constructor(private toastr: ToastrService, private formbulider: FormBuilder, private WebsiteService: WebsiteService, private router: Router, private route: ActivatedRoute, private http: HttpClient, public CommonService: CommonService) { }

  banners = [
    { img: "assets/website/images/ad-detail.jpg" },
    { img: "assets/website/images/ad-detail.jpg" },
    { img: "assets/website/images/ad-detail.jpg" },
    { img: "assets/website/images/ad-detail.jpg" },
  ];

  bannerConfig = {
    "dots": true,
    "arrow": true,
    "autoplay": true,
    "slidesToShow": 1,
    "slidesToScroll": 1,
  };

  ngOnInit() {
    this.getCategoryList();
    this.JobContactForm = this.formbulider.group({
      mailfor: ['', ''],
      Name: ['', Validators.required],
      Email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      Comments: ['', Validators.required],
      Phone: ['', [Validators.required, Validators.minLength(10)]],
      fileUrl: ['', '']
    });
    debugger
    this.getAdDetailsById(this.route.snapshot.queryParams['adId']);


  }
  readmore: boolean = false;
  readMore() {
    this.readmore = !this.readmore;
  }
  onSelectFile(event) {
    debugger;
    console.log(event);

  }

  openModal() {
    debugger;
  }
  public uploadFinishedFile = (event) => { this.fileresponse = event; }
  Submit(): void {

    debugger;
    this.submitted = true;

    // // stop the process here if form is invalid
    if (this.JobContactForm.invalid) {
      debugger
      this.adhidden=false;
      this.jobhidden=true;
      return;
      var x = document.getElementById("btnReply");
      this.adhidden=true;
      this.jobhidden=false;
      // x.setAttribute("style", "display: block;");
      //x.setAttribute("data-target", "#reply");
      return;
      //document.getElementById("btnReply").click();
    }
    debugger
    console.log(this.JobContactForm.value)
    debugger;
    this.JobContactForm.value.Name = this.JobContactForm.value.Name;
    this.JobContactForm.value.Phone = this.JobContactForm.value.Phone;
    this.JobContactForm.value.EmailUser = this.JobContactForm.value.Email;
    this.JobContactForm.value.Email = this.jobAdEmailId;
    this.JobContactForm.value.adId = this.jobadId;
    this.JobContactForm.value.Title = this.jobadTitle;
    if (this.fileresponse !== undefined)
      this.JobContactForm.value.fileUrl = this.fileresponse;
    else
      this.JobContactForm.value.fileUrl = "";
    //this.JobContactForm.value.Email = this.JobContactForm.value.Email;
    this.JobContactForm.value.Comments = this.JobContactForm.value.Comments;
    this.JobContactForm.value.mailfor = this.mailfor;


    console.log(this.JobContactForm.value)
    this.WebsiteService.contact(this.JobContactForm.value)
      .subscribe((response) => {
        console.log(response);
        this.toastr.success('Email Sent successfully.');

        if (this.JobContactForm.value.fileUrl !== undefined)
          this.DeleteAttachment(this.JobContactForm.value.fileUrl);
        this.JobContactForm.reset();
        this.submitted = false;
        this.ngOnInit();
        this.FileUploadCmpt.onfiledelete(0);
      },
        (error) => {
          console.log(error);
          this.toastr.error('Problem with service. Please try again later!');

        }

      );
  }


  DeleteAttachment(fileresponse: string) {
    this.http.delete(this.baseUrl + 'upload/DeleteFile' + fileresponse)
      .subscribe(event => {


      });
  }

  getAdDetailsById(adId) {
    debugger
    this.WebsiteService.getAdDetailsById(adId).subscribe(adposts => {
      console.log(adposts);
      this.adpost.adId = adposts.adId;
      this.jobadId = this.adpost.adId;
      this.adpost.PostedForName = adposts.postedForName;
      this.adpost.ContactPersonName = adposts.contactPersonName;
      this.adpost.Email = adposts.email;
      this.jobAdEmailId = adposts.email;
      this.mailto = "mailto:" + adposts.email; //+ "?Subject=Info: #"+adposts.adId+" " + adposts.title;
      console.log(this.mailto);
      this.adpost.ContactNumber = adposts.contactNumber;
      this.adpost.AlternateContactNumber = adposts.alternateContactNumber;
      this.adpost.ContactPersonName = adposts.contactPersonName;
      this.adpost.Website = adposts.website;
      this.adpost.FaxNumber = adposts.faxNumber;
 
      this.adpost.Title = adposts.title;
      this.jobadTitle = this.adpost.Title;
      this.subCatName = adposts.subCategoryName;

      this.navigateTo = adposts.categoryName;
      if (adposts.servicesOffered !== "") {
        this.showServiceOfferd = "";
        this.adpost.ServicesOffered = adposts.servicesOffered;
      }
      else
        this.showServiceOfferd = "d-none";
        if (adposts.tagLine !== "") {
          this.showtagline = "";
          this.adpost.TagLine = adposts.tagLine;
        }
        else
          this.showtagline = "d-none";
  
      if(adposts.categoryName=='Jobs')
      {
        this.adhidden=false;
        this.jobhidden = true;
        this.jobsButton = true;
      }
      else
      {
        this.adhidden=true;
        this.jobhidden = false;
        this.businessButton = true;
      }
      if (adposts.description !== "") {
        this.showDescription = "";
        this.adpost.Description = adposts.description;
        this.description = adposts.description;
      }
      else
        this.showDescription = "d-none";

      //AddressStreet1
      if (adposts.addressStreet1 == "" || adposts.addressStreet1 == undefined) {
        this.adpost.AddressStreet1 = "";
      }
      else
        this.adpost.AddressStreet1 = adposts.addressStreet1 + ",";
      //AddressStreet2
      if (adposts.addressStreet2 == "" || adposts.addressStreet2 == undefined) {
        this.adpost.AddressStreet2 = "";
      }
      else
        this.adpost.AddressStreet2 = adposts.addressStreet2 + ",";
      //CityName
      if (adposts.cityName == "" || adposts.cityName == undefined) {
        this.adpost.CityName = "";
      }
      else
        this.adpost.CityName = adposts.cityName + ",";
      //StateName
      if (adposts.stateName == "" || adposts.stateName == undefined) {
        this.adpost.StateName = "";
      }
      else
        this.adpost.StateName = adposts.stateName + ",";
      //countryName
      if (adposts.countryName == "" || adposts.countryName == undefined) {
        this.adpost.CountryName = "";
      }
      else
        this.adpost.CountryName = adposts.countryName + ".";

      this.adpost.CreatedBy = adposts.createdBy;
      this.adpost.CreatedDate = adposts.createdDate.split('T')[0];
      this.adpost.ZipCode = adposts.zipCode;
      this.adpost.visitorCount = adposts.visitorCount;
      if (adposts.categoryName === "Jobs") {
        this.showRreplyButton = "text-right";
        this.bannerSlickcss = "carousel ad-img-banner d-none";;
      }


      if (adposts.adLogoUrl !== null) {

        if (adposts.adLogoUrl !== "") {
          this.logoUrl = this.imagepath + 'Ads/' + adId + '/Logo/' + adposts.adLogoUrl;
          this.logourls.pop();
          this.logourls.push(this.logoUrl);
          if (this.logourls.length > 0) {
            this.adpost.AdLogoUrl = adposts.adLogoUrl;
            this.showLogo = "";
          }
        }
        else
          this.showLogo = "d-none";
      }
      else
        this.showLogo = "d-none";

      this.adpost.AdImageUrl = adposts.imageUrl;
      if (adposts.imageUrl !== null) {
        if (adposts.imageUrl !== "") {
          this.multiimageurls = adposts.imageUrl.split(',');
          if (this.multiimageurls.length > 0) {
            this.showImage = "";
            for (let i = 0; i < this.multiimageurls.length; i++) {
              this.imageurls.push(this.imagepath + 'Ads/' + adId + '/Images/' + this.multiimageurls[i])
            }
          }
          else
            this.showImage = "d-none";
        }
        else
          this.showImage = "d-none";
      }
      else
        this.showImage = "d-none";


      this.addressforMap = adposts.addressStreet1 + "," + adposts.addressStreet2 + "," + adposts.zipCode + "," + adposts.cityName + "," + adposts.stateName + "," + adposts.countryName;
      if (adposts.addressStreet1 === "" && adposts.addressStreet2 === "" && (adposts.zipCode === "" || adposts.zipCode === null) && (adposts.cityName === "" || adposts.cityName === null) && (adposts.stateName === "" || adposts.stateName === null) && (adposts.countryName === "" || adposts.countryName === null)) {
        this.showAddress = false;
        this.showMap = true;
        this.displayGoogleMap("");
      }
      else {
        this.showAddress = true;
        this.showMap = true;
        this.displayGoogleMap(this.addressforMap);
      }
      console.log(adposts);
      console.log(this.addressforMap);
      if (adposts.isPaidAd && adposts.categoryName != "Jobs")
        this.PremiumtagAd = true;
      else
        this.PremiumtagAd = false;
    });
  }
  getCategoryList(): void {
    debugger;
    this.CommonService.Popularcategories().subscribe(categories => {
      this.categories = categories;
      console.log(this.categories);

    });
    debugger;
  }
  getSubCategoryList(categoryId, categoryName) {
    debugger;
    this.CommonService.SubCategoryDDL(categoryId).subscribe(Subcategories => {
      this.Subcategories = Subcategories;
      if (Subcategories == null) {
        this.router.navigate(['/ads-categorywise'], { queryParams: { categoryId: categoryId, categoryName: categoryName } });
      }
    });
  }

  refresh(): void {
    debugger;
    window.location.reload();
    this.FileUploadCmpt.ngOnInit();

  }
  displayGoogleMap(address: string) {

    var geocoder = new google.maps.Geocoder();
    if (address == "") {
      var latitude = 42.331429;
      var longitude = -83.045753;

      const mapProperties =
      {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        marker: new google.maps.Marker({ position: { lat: latitude, lng: longitude }, animation: google.maps.Animation.BOUNCE })
      };
      this.map = new google.maps.Map(document.getElementById('map'), mapProperties);
      mapProperties.marker.setMap(this.map);
    }

    else {


      geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          latitude = results[0].geometry.location.lat();
          longitude = results[0].geometry.location.lng();
          //alert(latitude);

        }
        //debugger;
        const mapProperties =
        {
          center: new google.maps.LatLng(latitude, longitude),
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          marker: new google.maps.Marker({ position: { lat: latitude, lng: longitude }, animation: google.maps.Animation.BOUNCE })
        };
        this.map = new google.maps.Map(document.getElementById('map'), mapProperties);
        mapProperties.marker.setMap(this.map);

      });
    }
  }

  btnPostNew() {
    debugger;
    console.log(localStorage.getItem('roleName'));
    if (localStorage.getItem('token') === null) {
      this.router.navigate(['/login']);
    }
    else {
      console.log(localStorage.getItem);
      if ((localStorage.getItem('roleName') === 'SuperAdmin') || (localStorage.getItem('roleName') === 'Admin'))
      {
        console.log("kunal");
        console.log("this.navigateTo == Jobs");
        console.log(this.navigateTo == 'Jobs');
        if (this.navigateTo == 'Jobs') {
          this.router.navigate(['/theme/postad'], { queryParams: { Job: "Jobs" } });
        }
        else {
          this.router.navigate(['/theme/postad']);
        }
        
      }
      else {
        if (this.navigateTo == 'Jobs') {
          //this.router.navigate(['/theme/postad'], { queryParams: { Job: "Jobs" } });
          this.router.navigate(['/bupostad'], { queryParams: { Job: "Jobs" } });
        }
        else {
          this.router.navigate(['/bupostad']);
        }
        
      }
    }
  }

}
