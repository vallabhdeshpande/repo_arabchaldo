import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdsCategorywiseComponent } from './ads-categorywise.component';

describe('AdsCategorywiseComponent', () => {
  let component: AdsCategorywiseComponent;
  let fixture: ComponentFixture<AdsCategorywiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdsCategorywiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdsCategorywiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
