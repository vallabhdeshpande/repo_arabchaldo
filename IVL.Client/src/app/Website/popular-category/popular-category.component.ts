import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../views/shared/common.service';
import { Common, category, Subcategory } from '../../views/shared/common.model';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from "@angular/router";
 
@Component({
  selector: 'app-popular-category',
  templateUrl: './popular-category.component.html',
  styleUrls: ['./popular-category.component.scss']
})
export class PopularCategoryComponent implements OnInit {

  private _allCategory: Observable<category[]>;
  constructor(public CommonService: CommonService,private router: Router){ }

  categories: category[]=[]; 
  category = new category();
  Subcategories: Subcategory[]=[]; 
  Subcategory = new Subcategory();
  categoryId: string;
  SubcategoryId:string;

  ngOnInit() {
    this.getCategoryList();
  }
  
  getCategoryList(): void {
    debugger;
    this.CommonService.Popularcategories().subscribe(categories => {
      if (categories != null && categories.length > 0)
        this.categories = categories;
      else
        this.categories = [];
      console.log(this.categories);

    });
    debugger;
  }
  getSubCategoryList(categoryId,categoryName) {
    debugger;    
    this.CommonService.SubCategoryDDL(categoryId).subscribe(Subcategories => {
      if (Subcategories != null && Subcategories.length > 0)
      this.Subcategories = Subcategories;
      if(Subcategories==null)
      {
        this.Subcategories = [];
      this.router.navigate(['/ads-categorywise'],{ queryParams: { categoryId: categoryId,categoryName:categoryName } });
     }      
    });
  }
}



