import { Component, OnInit, AfterViewInit } from '@angular/core';
import { EventDetails } from '../../shared/eventdetails.model';
import { WebsiteService} from '../../shared/website.service'
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { UserService } from '../../shared/user.service';
import { User } from '../../shared/user.model';
import { Input, ElementRef } from '@angular/core';
//import { ViewChild } from '@angular/core';
import { } from 'googlemaps';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {
  eventId = 0;
  eventposts: EventDetails[];
  eventpost = new EventDetails();
  userdetail = new User();;
  ImageUrl: string;
  Imageurls = [];
  logourls=[];
  imagepath = environment.imagepath;
  addressforMap: string = "";
  showAddress:boolean=true;
  showMap: boolean = false;
  Premiumtag: boolean = false;
  showImage: string = "";
  bannerSlickcss: string = "";
  showDescription: string = "";
  mailto: string = "";
  map: google.maps.Map;
  userId: number;
  description: string = "";

  constructor(private WebsiteService: WebsiteService, private UserService: UserService, private route: ActivatedRoute, private router:Router) { }

  banners = [
    { img: "assets/website/images/ad-detail.jpg" },
    { img: "assets/website/images/ad-detail.jpg" },
    { img: "assets/website/images/ad-detail.jpg" },
    { img: "assets/website/images/ad-detail.jpg" },
  ];

  bannerConfig = {
    "dots": true,
    "arrow": true,
    "autoplay": true,
    "slidesToShow": 1,
    "slidesToScroll": 1,
  };

  ngOnInit() {
    debugger
    this.getBannerDetailsById(this.route.snapshot.queryParams['eventId']);

    //this.getAdDetailsById(this.route.snapshot.paramMap.get("adId"));
  }
  getBannerDetailsById(eventId) {
    //debugger
    this.WebsiteService.getEventDetailsById(eventId).subscribe(eventpost => {
      //var eventpost = JSON.parse(eventpost._body);
      console.log(eventpost);
      this.eventpost.Title = eventpost.title;
      this.eventpost.PostedForName = eventpost.postedForName;
      this.eventpost.PostedFor = eventpost.postedFor;
      this.eventpost.Website = eventpost.website;
      this.eventpost.visitorCount = eventpost.visitorCount;
      this.eventpost.CreatedDate = eventpost.createdDate.split('T')[0];;
      this.eventpost.EventDate = eventpost.eventDate.split('T')[0];
      if (!eventpost.isPremium) {
        this.showImage = "d-none";

      }
      else {
        this.showImage = "";
      }

     
      if (eventpost.description !== "") {
        this.showDescription = "";
        this.eventpost.Description = eventpost.description;
        this.description = eventpost.description;
      }
      else
        this.showDescription = "d-none";
      this.eventpost.ImageUrl = eventpost.imageUrl;
      if (eventpost.imageUrl !== null) {
        if (eventpost.imageUrl !== undefined) {
          if (eventpost.imageUrl !== "") {
            eventpost.imageUrl = eventpost.imageUrl.split(",")
            if (eventpost.imageUrl.length > 0) {
              this.showImage = "";
              if (eventpost.isPremium == true) {
                {
                  //this.Imageurls.push(this.imagepath + 'Events/' + eventId + '/Premium/' + eventpost.imageUrl[i]);
                  this.ImageUrl = this.imagepath + 'Events/' + eventId + '/Image/' + eventpost.imageUrl;
                }
              }
              else

                this.showImage = "d-none";
            }
            else

              this.showImage = "d-none";
          }
          else

            this.showImage = "d-none";
        }
          else

            this.showImage = "d-none";
        }
      


      
     this.Imageurls.push(this.ImageUrl);
      console.log(this.eventpost.PostedFor);
      debugger;
      
      
      //AddressStreet1
      if (eventpost.addressStreet1 == "" || eventpost.addressStreet1 == undefined) {
        this.eventpost.AddressStreet1 = "";
      }
      else
        this.eventpost.AddressStreet1 = eventpost.addressStreet1 + ",";
      //AddressStreet2
      if (eventpost.addressStreet2 == "" || eventpost.addressStreet2 == undefined) {
        this.eventpost.AddressStreet2 = "";
      }
      else
        this.eventpost.AddressStreet2 = eventpost.addressStreet2 + ",";
      //CityName
      if (eventpost.cityName == "" || eventpost.cityName == undefined) {
        this.eventpost.CityName = "";
      }
      else
        this.eventpost.CityName = eventpost.cityName + ",";
      //StateName
      if (eventpost.stateName == "" || eventpost.stateName == undefined) {
        this.eventpost.StateName = "";
      }
      else
        this.eventpost.StateName = eventpost.stateName + ",";
      //countryName
      if (eventpost.countryName == "" || eventpost.countryName == undefined) {
        this.eventpost.CountryName = "";
      }
      else
        this.eventpost.CountryName = eventpost.countryName + ".";

       //zipCode
      if (eventpost.ZipCode == "" || eventpost.zipCode == undefined)  {
        this.eventpost.ZipCode = "";
      }
      else
        this.eventpost.ZipCode = eventpost.zipCode + ".";

      
      //this.eventpost.AddressStreet1 = eventpost.addressStreet1 + ",";
    //  this.eventpost.AddressStreet2 = eventpost.addressStreet2 + ",";
      this.eventpost.AlternateContactNumber = eventpost.alternateContactNumber;
      this.eventpost.CityId = eventpost.cityId;
     // this.eventpost.CityName = eventpost.cityName + ",";
      this.eventpost.ContactNumber = eventpost.contactNumber;
      this.eventpost.CountryId = eventpost.countryId;
      this.eventpost.Email = eventpost.email;
      this.mailto = "mailto:" + eventpost.email;
      this.eventpost.FaxNumber = eventpost.faxNumber;
      this.eventpost.ContactPersonName = eventpost.contactPersonName;
     // this.eventpost.StateName = eventpost.stateName + ",";
      this.eventpost.StateId = eventpost.stateId;
   //   this.eventpost.ZipCode = eventpost.zipCode;
   

      if (eventpost.isPaidAd)
        this.Premiumtag = true;
      else
        this.Premiumtag = false;

      this.addressforMap = eventpost.addressStreet1 + "," + eventpost.addressStreet2 + "," + eventpost.zipCode + "," + eventpost.cityName + "," + eventpost.stateName + "," + eventpost.countryName;
      //this.getuserContactDetailsById(this.eventpost.PostedFor);
      //debugger;
      //this.mapDetails(this.addressforMap);

      if (eventpost.addressStreet1 === "" && eventpost.addressStreet2 === "" && (eventpost.zipCode === "" || eventpost.zipCode === null) && (eventpost.cityName === "" || eventpost.cityName === null) && (eventpost.stateName === "" || eventpost.stateName === null)) {
        this.showAddress = false;
        this.showMap = true;
        this.displayGoogleMap("");
      }
      else {
        this.showAddress = true;
        this.showMap = true;
        this.displayGoogleMap(this.addressforMap);
      }
      //console.log(eventpost);
      //console.log(this.addressforMap);
     
    });
   
  }
  getuserContactDetailsById(userId) {
    debugger
    this.UserService.getUserContactdetailsById(userId).subscribe(userdetail => {
      console.log(userdetail);
      var userdetail = JSON.parse(userdetail._body);
      debugger;
      this.userdetail.AddressStreet1 = userdetail.addressStreet1;
      this.userdetail.AddressStreet2 = userdetail.addressStreet2;
      this.userdetail.AlternateContactNumber = userdetail.alternateContactNumber;
      this.userdetail.CityId = userdetail.cityId;
      this.userdetail.cityName = userdetail.cityName;
      this.userdetail.ContactNumber = userdetail.contactNumber;
      this.userdetail.CountryId = userdetail.countryId;
      this.userdetail.Email = userdetail.email;
      this.userdetail.FaxNumber = userdetail.faxNumber;
      this.userdetail.FirstName = userdetail.firstName;
      this.userdetail.LastName = userdetail.lastName;
      this.userdetail.StateId = userdetail.stateId;
      this.userdetail.ZipCode = userdetail.zipCode;
      console.log(userdetail);
  
    });
  
  }
  readmore: boolean = false;
	readMore(){
	this.readmore = !this.readmore;       
    }

  displayGoogleMap(address: string) {
   
    var geocoder = new google.maps.Geocoder();
    if (address == "") {
      var latitude = 42.331429;
      var longitude = -83.045753;

      const mapProperties =
      {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        marker: new google.maps.Marker({ position: { lat: latitude, lng: longitude }, animation: google.maps.Animation.BOUNCE })
      };
      this.map = new google.maps.Map(document.getElementById('map'), mapProperties);
      mapProperties.marker.setMap(this.map);
    }

    else {


      geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          latitude = results[0].geometry.location.lat();
          longitude = results[0].geometry.location.lng();
          //alert(latitude);

        }
        //debugger;
        const mapProperties =
        {
          center: new google.maps.LatLng(latitude, longitude),
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          marker: new google.maps.Marker({ position: { lat: latitude, lng: longitude }, animation: google.maps.Animation.BOUNCE })
        };
        this.map = new google.maps.Map(document.getElementById('map'), mapProperties);
        mapProperties.marker.setMap(this.map);

      });
    }
  }

  btnPostNew() {
    console.log(localStorage.getItem('roleName'));
    if (localStorage.getItem('token') === null) {
      this.router.navigate(['/login']);
    }
    else {
      console.log(localStorage.getItem);
      if ((localStorage.getItem('roleName') === 'SuperAdmin') || (localStorage.getItem('roleName') === 'Admin')) {
        this.router.navigate(['/theme/postevent']);
      }
      else {
        this.router.navigate(['/bupostevent']);
      }
    }
  }
}
