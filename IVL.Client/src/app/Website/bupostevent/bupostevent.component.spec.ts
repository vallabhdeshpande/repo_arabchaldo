import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuposteventComponent } from './bupostevent.component';

describe('BuposteventComponent', () => {
  let component: BuposteventComponent;
  let fixture: ComponentFixture<BuposteventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuposteventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuposteventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
