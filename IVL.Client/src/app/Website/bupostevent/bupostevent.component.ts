import { Component, OnInit, Inject, Output, EventEmitter, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Country, State, City, category, Subcategory, Roles, CountryCodes } from '../../views/shared/common.model';
import { User } from '../../views/theme/user';
import { UserService } from '../../views/theme/user.service';
import { EventDetails } from '../../views/theme/eventdetails.model';
import { EventDetailsService } from '../../views/theme/eventdetails.service';
import { CommonService } from '../../views/shared/common.service';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { environment } from '../../../environments/environment';
import { element } from 'protractor';
import { ToastrService, Toast } from 'ngx-toastr'; 
import { first } from 'rxjs/operators';
import { Paymentplan } from '../../views/theme/paymentplan';
import { Paymentplanservice } from '../../views/theme/paymentplanservice';
import { UploadComponent } from '../../../app/Website/upload/upload.component';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import * as countryCodeData from '../../../app/views/shared/countrycodes.json';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-bupostevent',
  templateUrl: './bupostevent.component.html',
  styleUrls: ['./bupostevent.component.scss'],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }]
})
export class BuposteventComponent implements OnInit
{ 
  @ViewChild(UploadComponent, { static: false }) UploadCmpt: UploadComponent;
  public _allUser: Observable<User[]>;
  public _allCountry: Observable<Country[]>;
  public _allCountryCodes: Observable<CountryCodes[]>;
  public _allState: Observable<State[]>;
  public _allCity: Observable<City[]>;
  public _allCategory: Observable<category[]>;
  public _allPlan: Observable<Paymentplan[]>;
  public previousPlan: Paymentplan;

  postedFor: string = "0";
  CountryId: string = "0";
  StateId: string = "0";
  CityId: string = "0";
  logopath: string[];
  imagepath: any[];
  logoUrl: string = "";
  imageUrl: string = "";
  ABCD: string[];
  StateName: string = "";
  CityName: string = "";
  //dvImage: string[];
  FormPostEvent: any;
  actionButton;
  eventimage: string;
  action = "save";
  cities: any[];
  categories: any[];
  states: any[];
  myData = null;
  formData = null;
  msgs = null;
  public imagefilename: string[];
  public logofilename: string[];
  EventPost: EventDetails[];
  eventPost = new EventDetails();
  statusMessage: string;
  saveButtonText: string = "Save";
  displayText = 'btn btn-sm btn-primary';
  displayExtLogo = 'col-md-6 d-none';
  displayNewLogo = 'col-md-6';
  displayExtImages = 'col-md-6 d-none';
  displayExtUploads = 'form-bg-section d-none';
  maxDate: string;
  validTill: string;
  extLogoUrls = [];
  extLogos = [];
  pagedetail: string;
  //resetButtonStyle:string="block";
  public logoresponse: string[];
  loggedInUserId: string;
  addbutton: HTMLElement;
  serverpath = environment.serverpath;
  commonimagepath = environment.imagepath;
  submitted = false;
  Id: number;
  isPaidAd: boolean = false;
  displayPaidSection = 'col-md-12 d-none';
  showOtherCity: string = "d-none";

  remainingeventImage: number;
  tempImageUrl: any;
  minDateModel: NgbDateStruct;
  modelValidFrom: Date = null;
  ValidFromDate: NgbDateStruct;
  validFrom: string = "";
  datePlaceholder: string ="yyyy-mm-dd"
  subscription: any;
  countryCodeList: Array<any> = [];
  bindStateId: number = 0;
  adTypeFree: boolean = true;
  adTypePaid: boolean = false;
  RadioFree: boolean = false;
  RadioPaid: boolean = false;
  countryCodesNew: any = (countryCodeData as any).default;
  countrycode = this.countryCodesNew;
  constructor(private toastr: ToastrService, private _eventDetailsService: EventDetailsService, private paymentPlanService: Paymentplanservice, private _userService: UserService, private _commonService: CommonService,
    private _http: HttpClient, private _router: Router, private _formbulider: FormBuilder, private route: ActivatedRoute, @Inject(DOCUMENT) document, private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private config: NgbDatepickerConfig) {

  }
  navigatetocontactus() {
    this._router.navigate(['/contact']);
  }
  ngAfterViewInit()
  {
    document.getElementById("eventDate").setAttribute("min", this.maxDate);
    document.getElementById('#SaveEvent').innerHTML = this.saveButtonText;
  }

  //gettoday() {
  //  this.maxDate = new Date().toJSON().split('T')[0];
  //}

  selectToday() {
    this.minDateModel = this.calendar.getToday();

  }
  onvalidFromDateSelect(event: any) {
    debugger;
    this.ValidFromDate = event;
    //this.minValidTillDate = event;
    this.validFrom = this.modelValidFrom.toJSON().split('T')[0];

    //var abc = this.ValidFromDate.toJSON().split('T')[0];

  }

  public uploadFinishedLogo = (event) => {
    this.logoresponse = event;
    //this.displayNewLogo = 'col-md-6 d-none';
  }
  GetUserDetails(event:any)
  {}
  FillSubCategoryDDL(event:any)
  {}
  ngOnInit()
  {
    debugger;
    this.loggedInUserId = localStorage.getItem('userId');
    this.FillCategoryDDL();
    this.FillCountryDDL();
    this.FillUserDDL(); this._allCity
    //this.gettoday();
    this.selectToday();
    debugger;
    this.FormPostEvent = this._formbulider.group({
      eventId: ['', ''],
      title: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      faxNumber: [''],
      addressStreet1: ['', ''],
      addressStreet2: ['', ''],
      countryId: ['1', ''],
      countryCodeContact: ['1', ''],
      eventDate: [this.validFrom, Validators.required],
      stateId: [this.bindStateId, ''],
      cityId: ['', ''],
      zipCode: ['',  Validators.pattern("^[0-9]*$")],
      postedFor: ['', Validators.required],
      postedName: ['null', ''],
      contactPersonName: ['', [Validators.required]],
      contactNumber: ['', [Validators.required]],
      website: ['', ''],
      categoryId: ['', Validators.required],
      description: ['', ''],
      imageUrl: [],
      alternateContactNumber: [''],
      createdBy: ['', ''],
      eventImageCount: [''],
      isPaidAd: ['', ''],
      OtherCity: ['']
    });

    this.getEventDetailsById(this.route.snapshot.queryParams['id']);
  }



  getEventDetailsById(id)
  {
    debugger;

    if (id == undefined)
    {
      this.pagedetail = "Post New Event";
      this.action = "save";
      this._userService.getUserById(localStorage.userId).subscribe(userdetail => {
        var userdetail = JSON.parse(userdetail._body);

        console.log(userdetail);


        //this._allState = this._commonService.StateDDL(userdetail.countryId);
        //this._allCity = this._commonService.BUCityDDL(userdetail.stateId);
        this.FillSubscriptionPlan(userdetail.userId);
        this._commonService.BUCityDDL(userdetail.stateId).subscribe(data => {
          console.log(data);
          debugger;
          this.cities = data;
        });
        // this._roles = this._commonService.RoleDDL();
        debugger;

        if (userdetail.countryCodeContact === null)
          userdetail.countryCodeContact = "1";
        
        this.FormPostEvent.setValue({
          eventId: '',
          title: '',
          email: userdetail.email,
          faxNumber: userdetail.faxNumber,
          addressStreet1: userdetail.addressStreet1,
          addressStreet2: userdetail.addressStreet2,
          countryId: userdetail.countryId,
          stateId: userdetail.stateId.toString(),
          cityId: userdetail.cityId.toString(),
          zipCode: userdetail.zipCode,
          postedFor: userdetail.userId,
          postedName: userdetail.firstName + '' + userdetail.lastName,
          contactPersonName: userdetail.firstName + ' ' + userdetail.lastName,
          contactNumber: userdetail.contactNumber,
          countryCodeContact: "1",
          categoryId: '',
          website: '',
          description: '',
          alternateContactNumber: userdetail.alternateContactNumber,
          eventDate: '',
        
          imageUrl: '',
          createdBy: this.loggedInUserId,
          eventImageCount: "",
          isPaidAd: "",
          OtherCity: ""
        });
      });

    }

    else {
      this.pagedetail = "Update Event";
      this._eventDetailsService.getEventDetailsById(id).subscribe(data => {
        console.log(data);
        debugger
        //var data = JSON.parse(data._body);
        console.log(data);
        debugger;
        this.action = "edit";
        this.saveButtonText = "Update";
        this.displayText = 'btn btn-sm btn-primary d-none';

        this._userService.getUserById(data.postedFor).subscribe(userdetail => {
          var userdetail = JSON.parse(userdetail._body);

          console.log(userdetail);


          // Ad Images Retrieval
          this.displayExtUploads = 'form-bg-section';
          if (data.imageUrl !== null) {
            this.renderExistingImages(data.imageUrl, data.eventId, "Images");
            this.displayNewLogo = 'col-md-6 d-none';
          }
          else {
            this.displayExtImages = 'col-md-6 d-none';
          }
          this.FormPostEvent.value.isPaidAd = this.isPaidAd;

          if (data.isPaidAd == true) {
            // Ad Images Retrieval
            this.displayExtUploads = 'form-bg-section';
            if (data.imageUrl !== null) {
              this.selected("Paid");
              this.adTypePaid = true;
              this.adTypeFree = false;
              this.RadioFree = false;
              this.RadioPaid = true;
            }
            else
              this.displayExtImages = 'col-md-6 d-none';

            // Ad Logo Retrieval
            if (data.adLogoUrl !== null) {
              this.selected("Paid");
              this.adTypePaid = true;
              this.adTypeFree = false;
              this.RadioFree = true;
              this.RadioPaid = false;
            }
            else {
              this.displayExtLogo = 'col-md-6 d-none';
              this.RadioFree = true;
            }
          }
          else {
            this.RadioPaid = true;
          }



          // Ad Logo Retrieval
          //   if(data.adLogoUrl !==null)      
          //   this.renderExistingImages(data.adLogoUrl,data.eventId,"Logo");
          //  else
          //  this.displayExtLogo = 'col-md-6 d-none';


          if (data.imageUrl == null)
            this.displayExtUploads = 'form-bg-section d-none';
          //this.resetButtonStyle="none";
          //  this.resetButtonStyle="{display:none}";
          this._allState = this._commonService.StateDDL(data.countryId);
          this._allCity = this._commonService.BUCityDDL(data.stateId);
          this.FillSubscriptionPlan(userdetail.userId);
          this.countrycode = this.countryCodesNew;
          // this._roles = this._commonService.RoleDDL();

          this._commonService.StateDDL(data.countryId).subscribe(data => {
            debugger
            console.log(data);
            this.states = data;

          });

          this._commonService.BUCityDDL(data.stateId).subscribe(data => {
            debugger;
            console.log(data);
            this.cities = data;
          });

          this.countrycode = this.countryCodesNew;

          if (data.cityId == 9999) {
            this.showOtherCity = "col-md-4";
          }
          else
            this.showOtherCity = "d-none";

          debugger;
          if (data.eventDate !== null)
            this.validTill = data.eventDate.split('T')[0];
          else
            this.validTill = this.maxDate;
          this.validFrom = this.validTill
          this.datePlaceholder = this.validFrom;
          if (data.countryCodeContact == null)
            data.countryCodeContact = "1";

          this.tempImageUrl = data.imageUrl;

          this.FormPostEvent.setValue({
            eventId: data.eventId,
            title: data.title,
            email: data.email,
            faxNumber: data.faxNumber,
            addressStreet1: data.addressStreet1,
            addressStreet2: data.addressStreet2,
            countryId: data.countryId,
            stateId: data.stateId.toString(),
            cityId: data.cityId.toString(),
            zipCode: data.zipCode,
            postedFor: data.postedFor,
            postedName: data.postedForName,
            contactPersonName: data.postedForName,
            contactNumber: data.contactNumber,
            countryCodeContact: data.countryCodeContact,
            website: data.website,
            categoryId: data.categoryId.toString(),
            description: data.description,
            alternateContactNumber: data.alternateContactNumber,
            eventDate: this.validTill,
            imageUrl: data.imageUrl,
            createdBy: this.loggedInUserId,
            eventImageCount: 0,
            isPaidAd: this.isPaidAd,
            OtherCity: data.otherCity
          });
        });
      });

    }

  }

  // Insert Event
  postEvent(): void
  {

    if (this.action == "edit")
    {
      debugger;
      this.submitted = true;
  
      var datestatus=this.FormPostEvent.controls.eventDate.status;
      if(datestatus=="INVALID")
      {

      }
      else if (this.FormPostEvent.invalid) {
        this.toastr.warning("Mandatory fields missing. Please fill all mandatory fields.");
        return;
      }
      //this.FormPostEvent.controls.eventDate.pop();
     
      this.FormPostEvent.value.adLogoUrl = this.logoresponse;
      this.FormPostEvent.value.isPaidAd = this.isPaidAd;
      
      if (this.previousPlan == undefined) {
        this.remainingeventImage = 0;
      }

      debugger;
      if (this.extLogos.length > 0) {
        if (this.logoresponse === undefined || this.logoresponse === null)
        {
          this.logoresponse = this.extLogos.slice();
          this.eventimage = this.logoresponse[0];
          this.FormPostEvent.value.imageUrl = this.eventimage;
        }          
      }

      if (this.FormPostEvent.value.imageUrl != this.logoresponse)
      {
        this.FormPostEvent.value.imageUrl = this.logoresponse;
      }
      if (this.isPaidAd == true) {
        if (this.FormPostEvent.value.imageUrl == null || this.FormPostEvent.value.imageUrl == undefined) {
          this.toastr.warning("Event image missing. Please upload an image.");
          return;
        }

        if ((this.FormPostEvent.value.imageUrl != null) && (this.remainingeventImage == 0)
          && (this.FormPostEvent.value.imageUrl != this.tempImageUrl)) {
          this.toastr.warning('event subscription exausted.');
          return;
        }
      }

      // stop the process here if form is invalid

      // if (this.FormPostEvent.invalid) 
      // {
      //    return;
      // }
      console.log(this.FormPostEvent.value)

      //this.FormPostEvent.value.adLogoUrl = this.logoresponse;
      
      this.eventPost.IsPaidad = this.FormPostEvent.value.isPaidAd;
      this.eventPost.ImageUrl = this.FormPostEvent.value.imageUrl;
      this.eventPost.eventId = this.FormPostEvent.value.eventId;
        //this.eventPost.UpdatedBy = this.FormPostEvent.value.
      this.eventPost.Title = this.FormPostEvent.value.title;
      this.eventPost.Description = this.FormPostEvent.value.description;
      this.eventPost.Website = this.FormPostEvent.value.website;
      this.eventPost.AddressStreet1 = this.FormPostEvent.value.addressStreet1;
      this.eventPost.AddressStreet2 = this.FormPostEvent.value.addressStreet2;
      this.eventPost.CountryId = this.FormPostEvent.value.countryId;
      this.eventPost.StateId = this.FormPostEvent.value.stateId;
      this.eventPost.CityId = this.FormPostEvent.value.cityId;
      this.eventPost.ZipCode = this.FormPostEvent.value.zipCode;
        //this.eventPost.IsActive = this.FormPostEvent.value.
        //this.eventPost.IsVisible = this.FormPostEvent.value.
      //this.eventPost.Activity = this.FormPostEvent.value.
      this.eventPost.PostedFor = this.FormPostEvent.value.postedFor;
      this.eventPost.PostedForName = this.FormPostEvent.value.postedName;
        //this.eventPost.Workflowstatus = this.FormPostEvent.value.
        //this.eventPost.Remarks = this.FormPostEvent.value.
        //this.eventPost.StartDate = this.FormPostEvent.value.
        //this.eventPost.EndDate = this.FormPostEvent.value.
        //this.eventPost.Time = this.FormPostEvent.value.
      this.eventPost.CityName = this.FormPostEvent.value.cityId;
      this.eventPost.EventDate = this.FormPostEvent.value.eventDate;
      this.eventPost.CategoryId = this.FormPostEvent.value.categoryId;
      this.eventPost.AlternateContactNumber = this.FormPostEvent.value.alternateContactNumber;
      this.eventPost.ContactNumber = this.FormPostEvent.value.contactNumber;
      this.eventPost.ContactPersonName = this.FormPostEvent.value.contactPersonName;
      this.eventPost.FaxNumber = this.FormPostEvent.value.faxNumber;
      this.eventPost.createdBy = this.loggedInUserId;
      this.eventPost.Email = this.FormPostEvent.value.email;
      this.eventPost.CountryCodeContact = this.FormPostEvent.value.countryCodeContact;
      this.eventPost.OtherCity = this.FormPostEvent.value.OtherCity;
      
      if ((this.previousPlan != undefined) && (this.FormPostEvent.value.imageUrl != this.tempImageUrl)) {
        this.eventPost.EventImageCount = this.FormPostEvent.value.eventImageCount;
        this.eventPost.PlanId = this.previousPlan.planId;
      }


      debugger;
      this._eventDetailsService.updateEvent(this.eventPost)
        .pipe(first())
        .subscribe(
          data => {
            if (data.status === 200) {
              console.log(data);
              this.toastr.success('Event updated successfully.');
              this._router.navigate(['/bumanageevent']);
            } else {
              this.toastr.info(data.message);
            }
          },
          error => {
            this.toastr.error(error);
          });
    }

    else if (this.action == "save")
    {

      debugger;
      this.submitted = true;
      this.FormPostEvent.value.imageUrl = this.logoresponse;
      
      // stop the process here if form is invalid

      if (this.FormPostEvent.invalid) {
        this.toastr.warning("Mandatory fields missing. Please fill all mandatory fields.");
          return;
       }
      this.FormPostEvent.value.createdBy = this.loggedInUserId;
      this.FormPostEvent.value.isPaidAd = this.isPaidAd;
      console.log(this.FormPostEvent.value)

      if (this.isPaidAd == true) {
        // this.FormPostEvent.value.imageUrl = this.logoresponse;
        if (this.FormPostEvent.value.imageUrl == null || this.FormPostEvent.value.imageUrl == undefined) {
          this.toastr.warning("Event image missing. Please upload an image.");
          return;
        }
        if (this.FormPostEvent.value.imageUrl != null && this.previousPlan == undefined) {
          this.toastr.warning("please select/buy a subscription plan to post a event ad.");
          return;
        }
        if (this.FormPostEvent.value.imageUrl != null) {
          this.FormPostEvent.value.eventImageCount = this.FormPostEvent.value.eventImageCount;
          this.FormPostEvent.value.PlanId = this.previousPlan.planId;
        }
      }
      else {

        this.FormPostEvent.value.planId = null;
        this.FormPostEvent.value.imageUrl = null;
      }

      this.FormPostEvent.value.eventId = 0;
      this._eventDetailsService.postEvent(this.FormPostEvent.value)
        .subscribe((response) => {
          console.log(response);
          this.toastr.success('Event Posted successfully.It will be available on website after the admin approves it.');
          this._router.navigate(['/bumanageevent']);
        },

          (error) => {
            console.log(error);
            this.statusMessage = "Problem with service. Please try again later!";
          }
        );

    }


  }

    renderExistingImages(imageUrls, eventId, uploadType)
    {
      debugger;
      if (imageUrls !== "") {
        var extImages = imageUrls.split(',');
        if (extImages.length > 0) {
          if (uploadType == "Images") {
            for (let i = 0; i < extImages.length; i++) {
              this.extLogoUrls.push(this.commonimagepath + 'Events/' + eventId + '/Image/' + extImages[i]);
              this.extLogos.push(extImages[i]);
              this.displayExtLogo = 'col-md-6';
            }
          }

        }

      }
    }



  subscriptionPlanDetails(event: any) {
    debugger;
    console.log(event.target.value);

    this.paymentPlanService.getPlanDetails(event.target.value).subscribe(data => {
      var planIdDetails = JSON.parse(data._body);
      console.log(planIdDetails);
      this.previousPlan = planIdDetails;
      this.remainingeventImage = planIdDetails.eventImageCount;
      if (this.remainingeventImage == 0) {
        this.toastr.warning('Event subscription exausted. Please buy a new subscription or select other subscription plan.');
      }
      this.FormPostEvent.patchValue({
        eventImageCount: planIdDetails.eventImageCount
      });
    });
  }  

  onextLogoUrldelete(i) {
    this.extLogoUrls.splice(i, 1);
    this.extLogos.pop();
    this.displayExtLogo = 'col-md-6 d-none';
    this.displayNewLogo = 'col-md-6';
  }

  NavigatetoManageEvent() {
    this._router.navigate(['/theme/manageevent']);
  }



  // Insert Event
  

  //  Dropdowns

  FillCountryDDL()
  {
    this._allCountry = this._commonService.CountryDDL();
    this.CountryId = "1";
    // this._allState = this._commonService.StateDDL("1");
    // this._allCity = this._commonService.BUCityDDL("22");
    this._commonService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      this.states = data;
    });
    this._allState = this._commonService.StateDDL(this.CountryId);   
    this._allCity = this._commonService.BUCityDDL(this.StateId);
  }

  FillUserDDL()
  {
    //debugger;
    this._allUser = this._commonService.UserDDL();
  }

  //FillStateDDL(event: any)
  //{
  //  this._commonService.StateDDL("1").subscribe(data => {
  //    console.log(data);
  //    //debugger;
  //    this.states = data;
  //  });

  //  // debugger;
  //  if (event.target.value == "") {
  //    this.FormPostEvent.value.countryId = 0;
  //    this._allState = this._commonService.StateDDL("0");
  //    this.FormPostEvent.value.stateId = 0;
  //    this._allCity = this._commonService.CityDDL("0");
  //    this.FormPostEvent.value.cityId = 0;
  //    return
  //  }
  //  this.CountryId = event.target.value;
  //  //this.CountryName=event.target.options[this.CountryId].innerHTML;
  //  this._allState = this._commonService.StateDDL(this.CountryId);

  //}



  FillStateDDL(event: any) {

    debugger;
    if ((event.target.value == "0") || ((event.target.value == ""))) {
      this.FormPostEvent.value.countryId = 0;
      //this._allState = this._commonService.StateDDL("0");
      var x = document.getElementsByClassName("ng-value-label");
       x[2].innerHTML = "--Select State--";
       x[3].innerHTML = "--Select City--";
      this._commonService.StateDDL("0").subscribe(data => {
        console.log(data);
        this.states = data;
      });
      this.states = [];
      this.cities = [];
      this.FillCityDDL(0);
      console.log(this.states);
      this.FormPostEvent.value.stateId = 0;
      //this._allCity = this._commonService.CityDDL("0");
      this.FormPostEvent.value.cityId = 0;
    }
    else {
      this.CountryId = event.countryId;

      this._allState = this._commonService.StateDDL("1");
      debugger;
      this._commonService.StateDDL("1").subscribe(data => {
        console.log(data);
        //debugger;
        this.states = data;
      });

      this._commonService.BUCityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
      this._allState = this._commonService.StateDDL(this.CountryId);
     //this.CountryName = event.target.options[this.CountryId].innerHTML;
      //this.FormPostBanner.countryName = this.CountryName;
    }

  }

  FillCityDDL(event: any)
  {
    if (event!=0) {
    this.StateId = event.stateId;
    this.StateName = event.stateName;
    //this.StateName=event.target.options[this.StateId].innerHTML;
    this._allCity = this._commonService.BUCityDDL(this.StateId);
    //this._allCity = this._commonService.CityDDL(this.StateId);

    this._commonService.BUCityDDL(this.StateId).subscribe(data => {
      console.log(data);
      debugger;
      this.cities = data;
    });
  }
  else {
    var x = document.getElementsByClassName("ng-value-label");
    this.cities = [];
  }
}

  selected(listingType: string) {
    debugger;
    if (listingType == "Free") {
      this.isPaidAd = false;

      this.displayPaidSection = 'col-md-12 d-none';
    }
    if (listingType == "Paid") {
      this.isPaidAd = true;

      this.displayPaidSection = 'col-md-12';
    }
  }


  //FillCityName(event: any) {
  //  this.CityId = event.cityId;
  //  this.CityName = event.cityName;
  //  if (event.value === "9999") {
  //    this.showOtherCity = "col-md-4";
  //  }
  //  else
  //    this.showOtherCity = "d-none";
  //  // this.CityName=event.target.options[this.CityId].innerHTML;
  //}



  FillCityName(event: any) {
    debugger;
    this.CityId = event.cityId;
    this.CityName = event.cityName;

    if (event.cityId === "9999") {
      this.showOtherCity = "col-md-4";
    }
    else
      this.showOtherCity = "d-none";
    //this.CityName = event.target.options[this.CityId].innerHTML;
    //this.FormPostAd.cityName = this.CityName;

  }


  NavigatetoManageAd() {
    this._router.navigate(['/theme/manageevent']);
  }


  FillCategoryDDL() {
    debugger;
    this._allCategory = this._commonService.CategoryDDL();

    this._commonService.CategoryDDL().subscribe(data => {
      debugger;
      this.categories = data;


    });

  }
  FillSubscriptionPlan(userId: number) {
    this._allPlan = this.paymentPlanService.UserPlanDDL(userId);
  }

  reset() {
    this.ngOnInit();
    this.UploadCmpt.onimagedelete(0);

  }


  refresh(): void {
    debugger;
    window.location.reload();
    this.UploadCmpt.ngOnInit();

  }

  get f() {
    return this.FormPostEvent.controls;
  }
}
