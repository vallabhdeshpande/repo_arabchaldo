
import { Component, OnInit, Inject, Output, EventEmitter, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Country, State, City, category, Subcategory, Roles, CountryCodes } from '../../views/shared/common.model';
import { User } from '../../views/theme/user';
import { UserService } from '../../views/theme/user.service';
import { AdDetails, AdImageDetails } from '../../views/theme/addetails.model';
import { AdDetailsService } from '../../views/theme/addetails.service';
import { CommonService } from '../../views/shared/common.service';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { environment } from '../../../environments/environment';
import { element } from 'protractor';
import { first, debounce } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Paymentplan } from '../../views/theme/paymentplan';
import { Paymentplanservice } from '../../views/theme/paymentplanservice';
import { UploadComponent } from '../../../app/Website/upload/upload.component';
import { MultiUploadComponent } from '../../Website/multiupload/multiupload.component';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import * as countryCodeData from '../../../app/views/shared/countrycodes.json';
@Component({
  selector: 'app-bupostad',
  templateUrl: './bupostad.component.html',
  styleUrls: ['./bupostad.component.scss']
})
export class BupostadComponent implements OnInit {
  @ViewChild(UploadComponent, { static: false }) UploadCmpt: UploadComponent;
  @ViewChild(MultiUploadComponent, { static: false }) MultiUploadCmpt: MultiUploadComponent;
  public _allUser: Observable<User[]>;
  public _allCountry: Observable<Country[]>;
  public _allCountryCodes: Observable<CountryCodes[]>;
  public _allState: Observable<State[]>;
  public _allStateNew: Observable<State[]>;
  public _allCity: Observable<City[]>;
  public _allCategory: Observable<category[]>;
  public _allSubCategory: Observable<Subcategory[]>;
  public _allImagedata: Observable<AdImageDetails[]>;
  public _allroles: Observable<Roles[]>;
  postedFor: string = "0";
  CountryId: string = "0";
  StateId: string = "0";
  states: any[];
  cities: any[];
  countries: any[];
  CityId: string = "0";
  CategoryId: string = "0";
  SubCategoryId: string = "0";
  RoleId: string = "0";
  PostedForName: string = "";
  CountryName: string = "";
  StateName: string = "";
  CityName: string = "";
  CategoryName: string = "";
  SubCategoryName: string = "";
  logopath: string[];
  imagepath: any[];
  logoUrl: string = "";
  imageUrl: string = "";
  ABCD: string[];
  addivshow = true;
  jobdivshow = false;
  //dvImage: string[];
  FormPostAd: any;
  actionButton;
  action = "";
  myData = null;
  formData = null;
  msgs = null;
  public imagefilename: string[];
  public logofilename: string[];
  AdPost: AdDetails[]=[];
  adPost = new AdDetails();
  adimgdtls = new AdImageDetails();
  AdImageDetailsDTO = new AdImageDetails();
  statusMessage: string;
  saveButtonText: string = "Save";
  displayText = 'btn btn-sm btn-primary';
  displayExtLogo = 'col-md-6 d-none';
  displayExtImages = 'col-md-6 d-none';
  displayExtUploads = 'form-bg-section d-none';
  displayPaidSection = 'col-md-12 d-none';
  maxDate: string;
  minDate: string;
  validTill: string;
  extImageurls = [];
  extLogoUrls = [];
  extImages = [];
  extLogos = [];
  //resetButtonStyle:string="block";
  public logoresponse: string[];
  public imageresponse: string[];
  roleList1: any[] = [];
  roleData: any[] = [];
  categories: any[];
  subcategories: any[];
  addbutton: HTMLElement;
  serverpath = environment.serverpath;
  commonimagepath = environment.imagepath;
  submitted = false;
  lastkeydown1: number = 0;
  subscription: any;
  countryCodeList: Array<any> = [];
  titleText: string = "Post New Advertisement";
  titlecss: string = "nav-icon icon-doc";
  controlEnabled: boolean = true;
  loggedInUserId: string;
  adTypeFree: boolean = true;
  adTypePaid: boolean = false;
  isPaidAd: boolean = false;
  private _allPlan: Observable<Paymentplan[]>;
  private previousPlan: Paymentplan;
  tempPlanId: number;
  tempLogoImageUrl: any;
  tempAdImageUrl: any;
  Job: string;
  DefaultCategoryId: string = "0";


  remainingJobsCount: number = 0;
  remainingAdImage: number = 0;
  remainingLogoImage: number = 0;
  adImageCount: number;

  adimagefornewpost: string[];

  RadioFree: boolean = false;
  RadioPaid: boolean = false;
  showOtherCity: string = "d-none";
  bindStateId: number = 0;
  bindCategoryId: number = 0;
  countryCodesNew: any = (countryCodeData as any).default;
  countrycode = this.countryCodesNew;
  constructor(private toastr: ToastrService, private _adDetailsService: AdDetailsService, private _userService: UserService, private paymentPlanService: Paymentplanservice, private _commonService: CommonService, private _http: HttpClient, private _router: Router, private _formbulider: FormBuilder, private _route: ActivatedRoute, @Inject(DOCUMENT) document) {

  }
  ngAfterViewInit() {
    //if
    document.getElementById("validTillDate").setAttribute("min", this.minDate);
    document.getElementById("validTillDate").setAttribute("max", this.maxDate);
    document.getElementById('#SaveAd').innerHTML = this.saveButtonText;
    document.getElementById('#dvTitle').innerHTML = this.titleText;

  }

  gettoday() {
    ////debugger;
    var CurrentDate = new Date();
    this.minDate = CurrentDate.toJSON().split('T')[0];
    CurrentDate.setMonth(CurrentDate.getMonth() + 1);
    this.maxDate = CurrentDate.toJSON().split('T')[0];
  }

  // 
  public uploadFinishedLogo = (event) => { this.logoresponse = event; }
  public uploadFinishedImage = (event) => { this.imageresponse = event; }
  // AutoComplete

  ngOnInit() {
    debugger
   // var x = document.getElementsByClassName("ng-value-label");
   // x[3].innerHTML = " Select State";
    //x[4].innerHTML = " Select City";
    this.loggedInUserId = localStorage.getItem('userId');
    this.FillCategoryDDL();
    this.FillCountryDDL();
    this.FillUserDDL();
    this.gettoday();
    this.Job = this._route.snapshot.queryParams['Job']
    if (this.Job == "Jobs") {
      this.addivshow = false;
      this.jobdivshow = true;
      this.titleText = "Post New Job";
      this.titlecss = "nav-icon icon-briefcase";
      this.DefaultCategoryId = environment.defaultJobCategoryId;
      this._allSubCategory = this._commonService.SubCategoryDDL(this.DefaultCategoryId);
      this.isPaidAd = true;
    }
    else {
      this.DefaultCategoryId = "";
      this.addivshow = true;
      this.jobdivshow = false;
      this.titleText = "Post New Business Listing";
      this.titlecss = "nav-icon icon-doc";

    }
    // this.loggedInUserId = "57";
    //this.FillCategoryDDL();

    this.FormPostAd = this._formbulider.group({
      adId: ['', ''],
      //title: ['', [Validators.required, Validators.pattern(".*\\S.*[a-zA-z0-9 ]")]],
      title: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      faxNumber: [''],
      addressStreet1: ['', ''],
      addressStreet2: ['', ''],
      countryId: ['1', ''],
      countryCodeContact: [1, ''],
      adLogoUrl: ['', ''],
      adImageUrl: ['', ''],
      adextLogoUrl: ['', ''],
      adextImageUrl: ['', ''],
      stateId: [this.bindStateId, ''],
      cityId: ['', ''],
      stateName: ['null', ''],
      cityName: ['null', ''],
      zipCode: ['', Validators.pattern("^[0-9]*$")],
      tagLine: ['', ''],
      postedFor: ['', Validators.required],
      postedName: ['null', ''],
      categoryId: [this.DefaultCategoryId, Validators.required],
      categoryName: ['', ''],
      subCategoryId: ['', ''],
      subCategoryName: ['null', ''],
      contactPersonName: ['', [Validators.required]],
      contactNumber: ['', [Validators.required, Validators.minLength(10)]],
      website: ['', ''],
      servicesOffered: ['', ''],
      description: ['', ''],
      validTillDate: ['', ''],
      alternateContactNumber: [''],
      createdBy: ['', ''],
      isPaidAd: ['', ''],
      logoCount: [''],
      adImageCount: [''],
      jobsCount: [''],
      planId: [''],
      OtherCity: ['']
    });


    this.remainingJobsCount = 0;
    this.tempPlanId = 0;
    this.remainingLogoImage = 0;
    this.remainingAdImage = 0;
    this.getAdDetailsById(this._route.snapshot.queryParams['adId']);

    debugger;
    if (localStorage.getItem('roleName') == "Registered User")
      this.FetchUserInfo(this.loggedInUserId);

  }


  get f() { return this.FormPostAd.controls; }


  // Insert Post
  postAd(): void
  {
    debugger;
    this.submitted = true;
    if (this.saveButtonText == "Save") {
      this.action = "";
    }
    // stop the process here if form is invalid

    if (this.FormPostAd.invalid) {
      this.toastr.warning("Mandatory fields missing. Please fill all mandatory fields.");
      return;
    }
    var x = document.getElementsByClassName("ng-value-label");
    let xlenght = x.length;
    //if (this.Job == "Jobs") {
    // // this.PostedForName = x[0].innerHTML;
    //  this.StateName = x[1].innerHTML;
    //  this.CityName = x[2].innerHTML;
    //}
    //else {
    //  this.CategoryName = x[0].innerHTML;
    //  this.SubCategoryName = x[1].innerHTML;
    //  this.StateName = x[3].innerHTML;
    //  this.CityName = x[4].innerHTML;
    //}
    if (xlenght > 0) {
      if (this.Job == "Jobs") {
        this.PostedForName = localStorage.getItem('firstName') + ' ' + localStorage.getItem('lastName');
        this.StateName = x[1].innerHTML;
        this.CityName = x[2].innerHTML;
      }
      else {

        //this.PostedForName = x[0].innerHTML;
        this.CategoryName = x[0].innerHTML;
        if (xlenght == 5) {
          this.SubCategoryName = x[1].innerHTML;
          this.StateName = x[3].innerHTML;
          this.CityName = x[4].innerHTML;
        }
        else if (xlenght == 4) {
          this.SubCategoryName = "";
          this.FormPostAd.value.subCategoryId = 0;
          this.StateName = x[2].innerHTML;
          this.CityName = x[3].innerHTML;
        }

      }
    }
    this.FormPostAd.value.categoryName = this.CategoryName;
    this.FormPostAd.value.PostedForName = this.PostedForName;
    this.FormPostAd.value.SubCategoryName = this.SubCategoryName;
    if (this.SubCategoryName == "--Select Subcategory--") {
      this.FormPostAd.value.subCategoryId = 0;
    }
    this.FormPostAd.value.stateName = this.StateName;
    this.FormPostAd.value.cityName = this.CityName;
    this.FormPostAd.value.postedForName = localStorage.getItem('firstName') + ' ' + localStorage.getItem('lastName');
    console.log(this.FormPostAd.value)
    this.FormPostAd.value.createdBy = this.loggedInUserId;
    this.FormPostAd.value.postedFor = this.loggedInUserId;
    this.FormPostAd.value.isPaidAd = this.isPaidAd;
    if (this.isPaidAd == false) {
      this.FormPostAd.value.adLogoUrl = null;
      this.FormPostAd.value.imageUrl = null;
    }
    else {
      if (this.tempPlanId == 0) {
        this.toastr.warning("Please select a subscription plan to submit the listing.");
        return;
      }
      else {
        this.FormPostAd.value.adLogoUrl = this.logoresponse;
        this.FormPostAd.value.imageUrl = this.imageresponse;
      }
    }

    if (this.FormPostAd.value.subCategoryId == "")
      this.FormPostAd.value.subCategoryId = 0;


    if (this.action == "") {


      if ((this.FormPostAd.value.adLogoUrl != null) && (this.remainingLogoImage - 1 < 0)) {
        this.toastr.warning("Logo subscription utilised.Please choose another plan");
        return;
      }
      else if ((this.FormPostAd.value.adLogoUrl != null) || (this.FormPostAd.value.adLogoUrl != undefined)) {
        this.remainingLogoImage = this.remainingLogoImage - 1;
      }


      if ((this.FormPostAd.value.imageUrl != null) || (this.FormPostAd.value.imageUrl != undefined)) {
        var splitsNewImage = new String(this.FormPostAd.value.imageUrl);
        var afterSplit = splitsNewImage.split(",");
        var count = afterSplit.length;
        if (this.remainingAdImage - count < 0) {
          this.toastr.warning("Business listing image subscription fully utilised.Please choose another plan or reduce number of images.");
          return;
        }
        else {
          this.remainingAdImage = this.remainingAdImage - count;
        }
      }

      if (this.FormPostAd.value.categoryId == environment.defaultJobCategoryId) {
        if (this.remainingJobsCount - 1 < 0) {
          this.toastr.warning("Please buy a plan for job posting or select another plan.");
          return;
        }
        else {
          this.remainingJobsCount = this.remainingJobsCount - 1;
        }
      }

      this.FormPostAd.value.logoCount = this.remainingLogoImage;
      this.FormPostAd.value.adImageCount = this.remainingAdImage;
      this.FormPostAd.value.planId = this.tempPlanId;
      this.FormPostAd.value.jobsCount = this.remainingJobsCount;



      this.FormPostAd.value.adId = 0;
      this._adDetailsService.postAd(this.FormPostAd.value)
        .subscribe((response) => {
          console.log(response);
          //if (response.status === 200) {
          //  if ((<any>response)._body === "true") {
          //    if (this.Job == "Jobs") {
          //      this.toastr.success('Job posted successfully.');
          //      this._router.navigate(['bumanagejobs']);
          //    }
          //    else {
          //      this.toastr.success('Business listing posted successfully.');
          //      this._router.navigate(['bumanagead']);
          //    }
          //  }
          //  else {
          //    if (this.Job == "Jobs")
          //      this.toastr.warning('Job title already exist.');
          //    else
          //      this.toastr.warning('Business name already exist.');
          //  }

          //}
          if (response) {

            if (this.Job == "Jobs") {
              this.toastr.success('Job posted successfully.It will be available on website after the admin approves it.');
              this._router.navigate(['bumanagejobs']);
            }
            else {
              this.toastr.success('Business listing posted successfully. It will be available on website after the admin approves it.');
              this._router.navigate(['bumanagead']);
            }


          }
          else {
            //if (this.Job == "Jobs")
            //  this.toastr.warning('Job title already exist.');
            //else
            //  this.toastr.warning('Business listing title already exist.');
            this.toastr.error('Problem with service. Please try again later!');

          }

        },
          (error) => {
            console.log(error);
            this.toastr.error('Problem with service. Please try again later!');
          }
        );
    }
    else if (this.action == "edit") {
      //debugger;
      if (this.FormPostAd.value.subCategoryId == "")
        this.FormPostAd.value.subCategoryId = 0;

      if ((this.FormPostAd.value.adLogoUrl != null) && (this.FormPostAd.value.adLogoUrl != undefined) && (this.FormPostAd.value.adLogoUrl != "")) {
        if (this.remainingLogoImage - 1 < 0) {
          this.toastr.warning("Logo subscription fully utilised.Please choose another plan.");
          return;
        }
        else {
          this.remainingLogoImage = this.remainingLogoImage - 1;
        }
      }
      else if (this.extLogos.length > 0) {
        this.FormPostAd.value.adLogoUrl = this.extLogos.slice()[0];
      }

      if (this.extImages.length > 0) {
        if ((this.imageresponse != null) || (this.imageresponse != undefined)) {
          this.FormPostAd.value.imageUr = this.imageresponse;
          var splitsEditImage = new String(this.FormPostAd.value.imageUrl);
          var afterEditSplit = splitsEditImage.split(",");
          var count = afterEditSplit.length;
          var finalcount=this.extImages.length + afterEditSplit.length;
          if(finalcount >5)
          {
            this.toastr.warning('Only 5 image  allowed to upload');
            return;
          }
          if ((this.FormPostAd.value.imageUrl != null) && (this.remainingAdImage - count < 0)) {
            this.toastr.warning("Business listing image subscription fully utilised.Please choose another plan or reduce number of images.");
            return;
          }
          else {
            this.remainingAdImage = this.remainingAdImage - count;
          }
        }

        if (this.imageresponse === undefined)
          this.FormPostAd.value.imageUrl = this.extImages.join();
        else {
          this.FormPostAd.value.imageUrl = this.imageresponse + "," + this.extImages.join();
        }

      }
      else {
        if ((this.imageresponse != null) || (this.imageresponse != undefined)) {
          this.FormPostAd.value.imageUr = this.imageresponse;
          var splitsEditImage = new String(this.FormPostAd.value.imageUrl);
          var afterEditSplit = splitsEditImage.split(",");
          var count = afterEditSplit.length;

          if ((this.FormPostAd.value.imageUrl != null) && (this.remainingAdImage - count < 0)) {
            this.toastr.warning("Business listing image subscription fully utilised.Please choose another plan or reduce number of images.");
            return;
          }
          else {
            this.remainingAdImage = this.remainingAdImage - count;
          }
        }
        else if ((this.imageresponse == null) || (this.imageresponse == undefined)) {
          this.FormPostAd.value.imageUrl = "";
        }
      }

      this.FormPostAd.value.planId = this.tempPlanId;
      this.FormPostAd.value.logoCount = this.remainingLogoImage;
      this.FormPostAd.value.adImageCount = this.remainingAdImage;
      this.FormPostAd.value.adId = this._route.snapshot.queryParams['adId'];
      this._adDetailsService.updateAd(this.FormPostAd.value)
        .pipe(first())
        .subscribe(
          response => {
            console.log(response);
            if (response.status === 200) {
              if ((<any>response)._body === "true") {
                if (this.Job == "Jobs") {
                  this.toastr.success('Job updated successfully.');
                  this._router.navigate(['bumanagejobs']);
                }
                else {
                  this.toastr.success('Business listing updated successfully.');
                  this._router.navigate(['bumanagead']);
                }
              }
              else {
                //if (this.Job == "Jobs")
                //  this.toastr.warning('Job title already exist.');
                //else
                //  this.toastr.warning('Business listing title already exist.');
                this.toastr.error('Problem with service. Please try again later!');
              }

            }
            //if (response) {
             
            //    if (this.Job == "Jobs") {
            //      this.toastr.success('Job updated successfully.');
            //      this._router.navigate(['bumanagejobs']);
            //    }
            //    else {
            //      this.toastr.success('Business listing updated successfully.');
            //      this._router.navigate(['bumanagead']);
            //    }
            
              
            //}
            //else {
            //  if (this.Job == "Jobs")
            //    this.toastr.warning('Job title already exist.');
            //  else
            //    this.toastr.warning('Business listing title already exist.');

            //}

          },
          (error) => {
            console.log(error);
            this.toastr.error('Problem with service. Please try again later!');
          }
        );
    }

  }

  // Get User Details on Posted For selection

  subscriptionPlanDetails(event: any) {
    debugger;
    console.log(event.target.value);

    this.paymentPlanService.getPlanDetails(event.target.value).subscribe(data => {
      var planIdDetails = JSON.parse(data._body);
      console.log(planIdDetails);
      this.previousPlan = planIdDetails;
      this.tempPlanId = planIdDetails.planId;
      this.remainingLogoImage = planIdDetails.logoCount;
      this.remainingAdImage = planIdDetails.adImageCount;
      this.remainingJobsCount = planIdDetails.jobsAdvCount;
      if ((this.Job == "Jobs") && (this.remainingLogoImage == 0 || this.remainingJobsCount == 0)) {
        //console.log('under jobs');
        this.toastr.info("Logo/image subscription exhausted for this plan. Jobs");
      }
      else if ((this.Job != "Jobs") && (this.remainingLogoImage == 0 || this.remainingAdImage == 0)) {

        //console.log('under ads');
        this.toastr.info("Logo/image subscription exhausted for this plan.");
      }
      this.FormPostAd.patchValue({
        logoCount: planIdDetails.logoCount,
        adImageCount: planIdDetails.adImageCount,
        jobsCount: planIdDetails.jobsAdvCount
      });
    });
  }



  GetUserDetails(event: any) {
    var userId = event.target.value;
    this.FetchUserInfo(userId);
  }


  FetchUserInfo(UserId: any) {
    debugger;

    this._userService.getUserById(UserId).subscribe(data => {
      var userdata = JSON.parse(data._body);
     this._allState = this._commonService.StateDDL(userdata.countryId);
      this._allCity = this._commonService.BUCityDDL(userdata.stateId);

      this._commonService.BUCityDDL(userdata.stateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });

      this.FillSubscriptionPlan(userdata.userId);
      ////debugger;
      if (this.action == "") {
        this.FormPostAd.patchValue({
          email: userdata.email,
          faxNumber: userdata.faxNumber,
          addressStreet1: userdata.addressStreet1,
          addressStreet2: userdata.addressStreet2,
          countryId: userdata.countryId,
          stateId: userdata.stateId.toString(),
          cityId: userdata.cityId.toString(),
          zipCode: userdata.zipCode,
          postedFor: userdata.userId,
          contactPersonName: userdata.firstName + " " + userdata.lastName,
          contactNumber: userdata.contactNumber,
          alternateContactNumber: userdata.alternateContactNumber,
          adId: "",
          title: "",
          stateName: "",
          cityName: "",
          tagLine: "",
          postedName: "",
          categoryId: this.FormPostAd.value.categoryId,
          categoryName: "",
          subCategoryId: "",
          subCategoryName: "",
          countryCodeContact: "1",
          website: "",
          servicesOffered: "",
          description: "",
          validTillDate: "",
          adLogoUrl: "",
          adImageUrl: "",
          adextLogoUrl: "",
          adextImageUrl: "",
          createdBy: "",
          isPaidAd: "",
          logoCount: 0,
          adImageCount: 0,
          OtherCity: ""
        });
      }

      else if (this.action == "edit") {

        this.FormPostAd.patchValue({
          email: userdata.email,
          faxNumber: userdata.faxNumber,
          addressStreet1: userdata.addressStreet1,
          addressStreet2: userdata.addressStreet2,
          countryId: userdata.countryId,
          stateId: userdata.stateId,
          cityId: userdata.cityId,
          zipCode: userdata.zipCode,
          postedFor: userdata.userId,
          contactPersonName: userdata.firstName + " " + userdata.lastName,
          contactNumber: userdata.contactNumber,
          alternateContactNumber: userdata.alternateContactNumber,
          adId: this.FormPostAd.value.adId,
          title: this.FormPostAd.value.title,
          stateName: this.FormPostAd.value.stateName,
          cityName: this.FormPostAd.value.cityName,
          tagLine: this.FormPostAd.value.tagLine,
          postedName: this.FormPostAd.value.postedName,
          categoryId: this.FormPostAd.value.categoryId,
          categoryName: this.FormPostAd.value.categoryName,
          subCategoryId: this.FormPostAd.value.subCategoryId,
          subCategoryName: this.FormPostAd.value.subCategoryName,
          countryCodeContact: "1",
          website: this.FormPostAd.value.website,
          servicesOffered: this.FormPostAd.value.servicesOffered,
          description: this.FormPostAd.value.description,
          validTillDate: this.FormPostAd.value.validTillDate,
          adLogoUrl: this.FormPostAd.value.adLogoUrl,
          adImageUrl: this.FormPostAd.value.adImageUrl,
          adextLogoUrl: this.FormPostAd.value.adextLogoUrl,
          adextImageUrl: this.FormPostAd.value.adextImageUrl,
          createdBy: this.FormPostAd.value.createdBy,
          isPaidAd: this.isPaidAd,
          logoCount: 0,
          adImageCount: 0,
          OtherCity: this.FormPostAd.value.OtherCity
        });


      }
    });


  }

  renderExistingImages(imageUrls, adId, uploadType,isPaidAd) {
    debugger;
    if (isPaidAd) {
      if (imageUrls != "" ) {
        var extImages = imageUrls.split(',');
        if (extImages.length > 0) {
          if (uploadType == "Logo") {
            for (let i = 0; i < extImages.length; i++) {
              this.extLogoUrls.push(this.commonimagepath + 'Ads/' + adId + '/Logo/' + extImages[i]);
              this.extLogos.push(extImages[i]);
              this.displayExtLogo = 'col-md-6';
            }
          }
          else if (uploadType == "Images") {
            for (let i = 0; i < extImages.length; i++) { ////debugger;
              this.extImageurls.push(this.commonimagepath + 'Ads/' + adId + '/Images/' + extImages[i]);
              this.extImages.push(extImages[i]);
              this.displayExtImages = 'col-md-6';
            }
          }
          // this.imagepath+'Ads/'+adId+'/Logo/'+picurl;
        }
      }
      else {
        if (uploadType == "Images")
          this.displayExtImages = 'col-md-6 d-none';
        if (uploadType == "Logo")
          this.displayExtLogo = 'col-md-6 d-none';

      }

    }
    else {
      if (uploadType == "Images")
        this.displayExtImages = 'col-md-6 d-none';
      if (uploadType == "Logo")
        this.displayExtLogo = 'col-md-6 d-none';

    }
  }

  onextLogoUrldelete(i) {
    this.extLogoUrls.splice(i, 1);
    this.extLogos.pop();
  }
  onextextImageurldelete(i) {
    this.extImageurls.splice(i, 1);
    this.extImages.splice(i, 1);
    console.log(this.extImageurls);
    console.log(this.extImages);
  }

  getAdDetailsById(adId) {
    debugger;
    this._adDetailsService.getAdDetailsById(adId).subscribe(data => {
      this.action = "edit";
      this.saveButtonText = "Update";
      if (this.Job == "Jobs") {
        this.titlecss = "nav-icon icon-briefcase";
        this.titleText = "Edit Job";
      }
      else
        this.titleText = "Edit Business Listing";
      this.displayText = 'btn btn-sm btn-primary d-none';
      debugger;
      // Ad Images Retrieval
      this.displayExtUploads = 'form-bg-section';
      if (data.isPaidAd)  {
        this.selected("Paid");
        this.adTypePaid = true;
        this.adTypeFree = false;
        this.RadioPaid = false;
        this.RadioFree = true;
        if (data.imageUrl!=null)
        this.renderExistingImages(data.imageUrl, data.adId, "Images",true);
      }

      else {
        this.displayExtImages = 'col-md-6 d-none';
        this.adTypePaid = false;
        this.adTypeFree = true;
        this.RadioPaid = true;
        this.RadioFree = false;
      }

      // Ad Logo Retrieval
      if (data.isPaidAd) {
        this.selected("Paid");
        this.adTypePaid = true;
        this.adTypeFree = false;
        this.RadioPaid = false;
        this.RadioFree = true;
        if (data.adLogoUrl != null)
        this.renderExistingImages(data.adLogoUrl, data.adId, "Logo",true);
      }

      else {
        this.displayExtLogo = 'col-md-6 d-none';
        this.adTypePaid = false;
        this.adTypeFree = true;
        this.RadioPaid = true;
        this.RadioFree = false;
      }

      if (data.imageUrl == null && data.adLogoUrl == null) {
        this.RadioPaid = true;
        this.RadioFree = false;
        this.displayExtUploads = 'form-bg-section d-none';
      }
      else if (data.imageUrl == "" && data.adLogoUrl == "") {
        this.RadioPaid = true;
        this.RadioFree = false;
        this.displayExtUploads = 'form-bg-section d-none';
      }
      this._commonService.CountryDDL().subscribe(data => {
        console.log(data);
        //   //debugger;
        this.countries = data;
      });

      this._commonService.StateDDL(data.countryId).subscribe(data => {
        debugger
        console.log(data);
        this.states = data;

      });

      this._commonService.BUCityDDL(data.stateId).subscribe(data => {
        debugger;
        console.log(data);
        this.cities = data;
      });


      this._commonService.CategoryDDL().subscribe(data => {
        debugger;
        this.categories = data;
      });
      this._commonService.SubCategoryDDL(data.categoryId).subscribe(data => {
        debugger;
        this.subcategories = data;


      });


      // Show other city
      if (data.cityId === 9999) {
        this.showOtherCity = "col-md-4";
      }
      else
        this.showOtherCity = "d-none";


      this._allState = this._commonService.StateDDL(data.countryId);
      this._allCity = this._commonService.BUCityDDL(data.stateId);
      this._allSubCategory = this._commonService.SubCategoryDDL(data.categoryId);
      this._allCategory = this._commonService.CategoryDDL();
      this.FillSubscriptionPlan(data.postedFor);
      this.countrycode = this.countryCodesNew;
      
      console.log("AdObject");
      console.log(data);
      this.gettoday();
      if (data.validTillDate !== null)
        this.validTill = data.validTillDate.split('T')[0];
      else
        this.validTill = this.minDate;
      if (data.countryCodeContact === null)
        data.countryCodeContact = "1";
      this.FormPostAd.patchValue({
        adId: data.adId,
        title: data.title,
        email: data.email,
        postedFor: data.postedFor,
        countryId: data.countryId.toString(),
        stateId: data.stateId.toString(),
        cityId: data.cityId.toString(),
        categoryId: data.categoryId.toString(),
        subCategoryId: data.subCategoryId.toString(),
        faxNumber: data.faxNumber,
        addressStreet1: data.addressStreet1,
        addressStreet2: data.addressStreet2,
        stateName: data.stateName,
        cityName: data.cityName,
        zipCode: data.zipCode,
        tagLine: data.tagLine,
        postedName: data.postedForName,
        categoryName: data.categoryName,
        subCategoryName: data.subCategoryName,
        contactPersonName: data.contactPersonName,
        contactNumber: data.contactNumber,
        countryCodeContact: data.countryCodeContact,
        website: data.website,
        servicesOffered: data.servicesOffered,
        description: data.description,
        alternateContactNumber: data.alternateContactNumber,
        validTillDate: this.validTill,
        adLogoUrl: data.adLogoUrl,
        adImageUrl: data.imageUrl,
        adextLogoUrl: "",
        adextImageUrl: "",
        createdBy: data.createdBy,
        isPaidAd: data.isPaidAd,
        logoCount: 0,
        adImageCount: 0,
        OtherCity: data.otherCity
      });

    });
  }

  deleteAd(AdId: number) {
    if (confirm('Are you sure to delete this business listing?')) {
      this._adDetailsService.deleteAd(AdId).subscribe(response => { console.log(response); },
        (error) => {
          console.log(error);

          this.statusMessage = "Problem with service. Please try again later!";
        });
    }
  }

  FillJobCategoryDDL() { this._allCategory = this._commonService.JobCategoryDDL(); }
  // 

  selected(listingType: string) {
    debugger;
    if (listingType == "Free") {
      this.isPaidAd = false;

      this.displayPaidSection = 'col-md-12 d-none';
    }
    if (listingType == "Paid") {
      this.isPaidAd = true;

      this.displayPaidSection = 'col-md-12';
    }
  }


  //  Dropdowns


  FillCountryDDL() {
   this._allCountry = this._commonService.CountryDDL();
    //this._allCountry = this._commonService.CountryDDL();
    this.CountryId = "1";
    //this._allState = this._commonService.StateDDL(this.CountryId);
    //this.StateId = "22";
    //this.StateName=event.target.options[this.StateId].innerHTML;
   // this._allCity = this._commonService.BUCityDDL(this.StateId);
   

    this._commonService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      this.states = data;
    });
    this._allState = this._commonService.StateDDL(this.CountryId);   
    this._allCity = this._commonService.BUCityDDL(this.StateId);
  }

  FillCategoryDDL() {
    debugger;
    this._allCategory = this._commonService.CategoryDDL();

    this._commonService.CategoryDDL().subscribe(data => {
      debugger;
      this.categories = data;


    });

  }
  FillSubCategoryDDL(event: any) {
    debugger;
    this.CategoryId = event.categoryId;
    this.CategoryName = event.categoryName;
    this._allSubCategory = this._commonService.SubCategoryDDL(this.CategoryId);
    this._commonService.SubCategoryDDL(this.CategoryId).subscribe(data => {
      debugger;
      this.subcategories = data;


    });
    this.FormPostAd.categoryName = this.CategoryName;
    var x = document.getElementsByClassName("ng-value-label");
    if (x.length > 0) {
      x[1].innerHTML = "--Select Subcategory--";
    }


  }


  FillSubCategoryName(event: any) {
    debugger;
    this.SubCategoryId = event.subCategoryId;
    this.SubCategoryName = event.subCategoryName;
    this.FormPostAd.subCategoryName = this.SubCategoryName;


  }



  FillUserDDL() {
    //////debugger;
    this._allUser = this._commonService.UserDDL();
  }

  FillStateDDL(event: any) {

    debugger;
    if ((event.target.value == "0") || ((event.target.value == ""))) {
      this.FormPostAd.value.countryId = 0;
      //this._allState = this._commonService.StateDDL("0");
      var x = document.getElementsByClassName("ng-value-label");
      x[3].innerHTML = "--Select State--";
      x[4].innerHTML = "--Select City--";
      //var i;
      //for (i = 3; i < x.length; i++) {
      //  x[i].innerHTML = "";
      //}
        this.states = [];
      this.cities = [];
      this.FillCityDDL(0);
      console.log(this.states);
      this.FormPostAd.value.stateId = 0;
      //this._allCity = this._commonService.CityDDL("0");
      this.FormPostAd.value.cityId = 0;
    }
    else {
      this.CountryId = event.countryId;

      this._allState = this._commonService.StateDDL("1");
      debugger;
      this._commonService.StateDDL("1").subscribe(data => {
        console.log(data);
        //debugger;
        this.states = data;
      });

      this._commonService.BUCityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
      this._allState = this._commonService.StateDDL(this.CountryId);
      this.CountryName = event.target.options[this.CountryId].innerHTML;
      this.FormPostAd.countryName = this.CountryName;
    }
      
  }


  FillCityDDL(event: any)
  {
    debugger;
    var x = document.getElementsByClassName("ng-value-label");
    x[2].innerHTML = "--Select City--";
    if (event!=0) {
      this.StateId = event.stateId;
      this.StateName = event.stateName;
      this._allCity = this._commonService.BUCityDDL(this.StateId);
      //this.StateName = event.target.options[this.StateId].innerHTML;  
      this.FormPostAd.stateName = this.StateName;

      this._commonService.BUCityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
    }
    else {
      var x = document.getElementsByClassName("ng-value-label");
      this.cities = [];
    }
    
  }

  FillCityName(event: any) {
    debugger;
    this.CityId = event.cityId;
    this.CityName = event.cityName;

    if (event.cityId === "9999") {
      this.showOtherCity = "col-md-4";
    }
    else
      this.showOtherCity = "d-none";
    //this.CityName = event.target.options[this.CityId].innerHTML;
    //this.FormPostAd.cityName = this.CityName;

  }
  
  NavigatetoManageAd() {
    if (this.jobdivshow == true) {
      this._router.navigate(['/bumanagejobs']);
    }
    else {
      this._router.navigate(['/bumanagead']);
    }
  }
  navigatetocontactus()
  {
    this._router.navigate(['/contact']);
  }
  FillSubscriptionPlan(userId: number) {
    this._allPlan = this.paymentPlanService.UserPlanDDL(userId);
  }

  reset() {
    this.ngOnInit();
    this.UploadCmpt.onimagedelete(0);
    this.MultiUploadCmpt.onimagedelete(0);
  }

  refresh(): void {
    debugger;
    window.location.reload();
    this.UploadCmpt.ngOnInit();
    this.MultiUploadCmpt.ngOnInit();

  }


}
