import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BupostadComponent } from './bupostad.component';

describe('BupostadComponent', () => {
  let component: BupostadComponent;
  let fixture: ComponentFixture<BupostadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BupostadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BupostadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
