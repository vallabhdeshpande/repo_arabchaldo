import { Component, OnInit } from '@angular/core';
import { WebsiteService} from '../../shared/website.service'
import { Manageblog } from '../../shared/manageblog'
import { from } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  blogpost= new Manageblog();
  blogposts: Manageblog[]=[];
  ImageUrl: string;
  Imageurls = [];
  blogDetails=[];
  imagepath = environment.imagepath;
  blogNoRecords: string = "d-none"; 
  constructor(private WebsiteService : WebsiteService,private router: Router) { }


  ngOnInit() {
    this.getBlogList();
  }

  getBlogList(): void {
  debugger;
    this.WebsiteService.getBlogList().subscribe(blogposts => {
      //blogposts = blogposts.json();    
      if (blogposts != null && blogposts.length>0) {
        this.blogposts = blogposts;
      }
      else {
        this.blogNoRecords = "no-record";
        this.blogposts = [];
      }
      console.log(this.blogposts);
    });
    //bannerposts.imageUrl=bannerposts.imageUrl;  

  }
  getPicUrl(picurl: string, blogId) {
    if (picurl == null)
      return "assets/img/default/no_image_placeholder.jpg";
    else if (picurl == "")
      return "assets/img/default/no_image_placeholder.jpg";
    else
      //return this.imagepath+'Banners/'+bannerId+'/Image/'+picurl;
      return this.imagepath + 'blogs/' + blogId + '/Image/' + picurl;
  } 

  btnPostNew() {
    console.log(localStorage.getItem('roleName'));
    if (localStorage.getItem('token') === null){
      this.router.navigate(['/login']);
    }
    else {
      console.log(localStorage.getItem);
      if ((localStorage.getItem('roleName') === 'SuperAdmin') || (localStorage.getItem('roleName') === 'Admin'))
      {
        this.router.navigate(['/theme/manageblog']);
      }
      else {
        this.router.navigate(['/buadblog']);
      }
    }
  }

}
