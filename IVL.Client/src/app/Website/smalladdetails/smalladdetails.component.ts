import { Component, OnInit, ViewChild } from '@angular/core';
import { SmallAdDetails } from '../../shared/addetails.model';
import { Router, ActivatedRoute } from '@angular/router';

import { WebsiteService } from '../../shared/website.service'
import { HttpEventType, HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { FormBuilder, Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from '../../views/shared/common.service';
import { Common, category, Subcategory } from '../../views/shared/common.model';
import { FileuploadComponent } from '../../../app/Website/fileupload/fileupload.component';
import { Input, ElementRef } from '@angular/core';
import { } from 'googlemaps';

@Component({
  selector: 'app-smalladdetails',
  templateUrl: './smalladdetails.component.html',
  styleUrls: ['./smalladdetails.component.scss']
})
export class SmalladdetailsComponent implements OnInit {

  @ViewChild(FileuploadComponent, { static: false }) FileUploadCmpt: FileuploadComponent;
  JobContactForm: any;
  adposts: SmallAdDetails[];
  statusMessage: string;
  logoUrl: string;
  logourls = [];
  mailfor: string = "Jobs";
  submitted = false;
  imageurls = [];
  multiimageurls = [];
  addressforMap: string = "";
  adpost = new SmallAdDetails();;
  imagepath = environment.imagepath;
  baseUrl = environment.baseUrl;
  showRreplyButton: string = "text-right d-none";
  jobAdEmailId: string;
  jobadId: number;
  jobadTitle: string;
  public fileresponse: string[];
  bannerSlickcss = "carousel ad-img-banner";
  categories: category[];
  category = new category();
  Subcategories: Subcategory[];
  Subcategory = new Subcategory();
  categoryId: string;
  SubcategoryId: string;
  showImage: string = "";
  showLogo: string = "";
  showDescription: string = "";
  showAddress: boolean = true;
  showMap: boolean = true;
  PremiumtagAd: boolean = false;
  mailto: string = "";
  map: google.maps.Map;
  showtagline: string = "";
  adhidden: boolean;
  description: string = "";
  constructor(private toastr: ToastrService, private formbulider: FormBuilder, private WebsiteService: WebsiteService, private router: Router, private route: ActivatedRoute, private http: HttpClient, public CommonService: CommonService) { }

  banners = [
    { img: "assets/website/images/ad-detail.jpg" },
    { img: "assets/website/images/ad-detail.jpg" },
    { img: "assets/website/images/ad-detail.jpg" },
    { img: "assets/website/images/ad-detail.jpg" },
  ];

  bannerConfig = {
    "dots": true,
    "arrow": true,
    "autoplay": true,
    "slidesToShow": 1,
    "slidesToScroll": 1,
  };

  ngOnInit() {
   
    
    debugger
    this.getSmallAdDetailsById(this.route.snapshot.queryParams['smalladId']);


  }
  readmore: boolean = false;
  readMore() {
    this.readmore = !this.readmore;
  }
 

  getSmallAdDetailsById(smalladId) {
    debugger
    this.WebsiteService.getSmallAdDetailsById(smalladId).subscribe(adposts => {
      console.log(adposts);
      this.adpost.smalladId = adposts.smalladId;
      this.jobadId = this.adpost.smalladId;
      this.adpost.PostedForName = adposts.postedForName;
      this.adpost.ContactPersonName = adposts.contactPersonName;
      this.adpost.Email = adposts.email;
      this.jobAdEmailId = adposts.email;
      this.mailto = "mailto:" + adposts.email;
      this.adpost.ContactNumber = adposts.contactNumber;
      this.adpost.AlternateContactNumber = adposts.alternateContactNumber;
      this.adpost.ContactPersonName = adposts.contactPersonName;
      this.adpost.Website = adposts.website;
      this.adpost.FaxNumber = adposts.faxNumber;
     
      this.adpost.Title = adposts.title;
      this.jobadTitle = this.adpost.Title;
     
      if (adposts.tagLine !== "") {
        this.showtagline = "";
        this.adpost.TagLine = adposts.tagLine;
      }
      else
        this.showtagline = "d-none";

      if (adposts.description !== "") {
        this.showDescription = "";
        this.adpost.Description = adposts.description;
        this.description = adposts.description;
      }
      else
        this.showDescription = "d-none";

      //AddressStreet1
      if (adposts.addressStreet1 == "" || adposts.addressStreet1 == undefined) {
        this.adpost.AddressStreet1 = "";
      }
      else
        this.adpost.AddressStreet1 = adposts.addressStreet1 + ",";
      //AddressStreet2
      if (adposts.addressStreet2 == "" || adposts.addressStreet2 == undefined) {
        this.adpost.AddressStreet2 = "";
      }
      else
        this.adpost.AddressStreet2 = adposts.addressStreet2 + ",";
      //CityName
      if (adposts.cityName == "" || adposts.cityName == undefined) {
        this.adpost.CityName = "";
      }
      else
        this.adpost.CityName = adposts.cityName + ",";
      //StateName
      if (adposts.stateName == "" || adposts.stateName == undefined) {
        this.adpost.StateName = "";
      }
      else
        this.adpost.StateName = adposts.stateName + ",";
      //countryName
      if (adposts.countryName == "" || adposts.countryName == undefined) {
        this.adpost.CountryName = "";
      }
      else
        this.adpost.CountryName = adposts.countryName + ".";

      this.adpost.CreatedBy = adposts.createdBy;
      this.adpost.CreatedDate = adposts.createdDate.split('T')[0];
      this.adpost.ZipCode = adposts.zipCode;
      this.adpost.visitorCount = adposts.visitorCount;
      if (adposts.categoryName === "Jobs") {
        this.showRreplyButton = "text-right";
        this.bannerSlickcss = "carousel ad-img-banner d-none";;
      }


      if (adposts.adLogoUrl !== null) {

        if (adposts.adLogoUrl !== "") {
          this.logoUrl = this.imagepath + 'SmallAds/' + smalladId + '/Logo/' + adposts.adLogoUrl;
          this.logourls.push(this.logoUrl);
          if (this.logourls.length > 0) {
            this.adpost.AdLogoUrl = adposts.adLogoUrl;
            this.showLogo = "";
          }
        }
        else
          this.showLogo = "d-none";
      }
      else
        this.showLogo = "d-none";

      this.adpost.AdImageUrl = adposts.imageUrl;
      if (adposts.imageUrl !== null) {
        if (adposts.imageUrl !== "") {
          this.multiimageurls = adposts.imageUrl.split(',');
          if (this.multiimageurls.length > 0) {
            this.showImage = "";
            for (let i = 0; i < this.multiimageurls.length; i++) {
              this.imageurls.push(this.imagepath + 'SmallAds/' + smalladId + '/Images/' + this.multiimageurls[i])
            }
          }
          else
            this.showImage = "d-none";
        }
        else
          this.showImage = "d-none";
      }
      else
        this.showImage = "d-none";


      this.addressforMap = adposts.addressStreet1 + "," + adposts.addressStreet2 + "," + adposts.zipCode + "," + adposts.cityName + "," + adposts.stateName + "," + adposts.countryName;
      if (adposts.addressStreet1 === "" && adposts.addressStreet2 === "" && (adposts.zipCode === "" || adposts.zipCode === null) && (adposts.cityName === "" || adposts.cityName === null) && (adposts.stateName === "" || adposts.stateName === null) && (adposts.countryName === "" || adposts.countryName === null)) {
        this.showAddress = false;
        this.showMap = true;
        this.displayGoogleMap("");
      }
      else {
        this.showAddress = true;
        this.showMap = true;
        this.displayGoogleMap(this.addressforMap);
      }
      console.log(adposts);
      console.log(this.addressforMap);
      if (adposts.isFeatured)
        this.PremiumtagAd = true;
      else
        this.PremiumtagAd = true;
    });
  }
  
  displayGoogleMap(address: string) {

    var geocoder = new google.maps.Geocoder();
    if (address == "") {
      var latitude = 42.331429;
      var longitude = -83.045753;

      const mapProperties =
      {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        marker: new google.maps.Marker({ position: { lat: latitude, lng: longitude }, animation: google.maps.Animation.BOUNCE })
      };
      this.map = new google.maps.Map(document.getElementById('map'), mapProperties);
      mapProperties.marker.setMap(this.map);
    }

    else {


      geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          latitude = results[0].geometry.location.lat();
          longitude = results[0].geometry.location.lng();
          //alert(latitude);

        }
        //debugger;
        const mapProperties =
        {
          center: new google.maps.LatLng(latitude, longitude),
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          marker: new google.maps.Marker({ position: { lat: latitude, lng: longitude }, animation: google.maps.Animation.BOUNCE })
        };
        this.map = new google.maps.Map(document.getElementById('map'), mapProperties);
        mapProperties.marker.setMap(this.map);

      });
    }
  }
 
  btnPostNew() {
    console.log(localStorage.getItem('roleName'));
    if (localStorage.getItem('token') === null) {
      this.router.navigate(['/login']);
    }
    else {
      console.log(localStorage.getItem);
      if ((localStorage.getItem('roleName') === 'SuperAdmin') || (localStorage.getItem('roleName') === 'Admin')) {
        this.router.navigate(['/theme/postsmallad']);
      }
      else {
        this.router.navigate(['/bupostsmallad']);
      }
    }
  }
}
