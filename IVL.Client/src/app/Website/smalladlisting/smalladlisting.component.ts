import { Component, OnInit } from '@angular/core';
import { SmallAdDetails, AdList } from '../../shared/addetails.model';
import { Router, ActivatedRoute } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { WebsiteService } from '../../shared/website.service'
import { environment } from '../../../environments/environment';
import { CommonService } from '../../views/shared/common.service';
import { Common, category, Subcategory } from '../../views/shared/common.model';


@Component({
  selector: 'app-smalladlisting',
  templateUrl: './smalladlisting.component.html',
  styleUrls: ['./smalladlisting.component.scss']
})
export class SmalladlistingComponent implements OnInit {
  adposts: SmallAdDetails[]=[];
  adpost = new SmallAdDetails();
  Paidadposts: SmallAdDetails[]=[]=[];
  paidadpost = new SmallAdDetails();
  Freeadposts: SmallAdDetails[]=[];
  Freeadpost = new SmallAdDetails();
  Premiumposts: SmallAdDetails[]=[];
  Premiumpost = new SmallAdDetails();
  Navigateurl: string;
  isVisible: boolean;
  FreeAddetails = [];
  PaidAddetails = [];
  PremiumAddetails = [];
  Addetails = [];
  adpostsDetails = [];
  imagepath = environment.imagepath;
  ImageUrl: string;
  Imageurls = [];
  pageTitle: string = "Recent Ads";
  categories: category[]=[];
  category = new category();
  Subcategories: Subcategory[]=[];
  Subcategory = new Subcategory();
  categoryId: string;
  SubcategoryId: string;
  PremiumList = false;
  FreeList = false;
  PaidList = true;
  IsPaidAd = true;
  isChecked = false;
  Showbuttongroup = "";
  premiumTag: string = "ad-block premium";
  PremiumtagList: boolean = true;
  Verifiedtag: boolean = true;
  premiumNoRecords: string = "d-none";
  paidNoRecords: string = "d-none";
  freeNoRecords: string = "d-none";
  constructor(private WebsiteService: WebsiteService, private route: ActivatedRoute, private router: Router, public CommonService: CommonService) { }

  ngOnInit() {    
      this.getSmallAdListBySearchCriteria();
  }
  getSmallAdListBySearchCriteria() {
    debugger;
    //  if(searchCriteria.includes('viewmore'))
    //  {
    debugger;
  
    this.WebsiteService.getSmallAdList("Listing").subscribe(adposts => {
      this.adposts = adposts;
      if (this.adposts !== null && this.adposts.length > 0) {
        console.log(this.adposts);
        var adlen = this.adposts.length;
        for (let i = 0; i < adlen; i++) {
          if (adposts[i].IsPaidAd == "True") {
            this.PremiumAddetails.push(this.adposts[i])
          }          
          else {
            this.FreeAddetails.push(this.adposts[i])
          }
        }
        
        if (this.FreeAddetails.length > 0) {
          this.Freeadposts = this.FreeAddetails;
        }
        else {
          this.freeNoRecords = "no-record";
          this.Freeadposts = [];
          this.FreeAddetails = [];
        }

        if (this.PremiumAddetails.length > 0) {
          this.Premiumposts = this.PremiumAddetails;
        }
        else {
          this.premiumNoRecords = "no-record";
          this.Premiumposts = [];
          this.PremiumAddetails = [];
        }
      }
      else {
        this.paidNoRecords = "no-record";
        this.freeNoRecords = "no-record";
        this.premiumNoRecords = "no-record";
        this.Premiumposts = [];
        this.PremiumAddetails = [];
        this.Freeadposts = [];
        this.FreeAddetails = [];
      }
      this.PaidList = false;
      this.PremiumList = true;
      this.FreeList = true;
      document.getElementById('premium').className = "btn btn-secondary";
      // document.getElementById('free').className = "btn btn-secondary";
      document.getElementById('all').className = "btn btn-secondary active";
    });
    // }   
  }
 
 
  btnchanged(evt, action) {
    debugger;
    debugger;
    if (action == "premium") {
      // this.Paidadposts=this.PaidAddetails;
      this.Premiumposts = this.PremiumAddetails;
      this.PaidList = false;
      this.PremiumList = true;
      this.FreeList = false;
      document.getElementById('premium').className = "btn btn-secondary  active";
      //document.getElementById('free').className = "btn btn-secondary";
      document.getElementById('all').className = "btn btn-secondary";
    }
    // else if(action=="free")
    // {
    //   this.Freeadposts=this.FreeAddetails; 
    //   this.PremiumList=false;
    //   this.PaidList=false;
    //   this.FreeList=true;  
    //   document.getElementById('paid').className = "btn btn-secondary";
    //   document.getElementById('free').className = "btn btn-secondary  active";
    //   document.getElementById('all').className = "btn btn-secondary"; 
    // }
    else if (action == "all") {
      this.Paidadposts = this.PaidAddetails;
      this.Premiumposts = this.PremiumAddetails;
      this.Freeadposts = this.FreeAddetails;
      this.PremiumList = true;
      this.PaidList = true;
      this.FreeList = true;
      document.getElementById('premium').className = "btn btn-secondary";
      //document.getElementById('free').className = "btn btn-secondary";
      document.getElementById('all').className = "btn btn-secondary  active";
    }
  }
  
  getpremiumtag(IsPremium) {
    debugger;
    if (IsPremium == "True") {
      this.premiumTag = "ad-block premium";
      this.PremiumtagList = true;
      this.Verifiedtag = false;
    }
    else {
      this.premiumTag = "ad-block premium d-none";
      this.PremiumtagList = false;
      this.Verifiedtag = true;
    }
  }

  
  btnPostNew() {
    console.log(localStorage.getItem('roleName'));
    if (localStorage.getItem('token') === null){
      this.router.navigate(['/login']);
    }
    else {
      console.log(localStorage.getItem);
      if ((localStorage.getItem('roleName') === 'SuperAdmin') || (localStorage.getItem('roleName') === 'Admin'))
      {
        this.router.navigate(['/theme/postsmallad']);
      }
      else {
        this.router.navigate(['/bupostsmallad']);
      }
    }
  }


}
