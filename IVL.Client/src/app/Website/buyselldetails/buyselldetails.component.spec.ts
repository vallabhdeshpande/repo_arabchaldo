import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyselldetailsComponent } from './buyselldetails.component';

describe('BuyselldetailsComponent', () => {
  let component: BuyselldetailsComponent;
  let fixture: ComponentFixture<BuyselldetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyselldetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyselldetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
