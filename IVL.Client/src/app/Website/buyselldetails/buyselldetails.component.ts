import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { Buysell } from '../../views/theme/buysell.model';
import { WebsiteService } from '../../shared/website.service';
import { User} from '../../shared/user.model';
import {UserService} from '../../shared/user.service';
import { environment } from '../../../environments/environment';
import { Input, ElementRef } from '@angular/core';
import { } from 'googlemaps';


@Component({
  selector: 'app-buyselldetails',
  templateUrl: './buyselldetails.component.html',
  styleUrls: ['./buyselldetails.component.scss']
})
export class BuyselldetailsComponent implements OnInit {
  buysellpost = new Buysell();;
  userdetail = new User();;
  userId: bigint;
  ImageUrl: string;
  Imageurls = [];
  multiimageurl = [];
  logourls = [];
  addressforMap: string = "";
  imagepath = environment.imagepath;
  showAddress: boolean = true;
  showMap: boolean = true;
  showImage: string = "";
  PremiumtagAd: boolean = false;
  bannerSlickcss: string = "";
  mailto: string = "";
  map: google.maps.Map;
  description:string="";
  constructor(private WebsiteService: WebsiteService, private UserService: UserService, private route: ActivatedRoute, private router: Router) { }
  bannerConfig = {
    "dots": true,
    "arrow": true,
    "autoplay": true,
    "slidesToShow": 1,
    "slidesToScroll": 1,
  };
  ngOnInit() {
    debugger
    this.getBuysellDetailsById(this.route.snapshot.queryParams['buysellId']);

    //this.getAdDetailsById(this.route.snapshot.paramMap.get("adId"));
  }

  getBuysellDetailsById(buysellId) {
    debugger
    this.WebsiteService.getBuyellById(buysellId).subscribe(buysellpost => {
      //var buysellpost = JSON.parse(buysellpost._body);
      console.log(buysellpost);
      this.buysellpost.Title = buysellpost.title;
      this.description = buysellpost.description;
      //this.buysellpost.createdDate=buysellpost.createdDate;
      this.buysellpost.CreatedDate = buysellpost.createdDate.split('T')[0];
      this.buysellpost.PostedForName = buysellpost.postedForName;
      this.buysellpost.PostedFor = buysellpost.postedFor;
      this.buysellpost.ImageUrl = buysellpost.imageUrl;
      this.buysellpost.ExpectedPrice = buysellpost.expectedPrice;
      this.buysellpost.VisitorCount = buysellpost.visitorCount;

      //AddressStreet1
      if (buysellpost.addressStreet1 == "" || buysellpost.addressStreet1 == undefined) {
        this.buysellpost.AddressStreet1 = "";
      }
      else
        this.buysellpost.AddressStreet1 = buysellpost.addressStreet1 + ",";
      //AddressStreet2
      if (buysellpost.addressStreet2 == "" || buysellpost.addressStreet2 == undefined) {
        this.buysellpost.AddressStreet2 = "";
      }
      else
        this.buysellpost.AddressStreet2 = buysellpost.addressStreet2 + ",";
      //CityName
      if (buysellpost.cityName == "" || buysellpost.cityName == undefined) {
        this.buysellpost.CityName = "";
      }
      else
        this.buysellpost.CityName = buysellpost.cityName + ",";
      //StateName
      if (buysellpost.stateName == "" || buysellpost.stateName == undefined) {
        this.buysellpost.StateName = "";
      }
      else
        this.buysellpost.StateName = buysellpost.stateName + ",";
      //countryName
      if (buysellpost.countryName == "" || buysellpost.countryName == undefined) {
        this.buysellpost.CountryName = "";
      }
      else
        this.buysellpost.CountryName = buysellpost.countryName + ".";
      if (buysellpost.imageUrl.length > 0) {
        this.showImage = "";
        if (buysellpost.isPremium == true) {
          this.multiimageurl = buysellpost.imageUrl.split(",")
          for (let i = 0; i < this.multiimageurl.length; i++) {
            this.Imageurls.push(this.imagepath + 'BuySell/' + buysellId + '/Premium/' + this.multiimageurl[i]);
          }
        }
        else
          this.Imageurls.push(this.imagepath + 'BuySell/' + buysellId + '/Free/' + buysellpost.imageUrl);
      }
      else
        this.showImage = "d-none";
      console.log(this.buysellpost.PostedFor);
      this.addressforMap = buysellpost.addressStreet1 + "," + buysellpost.addressStreet2 + "," + buysellpost.zipCode + "," + buysellpost.cityName + "," + buysellpost.stateName + "," + buysellpost.countryName;
      //this.getuserContactDetailsById(this.buysellpost.postedFor);
      if (buysellpost.addressStreet1 === "" && buysellpost.addressStreet2 === "" && (buysellpost.zipCode === "" || buysellpost.zipCode === null) && (buysellpost.cityName === "" || buysellpost.cityName === null) && (buysellpost.stateName === "" || buysellpost.stateName === null)) {
        this.showAddress = false;
        this.showMap = true;
        this.displayGoogleMap("");
      }
      else {
        this.showAddress = true;
        this.showMap = true;
        this.displayGoogleMap(this.addressforMap);
      }
      this.buysellpost.AlternateContactNumber = buysellpost.alternateContactNumber;
      this.buysellpost.CityId = buysellpost.cityId;
      this.buysellpost.ContactNumber = buysellpost.contactNumber;
      this.buysellpost.CountryId = buysellpost.countryId;
      this.buysellpost.Email = buysellpost.email;
      this.mailto = "mailto:" + buysellpost.email;
      this.buysellpost.FaxNumber = buysellpost.faxNumber;
      this.buysellpost.ContactPersonName = buysellpost.contactPersonName;
      this.buysellpost.StateId = buysellpost.stateId;
      this.buysellpost.ZipCode = buysellpost.zipCode;
      var bannerlen = buysellpost.length;
      if (buysellpost.isPremium)
        this.PremiumtagAd = true;
      else
        this.PremiumtagAd = false;
      console.log(buysellpost);
    });
  }
  getuserContactDetailsById(userId) {
    debugger
    this.UserService.getUserContactdetailsById(userId).subscribe(userdetail => {
      console.log(userdetail);
      var userdetail = JSON.parse(userdetail._body);
      this.userdetail.AddressStreet1 = userdetail.addressStreet1;
      this.userdetail.AddressStreet2 = userdetail.addressStreet2;
      this.userdetail.AlternateContactNumber = userdetail.alternateContactNumber;
      this.userdetail.CityId = userdetail.cityId;
      this.userdetail.cityName = userdetail.cityName;
      this.userdetail.ContactNumber = userdetail.contactNumber;
      this.userdetail.CountryId = userdetail.countryId;
      this.userdetail.Email = userdetail.email;
      this.userdetail.FaxNumber = userdetail.faxNumber;
      this.userdetail.FirstName = userdetail.firstName;
      this.userdetail.LastName = userdetail.lastName;
      this.userdetail.StateId = userdetail.stateId;
      this.userdetail.ZipCode = userdetail.zipCode;
    });
  }
  readmore: boolean = false;
  readMore() {
    this.readmore = !this.readmore;
  }

  displayGoogleMap(address: string) {

    var geocoder = new google.maps.Geocoder();
    if (address == "") {
      var latitude = 42.331429;
      var longitude = -83.045753;

      const mapProperties =
      {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        marker: new google.maps.Marker({ position: { lat: latitude, lng: longitude }, animation: google.maps.Animation.BOUNCE })
      };
      this.map = new google.maps.Map(document.getElementById('map'), mapProperties);
      mapProperties.marker.setMap(this.map);
    }

    else {


      geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          latitude = results[0].geometry.location.lat();
          longitude = results[0].geometry.location.lng();
          //alert(latitude);

        }
        //debugger;
        const mapProperties =
        {
          center: new google.maps.LatLng(latitude, longitude),
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          marker: new google.maps.Marker({ position: { lat: latitude, lng: longitude }, animation: google.maps.Animation.BOUNCE })
        };
        this.map = new google.maps.Map(document.getElementById('map'), mapProperties);
        mapProperties.marker.setMap(this.map);

      });
    }
  }

  btnPostNew() {
    console.log(localStorage.getItem('roleName'));
    if (localStorage.getItem('token') === null) {
      this.router.navigate(['/login']);
    }
    else {
      console.log(localStorage.getItem);
      //this.userId = localStorage.getItem('userId');
      if ((localStorage.getItem('roleName') === 'SuperAdmin') || (localStorage.getItem('roleName') === 'Admin')) {
        this.router.navigate(['/theme/addbuysell']);
      }
      else {
        this.router.navigate(['/buaddbuysell']);
      }
    }
  }
}
