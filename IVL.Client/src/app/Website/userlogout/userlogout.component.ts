import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-userlogout',
  templateUrl: './userlogout.component.html',
  styleUrls: ['./userlogout.component.scss']
})
export class UserlogoutComponent implements OnInit {
  myDateValue: Date;
  validTill: string;
  validFrom: string;
  maxDate: string;
  minDate: string;
  minValidTillDate: string;
  maxValidFromDate: string;
  model: NgbDateStruct;
  date: { year: number, month: number };
  constructor(public translate: TranslateService, private calendar: NgbCalendar) { }
  selectToday() {
    this.model = this.calendar.getToday();
  }

  selectDate(event: any) {
    var x = event.target.value;
    debugger;
  }
  ngOnInit() {
    debugger;
    if (localStorage.getItem('language') !== null) {
      this.translate.setDefaultLang(localStorage.getItem('language'));
      this.translate.use(localStorage.getItem('language'));
    }
    else {
      localStorage.setItem('language', "English");
      this.translate.setDefaultLang('English');
      this.translate.use("English");
    }
     
  //  }
   // this.gettoday();
    //this.myDateValue = new Date();
  }
  onDateChange(newDate: Date) {
    console.log(newDate);
  }

  ngAfterViewInit() {
    document.getElementById("#modalvalidFrom").setAttribute("min", this.minDate);
    document.getElementById("#modalvalidTill").setAttribute("min", this.minDate);


  }

  //dateLessThan(from: string, to: string) {
  //  return (group: FormGroup): { [key: string]: any } => {
  //    let f = group.controls[from];
  //    let t = group.controls[to];
  //    if (f.value > t.value) {
  //      return {
  //        dates: "Date from should be less than Date to"
  //      };
  //    }
  //    return {};
  //  }
  //}

  gettoday() {
    debugger;
    var CurrentDate = new Date();
    this.minDate = CurrentDate.toJSON().split('T')[0];
    CurrentDate.setMonth(CurrentDate.getMonth() + 1);
    this.maxDate = CurrentDate.toJSON().split('T')[0];
  }
  onvalidFromDateSelect(event: any) {
    debugger;
    this.minValidTillDate = event.target.value;

  }
  onvalidTillDateSelect(event: any) {
    this.maxValidFromDate = event.target.value;

  }

}
