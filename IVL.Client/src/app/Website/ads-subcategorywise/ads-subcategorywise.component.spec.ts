import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdsSubcategorywiseComponent } from './ads-subcategorywise.component';

describe('AdsSubcategorywiseComponent', () => {
  let component: AdsSubcategorywiseComponent;
  let fixture: ComponentFixture<AdsSubcategorywiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdsSubcategorywiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdsSubcategorywiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
