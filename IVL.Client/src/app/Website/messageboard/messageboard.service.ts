import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders,HttpClientModule, HttpErrorResponse } from "@angular/common/http";
import { RequestOptions,Http,Response, Headers } from '@angular/http';
import { FormBuilder, Validators } from '@angular/forms';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/merge';



import { category} from '../../views/shared/common.model';
import { MessageBoard,MessageBoardDetails} from '../../Website/messageboard/messageboard.model';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})

export class MessageboardService
{
  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });
  constructor(private _httpService: Http,private http:HttpClient){ }
  baseUrl = environment.baseUrl;

  observable;
  MessageBoardListCacheData;

  postTopic(messageboard: MessageBoard) {
    debugger
    let body = JSON.parse(JSON.stringify(messageboard));
    this.observable = null;
    this.MessageBoardListCacheData = null;
    console.log(messageboard);
    {
      return this._httpService.post(this.baseUrl + 'messageboard', body, this.options);
    }
  }

  getMessageBoardList(): Observable<any>
  {
    if (this.MessageBoardListCacheData)
    {
      console.log(' first if condition for MessageBoardList');
      return Observable.of(this.MessageBoardListCacheData);
    }
    //else if(this.observable)
    //{

    //}
    else
    {
      console.log('fetch MessageBoardListCacheData from server');
      this.observable = this.http.get(this.baseUrl + 'messageboard',
        { observe: 'response' })
          .map(response => {
            this.observable = null;
            this.MessageBoardListCacheData = null;

            if (response.status === 400) {
              console.log('error while fetching MessageBoardListCacheData from server');
              return ' problem with service';
            }
            else if (response.status === 200) {
              console.log('fetch MessageBoardListCacheData from server successfully');
              this.MessageBoardListCacheData = response.body;
              return this.MessageBoardListCacheData;
            }

          });

      return this.observable;
    }
    //return this.http.get(this.baseUrl + 'messageboard');
  }

  getMessageBoardById(messageboardid: number): Observable<any> {
    return this.http.get(this.baseUrl + 'messageboard/' + messageboardid);
  }

  deleteMessageBoard(messageboardid: number) {
    return this._httpService.delete(this.baseUrl + 'messageboard/' + messageboardid, this.options);
  }

  // Post Reply
  postReply(messageboarddetails: MessageBoardDetails) {
    debugger;
    let body = JSON.parse(JSON.stringify(messageboarddetails));
    console.log(messageboarddetails);
    {
      return this._httpService.post(this.baseUrl + 'messageboard/InsertMessageBoardDetails', body, this.options);
    }
  }

  getMessageBoardDetailsListByBoardId(messageboardid: number): Observable<any> {
    let httpOptions = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=UTF-8').set('Access-Control-Allow-Origin', '*').set('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS') };
    return this.http.get(this.baseUrl + 'messageboard/GetMessageBoardReplyByMessageBoardId?MessageBoardId=' + messageboardid, httpOptions);
  }

  deleteMessageBoardDetails(messageboarddetailsid: number) {
    return this._httpService.delete(this.baseUrl + 'messageboard/' + messageboarddetailsid, this.options);
  }
}





