export class MessageBoard {
    constructor() { 
    }

public MessageBoardId: number;
public Topic: string;
public Description: string;
public Name: string;
public CreatedBy: string;
public CategoryId: number;

}


export class MessageBoardDetails {
    constructor() { 
    }
    public MessageBoardDetailsId: number;
    public MessageBoardId: number;
    public Description: string;
    public Name: string;
    public CreatedBy: string;
}