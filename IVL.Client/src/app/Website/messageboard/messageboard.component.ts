
import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { category} from '../../views/shared/common.model';
import { MessageBoard,MessageBoardDetails} from '../../Website/messageboard/messageboard.model';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from '../../views/shared/common.service';
import { MessageboardService } from '../messageboard/messageboard.service';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { OrderPipe } from 'ngx-order-pipe';
import { environment } from '../../../environments/environment';
import {NgSelectModule, NgOption} from '@ng-select/ng-select';
@Component({
  selector: 'app-messageboard',
  templateUrl: './messageboard.component.html',
  styleUrls: ['./messageboard.component.scss']
})
export class MessageboardComponent implements OnInit {
  private _allCategory: Observable<category[]>;
  CategoryId: string = "";
  loggedInUserId: string;
  loggedInUserName: string = "";
  categories: any[];
  loggedInUserProfilePic: string="";
  FormMessageBoard: any;
  submitted = false;
  showGrid:string="card-body";
  showAddTopic:string="row row-stretch d-none";
  showReply:string="d-none";
  messageboards: MessageBoard[]=[];
  messageboardsdetails: MessageBoardDetails[]=[];
  order: string;
  reverse: any ;
  sortedCollection: any[];
  action:string;
  messageboardIdforReply:number;
  messageBoardTopic:string="";
  showCategory:string="col-sm-12";
  pageTitle:string="Message Board";
  imagepath = environment.imagepath;
  public pageSize: number = 10;
  public p: number;
  termsAccepted: boolean = false;
  filter: string = "";
  constructor(private toastr: ToastrService,private orderPipe: OrderPipe,   private _commonService: CommonService,private _messageBoardService: MessageboardService,  private _http: HttpClient, private _router: Router, private _formbulider: FormBuilder, private _route: ActivatedRoute, @Inject(DOCUMENT) document) {

    this.sortedCollection = orderPipe.transform(this.messageboards, 'topic');
    console.log(this.sortedCollection);
   }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }

  ngOnInit() {
    //debugger;
    this.FillCategoryDDL();
    document.getElementById("CategoryId").removeAttribute("disabled");
   // document.getElementById("postMessage").setAttribute("disabled", "true");
    if(localStorage.getItem('userId')!==null)
    {
    this.loggedInUserId = localStorage.getItem('userId'); 
    this.loggedInUserName = localStorage.getItem('firstName')+" "+localStorage.getItem('lastName');
    this.loggedInUserProfilePic=this.imagepath+"UserProfile/"+localStorage.getItem('userId')+"/ProfilePic/"+localStorage.getItem('profilePic');
    document.getElementById("Name").setAttribute("disabled","true");
    }
    else{

      this.loggedInUserId = "0"; 
    this.loggedInUserName = "";
    document.getElementById("Name").removeAttribute("disabled");
    document.getElementById("Topic").removeAttribute("disabled");
    }
   
  
    this.getTopicList();
    this.FormMessageBoard = this._formbulider.group({
      MessageBoardId: ['', ''],
      Topic: [this.messageBoardTopic, [Validators.required,Validators.pattern(".*\\S.*[a-zA-z ]*$")]],
      categoryId: ['', ''],
      Name: [this.loggedInUserName, [Validators.required, Validators.pattern(".*\\S.*[a-zA-z ]*$")]],
      Description: ['', [Validators.required, Validators.pattern(".*\\S.*[a-zA-z ]*$")]],
      postMessageCheckbox: ['', Validators.required],
      CreatedBy:['', '']
    });
  }

  FillCategoryDDL() {
   this._allCategory = this._commonService.CategoryDDL();

    this._commonService.CategoryDDL().subscribe(data => {
      console.log(data);
      if (data != null && data.length > 0)
        this.categories = data;
      else
        this.categories = [];
    });

    
  }

  CheckBoxClicked(event: any)
  {
    debugger;
    console.log();
    this.termsAccepted = event.target.checked
   
    //if (event.target.checked) 
    //  document.getElementById("postMessage").removeAttribute("disabled");
    //else
    //  document.getElementById("postMessage").setAttribute("disabled", "true");
   
  }

  ShowAddTopic(){
    debugger;
    this.action="Topic"
    this.showGrid="card-body d-none";
    this.showAddTopic="row row-stretch";
    this.showReply="d-none";
    this.showCategory="col-sm-12 d-none";
    this.pageTitle="Message Board";
    this.messageBoardTopic="";
    document.getElementById("Topic").innerHTML="";
    this.FormMessageBoard.reset();
    document.getElementById("CategoryId").removeAttribute("disabled");
    document.getElementById("Topic").removeAttribute("disabled");
    this.FormMessageBoard.patchValue({
      Name:this.loggedInUserName,
      categoryId:"",
    });
    
  }

  ShowPostReply(){
    debugger;
    this.action="Reply"
    this.showGrid="card-body d-none";
    this.showAddTopic="row row-stretch";
    this.showReply="d-none";
    this.showCategory="col-sm-12 d-none";
    this.pageTitle="Post Reply";
    document.getElementById("Topic").setAttribute("disabled","true");
    document.getElementById("Description").innerHTML="";
    //alert(this.messageBoardTopic);
    this.FormMessageBoard.patchValue({
      Topic:this.messageBoardTopic,
      Description:"",
      Name:this.loggedInUserName,

    });
    document.getElementById("CategoryId").setAttribute("disabled","true");
    
  }

  ShowGridPanel(){    
    debugger;
    this.showGrid="card-body";
    this.showAddTopic="row row-stretch d-none";
    this.getTopicList();
    this.showReply="d-none";
    this.messageBoardTopic="";
    //this.loggedInUserName="";
    document.getElementById("Topic").innerHTML="";
  }

  getTopicList():void{
    //debugger;
    this._messageBoardService.getMessageBoardList().subscribe(data => {
      if (data != null && data.length > 0)
        this.messageboards = data;
      else
        this.messageboards = [];
  
  });
console.log(this.messageboards);
  }

  getMessageBoardReplyList(messageBoardId,messageBoardTopic)
  {
    debugger;
    this.messageboardIdforReply=messageBoardId;
    this.messageBoardTopic=messageBoardTopic;
    
    document.getElementById("Topic").setAttribute("disabled","true");
    this.showReply="";
    this.showAddTopic="row row-stretch d-none";
    this.showGrid="card-body d-none";
    let mbdetailsdata:any[]=[];
    this._messageBoardService.getMessageBoardDetailsListByBoardId(messageBoardId).subscribe(data => {
      if (data != null && data.length > 0) {
        mbdetailsdata = data;

        // this.messageboardsdetails = mbdetailsdata;
        this.messageboardsdetails = data;
      }
      else {
        this.messageboardsdetails = [];
      }
    
  });
console.log(this.messageboardsdetails);

  }

  getPicUrl(createdBy:string,picurl:string)
    {
//debugger;
      if(picurl==null)
      return "assets/img/default/no_image_placeholder.jpg";
      else if (picurl=="")
      return "assets/img/default/no_image_placeholder.jpg";
      else     
      return this.imagepath+"UserProfile/"+createdBy+"/ProfilePic/"+picurl;

    }

  // Add Topic
  postTopic(): void
  {
    debugger;
    this.submitted = true;
    if (this.FormMessageBoard.invalid) { return; }
    if (!this.termsAccepted) {
      this.toastr.warning('Please Accept Terms & Conditions');
      return;
    }
  if(this.FormMessageBoard.value.categoryId==""||this.FormMessageBoard.value.categoryId==null )
      this.FormMessageBoard.value.categoryId = 0;
  this.FormMessageBoard.value.MessageBoardId = 0;
  if(localStorage.getItem('userId')!==null)
    {
  this.FormMessageBoard.value.CreatedBy =this.loggedInUserId;
    this.FormMessageBoard.value.Name = this.loggedInUserName;
    
    }
    else
    this.FormMessageBoard.value.CreatedBy ="0";
  console.log(this.FormMessageBoard.value)
    if (this.action==="Topic")
  {
  
      this._messageBoardService.postTopic(this.FormMessageBoard.value)
        .subscribe((response) => {
          console.log(response);
          if (response.status === 200) {
            if ((<any>response)._body === "true") {
              this.toastr.success('Topic Posted Successfully.');
              this.ShowGridPanel()
          
              this.submitted = false;
            

            }
            else {
              this.toastr.warning('Topic Already Exist.');
            }

          }

        },
          (error) => {
            console.log(error);
            this.toastr.error('Problem with service. Please try again later!');
          }
        );

  }
  else if(this.action==="Reply")
  {
    debugger;
    this.FormMessageBoard.value.MessageBoardId = this.messageboardIdforReply;
    this._messageBoardService.postReply(this.FormMessageBoard.value)
        .subscribe((response) => {
          console.log(response);
          if (response.status === 200) {
            if ((<any>response)._body === "true") {
              this.toastr.success('Reply Posted Successfully.');
              this.getMessageBoardReplyList(this.messageboardIdforReply, this.messageBoardTopic);
              document.getElementById("Topic").removeAttribute("disabled");
            }
            else {
              this.toastr.warning('Reply Already Exist.');
            }

          }
        },
          (error) => {
            console.log(error);
            this.toastr.error('Problem with service. Please try again later!');
          }
        );


    }


}
}
