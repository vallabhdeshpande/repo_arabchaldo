
import { Component, OnInit } from '@angular/core';
import { ManageblogService } from '../../views/theme/manageblog.service';
import {Router, ActivatedRoute} from "@angular/router";
import { environment } from '../../../environments/environment';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Observable } from 'rxjs';
import {Manageblog} from '../../views/theme/manageblog';
import { UserService } from '../../views/theme/user.service';
import { User } from '../../views/theme/user'; 

@Component({
  selector: 'app-bublogdetailsview',
  templateUrl: './bublogdetailsview.component.html',
  styleUrls: ['./bublogdetailsview.component.scss']
})
export class BublogdetailsviewComponent implements OnInit {

  //adposts: AdDetails[];
  blogposts: Manageblog[];
  blogForm: any;
  workflowDetails = [];
  blogpost = new Manageblog();  
  statusMessage: string;
  Address: string;
  users: User[]; 
  userDetails = new User();
  ContactDetails: string;  
  RoleName:string; 
  logoUrl: string;
  logourls = [];
  imageurls = []; 
  imagepath = environment.imagepath;
  workflow: string;
  desciption: string;

  constructor(private route: ActivatedRoute, private userService: UserService, private manageblogservice: ManageblogService,private router: Router, private http: HttpClient) { }
  manageblogback() { this.router.navigate(['/bumanageblog']); }

  ngOnInit() 
  {
    //debugger
    this.getBlogDetailsById(this.route.snapshot.queryParams['id'])
  }

  getBlogDetailsById(id)
  {
    debugger
    this.workflow=id+","+"Blog";
    this.manageblogservice.getBlog(id).subscribe(data => 
      {
        var data = JSON.parse(data._body);

        console.log(data); 
     
        this.blogpost.Title= data.title;
      this.blogpost.Description = data.description;
      this.desciption = data.description;
      //  this.blogpost.ValidTill = data.validTill;
        // debugger
        this.blogpost.validFromDate=data.validFromDate;
        this.blogpost.validTillDate=data.validTillDate;
        this.blogpost.ImageUrl = data.imageUrl;
        this.logoUrl=this.imagepath+'Blogs/'+id+'/Image/'+data.imageUrl;
        this.logourls.push(this.logoUrl);
        //debugger

        this.userService.getUserById(data.createdBy).subscribe(detail => 
        {
          var detail = JSON.parse(detail._body);

          console.log(detail);

          if (detail.addressStreet1 == null || detail.addressStreet1 == "")
          {
            detail.addressStreet1 = '';
          }
          else  {
            detail.addressStreet1 = detail.addressStreet1 + ',';
          }

          if (detail.addressStreet2 == null || detail.addressStreet2 == "")
          {
            detail.addressStreet2 = '';
          }
          else {
            detail.addressStreet2 = detail.addressStreet2 + ',';
          }
          if (detail.cityName == null || detail.cityName == "") {
            detail.cityName = '';
          }
          else {
            detail.cityName = detail.cityName + ',';
          }
          if (detail.stateName == null || detail.stateName == "") {
            detail.stateName = '';
          }
          else {
            detail.stateName = detail.stateName + ',';
          }
          if (detail.zipCode == null) {
            detail.zipCode = '';
          }
          else {
            detail.zipCode = '(' + detail.zipCode + ')';
          }
            

          //this.Address = detail.addressStreet1  + detail.addressStreet2 + detail.cityName  + detail.stateName  + "USA" + detail.zipCode ;
          this.Address = detail.addressStreet1 + "  " + detail.addressStreet2 + "  " + detail.cityName + "  " + detail.stateName + "  " + "USA" + "  " + detail.zipCode;

          if (detail.alternateContactNumber == null) {
            this.ContactDetails = detail.contactNumber;
          }
          else {
            this.ContactDetails = detail.contactNumber + '/' + detail.alternateContactNumber;
          }
          this.userDetails.Email = detail.email;
          this.userDetails.FaxNumber = detail.faxNumber;
          this.userDetails.roleName = detail.roleName;
        })
              
      });

  }   
}

