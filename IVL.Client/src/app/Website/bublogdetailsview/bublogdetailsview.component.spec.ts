import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BublogdetailsviewComponent } from './bublogdetailsview.component';

describe('BublogdetailsviewComponent', () => {
  let component: BublogdetailsviewComponent;
  let fixture: ComponentFixture<BublogdetailsviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BublogdetailsviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BublogdetailsviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
