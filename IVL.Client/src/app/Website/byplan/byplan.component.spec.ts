import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByplanComponent } from './byplan.component';

describe('ByplanComponent', () => {
  let component: ByplanComponent;
  let fixture: ComponentFixture<ByplanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByplanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
