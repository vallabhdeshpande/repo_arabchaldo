import { Component, OnInit } from '@angular/core';
import { WebsiteService} from '../../shared/website.service';
import { HttpEventType, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-terms-use',
  templateUrl: './terms-use.component.html',
  styleUrls: ['./terms-use.component.scss']
})
export class TermsUseComponent implements OnInit {

  TermsofUsedata: string="";
  TermsofUseArabicdata: string = "";
  showEng: string = "";
  showArb: string = "d-none";
  constructor(private WebsiteService: WebsiteService, private http: HttpClient) { }
  ngOnInit() {
    this.GetTermsofUseData();
    this.changeTermsOfUseData();
  }

  changeTermsOfUseData() {

    if (localStorage.getItem('language') !== null) {
      if (localStorage.getItem('language') === "English") {
        this.showEng = "";
        this.showArb = "d-none";
      }
      else {
        this.showEng = "d-none";
        this.showArb = "";
      }

    }
  }


  GetTermsofUseData()
  {
    debugger;
    this.WebsiteService.GetTermsofUseData().subscribe(data => {
      if (data != null && data.length>0) {


        console.log(data);
        this.TermsofUsedata = data[0].TermsofUse;
        this.TermsofUseArabicdata = data[0].TermsofUseArabic;
        if (localStorage.getItem('language') !== null) {
          if (localStorage.getItem('language') === "English") {

            this.TermsofUsedata = data[0].TermsofUse;
          }
          else {
            this.TermsofUseArabicdata = data[0].TermsofUseArabic;

          }

        }
        else
          this.TermsofUsedata = data[0].TermsofUse;


      }
    });
     
  }

}
