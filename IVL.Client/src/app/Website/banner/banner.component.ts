import { Component, OnInit } from '@angular/core';
import { Managebanner } from '../../shared/managebanner'
import { WebsiteService } from '../../shared/website.service'
import { Observable, observable } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Country, State, City, category, Subcategory, Roles, CountryCodes } from '../../shared/common.model';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
  
  cities: any[];
  categories: any[];
  CityName: string = "";
  CategoryName: string = "";
  showCategory: string = "col-sm-12";
  bannerpost = new Managebanner();;
  bannerposts: Managebanner[]=[];
  tempbannerposts: Managebanner[]=[];
  tempbannerpost = new Managebanner();
  keyword:string="";
  ImageUrl: string;
  Imageurls = [];
  BannerDetails = [];
  imagepath = environment.imagepath;
  private _allCity: Observable<City[]>;
  private _allCategory: Observable<category[]>;
  CityId: string = "0";
  CategoryId: string = "0";
  searchCriteria: string = "";
  style: any;
  showEng: string = "";
  showArb: string = "d-none";
  constructor(private WebsiteService: WebsiteService, private router: Router) { }

  banners = [
    { img: "assets/website/images/banner1.jpg" },
    { img: "assets/website/images/banner1.jpg" },
    { img: "assets/website/images/banner1.jpg" },
    { img: "assets/website/images/banner1.jpg" },
  ];

  bannerConfig = {
    "dots": true,
    "arrow": true,
    "autoplay": true,
    "slidesToShow": 1,
    "slidesToScroll": 1,
  };

  getBannerList(): void {
    //debuggerFillCategoryDDL()
    this.WebsiteService.getPremiumBannerList().subscribe(bannerposts => {
      if (bannerposts!=null) {
        if (bannerposts.length>0) {
          //bannerposts = bannerposts.json();    
          //   var bannerlen=bannerposts.length;
          //   var bannerlen=bannerposts.length;
          //   for (let i = 0; i < bannerlen; i++) {
          // 	  if(bannerposts[i].isVisible==true)
          // 	   this.BannerDetails.push(bannerposts[i])		
          // 	   }
          // 	   console.log(this.BannerDetails);
          //    this.bannerposts=this.BannerDetails; 
          this.bannerposts = bannerposts;
          var prembannerLength = this.bannerposts.length;
          if (prembannerLength > 15) {
            this.tempbannerposts = this.bannerposts.slice(0, 15);
            this.bannerposts = this.tempbannerposts;
          }
        }
        else {
          this.bannerposts = [];
          this.tempbannerposts=[]
        }
      }
      else {
        this.bannerposts = [];
        this.tempbannerposts = []
      }
     
      console.log(bannerposts);
    });
    //bannerposts.imageUrl=bannerposts.imageUrl;  

  }
  getPicUrl(picurl: string, bannerId) {
    if (picurl == null)
      return "assets/img/default/no_image_placeholder.jpg";
    else if (picurl == "")
      return "assets/img/default/no_image_placeholder.jpg";
    else
      //return this.imagepath+'Banners/'+bannerId+'/Image/'+picurl;
      return this.imagepath + 'Banners/' + bannerId + '/Image/' + picurl;
  }
  ngOnInit()
  {
    debugger;
    this.getBannerList();
    this.FillCategoryDDL();
    this.FillAllCityDDL();
    this.changeData();
  }

  // Region Global Search

  FillCategoryDDL() {
    //debugger;
  //this._allCategory = this.WebsiteService.CategorySearchDDL();

    this.WebsiteService.CategorySearchDDL().subscribe(data => {
      console.log(data);
      //debugger;
      if (data != null) {
        if (data.length > 0)
          this.categories = data;
        else
          this.categories = [];
      }
      else
        this.categories = [];
      
     
    });


  }
  FillAllCityDDL()
  {
    debugger;


 // this._allCity = this.WebsiteService.CitySearchDDL();

    this.WebsiteService.CitySearchDDL().subscribe(data => {
      console.log(data);
      //debugger;
     // this.cities = data;
      if (data != null) {


        if (data.length > 0)
          this.cities = data;
        else 
          this.cities = [];
      }
      else {
        this.cities = [];
      }

    });

    var x = document.getElementsByClassName("ng-placeholder");
    if (localStorage.getItem('language') == null || localStorage.getItem('language') == "English" ) {
      x[0].innerHTML = "--Select City--";
      x[1].innerHTML = "--Select Category--";
    }
      else {

      x[0].innerHTML = "--اختر مدينة--";
      x[1].innerHTML = "--اختر الفئة--";
      }
  }


  FillCategory(event: any) {
    //debugger;
  //this.CategoryId = event.target.value;

    this.CategoryId = event.categoryId;

    var x = document.getElementsByClassName("ng-placeholder");
    x[1].innerHTML = "";
  }

  FillCity(event: any) {
    debugger;

    this.CityId = event.cityId;
    var x = document.getElementsByClassName("ng-placeholder");
    x[0].innerHTML = "";
   
   

  }
  changeData() {

    if (localStorage.getItem('language') !== null) {
      if (localStorage.getItem('language') === "English") {
        this.showEng = "";
        this.showArb = "d-none";
      }
      else {
        this.showEng = "d-none";
        this.showArb = "";
      }
     
    }
    var x = document.getElementsByClassName("ng-placeholder");
    debugger;
    if (localStorage.getItem('language') == null || localStorage.getItem('language') == "English") {
      x[0].innerHTML = "--Select City--";
      x[1].innerHTML = "--Select Category--";
    }
    else {

      x[0].innerHTML = "--اختر مدينة--";
      x[1].innerHTML = "--اختر الفئة--";
    }
  }


  getSearchCriteria(keyword) {
    //debugger;
    if (keyword === undefined)
      keyword = 'null'
    else
      keyword = keyword;
    this.searchCriteria = keyword + ',' + this.CategoryId + ',' + this.CityId;
    this.router.navigate(['/searchresult'], { queryParams: { searchCriteria: this.searchCriteria } });

  }
}
export class AppComponent {

}

