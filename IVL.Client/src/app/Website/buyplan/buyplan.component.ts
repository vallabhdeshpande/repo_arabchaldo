import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { category,Subcategory} from '../../views/shared/common.model';
import {WebsiteService} from '../../shared/website.service';
import { FormBuilder, Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../../shared/user';
import { CommonService } from '../../views/shared/common.service';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
@Component({
  selector: 'app-buyplan',
  templateUrl: './buyplan.component.html',
  styleUrls: ['./buyplan.component.scss']
})
export class BuyplanComponent implements OnInit {
  private _allCategory: Observable<category[]>;
  private _allSubCategory: Observable<Subcategory[]>;
  constructor(private toastr: ToastrService, private _commonService: CommonService, private formbulider: FormBuilder, private WebsiteService: WebsiteService, private route: ActivatedRoute, private router: Router) { }
  charLength: string = "0";
  categories: any[]=[];
  subcategories: any[]=[];
  buyplanForm: any;
  submitted = false;
  loggedInUserId: string;
  SubCategoryId: string = "0";
  CategoryId: string = "0";
  DefaultCategoryId: string = "0";
  CategoryName: string = "";
  SubCategoryName: string = "";
  loggedInUserName: string = "";
  loggedInUserEmail: string = "";
  loggedInUserProfilePic: string = "";
  mailfor: string = "buyplanmail";
  showmessgage: boolean = false;
  showlogin: boolean = false;
  showsignup: string = "signup";
  ngOnInit() {

    this.charLength = "0/400";

    if (localStorage.getItem('userId') !== null) {
      this.loggedInUserId = localStorage.getItem('userId');
      this.loggedInUserName = localStorage.getItem('firstName') + " " + localStorage.getItem('lastName');
      this.loggedInUserEmail = localStorage.getItem('email');
      document.getElementById("Name").setAttribute("disabled", "true");
      document.getElementById("Email").setAttribute("disabled", "true");
      this.showlogin = false;
      this.showsignup = "d-none";
    }
    else {

      this.loggedInUserId = "0";
      this.loggedInUserName = "";
      this.loggedInUserEmail = "";
      document.getElementById("Name").removeAttribute("disabled");
      document.getElementById("Email").removeAttribute("disabled");
      this.showlogin = true;
      this.showsignup = "";
    }
    this.buyplanForm = this.formbulider.group({
      mailfor: ['', ''],
      Name: [this.loggedInUserName, Validators.required],
      Email: [this.loggedInUserEmail, [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      City: ['', Validators.required],
      categoryId: ['', Validators.required],
      categoryName: ['', ''],
      subCategoryId: ['', ''],
      subCategoryName: ['', ''],
      Phone: ['', [Validators.required, Validators.minLength(10)]],
      BusinessName: ['', Validators.required],
      BusinessPurpose: ['', Validators.required],
      PinCode: ['', [Validators.required, Validators.pattern("^[0-9]*$")]]
    });
    this.FillCategoryDDL();
  }

  onKeyUp(boxInput: HTMLInputElement) {
    debugger;
    let length = boxInput.value.length; //this will have the length of the text entered in the input box
    this.charLength = length + "/400";
    console.log(length);
  }

  Submit(): void {

    debugger;

    this.submitted = true;
    // stop the process here if form is invalid
    if (this.buyplanForm.invalid) {
      //debugger
      return;
    }
    debugger
    console.log(this.buyplanForm.value)
    debugger;
    this.buyplanForm.value.Name = this.buyplanForm.value.Name;
    this.buyplanForm.value.Phone = this.buyplanForm.value.Phone;
    this.buyplanForm.value.Email = this.buyplanForm.value.Email;
    this.buyplanForm.value.BusinessName = this.buyplanForm.value.BusinessName;
    this.buyplanForm.value.BusinessPurpose = this.buyplanForm.value.BusinessPurpose;
    this.buyplanForm.value.City = this.buyplanForm.value.City;
    this.buyplanForm.value.categoryId = this.buyplanForm.value.categoryId;
    this.buyplanForm.value.categoryName = this.buyplanForm.categoryName;
    this.buyplanForm.value.subCategoryId = this.buyplanForm.value.subCategoryId;
    this.buyplanForm.value.subCategoryName = this.buyplanForm.SubCategoryName;
    this.buyplanForm.value.PinCode = this.buyplanForm.value.PinCode;
    this.buyplanForm.value.mailfor = this.mailfor;
    if (this.buyplanForm.value.subCategoryId == "")
      this.buyplanForm.value.subCategoryId = 0;


    console.log(this.buyplanForm.value)
    this.WebsiteService.buyplan(this.buyplanForm.value)
      .subscribe((response) => {
        console.log(response);
        this.toastr.success(' Thanks for reaching us. Our support team will contact you shortly.');
        //  this.ContactForm.patchValue({
        //   Name:"",    
        //   Email:"",
        //   Comments:"",
        //   Phone:"",
        //   reason:"",


        // });

        this.buyplanForm.reset();
        this.submitted = false;
        this.showmessgage = true;
        this.ngOnInit();

      },
        (error) => {
          console.log(error);
          this.toastr.error('Problem with service. Please try again later!');

        }

      );
  }

  FillCategoryDDL() {
    this._allCategory = this._commonService.CategoryDDL();

    this._commonService.CategoryDDL().subscribe(data => {
      console.log(data);
      if (data.length > 0)
        this.categories = data;
      else
        this.categories = [];
    });
    var x = document.getElementsByClassName("ng-placeholder");
    x[0].innerHTML = "--Select Category--";
    x[1].innerHTML = "--Select SubCategory--";
  }



  FillSubCategoryDDL(event: any) {
    debugger;
    var x = document.getElementsByClassName("ng-value-label");
    x[1].innerHTML = "";
    this.CategoryId = event.categoryId;

    //this.CategoryName=event.target.options[this.CategoryId].innerHTML;   
    this._allSubCategory = this._commonService.SubCategoryDDL(this.CategoryId);
    this.buyplanForm.categoryName = event.categoryName;

    debugger;
    this._commonService.SubCategoryDDL(this.CategoryId).subscribe(data => {
      console.log(data);
      if (data!=null && data.length>0) {
        this.subcategories = data;
      }
      else
      this.subcategories = [];
    });
    var x = document.getElementsByClassName("ng-placeholder");
    x[0].innerHTML = "";
    x[1].innerHTML = "";
    x[1].innerHTML = "--Select SubCategory--";

  }

  FillSubCategoryName(event: any) {
    debugger;
    this.SubCategoryId = event.subCategoryId;
    this.buyplanForm.SubCategoryName = event.subCategoryName;
    //this.buyplanForm.subCategoryName = event.SubCategoryName;
    var x = document.getElementsByClassName("ng-placeholder");
    x[1].innerHTML = "";
  }
}
