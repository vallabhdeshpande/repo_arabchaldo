import { Component, OnInit, Inject, Output, EventEmitter, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Country, State, City, category, Subcategory, Roles, CountryCodes } from '../../views/shared/common.model';
import { User } from '../../views/theme/user';
import { UserService } from '../../views/theme/user.service';
import { Buysell } from '../../views/theme/buysell.model';
import { BuysellService } from '../../views/theme/buysell.service';
import { CommonService } from '../../views/shared/common.service';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { environment } from '../../../environments/environment';
import { element } from 'protractor';
import { first, debounce } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Paymentplan } from '../../views/theme/paymentplan';
import { Paymentplanservice } from '../../views/theme/paymentplanservice';
import { UploadComponent } from '../../../app/Website/upload/upload.component';
import { MultiUploadComponent } from '../../Website/multiupload/multiupload.component';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import * as countryCodeData from '../../../app/views/shared/countrycodes.json';
@Component({
  selector: 'app-buaddbuysell',
  templateUrl: './buaddbuysell.component.html',
  styleUrls: ['./buaddbuysell.component.scss']
})
export class BuaddbuysellComponent implements OnInit {
  @ViewChild(UploadComponent, { static: false }) UploadCmpt: UploadComponent;
  @ViewChild(MultiUploadComponent, { static: false }) MultiUploadCmpt: MultiUploadComponent;
  public _allUser: Observable<User[]>;
  public _allCountry: Observable<Country[]>;
  public _allCountryCodes: Observable<CountryCodes[]>;
  public _allState: Observable<State[]>;
  public _allStateNew: Observable<State[]>;
  public _allCity: Observable<City[]>;
  public _allCategory: Observable<category[]>;
  public _allSubCategory: Observable<Subcategory[]>;
  public _allroles: Observable<Roles[]>;
  postedFor: string = "0";
  CountryId: string = "0";
  StateId: string = "0";
  states: any[];
  cities: any[];
  countries: any[];
  CityId: string = "0";
  CategoryId: string = "0";
  SubCategoryId: string = "0";
  RoleId: string = "0";
  PostedForName: string = "";
  CountryName: string = "";
  StateName: string = "";
  CityName: string = "";
  CategoryName: string = "";
  SubCategoryName: string = "";
  logopath: string[];
  imagepath: any[];
  logoUrl: string = "";
  imageUrl: string = "";
  ABCD: string[];
  addivshow = true;
  jobdivshow = false;
  //dvImage: string[];
  FormPostAd: any;
  actionButton;
  action = "";
  myData = null;
  formData = null;
  msgs = null;
  public imagefilename: string[];
  public logofilename: string[];
  AdPost: Buysell[];
  adPost = new Buysell();
 // adimgdtls = new AdImageDetails();
 // AdImageDetailsDTO = new AdImageDetails();
  statusMessage: string;
  saveButtonText: string = "Save";
  displayText = 'btn btn-sm btn-primary';
  displayExtLogo = 'col-md-6 d-none';
  displayExtImages = 'col-md-6 d-none';
  displayExtUploads = 'form-bg-section d-none';
  displayPaidSection = 'col-md-12 d-none';
  displayFreeSection = 'col-md-12 d-none';
  maxDate: string;
  minDate: string;
  validTill: string;
  extImageurls = [];
  extLogoUrls = [];
  extImages = [];
  extLogos = [];
  //resetButtonStyle:string="block";
  public logoresponse: string[];
  public imageresponse: string[];
  roleList1: any[] = [];
  roleData: any[] = [];
  categories: any[];
  addbutton: HTMLElement;
  serverpath = environment.serverpath;
  commonimagepath = environment.imagepath;
  submitted = false;
  lastkeydown1: number = 0;
  subscription: any;
  countryCodeList: Array<any> = [];
  titleText: string = "Post New Advertisement";
  titlecss: string = "nav-icon icon-tag";
  controlEnabled: boolean = true;
  loggedInUserId: string;
  adTypeFree: boolean = true;
  adTypePaid: boolean = false;
  isPaidAd: boolean = false;
  public _allPlan: Observable<Paymentplan[]>;
  public previousPlan: Paymentplan;
  tempPlanId: number;
  tempLogoImageUrl: any;
  tempAdImageUrl: any;
  Job: string;
  DefaultCategoryId: string = "0";


  remainingbuysellCount: number = 0;
  remainingAdImage: number = 0;
  remainingLogoImage: number = 0;
  adImageCount: number;

  adimagefornewpost: string[];

  RadioFree: boolean = false;
  RadioPaid: boolean = false;
  showOtherCity: string = "d-none";
  bindStateId: number = 0;
  bindCategoryId: number = 0;
  displayBrowseforFree:boolean=true;
  validTillDate: any
  countryCodesNew: any = (countryCodeData as any).default;
  countrycode = this.countryCodesNew;
  constructor(private toastr: ToastrService, private _buySellService: BuysellService, private _userService: UserService, private paymentPlanService: Paymentplanservice, private _commonService: CommonService, private _http: HttpClient, private _router: Router, private _formbulider: FormBuilder, private _route: ActivatedRoute, @Inject(DOCUMENT) document) {

  }
  ngAfterViewInit() {
    //if
    document.getElementById("validTillDate").setAttribute("min", this.minDate);
    document.getElementById("validTillDate").setAttribute("max", this.maxDate);
    document.getElementById('#SaveAd').innerHTML = this.saveButtonText;
    document.getElementById('#dvTitle').innerHTML = this.titleText;

  }

  gettoday() {
    ////debugger;
    var CurrentDate = new Date();
    this.minDate = CurrentDate.toJSON().split('T')[0];
    CurrentDate.setMonth(CurrentDate.getMonth() + 1);
    this.maxDate = CurrentDate.toJSON().split('T')[0];
  }

  // 
  public uploadFinishedLogo = (event) => { this.logoresponse = event; }
  public uploadFinishedImage = (event) => { this.imageresponse = event; }
  // AutoComplete

  ngOnInit() {
    debugger
   // var x = document.getElementsByClassName("ng-value-label");
   // x[3].innerHTML = " Select State";
    //x[4].innerHTML = " Select City";
    this.loggedInUserId = localStorage.getItem('userId');
    this.FillCategoryDDL();
    this.FillCountryDDL();
    this.FillUserDDL();
    this.DefaultCategoryId= environment.defaultbuysellCategoryId;
    this.gettoday();
      this.DefaultCategoryId = environment.defaultbuysellCategoryId;
      this._allSubCategory = this._commonService.SubCategoryDDL(this.DefaultCategoryId);
      this.titleText = "Post New Buy/Sell Listing";
      this.titlecss = "nav-icon icon-tag";

    // this.loggedInUserId = "57";
    //this.FillCategoryDDL();

    this.FormPostAd = this._formbulider.group({
      buySellId: ['', ''],
     // title: ['', [Validators.required, Validators.pattern(".*\\S.*[a-zA-z0-9 ]")]],
      title: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      faxNumber: [''],
      addressStreet1: ['', ''],
      addressStreet2: ['', ''],
      countryId: ['1', ''],
      countryCodeContact: [1, ''],
      adLogoUrl: ['', ''],
      adImageUrl: ['', ''],
      adextLogoUrl: ['', ''],
      adextImageUrl: ['', ''],
      stateId: [this.bindStateId, ''],
      cityId: ['', ''],
      stateName: ['null', ''],
      cityName: ['null', ''],
      zipCode: ['', Validators.pattern("^[0-9]*$")],
      expectedPrice: ['', ''],
      postedFor: ['', Validators.required],
      postedName: ['null', ''],
      categoryId: [this.DefaultCategoryId, ''],
      categoryName: ['', ''],
      subCategoryId: ['', Validators.required],
      subCategoryName: ['null', ''],
      contactPersonName: ['', Validators.required],
      contactNumber: ['', [Validators.required, Validators.minLength(10)]],
      description: ['', ''],
      validTillDate: ['', ''],
      alternateContactNumber: [''],
      createdBy: ['', ''],
      isPaidAd: ['', ''],
      BuySellCount: [''],
      planId: [''],
      OtherCity: ['']
    });


    this.remainingbuysellCount = 0;
    this.tempPlanId = 0;
    this.remainingLogoImage = 0;
    this.remainingAdImage = 0;
    //this.FormPostAd.controls['categoryId'].disable(true);
    this.selected("Free");
    this.getAdDetailsById(this._route.snapshot.queryParams['buysellId']);

    debugger;
    if (localStorage.getItem('roleName') == "Registered User")
      this.FetchUserInfo(this.loggedInUserId);

  }


  get f() { return this.FormPostAd.controls; }


  // Insert Post
  postAd(): void {
    debugger;
    this.submitted = true;
    if (this.saveButtonText == "Save") {
      this.action = "";
    }
    // if (this.saveButtonText == "Update") {
    //   this.action = "edit";
    // }
    // stop the process here if form is invalid

    if (this.FormPostAd.invalid) {
      this.toastr.warning("Mandatory fields missing. Please fill all mandatory fields.");
      return;
    }
    var x = document.getElementsByClassName("ng-value-label");
    this.StateName = x[2].innerHTML;
    this.CityName = x[3].innerHTML;
    this.FormPostAd.value.stateName = this.StateName;
    this.FormPostAd.value.cityName = this.CityName;
    console.log(this.FormPostAd.value)
    this.FormPostAd.value.createdBy = this.loggedInUserId;
    this.FormPostAd.value.isPaidAd = this.isPaidAd;
    if (this.isPaidAd == false) {
      this.FormPostAd.value.imageUrl = this.logoresponse;
    }
    else {
      
      this.FormPostAd.value.imageUrl = this.imageresponse;
    }

    if(this.FormPostAd.value.subCategoryId==1)
    this.FormPostAd.value.subCategoryName="Buy";
    else
    this.FormPostAd.value.subCategoryName="Sell";


    if (this.action == "") {


      // if ((this.FormPostAd.value.adLogoUrl != null) && (this.remainingLogoImage - 1 < 0)) {
      //   this.toastr.warning("Logo subscription utilised.Please choose another plan");
      //   return;
      // }
      // else if ((this.FormPostAd.value.adLogoUrl != null) || (this.FormPostAd.value.adLogoUrl != undefined)) {
      //   this.remainingLogoImage = this.remainingLogoImage - 1;
      // }


      if ((this.FormPostAd.value.imageUrl != null) || (this.FormPostAd.value.imageUrl != undefined)) {
        var splitsNewImage = new String(this.FormPostAd.value.imageUrl);
        var afterSplit = splitsNewImage.split(",");
        var count = afterSplit.length;

      if (this.isPaidAd==true) {
        if (this.remainingbuysellCount - 1 < 0) {
          this.toastr.warning("Please buy a plan for buy/sell posting or select another plan.");
          return;
        }
        else {
          this.remainingbuysellCount = this.remainingbuysellCount - 1;
        }
      }
    }
      // this.FormPostAd.value.logoCount = this.remainingLogoImage;
      // this.FormPostAd.value.adImageCount = this.remainingAdImage;
      this.FormPostAd.value.planId = this.tempPlanId;
      this.FormPostAd.value.BuySellCount = this.remainingbuysellCount;



      this.FormPostAd.value.buySellId = 0;
      this._buySellService.postBuySell(this.FormPostAd.value)
        .subscribe((response) => {
          console.log(response);
          if (response.status === 200) {
            if ((<any>response)._body === "true") {
              this.toastr.success('Buy/Sell listing posted successfully. It will be available on website after the admin approves it.');
                this._router.navigate(['/bumanagebuysell']);              
            }           
          }

        },
          (error) => {
            console.log(error);
            this.toastr.error('Problem with service. Please try again later!');
          }
        );
    }
    else if (this.action == "edit") {
      debugger;
      if(this.FormPostAd.value.subCategoryId==1)
      this.FormPostAd.value.subCategoryName="Buy";
      else
      this.FormPostAd.value.subCategoryName="Sell";
    
     if(this.isPaidAd==true)
     {
      if (this.extImages.length > 0) {
        if ((this.imageresponse != null) || (this.imageresponse != undefined)) {
          this.FormPostAd.value.imageUrl = this.imageresponse;
          var splitsEditImage = new String(this.FormPostAd.value.imageUrl);
          var afterEditSplit = splitsEditImage.split(",");
          var count = afterEditSplit.length;
          var finalcount=this.extImages.length + afterEditSplit.length;
          if(finalcount >5)
          {
            this.toastr.warning('Only 5 image  allowed to upload');
            return;
          }
        }

        if (this.imageresponse === undefined)
          this.FormPostAd.value.imageUrl = this.extImages.join();
        else {
          this.FormPostAd.value.imageUrl = this.imageresponse + "," + this.extImages.join();
        }

      }
      else {
        if ((this.imageresponse != null) || (this.imageresponse != undefined)) {
          this.FormPostAd.value.imageUrl = this.imageresponse;
          var splitsEditImage = new String(this.FormPostAd.value.imageUrl);
          var afterEditSplit = splitsEditImage.split(",");
          var count = afterEditSplit.length;
        }
        else if ((this.imageresponse == null) || (this.imageresponse == undefined)) {
          this.FormPostAd.value.imageUrl = "";
        }
      }
    }
    else
    {
      if ((this.logoresponse != null) || (this.logoresponse != undefined)) {
        this.FormPostAd.value.imageUrl = this.logoresponse;
      }
        else
        {
          this.FormPostAd.value.imageUrl = this.FormPostAd.value.adLogoUrl;
        }      
    }
      this.FormPostAd.value.planId = this.tempPlanId;
      this.FormPostAd.value.logoCount = this.remainingLogoImage;
      this.FormPostAd.value.adImageCount = this.remainingAdImage;

      this._buySellService.updatebuySellRecord(this.FormPostAd.value)
        .pipe(first())
        .subscribe(
          response => {
            console.log(response);
            if (response.status === 200) {
              if ((<any>response)._body === "true") {
                  this.toastr.success('Buy/Sell listing updated successfully.');
                  this._router.navigate(['/bumanagebuysell']);
                }            
            }

          },
          (error) => {
            console.log(error);
            this.toastr.error('Problem with service. Please try again later!');
          }
        );
    }

  }

  // Get User Details on Posted For selection

  subscriptionPlanDetails(event: any) {
    debugger;
    console.log(event.target.value);

    this.paymentPlanService.getPlanDetails(event.target.value).subscribe(data => {
      var planIdDetails = JSON.parse(data._body);
      console.log(planIdDetails);
      this.previousPlan = planIdDetails;
      this.tempPlanId = planIdDetails.planId;
      this.remainingLogoImage = planIdDetails.logoCount;
      this.remainingAdImage = planIdDetails.adImageCount;
      this.remainingbuysellCount = planIdDetails.buySellCount;
      if (this.remainingbuysellCount == 0) {
        this.toastr.info("Plan subscription exhausted for this plan.");
      }
      this.FormPostAd.patchValue({
        logoCount: planIdDetails.logoCount,
        adImageCount: planIdDetails.adImageCount,
        BuySellCount: planIdDetails.buySellCount
      });
    });
  }



  GetUserDetails(event: any) {
    var userId = event.target.value;
    this.FetchUserInfo(userId);
  }

  navigatetocontactus()
  {
    this._router.navigate(['/contact']);
  }
  FetchUserInfo(UserId: any) {
    debugger;

    this._userService.getUserById(UserId).subscribe(data => {
      var userdata = JSON.parse(data._body);
     this._allState = this._commonService.StateDDL(userdata.countryId);
      this._allCity = this._commonService.BUCityDDL(userdata.stateId);

      this._commonService.BUCityDDL(userdata.stateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });

      this.FillSubscriptionPlan(userdata.userId);
      ////debugger;
      if (this.action == "") {
        this.FormPostAd.patchValue({
          email: userdata.email,
          faxNumber: userdata.faxNumber,
          addressStreet1: userdata.addressStreet1,
          addressStreet2: userdata.addressStreet2,
          countryId: userdata.countryId,
          stateId: userdata.stateId.toString(),
          cityId: userdata.cityId.toString(),
          zipCode: userdata.zipCode,
          postedFor: userdata.userId,
          contactPersonName: userdata.firstName + " " + userdata.lastName,
          contactNumber: userdata.contactNumber,
          alternateContactNumber: userdata.alternateContactNumber,
          buySellId: "",
          title: "",
          stateName: "",
          cityName: "",
          expectedPrice: "",
          postedName: "",
          categoryId: this.FormPostAd.value.categoryId,
          categoryName: "",
          subCategoryId: "",
          subCategoryName: "",
          countryCodeContact: "1",
          description: "",
           validTillDate: "",
          adLogoUrl: "",
          adImageUrl: "",
          adextLogoUrl: "",
          adextImageUrl: "",
          createdBy: "",
          isPaidAd: "",
          logoCount: 0,
          adImageCount: 0,
          OtherCity: ""
        });
      }

      else if (this.action == "edit") {

        this.FormPostAd.patchValue({
          email: data.email,
          faxNumber: data.faxNumber,
          addressStreet1: data.addressStreet1,
          addressStreet2: data.addressStreet2,
          countryId: data.countryId.toString(),
          stateId: data.stateId.toString(),
          cityId: data.cityId.toString(),
          zipCode: data.zipCode,
          postedFor: data.userId,
          contactPersonName: data.firstName + " " + data.lastName,
          contactNumber: data.contactNumber,
          alternateContactNumber: data.alternateContactNumber,
          buySellId: this.FormPostAd.value.buySellId,
          title: this.FormPostAd.value.title,
          stateName: this.FormPostAd.value.stateName,
          cityName: this.FormPostAd.value.cityName,
          expectedPrice: this.FormPostAd.value.expectedPrice,
          postedName: this.FormPostAd.value.postedName,
          categoryId: this.FormPostAd.value.categoryId,
          categoryName: this.FormPostAd.value.categoryName,
          subCategoryId: this.FormPostAd.value.subCategoryId,
          subCategoryName: this.FormPostAd.value.subCategoryName,
          countryCodeContact: "1",
          description: this.FormPostAd.value.description,
          validTillDate: this.FormPostAd.value.validTillDate,
          adLogoUrl: this.FormPostAd.value.adLogoUrl,
          adImageUrl: this.FormPostAd.value.adImageUrl,
          adextLogoUrl: this.FormPostAd.value.adextLogoUrl,
          adextImageUrl: this.FormPostAd.value.adextImageUrl,
          createdBy: this.FormPostAd.value.createdBy,
          isPaidAd: this.isPaidAd,
          logoCount: 0,
          adImageCount: 0,
          OtherCity: this.FormPostAd.value.OtherCity
        });


      }
    });

    
  }

  renderExistingImages(imageUrls, buySellId, uploadType) {
    debugger;
    if (imageUrls !== null) {
      if (imageUrls !== "") {
        var extImages = imageUrls.split(',');
        if (extImages.length > 0) {
          if (uploadType == "Free") {
            for (let i = 0; i < extImages.length; i++) {
              this.extLogoUrls.push(this.commonimagepath + 'BuySell/' + buySellId + '/Free/' + extImages[i]);
              this.extImages.push(extImages[i]);
              this.displayExtLogo = 'col-md-6';
              this.displayBrowseforFree=false;
            }
          }
          else if (uploadType == "Premium") {
            for (let i = 0; i < extImages.length; i++) { ////debugger;
              this.extImageurls.push(this.commonimagepath + 'BuySell/' + buySellId + '/Premium/' + extImages[i]);
              this.extImages.push(extImages[i]);
              this.displayExtImages = 'col-md-6';
            }
          }
          // this.imagepath+'Ads/'+adId+'/Logo/'+picurl;
        }
      }
      else {
        if (uploadType == "Premium")
          this.displayExtImages = 'col-md-6 d-none';
        if (uploadType == "Free")
          this.displayExtLogo = 'col-md-6 d-none';

      }

    }
    else {
      if (uploadType == "Premium")
        this.displayExtImages = 'col-md-6 d-none';
      if (uploadType == "Free")
        this.displayExtLogo = 'col-md-6 d-none';

    }
  }

  onextLogoUrldelete(i) {
    this.extLogoUrls.splice(i, 1);
    this.extLogos.pop();
    this.displayExtUploads= 'col-md-6 d-none';  
    this.displayBrowseforFree=true;
  }
  onextextImageurldelete(i) {
    this.extImageurls.splice(i, 1);
    this.extImages.splice(i, 1);
    console.log(this.extImageurls);
    console.log(this.extImages);
  }

  getAdDetailsById(buySellId) {
    debugger;
    this._buySellService.getbuySellDetailsById(buySellId).subscribe(data => {
      this.action = "edit";
      this.DefaultCategoryId= environment.defaultbuysellCategoryId;
      this.saveButtonText = "Update";
      this.titleText = "Edit Buy/Sell Listing";
      this.displayText = 'btn btn-sm btn-primary d-none';
      debugger;
    //  Ad Images Retrieval
      this.displayExtUploads = 'form-bg-section';
      if(data.isPaidAd==true)
      {
      if (data.imageUrl !== null) {
        this.selected("Paid");
        this.adTypePaid = true;
        this.adTypeFree = false;
        this.RadioPaid = false;
        this.RadioFree = true;
        this.renderExistingImages(data.imageUrl, data.buySellId, "Premium");
      }

      else {
        this.displayExtImages = 'col-md-6 d-none';
        this.adTypePaid = false;
        this.adTypeFree = true;
        this.RadioPaid = true;
        this.RadioFree = false;
      }
    }
    else
    {
    // Ad Logo Retrieval
    if (data.imageUrl !== null) {
      this.selected("Free");
      this.adTypePaid = false;
      this.adTypeFree = true;
      this.RadioPaid = true;
      this.RadioFree = false;
      data.adLogoUrl=data.imageUrl;
      this.renderExistingImages(data.adLogoUrl, data.buySellId, "Free");
    }

    else {
      this.displayExtLogo = 'col-md-6 d-none';
      this.adTypePaid = false;
      this.adTypeFree = true;
      this.RadioPaid = true;
      this.RadioFree = false;
    }
    }
  

      if (data.imageUrl == null && data.adLogoUrl == null) {
        this.RadioPaid = true;
        this.RadioFree = false;
        this.displayExtUploads = 'form-bg-section d-none';
      }
      else if (data.imageUrl == "" && data.adLogoUrl == "") {
        this.RadioPaid = true;
        this.RadioFree = false;
        this.displayExtUploads = 'form-bg-section d-none';
     }
      this._commonService.CountryDDL().subscribe(data => {
        console.log(data);
        //   //debugger;
        this.countries = data;
      });

      this._commonService.StateDDL(data.countryId).subscribe(data => {
        debugger
        console.log(data);
        this.states = data;

      });

      this._commonService.BUCityDDL(data.stateId).subscribe(data => {
        debugger;
        console.log(data);
        this.cities = data;
      });


      this._commonService.CategoryDDL().subscribe(data => {
        debugger;
        this.categories = data;
      });
      

      // Show other city
      if (data.cityId === 9999) {
        this.showOtherCity = "col-md-4";
      }
      else
        this.showOtherCity = "d-none";


      this._allState = this._commonService.StateDDL(data.countryId);
      this._allCity = this._commonService.BUCityDDL(data.stateId);
      this._allSubCategory = this._commonService.SubCategoryDDL(data.categoryId);
      this._allCategory = this._commonService.CategoryDDL();
      this.FillSubscriptionPlan(data.postedFor);
      this.countrycode = this.countryCodesNew;

      console.log("AdObject");
      console.log(data);
      this.gettoday();
      if (data.validTillDate !== null)
        this.validTill = data.validTillDate.split('T')[0];
      else
        this.validTill = this.minDate;
      if (data.countryCodeContact === null)
        data.countryCodeContact = "1";
      this.FormPostAd.patchValue({
        buySellId: data.buySellId,
        title: data.title,
        email: data.email,
        postedFor: data.postedFor,
        countryId: data.countryId.toString(),
        stateId: data.stateId.toString(),
        cityId: data.cityId.toString(),
        categoryId: data.categoryId.toString(),
        subCategoryId: data.subCategoryId,
        faxNumber: data.faxNumber,
        addressStreet1: data.addressStreet1,
        addressStreet2: data.addressStreet2,
        stateName: data.stateName,
        cityName: data.cityName,
        zipCode: data.zipCode,
        expectedPrice: data.expectedPrice,
        postedName: data.postedForName,
        categoryName: data.categoryName,
        subCategoryName: data.subCategoryName,
        contactPersonName: data.contactPersonName,
        contactNumber: data.contactNumber,
        countryCodeContact: data.countryCodeContact,
        description: data.description,
        alternateContactNumber: data.alternateContactNumber,
        validTillDate: this.validTill,
        adLogoUrl: data.imageUrl,
        adImageUrl: data.imageUrl,
        adextLogoUrl: "",
        adextImageUrl: "",
        createdBy: data.createdBy,
        isPaidAd: data.isPaidAd,
        logoCount: 0,
        BuySellCount: 0,
        OtherCity: data.otherCity
      });

    });
    //this.action = "edit";
  }

  deleteAd(buySellId: number) {
    if (confirm('Are you sure to delete this buy/sell listing?')) {
      this._buySellService.deleteAd(buySellId).subscribe(response => { console.log(response); },
        (error) => {
          console.log(error);

          this.statusMessage = "Problem with service. Please try again later!";
        });
    }
  }

  FillJobCategoryDDL() { this._allCategory = this._commonService.JobCategoryDDL(); }
  // 

  selected(listingType: string) {
    debugger;
    if (listingType == "Free") {
      this.isPaidAd = false;
      this.displayFreeSection='col-md-12';
      this.displayPaidSection = 'col-md-12 d-none';
    }
    if (listingType == "Paid") {
      this.isPaidAd = true;
      this.displayFreeSection='col-md-12 d-none';
      this.displayPaidSection = 'col-md-12';
    }
  }


  //  Dropdowns


  FillCountryDDL() {
   this._allCountry = this._commonService.CountryDDL();
    //this._allCountry = this._commonService.CountryDDL();
    this.CountryId = "1";
    //this._allState = this._commonService.StateDDL(this.CountryId);
    //this.StateId = "22";
    //this.StateName=event.target.options[this.StateId].innerHTML;
   // this._allCity = this._commonService.BUCityDDL(this.StateId);
   

    this._commonService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      this.states = data;
    });
    this._allState = this._commonService.StateDDL(this.CountryId);   
    this._allCity = this._commonService.BUCityDDL(this.StateId);
  }

  FillCategoryDDL() {
    debugger;
    this._allCategory = this._commonService.CategoryDDL();

    this._commonService.CategoryDDL().subscribe(data => {
      debugger;
      this.categories = data;


    });

  }
  FillSubCategoryDDL(event: any) {
    debugger;
    this.CategoryId = event.categoryId;
    this.CategoryName = event.categoryName;
    this._allSubCategory = this._commonService.SubCategoryDDL(this.CategoryId);
    this._commonService.SubCategoryDDL(this.CategoryId).subscribe(data => {
      debugger;
      //this.subcategories = data;
      this.subcategories=[  
        { subCategoryId: 1, subCategoryName: "Buy" },
        { subCategoryId: 2, subCategoryName: "Sell" },
     
        ] 

    });
    this.FormPostAd.categoryName = this.CategoryName;


  }


  FillSubCategoryName(event: any) {
    debugger;
    this.SubCategoryId = event.subCategoryId;
    this.SubCategoryName = event.subCategoryName;
    this.FormPostAd.subCategoryName = this.SubCategoryName;


  }



  FillUserDDL() {
    //////debugger;
    this._allUser = this._commonService.UserDDL();
  }

  FillStateDDL(event: any) {

    debugger;
    if ((event.target.value == "0") || ((event.target.value == ""))) {
      this.FormPostAd.value.countryId = 0;
      //this._allState = this._commonService.StateDDL("0");
      var x = document.getElementsByClassName("ng-value-label");
      x[2].innerHTML = "--Select State--";
      x[3].innerHTML = "--Select City--";
      //var i;
      //for (i = 3; i < x.length; i++) {
      //  x[i].innerHTML = "";
      //}
        this.states = [];
      this.cities = [];
     
      console.log(this.states);
      this.FormPostAd.value.stateId = 0;     
      this.FormPostAd.value.cityId = 0;
    }
    else {

     
      this.CountryId = event.countryId;

      this._allState = this._commonService.StateDDL("1");
      debugger;
      this._commonService.StateDDL("1").subscribe(data => {
        console.log(data);
        //debugger;
       
        var x = document.getElementsByClassName("ng-value-label");
        x[2].innerHTML = "--Select State--";
        x[3].innerHTML = "--Select City--";
        this.states = data;
      });

      //this._commonService.BUCityDDL("1").subscribe(data => {
      //  console.log(data);
      //  debugger;
      //  this.cities = data;
      //});
     // this._allState = this._commonService.StateDDL(this.CountryId);
      this.CountryName = event.target.options[this.CountryId].innerHTML;
      this.FormPostAd.countryName = this.CountryName;
      
    }
      
  }

  FillCityDDL(event: any)
  {
    debugger;
    var x = document.getElementsByClassName("ng-value-label");
    x[3].innerHTML = "--Select City--";
    if (event!=0) {
      this.StateId = event.stateId;
      this.StateName = event.stateName;
      this._allCity = this._commonService.BUCityDDL(this.StateId);
      //this.StateName = event.target.options[this.StateId].innerHTML;  
      this.FormPostAd.stateName = this.StateName;
      
      this._commonService.BUCityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
    }
    else {
      var x = document.getElementsByClassName("ng-value-label");
      this.cities = [];
    }
    
  }

  FillCityName(event: any) {
    debugger;
    this.CityId = event.cityId;
    this.CityName = event.cityName;

    if (event.cityId === "9999") {
      this.showOtherCity = "col-md-4";
    }
    else
      this.showOtherCity = "d-none";
    //this.CityName = event.target.options[this.CityId].innerHTML;
    //this.FormPostAd.cityName = this.CityName;

  }
  
  NavigatetoManageAd() {
      this._router.navigate(['/bumanagebuysell']);
  }

  FillSubscriptionPlan(userId: number) {
    this._allPlan = this.paymentPlanService.UserPlanDDL(userId);
  }

  reset() {
    this.ngOnInit();
    this.UploadCmpt.onimagedelete(0);
    this.MultiUploadCmpt.onimagedelete(0);
  }

  refresh(): void {
    debugger;
    window.location.reload();
    this.UploadCmpt.ngOnInit();
    this.MultiUploadCmpt.ngOnInit();

  }

 
  subcategories=[  
    { subCategoryId: 1, subCategoryName: "Buy" },
    { subCategoryId: 2, subCategoryName: "Sell" },
 
    ] 
}
