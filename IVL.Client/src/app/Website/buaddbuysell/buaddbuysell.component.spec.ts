import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuaddbuysellComponent } from './buaddbuysell.component';

describe('BuaddbuysellComponent', () => {
  let component: BuaddbuysellComponent;
  let fixture: ComponentFixture<BuaddbuysellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuaddbuysellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuaddbuysellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
