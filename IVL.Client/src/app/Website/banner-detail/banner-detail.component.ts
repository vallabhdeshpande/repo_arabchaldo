import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Managebanner } from '../../shared/managebanner';
import { WebsiteService } from '../../shared/website.service';
import { User} from '../../shared/user.model';
import {UserService} from '../../shared/user.service';
import { environment } from '../../../environments/environment';
import { Input, ElementRef } from '@angular/core';
import { } from 'googlemaps';

@Component({
  selector: 'app-banner-detail',
  templateUrl: './banner-detail.component.html',
  styleUrls: ['./banner-detail.component.scss']
})
export class BannerDetailComponent implements OnInit {

  bannerpost = new Managebanner();;
  userdetail = new User();;
  userId: bigint;
  ImageUrl: string;
  Imageurls = [];
  logourls = [];
  addressforMap: string = "";
  bannerConfig: string = "";
  imagepath = environment.imagepath;
  showAddress: boolean = true;
  showMap: boolean = true;
  mailto: string = "";
  PremiumtagAd = false;
  FeaturedtagAd = false;
  map: google.maps.Map;
  description: string = "";

  constructor(private WebsiteService: WebsiteService, private UserService: UserService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    debugger
    this.getBannerDetailsById(this.route.snapshot.queryParams['bannerId']);

    //this.getAdDetailsById(this.route.snapshot.paramMap.get("adId"));
  }

  getBannerDetailsById(bannerId) {
    debugger
    this.WebsiteService.getBannerById(bannerId).subscribe(bannerpost => {
      //var bannerpost = JSON.parse(bannerpost._body);
      console.log(bannerpost);

      var featured = bannerpost.isFeatured;
      if (featured === true){
        this.PremiumtagAd = true;
      }
      else{
        this.FeaturedtagAd = true;
      }

      this.bannerpost.title = bannerpost.title;
      this.bannerpost.description = bannerpost.description;
      this.description = this.bannerpost.description;
      //this.bannerpost.createdDate=bannerpost.createdDate;
      this.bannerpost.createdDate = bannerpost.createdDate.split('T')[0];
      this.bannerpost.postedForName = bannerpost.postedForName;
      this.bannerpost.postedFor = bannerpost.postedFor;
      this.bannerpost.imageUrl = bannerpost.imageUrl;
      this.bannerpost.website = bannerpost.website;
      this.bannerpost.visitorCount = bannerpost.visitorCount;
      this.ImageUrl = this.imagepath + 'Banners/' + bannerId + '/Image/' + bannerpost.imageUrl;
      this.Imageurls.push(this.ImageUrl);
      // this.ImageUrl=this.imagepath+'Banners/'+bannerId+'/Image/'+ bannerpost.imageUrl;
      // this.Imageurls.push(this.ImageUrl.toString()); 
      console.log(this.bannerpost.postedFor);
      this.addressforMap = bannerpost.addressStreet1 + "," + bannerpost.addressStreet2 + "," + bannerpost.zipCode + "," + bannerpost.cityName + "," + bannerpost.stateName + "," + bannerpost.countryName;
      //this.getuserContactDetailsById(this.bannerpost.postedFor);
      
      //AddressStreet1
      if (bannerpost.addressStreet1 == "" || bannerpost.addressStreet1 == undefined) {
        this.bannerpost.AddressStreet1 = "";
      }
      else
        this.bannerpost.AddressStreet1 = bannerpost.addressStreet1 + ",";
      //AddressStreet2
      if (bannerpost.addressStreet2 == "" || bannerpost.addressStreet2 == undefined) {
        this.bannerpost.AddressStreet2 = "";
      }
      else
        this.bannerpost.AddressStreet2 = bannerpost.addressStreet2 + ",";
      //CityName
      if (bannerpost.cityName == "" || bannerpost.cityName == undefined) {
        this.bannerpost.CityName = "";
      }
      else
        this.bannerpost.CityName = bannerpost.cityName + ",";
      //StateName
      if (bannerpost.stateName == "" || bannerpost.stateName == undefined) {
        this.bannerpost.StateName = "";
      }
      else
        this.bannerpost.StateName = bannerpost.stateName + ",";
      //countryName
      if (bannerpost.countryName == "" || bannerpost.countryName == undefined) {
        this.bannerpost.CountryName = "";
      }
      else
        this.bannerpost.CountryName = bannerpost.countryName + ".";

      //zipCode
      if (bannerpost.ZipCode == "" || bannerpost.zipCode == undefined) {
        this.bannerpost.ZipCode = "";
      }
      else
        this.bannerpost.ZipCode = bannerpost.zipCode + ".";


      if (bannerpost.addressStreet1 === "" && bannerpost.addressStreet2 === "" && (bannerpost.zipCode === "" || bannerpost.zipCode === null) && (bannerpost.cityName === "" || bannerpost.cityName === null) && (bannerpost.stateName === "" || bannerpost.stateName === null)) {
        this.showAddress = false;
        this.showMap = true;
        this.displayGoogleMap("");
      }
      else {
        this.showAddress = true;
        this.showMap = true;
        this.displayGoogleMap(this.addressforMap);
      }

      this.bannerpost.ContactNumber = bannerpost.contactNumber;
      this.bannerpost.AlternateContactNumber = bannerpost.alternateContactNumber;
      this.bannerpost.CountryId = bannerpost.countryId;
      this.bannerpost.CityId = bannerpost.cityId;
      this.bannerpost.Email = bannerpost.email;
      this.mailto = "mailto:" + bannerpost.email;
      this.bannerpost.FaxNumber = bannerpost.faxNumber;
      this.bannerpost.ContactPersonName = bannerpost.contactPersonName;

      this.bannerpost.StateId = bannerpost.stateId;
      
      var bannerlen = bannerpost.length;
      console.log(bannerpost);
    });
  }

  getuserContactDetailsById(userId) {
    debugger
    this.UserService.getUserContactdetailsById(userId).subscribe(userdetail => {
      console.log(userdetail);
      var userdetail = JSON.parse(userdetail._body);
      this.userdetail.AddressStreet1 = userdetail.addressStreet1;
      this.userdetail.AddressStreet2 = userdetail.addressStreet2;
      this.userdetail.AlternateContactNumber = userdetail.alternateContactNumber;
      this.userdetail.CityId = userdetail.cityId;
      this.userdetail.cityName = userdetail.cityName;
      this.userdetail.ContactNumber = userdetail.contactNumber;
      this.userdetail.CountryId = userdetail.countryId;
      this.userdetail.Email = userdetail.email;
      this.userdetail.FaxNumber = userdetail.faxNumber;
      this.userdetail.FirstName = userdetail.firstName;
      this.userdetail.LastName = userdetail.lastName;
      this.userdetail.StateId = userdetail.stateId;
      this.userdetail.ZipCode = userdetail.zipCode;
    });
  }

  readmore: boolean = false;

  readMore() {
    this.readmore = !this.readmore;
  }

  displayGoogleMap(address: string) {

    var geocoder = new google.maps.Geocoder();
    if (address == "") {
      var latitude = 42.331429;
      var longitude = -83.045753;

      const mapProperties =
      {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        marker: new google.maps.Marker({ position: { lat: latitude, lng: longitude }, animation: google.maps.Animation.BOUNCE })
      };
      this.map = new google.maps.Map(document.getElementById('map'), mapProperties);
      mapProperties.marker.setMap(this.map);
    }

    else {


      geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          latitude = results[0].geometry.location.lat();
          longitude = results[0].geometry.location.lng();
          //alert(latitude);

        }
        //debugger;
        const mapProperties =
        {
          center: new google.maps.LatLng(latitude, longitude),
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          marker: new google.maps.Marker({ position: { lat: latitude, lng: longitude }, animation: google.maps.Animation.BOUNCE })
        };
        this.map = new google.maps.Map(document.getElementById('map'), mapProperties);
        mapProperties.marker.setMap(this.map);

      });
    }
  }

  btnPostNew()
  {
    debugger;
    console.log(localStorage.getItem('roleName'));
    if (localStorage.getItem('token') === null) {
      this.router.navigate(['/login']);
    }
    else {
      console.log(localStorage.getItem);
      //this.userId = localStorage.getItem('userId');
      if ((localStorage.getItem('roleName') === 'SuperAdmin') || (localStorage.getItem('roleName') === 'Admin')) {
        this.router.navigate(['/theme/adbanner']);
      }
      else {
        this.router.navigate(['/bupostbanner']);
      }
    }
  }

}
