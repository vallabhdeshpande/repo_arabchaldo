import { Component, OnInit,Input } from '@angular/core';
import { ViewChild } from '@angular/core';
import {} from 'googlemaps';
@Component({
  selector: 'app-gmap',
  templateUrl: './gmap.component.html',
  styleUrls: ['./gmap.component.scss']
})
export class GmapComponent implements OnInit {
  @Input() addressforMap: string;
  @ViewChild('map', {static: true}) mapElement: any;
map: google.maps.Map;
  constructor() { }

  ngOnInit() {
    this.renderAddressMap(this.addressforMap);
  }




renderAddressMap(address:string){
//   var geocoder = new google.maps.Geocoder();
// var address = address;
  //Blue Ridge Pune
  //var latitude = 18.5758;
  //var longitude = 73.7404;
  // Detroit Michgan
  var latitude = 42.331429;
  var longitude = -83.045753;
//debugger;
// geocoder.geocode( { 'address': address}, function(results, status) {

//   if (status == google.maps.GeocoderStatus.OK) {
//      latitude = results[0].geometry.location.lat();
//      longitude = results[0].geometry.location.lng();
//     alert(latitude);
//   } 
// });
  const mapProperties = {
    center: new google.maps.LatLng(latitude, longitude),
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    marker: new google.maps.Marker({ position: { lat: latitude, lng: longitude},animation:google.maps.Animation.BOUNCE})
   };
   this.map = new google.maps.Map(this.mapElement.nativeElement,    mapProperties);
   mapProperties.marker.setMap(this.map); 
  }
}
