import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BumanagebannerComponent } from './bumanagebanner.component';

describe('BumanagebannerComponent', () => {
  let component: BumanagebannerComponent;
  let fixture: ComponentFixture<BumanagebannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BumanagebannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BumanagebannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
