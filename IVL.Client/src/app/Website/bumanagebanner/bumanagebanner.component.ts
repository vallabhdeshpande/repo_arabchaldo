import { BubannerdetailsviewComponent } from './../bubannerdetailsview/bubannerdetailsview.component';
import { BannerDetailComponent } from './../banner-detail/banner-detail.component';
import { Response } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { Managebannerservice } from '../../views/theme/managebannerservice';
import {Managebanner} from '../../views/theme/managebanner';
import { Observable } from 'rxjs/Observable'; 
import { User } from '../../views/theme/user'; 
import {FormBuilder,Validators} from '@angular/forms';
import {Router} from "@angular/router";
//import {AdbannerComponent} from '../adbanner/adbanner.component';
import { OrderPipe } from 'ngx-order-pipe';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr'; 
//import { BannerDetailComponent} from '../../views/theme/bannerdetailsview'
//import { BubannerdetailsviewComponent} from '../bubannerdetailsview/bubannerdetailsview.component';

@Component({
  selector: 'app-bumanagebanner',
  templateUrl: './bumanagebanner.component.html',
  styleUrls: ['./bumanagebanner.component.scss']
})
export class BumanagebannerComponent implements OnInit {
  private _allUser: Observable<User[]>;
  UserId: string = "0";
  managebanners: Managebanner[] = [];
  statusMessage: string;
  loggedInUserId: string;
  managebanner = new Managebanner();
  order: string;
  setStatus: string = "";
  reverse: boolean;
  users: User[];
  user = new User();;
  public pageSize: number = 10;
  public p: number;
  bannderId: bigint;
  public response: { 'dbPath': '' };
  FormUserAdd: any;
  remark: string = "";
  workflowstatusId: number;
  action: string = "";
  sortedCollection: any[];
  id: number;
  workflowdetails: string;
  deleteremark: string;
  imagepath = environment.imagepath;
  activity: boolean;
  visibilitybutton: string;
  visibilityspan: string;
  j: any;
  localmanagebanners: Array<any>;
  actionmessage: string;
  filter: string = "";
  setType: string;
  constructor(private toastr: ToastrService, private orderPipe: OrderPipe, private managebannerservice: Managebannerservice, private formbulider: FormBuilder, private router: Router) {
    this.sortedCollection = orderPipe.transform(this.users, 'firstName');
    console.log(this.sortedCollection);
  }

  ngOnInit() {
    this.loggedInUserId = localStorage.getItem('userId');
    this.getBannerList();
  }

  gotoBannerDetails(bannerPost: Managebanner): void {
    debugger;
    this.router.navigate(['/bubannerdetailsview'], { queryParams: { id: bannerPost.bannerId } });
  };

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }

  getType(isFeatured) {
    //debugger;
    if (isFeatured === "Premium")
      return this.setStatus = "";
    else
      return this.setStatus = " ";
  }

  getStatus(Status: string) {
    //debugger;
    if (Status == "Active" || Status == "True")
      return this.setStatus = "badge badge-success";

    if (Status == "InActive" || Status == "False")
      return this.setStatus = "badge badge-danger";

    if (Status == "Pending")
      return this.setStatus = "badge badge-warning";

  }

  getBannerList(): void {
    debugger
    this.managebannerservice.FillBannerList(localStorage.userId).subscribe(managebanners => {

      if (managebanners != null) {
        //managebanners = managebanners.json();
        console.log(this.managebanners);

        // alert(this.managebanners);
        var adlenth = managebanners.length;
        debugger
        for (let i = 0; i < adlenth; i++) {
          if (managebanners[i].isVisible === true || managebanners[i].isVisible === "True") {
            managebanners[i].isVisible = "Active";
            managebanners[i].Activity = true;
            this.visibilitybutton = "hide";
            this.visibilityspan = "show";
          }
          else if (managebanners[i].isVisible === false || managebanners[i].isVisible === "False") {
            managebanners[i].isVisible = "InActive";
            managebanners[i].Activity = true;
            this.visibilitybutton = "hide";
            this.visibilityspan = "show";
          }
          else if (managebanners[i].isVisible === null || managebanners[i].isVisible === "") {
            managebanners[i].isVisible = "Pending";
            managebanners[i].Activity = false;
            this.visibilitybutton = "show";
            this.visibilityspan = "hide";
          }
          if (managebanners[i].isFeatured === true || managebanners[i].isFeatured === "Premium" || managebanners[i].isFeatured === "True") {
            managebanners[i].isFeatured = "Premium";
            // this.setType = "badge badge-warning";
          }
          else {
            managebanners[i].isFeatured = "Featured";
            this.setType = "";
          }
        }
        this.managebanners = managebanners;
      }
      else {
         this.managebanners = [];
      }

      
    });
  }

  getPicUrl(picurl: string, bannerId) {
    if (picurl == null)
      return "assets/img/default/no_image_placeholder.jpg";
    else if (picurl == "")
      return "assets/img/default/no_image_placeholder.jpg";
    else
      return this.imagepath + 'Banners/' + bannerId + '/Image/' + picurl;
  }

  onDelete(BannerId: bigint, event) {
    this.bannderId = BannerId;
    this.actionmessage = "Are you sure to delete this Banner?"
  }

  ActionDelete() {
    debugger;
    this.action = "Delete";
    this.deleteremark = "";
    this.workflowdetails = this.bannderId + "," + this.loggedInUserId + "," + this.action + "," + this.deleteremark;
    this.managebannerservice.updatewithWorkflow(this.workflowdetails).
      subscribe(response => {
        console.log(response);
        //this.getBannerList();

        if (response.status === 200) {
          console.log(response);
          if ((<any>response)._body === "true") {
            this.toastr.success('Banner successfully deleted.');
            this.getBannerList();
          }
            
          else
            this.toastr.info('Banner is active on website.');
          debugger

        }
        else {
          this.toastr.warning('Problem with service. Please try again later!');
        }

      },

        (error) => {
          console.log(error);

          this.toastr.error('Problem with service. Please try again later!');
        });
  }

  ActionOnApprove(BannerId, event: any) {
    this.bannderId = BannerId;
    this.action = "Approve"
    this.remark = " "
  }

  ActionOnReject(BannerId, event: any) {
    this.bannderId = BannerId;
    this.action = "Reject"
    this.remark = " "
  }

  ActionOnPost(remark) {
    debugger;
    this.workflowdetails = this.bannderId + "," + remark + "," + this.action;
    //if (confirm('Are you sure to perform this Action?')) 
    //{
    //  this.managebannerservice.updatewithWorkflow(this.workflowdetails).subscribe(response => {
    //    console.log(response);
    //    console.log('Updated');
    //    this.getBannerList();
    //  },

    //    (error) => {
    //      console.log(error);

    //      this.statusMessage = "Problem with service. Please try again later!";
    //    });
    //}       
  }

  GetPostedFor(event: any) {
    this.managebanner.PostedFor = event.target.value;
  }

}
