import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuadblogComponent } from './buadblog.component';

describe('BuadblogComponent', () => {
  let component: BuadblogComponent;
  let fixture: ComponentFixture<BuadblogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuadblogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuadblogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
