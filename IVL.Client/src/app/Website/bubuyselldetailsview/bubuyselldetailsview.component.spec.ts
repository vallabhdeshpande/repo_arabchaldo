import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BubuyselldetailsviewComponent } from './bubuyselldetailsview.component';

describe('BubuyselldetailsviewComponent', () => {
  let component: BubuyselldetailsviewComponent;
  let fixture: ComponentFixture<BubuyselldetailsviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BubuyselldetailsviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BubuyselldetailsviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
