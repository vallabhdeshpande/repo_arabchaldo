import { Component, OnInit } from '@angular/core';
import { BuysellService } from '../../views/theme/buysell.service';
import { Router, ActivatedRoute } from "@angular/router";
import { Buysell } from '../../views/theme/buysell.model';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-bubuyselldetailsview',
  templateUrl: './bubuyselldetailsview.component.html',
  styleUrls: ['./bubuyselldetailsview.component.scss']
})
export class BubuyselldetailsviewComponent implements OnInit {
  adposts: Buysell[];
  workflow: string;
  statusMessage: string;
  logoUrl: string;
  logourls = [];
  workflowDetails = [];
  imageurls = [];
  multiimageurls = [];
  maxDate: string;
  validTill: string;
  showLogo: string = "col-md-4";
  showImages: string = "col-md-8";
  adpost = new Buysell();;
  imagepath = environment.imagepath;
  addivshow = true;
  jobdivshow = false;
  detailsViewName: string = "Buy/Sell Details";
  detailsViewTitleCss: string = "nav-icon icon-docs";
  Job: string;
  showOtherCity: string = "d-none";
  IsType:string="";
  description:string="";
  constructor(private _buysellService: BuysellService, private route: ActivatedRoute, private router: Router, private http: HttpClient) { }


  ngOnInit() {
    //debugger
    this.getbuysellDetailsById(this.route.snapshot.queryParams['buysellId']);
    this.gettoday();
    
  }

  manageSmallAdsback() {
   
      this.router.navigate(['/bumanagebuysell']);

  }
  gettoday() {

    this.maxDate = new Date().toJSON().split('T')[0];
  }
  getbuysellDetailsById(buysellId) {
    debugger;
    this.workflow = buysellId + "," + "BuySell";
    this._buysellService.getbuySellDetailsById(buysellId).subscribe(adposts => {
      buysellId = adposts.buySellId;
      if (adposts.validTillDate !== null)
        this.validTill = adposts.validTillDate.split('T')[0];
      else
        this.validTill = this.maxDate;
      if (adposts.countryCodeContact === null || adposts.countryCodeContact === "1")
        adposts.countryCodeContact = "USA (+1)";
      console.log(adposts);
      debugger;
      // this.workflow=adId+","+"Ad";     
      this.adpost.PostedForName = adposts.postedForName;
     
      this.adpost.ContactPersonName = adposts.contactPersonName;
      this.adpost.Email = adposts.email;
      this.adpost.ContactNumber = adposts.contactNumber;
      this.adpost.AlternateContactNumber = adposts.alternateContactNumber;
      this.adpost.ContactPersonName = adposts.contactPersonName;
      this.adpost.Website = adposts.website;
      this.adpost.FaxNumber = adposts.faxNumber;
      this.adpost.ExpectedPrice=adposts.expectedPrice;
      this.adpost.Title = adposts.title;
      this.adpost.CategoryName = adposts.categoryName;
      this.adpost.SubCategoryName = adposts.subCategoryName;
      this.description = adposts.description;
      console.log(this.description);
      this.adpost.AddressStreet1 = adposts.addressStreet1;
      this.adpost.AddressStreet2 = adposts.addressStreet2;
      
      if(adposts.isPremium==true)
      this.IsType = "Premium";
      else
      this.IsType = "Free";
      if (adposts.countryId == 0) {
        this.adpost.CountryName = "";
      }
      else
        this.adpost.CountryName = adposts.countryName;
      this.adpost.StateName = adposts.stateName;
      this.adpost.CityName = adposts.cityName;
      this.adpost.ZipCode = adposts.zipCode;
      if (adposts.validFromDate !== null)
        this.adpost.ValidFromDate = adposts.validFromDate.split('T')[0];
      if (adposts.validTillDate !== null)
        this.adpost.ValidTillDate = adposts.validTillDate.split('T')[0];
      this.adpost.CountryCodeContact = adposts.countryCodeContact;
      this.adpost.buySelllStatusForDetailsView = this.getStatus(adposts.isVisible);
      if (adposts.cityId === 9999) {
        this.showOtherCity = "col-md-4"
        this.adpost.OtherCity = adposts.otherCity;
      }
      else
        this.showOtherCity = "d-none";
      debugger;
      if (adposts.imageUrl === null) {
        if (adposts.imageUrl === "") {
          this.adpost.ImageUrl = "";
          this.showLogo = "col-md-4 d-none";
        }
        else
          this.showLogo = "col-md-4 d-none";

      }
      else {
        if (adposts.imageUrl != "") {
          this.adpost.ImageUrl = adposts.imageUrl;
          // this.logoUrl = this.imagepath + 'BuySell/' + adposts.buySellId + '/Image/' + this.adpost.ImageUrl;
          // this.logourls.push(this.logoUrl);
          this.multiimageurls = adposts.imageUrl.split(',');
          for (let i = 0; i < this.multiimageurls.length; i++) {
          if(adposts.isPaidAd==true)
            this.imageurls.push(this.imagepath + 'BuySell/' + adposts.buySellId + '/Premium/' + this.multiimageurls[i]);
            else
            this.imageurls.push(this.imagepath + 'BuySell/' + adposts.buySellId + '/Free/' + this.multiimageurls[i]);
        }
          this.showLogo = "col-md-4";
        }
        else
          this.showLogo = "col-md-4 d-none";
      }
      //if(adposts.imageUrl==="")  
      //{
      //  this.adpost.AdImageUrl="";
      //  this.showLogo="col-md-8 d-none";

      //}
      // if (adposts.imageUrl === null) {
      //   if (adposts.imageUrl === "") {
      //     this.adpost.AdImageUrl = "";
      //     this.showImages = "col-md-8 d-none";
      //   }
      //   else
      //     this.showImages = "col-md-8 d-none";

      // }
      // else {
      //   if (adposts.imageUrl !== "") {
      //     this.adpost.AdImageUrl = adposts.imageUrl;
      //     this.multiimageurls = adposts.imageUrl.split(',');
      //     for (let i = 0; i < this.multiimageurls.length; i++) {
      //       this.imageurls.push(this.imagepath + 'SmallAds/' + adposts.smallAdId + '/Images/' + this.multiimageurls[i]);
      //     }
      //     this.showImages = "col-md-8";
      //   }
      //   else
      //     this.showImages = "col-md-8 d-none";
      //   //alert(this.imageurls);
      // }

    });
  }
  getStatus(status: boolean) {
    debugger;
    var currentStatus: string = "";
    if (status == true)
      currentStatus = "Active";
    else if (status == false)
      currentStatus = "Inactive";
    else
      currentStatus = "Pending";
    return currentStatus;


  }


}
