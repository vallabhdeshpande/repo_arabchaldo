
import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Manageblog} from '../../views/theme/manageblog';
import { ManageblogService } from '../../views/theme/manageblog.service';
import {FormBuilder,Validators} from '@angular/forms';
import { User } from '../../views/theme/user'; 
import { Observable } from 'rxjs/Observable'; 
import {AdblogComponent} from '../../views/theme/adblog/adblog.component';
import { environment } from '../../../environments/environment';
import { OrderPipe } from 'ngx-order-pipe';
import { ToastrService } from 'ngx-toastr'; 
@Component({
  selector: 'app-bumanageblog',
  templateUrl: './bumanageblog.component.html',
  styleUrls: ['./bumanageblog.component.scss']
})
export class BumanageblogComponent implements OnInit {

  private _allUser: Observable<User[]>; 
  UserId:string="0"; 
   manageblogs: Manageblog[];  
  statusMessage: string;
  manageblog = new Manageblog();order: string;
  reverse: boolean;
  users: User[];  
  setStatus: string = "";
  user = new User();;
  public pageSize: number = 10;
  public p: number;
  blogId:bigint;
  public response: {'dbPath': ''};
  FormUserAdd: any;
  remark: string="";
  workflowstatusId:number;
  action:string="";
  sortedCollection: any[];
  id:number;
  workflowdetails:string;
  deleteremark:string;
  imagepath = environment.imagepath;
  activity:boolean;
    setStatusActive:string="";
  setStatusInactive:string="";
  visibilitybutton:string;
  visibilityspan: string;
  loggedInUserId: string;
  actionmessage:string;
  filter: string = "";
  statusText: string = "";
  public uploadFinished = (event) => 
  {
    
    this.response = event;
  }

  gotoBlogDetails(blogPost:Manageblog): void 
    {
      debugger
      this.router.navigate(['/bublogdetailsview'],{queryParams:{id:blogPost.blogId}});
    };

  
  Adblog(): void 
  {

    this.router.navigate(['/buadblog']);        
  }

  updateBlog(manageblog:Manageblog): void
       {
         debugger
         this.router.navigate(['buadblog'],{queryParams:{blogId:manageblog.blogId}});     
       }
       constructor(private toastr: ToastrService, private orderPipe: OrderPipe,private manageblogService: ManageblogService,private formbulider: FormBuilder, private router: Router)
       {
        this.sortedCollection = orderPipe.transform(this.users, 'firstName');
        console.log(this.sortedCollection);
      }
  
    setOrder(value: string) 
    {
      if (this.order === value) 
      {
        this.reverse = !this.reverse;
      }
  
      this.order = value;
    }

    
   
       getBlogList(): void 
       {
         debugger
         // this.managebanner.IsActive= "Active";
         this.manageblogService.getBlogList().subscribe(manageblogs => {
         manageblogs = manageblogs.body;     
         console.log(this.manageblogs);
         if(manageblogs !=null)
         {
           if(manageblogs.length >0)
           this.manageblogs = manageblogs;  
           else
           this.manageblogs = [];    
         }
         else
         this.manageblogs = [];    
       });
       }


       getStatus(Status: string) {  
        debugger;
         if (Status == "Active") {
           this.statusText = "Active";
           return this.setStatus = "badge badge-success";
         }
           
   
         if (Status == "InActive") {
           this.statusText = "InActive";
           return this.setStatus = "badge badge-danger";
         }
             
   
         if (Status == "" ||Status == null) {
           this.statusText = "Pending";
           return this.setStatus = "badge badge-warning";
         }
                  
   
     } 
     getPicUrl(picurl:string,blogId)
   {
     if(picurl==null)
     return "assets/img/default/no_image_placeholder.jpg";
     else if (picurl=="")
     return "assets/img/default/no_image_placeholder.jpg";
     else 
     return this.imagepath+'Blogs/'+blogId+'/Image/'+picurl;
   } 
   
     ngOnInit() 
     {
       this.getBlogList();
       this.loggedInUserId = localStorage.getItem('userId');
     }
   
     onDelete(BlogId: bigint) 
          {
            this.blogId=BlogId;
            this.actionmessage="Are you sure to delete this Blog?"
          }
          ActionDelete()
          {
            this.action="Delete";
            this.deleteremark="";
       
            this.workflowdetails = this.blogId + "," + this.loggedInUserId + "," + this.action + "," + this.deleteremark; 
               this.manageblogService.updatewithWorkflow(this.workflowdetails).
               subscribe(response=> {console.log(response); 
                 this.getBlogList(); 
   
               if (response.status === 200) 
               {
                 console.log(response);
                 if((<any>response)._body==="true")
                 this.toastr.success('Blog successfully deleted.');
                   else
                   this.toastr.info('Blog is active on website.');
                   debugger
               
               } 
               else 
               {
                 this.toastr.warning('Problem with service. Please try again later!');
               }
               
               },
               
                 (error) =>{
                   console.log(error);

                   this.toastr.error('Problem with service. Please try again later!');
               });
          }
   
     ActionOnApprove(BlogId, event:any)
     {
       this.blogId = BlogId;
       this.action="Approve"
       this.remark=" "
     }
   
     ActionOnReject(BlogId, event:any)
     {
       this.blogId = BlogId;
       this.action="Reject"
       this.remark=" "
     }
   
     ActionOnPost(remark) 
     {
       debugger;
           
       this.workflowdetails = this.blogId + "," + this.loggedInUserId + "," + this.action + "," + remark;
       
           if (confirm('Are you sure to perform this Action?')) {
             this.manageblogService.updatewithWorkflow(this.workflowdetails).subscribe(response => {
               console.log(response);
               console.log('Updated');
               this.getBlogList();
             },
       
               (error) => {
                 console.log(error);
       
                 this.toastr.error( 'Problem with service. Please try again later!');
               });
           }
          
       }


  bublogDetailsView(blogId: number) {
    debugger;
    this.router.navigate(['/bublogdetailsview'], { queryParams: { id: blogId } });
  }


   }
   
   





