import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BumanageblogComponent } from './bumanageblog.component';

describe('BumanageblogComponent', () => {
  let component: BumanageblogComponent;
  let fixture: ComponentFixture<BumanageblogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BumanageblogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BumanageblogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
