import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { HttpEventType, HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.scss']
})
export class FileuploadComponent implements OnInit {
  baseUrl = environment.baseUrl;
  public progress: number;
  public message: string;
  public filePath;
  uploadedFileName:string;
  imgURL: any;
public imgmessage: string;
urls = [];
filenames=[];
@Input() imgpatharr:string[];
  url:any;
  @Output() public onuploadFinishedFile = new EventEmitter();

  constructor(private http: HttpClient,private toastr: ToastrService) { }

  ngOnInit() {
  } 

  //Image Upload
  onSelectFile(event) {   
debugger;
    if (event.target.files && event.target.files[0]) {
       var filesAmount = event.target.files.length;
        for (let i = 0; i < filesAmount; i++) {
          //Image type validation

          var allowedFiles = [".doc", ".docx", ".pdf"];
          var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
          // if(event.target.files[i].type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || 
          // event.target.files[i].type === 'application/doc' || 
          // event.target.files[i].type ==='application/pdf'|| 
          // event.target.files[i].type ==='application/txt')
          
            let fileToUpload = <File>event.target.files[i];
            if (regex.test(fileToUpload.name.toLowerCase()))
          {
            if(fileToUpload.size<=2097152)
            {
            var fileNameToUpload=this.getFileNameWithDateTime(fileToUpload.name);
            this.uploadedFileName=fileNameToUpload;
            var reader = new FileReader();
            this.filenames.pop();
          this.filenames.push(fileNameToUpload);
          this.urls.pop();
          this.urls.push(fileToUpload.name);
         
      //       reader.onload = (event: any) => {
            
      //     this.urls.push(event.target.result); 
      //     event.target.result="";
      //  }
       //debugger;
       const formData = new FormData();
      
       formData.append('file', fileToUpload, fileNameToUpload);
       this.http.post(this.baseUrl+'uploadfile', formData, {reportProgress: true, observe: 'events'})
       .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
        this.progress = Math.round(100 * event.loaded / event.total);
      else if (event.type === HttpEventType.Response) {
        //debugger;
        this.message = 'Upload success.';
        this.onuploadFinishedFile.emit(this.filenames.join());
        debugger;
      }
       });
      }
      else
      this.toastr.warning("file size should be less than 2 MB");
        }
        else{
          this.imgmessage = "Please upload files having extensions: " + allowedFiles.join(', ') + " only.";
          this.toastr.warning(this.imgmessage);
      return;
        }
      }
        
    }
    this.imgpatharr=this.filenames;
   
  }

  // Get Fime with current Date time stamp

  getFileNameWithDateTime(fileNameToUpload)
  {
    var ext=(fileNameToUpload.substring(fileNameToUpload.lastIndexOf('.')+1, fileNameToUpload.length) || fileNameToUpload);
    fileNameToUpload=fileNameToUpload.substring(0, fileNameToUpload.lastIndexOf('.'));
    //fileNameToUpload+=
 
    var today = new Date();
 var sToday = (today.getMonth()+1).toString();
 sToday += today.getDate().toString();
 //sToday += today.getYear().toString();
 sToday += today.getHours().toString();
 sToday += today.getMinutes().toString();
 sToday += today.getSeconds().toString();
 return fileNameToUpload+"_"+sToday+"."+ext;
  }




// Image Delete
     onfiledelete(i){     
       debugger;
      this.urls.splice(i,1);
      this.filenames.splice(i,1);
      this.http.delete(this.baseUrl + 'uploadfile/DeleteFile?FileName='+this.uploadedFileName)
      //?searchCriteria='+searchCriteria
        .subscribe(event => {
          
         
        });
}

  
  
  
  
}
