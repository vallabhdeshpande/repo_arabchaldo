import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DashboardService } from '../../views/shared/dashboard.service';
import { Dashboard } from '../../views/shared/dashboard.model';
import { Observable } from 'rxjs';
import { Paymentplan } from '../../views/theme/paymentplan';
import { Paymentplanservice } from '../../views/theme/paymentplanservice';
import { StripeComponent } from '../stripe/stripe.component';
import { ToastrService } from 'ngx-toastr'


@Component({
  selector: 'app-budashboard',
  templateUrl: './budashboard.component.html',
  styleUrls: ['./budashboard.component.scss']
})
export class BudashboardComponent implements OnInit {
  public userId:string;
  BUdashboardCountList: Dashboard[];
  Dashboard=new Dashboard(); 
  // pieChartLabels: string[];
  // pieChartData: number[];
  // pieChartType:string;
  planList: any[];
  public _allPlan: Observable<Paymentplan[]>;

  LogoImageCount: number;
  BusinessListingImageCount:number;
  EventImageCount:number;
  BannerImageCount:number;
  JobsAdvCount: number;
  BuySellCount: number;
  SmallAdsCount: number;
  Miscellaneous: string = "";
  planPaid: string;
  ValidFrom: Date;
  ValidTill: Date;
  FinalAmount: number= 0;

  paymentButton: boolean = false;


  // stripe payment starts
  @ViewChild(StripeComponent, { static: false }) stripeCmp: StripeComponent;

  public finalAmount: number;
  public CustomerName: string;
  public CustomerId: number;
  public PlanIdForStripe: number;
  public CustomerEmail: string;

  public stripeResponse = (event) => { this.stripepaymentresponse = event; }

  public stripepaymentresponse: string;


  public stripePaymentStatus: string;
  public stripePaymentAmount: number;
  public stripePaymentChargeId: string;
  public stripePaymentTokenId: string;
  public stripePaymentError: string;

  public BUStripe: boolean = false;

  // stripe payment ends

  //pie
  pieChartLabels: string[]= ['BusinessListing', 'Banners', 'Events','Jobs','Blogs','Buy/Sell','Advertisement'];
  pieChartData: number[]=[0,0,0,0,0,0,0];
  pieChartType:string= 'pie'; 
  constructor(private router: Router, private route: ActivatedRoute, private DashboardService: DashboardService, private toastr: ToastrService) { }

  logos = [
    { img: "assets/website/images/clientele-logos/logo1.jpg" },
  ];

  logoConfig = {
    "arrow": true,
    "autoplay": false,
    "slidesToShow": 3,
    "slidesToScroll": 1,
  };

  afterChange(event:any)
  {}

  ngOnInit() {
    debugger;
    if (localStorage.getItem('token') === null)
      this.router.navigate(['/login']);
    else {
      console.log(localStorage.getItem);
      this.userId = localStorage.getItem('userId');
      this.DashboardService.GetBUtotalCount(this.userId).subscribe(BUdashboardCountList => {
        console.log(BUdashboardCountList);
        this.Dashboard.ActiveBusiLitingCount = BUdashboardCountList[0].ActiveBusiLitingCount;
        this.Dashboard.PendingBusiLitingCount = BUdashboardCountList[0].PendingBusiLitingCount;
        this.Dashboard.ActiveSmallAdsCount = BUdashboardCountList[0].ActiveSmallAdsCount;
        this.Dashboard.PendingSmallAdsCount = BUdashboardCountList[0].PendingSmallAdsCount;
        this.Dashboard.ActiveBannerCount = BUdashboardCountList[0].ActiveBannerCount;
        this.Dashboard.PendingBannerCount = BUdashboardCountList[0].PendingBannerCount;
        this.Dashboard.ActiveEventCount = BUdashboardCountList[0].ActiveEventCount;
        this.Dashboard.PendingEventCount = BUdashboardCountList[0].PendingEventCount;
        this.Dashboard.ActiveJobsCount = BUdashboardCountList[0].ActiveJobsCount;
        this.Dashboard.PendingJobsCount = BUdashboardCountList[0].PendingJobsCount;
        this.Dashboard.ActiveBuysellCount = BUdashboardCountList[0].ActiveBuysellCount;
        this.Dashboard.PendingBuysellCount = BUdashboardCountList[0].PendingBuysellCount;
        this.Dashboard.ActiveBlogsCount = BUdashboardCountList[0].ActiveBlogsCount;
        this.Dashboard.PendingBlogsCount = BUdashboardCountList[0].PendingBlogsCount;
        this.Dashboard.MessageTopicCount = BUdashboardCountList[0].MessageTopicCount;
        this.Dashboard.MessageReplyCount = BUdashboardCountList[0].MessageReplyCount;
        // this.Dashboard.PaidAds = BUdashboardCountList[0].PaidAds;
        // this.Dashboard.FreeAds = BUdashboardCountList[0].FreeAds;
        // this.Dashboard.ApprovedAds = BUdashboardCountList[0].ApprovedAds;
        // this.Dashboard.RejectedAds = BUdashboardCountList[0].RejectedAds;
        // this.Dashboard.PendingAds = BUdashboardCountList[0].PendingAds;
        // this.Dashboard.PaidBanner = BUdashboardCountList[0].PaidBanner;
        // this.Dashboard.FreeBanner = BUdashboardCountList[0].FreeBanner;
        // this.Dashboard.ApprovedBanner = BUdashboardCountList[0].ApprovedBanner;
        // this.Dashboard.RejectedBanner = BUdashboardCountList[0].RejectedBanner;
        // this.Dashboard.PendingBanner = BUdashboardCountList[0].PendingBanner;
        // this.Dashboard.PaidEvents = BUdashboardCountList[0].PaidEvents;
        // this.Dashboard.FreeEvents = BUdashboardCountList[0].FreeEvents;
        // this.Dashboard.ApprovedEvents = BUdashboardCountList[0].ApprovedEvents;
        // this.Dashboard.RejectedEvents = BUdashboardCountList[0].RejectedEvents;
        // this.Dashboard.PendingEvents = BUdashboardCountList[0].PendingEvents;
        // this.Dashboard.PaidJobs = BUdashboardCountList[0].PaidJobs;
        // this.Dashboard.FreeJobs = BUdashboardCountList[0].FreeJobs;
        // this.Dashboard.ApprovedJobs = BUdashboardCountList[0].ApprovedJobs;
        // this.Dashboard.RejectedJobs = BUdashboardCountList[0].RejectedJobs;
        // this.Dashboard.PendingJobs = BUdashboardCountList[0].PendingJobs;
        this.Dashboard.AdsImageCount = BUdashboardCountList[0].AdsImageCount;
        this.Dashboard.BannerImageCount = BUdashboardCountList[0].BannerImageCount;
        this.Dashboard.EventImageCount = BUdashboardCountList[0].EventImageCount;
        this.Dashboard.LogoCount = BUdashboardCountList[0].LogoCount;
        this.Dashboard.JobsAdvCount = BUdashboardCountList[0].JobsAdvCount;
        this.Dashboard.LogoCount = BUdashboardCountList[0].LogoCount;
        this.Dashboard.JobsAdvCount = BUdashboardCountList[0].JobsAdvCount;
        this.Dashboard.BuySellCount = BUdashboardCountList[0].BuySellCount;
        this.Dashboard.SmallAdsCount = BUdashboardCountList[0].SmallAdsCount
        //Pie Chart
        this.pieChartLabels = ['BusinessListing', 'Banners', 'Events','Jobs','Blogs','Buy/Sell','Advertisement'];
        this.pieChartData = [Number(this.Dashboard.AdsImageCount), Number(this.Dashboard.BannerImageCount), Number(this.Dashboard.EventImageCount), Number(this.Dashboard.LogoCount), Number(this.Dashboard.JobsAdvCount), Number(this.Dashboard.BuySellCount), Number(this.Dashboard.SmallAdsCount)];
        this.pieChartType = 'pie';
        // plan list
        this._allPlan = this.DashboardService.UserPlanList(this.userId, "BUDashboard");
        console.log(this._allPlan);
      });
    }
  }

  subscriptionPlanDetails(event: any) {
    debugger;
    console.log(event.target.value);
    this.BUStripe = false;
    this.DashboardService.getPlanDetails(event.target.value).subscribe(data => {
      var planIdDetails = JSON.parse(data._body);
      console.log(planIdDetails);
      console.log(planIdDetails.planId);
      this.BusinessListingImageCount = planIdDetails.adImageCount;
      this.BannerImageCount = planIdDetails.bannerImageCount;
      this.EventImageCount = planIdDetails.eventImageCount;
      this.LogoImageCount = planIdDetails.logoCount;
      this.JobsAdvCount = planIdDetails.jobsAdvCount;
      this.BuySellCount = planIdDetails.buySellCount;
      this.SmallAdsCount = planIdDetails.smallAdsCount;
      this.ValidFrom = planIdDetails.validFrom;
      this.ValidTill = planIdDetails.validTill;
      this.FinalAmount = planIdDetails.finalAmount;
      console.log(planIdDetails.finalAmount + "  " + this.FinalAmount);
      if (planIdDetails.miscellaneiousTotal >= 0) {
        this.Miscellaneous = "$ " + planIdDetails.miscellaneiousTotal;
      }
      else
        this.Miscellaneous = "";

      console.log(planIdDetails.isPaid + "  " + this.planPaid);

      if (planIdDetails.isPaid === true) {
        this.planPaid = "Paid";
        this.paymentButton = false;
      }
      else if (planIdDetails.isPaid === false) {
        this.planPaid = "Pending";
        this.paymentButton = true;
      }
      this.finalAmount = planIdDetails.finalAmount;
      this.CustomerName = localStorage.getItem('firstName');
      this.CustomerId = planIdDetails.userId;
      this.PlanIdForStripe = planIdDetails.planId
      this.CustomerEmail = localStorage.getItem('email');



    });
  }

  paymentForPlanAfterSave() {
    debugger;
    document.getElementById("checkoutAfterSave").setAttribute("disabled", "disabled");
    this.BUStripe = true;
  }

  onlinePaymentResponse($event) {
    debugger;
    this.stripePaymentStatus = $event.paymentStatus;

    if (this.stripePaymentStatus == "true") {
      this.stripePaymentAmount = $event.paymentamount;
      this.stripePaymentChargeId = $event.paymentchargeid;
      this.stripePaymentTokenId = $event.paymenttokenid;
      //this.addPlan();
      this.toastr.success('Payment successfully done');
      this.router.navigate(['/dashboard']);

    }

    else if (this.stripePaymentStatus == "false") {
      this.stripePaymentError = $event.errorMessage;
      console.log(this.stripePaymentError);
      this.toastr.error(this.stripePaymentError);
      return;
    }
  }
}






  

  
