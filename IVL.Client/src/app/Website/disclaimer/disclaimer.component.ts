import { Component, OnInit } from '@angular/core';
import { WebsiteService} from '../../shared/website.service';
import { HttpEventType, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-disclaimer',
  templateUrl: './disclaimer.component.html',
  styleUrls: ['./disclaimer.component.scss']
})
export class DisclaimerComponent implements OnInit {
  DiscalimerData: string="";
  DiscalimerArabicdata: string = "";
  showEng: string = "";
  showArb: string = "d-none";
  constructor(private WebsiteService: WebsiteService, private http: HttpClient) { }

  ngOnInit() {
    this.GetDisclaimerData();
    this.changeDisclaimerData();
  }
  changeDisclaimerData() {

    if (localStorage.getItem('language') !== null) {
      if (localStorage.getItem('language') === "English") {
        this.showEng = "";
        this.showArb = "d-none";
      }
      else {
        this.showEng = "d-none";
        this.showArb = "";
      }

    }
  }
  GetDisclaimerData()
  {
    debugger;
   this.WebsiteService.GetDisclaimerData().subscribe(disclaimer => {
     console.log(disclaimer);
     this.DiscalimerData = disclaimer[0].Disclaimer;
     this.DiscalimerArabicdata = disclaimer[0].DisclaimerArabic;
      if (localStorage.getItem('language') !== null) {
        if (localStorage.getItem('language') === "English") {
        
          this.DiscalimerData = disclaimer[0].Disclaimer;
        }
        else {
          this.DiscalimerArabicdata = disclaimer[0].DisclaimerArabic;
       
        }

      }
      else
        this.DiscalimerData = disclaimer[0].Disclaimer;


    });

  } 

}
