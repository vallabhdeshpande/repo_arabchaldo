import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusidebarComponent } from './busidebar.component';

describe('BusidebarComponent', () => {
  let component: BusidebarComponent;
  let fixture: ComponentFixture<BusidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
