import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { environment} from "../../../environments/environment"

@Component({
  selector: 'app-busidebar',
  templateUrl: './busidebar.component.html',
  styleUrls: ['./busidebar.component.scss']
})
export class BusidebarComponent implements OnInit 
{
  imagepath = environment.imagepath;
  userName: string;
  ProfilepicUrl: string;
  Profilepicurls = [];
  userId:string;
  constructor(private router: Router) { }

  ngOnInit() 
  {
    debugger;
    console.log(localStorage.getItem);
    this.userName = localStorage.getItem('firstName') + ' ' + localStorage.getItem('lastName');
    this.userId = localStorage.getItem('userId');
    if (localStorage.getItem('profilePic') != "") {
      this.ProfilepicUrl = localStorage.getItem('profilePic')
      this.Profilepicurls.push(this.imagepath + 'UserProfile/' + localStorage.getItem('userId') + '/ProfilePic/' + localStorage.getItem('profilePic'));
    }
    else { 
      this.ProfilepicUrl = "assets/img/default/ArabChaldoLogo.PNG";
      this.Profilepicurls.push(this.ProfilepicUrl);
    }

  }
 
}
