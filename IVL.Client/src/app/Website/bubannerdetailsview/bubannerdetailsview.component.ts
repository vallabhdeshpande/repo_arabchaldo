import { Component, OnInit } from '@angular/core';
import { Managebannerservice } from '../../views/theme/managebannerservice';
import {Router, ActivatedRoute} from "@angular/router";
import {Managebanner} from '../../views/theme/managebanner';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Observable } from 'rxjs';
import { User } from '../../views/theme/user'; 
import { UserService } from '../../views/theme/user.service';

@Component({
  selector: 'app-bubannerdetailsview',
  templateUrl: './bubannerdetailsview.component.html',
  styleUrls: ['./bubannerdetailsview.component.scss']
})
export class BubannerdetailsviewComponent implements OnInit 
{
  
  bannerposts: Managebanner[];
  users: User[]; 
  bannerpost = new Managebanner();  
  userDetails = new User();
  FaxNumber: number;
  showOtherCity: string = "d-none";
  bannerForm:any;
  //managebanners: Managebanner[];
  //managebanner = new Managebanner();
  public RoleId: any;
  RoleName: string;
  statusMessage: string = "";
  type: string = "";
  logoUrl: string;
  logourls = [];
  imageurls = []; 
  imagepath = environment.imagepath;
  workflowDetails = [];
  workflow: string;
  description: string = "";
  constructor(private managebannerservice: Managebannerservice,private userService: UserService,private route: ActivatedRoute, private router: Router, private http: HttpClient) { }
  

  // manageAdsback()
  // {
  //  this.router.navigate(['/theme/managebanner']);
  // }
  //managebannerback() { this.router.navigate(['/theme/managebanner']); }
  ngOnInit() 
  {
    debugger
    this.getBannerDetailsById(this.route.snapshot.queryParams['id']);
  }


  getBannerDetailsById(id)
  {
    debugger;
    this.workflow = id + "," + "Banner";
    this.managebannerservice.getBanner(id).subscribe(data =>
    {
      var data = JSON.parse(data._body);

      console.log(data);
      // debugger
      this.bannerpost.ContactPersonName = data.contactPersonName;
      if (data.countryCodeContact == "1") {
        this.bannerpost.ContactNumber = "USA" + "(+"+data.countryCodeContact + ") " + data.contactNumber;
      }
      else if (data.countryCodeContact == "2")
      {
        this.bannerpost.ContactNumber = "UK" + "(+" + data.countryCodeContact + ") " + data.contactNumber;
      }
      else
        this.bannerpost.ContactNumber = "(+" + data.countryCodeContact + ") " + data.contactNumber;

      if (data.isFeatured) {
        this.type = "Premium";
      }
      else
        this.type = "Featured";

      if (data.isVisible == true)
        this.statusMessage = "Active";
      else if (data.isVisible == false)
        this.statusMessage = "Inactive";
      else if (data.isVisible == null)
        this.statusMessage = "Pending";

      this.bannerpost.AlternateContactNumber = data.alternateContactNumber;      
      this.bannerpost.AddressStreet1 = data.addressStreet1;
      this.bannerpost.AddressStreet2 = data.addressStreet2;
      this.bannerpost.StateName = data.stateName;
      this.bannerpost.CityName = data.cityName;
      this.bannerpost.ZipCode = data.zipCode;
      if(data.validFromDate!==null)
      this.bannerpost.ValidFromDate = data.validFromDate;
      if(data.validTillDate!==null)
      this.bannerpost.ValidTillDate = data.validTillDate;
      this.bannerpost.Title = data.title;
      this.bannerpost.Website = data.website;
      this.bannerpost.Description = data.description;
      this.description = this.bannerpost.Description;

      this.bannerpost.ImageUrl = data.imageUrl;
      this.bannerpost.FaxNumber = data.faxNumber;
      this.bannerpost.Email = data.email;
      if (data.cityName == "Other") {
        this.showOtherCity = "col-md-4";
        this.bannerpost.OtherCity = data.otherCity;
      }
      //this.bannerpost.OtherCity = data.otherCity;
      this.logoUrl = this.imagepath + 'Banners/' + id + '/Image/' + data.imageUrl;
      this.logourls.push(this.logoUrl);
      debugger

      this.userService.getUserById(data.postedFor).subscribe(detail =>
      {
        var detail = JSON.parse(detail._body);
        //this.userDetails.Email = detail.email;
        //this.userDetails.FaxNumber = detail.faxNumber;
        this.bannerpost.PostedForName = detail.firstName + " " + detail.lastName;
        //this.FaxNumber = detail.faxNumber;

        console.log(detail);

      });    
  });

  }
}
