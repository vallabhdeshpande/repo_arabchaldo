import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BubannerdetailsviewComponent } from './bubannerdetailsview.component';

describe('BubannerdetailsviewComponent', () => {
  let component: BubannerdetailsviewComponent;
  let fixture: ComponentFixture<BubannerdetailsviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BubannerdetailsviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BubannerdetailsviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
