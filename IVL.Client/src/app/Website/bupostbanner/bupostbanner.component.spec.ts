import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BupostbannerComponent } from './bupostbanner.component';

describe('BupostbannerComponent', () => {
  let component: BupostbannerComponent;
  let fixture: ComponentFixture<BupostbannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BupostbannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BupostbannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
