import { Response } from '@angular/http';
import { Component, OnInit, Inject, Output, EventEmitter,ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Country, State, City, category, Subcategory, Roles, CountryCodes } from '../../views/shared/common.model';
import { User } from '../../views/theme/user';
import { UserService } from '../../views/theme/user.service';
import { Managebannerservice } from '../../views/theme/managebannerservice';
import { Managebanner } from '../../views/theme/managebanner';
import { CommonService } from '../../views/shared/common.service';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { environment } from '../../../environments/environment';
import { element } from 'protractor';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Paymentplan } from '../../views/theme/paymentplan';
import { Paymentplanservice } from '../../views/theme/paymentplanservice';
import { UploadComponent } from '../../../app/Website/upload/upload.component'; 
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import * as countryCodeData from '../../../app/views/shared/countrycodes.json';
@Component({
  selector: 'app-bupostbanner',
  templateUrl: './bupostbanner.component.html',
  styleUrls: ['./bupostbanner.component.scss']
})
export class BupostbannerComponent implements OnInit {
  @ViewChild(UploadComponent, { static: false }) UploadCmpt: UploadComponent;
  public _allUser: Observable<User[]>;
  public _allCountry: Observable<Country[]>;
  public _allCountryCodes: Observable<CountryCodes[]>;
  public _allState: Observable<State[]>;
  public _allCity: Observable<City[]>;
  public _allPlan: Observable<Paymentplan[]>;
  public previousPlan: Paymentplan;

  pagedetail: string;
  postedFor: string = "0";
  CountryId: string = "0";
  StateId: string = "0";
  CityId: string = "0";
  logopath: string[];
  imagepath: any[];
  countries: any[];
  logoUrl: string = "";
  imageUrl: string = "";
  ABCD: string[];
  AddBannerText: string = "Add Banner";
  CountryName: string = "";
  CityName: string = "";
  StateName: string = "";
  states: any[];
  cities: any[];
  //dvImage: string[];
  FormPostBanner: any;
  actionButton;
  action = "save";
  myData = null;
  formData = null;
  msgs = null;
  public imagefilename: string[];
  public logofilename: string[];
  managebanners: Managebanner[];
  managebanner = new Managebanner();
  statusMessage: string;
  saveButtonText: string = "Save";
  IsFeatured: boolean;
  HorizontalView: boolean;
  VerticalView: boolean;
  displayText = 'btn btn-sm btn-primary';
  displayExtLogo = 'col-md-6 d-none';
  displayNewLogo = 'col-md-6';
  displayExtImages = 'col-md-6 d-none';
  displayExtUploads = 'form-bg-section d-none';
  maxDate: string;
  validTill: string;
  Vertical: string = "";
  Horizontal: string = "";
  extLogoUrls = [];
  extLogos = [];
  //resetButtonStyle:string="block";
  public logoresponse: string[];
  bannerimage: string;
  loggedInUserId: string;
  addbutton: HTMLElement;
  serverpath = environment.serverpath;
  commonimagepath = environment.imagepath;
  submitted = false;
  Id: number;
  remainingBannerImage: number;
  tempImageUrl: any;
  showOtherCity: string = "d-none";
  subscription: any;
  countryCodeList: Array<any> = [];

  bindStateId: number = 0;
  countryCodesNew: any = (countryCodeData as any).default;
  countrycode = this.countryCodesNew;
  constructor(private toastr: ToastrService, private managebannerservice: Managebannerservice, private paymentPlanService: Paymentplanservice, private _userService: UserService, private _commonService: CommonService,
    private _http: HttpClient, private _router: Router, private _formbulider: FormBuilder, private route: ActivatedRoute, @Inject(DOCUMENT) document) {

  }

  ngAfterViewInit() {
    //document.getElementById("eventDate").setAttribute("min", this.maxDate);
    //document.getElementById('#SaveEvent').innerHTML = this.saveButtonText;
  }
  GetUserDetails(event: any) { }
  gettoday() {
    this.maxDate = new Date().toJSON().split('T')[0];
  }
 navigatetocontactus()
  {
    this._router.navigate(['/contact']);
  }
  ngOnInit() {
    this.loggedInUserId = localStorage.getItem('userId');
    debugger;
    this.FillCountryDDL();
    this.FillUserDDL();
    this.gettoday();
    debugger;
    this.FormPostBanner = this._formbulider.group({
      bannerId: ['', ''],
      title: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      faxNumber: [''],
      addressStreet1: ['', ''],
      addressStreet2: ['', ''],
      countryId: ['1', ''],
      countryCodeContact: ['1', ''],
      //validTill: ['', Validators.required],
      stateId: [this.bindStateId, ''],
      cityId: ['', ''],
      isFeatured: [''],
      zipCode: ['', Validators.pattern("^[0-9]*$")],
      postedFor: ['', Validators.required],
      postedName: ['null', ''],
      contactPersonName: ['', [Validators.required]],
      contactNumber: ['', [Validators.required, Validators.minLength(10)]],
      website: ['', ''],
      description: ['', ''],
      imageUrl: [],
      alternateContactNumber: [''],
      createdBy: ['', ''],
      bannerImageCount: [''],
      OtherCity: ['']
    });

    this.getBannerDetailsById(this.route.snapshot.queryParams['id']);
  }


  getBannerDetailsById(id) {
    debugger;

    if (id == undefined) {
      this.VerticalView = true;
      this.pagedetail = "Post New banner";
      this.action = "save";
      this._userService.getUserById(localStorage.userId).subscribe(userdetail => {
        var userdetail = JSON.parse(userdetail._body);

        console.log(userdetail);


        //this._allState = this._commonService.StateDDL(userdetail.countryId);
        //this._allCity = this._commonService.BUCityDDL(userdetail.stateId);
        this._commonService.BUCityDDL(userdetail.stateId).subscribe(data => {
          console.log(data);
          debugger;
          this.cities = data;
        });
        this.FillSubscriptionPlan(userdetail.userId);
        // this._roles = this._commonService.RoleDDL();
        debugger;

        if (userdetail.countryCodeContact === null)
          userdetail.countryCodeContact = "1";

        this.FormPostBanner.patchValue({
          bannerId: '',
          title: '',
          email: userdetail.email,
          faxNumber: userdetail.faxNumber,
          addressStreet1: userdetail.addressStreet1,
          addressStreet2: userdetail.addressStreet2,
          countryId: userdetail.countryId,
          stateId: userdetail.stateId.toString(),
          cityId: userdetail.cityId.toString(),
          zipCode: userdetail.zipCode,
          postedFor: userdetail.userId,
          postedName: userdetail.firstName + '' + userdetail.lastName,
          contactPersonName: userdetail.firstName + " " + userdetail.lastName,
          contactNumber: userdetail.contactNumber,
          countryCodeContact: "1",
          website: '',
          description: '',
          alternateContactNumber: userdetail.alternateContactNumber,
          // validTill: '',
          imageUrl: '',
          createdBy: this.loggedInUserId,
          bannerImageCount: "",
          OtherCity: ""
        });
      });


    }

    else {
      this.pagedetail = "Update Banner";
      this.managebannerservice.getBannerById(id).subscribe(data => {
        var data = JSON.parse(data._body);
        console.log(data);
        debugger;
        this.action = "edit";
        // this.AddBannerText="Update Banner";
        this.saveButtonText = "Update";
        this.displayText = 'btn btn-sm btn-primary d-none';
        this.StateId = data.stateId;

        this._userService.getUserById(localStorage.userId).subscribe(userdetail => {
          var userdetail = JSON.parse(userdetail._body);

          this._commonService.BUCityDDL(data.stateId).subscribe(data => {
            debugger;
            console.log(data);
            this.cities = data;
          });
          console.log(userdetail);
          //debugger;

          // Ad Images Retrieval
          this.displayExtUploads = 'form-bg-section';
          if (data.imageUrl !== null) {
            this.renderExistingImages(data.imageUrl, data.bannerId, "Images");
            this.displayNewLogo = 'col-md-6 d-none';
          }
          else {
            this.displayExtImages = 'col-md-6 d-none';
          }



          this._commonService.StateDDL(data.countryId).subscribe(data => {
            debugger
            console.log(data);
            this.states = data;

          });

          this.FillSubscriptionPlan(userdetail.userId);
          this.countrycode = this.countryCodesNew;
         
          // this._roles = this._commonService.RoleDDL();
          //debugger;
          // if (data.validTill !== null)
          // {
          //   this.validTill = data.validTill.split('T')[0];
          // }
          // else
          // {
          //   this.validTill = this.maxDate;
          // }

          if (data.cityId == 9999) {
            this.showOtherCity = "col-md-4";
          }
          else
            this.showOtherCity = "d-none";


          if (userdetail.countryId === null) {
            userdetail.countryId = "1";
          }

          if (data.countryCodeContact === null) {
            data.countryCodeContact = "1";
          }

          if (data.website == null) {
            data.website = '';
          }
          if (data.isFeatured === true) {
            this.HorizontalView = true;
            document.getElementById("Vertical").setAttribute("disabled", "disabled");
          }
          else if (data.isFeatured === false || data.isFeatured === null) {
            this.VerticalView = true;
            document.getElementById("Horizontal").setAttribute("disabled", "disabled");
          }

          //document.getElementById("stateId").setAttribute("bindValue", data.stateId);

          this.tempImageUrl = data.imageUrl;
          debugger
          this.bindStateId = data.stateId;
          this.FormPostBanner.patchValue({
            bannerId: data.bannerId,
            title: data.title,
            email: data.email,

            addressStreet1: data.addressStreet1,
            addressStreet2: data.addressStreet2,
            countryId: data.countryId,
            stateId: data.stateId.toString(),
            cityId: data.cityId.toString(),
            zipCode: data.zipCode,
            postedFor: userdetail.userId,
            postedName: userdetail.firstName + ' ' + userdetail.lastName,
            contactPersonName: data.contactPersonName,
            contactNumber: data.contactNumber,
            countryCodeContact: data.countryCodeContact,
            website: data.website,

            description: data.description,
            alternateContactNumber: data.alternateContactNumber,
            //validTill: this.validTill,
            imageUrl: data.imageUrl,
            faxNumber: data.faxNumber,
            createdBy: this.loggedInUserId,
            bannerImageCount: 0,
            OtherCity: data.otherCity
          });
        });
      });

    }
  }

  // Insert Event
  postEvent(): void {
    debugger
    this.submitted = true;
    // stop the process here if form is invalid
    this.managebanner.CreatedBy = parseInt(this.loggedInUserId);
    if (this.FormPostBanner.invalid) {
      this.toastr.warning("Mandatory fields missing. Please fill all mandatory fields.");
      return;
    }

    if (this.action == "edit") {
      //debugger;
      if (this.previousPlan == undefined) {
        this.remainingBannerImage = 0;
      }

      debugger;
      if (this.extLogos.length > 0) {
        if (this.logoresponse === undefined || this.logoresponse === null) {
          this.logoresponse = this.extLogos.slice();
          this.bannerimage = this.logoresponse[0];
          this.FormPostBanner.value.imageUrl = this.bannerimage;
        }
      }

      if (this.FormPostBanner.value.imageUrl != this.logoresponse) {
        this.FormPostBanner.value.imageUrl = this.logoresponse;

      }

      if (this.FormPostBanner.value.imageUrl == null || this.FormPostBanner.value.imageUrl == undefined) {
        this.toastr.warning("Banner image missing. Please upload an image.");
        return;
      }

      if ((this.FormPostBanner.value.imageUrl != null) && (this.remainingBannerImage == 0)
        && (this.FormPostBanner.value.imageUrl != this.tempImageUrl)) {
        this.toastr.warning('Banner subscription exausted.');
        return;
      }


      console.log(this.FormPostBanner.value);


      this.managebanner.ImageUrl = this.FormPostBanner.value.imageUrl;

      //this.FormPostBanner.value.adLogoUrl = this.logoresponse;

      this.managebanner.bannerId = this.FormPostBanner.value.bannerId;
      this.managebanner.Title = this.FormPostBanner.value.title;
      this.managebanner.Description = this.FormPostBanner.value.description;
      this.managebanner.PostedFor = this.FormPostBanner.value.postedFor;
      this.managebanner.PostedForName = this.FormPostBanner.value.contactPersonName;
      // this.managebanner.ValidTill = this.FormPostBanner.value.validTill;
      this.managebanner.Website = this.FormPostBanner.value.website;

      this.managebanner.AddressStreet1 = this.FormPostBanner.value.addressStreet1;
      this.managebanner.AddressStreet2 = this.FormPostBanner.value.addressStreet2;
      this.managebanner.CountryId = this.FormPostBanner.value.countryId;
      this.managebanner.StateId = this.FormPostBanner.value.stateId;
      this.managebanner.CityId = this.FormPostBanner.value.cityId;


      this.managebanner.ZipCode = this.FormPostBanner.value.zipCode;
      this.managebanner.ContactNumber = this.FormPostBanner.value.contactNumber;
      this.managebanner.ContactPersonName = this.FormPostBanner.value.contactPersonName;
      this.managebanner.AlternateContactNumber = this.FormPostBanner.value.alternateContactNumber;
      this.managebanner.Email = this.FormPostBanner.value.email;
      this.managebanner.CountryCodeContact = this.FormPostBanner.value.countryCodeContact;
      this.managebanner.FaxNumber = this.FormPostBanner.value.faxNumber;
      this.managebanner.OtherCity = this.FormPostBanner.value.OtherCity;

      if ((this.previousPlan != undefined) && (this.FormPostBanner.value.imageUrl != this.tempImageUrl)) {
        this.managebanner.BannerImageCount = this.FormPostBanner.value.bannerImageCount;
        this.managebanner.PlanId = this.previousPlan.planId;
      }


      debugger;
      this.managebannerservice.updateBanner(this.managebanner)
        .pipe(first())
        .subscribe(
          data => {
            if (data.status === 200) {
              console.log(data);
              this.toastr.success('Banner updated successfully.');
              this._router.navigate(['/bumanagebanner']);
            }
            else {
              //alert(data.message);
            }
          },
          error => {
            this.toastr.error(error);
          });
    }

    else if (this.action == "save") {

      debugger;
      this.submitted = true;
      this.FormPostBanner.value.imageUrl = this.logoresponse;

      // stop the process here if form is invalid

      // if (this.FormPostBanner.invalid) {
      //    return;
      // }

      console.log(this.FormPostBanner.value)

      // this.FormPostBanner.value.imageUrl = this.logoresponse;
      if (this.FormPostBanner.value.imageUrl == null || this.FormPostBanner.value.imageUrl == undefined) {
        this.toastr.warning("Banner image missing. Please upload an image.");
        return;
      }
      if (this.FormPostBanner.value.imageUrl != null && this.previousPlan == undefined) {
        this.toastr.warning("please select/buy a subscription plan to post a banner ad.");
        return;
      }
      if (this.FormPostBanner.value.imageUrl != null) {
        this.FormPostBanner.value.BannerImageCount = this.FormPostBanner.value.bannerImageCount;
        this.FormPostBanner.value.PlanId = this.previousPlan.planId;
      }

      this.FormPostBanner.value.bannerId = 0;
      this.FormPostBanner.value.isFeatured = this.IsFeatured;

      this.managebannerservice.add(this.FormPostBanner.value)
        .subscribe((response) => {
          console.log(response);
          this.toastr.success('Banner posted successfully.It will be available on website after the admin approves it.');
          this._router.navigate(['/bumanagebanner']);
        },

          (error) => {
            console.log(error);
            this.statusMessage = "Problem with service. Please try again later!";
          }
        );

    }
  }

  subscriptionPlanDetails(event: any) {
    debugger;
    console.log(event.target.value);

    this.paymentPlanService.getPlanDetails(event.target.value).subscribe(data => {
      var planIdDetails = JSON.parse(data._body);
      console.log(planIdDetails);
      this.previousPlan = planIdDetails;
      this.remainingBannerImage = planIdDetails.bannerImageCount;
      if (this.remainingBannerImage == 0) {
        this.toastr.warning('Banner subscription exausted. Please buy a new subscription or select other subscription plan.');
      }
      this.FormPostBanner.patchValue({
        bannerImageCount: planIdDetails.bannerImageCount
      });
    });
  }

  renderExistingImages(imageUrls, bannerId, uploadType) {
    debugger;
    if (imageUrls !== "") {
      var extImages = imageUrls.split(',');
      if (extImages.length > 0) {
        if (uploadType == "Images") {
          for (let i = 0; i < extImages.length; i++) {
            this.extLogoUrls.push(this.commonimagepath + 'Banners/' + bannerId + '/Image/' + extImages[i]);
            this.extLogos.push(extImages[i]);
            this.displayExtLogo = 'col-md-6';
            this.displayNewLogo = 'col-md-6 d-none';
          }
        }

      }

    }
  }
  onextLogoUrldelete(i) {
    this.extLogoUrls.splice(i, 1);
    this.extLogos.pop();
    this.displayExtLogo = 'col-md-6 d-none';
    this.displayNewLogo = 'col-md-6';
  }
  public uploadFinishedLogo = (event) => {
    this.logoresponse = event;
    //this.displayNewLogo = 'col-md-6 d-none';
  }
  NavigatetoManageEvent() {
    this._router.navigate(['bumanagebanner']);
  }

  //  Dropdowns
  FillUserDDL() {
    //debugger;
    this._allUser = this._commonService.UserDDL();
  }

  FillCountryDDL() {
    this._allCountry = this._commonService.CountryDDL();
    this.CountryId = "1";
    // this._allState = this._commonService.StateDDL("1");
    // this._allCity = this._commonService.BUCityDDL("22");
    this._commonService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      this.states = data;
    });
    this._allState = this._commonService.StateDDL(this.CountryId);
    this._allCity = this._commonService.BUCityDDL(this.StateId);
  }
  

  FillStateDDL(event: any) {

    debugger;
    if ((event.target.value == "0") || ((event.target.value == ""))) {
      this.FormPostBanner.value.countryId = 0;
      //this._allState = this._commonService.StateDDL("0");
      var x = document.getElementsByClassName("ng-value-label");
      x[1].innerHTML = "--Select State--";
      x[2].innerHTML = "--Select City--";
      this._commonService.StateDDL("0").subscribe(data => {
        console.log(data);
        this.states = data;
      });
      this.states = [];
      this.cities = [];
      this.FillCityDDL(0);

      console.log(this.states);
      this.FormPostBanner.value.stateId = 0;
      //this._allCity = this._commonService.CityDDL("0");
      this.FormPostBanner.value.cityId = 0;
    }
    else {
      this.CountryId = event.countryId;

      this._allState = this._commonService.StateDDL("1");
      debugger;
      this._commonService.StateDDL("1").subscribe(data => {
        console.log(data);
        //debugger;
        this.states = data;
      });

      this._commonService.BUCityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
      this._allState = this._commonService.StateDDL(this.CountryId);
      this.CountryName = event.target.options[this.CountryId].innerHTML;
      this.FormPostBanner.countryName = this.CountryName;
    }

  }

  FillCityDDL(event: any) {
    debugger
    if (event != 0) {

      var x = document.getElementsByClassName("ng-value-label");
      //x[1].innerHTML = "--Select State--";
      x[2].innerHTML = "--Select City--";
      this.StateId = event.stateId;
      this.StateName = event.stateName;
      //this.StateName=event.target.options[this.StateId].innerHTML;
      this._allCity = this._commonService.BUCityDDL(this.StateId);
      //this._allCity= this._allCity
      this.FormPostBanner.stateName = this.StateName;


      this._commonService.BUCityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
    }
    else {
      var x = document.getElementsByClassName("ng-value-label");
      this.cities = [];
    }
  }

  FillCityName(event: any) {
    debugger;
    this.CityId = event.cityId;
    this.CityName = event.cityName;

    if (event.cityId === "9999") {
      this.showOtherCity = "col-md-4";
    }
    else
      this.showOtherCity = "d-none";
    //this.FormPostBanner.cityName = this.CityName;
    // this.CityName=event.target.options[this.CityId].innerHTML;
  }

  FillSubscriptionPlan(userId: number) {
    this._allPlan = this.paymentPlanService.UserPlanDDL(userId);
  }

  WebsiteLocation(event: any) {
    debugger;
    if (event.target.value == "Horizontal") {
      this.IsFeatured = true;
    }
    else if (event.target.value == "Vertical") {
      this.IsFeatured = false;
    }

  }

  reset() {
    this.ngOnInit();
    this.UploadCmpt.onimagedelete(0);

  }

  refresh(): void {
    debugger;
    window.location.reload();
    this.UploadCmpt.ngOnInit();

  }

  get f() {
    return this.FormPostBanner.controls;
  }


}
