import { Component, OnInit } from '@angular/core';
import { AdDetails, AdList } from '../../shared/addetails.model';
import { Router, ActivatedRoute } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { WebsiteService } from '../../shared/website.service'
import { environment } from '../../../environments/environment';
import { CommonService } from '../../views/shared/common.service';
import { Common, category, Subcategory } from '../../views/shared/common.model';

@Component({
  selector: 'app-searchresult',
  templateUrl: './searchresult.component.html',
  styleUrls: ['./searchresult.component.scss']
})
export class SearchresultComponent implements OnInit {

  adposts: AdDetails[] = [];
  adpost = new AdDetails();
  Paidadposts: AdDetails[] = [];
  paidadpost = new AdDetails();
  Freeadposts: AdDetails[] = [];
  Freeadpost = new AdDetails();
  Premiumposts: AdDetails[] = [];
  Premiumpost = new AdDetails();
  Navigateurl: string;
  isVisible: boolean;
  FreeAddetails = [];
  PaidAddetails = [];
  PremiumAddetails = [];
  Addetails = [];
  adpostsDetails = [];
  imagepath = environment.imagepath;
  ImageUrl: string;
  Imageurls = [];
  pageTitle: string = "Recent Ads";
  categories: category[] = [];
  category = new category();
  Subcategories: Subcategory[] = [];
  Subcategory = new Subcategory();
  categoryId: string;
  SubcategoryId: string;
  PremiumList = false;
  FreeList = false;
  PaidList = true;
  IsPaidAd = true;
  isChecked = false;
  Showbuttongroup = "";
  premiumTag: string = "ad-block premium";
  PremiumtagList: boolean = true;
  Verifiedtag: boolean = true;
  premiumNoRecords: string = "d-none";
  paidNoRecords: string = "d-none";
  freeNoRecords: string = "d-none";
  constructor(private WebsiteService: WebsiteService, private route: ActivatedRoute, private router: Router, public CommonService: CommonService) { }

  ngOnInit() {
    this.getCategoryList();
    debugger
    if (this.route.snapshot.queryParams['searchCriteria'] != undefined) {
      this.pageTitle = "Search Result";
      this.getAdListBySearchCriteria(this.route.snapshot.queryParams['searchCriteria']);

    }

  }
  getAdListBySearchCriteria(searchCriteria) {
    debugger;
    //  if(searchCriteria.includes('viewmore'))
    //  {
    debugger;
    var searchparam = searchCriteria.split(':');
    searchCriteria = searchparam[0];
    this.WebsiteService.getAdListBySearchCriteria(searchCriteria).subscribe(adposts => {
      this.adposts = adposts;
      if (this.adposts !== null && this.adposts.length > 0) {
        console.log(this.adposts);
        var adlen = this.adposts.length;
        for (let i = 0; i < adlen; i++) {
          if (adposts[i].IsPremium == "True") {
            this.PremiumAddetails.push(this.adposts[i])
          }

          //else if (adposts[i].IsPaidAd == "True" && adposts[i].IsPremium != "True") {
          //  this.PaidAddetails.push(this.adposts[i])
          //}
          else {
            this.FreeAddetails.push(this.adposts[i])
          }
        }
        if (this.PaidAddetails.length > 0) {
          this.Paidadposts = this.PaidAddetails;
        }
        else {
          this.paidNoRecords = "no-record";
          this.Paidadposts = [];
          this.PaidAddetails = [];
        }

        if (this.FreeAddetails.length > 0) {
          this.Freeadposts = this.FreeAddetails;
        }
        else {
          this.freeNoRecords = "no-record";
          this.Freeadposts = [];
          this.FreeAddetails = [];
        }

        if (this.PremiumAddetails.length > 0) {
          this.Premiumposts = this.PremiumAddetails;
        }
        else {
          this.premiumNoRecords = "no-record";
          this.Premiumposts = [];
          this.PremiumAddetails = [];
        }



      }
      else {
        this.paidNoRecords = "no-record";
        this.freeNoRecords = "no-record";
        this.premiumNoRecords = "no-record";
        this.Premiumposts = [];
        this.PremiumAddetails = [];
        this.Freeadposts = [];
        this.FreeAddetails = [];
      }
      this.PaidList = true;
      this.PremiumList = true;
      this.FreeList = true;
      document.getElementById('premium').className = "btn btn-secondary";
      // document.getElementById('free').className = "btn btn-secondary";
      document.getElementById('all').className = "btn btn-secondary active";
    });
    // }   
  }
  getCategoryList(): void {
    debugger;
    this.CommonService.Popularcategories().subscribe(categories => {
      if (categories != null && categories.length > 0)
        this.categories = categories;
      else
        this.categories = [];
      console.log(this.categories);

    });
    debugger;
  }
  getSubCategoryList(categoryId, categoryName) {
    debugger;
    this.CommonService.SubCategoryDDL(categoryId).subscribe(Subcategories => {
      if (Subcategories != null && Subcategories.length > 0)
        this.Subcategories = Subcategories;
      if (Subcategories == null) {
        this.Subcategories = [];
        this.router.navigate(['/ads-categorywise'], { queryParams: { categoryId: categoryId, categoryName: categoryName } });
      }
    });
  }
  btnchanged(evt, action) {
    debugger;
    debugger;
    if (action == "premium") {
      // this.Paidadposts=this.PaidAddetails;
      this.Premiumposts = this.PremiumAddetails;
      this.PaidList = false;
      this.PremiumList = true;
      this.FreeList = false;
      document.getElementById('premium').className = "btn btn-secondary  active";
      //document.getElementById('free').className = "btn btn-secondary";
      document.getElementById('all').className = "btn btn-secondary";
    }
    // else if(action=="free")
    // {
    //   this.Freeadposts=this.FreeAddetails; 
    //   this.PremiumList=false;
    //   this.PaidList=false;
    //   this.FreeList=true;  
    //   document.getElementById('paid').className = "btn btn-secondary";
    //   document.getElementById('free').className = "btn btn-secondary  active";
    //   document.getElementById('all').className = "btn btn-secondary"; 
    // }
    else if (action == "all") {
      this.Paidadposts = this.PaidAddetails;
      this.Premiumposts = this.PremiumAddetails;
      this.Freeadposts = this.FreeAddetails;
      this.PremiumList = true;
      this.PaidList = true;
      this.FreeList = true;
      document.getElementById('premium').className = "btn btn-secondary";
      //document.getElementById('free').className = "btn btn-secondary";
      document.getElementById('all').className = "btn btn-secondary  active";
    }
  }
  getAdListBySubcategoryId(SubcategoryId) {
    debugger;
    this.WebsiteService.getAdListBySubcategoryId(SubcategoryId).subscribe(adposts => {
      if (adposts != null && adposts.length > 0) {

        var adlen = adposts.length;
        for (let i = 0; i < adlen; i++) {
          if (adposts[i].isVisible == true)
            this.Addetails.push(adposts[i])
        }
      }
      else {
        this.Addetails = [];
      }
    });
    console.log(this.Addetails);
    this.adposts = this.Addetails;
  }
  getpremiumtag(IsPremium) {
    debugger;
    if (IsPremium == "True") {
      this.premiumTag = "ad-block premium";
      this.PremiumtagList = true;
      this.Verifiedtag = false;
    }
    else {
      this.premiumTag = "ad-block premium d-none";
      this.PremiumtagList = false;
      this.Verifiedtag = true;
    }
  }

  btnPostNew() {
    debugger;
    console.log(localStorage.getItem('roleName'));
    if (localStorage.getItem('token') === null) {
      this.router.navigate(['/login']);
    }
    else {
      console.log(localStorage.getItem);
      if ((localStorage.getItem('roleName') === 'SuperAdmin') || (localStorage.getItem('roleName') === 'Admin')) {
        this.router.navigate(['/theme/postad']);
      }
      else {
        this.router.navigate(['/bupostad']);
      }

    }
  }
}
