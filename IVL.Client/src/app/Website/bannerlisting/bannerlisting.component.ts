import { Component, OnInit } from '@angular/core';
import { WebsiteService } from '../../shared/website.service';
import { Observable, observable } from 'rxjs';
import { Router } from '@angular/router';
import { Managebanner } from '../../shared/managebanner';

@Component({
  selector: 'app-bannerlisting',
  templateUrl: './bannerlisting.component.html',
  styleUrls: ['./bannerlisting.component.scss']
})
export class BannerlistingComponent implements OnInit {

  premiumbannerposts: Managebanner[]=[];
  premiumbannerpost = new Managebanner();
  bannerposts: Managebanner[]=[];
  bannerpost = new Managebanner();
  Premiumbanners: Managebanner[]=[];
  Premiumbanner = new Managebanner();
  Featuredbanners: Managebanner[]=[];
  Featuredbanner = new Managebanner();
  PremiumBannerDetails = [];
  FeaturedBannerDetails = [];
  PremiumList = false;
  FeaturedList = false;
  premiumNoRecords: string = "d-none";
  featuredNoRecords: string = "d-none";
  constructor(private WebsiteService: WebsiteService, private router: Router) { }

  ngOnInit() {
    this.getPremiumBannerList();
    this.getBannerList();  // for Featured Banner List
    document.getElementById('all').className = 'btn btn-secondary active';
    document.getElementById('premium').className = 'btn btn-secondary';
    document.getElementById('featured').className = 'btn btn-secondary';
  }

  getPremiumBannerList(): void {
    //debugger;
    this.WebsiteService.getPremiumBannerList().subscribe(premiumbannerposts => {
      this.premiumbannerposts = premiumbannerposts;
      if (this.premiumbannerposts !== null && this.premiumbannerposts.length > 0) {
        //console.log(this.premiumbannerposts);
        var adlen = this.premiumbannerposts.length;
        for (let i = 0; i < adlen; i++) {
          this.PremiumBannerDetails.push(this.premiumbannerposts[i]);
        }
        if (this.PremiumBannerDetails.length > 0) {
          this.Premiumbanners = this.PremiumBannerDetails;
        }
        else {
          this.premiumNoRecords = 'no-record';
          this.premiumbannerposts = [];
          this.PremiumBannerDetails = [];
          this.Premiumbanners = [];
        }
      }
      else {
        this.premiumNoRecords = 'no-record';
        this.premiumbannerposts = [];
        this.PremiumBannerDetails = [];
        this.Premiumbanners = [];
      }
      this.PremiumList = true;
      this.FeaturedList = true;
    });
  }

  getBannerList(): void {
    //debugger;
    this.WebsiteService.getFeaturedBannerListing().subscribe(bannerposts => {
      this.bannerposts = bannerposts;
      if (this.bannerposts !== null && this.bannerposts.length > 0) {
        var adlen = this.bannerposts.length;
        for (let i = 0; i < adlen; i++) {
          this.FeaturedBannerDetails.push(this.bannerposts[i]);
        }
        if (this.FeaturedBannerDetails.length > 0) {
          this.Featuredbanners = this.FeaturedBannerDetails;
        }
        else {
          this.featuredNoRecords = 'no-record';
          this.bannerposts = [];
          this.FeaturedBannerDetails = [];
          this.Featuredbanners = [];
        }
      }
      else {
        this.featuredNoRecords = 'no-record';
        this.bannerposts = [];
        this.FeaturedBannerDetails = [];
        this.Featuredbanners = [];
      }
      this.PremiumList = true;
      this.FeaturedList = true;
    });
  }

  btnchanged(evt, action) {
    if (action === 'all') {
      this.Premiumbanners = this.PremiumBannerDetails;
      this.Featuredbanners = this.FeaturedBannerDetails;
      this.PremiumList = true;
      this.FeaturedList = true;
      document.getElementById('all').className = 'btn btn-secondary active';
      document.getElementById('premium').className = 'btn btn-secondary';
      document.getElementById('featured').className = 'btn btn-secondary';
    }
    else if (action === 'premium') {
      this.Premiumbanners = this.PremiumBannerDetails;
      this.PremiumList = true;
      this.FeaturedList = false;
      document.getElementById('premium').className = 'btn btn-secondary  active';
      document.getElementById('featured').className = 'btn btn-secondary';
      document.getElementById('all').className = 'btn btn-secondary';
    }
    else if (action === 'featured')
    {
      this.Featuredbanners = this.FeaturedBannerDetails;
      this.PremiumList = false;
      this.FeaturedList = true;
      document.getElementById('premium').className = 'btn btn-secondary';
      document.getElementById('featured').className = 'btn btn-secondary  active';
      document.getElementById('all').className = 'btn btn-secondary';
    }
  }

  btnPostNew() {
    console.log(localStorage.getItem('roleName'));
    if (localStorage.getItem('token') === null){
      this.router.navigate(['/login']);
    }
    else {
      console.log(localStorage.getItem);
      if ((localStorage.getItem('roleName') === 'SuperAdmin') || (localStorage.getItem('roleName') === 'Admin'))
      {
        this.router.navigate(['/theme/adbanner']);
      }
      else {
        this.router.navigate(['/bupostbanner']);
      }
    }
  }

}
