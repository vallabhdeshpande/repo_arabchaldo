import { Component, OnInit } from '@angular/core';

//import { AdDetailsService } from './addetails.service';
import { AdDetailsService } from '../../views/theme/addetails.service';
import { Router } from "@angular/router";
import { AdDetails, WorkflowstatusDTO, AdStatusDetailsDTO } from '../../views/theme/addetails.model';
import { IfStmt, debugOutputAstAsTypeScript } from '@angular/compiler';
import { OrderPipe } from 'ngx-order-pipe';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr'; 


@Component({
  selector: 'app-bumanagejobs',
  templateUrl: './bumanagejobs.component.html',
  styleUrls: ['./bumanagejobs.component.scss']
})
export class BumanagejobsComponent implements OnInit {

  adposts: AdDetails[];

 // workflowdetails: WorkflowstatusDTO[];
  adStatusdetails: AdStatusDetailsDTO[];
  workflowdetails:string;
  statusMessage: string;
  deleteremark:string;
  activity:boolean;
  visibilitybutton:string;
  visibilityspan:string;
  setStatusActive: string = "";
  setStatus: string = "";
  setStatusInactive: string = "";
  statusText: string = "";
  adpost = new AdDetails();order: string;
  reverse: any ;
modalRemark:string;
  public pageSize: number = 10;
  public p: number;
  remark: string;
  workflowstatusId:number;
  action:string="";
  actionmessage:string="";
  adId=0;
imagepath=environment.imagepath;
loggedInUserId:string;
  adStatus: string;
  filter: string = "";
  // constructor(private AdDetailsService: AdDetailsService, private router: Router) { }
  ngAfterViewInit(){   
  //  document.getElementById('#modalRemarkText').value = this.modalRemark;
     }
  getAdList(): void {
    
    this.AdDetailsService.getJobList().subscribe(adposts => {
      this.adposts = adposts;
      
      console.log(this.adposts);
      // Status
debugger;
      var adlenth = 0;
      if (this.adposts!=null) {
        if (adposts.length > 0) {
          adlenth = adposts.length;
          for (let i = 0; i < adlenth; i++) {

            if (adposts[i].isVisible == true) {
              adposts[i].isVisible = "Active";
              adposts[i].Activity = true;
              this.visibilitybutton = "hide";
              this.visibilityspan = "show";

              //  this.setStatus="badge badge-success";
              // this.setStatusInactive ="badge badge-secondary d-none";
            }
            else if (adposts[i].isVisible == false) {
              adposts[i].isVisible = "InActive";
              adposts[i].Activity = true;
              this.visibilitybutton = "hide";
              this.visibilityspan = "show";

              // this.setStatus ="badge badge-secondary";
              // this.setStatusInactive ="badge badge-secondary";
            }
            else if (adposts[i].isVisible == null) {
              adposts[i].isVisible = "Pending";
              adposts[i].Activity = false;
              this.visibilitybutton = "show";
              this.visibilityspan = "hide";
              // this.setStatus = "badge badge-warning";
            }
            if (adposts[i].status == "") {
              adposts[i].status = "Pending";
            }

          }

          this.adposts = adposts;
        }
        else
          this.adposts = [];
      }
      else
        this.adposts = [];
   
    });
  }

  getPicUrl(picurl:string,adId)
  {
//debugger;
    if(picurl==null)
    return "assets/img/default/no_image_placeholder.jpg";
    else if (picurl=="")
    return "assets/img/default/no_image_placeholder.jpg";
    else     
    return this.imagepath+'Ads/'+adId+'/Logo/'+picurl;

  }
 
// openModal()
// {
//  // debugger;
//   //#modalRemarkText
//   let modalRemarkText = (<HTMLInputElement>document.getElementById('modalRemarkText')).value;
//   if(modalRemarkText!="")
//   modalRemarkText="";
//   return "modal";
  
// }


  getStatus(Status: string) {
    debugger;
    if (Status == "Active") {
      this.statusText = "Active";
      return this.setStatus = "badge badge-success";
    }


    if (Status == "InActive") {
      this.statusText = "InActive";
      return this.setStatus = "badge badge-danger";
    }


    if (Status == "Pending") {
      this.statusText = "Pending";
      return this.setStatus = "badge badge-warning";
    }


  } 
  
  updatePost(adpost:AdDetails): void {
   // debugger;
    this.router.navigate(['/bupostad'], { queryParams: { adId: adpost.adId, Job: "Jobs" } });
    };
    gotoAdDetails(adpost:AdDetails): void {
      debugger
      this.router.navigate(['/theme/addetailsview'],{queryParams:{adId:adpost.adId}});
      };
  
  sortedCollection: any[];

  constructor(private toastr: ToastrService,private orderPipe: OrderPipe, private AdDetailsService: AdDetailsService, private router: Router) {
    this.sortedCollection = orderPipe.transform(this.adposts, 'firstName');
    console.log(this.sortedCollection);
  }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }
  ActionOnApprove(adId,event)
  {
   
debugger;
//$("#modalRemarkText").value="";
// (<HTMLInputElement>document.getElementById('remark')).value="";
this.remark=null;
    this.adId=adId;
    this.action="Approve";
    this.actionmessage="Are you sure to approve this record?";
  }
  ActionOnReject(adId,event)
  {
    this.remark=null;
    this.adId=adId;
    this.action="Reject";
    this.actionmessage="Are you sure to reject this record?";
  }
  ActionOnPost(remark) {
debugger;
    this.workflowdetails=this.adId+","+remark+","+this.action;
    

    if (confirm(this.actionmessage)) {
      this.AdDetailsService.updatewithWorkflow(this.workflowdetails).subscribe(response => {
        console.log(response);
        console.log('Updated');
        this.getAdList();
      },

        (error) => {
          console.log(error);

          this.toastr.error('Problem with service. Please try again later!');
        });
    }
    //this.router.navigate(['/theme/postad'],{queryParams:{adId:adpost.adId}});
  }
  DeleteAd(adId :number,adStatus :string,event) {
    this.adId=adId;
    this.adStatus=adStatus;
    this.actionmessage="Are you sure to delete this job?"
  }

  ActionDelete()
  {
   // debugger;
    if(this.adStatus!=="Active")
  {
    this.action="Delete";
    this.deleteremark="";
    this.workflowdetails=this.adId+","+this.loggedInUserId+","+this.action+","+this.deleteremark;
    //this.workflowdetails=this.adId+","+this.deleteremark+","+this.action;
        this.AdDetailsService.updatewithWorkflow(this.workflowdetails).subscribe(response => {
          this.toastr.success('Job deleted successfully.');
          this.getAdList();
        },

          (error) => {
            console.log(error);

            this.statusMessage = "Problem with service. Please try again later!";
          });      
    }
      else{
        this.toastr.info('This is active ad, can not be deleted.');

      }
  }

  NavigateToPostJob()
  {
    debugger
    this.router.navigate(['/bupostad'],{ queryParams: { Job:"Jobs" } });
  } 


  ngOnInit() {
	  this.loggedInUserId=localStorage.getItem('userId');
	  this.getAdList();
  }
  adDetailsView(adId: number) {

    this.router.navigate(['/buaddetailsview'], { queryParams: { adId: adId, Job: 'Jobs' } });
  }
}

