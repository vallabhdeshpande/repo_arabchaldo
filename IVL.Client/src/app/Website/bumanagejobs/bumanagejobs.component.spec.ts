import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BumanagejobsComponent } from './bumanagejobs.component';

describe('BumanagejobsComponent', () => {
  let component: BumanagejobsComponent;
  let fixture: ComponentFixture<BumanagejobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BumanagejobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BumanagejobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
