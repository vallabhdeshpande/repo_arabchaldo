import { Component, OnInit, ViewChild } from '@angular/core';
import { ContentComponent } from '../Website/content/content.component';
import { BannerComponent } from '../Website/banner/banner.component';

@Component({
  selector: 'app-website',
  templateUrl: './website.component.html',
  styleUrls: ['./website.component.scss']
})
export class WebsiteComponent implements OnInit {
  @ViewChild(ContentComponent, { static: false }) contentCmpt: ContentComponent;
  @ViewChild(BannerComponent, { static: false }) bnrCmpt: BannerComponent;

  
  constructor() { }

  ngOnInit() {
  }

  public convertContent = (event) => {
    debugger;
    this.contentCmpt.changeCategoryContent();
    this.bnrCmpt.changeData(); 
  }
}
