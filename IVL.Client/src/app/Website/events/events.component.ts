import { Component, OnInit } from '@angular/core';
import { EventDetails } from '../../shared/eventdetails.model';
import { WebsiteService } from '../../shared/website.service'
import { environment } from '../../../environments/environment';
import { CommonService } from '../../views/shared/common.service';
import { Common, category, Subcategory } from '../../views/shared/common.model';
import { Observable } from 'rxjs/Observable';
import { CLASS_NAME } from 'ngx-bootstrap/modal/modal-options.class';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  eventposts: EventDetails[]=[];
  eventpost = new EventDetails();
  EventDetails = [];
  Premiumadposts: EventDetails[]=[];
  imagepath = environment.imagepath;
  EventId = 0;
  PremiumList = false;
  FreeList = false;
  Freeposts: EventDetails[]=[];
  Freepost = new EventDetails();
  categories: category[]=[];
  //PremiumList = true;
  category = new category();
  categoryId = "";
  showCatEng: string = "";
  showCatArb: string = "d-none";
  eventsNoRecords: string = "d-none";
  eventsfreeRecords: string = "d-none";
  Premiumposts: EventDetails[]=[];
  premiumTag: string = "ad-block premium";
  PremiumtagList: boolean = true;
  Verifiedtag: boolean = true;
  FreeEventdetails = [];
  PremiumEventdetails = [];
  PaidList = true;
  isPaidAd = true;
  style: any;
  constructor(private WebsiteService: WebsiteService, public CommonService: CommonService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getCategoryList();
    this.getEventsbycategory(this.categoryId);
    this.changeCategoryContent();

  }

  changeCategoryContent() {

    if (localStorage.getItem('language') !== null) {
      if (localStorage.getItem('language') === "English") {
        this.showCatEng = "";
        this.showCatArb = "d-none";
      }
      else {
        this.showCatEng = "d-none";
        this.showCatArb = "";
      }

    }
  }

  getEventsbycategory(categoryId): void {
    debugger;
    if (categoryId == "")
      categoryId = "0";
    this.categoryId = categoryId;

    this.WebsiteService.getEventsBycategoryId(categoryId).subscribe(eventposts => {
      console.log(eventposts);
      if (eventposts != null && eventposts.length>0) {


        this.eventposts = eventposts;
        var adlen = this.eventposts.length;
        console.log(this.eventposts);
        this.PremiumEventdetails = [];
        this.FreeEventdetails = [];
        for (let i = 0; i < adlen; i++) {
          if (eventposts[i].isPaidAd == true || eventposts[i].isPaidAd == "True") {
            this.PremiumEventdetails.push(this.eventposts[i])
          }
          else {
            this.FreeEventdetails.push(this.eventposts[i])
          }
        }
        this.Premiumposts = this.PremiumEventdetails;
        this.Freeposts = this.FreeEventdetails;
      }
      else {
        this.eventposts = [];
        this.PremiumEventdetails = [];
        this.FreeEventdetails = [];
        this.Premiumposts = [];
        this.Freeposts = [];
      }

      console.log(this.PremiumEventdetails);
      console.log(this.FreeEventdetails);

      if (this.PremiumList === true && this.FreeList === false) {
        this.PremiumList = true;
        this.FreeList = false;

        document.getElementById('premium').className = "btn btn-secondary active";
        document.getElementById('free').className = "btn btn-secondary";
      }
      else
      {
        this.PremiumList = true;
        this.FreeList = true;

        document.getElementById('premium').className = "btn btn-secondary";
        document.getElementById('free').className = "btn btn-secondary active";

      }
      debugger;
      // if (this.eventposts !== null && this.eventposts.length > 0) {

      //  this.eventsNoRecords = "d-none";
      //}
      //else
      //  this.eventsNoRecords = "no-record";

      if (this.PremiumEventdetails.length > 0) {

        this.eventsNoRecords = "d-none";
      }
      else {
        this.eventsNoRecords = "no-record";
      }


      if (this.FreeEventdetails.length > 0) {

        this.eventsfreeRecords = "d-none";
      }
      else {
        this.eventsfreeRecords = "no-record";
      }
      console.log(eventposts);
    });

    document.getElementById("0").classList.remove('active');
    //this.categories.forEach(function (item) {

    //  document.getElementById(item.CategoryId).classList.remove('active');
    //});
    if (categoryId <= 0) {
      document.getElementById(categoryId).className = "active";
    }
    else {
      document.getElementById(categoryId).classList.remove('active');
    }
  }

  btnchanged(evt, action) {
    debugger;
    if (action == "premium") {
      this.Premiumposts = this.Premiumposts;

      this.PremiumList = true;
      this.FreeList = false;
      document.getElementById('premium').className = "btn btn-secondary active";
      document.getElementById('free').className = "btn btn-secondary ";
   
    }

    else if (action == "free") {

      this.Premiumposts = this.Premiumposts;
      this.Freeposts = this.Freeposts;
      this.PremiumList = true;
      this.FreeList = true;

      document.getElementById('premium').className = "btn btn-secondary ";
      document.getElementById('free').className = "btn btn-secondary active ";
    }
  }

  getpremiumtag(isPaidAd) {
    //debugger;
    if (isPaidAd == true || isPaidAd == "True") {
      this.premiumTag = "ad-block premium";
      this.PremiumtagList = true;
      this.Verifiedtag = false;
    }
    else {
      this.premiumTag = "ad-block premium d-none";
      this.PremiumtagList = false;
      this.Verifiedtag = true;
    }


  }
  
  getPicUrl(picurl: string, eventId) {
    if (picurl == null)
      return "assets/img/default/no_image_placeholder.jpg";
    else if (picurl == "")
      return "assets/img/default/no_image_placeholder.jpg";
    else
      return this.imagepath + 'Events/' + eventId + '/Image/' + picurl;
  }

  getCategoryList(): void {
    debugger;
    this.CommonService.PopularEventcategories().subscribe(categories => {
      if (categories != null && categories.length > 0) {


        this.categories = categories;
        console.log(this.categories);
        if (this.categoryId == null)
          this.categoryId = categories[0].CategoryId;

        if (this.categoryId != null) {
          //document.getElementById('all').style.background = "-webkit-gradient(linear, left top, left bottom, from(#ffcd00), to(#006a4e))";
          document.getElementById(this.categoryId).className = "active";
        }
      }

    });

  }

  btnPostNew() {
    console.log(localStorage.getItem('roleName'));
    if (localStorage.getItem('token') === null){
      this.router.navigate(['/login']);
    }
    else {
      console.log(localStorage.getItem);
      if ((localStorage.getItem('roleName') === 'SuperAdmin') || (localStorage.getItem('roleName') === 'Admin'))
      {
        this.router.navigate(['/theme/postevent']);
      }
      else {
        this.router.navigate(['/bupostevent']);
      }
    }
  }
}
