import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { WebsiteService} from '../../shared/website.service';
import { FormBuilder, Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { HttpEventType, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-advertiseus',
  templateUrl: './advertiseus.component.html',
  styleUrls: ['./advertiseus.component.scss']
})
export class AdvertiseusComponent implements OnInit {

  ContactForm: any;
  submitted = false;
  mailfor:string  = "Contact Us";
  ContactUsData: string="";
  ContactUsArabicData: string = "";
  showEng: string = "";
  showArb: string = "d-none";

  constructor(private toastr: ToastrService, private formbulider: FormBuilder, private WebsiteService: WebsiteService, private route: ActivatedRoute, private router: Router) { }


  ngOnInit() {
    this.ContactForm = this.formbulider.group({
      mailfor: ['', ''],
      Name: ['', Validators.required],
      Email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      Comments: ['', Validators.required],
      Phone: ['', [Validators.required, Validators.minLength(10)]],
      reason: ['', Validators.required]
    });  
    this.GetContactUSData();
    this.changeContactUSData();
  }

  changeContactUSData() {

    if (localStorage.getItem('language') !== null) {
      if (localStorage.getItem('language') === "English") {
        this.showEng = "";
        this.showArb = "d-none";
      }
      else {
        this.showEng = "d-none";
        this.showArb = "";
      }

    }
  }

  GetContactUSData()
  {
    debugger;
   this.WebsiteService.GetContactUSData().subscribe(data => {
     console.log(data);
     this.ContactUsArabicData = data[0].ContactUsArabic;
     this.ContactUsData = data[0].ContactUs;
     if (localStorage.getItem('language') !== null) {
       if (localStorage.getItem('language') === "English") {
        
         this.ContactUsData = data[0].ContactUs;
       }
       else {
         this.ContactUsArabicData = data[0].ContactUsArabic;
       }

     }
     else 
       this.ContactUsData = data[0].ContactUs;
    
   });
  }

  Submit(): void 
  {

 debugger;  
this.submitted = true;

 // stop the process here if form is invalid
  if (this.ContactForm.invalid) {
 	//debugger
	return;
  }
debugger
   console.log(this.ContactForm.value)  
  debugger;  
   this.ContactForm.value.Name = this.ContactForm.value.Name;
   this.ContactForm.value.Phone = this.ContactForm.value.Phone;
   this.ContactForm.value.Email= this.ContactForm.value.Email;
   this.ContactForm.value.Comments = this.ContactForm.value.Comments;
   this.ContactForm.value.reason = this.ContactForm.value.reason;
   this.ContactForm.value.mailfor = this.mailfor;


 console.log(this.ContactForm.value)
 this.WebsiteService.contact(this.ContactForm.value)
 .subscribe((response) => {
  console.log(response);
   this.toastr.success('Contact Us form submitted successfully.');
  this.ContactForm.reset();
  this.submitted = false;
 },
 (error) => {
  console.log(error);
  this.toastr.error('Problem with service. Please try again later!');
}

);
}

}
