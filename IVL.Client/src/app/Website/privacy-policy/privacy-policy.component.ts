import { Component, OnInit } from '@angular/core';
import { WebsiteService} from '../../shared/website.service';
import { HttpEventType, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss']
})
export class PrivacyPolicyComponent implements OnInit {
  privacypolicydata: string="";
  privacypolicyArabicdata: string = "";
  showEng: string = "";
  showArb: string = "d-none";
  constructor(private WebsiteService: WebsiteService, private http: HttpClient) { }

  ngOnInit() {
    this.GetPrivacyPolicyData();
    this.changePrivacyPolicyData();
  }

  changePrivacyPolicyData() {

    if (localStorage.getItem('language') !== null) {
      if (localStorage.getItem('language') === "English") {
        this.showEng = "";
        this.showArb = "d-none";
      }
      else {
        this.showEng = "d-none";
        this.showArb = "";
      }

    }
  }
  GetPrivacyPolicyData()
  {
    debugger;
    this.WebsiteService.GetPrivacyPolicyData().subscribe(data => {
      if (data != null && data.length>0) {


        console.log(data);
        this.privacypolicydata = data[0].PrivacyPolicy;
        this.privacypolicydata = data[0].PrivacyPolicyArabic;
        if (localStorage.getItem('language') !== null) {
          if (localStorage.getItem('language') === "English") {


            this.privacypolicydata = data[0].PrivacyPolicy;
          }
          else {
            this.privacypolicydata = data[0].PrivacyPolicyArabic;

          }

        }
        else
          this.privacypolicydata = data[0].PrivacyPolicy;
      }

   });

  }

}
