import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuresetpasswordComponent } from './buresetpassword.component';

describe('BuresetpasswordComponent', () => {
  let component: BuresetpasswordComponent;
  let fixture: ComponentFixture<BuresetpasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuresetpasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuresetpasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
