import { Component, OnInit } from '@angular/core';

//import { AdDetailsService } from './addetails.service';
import { BuysellService } from '../../views/theme/buysell.service';
import { Router } from "@angular/router";
import { Buysell} from '../../views/theme/buysell.model';
import { IfStmt, debugOutputAstAsTypeScript } from '@angular/compiler';
import { OrderPipe } from 'ngx-order-pipe';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr'; 


@Component({
  selector: 'app-bumanagebuysell',
  templateUrl: './bumanagebuysell.component.html',
  styleUrls: ['./bumanagebuysell.component.scss']
})
export class BumanagebuysellComponent implements OnInit {

  buysellposts: Buysell[];

 // workflowdetails: WorkflowstatusDTO[];
 // adStatusdetails: AdStatusDetailsDTO[];
  workflowdetails:string;
  statusMessage: string;
  deleteremark:string;
  activity:boolean;
  visibilitybutton:string;
  visibilityspan:string;
  setStatusActive: string = "";
  setStatus: string = "";
  setStatusInactive:string="";
  buysellpost = new Buysell();order: string;
  reverse: any ;
  modalRemark:string;
  public pageSize: number = 10;
  public p: number;
  remark: string;
  workflowstatusId:number;
  action:string="";
  actionmessage:string="";
  buysellId=0;
 imagepath=environment.imagepath;
 loggedInUserId:string;
 adStatus:string;
  setType: string;
  statusText: string = "";
  filter: string = "";
  // constructor(private AdDetailsService: AdDetailsService, private router: Router) { }
  ngAfterViewInit(){   
  //  document.getElementById('#modalRemarkText').value = this.modalRemark;
     }
     getBuySellList(): void {
    
    this._buysellService.getBuySellList().subscribe(buysellposts => {
      this.buysellposts = buysellposts;
      
      console.log(this.buysellposts);
      // Status
debugger;
      // var adlenth=buysellposts.length;    
      //  this.buysellposts = buysellposts;
       var adlenth = 0;
       if (this.buysellposts != null) {
         adlenth = buysellposts.length;
         if (buysellposts.length > 0) {

           
           for (var i = 0; i < this.buysellposts.length; i++) {
             if (buysellposts[i].status == "") {
               buysellposts[i].status = "Pending";
             }
           }
           this.buysellposts = buysellposts;
         }
         else
           this.buysellposts = [];
       }
       else
         this.buysellposts = [];
 
   
   
    });
  }

  getPicUrl(picurl:string,buysellId)
  {
//debugger;
    if(picurl==null)
    return "assets/img/default/no_image_placeholder.jpg";
    else if (picurl=="")
    return "assets/img/default/no_image_placeholder.jpg";
    else     
    return this.imagepath+'BuySell/'+buysellId+'/Image/'+picurl;

  }
 
// openModal()
// {
//  // debugger;
//   //#modalRemarkText
//   let modalRemarkText = (<HTMLInputElement>document.getElementById('modalRemarkText')).value;
//   if(modalRemarkText!="")
//   modalRemarkText="";
//   return "modal";
  
// }
  hideActivity(Status: string) {

    if (Status == "Active")
      return "d-none";

    if (Status == "InActive")
      return "d-none";

    if (Status == "Pending")
      return "";

  }


    getStatus(Status: string) {  
     // debugger;
      if (Status == "Active") {
        this.statusText = "Active";
        return this.setStatus = "badge badge-success";
      }
        

      if (Status == "InActive") {
        this.statusText = "InActive";
        return this.setStatus = "badge badge-danger";
      }
          

      if (Status == "Pending") {
        this.statusText = "Pending";
        return this.setStatus = "badge badge-warning";
      }
               

  } 
  getType(IsFeatured) {  
  //  debugger;
    if(IsFeatured==="Premium")
    return this.setStatus="badge badge-warning";
    else
    return this.setStatus=" "; 
} 
  updatePost(buysellpost:Buysell): void {
   // debugger;
    this.router.navigate(['/buaddbuysell'],{queryParams:{buysellId:buysellpost.BuySellId}});
    };
    gotoAdDetails(buysellpost:Buysell): void {
      debugger
      this.router.navigate(['/theme/bubuyselldetailsview'],{queryParams:{BuySellId:buysellpost.BuySellId}});
      };
    NavigateToAddPost( ): void {
      debugger
       this.router.navigate(['/bupostad']);
       };
       buysellDetailsView(buysellId ): void {
        debugger
         this.router.navigate
         (['/bubuyselldetailsview'], { queryParams: { buysellId: buysellId } });
         };
  sortedCollection: any[];

  constructor(private toastr: ToastrService,private orderPipe: OrderPipe, private _buysellService: BuysellService, private router: Router) {
    this.sortedCollection = orderPipe.transform(this.buysellposts, 'firstName');
    console.log(this.sortedCollection);
  }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }
  ActionOnApprove(buysellId,event)
  {
   
debugger;
//$("#modalRemarkText").value="";
// (<HTMLInputElement>document.getElementById('remark')).value="";
this.remark=null;
    this.buysellId=buysellId;
    this.action="Approve";
    this.actionmessage="Are you sure to approve this record?";
  }
  ActionOnReject(buysellId,event)
  {
    this.remark=null;
    this.buysellId=buysellId;
    this.action="Reject";
    this.actionmessage="Are you sure to reject this record?";
  }
  ActionOnPost(remark) {
debugger;
    this.workflowdetails=this.buysellId+","+remark+","+this.action;
    

    if (confirm(this.actionmessage)) {
      this._buysellService.updatewithWorkflow(this.workflowdetails).subscribe(response => {
        console.log(response);
        console.log('Updated');
        this.getBuySellList();
      },

        (error) => {
          console.log(error);

          this.toastr.error('Problem with service. Please try again later!');
        });
    }
    //this.router.navigate(['/theme/postad'],{queryParams:{adId:adpost.adId}});
  }
  DeleteAd(buysellId :number,adStatus :string): void {
    this.buysellId=buysellId;
    this.adStatus=adStatus;
    this.actionmessage="Are you sure to delete this buy/sell listing?"


  }
  ActionDelete()
  {
   debugger;
    if(this.adStatus!=="True")
  {
    this.action="Delete";
    this.deleteremark="";
    this.workflowdetails=this.buysellId+","+this.loggedInUserId+","+this.action+","+this.deleteremark;
    //this.workflowdetails=this.adId+","+this.deleteremark+","+this.action;
        this._buysellService.updatewithWorkflow(this.workflowdetails).subscribe(response => {
          this.toastr.success('Buy/sell deleted successfully.');
          this.getBuySellList();
        },

          (error) => {
            console.log(error);

            this.statusMessage = "Problem with service. Please try again later!";
          });      
    }
      else{
        this.toastr.info('This is active buy/sell listing, can not be deleted.');

      }
  }




  ngOnInit() {
	  this.loggedInUserId=localStorage.getItem('userId');
	  this.getBuySellList();
  }  
}

