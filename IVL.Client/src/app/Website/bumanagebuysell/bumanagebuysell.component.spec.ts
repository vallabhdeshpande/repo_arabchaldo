import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BumanagebuysellComponent } from './bumanagebuysell.component';

describe('BumanagebuysellComponent', () => {
  let component: BumanagebuysellComponent;
  let fixture: ComponentFixture<BumanagebuysellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BumanagebuysellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BumanagebuysellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
