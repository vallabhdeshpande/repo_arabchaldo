import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService } from '../../views/theme/user.service'
import { User } from '../../views/theme/user';
import {first} from "rxjs/operators";
import {Router, ActivatedRoute} from "@angular/router";
import { Observable } from 'rxjs';
import { City } from '../../views/theme/city.model';
import { State } from '../../views/theme/state.model';
import { Country } from '../../views/theme/country.model';
import { HttpClient, HttpHeaders,HttpClientModule } from "@angular/common/http";
import { AdDetailsService } from '../../views/theme/addetails.service';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { MainheadComponent } from '../mainhead/mainhead.component';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { CommonService } from '../../views/shared/common.service';

@Component({
  selector: 'app-buprofile',
  templateUrl: './buprofile.component.html',
  styleUrls: ['./buprofile.component.scss']
})
export class BuprofileComponent implements OnInit {
  UserId: string = "0";
  CountryId: string = "0";
  StateId: string = "0";
  CityId: string = "0";
  CategoryId: string = "0";
  SubCategory: string = "0";
  //roleId: string="0";
  editBUProfile: any;
  user: User;
  isLoadingResults = false;
  state: any;
  public _allUserRole: Observable<User[]>;
  public _allCountry: Observable<Country[]>;
  public _allState: Observable<State[]>;
  public _allCity: Observable<City[]>;
  public _allStateNew: Observable<State[]>;
  submitted = false;

  displayText = 'btn btn-sm btn-primary';
  displayExtLogo = 'col-md-6 d-none';
  displayNewLogo = 'col-md-6';
  displayExtImages = 'col-md-6 d-none';
  displayExtUploads = 'form-bg-section d-none';
  maxDate: string;
  validTill: string;
  StateName: string = "";
  CityName: string = "";
  CountryName: string = "";
  extLogoUrls = [];
  extLogos = [];
  states: any[];
  cities: any[];
  countries: any[];
  logopath: string[];
  //imagepath: any[];
  imagepath = environment.imagepath;
  logoUrl: string = "";
  imageUrl: string = "";
  public logoresponse: string[];
  serverpath = environment.serverpath;
  commonimagepath = environment.imagepath;
  @ViewChild(MainheadComponent, { static: false }) updateprofilepic: MainheadComponent;
  // constructor() { }
  constructor(private toastr: ToastrService, private formBuilder: FormBuilder, private _commonService: CommonService, private router: Router, private AdDetailsService: AdDetailsService, private route: ActivatedRoute, private userService: UserService, private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    debugger;
    this.FillCountryDDL();
    //this.FillStateDDL();
    //this.FillCityDDL();
    //this. FillUserRoleDDL();

    this.editBUProfile = this.formBuilder.group(
      {
        'userId': ['', Validators.required],
        'firstName': ['', Validators.required],
        'lastName': ['', Validators.required],
        'gender': ['', Validators.required],
        //'roleId': ['', Validators.required],
        //'roleName': ['', Validators.required],
        'profilePicUrl': [''],
        'contactNumber': ['', [Validators.required, Validators.minLength(10)]],
        'alternateContactNumber': [''],
        'email': ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
        'faxNumber': [''],
        'addressStreet1': ['', Validators.pattern(".*\\S.*[a-zA-z0-9 ]")],
        'addressStreet2': ['', Validators.pattern(".*\\S.*[a-zA-z0-9 ]")],
        'countryId': ['', ''],
        'stateId': ['', ''],
        'cityId': ['', ''],
        'zipCode': ['', Validators.pattern("^[0-9]*$")]
      }
    );


    debugger;
    console.log(localStorage);
    this.getUserDetails();
  }

  get f() {
    return this.editBUProfile.controls;
  }


  getUserDetails() {

    this.userService.getUserById(localStorage.userId).subscribe(data => {
      var data = JSON.parse(data._body);
      console.log(data);
      debugger;
      this.AdDetailsService.CountryDDL().subscribe(data => {
        console.log(data);
        //   //debugger;
        this.countries = data;
      });

      this.AdDetailsService.CityDDL(data.stateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });

      this.AdDetailsService.StateDDL(data.countryId).subscribe(data => {
        console.log(data);
        //debugger;
        this.states = data;
      });

      this._allState = this.AdDetailsService.StateDDL(data.countryId);
      this._allCity = this.AdDetailsService.CityDDL(data.stateId);
      var genderreceived;


      this.displayExtUploads = 'form-bg-section';
      if (data.profilePicUrl !== null)
        this.renderExistingImages(data.profilePicUrl, data.userId, "Logo");
      else
        this.displayExtLogo = 'col-md-6 d-none';

      if (data.imageUrl === null && data.profilePicUrl === null)
        this.displayExtUploads = 'form-bg-section d-none';


      if (data.gender == 1)
        genderreceived = "Male";
      else
        genderreceived = "Female";
      //debugger;

      this.editBUProfile.patchValue({
        'userId': data.userId,
        'firstName': data.firstName,
        'lastName': data.lastName,
        'gender': genderreceived,
        // 'roleId': data.roleId,
        //'roleName': data.roleName,
        //'profilePicUrl': data.profilePicUrl,
        'contactNumber': data.contactNumber,
        'alternateContactNumber': data.alternateContactNumber,
        'email': data.email,
        'faxNumber': data.faxNumber,
        'addressStreet1': data.addressStreet1,
        'addressStreet2': data.addressStreet2,
        'countryId': data.countryId.toString(),
        'stateId': data.stateId.toString(),
        'cityId': data.cityId.toString(),
        'zipCode': data.zipCode

      });
    });
  }

  onSubmit() {
    debugger;
    this.submitted = true;
    // stop here if form is invalid
    if (this.editBUProfile.invalid) {
      this.toastr.warning("Mandatory fields missing. Please fill all mandatory fields.");
      return;
    }

    if (this.extLogos.length > 0) {
      //this.logoresponse.pop();
      this.logoresponse = this.extLogos[0];
      // this.editBUProfile.value.profilePicUrl = this.logoresponse;
    }
    if (this.logoresponse != undefined || this.logoresponse !=null)
      this.editBUProfile.value.profilePicUrl = this.logoresponse;
    //else if (this.logoresponse !== null)
    //  this.editBUProfile.value.profilePicUrl = this.logoresponse;
    else
      this.editBUProfile.value.profilePicUrl = "";
    console.log(this.editBUProfile.value);
    if (this.editBUProfile.value.gender == "Male")
      this.editBUProfile.value.gender = 1;
    else
      this.editBUProfile.value.gender = 2;
    if (this.editBUProfile.value.countryId == "") {
      this.editBUProfile.value.countryId = 0;
      this.editBUProfile.value.stateId = 0;
      this.editBUProfile.value.cityId = 0;
    }
    this.userService.updateUser(this.editBUProfile.value)
      .pipe(first())
      .subscribe(
        data => {
          if (data.status === 200) {
            console.log(data);
            localStorage.setItem('firstName', this.editBUProfile.value.firstName);
            localStorage.setItem('lastName', this.editBUProfile.value.lastName);
            if (this.editBUProfile.value.profilePicUrl != "")
              localStorage.setItem('profilePic', this.editBUProfile.value.profilePicUrl);
            else
              localStorage.setItem('profilePic', "");

            this.updateprofilepic.ngOnInit();


            this.toastr.success('Profile Updated Successfully.');
            this.router.navigate(['/theme/manageuser']);
          }
          else {
            this.toastr.info(data.message);
          }
        },
        error => {
          this.toastr.error(error);
        });
  }

  public uploadFinishedLogo = (event) => {
    debugger;
    this.logoresponse = event;
  }

  renderExistingImages(imageUrls, userId, uploadType) {
    debugger;
    if (imageUrls !== "") {
      debugger
      var extImages = imageUrls.split(',');
      if (extImages.length > 0) {
        if (uploadType == "Logo") {
          for (let i = 0; i < extImages.length; i++) {
            this.extLogoUrls.push(this.commonimagepath + 'UserProfile/' + userId + '/ProfilePic/' + extImages[i]);
            this.extLogos.push(extImages[i]);
            this.displayExtLogo = 'col-md-6';
            this.displayNewLogo = 'col-md-6 d-none';
          }
        }
      }
    }
  }

  onextLogoUrldelete(i) {
    this.extLogoUrls.splice(i, 1);
    this.extLogos.pop();
    this.displayExtLogo = 'col-md-6 d-none';
    this.displayNewLogo = 'col-md-6';
  }
  Navigatetoprofile() {
    this.router.navigate(['/budashboard']);
  }
  // FillCountryDDL()
  // {  
  //   this._allCountry=this.AdDetailsService.CountryDDL();
  //   this.AdDetailsService.CountryDDL().subscribe(data => {
  //     console.log(data);
  //     //   //debugger;
  //     this.countries = data;
  //   });
  // }
  // FillStateDDL(event: any)
  // {

  //   this.CountryId = event.countryId;
  //   this.CountryName = event.countryName;

  //   this._allState = this.AdDetailsService.StateDDL(this.CountryId);

  //   this.AdDetailsService.StateDDL(this.CountryId).subscribe(data => {
  //     console.log(data);
  //     //debugger;
  //     this.states = data;
  //   });

  // }
  // FillCityDDL(event: any)
  // {
  //   this.StateId = event.stateId;
  //   this.StateName = event.stateName;
  //   this._allCity = this.AdDetailsService.CityDDL(this.StateId);

  //   this.AdDetailsService.CityDDL(this.StateId).subscribe(data => {
  //     console.log(data);
  //     debugger;
  //     this.cities = data;
  //   });
  // }
  FillCountryDDL() {
    this._allCountry = this.AdDetailsService.CountryDDL();
    //this._allCountry = this._commonService.CountryDDL();
    this.CountryId = "1";

    this._commonService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      this.states = data;
    });
    this._allState = this.AdDetailsService.StateDDL(this.CountryId);
    this._allCity = this.AdDetailsService.CityDDL(this.StateId);
  }

  FillStateDDL(event: any) {

    debugger;
    if ((event.target.value == "0") || ((event.target.value == ""))) {
      this.editBUProfile.value.countryId = 0;
      //this._allState = this._commonService.StateDDL("0");
      var x = document.getElementsByClassName("ng-value-label");
      x[0].innerHTML = "--Select State--";
      x[1].innerHTML = "--Select City--";
      //var i;
      //for (i = 3; i < x.length; i++) {
      //  x[i].innerHTML = "";
      //}
      this.states = [];
      this.cities = [];
      this.FillCityDDL(0);
      console.log(this.states);
      this.editBUProfile.value.stateId = 0;
      //this._allCity = this._commonService.CityDDL("0");
      this.editBUProfile.value.cityId = 0;
    }
    else {
      this.CountryId = event.countryId;

      this._allState = this.AdDetailsService.StateDDL("1");
      debugger;
      this._commonService.StateDDL("1").subscribe(data => {
        console.log(data);
        //debugger;
        this.states = data;
      });

      this._commonService.BUCityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
      this._allState = this.AdDetailsService.StateDDL(this.CountryId);
      this.CountryName = event.target.options[this.CountryId].innerHTML;
      this.editBUProfile.countryName = this.CountryName;
    }

  }


  FillCityDDL(event: any) {
    debugger;
    var x = document.getElementsByClassName("ng-value-label");
    x[1].innerHTML = "--Select City--";
    if (event != 0) {
      this.StateId = event.stateId;
      this.StateName = event.stateName;
      this._allCity = this.AdDetailsService.CityDDL(this.StateId);
      //this.StateName = event.target.options[this.StateId].innerHTML;  
      this.editBUProfile.stateName = this.StateName;

      this._commonService.BUCityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
    }
    else {
      var x = document.getElementsByClassName("ng-value-label");
      this.cities = [];
    }

  }


  FillCityName(event: any) {
    debugger;
    this.CityId = event.cityId;
    this.CityName = event.cityName;

  }

}
