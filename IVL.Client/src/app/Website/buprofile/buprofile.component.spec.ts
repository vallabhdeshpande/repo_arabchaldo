import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuprofileComponent } from './buprofile.component';

describe('BuprofileComponent', () => {
  let component: BuprofileComponent;
  let fixture: ComponentFixture<BuprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
