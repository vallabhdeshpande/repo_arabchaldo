import { Component, OnInit } from '@angular/core';
import { WebsiteService} from '../../shared/website.service';
import { HttpEventType, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  AboutUsData: string="";
  AboutUsArabicData: string = "";
  showEng: string = "";
  showArb: string = "d-none";
  constructor(private WebsiteService: WebsiteService, private http: HttpClient) { }

  ngOnInit() {
    debugger;
    this.getAboutUSData();
    this.changeAboutUSData();
	
  }

  changeAboutUSData() {

    if (localStorage.getItem('language') !== null) {
      if (localStorage.getItem('language') === "English") {
        this.showEng = "";
        this.showArb = "d-none";
      }
      else {
        this.showEng = "d-none";
        this.showArb = "";
      }

    }
  }
 getAboutUSData()
 {
   debugger;
  this.WebsiteService.getAboutUsData().subscribe(adposts => {
    console.log(adposts);
    this.AboutUsData = adposts[0].AboutUs;
    this.AboutUsArabicData = adposts[0].AboutUsArabic;
  });
 }
}
