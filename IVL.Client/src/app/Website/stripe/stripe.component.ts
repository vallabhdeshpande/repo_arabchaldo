import { Component, OnInit, ViewChild, NgModule, Injectable, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxStripeModule } from 'ngx-stripe';
import { StripeService, Elements, Element as StripeElement, ElementsOptions, ElementType, ConfirmPaymentIntentOptions } from 'ngx-stripe';
//import { CardsComponent } from '../../base/cards.component';
import { Paymentplanservice } from '../../views/theme/paymentplanservice';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Http, Response, Headers } from '@angular/http';
import { environment } from '../../../environments/environment';
import { style } from '@angular/animations';
import { ToastrService } from 'ngx-toastr';
import {  Transcationdetails} from '../../views/theme/transcationdetails';

@Component({
  selector: 'app-stripe',
  templateUrl: './stripe.component.html',
  styleUrls: ['./stripe.component.scss']
})
export class StripeComponent implements OnInit
{
  //headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  //options = new RequestOptions({ headers: this.headers });
  baseUrl = environment.baseUrl;

  @Input() finalAmount: number;
  @Input() CustomerEmail: string;
  @Input() CustomerName: string;
  //@Input() CustomerId: number;
  @Input() PlanIdForStripe: number;

  //@Input() public amount = new EventEmitter();
  @Output() public stripeResponse = new EventEmitter();

  url: any;

  finalAmountToPay: number;
  errorMessage: string;
  transactionDetails = new Transcationdetails();

  constructor(private formbuilder: FormBuilder, private stripeService: StripeService, private paymentplanservice: Paymentplanservice,
    private httpClient: HttpClient, private http: Http, private toastr: ToastrService) { }

  elements: Elements;
  card: StripeElement;
  elementType: ElementType;
  // optional parameters
  elementsOptions: ElementsOptions = {
    locale: 'en'
  };


  stripeTest: FormGroup;
  BUstripepaymentForm: any;
  submitted = false;


  ngOnInit()
  {
    debugger;
    //this.stripeTest = this.fb.group({
    //  name: ['', [Validators.required]],

    //});
    this.BUstripepaymentForm = this.formbuilder.group(
      {
        //name: ['', [Validators.required, Validators.pattern(".*\\S.*[a-zA-z ]")]]
        name: ['', [Validators.required, Validators.pattern(".*\\S.*[a-zA-z ]")]],
        email: ['', [Validators.required, Validators.email]],
        phone: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(10)], Validators.maxLength(10)],
        address1: ['', [Validators.required]],
        address2: ['', [Validators.required]],
        state: ['', [Validators.required]],
        zip: ['', [Validators.required]],
      });

    this.finalAmountToPay = this.finalAmount;

    this.stripeService.elements(this.elementsOptions)
      .subscribe(elements => {
        this.elements = elements;

        // Only mount the element the first time
        if (!this.card) {
          this.card = this.elements.create('card', {
            hidePostalCode: true,
            style: {

              base: {
                iconColor: '#eb2126',
                color: '#fd7e14',
                fontWeight: 500,
                fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
                fontSize: '18px',
                fontSmoothing: 'antialiased',


                ':-webkit-autofill': {
                  color: '#fce883',
                },
                '::placeholder': {
                  color: '#c8ced3',
                },
              },
              invalid: {
                iconColor: '#FFC7EE',
                color: '#FFC7EE',
              },
            },
          });
          this.card.mount('#card-element');
        }
      });
  }

  InitializeStripe() {
    this.BUstripepaymentForm = this.formbuilder.group(
      {
        //name: ['', [Validators.required, Validators.pattern(".*\\S.*[a-zA-z ]")]]
        paymentname: ['']
      });

    this.finalAmountToPay = this.finalAmount;

    this.stripeService.elements(this.elementsOptions)
      .subscribe(elements => {
        this.elements = elements;

        // Only mount the element the first time
        if (!this.card) {
          this.card = this.elements.create('card', {
            hidePostalCode: true,
            style: {

              base: {
                iconColor: '#eb2126',
                color: '#fd7e14',
                fontWeight: 500,
                fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
                fontSize: '18px',
                fontSmoothing: 'antialiased',


                ':-webkit-autofill': {
                  color: '#fce883',
                },
                '::placeholder': {
                  color: '#c8ced3',
                },
              },
              invalid: {
                iconColor: '#FFC7EE',
                color: '#FFC7EE',
              },
            },
          });
          this.card.mount('#card-element');
        }
      });
  }

  pay() {
    //debugger;
    //console.log(this.BUstripepaymentForm);
    //console.log(this.BUstripepaymentForm.value.paymentname);
    this.submitted = true;
    if (this.BUstripepaymentForm.invalid) {
      this.toastr.warning("Mandatory fields missing or incorrect data entered.");
      return;
    }

    if (this.finalAmountToPay <= 0.5) {
      this.toastr.warning('Final amount needs to be more than 50 cents to make payment.');
      return;
    }

    this.transactionDetails.name = this.BUstripepaymentForm.value.name;
    this.transactionDetails.email = this.BUstripepaymentForm.value.email;
    this.transactionDetails.address1 = this.BUstripepaymentForm.value.address1;
    this.transactionDetails.address2 = this.BUstripepaymentForm.value.address2;
    this.transactionDetails.amount = this.finalAmount;
    this.transactionDetails.paymentMode = "Online";
    this.transactionDetails.phone = this.BUstripepaymentForm.value.phone;
    this.transactionDetails.state = this.BUstripepaymentForm.value.state;
    this.transactionDetails.zip = this.BUstripepaymentForm.value.zip;

    this.stripeService.paymentRequest({
      country: 'US',
      currency: 'usd',
      total: {
        label: 'Demo total',
        amount: this.finalAmountToPay * 100,
      },
      requestPayerName: true,
      requestPayerEmail: true,
    });

    this.stripeService
      .createToken(this.card, { name })
      .subscribe(result => {
        if (result.token) {
          //this.CustomerName = this.CustomerName;
          const headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
          //options = new RequestOptions({ headers: this.headers });
          console.log(result);
          let obj = {
            token: result.token.id,
            //customerId: this.CustomerId,
            customerEmail: this.transactionDetails.email,
            customerName: this.transactionDetails.name,
            amount: this.finalAmountToPay * 100,
            planId: this.PlanIdForStripe,
            address1: this.transactionDetails.address1,
            address2: this.transactionDetails.address2,
            phone: this.transactionDetails.phone,
            state: this.transactionDetails.state,
            zip: this.transactionDetails.zip
          }

          this.httpClient.post(this.baseUrl + 'PaymentPlan/StripePaymentConfirmation?tokenDetails=' +
            JSON.stringify(obj),
            { headers: headers }).subscribe(data => {
              console.log(data);
              this.stripeResponse.emit(data);
            });

          console.log(result.token);
        }
        else if (result.error) {
          this.errorMessage = result.error.message;
          this.toastr.error(this.errorMessage);
          console.log(result.error.message);
        }
      });
  }

  get f() { return this.BUstripepaymentForm.controls; }

}
