import { Component, OnInit } from '@angular/core';
import { AdDetails } from '../../shared/addetails.model';
import { WebsiteService } from '../../shared/website.service'
import { environment } from '../../../environments/environment';
import { CommonService } from '../../views/shared/common.service';
import { Common, category, Subcategory } from '../../views/shared/common.model';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit {
  jobposts: AdDetails[] = [];
  jobpost = new AdDetails();
  Job: string;
  JobDetails = [];
  //EventDetails = [];
  imagepath = environment.imagepath;
  EventId = 0;
  categories: category[] = [];
  subcategories: Subcategory[] = [];
  category = new category();
  subcategory = new Subcategory();
  categoryId = 0;
  subcategoryId = 0;
  showCatEng: string = "";
  showCatArb: string = "d-none";
  jobsNoRecords: string = "d-none";
  public _allCategory: Observable<category[]>;
  jobcontents = [];
  constructor(private WebsiteService: WebsiteService, public CommonService: CommonService, private route: ActivatedRoute, private router: Router) { }
  ngOnInit() {
    this.getJobListBysubCategory(this.subcategoryId);
    // this.getJobList();
    // this.getAdListBysubCategory(this.subcategoryId);
    //  this.getJobsbycategory(this.categoryId);
    this.getCategoryList();
    this.changeCategoryContent();
  }

  //getJobList(): void {
  //  this.WebsiteService.getJobsList().subscribe(jobposts => {
  //    this.jobcontents = jobposts;
  //  });
  //}

  getPicUrl(picurl: string, AdId) {
    if (picurl == null)
      return "assets/img/default/no_image_placeholder.jpg";
    else if (picurl == "")
      return "assets/img/default/no_image_placeholder.jpg";
    else
      //return this.imagepath+'Banners/'+bannerId+'/Image/'+picurl;
      return this.imagepath + 'Jobs/' + AdId + '/Image/' + picurl;
  }

  changeCategoryContent() {
    debugger;
    if (localStorage.getItem('language') !== null) {
      if (localStorage.getItem('language') === "English") {
        this.showCatEng = "";
        this.showCatArb = "d-none";
      }
      else {
        this.showCatEng = "d-none";
        this.showCatArb = "";
      }

    }
  }

  //getAdListByCategoryId(categoryId): void {
  //  //debugger;
  //  this.WebsiteService.getAdListByCategoryId(categoryId).subscribe(jobposts => {
  //    this.jobposts = jobposts;
  //    console.log(jobposts);
  //  });
  //}

  //getAdListBysubCategory(subcategoryId): void {
  //  debugger;
  //  this.WebsiteService.getAdListBySubcategoryId(subcategoryId).subscribe(jobposts => {
  //    this.jobposts = jobposts;
  //    console.log(jobposts);
  //  });
  //}

  //getEventsbycategory(categoryId): void {
  //  //debugger;
  //  this.WebsiteService.getEventsBycategoryId(categoryId).subscribe(eventposts => {
  //    this.jobposts = eventposts;
  //    console.log(eventposts);
  //  });
  //}


  getJobListBysubCategory(subcategoryId): void {
    debugger;

    if (subcategoryId === 0) {
      document.getElementById(subcategoryId).className = "active";
    }
    else {
      document.getElementById('0').classList.remove('active');
    }

    this.WebsiteService.getJobListBySubcategoryId(subcategoryId).subscribe(jobposts => {

      if (jobposts !== null && jobposts.length > 0) {
        this.jobposts = jobposts;
        this.jobsNoRecords = "d-none";
      }
      else {
        this.jobposts = [];
        this.jobsNoRecords = "no-record";
      }



      console.log(jobposts);
    });
  }


  //getJobsbycategory(categoryId): void {
  //  //debugger;
  //  this.WebsiteService.getEventsBycategoryId(categoryId).subscribe(eventposts => {
  //    this.jobposts = eventposts;
  //    //console.log(jobposts);
  //  });
  //}


  getCategoryList(): void {
    debugger;
    this.CommonService.PopularJobSubCategories().subscribe(subcategories => {
      if (subcategories !== null && subcategories.length > 0) {
        
        this.subcategories = subcategories;
        console.log(this.subcategories);
      }
      else
        this.subcategories = [];
      //console.log(this.subcategories);

    });
  }

  btnPostNew() {
    debugger;
    console.log(localStorage.getItem('roleName'));
    if (localStorage.getItem('token') === null) {
      this.router.navigate(['/login']);
    }
    else {
      console.log(localStorage.getItem);
      if ((localStorage.getItem('roleName') === 'SuperAdmin') || (localStorage.getItem('roleName') === 'Admin')) {
        this.router.navigate(['/theme/postad'], { queryParams: { Job: "Jobs" } });
      }
      else {
        this.router.navigate(['/bupostad'], { queryParams: { Job: "Jobs" } });
      }
    }
  }
}
