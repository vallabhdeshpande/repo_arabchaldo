import { Component, OnInit } from '@angular/core';
import { AdDetails, AdList } from '../../shared/addetails.model';
import { Router, ActivatedRoute } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { WebsiteService } from '../../shared/website.service'
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-buysell',
  templateUrl: './buysell.component.html',
  styleUrls: ['./buysell.component.scss']
})
export class BuysellComponent implements OnInit {
  public userId : string;
  BUser = false;
  adposts: AdDetails[]=[];
  adpost = new AdDetails();
  Paidadposts: AdDetails[]=[];
  paidadpost = new AdDetails();
  Freeadposts: AdDetails[]=[];
  Freeadpost = new AdDetails();
  Premiumposts: AdDetails[]=[];
	Premiumpost = new AdDetails();
	Navigateurl:string;
  isVisible:boolean;
  Addetails=[];
  FreeAddetails = [];
  PaidAddetails = [];
  PremiumAddetails = [];
  PremiumBuyAddetails = [];
  PremiumsellAddetails = [];
  FreeBuyAddetails = [];
  FreesellAddetails = [];
  Premiumbuyposts: AdDetails[]=[];
  Premiumsellposts: AdDetails[]=[];
  Freebuyposts: AdDetails[]=[];
  Freesellposts: AdDetails[]=[];
  imagepath = environment.imagepath;
  ImageUrl: string;
  Imageurls = [];
  PremiumList=false;
  FreeList=false;
  PaidList=true;
  IsPaidAd=true;
  isChecked=false;
  listView:string="col-sm-12";
  premiumTag:string="ad-block premium";
  PremiumtagList:boolean=true;
  Verifiedtag:boolean=true;
  buttonActiveClass="btn btn-secondary active";
  buttonclass="btn btn-secondary";
  premiumBuyNoRecords: string = "d-none";
  premiumSellNoRecords: string = "d-none";
  freeSellNoRecords: string = "d-none";  
  freeBuyNoRecords: string = "d-none"; 
  constructor(private WebsiteService: WebsiteService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getBuySellListing();

  }
  getBuySellListing() {
    debugger;
    this.WebsiteService.getBuySellListing().subscribe(Premiumposts => {
      console.log(Premiumposts);
      if (Premiumposts != null && Premiumposts.length > 0) {
        this.Premiumposts = Premiumposts;
        var adlen = this.Premiumposts.length;
        console.log(this.adposts);
        for (let i = 0; i < adlen; i++) {
          if (Premiumposts[i].IsPremium == "Premium" && Premiumposts[i].subCategoryName == "Buy") {
            // this.PremiumAddetails.push(this.Premiumposts[i])
            this.PremiumBuyAddetails.push(this.Premiumposts[i])
          }
          else if (Premiumposts[i].IsPremium == "Premium" && Premiumposts[i].subCategoryName == "Sell") {
            // this.PremiumAddetails.push(this.Premiumposts[i])
            this.PremiumsellAddetails.push(this.Premiumposts[i])
          }
          else if (Premiumposts[i].IsPremium == "Free" && Premiumposts[i].subCategoryName == "Buy") {
            this.FreeBuyAddetails.push(this.Premiumposts[i])
          }
          else if (Premiumposts[i].IsPremium == "Free" && Premiumposts[i].subCategoryName == "Sell") {
            this.FreesellAddetails.push(this.Premiumposts[i])
          }
        }
      }
      else {
        this.Premiumposts = [];
      }
      if (this.FreeBuyAddetails.length > 0) {
        this.Freebuyposts = this.FreeBuyAddetails;
      }
      else {
        this.freeBuyNoRecords = "no-record";
        this.Freebuyposts = [];
        this.FreeBuyAddetails=[];
    }
      if (this.FreesellAddetails.length > 0) {
        this.Freesellposts = this.FreesellAddetails;
      }
      else {
        this.freeSellNoRecords = "no-record";
        this.Freesellposts = [];
        this.FreesellAddetails=[];
      }

      if (this.PremiumBuyAddetails.length > 0) {
        this.Premiumbuyposts = this.PremiumBuyAddetails;
      }
      else {
        this.premiumBuyNoRecords = "no-record";
        this.Premiumbuyposts = [];
        this.PremiumBuyAddetails=[];
      }

      if (this.PremiumsellAddetails.length > 0) {
        this.Premiumsellposts = this.PremiumsellAddetails;
      }
      else {
        this.premiumSellNoRecords = "no-record";
        this.Premiumsellposts = [];
        this.PremiumsellAddetails = [];
      }
      //   this.Freeadposts = this.FreeAddetails;
      //  this.Premiumposts = this.PremiumAddetails;  
      this.Freebuyposts = this.FreeBuyAddetails;
      this.Freesellposts=this.FreesellAddetails;
      this.Premiumbuyposts = this.PremiumBuyAddetails;
      this.Premiumsellposts=this.PremiumsellAddetails;
    console.log(this.FreeAddetails);
    console.log(this.PremiumAddetails);
      this.PaidList = false;
      this.PremiumList = true;
      this.FreeList = true;
      document.getElementById('premium').className = "btn btn-secondary";
      document.getElementById('all').className = "btn btn-secondary active";
    });

  }

  // btnchanged(evt, action) {
  //   //debugger;
  //   if (action == "paid") {
  //     this.Paidadposts = this.Paidadposts;
  //     //  this.Premiumposts=this.Premiumposts;
  //     this.PaidList = true;
  //     this.PremiumList = false;
  //     this.FreeList = false;
  //     document.getElementById('paid').className = "btn btn-secondary active";
  //     document.getElementById('free').className = "btn btn-secondary";
  //     document.getElementById('all').className = "btn btn-secondary";
  //   }
  //   else if (action == "free") {
  //     this.Freeadposts = this.Freeadposts;
  //     this.PremiumList = false;
  //     this.PaidList = false;
  //     this.FreeList = true;
  //     document.getElementById('paid').className = "btn btn-secondary";
  //     document.getElementById('free').className = "btn btn-secondary active";
  //     document.getElementById('all').className = "btn btn-secondary";
  //   }
  //   else if (action == "all") {
  //     this.Paidadposts = this.Paidadposts;
  //     // this.Premiumposts=this.Premiumposts;
  //     // this.Freeadposts=this.Freeadposts;
  //     this.PremiumList = false;
  //     this.PaidList = true;
  //     this.FreeList = true;
  //     document.getElementById('paid').className = "btn btn-secondary";
  //     document.getElementById('free').className = "btn btn-secondary";
  //     document.getElementById('all').className = "btn btn-secondary active";
  //   }
  // }
  btnchanged(evt,action) {    
    debugger;
    debugger;
        if(action=="premium") {
         // this.Paidadposts=this.PaidAddetails;
          this.Premiumposts = this.PremiumAddetails;
          this.PaidList = false;
          this.PremiumList = true;
          this.FreeList=false; 
          document.getElementById('premium').className = "btn btn-secondary  active";
          //document.getElementById('free').className = "btn btn-secondary";
          document.getElementById('all').className = "btn btn-secondary";
        } 
    // else if(action=="free")
    // {
    //   this.Freeadposts=this.FreeAddetails; 
    //   this.PremiumList=false;
    //   this.PaidList=false;
    //   this.FreeList=true;  
    //   document.getElementById('paid').className = "btn btn-secondary";
    //   document.getElementById('free').className = "btn btn-secondary  active";
    //   document.getElementById('all').className = "btn btn-secondary"; 
    // }
    else if(action=="all")
    {
      this.Paidadposts=this.PaidAddetails;
       this.Premiumposts=this.PremiumAddetails;
      this.Freeadposts=this.FreeAddetails;
      this.PremiumList=true;
      this.PaidList=true;
      this.FreeList=true;  
      document.getElementById('premium').className = "btn btn-secondary";
      //document.getElementById('free').className = "btn btn-secondary";
      document.getElementById('all').className = "btn btn-secondary  active";
    }
  }
  getpremiumtag(IsPremium) {
    //debugger;
    if (IsPremium == "Premium") {
      this.premiumTag = "ad-block premium";
      this.PremiumtagList = true;
      this.Verifiedtag = false;
    }
    else {
      this.premiumTag = "ad-block premium d-none";
      this.PremiumtagList = false;
      this.Verifiedtag = true;
    }
  }
  navigateToPostAd() {
    //debugger;

    console.log(localStorage.getItem('roleName'));
    if ((localStorage.getItem('roleName') == "SuperAdmin") || (localStorage.getItem('roleName') == "Admin"))
      this.router.navigate(['/dashboard']);
    else
      this.router.navigate(['/bupostad']);

  }

  btnPostNew() {
    console.log(localStorage.getItem('roleName'));
    if (localStorage.getItem('token') === null){
      this.router.navigate(['/login']);
    }
    else {
      console.log(localStorage.getItem);
      this.userId = localStorage.getItem('userId');
      if ((localStorage.getItem('roleName') === 'SuperAdmin') || (localStorage.getItem('roleName') === 'Admin')){
        this.router.navigate(['/theme/addbuysell']);
      }
      else {
        this.router.navigate(['/buaddbuysell']);
      }
    }
  }
}
