import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  constructor(private router: Router, private authenticationService: AuthenticationService) {


  }
  canActivate(route: ActivatedRouteSnapshot,state: RouterStateSnapshot){
    const currentUser = this.authenticationService.currentUserValue;
//debugger;
    if (currentUser) {
        // check if route is restricted by role
        //let role = localStorage.getItem(role);
        if (route.data.roles && route.data.roles.indexOf(currentUser.Email) === "null") {
            // role not authorised so redirect to home page
            this.router.navigate(['/login']);
            return false;
        }

        // authorised so return true
        return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    return false;
}
}
