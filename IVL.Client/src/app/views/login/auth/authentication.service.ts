import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { LoggedInUser } from '../../shared/common.model';
import { RequestOptions,Http,Response, Headers } from '@angular/http';
@Injectable({ providedIn: 'root' })
export class AuthenticationService {

   
   // headers = new Headers({ 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*','Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  //  headers.
   // options = new RequestOptions({ headers: this.headers });
   options = { headers: new HttpHeaders().set('Content-Type', 'application/json').set('Access-Control-Allow-Origin', '*').set('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS') };

    private currentUserSubject: BehaviorSubject<LoggedInUser>;
    public currentUser: Observable<LoggedInUser>;
    public currentLoggedInUser= new LoggedInUser();
    BaseURI = environment.baseUrl;
    loggedInRole:string;

    constructor(private http: HttpClient,private _httpService: Http) {
        this.currentUserSubject = new BehaviorSubject<LoggedInUser>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): LoggedInUser {
        return this.currentUserSubject.value;
    }

    login(formData) {
        debugger;

        //let headers = new HttpHeaders({ 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*','Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
       // headers.append('Content-Type','application/json');
       // headers.append('Access-Control-Allow-Origin','*');
      // let options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };

        return this.http.post<any>(this.BaseURI + 'Login/Login', formData,this.options)
            .pipe(map(response => {
                debugger;
                // login successful if there's a jwt token in the response

               
                if (response && response.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    debugger;
                    this.currentLoggedInUser.Token=response.token;
                    this.currentLoggedInUser.FirstName=response.loggedInUserInfo.split(":")[0];
                    this.currentLoggedInUser.LastName=response.loggedInUserInfo.split(":")[1];
                    this.currentLoggedInUser.UserId=response.loggedInUserInfo.split(":")[2];
                    this.currentLoggedInUser.Email=response.loggedInUserInfo.split(":")[3];
                    this.currentLoggedInUser.RoleId=response.loggedInUserInfo.split(":")[4];
                    this.currentLoggedInUser.RoleName=response.loggedInUserInfo.split(":")[5];
                    if(response.loggedInUserInfo.split(":")[6]!="")
                    this.currentLoggedInUser.ProfilePic=response.loggedInUserInfo.split(":")[6];
                    else
                    this.currentLoggedInUser.ProfilePic="";;
                    // setting local storage
                    localStorage.setItem('token',this.currentLoggedInUser.Token);
                    localStorage.setItem('firstName',this.currentLoggedInUser.FirstName);
                    localStorage.setItem('lastName',this.currentLoggedInUser.LastName);
                    localStorage.setItem('userId',this.currentLoggedInUser.UserId);
                    localStorage.setItem('email',this.currentLoggedInUser.Email);
                    localStorage.setItem('roleId',this.currentLoggedInUser.RoleId);
                    localStorage.setItem('roleName',this.currentLoggedInUser.RoleName);
                    localStorage.setItem('profilePic',this.currentLoggedInUser.ProfilePic);
                    
                    localStorage.setItem('currentUser', JSON.stringify(response));                 



                    this.currentUserSubject.next(this.currentLoggedInUser);
                }
                debugger;
                return response;
            }));
    }
    // Sociallogin(User):Observable<any[]> {
    //     debugger
    //       return this.http.get<any[]>(this.BaseURI + '/Login/SocialloginInfo?Email='+ User.email+'& SocialMediaMode='+User.provider);   
    // }
    Sociallogin(user) {
      debugger
     // let headers = new HttpHeaders({ 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*','Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
     // headers.append('Content-Type', 'application/json');
     // headers.append('Access-Control-Allow-Origin', '*');
     let options = { headers: new HttpHeaders().set('Content-Type', 'application/json').set('Access-Control-Allow-Origin', '*').set('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS') };
      return this.http.post<any>(this.BaseURI + 'UserDetails/SocialloginInfo', user, this.options)
        .pipe(map(response => {
            // debugger;
             // login successful if there's a jwt token in the response
             this.logout();
             if (response && response.token) {
                 // store user details and jwt token in local storage to keep user logged in between page refreshes
                 
                 this.currentLoggedInUser.Token=response.token;
                 this.currentLoggedInUser.FirstName=response.loggedInUserInfo.split(":")[0];
                 this.currentLoggedInUser.LastName=response.loggedInUserInfo.split(":")[1];
                 this.currentLoggedInUser.UserId=response.loggedInUserInfo.split(":")[2];
                 this.currentLoggedInUser.Email=response.loggedInUserInfo.split(":")[3];
                 this.currentLoggedInUser.RoleId=response.loggedInUserInfo.split(":")[4];
                 this.currentLoggedInUser.RoleName=response.loggedInUserInfo.split(":")[5];
                 this.currentLoggedInUser.ProfilePic=response.loggedInUserInfo.split(":")[6] +":" +response.loggedInUserInfo.split(":")[7];
                 
                 // setting local storage
                 localStorage.setItem('token',this.currentLoggedInUser.Token);
                 localStorage.setItem('firstName',this.currentLoggedInUser.FirstName);
                 localStorage.setItem('lastName',this.currentLoggedInUser.LastName);
                 localStorage.setItem('userId',this.currentLoggedInUser.UserId);
                 localStorage.setItem('email',this.currentLoggedInUser.Email);
                 localStorage.setItem('roleId',this.currentLoggedInUser.RoleId);
                 localStorage.setItem('roleName',this.currentLoggedInUser.RoleName);
                 localStorage.setItem('profilePic',this.currentLoggedInUser.ProfilePic);
                 
                 localStorage.setItem('currentUser', JSON.stringify(response));                 



                 this.currentUserSubject.next(this.currentLoggedInUser);
             }

             return response;
         }));
    }
  logout() {
    debugger;
        // remove user from local storage to log user out
      localStorage.removeItem('token');
      localStorage.removeItem('firstName');
      localStorage.removeItem('lastName');
      localStorage.removeItem('userId');
      localStorage.removeItem('email');
      localStorage.removeItem('roleId');
      localStorage.removeItem('roleName');
      localStorage.removeItem('profilePic');
      localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
    GetUserInfoByEmail(email:string) {
        debugger;

        //let headers = new HttpHeaders({ 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*','Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
       // headers.append('Content-Type','application/json');
       // headers.append('Access-Control-Allow-Origin','*');
      // let options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };

        return this.http.get<any>(this.BaseURI + 'Login/GetUserInfoByEmail?Email='+email);
            // .pipe(map(response => {
            //     debugger;
            //      console.log(response);
            //     return response;
            // }));
    }
}
