import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { UserService } from '../shared/user.service';
import { AuthenticationService } from '../../views/login/auth/authentication.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthService, SocialUser, GoogleLoginProvider, FacebookLoginProvider, LinkedinLoginProvider } from 'ng4-social-login';
import { TranslateService } from '@ngx-translate/core';
import { User } from '../../shared/user';
import { LoggedInUser } from '../shared/common.model';
@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})


export class LoginComponent implements OnInit {
  formModel = {
    Email: '',
    Password: ''
  }
  loggedInRole:string;
  returnUrl: string;
  public user: any=SocialUser
  UserName: [];
  langselected: string = "English";
  constructor(private SocialAuthserice: AuthService,private service: AuthenticationService, public translate: TranslateService,private router: Router,private route: ActivatedRoute, private toastr: ToastrService) { 

     // redirect to home if already logged in
    //  if (this.service.currentUserValue) { 
    //   this.router.navigate(['/dashboard']);
 // }
  }
  
  ngOnInit() 
  {
    debugger;
     // get return url from route parameters or default to '/'
     //this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    if (localStorage.getItem('token') != null) {
      this.translate.setDefaultLang(localStorage.getItem('language'));
      this.langselected = localStorage.getItem('language');
      this.translate.use(localStorage.getItem('language'));
      if ((localStorage.getItem('roleName') == "SuperAdmin") || (localStorage.getItem('roleName') == "Admin"))
        this.router.navigate(['/dashboard']);
      else
        this.router.navigate(['/budashboard']);


    }
    else {

      if (localStorage.getItem('language') !== null) {
        this.translate.setDefaultLang(localStorage.getItem('language'));
        this.translate.use(localStorage.getItem('language'));
        this.langselected = localStorage.getItem('language');
      }
      else {
        this.translate.setDefaultLang('English');
        this.translate.use("English");
        this.langselected = "English";
      }
      
      //this.router.navigate(['/login']);
    }
     

    //if (localStorage.getItem('language') !== null) {
      
    //  //catcontent.ngOnInit();
    //}
    //else {
     

    //}
  }


  switchLanguage(event: any) {
    debugger;
    this.translate.use(event.target.value);
    localStorage.setItem('language', event.target.value);
    //var langselected = document.getElementById('langSelect');
    if (event.target.value == "English")
      this.langselected = "English";
    else
      this.langselected = "Arabic";
    localStorage.setItem('language', this.langselected);
    this.langselected = localStorage.getItem('language');


  }
  onSubmit(form: NgForm) 
  {
    this.service.login(form.value).pipe(first()).subscribe(
      (res: any) => {
        debugger;
      //  this.router.navigate([this.returnUrl]);
      console.log(res);
        if (res.token != null) {
          if (res.loggedInUserInfo.split(":")[7]) {
            if (res.loggedInUserInfo.split(":")[4] == 2 || res.loggedInUserInfo.split(":")[4] == 1) {
              this.router.navigate(['/dashboard']);
            }
            else if (res.loggedInUserInfo.split(":")[4] == 8) {
              // this.router.navigate(['/budashboard'],{queryParams:{res}});
              this.router.navigate(['/budashboard']);
            }
          }
          else
            this.toastr.error('Your account is not activated. Activation link is already sent on registered email.');
        }
      
      
      
     
      },
      err => {
        console.log(err.status);
        if (err.status == 400)
        {
          this.toastr.error('Incorrect email or password.', 'Authentication failed.');
        
         // alert('Incorrect username or password');
        }
        else
          console.log(err);
      }
    );
  }

  Googlelogin(){
    debugger
    this.SocialAuthserice.signIn(GoogleLoginProvider.PROVIDER_ID).then((userData) =>{this.user=userData;
      this.SocialLogin(this.user);
    });
    //this.SocialLogin(this.user);
  }
  facebooklogin(){
    debugger
    this.SocialAuthserice.signIn(FacebookLoginProvider.PROVIDER_ID).then((userData) =>{this.user=userData;
      console.log(this.user);

    });
    //this.SocialLogin(this.user);
  }
  Linkdenlogin(){
    debugger
    this.SocialAuthserice.signIn(LinkedinLoginProvider.PROVIDER_ID).then((userData) =>{this.user=userData;
      console.log(this.user);
    });
    //this.SocialLogin(this.user);
  }

  Twitterlogin() {
    debugger
    this.SocialAuthserice.signIn(LinkedinLoginProvider.PROVIDER_ID).then((userData) => {
    this.user = userData;
      console.log(this.user);
    });
    //this.SocialLogin(this.user);
  }
  SocialLogin(user)
  {
  debugger;
  user.name=user.name.split(" ");
  user.FirstName=user.name[0];
  user.LastName=user.name[1];
  user.ProfilePicurl=user.photoUrl;
    this.service.Sociallogin(user).pipe(first()).subscribe(
      (res: any) => {
        debugger;
      this.router.navigate(['/budashboard']);               
      });
  }

  //Forgot Password




}

  




