export class Transcationdetails {
  public transactionId: number;
  public planId: number;
  public paymentMode: string;
  public tokenId: string;
  public receiptId: string;
  public transactionDate: Date;
  public amount: number;
  public name: string;
  //public userId :number ; 
  public email: string;
  public phone: number;
  public address1: string;
  public address2: string;
  public state: string;
  public zip: number;
  public type: string;
}
