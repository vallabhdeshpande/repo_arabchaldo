// Angular
import { CommonModule } from '@angular/common';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { FormsModule,ReactiveFormsModule  }   from '@angular/forms';
import { ColorsComponent } from './colors.component';
import { TypographyComponent } from './typography.component';
import { ConfigurationService } from '../theme/configurationservice';
import { UserService } from '../theme/user.service';
import { AdDetailsService } from '../theme/addetails.service';
import { EventDetailsService } from '../theme/eventdetails.service';
import { CommonService } from '../shared/common.service';
import { HttpModule } from '@angular/http';
import { SharedModule } from '../../shared/shared.module';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { StateService } from '../theme/stateservice';
import { CityService } from '../theme/city.service';
//import { NgSelectModule } from '@ng-select/ng-select';
//import { googlemaps } from '@types/googlemaps';
  

import { ToastrModule } from 'ngx-toastr';
// Theme Routing
import { ThemeRoutingModule } from './theme-routing.module';
import { PostadComponent } from '../theme/postad/postad.component';
import { PosteventComponent } from '../theme/postevent/postevent.component';
import { AdduserComponent } from '../theme/adduser/adduser.component';
import { ManageuserComponent } from '../theme/manageuser/manageuser.component';
import { ManageadComponent } from '../theme/managead/managead.component';
import { ManageeventComponent } from '../theme/manageevent/manageevent.component';

import { ManagecategoryComponent } from '../theme/managecategory/managecategory.component';
import { ManagesubcategoryComponent } from '../theme/managesubcategory/managesubcategory.component';
import { AddstateComponent } from '../theme/addstate/addstate.component';
import { AddcityComponent } from '../theme/addcity/addcity.component';
import { UploadComponent } from '../theme/upload/upload.component';
import { MultiUploadComponent } from '../theme/multiupload/multiupload.component';
import { UpdateuserComponent} from '../theme/updateuser/updateuser.component';
import {UpdateeventComponent} from '../theme/updateevent/updateevent.component';
import { EventdetailviewComponent} from '../theme/eventdetailview/eventdetailview.component';
import { ManagebannerComponent } from './managebanner/managebanner.component';
import { ResetpasswordComponent } from '../theme/resetpassword/resetpassword.component';
import {EditcategoryComponent} from '../theme/editcategory/editcategory.component';
import {EditsubcategoryComponent} from '../theme/editsubcategory/editsubcategory.component';
import {EditbannerComponent} from '../theme/editbanner/editbanner.component';
import { AdbannerComponent } from './adbanner/adbanner.component';
import { AddetailsviewComponent} from '../theme/addetailsview/addetailsview.component';
import { UserdetailsviewComponent } from '../theme/userdetailsview/userdetailsview.component';
import {BannerdetailsviewComponent} from '../theme/bannerdetailsview/bannerdetailsview.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { OrderModule } from 'ngx-order-pipe';
import { NgSelectModule } from '@ng-select/ng-select';
 //import { NgSelectModule } from '@ng-select/ng-select';
import { SettingComponent } from '../theme/setting/setting.component';

import { CreateplanComponent } from '../theme/createplan/createplan.component';
import { AssignplanComponent } from '../theme/assignplan/assignplan.component';
import { StripeComponent } from '../theme/stripe/stripe.component';

import { WorkflowdetailsComponent } from './workflowdetails/workflowdetails.component';
import { AdblogComponent } from '../theme/adblog/adblog.component';
import {BlogdetailsviewComponent} from '../theme/blogdetailsview/blogdetailsview.component';
import { ManageblogComponent } from '../theme/manageblog/manageblog.component';
//import { PostjobComponent} from '../theme/postjob/postjob.component';
import { ManagejobsComponent} from '../theme/managejobs/managejobs.component';
import { InboxmessageComponent } from '../theme/inboxmessage/inboxmessage.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { LoaderComponent } from '../theme/loader/loader.component';
//import { LoaderService } from '../../loader/loader.service';
//import { LoaderInterceptor } from '../../loader/loader.interceptor';

import { MultifileuploadComponent } from './multifileupload/multifileupload.component';
import { MessageBoardComponent } from '../theme/message-board/message-board.component';
import { ManagesmalladComponent } from './managesmallad/managesmallad.component';
import { PostsmalladComponent } from './postsmallad/postsmallad.component';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SmalladdetailsviewComponent } from './smalladdetailsview/smalladdetailsview.component';
import {ManagebuysellComponent}  from '../theme/managebuysell/managebuysell.component';
import {AddbuysellComponent}  from '../theme/addbuysell/addbuysell.component';
import { BuyselldetailsviewComponent} from '../theme/buyselldetailsview/buyselldetailsview.component';


@NgModule({
  imports: [
    CommonModule,
    ThemeRoutingModule,
    FormsModule,
    NgSelectModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule ,
    ToastrModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    AngularEditorModule,
    NgMultiSelectDropDownModule,
    OrderModule, SharedModule,
    NgbModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot()

    
  ],
 
  declarations: [
    ColorsComponent,
    TypographyComponent,
    ManageadComponent,
    ManageuserComponent,
    AdduserComponent,
    PostadComponent,
    PosteventComponent,
    ManagecategoryComponent,
    ManagesubcategoryComponent,
    AddstateComponent,
    AddcityComponent,
    ResetpasswordComponent,
    UploadComponent,
    MultiUploadComponent,
    ManagebannerComponent,
    UpdateuserComponent,
    UpdateeventComponent,
    EventdetailviewComponent,
    EditcategoryComponent,
    EditsubcategoryComponent,
    AdbannerComponent,
    EditbannerComponent,
    AddetailsviewComponent,
    UserdetailsviewComponent,
    SettingComponent,
    BannerdetailsviewComponent,
    ManageeventComponent,
    CreateplanComponent,
    AssignplanComponent,
    StripeComponent,
    WorkflowdetailsComponent,
    AdblogComponent,

    ManageblogComponent,
    BlogdetailsviewComponent,
    //PostjobComponent,
    InboxmessageComponent,
    ManagejobsComponent,
    MultifileuploadComponent,
    LoaderComponent,
    MessageBoardComponent,
    ManagesmalladComponent,
    PostsmalladComponent,
    SmalladdetailsviewComponent,
    ManagebuysellComponent,
    AddbuysellComponent,
    BuyselldetailsviewComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
 // providers: [UserService, AdDetailsService, ConfigurationService, CommonService, StateService, CityService]  
  providers: [UserService, AdDetailsService, ConfigurationService, CommonService, EventDetailsService, StateService, CityService//, LoaderService, { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }
  ]
})
export class ThemeModule { }
