import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../login/auth/auth.guard';
import { ColorsComponent } from './colors.component';
import { TypographyComponent } from './typography.component';
import { PostadComponent } from '../theme/postad/postad.component';
import { PosteventComponent } from '../theme/postevent/postevent.component';
import { ManageuserComponent } from '../theme/manageuser/manageuser.component';
import { AdduserComponent } from '../theme/adduser/adduser.component';
import { ManageadComponent } from '../theme/managead/managead.component';
import { ManageeventComponent } from '../theme/manageevent/manageevent.component';
import { EventdetailviewComponent} from '../theme/eventdetailview/eventdetailview.component';
import { ManagecategoryComponent } from '../theme/managecategory/managecategory.component';
import { ManagesubcategoryComponent } from '../theme/managesubcategory/managesubcategory.component';
import { AddstateComponent } from '../theme/addstate/addstate.component';
import { UpdateuserComponent }  from '../theme/updateuser/updateuser.component';
import { AddcityComponent } from '../theme/addcity/addcity.component';
import { ManagebannerComponent} from  '../theme/managebanner/managebanner.component';
import { ResetpasswordComponent } from '../theme/resetpassword/resetpassword.component';
import { EditcategoryComponent } from '../theme/editcategory/editcategory.component';
import { EditsubcategoryComponent} from '../theme/editsubcategory/editsubcategory.component';
import { EditbannerComponent} from '../theme/editbanner/editbanner.component';
import { AdbannerComponent} from '../theme/adbanner/adbanner.component';
import { AddetailsviewComponent } from '../theme/addetailsview/addetailsview.component';
import { UserdetailsviewComponent} from '../theme/userdetailsview/userdetailsview.component';
import { BannerdetailsviewComponent } from '../theme/bannerdetailsview/bannerdetailsview.component';
import { SettingComponent } from '../theme/setting/setting.component';
import { AppRole } from '../shared/common.model';
import {UpdateeventComponent} from '../theme/updateevent/updateevent.component';
import { CreateplanComponent } from '../theme/createplan/createplan.component';
import { AssignplanComponent } from '../theme/assignplan/assignplan.component';
//import { StripeComponent} from '../theme/stripe/stripe.component';
import { AdblogComponent } from '../theme/adblog/adblog.component';
import {BlogdetailsviewComponent} from '../theme/blogdetailsview/blogdetailsview.component';
import { ManageblogComponent } from '../theme/manageblog/manageblog.component';
//import { PostjobComponent} from '../theme/postjob/postjob.component';
import { ManagejobsComponent } from '../theme/managejobs/managejobs.component';
import { InboxmessageComponent } from '../theme/inboxmessage/inboxmessage.component';
import { MessageBoardComponent } from '../theme/message-board/message-board.component';
import { ManagesmalladComponent } from './managesmallad/managesmallad.component';
import { PostsmalladComponent } from './postsmallad/postsmallad.component';
import { SmalladdetailsviewComponent } from './smalladdetailsview/smalladdetailsview.component';
import {ManagebuysellComponent}  from '../theme/managebuysell/managebuysell.component';
import {AddbuysellComponent}  from '../theme/addbuysell/addbuysell.component';
import { BuyselldetailsviewComponent} from '../theme/buyselldetailsview/buyselldetailsview.component';

 
const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Theme'
    },
    children: [
      
      {
        path: 'postad',
        component: PostadComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Post New Business Listing',
          roles:[AppRole.SuperAdmin]
        }
      },

      {
        path: 'adduser',
        component: AdduserComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'adduser'
        }
      },

      {
        path: 'manageuser',
        component: ManageuserComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'manageuser'
        }
      },

      {
        path: 'addetailsview',
        component: AddetailsviewComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'addetailsview'
        }
      },
      {
        path: 'userdetailsview',
        component: UserdetailsviewComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'userdetailsview'
        }
      },
      {
        path: 'inboxmessage',
        component: InboxmessageComponent,
        data: {
          title: 'Inbox Message'
        }
      },
      {
        path: 'managead',
        component: ManageadComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Business Listings'
        }
      },
      {
        path: 'managesmallad',
        component: ManagesmalladComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Small Ads'
        }
      },
      {
        path: 'postsmallad',
        component: PostsmalladComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Small Ads'
        }
      },
      {
        path: 'smalladdetailsview',
        component: SmalladdetailsviewComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'smalladdetailsview'
        }
      },
      {
        path: 'manageevent',
        component: ManageeventComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Events'
        }
      },
      {
        path: 'postevent',
        component: PosteventComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Post New Event'
        }
      },
      {
        path: 'editbanner',
        component: EditbannerComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'EditbannerComponent'
        }
      },
      {
        path: 'adbanner',
        component: AdbannerComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'AdbannerComponent'
        }
      },
      {
        path: 'managecategory',
        component: ManagecategoryComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Managecategory'
        }
      },

      {
        path: 'addstate',
        component: AddstateComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'State'
        }
      },

      {
        path: 'addcity',
        component: AddcityComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'City'
        }
      },
      {
        path: 'setting',
        component: SettingComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Setting'
        }
      },

      {
        path: 'resetpassword',
        component: ResetpasswordComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Reset Password'
        }
      },

      {
        path: 'managebanner',
         component: ManagebannerComponent,
         canActivate: [AuthGuard],
         data: {
           title: 'managebanner'
         }
       },
 

      {
        path: 'updateuser',
        component: UpdateuserComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'updateuser'
        }
      },
      {
        path: 'updateevent',
        component: UpdateeventComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'updateevent'
        }
      },

      {
        path: 'eventdetailview',
        component: EventdetailviewComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'eventdetailview'
        }
      },      
      
      {
        path: 'managesubcategory',
        component: ManagesubcategoryComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Managesubsubcategory'
        }
      },
      {
        path: 'editcategory',
        component: EditcategoryComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'EditcategoryComponent'
        }
      },
      {
        path: 'editsubcategory',
        component: EditsubcategoryComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'EditsubcategoryComponent'
        }
      },
      {
        path: 'bannerdetailsview',
        component: BannerdetailsviewComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'bannerdetailsview'
        }
      },

      {
        path: 'updateuser',
        component: UpdateuserComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'updateuser'
        }
      },
  
      {
        path: 'createplan',
        component: CreateplanComponent,
        data: {
          title: 'Create Plan'
        }
      },
      
      {
        path: 'assignplan',
        component: AssignplanComponent,
        data: {
          title: 'Assign Plan'
        }
      },

      //{
      //  path: 'stripe',
      //  component: StripeComponent,
      //  data: {
      //    title: 'Stripe'
      //  }
      //},

      {
        path: 'manageblog',
        component: ManageblogComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Mangeblog'
        }
      },
      {
        path: 'adblog',
        component: AdblogComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Adblog'
        }
      },
{
        path: 'blogdetailsview',
        component: BlogdetailsviewComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'blogdetailsview'
        }
      },
      // {
      //   path: 'postjob',
      //   component: PostjobComponent,
      //   canActivate: [AuthGuard],
      //   data: {
      //     title: 'postjob'
      //   }
      // },
      {
        path: 'managejobs',
        component: ManagejobsComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Jobs'
        }
      },
      {
        path: 'message-board',
        component: MessageBoardComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'messageboard'
        }
      },
      {
        path: 'managebuysell',
        component: ManagebuysellComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'managebuysell'
        }
      },
      {
        path: 'addbuysell',
        component: AddbuysellComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'addbuysell'
        }
      },
      {
        path: 'buyselldetailsview',
        component: BuyselldetailsviewComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'buyselldetailsview'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThemeRoutingModule {}
