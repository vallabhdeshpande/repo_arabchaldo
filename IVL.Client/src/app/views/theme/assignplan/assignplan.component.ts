import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
//import { UserService} from '../user.service'
import { Observable, from, concat } from 'rxjs';
import { User } from '../user';
import { CommonService } from '../../shared/common.service'
import {Router, ActivatedRoute } from '@angular/router'
import { count, debounce, retry } from 'rxjs/operators';
import { log } from 'util';
import { Paymentplan } from '../paymentplan';
import { PaymentplanList } from '../paymentplan';
import { Transcationdetails } from '../transcationdetails';
import { Paymentplanservice} from '../paymentplanservice';
import { transcode } from 'buffer';
import { ToastrService } from'ngx-toastr'
import { error } from 'protractor';
import { DOCUMENT } from '@angular/common';
import { environment } from '../../../../environments/environment';
import { DatePipe } from '@angular/common';
import { OrderPipe } from 'ngx-order-pipe';
//import { NgxStripeModule } from 'ngx-stripe';
//import { StripeService, Elements, Element as StripeElement, ElementsOptions } from 'ngx-stripe'; 
import { StripeComponent } from '../stripe/stripe.component';
//import { document } from 'ngx-bootstrap/utils/public_api';
//import { isNumber } from 'ngx-bootstrap/chronos/utils/type-checks';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap'; 
import { Jsonp } from '@angular/http';

@Component({
  selector: 'app-assignplan',
  templateUrl: './assignplan.component.html',
  styleUrls: ['./assignplan.component.scss'],
  providers: [DatePipe, { provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }]
})
export class AssignplanComponent implements OnInit
{
  @ViewChild(StripeComponent, { static: false }) stripecmpt: StripeComponent;

  public stripeResponse = (event) => { this.stripepaymentresponse = event;}

  public stripepaymentresponse: string;
  public stripePaymentStatus: string;
  public stripePaymentAmount: number;
  public stripePaymentChargeId: string;
  public stripePaymentTokenId: string;
  public stripePaymentError: string;

  //// stripe start
  //elements: Elements;
  //card: StripeElement;
  //// optional parameters
  //elementsOptions: ElementsOptions = {
  //  locale: 'es'
  //};
  //stripeTest: FormGroup;
  ////stripe end

  assignPlanForm: any;
  order: string;
  reverse: boolean;

  public _allUser: Observable<User[]>;
  public _selectedEmail: Observable<User[]>;
  public _allUsersforBanner: Observable<User[]>;

  //private plan = Paymentplan;
  public plan = new Paymentplan;
  paymentplans: Paymentplan[];
  paymentplanList = new PaymentplanList;
  allpaymentplanList: PaymentplanList[];
  transDetails = new Transcationdetails();
  imagepath = environment.imagepath;
  paymentKey = environment.paymentkey;
  filter: string = "";
  viewPlanDetail: number = 0;
  createplan = true;
  planDetails = false;
  IsPlanPaid = false;
  setStatus: string = "";
  paymentAfterSaveButton = false;
  paymentsectionaftersave = false;

  planConfirmed = false;
  paymentsection = false;
  Title: string = "Create & Assign Plan";
  //private transcationDetails = new Transcationdetails;
  
  user = new User();
  Id: number;

  maxDate: string;
  minDate: string;
  //minValidTillDate: string;
  //maxValidFromDate: string;
  submitted = false;

  adTotal: number = 0;
  bannerTotal: number=0;
  eventTotal: number = 0 ;
  logoTotal: number = 0;
  JobsAdvTotal: number = 0;
  SmallAdsTotal: number = 0;
  BuySellTotal: number = 0;

  MiscellaneiousTotal: number = 0;
  count: number = 0;

  public pageSize: number = 10;
  public p: number;
  ads: string = "disabled"; 
  banners: boolean = false;
  events: boolean = false;

  amount: number=0;
  discount: number=0;
  finalAmount: number = 0;
  finalAmountAfterSave: number = 0;
  //payFinalAmount: number = 0;
  checkoutAmt: number = 0;
  lastPlanName: string;
  finalPlanName: string;
  sortedCollection: any[];
  UserId: string;

  AdChecked: string="";
  BannerChecked: string = "";
  EventChecked: string = "";
  LogoChecked: string = "";
  JobsAdvcheckbox: string = "";
  Miscellaneiouscheckbox: string = "";
  SmallAdsChecked: string = "";
  BuySellChecked: string = "";
  
  // stripe details
  tokenId: string = "";
  stripetoken: any;
  offlineDiv = false;
  OnlineDiv = false;
  OnlineDivAfterSave = false;
  offlineDivAfterSave = false;

  handler: any = null;
  planPaidStatus: string = "";
  CustomerEmail :string;
  CustomerName: string;
  PlanIdForStripe: number;
  minDateModel: NgbDateStruct;
  minValidTillDate: NgbDateStruct;
  maxValidFromDate: NgbDateStruct;
  users: any[];
  date: { year: number, month: number };
  hoveredDate: NgbDate;
  modelValidFrom: Date = null;
  modelValidTill: Date = null;
  validFrom: string = "";
  validTill: string = "";

  selectToday() {
    this.minDateModel = this.calendar.getToday();
    this.minValidTillDate = this.calendar.getToday();
  }
  onvalidFromDateSelect(event: any) {
    this.minValidTillDate = event;
    this.validFrom = this.modelValidFrom.toJSON().split('T')[0];

  }
  onvalidTillDateSelect(event: any) {
    this.maxValidFromDate = event;
    this.validTill = this.modelValidTill.toJSON().split('T')[0];

  }
  constructor(private orderPipe: OrderPipe, private formbuilder: FormBuilder, private _commonService: CommonService, private paymentplanservice: Paymentplanservice,
    private toastr: ToastrService, private router: Router, private route: ActivatedRoute, private datePipe: DatePipe, private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private config: NgbDatepickerConfig)
  {
    //handler: any = null;
    this.sortedCollection = orderPipe.transform(this.paymentplanList, 'PlanName');
    console.log(this.sortedCollection);
  }

  //// stripe ctt start
  //constructor(private orderPipe: OrderPipe, private formbuilder: FormBuilder, private _commonService: CommonService, private paymentplanservice: Paymentplanservice,
  //  private toastr: ToastrService, private router: Router, private route: ActivatedRoute, private datePipe: DatePipe, private fb: FormBuilder,
  //  private stripeService: StripeService) {
  //  //handler: any = null;
  //  this.sortedCollection = orderPipe.transform(this.paymentplanList, 'PlanName');
  //  console.log(this.sortedCollection);
  //}

  //// stripe ctt end

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }

  ngOnInit()
  {
    this.plan.planId = undefined;
    this.offlineDiv = false;
    this.OnlineDiv = false;
    if (this.viewPlanDetail == 0) {
      this.createplan = true;
      this.planDetails = false;
    }
    else {
      this.createplan = false;
      this.planDetails = true;
    }
    this.UserList();

    //this._commonService.getUserById(localStorage.userId).subscribe(userdetail => {
    //  var userdetail = JSON.parse(userdetail._body);

    //  console.log(userdetail);
    //  this.lastPlanName = userdetail.userId;
    //});

    this.AdChecked = "";
    this.BannerChecked = "";
    this.EventChecked = "";
    this.LogoChecked = "";
    this.Miscellaneiouscheckbox = "";
    this.assignPlanForm = this.formbuilder.group(
      {
        email: [''],
        userId: ['', Validators.required],
        Adcheckbox: [''],
        AdImageCount: ['', [Validators.pattern("^[0-9]*")]],
        AdImagePrice: ['', [Validators.pattern("^[0-9]*\d*(\.[0-9]+)?$")]],
        AdImageTotal: [''],
        Bannercheckbox: [''],
        BannerImageCount: ['', [Validators.pattern("^[0-9]*")]],
        BannerImagePrice: ['', [Validators.pattern("^[0-9]*\d*(\.[0-9]+)?$")]],
        BannerImageTotal: [''],
        Eventcheckbox: [''],
        EventImageCount: ['', [Validators.pattern("^[0-9]*")]],
        EventImagePrice: ['', [Validators.pattern("^[0-9]*\d*(\.[0-9]+)?$")]],
        EventImageTotal: [''],
        Logocheckbox: [''],
        LogoImageCount: ['', [Validators.pattern("^[0-9]*")]],
        LogoImagePrice: ['', [Validators.pattern("^[0-9]*\d*(\.[0-9]+)?$")]],
        LogoImageTotal: [''],
        JobsAdvcheckbox: [''],
        JobsAdvCount: [''],
        JobsAdvPrice: [''],
        JobsAdvTotal: [''],
        Miscellaneiouscheckbox: [''],
        MiscellaneiousTotal: [''],
        SmallAdscheckbox: [''],
        SmallAdsImageCount: ['', [Validators.pattern("^[0-9]*")]],
        SmallAdsImagePrice: ['', [Validators.pattern("^[0-9]*\d*(\.[0-9]+)?$")]],
        SmallAdsImageTotal: [''],
        BuySellcheckbox: [''],
        BuySellImageCount: ['', [Validators.pattern("^[0-9]*")]],
        BuySellImagePrice: ['', [Validators.pattern("^[0-9]*\d*(\.[0-9]+)?$")]],
        BuySellImageTotal: [''],
        description: [''],
        amount: [''],
        finalamount: [''],
        checkbox:[''],
        checkout: [''],
        ReceiptNo: [''],
        planName: [''],
        validfrom: [this.validFrom, Validators.required],
        validtill: [this.validTill, Validators.required],
        discount: ['', [Validators.pattern("^[0-9]*\d*(\.[0-9]+)?$")]],
        billingName: [''],
        billingEmail: ['', [Validators.email]],
        billingPhone: ['', Validators.minLength(10)],
        billingAddress1: [''],
        billingAddress2: [''],
        billingState: [''],
        billingZIP: [''],
      });
      
    //this.gettoday();
    this.selectToday();
    this.getAllPlan();
    if (this.viewPlanDetail == 0) {
      document.getElementById("ReceiptNo").setAttribute("disabled", "disabled");
      document.getElementById("checkout").setAttribute("disabled", "disabled");
    }        
  }

  //gettoday()
  //{
  //  var CurrentDate = new Date();
  //  this.minDate = CurrentDate.toJSON().split('T')[0];
  //  CurrentDate.setMonth(CurrentDate.getMonth() + 1);
  //  this.maxDate = CurrentDate.toJSON().split('T')[0];
  //}

  //onvalidFromDateSelect(event: any) {
  //  debugger;
  //  this.minValidTillDate = event.target.value;
  //  //this.minDate = this.minValidTillDate;
  //  //document.getElementById("#modalvalidTill").setAttribute("min", this.minValidTillDate);
  //}
  //onvalidTillDateSelect(event: any)
  //{
  //  this.maxValidFromDate = event.target.value;
  //}

  ngAfterViewInit()
  {
    //document.getElementById("#modalvalidFrom").setAttribute("min", this.minDate);
    //document.getElementById("#modalvalidTill").setAttribute("min", this.minValidTillDate);    
  }

  UserList()
  {
    this._allUser = this._commonService.UserDDL();
    console.log(this._allUser);
    this._commonService.UserInboxMessageDDL().subscribe(data => {
      debugger;
      this.users = data;

      var x = document.getElementsByClassName("ng-placeholder");
      x[0].innerHTML = "--Select User--";

    });
  }

  selectedEmailId(event: any)
  {
    debugger;
    this.Id = event.userId;
    this.assignPlanForm.value.email = '';
    console.log(this.Id);
    debugger;
    this._commonService.getUserById(this.Id).subscribe(userdetail => {
      var userdetail = JSON.parse(userdetail._body);

      this.UserId = userdetail.userId.toString();
      this.finalPlanName = this.UserId;
      console.log(userdetail);
      debugger;
     
      this.CustomerEmail = userdetail.email;
      this.CustomerName = userdetail.firstName + ' ' + userdetail.lastName

      this.assignPlanForm.patchValue({
        email: userdetail.email,
        userId: userdetail.userId.toString(),
        Adcheckbox: '',
        AdImageCount: 0,
        AdImagePrice: 0,
        AdImageTotal: 0,
        Bannercheckbox: '',
        BannerImageCount: 0,
        BannerImagePrice: 0,
        BannerImageTotal: 0,
        Eventcheckbox: '',
        EventImageCount: 0,
        EventImagePrice: 0,
        EventImageTotal: 0,
        Logocheckbox: '',
        LogoImageCount: 0,
        LogoImagePrice: 0,
        LogoImageTotal: 0,
        JobsAdvcheckbox: '',
        JobsAdvCount: 0,
        JobsAdvPrice: 0,
        JobsAdvTotal: 0,
        SmallAdscheckbox:'',
        SmallAdsImageCount:0,
        SmallAdsImagePrice: 0,
        BuySellcheckbox: '',
        BuySellImageCount:0,
        BuySellImagePrice:0,
        Miscellaneiouscheckbox: '',
        MiscellaneiousTotal: 0,
        amount: 0,
        finalamount: 0,
        checkout:0,
        planName: this.finalPlanName,
        validfrom: '',
        validtill: '',
        discount: 0
      });
      console.log(this.assignPlanForm.value.email);
    });
    console.log(this.assignPlanForm.value.email);
    var x = document.getElementsByClassName("ng-placeholder");
    x[0].innerHTML = "";
  }

  CheckBoxClicked(event: any)
  {
    debugger;
    console.log(event.target);
    if (event.target.id == "Adcheckbox")
    {
      debugger;
      if (event.target.checked == true)
      {
        this.count = this.count + 1;
        this.AdChecked = "/BL";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked  + this.Miscellaneiouscheckbox;
        this.assignPlanForm.patchValue({
          planName: this.finalPlanName          
        });
        document.getElementById("AdImageCount").removeAttribute("disabled");
        document.getElementById("AdImagePrice").removeAttribute("disabled");
      }

      else if (event.target.checked == false)
      {
        this.count = this.count -1;
        this.AdChecked = "";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked  + this.Miscellaneiouscheckbox;
        this.adTotal = 0;
        this.amount = this.adTotal + this.bannerTotal + this.eventTotal + this.logoTotal + this.JobsAdvTotal + this.MiscellaneiousTotal;
        this.finalAmount = this.amount - this.assignPlanForm.value.discount;
        this.assignPlanForm.patchValue({
          AdImageCount: 0,
          AdImagePrice: 0,
          AdImageTotal: 0,
          amount: this.amount,
          finalamount: this.finalAmount,
          planName: this.finalPlanName 
        });
        document.getElementById("AdImageCount").setAttribute("disabled", "disabled");
        document.getElementById("AdImagePrice").setAttribute("disabled", "disabled");
      }
    }

    else if (event.target.id == "Bannercheckbox")
    {
      if (event.target.checked == true)
      {
        this.count = this.count + 1;
        this.BannerChecked = "/Bnr";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked + this.Miscellaneiouscheckbox;
        this.assignPlanForm.patchValue({
          planName: this.finalPlanName
        });
        document.getElementById("BannerImageCount").removeAttribute("disabled");
        document.getElementById("BannerImagePrice").removeAttribute("disabled");
      }
      else if (event.target.checked == false)
      {
        this.count = this.count - 1;
        this.BannerChecked = "";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked + this.Miscellaneiouscheckbox;
        this.bannerTotal = 0;
        this.amount = this.adTotal + this.bannerTotal + this.eventTotal + this.logoTotal + this.JobsAdvTotal + this.MiscellaneiousTotal;
        this.finalAmount = this.amount - this.assignPlanForm.value.discount;
        this.assignPlanForm.patchValue({
          BannerImageCount: 0,
          BannerImagePrice: 0,
          BannerImageTotal: 0,
          amount: this.amount,
          finalamount: this.finalAmount,
          planName: this.finalPlanName
        });
        document.getElementById("BannerImageCount").setAttribute("disabled", "disabled");
        document.getElementById("BannerImagePrice").setAttribute("disabled", "disabled");
      }
    }

    else if (event.target.id == "Eventcheckbox")
    {
      if (event.target.checked == true)
      {
        this.count = this.count + 1;
        this.EventChecked = "/Event";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked + this.Miscellaneiouscheckbox;
        this.assignPlanForm.patchValue({
          planName: this.finalPlanName
        });
        document.getElementById("EventImageCount").removeAttribute("disabled");
        document.getElementById("EventImagePrice").removeAttribute("disabled");
      }
      else if (event.target.checked == false)
      {
        this.count = this.count - 1;
        this.EventChecked = "";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked + this.Miscellaneiouscheckbox;
        this.eventTotal = 0;
        this.amount = this.adTotal + this.bannerTotal + this.eventTotal + this.logoTotal + this.JobsAdvTotal + this.MiscellaneiousTotal;
        this.finalAmount = this.amount - this.assignPlanForm.value.discount;
        this.assignPlanForm.patchValue({
          EventImageCount: 0,
          EventImagePrice: 0,
          EventImageTotal: 0,
          amount: this.amount,
          finalamount: this.finalAmount,
          planName: this.finalPlanName
        });
        document.getElementById("EventImageCount").setAttribute("disabled", "disabled");
        document.getElementById("EventImagePrice").setAttribute("disabled", "disabled");
      }
    }

    else if (event.target.id == "Logocheckbox")
    {
      if (event.target.checked == true)
      {
        this.count = this.count + 1;
        this.LogoChecked = "/Logo";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked + this.Miscellaneiouscheckbox;
        this.assignPlanForm.patchValue({
          planName: this.finalPlanName
        });
        document.getElementById("LogoImageCount").removeAttribute("disabled");
        document.getElementById("LogoImagePrice").removeAttribute("disabled");
      }
      else if (event.target.checked == false)
      {
        this.count = this.count - 1;
        this.LogoChecked = "";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked + this.Miscellaneiouscheckbox;
        this.logoTotal = 0;
        this.amount = this.adTotal + this.bannerTotal + this.eventTotal + this.logoTotal + this.JobsAdvTotal + this.MiscellaneiousTotal;
        this.finalAmount = this.amount - this.assignPlanForm.value.discount;
        this.assignPlanForm.patchValue({
          LogoImageCount: 0,
          LogoImagePrice: 0,
          LogoImageTotal: 0,
          amount: this.amount,
          finalamount: this.finalAmount,
          planName: this.finalPlanName
        });
        document.getElementById("LogoImageCount").setAttribute("disabled", "disabled");
        document.getElementById("LogoImagePrice").setAttribute("disabled", "disabled");
      }
    }

    else if (event.target.id == "JobsAdvcheckbox")
    {
      if (event.target.checked == true)
      {
        this.count = this.count + 1;
        this.JobsAdvcheckbox = "/Job";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked + this.Miscellaneiouscheckbox;
        this.assignPlanForm.patchValue({
          planName: this.finalPlanName
        });
        document.getElementById("JobsAdvCount").removeAttribute("disabled");
        document.getElementById("JobsAdvPrice").removeAttribute("disabled");
      }
      else if (event.target.checked == false)
      {
        this.count = this.count - 1;
        this.JobsAdvcheckbox = "";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked + this.Miscellaneiouscheckbox;
        this.logoTotal = 0;
        this.amount = this.adTotal + this.bannerTotal + this.eventTotal + this.logoTotal + this.JobsAdvTotal + this.MiscellaneiousTotal;
        this.finalAmount = this.amount - this.assignPlanForm.value.discount;
        this.assignPlanForm.patchValue({
          JobsAdvCount: 0,
          JobsAdvPrice: 0,
          JobsAdvTotal: 0,
          amount: this.amount,
          finalamount: this.finalAmount,
          planName: this.finalPlanName
        });
        document.getElementById("JobsAdvCount").setAttribute("disabled", "disabled");
        document.getElementById("JobsAdvPrice").setAttribute("disabled", "disabled");
      }
    }

    else if (event.target.id == "Miscellaneiouscheckbox")
    {
      if (event.target.checked == true)
      {
        this.count = this.count + 1;
        this.Miscellaneiouscheckbox = "/Msc";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked + this.Miscellaneiouscheckbox;
        this.assignPlanForm.patchValue({
          planName: this.finalPlanName
        });
        document.getElementById("MiscellaneiousTotal").removeAttribute("disabled");
      }
      else if (event.target.checked == false)
      {
        this.count = this.count - 1;
        this.Miscellaneiouscheckbox = "";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked + this.Miscellaneiouscheckbox;
        this.MiscellaneiousTotal = 0;
        this.amount = this.adTotal + this.bannerTotal + this.eventTotal + this.logoTotal + this.JobsAdvTotal + this.MiscellaneiousTotal ;
        this.finalAmount = this.amount - this.assignPlanForm.value.discount;
        this.assignPlanForm.patchValue({
          MiscellaneiousTotal: 0,
          amount: this.amount,
          finalamount: this.finalAmount,
          planName: this.finalPlanName
        });
        document.getElementById("MiscellaneiousTotal").setAttribute("disabled", "disabled");
      }
    }

    else if (event.target.id == "SmallAdscheckbox") {
      if (event.target.checked == true) {
        this.count = this.count + 1;
        this.SmallAdsChecked = "/Adv";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked + this.Miscellaneiouscheckbox;
        this.assignPlanForm.patchValue({
          planName: this.finalPlanName
        });
        document.getElementById("SmallAdsImageCount").removeAttribute("disabled");
        document.getElementById("SmallAdsImagePrice").removeAttribute("disabled");
      }
      else if (event.target.checked == false) {
        this.count = this.count - 1;
        this.SmallAdsChecked = "";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked + this.Miscellaneiouscheckbox;
        this.SmallAdsTotal = 0;
        this.amount = this.adTotal + this.bannerTotal + this.eventTotal + this.logoTotal + this.JobsAdvTotal + this.SmallAdsTotal + this.BuySellTotal + this.MiscellaneiousTotal;
        this.finalAmount = this.amount - this.assignPlanForm.value.discount;
        this.assignPlanForm.patchValue({
          SmallAdsImageCount: 0,
          SmallAdsImagePrice: 0,
          SmallAdsImageTotal: 0,
          amount: this.amount,
          finalamount: this.finalAmount,
          planName: this.finalPlanName
        });
        document.getElementById("SmallAdsImageCount").setAttribute("disabled", "disabled");
        document.getElementById("SmallAdsImagePrice").setAttribute("disabled", "disabled");
      }
    }

    else if (event.target.id == "BuySellcheckbox") {
      if (event.target.checked == true) {
        this.count = this.count + 1;
        this.BuySellChecked = "/BS";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked + this.Miscellaneiouscheckbox;
        this.assignPlanForm.patchValue({
          planName: this.finalPlanName
        });
        document.getElementById("BuySellImageCount").removeAttribute("disabled");
        document.getElementById("BuySellImagePrice").removeAttribute("disabled");
      }
      else if (event.target.checked == false) {
        this.count = this.count - 1;
        this.BuySellChecked = "";
        this.finalPlanName = this.UserId + this.AdChecked + this.BannerChecked + this.EventChecked + this.LogoChecked + this.JobsAdvcheckbox + this.SmallAdsChecked + this.BuySellChecked + this.Miscellaneiouscheckbox;
        this.BuySellTotal = 0;
        this.amount = this.adTotal + this.bannerTotal + this.eventTotal + this.logoTotal + this.JobsAdvTotal + this.SmallAdsTotal + this.BuySellTotal + this.MiscellaneiousTotal;
        this.finalAmount = this.amount - this.assignPlanForm.value.discount;
        this.assignPlanForm.patchValue({
          BuySellImageCount: 0,
          BuySellImagePrice: 0,
          BuySellImageTotal: 0,
          amount: this.amount,
          finalamount: this.finalAmount,
          planName: this.finalPlanName
        });
        document.getElementById("BuySellImageCount").setAttribute("disabled", "disabled");
        document.getElementById("BuySellImagePrice").setAttribute("disabled", "disabled");
      }
    }

  }

  enableAds()
  {
    debugger
    if (this.ads == "disabled") this.ads = "";
    else this.ads = "disabled";
  }  

  //FinalAmount(changeEvent: string, $event: any)
  Calculation(event: any)
  {
    console.log(this.assignPlanForm.value);
        
    if (event.target.id == "AdImageCount")
    {
      this.adTotal = event.target.value * this.assignPlanForm.value.AdImagePrice;
    }
    else if (event.target.id == "AdImagePrice")
    {
      this.adTotal = event.target.value * this.assignPlanForm.value.AdImageCount;
    }
    else if (event.target.id == "BannerImageCount")
    {
      this.bannerTotal = event.target.value * this.assignPlanForm.value.BannerImagePrice;
    }
    else if (event.target.id == "BannerImagePrice")
    {
      this.bannerTotal = event.target.value * this.assignPlanForm.value.BannerImageCount;
    }
    else if (event.target.id == "EventImageCount")
    {
      this.eventTotal = event.target.value * this.assignPlanForm.value.EventImagePrice;
    }
    else if (event.target.id == "EventImagePrice")
    {
      this.eventTotal = event.target.value * this.assignPlanForm.value.EventImageCount;
    }
    else if (event.target.id == "LogoImageCount")
    {
      this.logoTotal = event.target.value * this.assignPlanForm.value.LogoImagePrice;
    }
    else if (event.target.id == "LogoImagePrice")
    {
      this.logoTotal = event.target.value * this.assignPlanForm.value.LogoImageCount;
    }
    else if (event.target.id == "JobsAdvCount")
    {
      this.JobsAdvTotal = event.target.value * this.assignPlanForm.value.JobsAdvPrice;
    }
    else if (event.target.id == "JobsAdvPrice")
    {
      this.JobsAdvTotal = event.target.value * this.assignPlanForm.value.JobsAdvCount;
    }
    else if (event.target.id == "SmallAdsImageCount") {
      this.SmallAdsTotal = event.target.value * this.assignPlanForm.value.SmallAdsImagePrice;
    }
    else if (event.target.id == "SmallAdsImagePrice") {
      this.SmallAdsTotal = event.target.value * this.assignPlanForm.value.SmallAdsImageCount;
    }
    else if (event.target.id == "BuySellImageCount") {
      this.BuySellTotal = event.target.value * this.assignPlanForm.value.BuySellImagePrice;
    }
    else if (event.target.id == "BuySellImagePrice") {
      this.BuySellTotal = event.target.value * this.assignPlanForm.value.BuySellImageCount;
    }


    else if (event.target.id == "MiscellaneiousTotal")
    {
      this.MiscellaneiousTotal = 1 * this.assignPlanForm.value.MiscellaneiousTotal;
    }

    this.amount = this.adTotal + this.bannerTotal + this.eventTotal + this.logoTotal + this.JobsAdvTotal + this.SmallAdsTotal + this.BuySellTotal + this.MiscellaneiousTotal;
    
    if ((this.assignPlanForm.value.discount != 0) &&((this.amount -this.assignPlanForm.value.discount)  <= 0.5))
    {
      if ((this.amount - this.assignPlanForm.value.discount) < 0)
      {
        this.toastr.error('Discount cannot be more than Amount.');
        this.assignPlanForm.value.discount = 0;
        this.assignPlanForm.patchValue({
          discount: 0
        })
        return;
      }
      this.toastr.warning('(Amount- Discount) must be greater than 50 cents for onliine payment.');
      
    }
    this.finalAmount = this.amount - this.assignPlanForm.value.discount;
    this.finalAmount = parseFloat(this.finalAmount.toFixed(2));
    //this.finalAmount = Math.floor((this.finalAmount * 1000) / 1000);
    this.checkoutAmt = this.finalAmount; 
    this.assignPlanForm.patchValue({
      AdImageTotal: this.adTotal,
      BannerImageTotal: this.bannerTotal,
      EventImageTotal: this.eventTotal,
      LogoImageTotal: this.logoTotal,
      JobsAdvTotal: this.JobsAdvTotal,
      SmallAdsImageTotal: this.SmallAdsTotal,
      BuySellImageTotal: this.BuySellTotal,
      MiscellaneiousTotal: this.MiscellaneiousTotal,
      amount: this.amount,
      finalamount: this.finalAmount,
      checkout: this.checkoutAmt
    });
  }

  confirmPlan(event: any) {
    debugger;
    this.submitted = true;
    console.log(event.target.checked);
    console.log(this.assignPlanForm.value.checkbox);

    if (this.assignPlanForm.invalid) {
      this.toastr.warning("Please make sure mandatory informations are provided.");
      this.assignPlanForm.patchValue({
        checkbox: false
      });

      this.assignPlanForm.value.checkbox = false;
      return;
    }

    console.log(event.target.checked);
    if (event.target.checked === true)
    {
      

      // stop the process here if form is invalid
      //if (this.assignPlanForm.invalid)
      //{
      //  this.assignPlanForm.value.checkbox = false;
      //  return;
      //}

      if (this.count == 0) {
        this.toastr.warning("No plans selected.Please Select any combination to submit plan");
        this.assignPlanForm.patchValue({
          checkbox: false
        });
        this.assignPlanForm.value.checkbox = false;
        return;
      }

      document.getElementById("checkout").removeAttribute("disabled");

      //(<HTMLInputElement>document.getElementById("description")).disabled = true;
      document.getElementById("description").setAttribute("disabled", "disabled");
      //this.paymentsection = true;
      this.toastr.success("Plan validated successfully. Please save plan to make payment");
    }

    else if (event.target.checked === false) {
      //this.editPlan();
      document.getElementById("checkout").setAttribute("disabled", "disabled");
      document.getElementById("description").removeAttribute("disabled");
      this.paymentsection = false;
    }

  }

  editPlan() {
    document.getElementById("description").removeAttribute("disabled");
    this.paymentsection = false;
  }

  addPlan(): void
  {
    //debugger
    this.submitted = true;
    
     // stop the process here if form is invalid
    if (this.assignPlanForm.invalid)
    {
      return;
    }

    if (this.count == 0)
    {
      this.toastr.warning("No plans selected.Please select any combination to submit plan");
      return;
    }
    //console.log(localStorage.value);
    this.plan.UserId = localStorage.userId;

    this.plan.UserId = this.Id;
    this.plan.PlanName = this.assignPlanForm.value.userId;
    this.plan.adImageCount = this.assignPlanForm.value.AdImageCount;
    this.plan.AdImagePrice = this.assignPlanForm.value.AdImagePrice;
    this.plan.bannerImageCount = this.assignPlanForm.value.BannerImageCount;
    this.plan.BannerImagePrice = this.assignPlanForm.value.BannerImagePrice;
    this.plan.eventImageCount = this.assignPlanForm.value.EventImageCount;
    this.plan.EventImagePrice = this.assignPlanForm.value.EventImagePrice;
    this.plan.logoCount = this.assignPlanForm.value.LogoImageCount;
    this.plan.LogoPrice = this.assignPlanForm.value.LogoImagePrice;
    this.plan.CreatedBy = localStorage.userId;
    this.plan.UpdatedBy = localStorage.userId;
    this.plan.ValidFrom = this.assignPlanForm.value.validfrom;
    this.plan.ValidTill = this.assignPlanForm.value.validtill;
    this.plan.Amount = this.assignPlanForm.value.amount
    this.plan.Discount = this.assignPlanForm.value.discount;
    this.plan.FinalAmount = this.assignPlanForm.value.finalamount;
    this.plan.PlanName = this.assignPlanForm.value.planName;
    this.plan.Description = this.assignPlanForm.value.description;
    this.plan.JobsAdvCount = this.assignPlanForm.value.JobsAdvCount;
    this.plan.JobsAdvPrice = this.assignPlanForm.value.JobsAdvPrice;
    this.plan.SmallAdsCount = this.assignPlanForm.value.SmallAdsImageCount;
    this.plan.SmallAdsPrice = this.assignPlanForm.value.SmallAdsImagePrice;
    this.plan.BuySellCount = this.assignPlanForm.value.BuySellImageCount;
    this.plan.BuySellPrice = this.assignPlanForm.value.BuySellImagePrice;
    this.plan.MiscellaneiousTotal = this.assignPlanForm.value.MiscellaneiousTotal;

    this.paymentplanservice.addUserPlan(this.plan)
      .subscribe((response) => {
        console.log(JSON.parse((<any>response)._body));
        console.log(JSON.parse((<any>response)._body).planId);
        var planIdResponse = JSON.parse((<any>response)._body);
        console.log(planIdResponse);
        console.log(response);
        
        debugger;
        console.log(planIdResponse.planSaved);
        if (planIdResponse.planSaved === true)
        {
          this.PlanIdForStripe = planIdResponse.planId;
          console.log(planIdResponse.planId);
          console.log(this.PlanIdForStripe);
          this.paymentsection = true;
          this.toastr.success('Plan successfully added');
          document.getElementById('checkout').setAttribute("disabled", "disabled");
          document.getElementById('checkbox').setAttribute("disabled", "disabled");
        }
        else
        {
          this.toastr.success('Error while saving plan. Please try once again.');
        }
        
        //this.toastr.success('plan successfully added');
        //if()
        //this.paymentsection = true;
        //this.router.navigate(['/dashboard']);
    },
      (error) => {
        this.toastr.error('Problem with service. Please try again later!');
      }
    );
    //this.assignPlanForm.reset();
    //this.submitted = false;
  }

  OfflinePayment()
  {    
    debugger;
    this.submitted = true;

    if (this.assignPlanForm.invalid) {
      //this.toastr.warning("Validation error. Please check validation message.");
      return;
    }

    //if ((this.assignPlanForm.invalid) && (this.assignPlanForm.value.billingName == "" || this.assignPlanForm.value.billingEmail == "" || this.assignPlanForm.value.billingPhone == "" ||
    //  this.assignPlanForm.value.billingAddress1 == "" || this.assignPlanForm.value.billingAddress2 == "" || this.assignPlanForm.value.billingState == "" || this.assignPlanForm.value.billingZIP == "")) {
    //  this.toastr.warning("Mandatory fields missing.");
    //  return;
    //}    

    //if (this.plan.paymentMode == "Offline" && (this.assignPlanForm.value.ReceiptNo == null || this.assignPlanForm.value.ReceiptNo == ""))
    //{
    //  this.toastr.warning("Please enter receipt number to save subscription plan ");
    //  return;
    //}

    this.transDetails.name = this.assignPlanForm.value.billingName;
    this.transDetails.email = this.assignPlanForm.value.billingEmail;
    this.transDetails.phone = this.assignPlanForm.value.billingPhone;
    this.transDetails.address1 = this.assignPlanForm.value.billingAddress1;
    this.transDetails.address2 = this.assignPlanForm.value.billingAddress2;
    this.transDetails.state = this.assignPlanForm.value.billingState;
    this.transDetails.zip = this.assignPlanForm.value.billingZIP;
    this.transDetails.planId = this.PlanIdForStripe;
    this.transDetails.receiptId = this.assignPlanForm.value.ReceiptNo;
    this.transDetails.amount = this.assignPlanForm.value.finalamount;

    let offlinePaymentDetails =
    {
      name: this.transDetails.name,
      email: this.transDetails.email,
      planId: this.PlanIdForStripe,
      receiptId: this.transDetails.receiptId,
      amount: this.transDetails.amount,
      phone: this.transDetails.phone,
      address1: this.transDetails.address1,
      address2: this.transDetails.address2,
      state: this.transDetails.state,
      ZIP: this.transDetails.zip,

    }

    var success = this.paymentplanservice.OfflinePayment(offlinePaymentDetails);

    if (success === true) {
      this.toastr.success("Offline payment sucessfully done");
      this.router.navigate(['/dashboard']);
    }
  }

  onlinePaymentResponse($event)
  {
    debugger;
    this.stripePaymentStatus = $event.paymentStatus;
    
    if (this.stripePaymentStatus == "true")
    {
      //this.actionMessage = "pop up Payment successfully done";
      //document.getElementById('DeletePopUp').attributes.
      //document.getElementById("DeletePopUp").click();

      //this.stripePaymentAmount = $event.paymentamount;
      //this.stripePaymentChargeId = $event.paymentchargeid;
      //this.stripePaymentTokenId = $event.paymenttokenid;
      //this.addPlan();
      this.toastr.success('Payment successfully done');
      this.router.navigate(['/dashboard']);

    }
    
    else if (this.stripePaymentStatus == "false")
    {
      this.stripePaymentError = $event.errorMessage;
      console.log(this.stripePaymentError);
      this.toastr.error(this.stripePaymentError);
      return;
    }

  }

  get f() { return this.assignPlanForm.controls; }

  selected(event: any)
  {
    debugger;
    console.log(event);

    if (event == "Offline")
    {
      this.offlineDiv = true;
      this.OnlineDiv = false;
      this.plan.paymentMode = "Offline";

      this.assignPlanForm.get('billingName').setValidators(Validators.required);
      this.assignPlanForm.get('billingName').updateValueAndValidity();
      this.assignPlanForm.get('validfrom').clearValidators();
      this.assignPlanForm.get('validfrom').updateValueAndValidity();
      this.assignPlanForm.get('validtill').clearValidators();
      this.assignPlanForm.get('validtill').updateValueAndValidity();
      this.assignPlanForm.get('userId').clearValidators();
      this.assignPlanForm.get('userId').updateValueAndValidity();

      this.assignPlanForm.get('billingEmail').setValidators(Validators.required);
      this.assignPlanForm.get('billingEmail').updateValueAndValidity();
      this.assignPlanForm.get('billingPhone').setValidators(Validators.required);
      this.assignPlanForm.get('billingPhone').updateValueAndValidity();
      this.assignPlanForm.get('billingAddress1').setValidators(Validators.required);
      this.assignPlanForm.get('billingAddress1').updateValueAndValidity();
      this.assignPlanForm.get('billingAddress2').setValidators(Validators.required);
      this.assignPlanForm.get('billingAddress2').updateValueAndValidity();
      this.assignPlanForm.get('billingState').setValidators(Validators.required);
      this.assignPlanForm.get('billingState').updateValueAndValidity();
      this.assignPlanForm.get('billingZIP').setValidators(Validators.required);
      this.assignPlanForm.get('billingZIP').updateValueAndValidity();
      this.assignPlanForm.get('ReceiptNo').setValidators(Validators.required);
      this.assignPlanForm.get('ReceiptNo').updateValueAndValidity();

      //document.getElementById("ReceiptNo").removeAttribute("disabled");
      //document.getElementById("checkout").setAttribute("disabled", "disabled");      
    }
    else if (event == "Online")
    {
      this.OnlineDiv = true;
      this.offlineDiv = false;

      this.plan.paymentMode = "Online";
      document.getElementById("checkout").removeAttribute("disabled");
      document.getElementById("ReceiptNo").setAttribute("disabled", "disabled");
    }
  } 

  getAllPlan():void
  {
    this.paymentplanservice.GetAllPlan().subscribe(plandetails => {
      plandetails = plandetails.json();
      console.log(plandetails);
      debugger;

      var length = plandetails.length;

    
      
      for (var i = 0; i < length; i++)
      {
        if (plandetails[i].IsPaid == "True")
        {
          plandetails[i].IsPaid = "Paid";
        }
        else
          plandetails[i].IsPaid = "Not Paid";

      //  //this.allpaymentplanList[i].ValidFromConverted = this.datePipe.transform(this.allpaymentplanList[i].ValidFrom, 'yyyy-MMM-dd');
      //  //this.allpaymentplanList[i].ValidTillConverted = this.datePipe.transform(this.allpaymentplanList[i].ValidTill, 'dd-MMM-yyyy');

      //  this.allpaymentplanList[i].ValidFrom = this.datePipe.transform(this.allpaymentplanList[i].ValidFrom, 'yyyy-MMM-dd');
      //  this.allpaymentplanList[i].ValidTillConverted = this.datePipe.transform(this.allpaymentplanList[i].ValidTill, 'dd-MMM-yyyy');
      }
      this.allpaymentplanList = plandetails;
    });
    //this.paymentplanservice.GetAllPlan();
  }

  getPicUrl(picurl: string, userId)
  {
    if (picurl == null)
      return "assets/img/default/no_image_placeholder.jpg";
    else if (picurl == "")
      return "assets/img/default/no_image_placeholder.jpg";
    else
      return this.imagepath + 'UserProfile/' + userId + '/ProfilePic/' + picurl;
  }

  showPlanDetails(plandetail:PaymentplanList)
  {
    this.IsPlanPaid = false;
    this.paymentAfterSaveButton = false;
    this.paymentsectionaftersave = false;
    this.Title = "Plan Details";
    //debugger;
    this.createplan = false; 
    this.planDetails = true;
    console.log(plandetail.PlanId);

    this.paymentplanservice.getPlanDetails(plandetail.PlanId).subscribe(data => {
      data = data.json();
      this.plan = data;
      console.log(data);
      console.log(this.plan);

      //data.validFrom = data.validFrom
      data.validFrom = this.datePipe.transform(data.validFrom, 'yyyy-MM-dd');
      data.validTill = this.datePipe.transform(data.validTill, 'yyyy-MM-dd');
      this._allUser = this._commonService.UserDDL();
      this.assignPlanForm.patchValue({
        email: plandetail.Email,

        AdImageCount: data.adImageCount,
        AdImagePrice: data.adImagePrice,
        AdImageTotal: data.adImageCount * data.adImagePrice,
        BannerImageCount: data.bannerImageCount,
        BannerImagePrice: data.bannerImagePrice,
        BannerImageTotal: data.bannerImageCount * data.bannerImagePrice,
        EventImageCount: data.eventImageCount,
        EventImagePrice: data.eventImagePrice,
        EventImageTotal: data.eventImageCount * data.eventImagePrice,
        LogoImageCount: data.logoCount,
        LogoImagePrice: data.logoPrice,
        LogoImageTotal: data.logoCount * data.logoPrice,
        JobsAdvCount: data.jobsAdvCount,
        JobsAdvPrice: data.jobsAdvPrice,
        JobsAdvTotal: data.jobsAdvCount * data.jobsAdvPrice,
        SmallAdsImageCount: data.smallAdsCount,
        SmallAdsImagePrice: data.smallAdsPrice,
        SmallAdsImageTotal: data.smallAdsCount * data.smallAdsPrice,
        BuySellImageCount: data.buySellCount,
        BuySellImagePrice: data.buySellPrice,
        BuySellImageTotal: data.buySellCount * data.buySellPrice,
        MiscellaneiousTotal: data.miscellaneiousTotal,
        description: data.description,
        amount: data.amount,
        finalamount: data.finalAmount,
        planName: data.planName,
        validfrom: data.validFrom,
        validtill: data.validTill,
        discount: data.discount,
        userId: plandetail.FirstName + " " + plandetail.LastName
      });

      this.finalAmount = data.finalAmount;
      this.CustomerEmail = plandetail.Email;
      this.CustomerName = "demo 12345";
      this.PlanIdForStripe = data.planId;

      if (this.plan.isPaid === false) {
        debugger;
        this.finalAmountAfterSave = data.finalAmount;
        this.IsPlanPaid = true;
        //document.getElementById("checkoutAfterSave").removeAttribute("disabled");
      }


    });
  }

  payAfterSaveButton() {
    debugger;
    this.paymentAfterSaveButton = true;
    this.OnlineDivAfterSave = false;
    this.offlineDivAfterSave = false;
  }

  selectedAfterSave(event: any) {
    debugger;
    console.log(event);

    if (event == "Offline") {
      this.offlineDivAfterSave = true;
      this.OnlineDivAfterSave = false;
      this.plan.paymentMode = "Offline";
      this.assignPlanForm.get('billingName').setValidators(Validators.required);
      this.assignPlanForm.get('billingName').updateValueAndValidity();
      this.assignPlanForm.get('validfrom').clearValidators();
      this.assignPlanForm.get('validfrom').updateValueAndValidity();
      this.assignPlanForm.get('validtill').clearValidators();
      this.assignPlanForm.get('validtill').updateValueAndValidity();
      this.assignPlanForm.get('userId').clearValidators();
      this.assignPlanForm.get('userId').updateValueAndValidity();

      this.assignPlanForm.get('billingEmail').setValidators(Validators.required);
      this.assignPlanForm.get('billingEmail').updateValueAndValidity();
      this.assignPlanForm.get('billingPhone').setValidators(Validators.required);
      this.assignPlanForm.get('billingPhone').updateValueAndValidity();
      this.assignPlanForm.get('billingAddress1').setValidators(Validators.required);
      this.assignPlanForm.get('billingAddress1').updateValueAndValidity();
      this.assignPlanForm.get('billingAddress2').setValidators(Validators.required);
      this.assignPlanForm.get('billingAddress2').updateValueAndValidity();
      this.assignPlanForm.get('billingState').setValidators(Validators.required);
      this.assignPlanForm.get('billingState').updateValueAndValidity();
      this.assignPlanForm.get('billingZIP').setValidators(Validators.required);
      this.assignPlanForm.get('billingZIP').updateValueAndValidity();
      this.assignPlanForm.get('ReceiptNo').setValidators(Validators.required);
      this.assignPlanForm.get('ReceiptNo').updateValueAndValidity();
      
    }
    else if (event == "Online") {
      this.OnlineDivAfterSave = true;
      this.offlineDivAfterSave = false;

      this.plan.paymentMode = "Online";
      //document.getElementById("checkout").removeAttribute("disabled");
      //document.getElementById("ReceiptNo").setAttribute("disabled", "disabled");
    }
  }

  paymentForPlanAfterSave()
  {

    //this.actionMessage = "pop up Payment successfully done";
    //document.getElementById('DeletePopUp').attributes.
    //document.getElementById("openModalButton").click();
    //document.getElementById("DeletePopUp").click();

    debugger; 
    this.paymentsectionaftersave = true;
    this.OnlineDivAfterSave = false;
    this.offlineDivAfterSave = false;
  }

  planDetailCancel()
  {
    this.Title = "Assign Plan";
    this.viewPlanDetail = 0;
    this.ngOnInit();
  }

  getStatus(Status: string) {

    if (Status == "Paid")
      return this.setStatus = "badge badge-success";

    if (Status == "Not Paid")
      return this.setStatus = "badge badge-danger";

  }
}
