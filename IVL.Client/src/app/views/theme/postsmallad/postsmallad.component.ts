
import { Component, OnInit, Inject, Output, EventEmitter, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/filter';
import { Country, State, City, Roles, CountryCodes } from '../../shared/common.model';
import { User } from '../user';
import { UserService } from '../user.service';
import { SmallAdDetails, SmallAdImageDetails } from '../smalladdetails.model';
import { SmallAdDetailsService } from '../smalladdetails.service';
import { CommonService } from '../../shared/common.service';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { environment } from '../../../../environments/environment';
import { element } from 'protractor';
import { first, count } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Paymentplan } from '../paymentplan';
import { Paymentplanservice } from '../paymentplanservice';
import { UploadComponent } from '../upload/upload.component';
import { MultiUploadComponent } from '../multiupload/multiupload.component';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import * as countryCodeData from '../../shared/countrycodes.json';
@Component({
  selector: 'app-postsmallad',
  templateUrl: './postsmallad.component.html',
  styleUrls: ['./postsmallad.component.scss']
})




export class PostsmalladComponent implements OnInit {
  @ViewChild(UploadComponent, { static: false }) UploadCmpt: UploadComponent;
  @ViewChild(MultiUploadComponent, { static: false }) MultiUploadCmpt: MultiUploadComponent;
  public _allUser: Observable<User[]>;
  public _allCountry: Observable<Country[]>;
  public _allCountryCodes: Observable<CountryCodes[]>;
  public _allState: Observable<State[]>;
  public _allStateNew: Observable<State[]>;
  public _allCity: Observable<City[]>;

  public _allImagedata: Observable<SmallAdImageDetails[]>;
  public _allroles: Observable<Roles[]>;
  postedFor: string = "0";
  CountryId: string = "0";
  StateId: string = "0";
  CityId: string = "0";
  states: any[];
  cities: any[];
  users: any[];
  
  RoleId: string = "0";
  PostedForName: string = "";
  PostedForId: string = "0";
  CountryName: string = "";
  StateName: string = "";
  CityName: string = "";
 
  logopath: string[];
  imagepath: any[];
  logoUrl: string = "";
  imageUrl: string = "";
  ABCD: string[];
 
  //dvImage: string[];
  FormPostSmallAd: any;
  actionButton;
  action = "";
  myData = null;
  formData = null;
  msgs = null;
  public imagefilename: string[];
  public logofilename: string[];
  AdPost: SmallAdDetails[];
  adPost = new SmallAdDetails();
  adimgdtls = new SmallAdImageDetails();
  AdImageDetailsDTO = new SmallAdImageDetails();
  statusMessage: string;
  saveButtonText: string = "Save";
  displayText = 'btn btn-sm btn-primary';
  displayExtLogo = 'col-md-6 d-none';
  displayExtImages = 'col-md-6 d-none';
  displayExtUploads = 'form-bg-section d-none';
  maxDate: string;
  minDate: string;
  validTill: string;
  loggedInUserId: string;
  extImageurls = [];
  extLogoUrls = [];
  extImages = [];
  extLogos = [];
  //resetButtonStyle:string="block";
  public logoresponse: string[];
  public imageresponse: string[];
  roleList1: any[] = [];
  roleData: any[] = [];
  addbutton: HTMLElement;
  serverpath = environment.serverpath;
  commonimagepath = environment.imagepath;
  submitted = false;
  lastkeydown1: number = 0;
  subscription: any;
  countryCodeList: Array<any> = [];
  titleText: string = "Post New Advertisement";
  titlecss: string = "nav-icon icon-docs";

  private _allPlan: Observable<Paymentplan[]>;
  private previousPlan: Paymentplan;
  tempPlanId: number;
  tempLogoImageUrl: any;
  tempAdImageUrl: any;

  remainingSmallAdImage: number = 0;
  remainingLogoImage: number = 0;
  adImageCount: number;
  jobsCount: number;
  remainingJobsCount: number = 0;

  adimagefornewpost: string[];
  addivshow = true;
  jobdivshow = false;
  Job: string;
  adTypeFree: boolean = true;
  adTypePaid: boolean = false;
  isPaidAd: boolean = false;
  displayPaidSection = 'col-md-12 d-none';
  RadioFree: boolean = false;
  RadioPaid: boolean = false;
  countryCodesNew: any = (countryCodeData as any).default;
  countrycode = this.countryCodesNew;
  constructor(private toastr: ToastrService, private _adDetailsService: SmallAdDetailsService, private paymentPlanService: Paymentplanservice,
    private _userService: UserService, private _commonService: CommonService, private _http: HttpClient, private _router: Router,
    private _formbulider: FormBuilder, private _route: ActivatedRoute, @Inject(DOCUMENT) document) {
  }

  ngAfterViewInit() {
    //if
    document.getElementById("validTillDate").setAttribute("min", this.minDate);
    document.getElementById("validTillDate").setAttribute("max", this.maxDate);
    document.getElementById('#SaveAd').innerHTML = this.saveButtonText;
    document.getElementById('#dvTitle').innerHTML = this.titleText;
  }

  gettoday() {
    ////debugger;
    var CurrentDate = new Date();
    this.minDate = CurrentDate.toJSON().split('T')[0];
    CurrentDate.setMonth(CurrentDate.getMonth() + 1);
    this.maxDate = CurrentDate.toJSON().split('T')[0];
  }

  // 
  public uploadFinishedLogo = (event) => { this.logoresponse = event; }
  public uploadFinishedImage = (event) => { this.imageresponse = event; }
  // AutoComplete

  ngOnInit() {
    debugger
    this.loggedInUserId = localStorage.getItem('userId');
    this.FillCountryDDL();

    this.FillUserDDL();
    this.gettoday();
 
    
      this.addivshow = true;
  
      this.titleText = "Post New Advertisement";
      this.titlecss = "nav-icon icon-docs";
    
    ////debugger;
    this.FormPostSmallAd = this._formbulider.group({

      smalladId: ['', ''],
      title: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      faxNumber: [''],
      addressStreet1: ['', ''],
      addressStreet2: ['', ''],
      countryId: ['1', ''],
      countryCodeContact: ['1', ''],
      adLogoUrl: ['', ''],
      adImageUrl: ['', ''],
      adextLogoUrl: ['', ''],
      adextImageUrl: ['', ''],
      stateId: [this.StateId, ''],
      cityId: [this.CityId, ''],
      stateName: [this.StateName, ''],
      cityName: [this.CityName, ''],
      zipCode: ['', Validators.pattern("^[0-9]*$")],
      tagLine: ['', ''],
      postedFor: ['', Validators.required],
      postedForName: ['null', ''],
      
      
      contactPersonName: ['', Validators.required],
      contactNumber: ['', [Validators.required, Validators.minLength(10)]],
      website: ['', ''],
     
      description: ['', ''],
      validTillDate: ['', ''],
      alternateContactNumber: [''],
      createdBy: ['', ''],
      isPaidAd: ['', ''],
      logoCount: [''],
      adImageCount: [''],
      jobsCount: [''],
      planId: ['']
    });

    //if(this.route.snapshot.queryParams['adId']!="undefined")
    this.tempPlanId = 0;
    this.remainingLogoImage = 0;
    this.remainingSmallAdImage = 0;
    this.remainingJobsCount = 0;
    this.getSmallAdDetailsById(this._route.snapshot.queryParams['smalladId']);
    
  }


  get f() { return this.FormPostSmallAd.controls; }


  // Insert Post
  postSmallAd(): void {
    debugger;
    this.submitted = true;
    if (this.saveButtonText == "Save") {
      this.action = "";
    }
    // stop the process here if form is invalid

    if (this.FormPostSmallAd.invalid) {
      this.toastr.warning("Mandatory fields missing. Please fill all mandatory fields.");
      return;
    }

    var x = document.getElementsByClassName("ng-value-label");
    this.PostedForName = x[0].innerHTML;
    this.StateName = x[2].innerHTML;
    this.CityName = x[3].innerHTML;
    this.FormPostSmallAd.value.stateName = this.StateName;
    this.FormPostSmallAd.value.cityName = this.CityName;

    this.FormPostSmallAd.value.createdBy = this.loggedInUserId;
    this.FormPostSmallAd.value.isPaidAd = this.isPaidAd;
    if (this.isPaidAd == true)
    {
      if (this.tempPlanId == 0) {
        this.toastr.warning("Please select a subscription plan to submit the listing.");
        return;
      }
      else {
        this.FormPostSmallAd.value.adLogoUrl = this.logoresponse;
        this.FormPostSmallAd.value.imageUrl = this.imageresponse;
      }
    }
    else {
      this.FormPostSmallAd.value.adLogoUrl = null;
      this.FormPostSmallAd.value.imageUrl = null;
    }
   
    var count = 0;
   
    console.log(this.FormPostSmallAd.value);
    if (this.action == "") {
      if (this.FormPostSmallAd.value.imageUrl != undefined) {
        var splitsNewImage = new String(this.FormPostSmallAd.value.imageUrl);
        var afterSplit = splitsNewImage.split(",");
        count = afterSplit.length;
      }


      if (this.FormPostSmallAd.value.adLogoUrl != null) {
        if (this.remainingLogoImage - 1 < 0) {
          this.toastr.warning("Logo subscription utilised.Please choose another plan");
          return;
        }
        else {
          this.remainingLogoImage = this.remainingLogoImage - 1;
        }
      }


      if (this.FormPostSmallAd.value.imageUrl != null) {
        if (this.remainingSmallAdImage - count < 0) {
          this.toastr.warning("Advertisement image subscription fully utilised.Please choose another plan or reduce number of images.");
          return;
        }
        else {
          this.remainingSmallAdImage = this.remainingSmallAdImage - count;
        }
      }

    


      this.FormPostSmallAd.value.logoCount = this.remainingLogoImage;
      this.FormPostSmallAd.value.adImageCount = this.remainingSmallAdImage;
      this.FormPostSmallAd.value.jobsCount = this.remainingJobsCount;
      this.FormPostSmallAd.value.planId = this.tempPlanId;

      this.FormPostSmallAd.value.smalladId = 0;
      this._adDetailsService.postSmallAd(this.FormPostSmallAd.value)
        .subscribe((response) => {
          console.log(response);
          if (response.status === 200) {
            if ((<any>response)._body === "true") {
              
                this.toastr.success('Advertisement posted successfully.');
                this._router.navigate(['/theme/managesmallad']);
             

            }
            else {
              this.toastr.warning('Advertisement name already exist.');
            }

          }

        },
          (error) => {
            console.log(error);
            this.toastr.error('Problem with service. Please try again later!');
          }
        );
    }

    else if (this.action == "edit") {
      debugger;
      if ((this.FormPostSmallAd.value.adLogoUrl != null)) {
        if ((this.FormPostSmallAd.value.adLogoUrl != undefined) || (this.FormPostSmallAd.value.adLogoUrl != "")) {
          if (this.remainingLogoImage - 1 < 0) {
            this.toastr.warning("Logo subscription fully utilised.Please choose another plan.");
            return;
          }
          else {
            this.remainingLogoImage = this.remainingLogoImage - 1;
          }
        }

      }
      else if (this.extLogos.length > 0) {
        this.FormPostSmallAd.value.adLogoUrl = this.extLogos.slice()[0];
      }


      if (this.extImages.length > 0) {
        if ((this.imageresponse != null) || (this.imageresponse != undefined)) {
          this.FormPostSmallAd.value.imageUr = this.imageresponse;
          var splitsEditImage = new String(this.FormPostSmallAd.value.imageUrl);
          var afterEditSplit = splitsEditImage.split(",");
          var count = afterEditSplit.length;
          var finalcount=count + this.extImages.length;
          if(finalcount >5)
          {
            this.toastr.warning('Only 5 image  allowed to upload');
            count=0;
            return;
          }
          if ((this.FormPostSmallAd.value.imageUrl != null) && (this.remainingSmallAdImage - count < 0)) {
            this.toastr.warning("Advertisement image subscription fully utilised.Please choose another plan or reduce number of images.");
            return;
          }
          else {
            this.remainingSmallAdImage = this.remainingSmallAdImage - count;
          }
        }


        if (this.imageresponse === undefined)
          this.FormPostSmallAd.value.imageUrl = this.extImages.join();
        else {
          this.FormPostSmallAd.value.imageUrl = this.imageresponse + "," + this.extImages.join();
        }

      }
      else {
        if (this.imageresponse === undefined)
          this.FormPostSmallAd.value.imageUrl = "";
        else {
          this.FormPostSmallAd.value.imageUrl = this.imageresponse;
          var splitsEditImage = new String(this.FormPostSmallAd.value.imageUrl);
          var afterEditSplit = splitsEditImage.split(",");
          var count = afterEditSplit.length;

          if ((this.FormPostSmallAd.value.imageUrl != null) && (this.remainingSmallAdImage - count < 0)) {
            this.toastr.warning("Advertisement image subscription fully utilised.Please choose another plan or reduce number of images.");
            return;
          }
          else {
            this.remainingSmallAdImage = this.remainingSmallAdImage - count;
          }
        }
      }

      this.FormPostSmallAd.value.planId = this.tempPlanId;
      this.FormPostSmallAd.value.logoCount = this.remainingLogoImage;
      this.FormPostSmallAd.value.adImageCount = this.remainingSmallAdImage;
    
      this.FormPostSmallAd.value.stateName = this.StateName;
      this.FormPostSmallAd.value.cityName = this.CityName;
      this.FormPostSmallAd.postedFor = this.PostedForId;
      //this.FormPostSmallAd.value.adLogoUrl=this.FormPostSmallAd.value.adLogoUrl.join();
      ////debugger;
      this._adDetailsService.updateSmallAd(this.FormPostSmallAd.value)
        .pipe(first())
        .subscribe(
          response => {
            console.log(response);
            if (response.status === 200) {
              if ((<any>response)._body === "true") {
                
                this.toastr.success('Advertisement updated successfully.');
                  this._router.navigate(['/theme/managesmallad']);
                

              }
              else {
                
                this.toastr.warning('Advertisement name already exist.');
              }

            }

          },
          (error) => {
            console.log(error);
            this.toastr.error('Problem with service. Please try again later!');
          }
        );
    }
  }
  // Get User Details on Posted For selection

  subscriptionPlanDetails(event: any) {
    debugger;
    console.log(event.target.value);

    this.paymentPlanService.getPlanDetails(event.target.value).subscribe(data => {
      var planIdDetails = JSON.parse(data._body);
      console.log(planIdDetails);
      this.previousPlan = planIdDetails;
      this.tempPlanId = planIdDetails.planId;
      this.remainingLogoImage = planIdDetails.logoCount;
      this.remainingSmallAdImage = planIdDetails.smallAdsCount;
    
      this.FormPostSmallAd.patchValue({
        logoCount: planIdDetails.logoCount,
        adImageCount: planIdDetails.smallAdsCount,
       
      });
    });
  }

  GetUserDetails(event: any) {

    debugger;

    this._userService.getUserById(event.userId).subscribe(data => {
      var userdata = JSON.parse(data._body);
      // this._allStateNew = this._commonService.StateDataByCountry(data.countryId);
      this.FormPostSmallAd.postedForName = event.firstName + ' ' + event.lastName;
      this._commonService.CityDDL(userdata.stateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });


      this.FillSubscriptionPlan(userdata.userId);

      debugger;
      if (this.action == "") {
        this.StateId = userdata.stateId;
        this.CityId = userdata.cityId;
        this.FormPostSmallAd.patchValue({
          email: userdata.email,
          faxNumber: userdata.faxNumber,
          addressStreet1: userdata.addressStreet1,
          addressStreet2: userdata.addressStreet2,
          countryId: 1,
          stateId: userdata.stateId.toString(),
          cityId: userdata.cityId.toString(),
          zipCode: userdata.zipCode,
          postedFor: userdata.userId.toString(),
          contactPersonName: userdata.firstName + " " + userdata.lastName,
          contactNumber: userdata.contactNumber,
          alternateContactNumber: userdata.alternateContactNumber,
          smalladId: "",
          title: "",
          stateName: "",
          cityName: "",

          tagLine: "",

         // postedForName: "",
          postedForName:this.FormPostSmallAd.postedForName,
         

          countryCodeContact: "1",
          website: "",
         
          description: "",
          validTillDate: "",

          adLogoUrl: "",
          adImageUrl: "",
          adextLogoUrl: "",
          adextImageUrl: "",
          createdBy: "",
          isPaidAd: "",
          logoCount: 0,
          adImageCount: 0,
          jobsCount: 0
        });
      }

      else if (this.action == "edit") {

        this.FormPostSmallAd.patchValue({


          email: userdata.email,
          faxNumber: userdata.faxNumber,
          addressStreet1: userdata.addressStreet1,
          addressStreet2: userdata.addressStreet2,
          countryId: userdata.countryId,
          stateId: userdata.stateId,
          cityId: userdata.cityId,
          zipCode: userdata.zipCode,
          postedFor: userdata.userId.toString(),
          contactPersonName: userdata.firstName + " " + userdata.lastName,
          contactNumber: userdata.contactNumber,
          alternateContactNumber: userdata.alternateContactNumber,
          smalladId: this.FormPostSmallAd.value.smalladId,
          title: this.FormPostSmallAd.value.title,
          stateName: this.FormPostSmallAd.value.stateName,
          cityName: this.FormPostSmallAd.value.cityName,

          tagLine: this.FormPostSmallAd.value.tagLine,

          postedForName: this.FormPostSmallAd.value.postedForName,
         

          countryCodeContact: "1",
          website: this.FormPostSmallAd.value.website,
        
          description: this.FormPostSmallAd.value.description,
          validTillDate: this.FormPostSmallAd.value.validTillDate,

          adLogoUrl: this.FormPostSmallAd.value.adLogoUrl,
          adImageUrl: this.FormPostSmallAd.value.adImageUrl,
          adextLogoUrl: this.FormPostSmallAd.value.adextLogoUrl,
          adextImageUrl: this.FormPostSmallAd.value.adextImageUrl,
          createdBy: this.FormPostSmallAd.value.createdBy,
          isPaidAd: this.isPaidAd,
          logoCount: 0,
          adImageCount: 0,
          jobsCount: 0
        });


      }
    });
  }

  renderExistingImages(imageUrls, smalladId, uploadType, isPaidAd) {
    debugger;
    if (isPaidAd) {
      if (imageUrls !== "") {
        var extImages = imageUrls.split(',');
        if (extImages.length > 0) {
          if (uploadType == "Logo") {
            for (let i = 0; i < extImages.length; i++) {
              this.extLogoUrls.push(this.commonimagepath + 'SmallAds/' + smalladId + '/Logo/' + extImages[i]);
              this.extLogos.push(extImages[i]);
              this.displayExtLogo = 'col-md-6';
            }
          }
          else if (uploadType == "Images") {
            for (let i = 0; i < extImages.length; i++) { ////debugger;
              this.extImageurls.push(this.commonimagepath + 'SmallAds/' + smalladId + '/Images/' + extImages[i]);
              this.extImages.push(extImages[i]);
              this.displayExtImages = 'col-md-6';
            }
          }
          // this.imagepath+'Ads/'+adId+'/Logo/'+picurl;
        }
      }
      else {
        if (uploadType == "Images")
          this.displayExtImages = 'col-md-6 d-none';
        if (uploadType == "Logo")
          this.displayExtLogo = 'col-md-6 d-none';

      }

    }
    else {
      if (uploadType == "Images")
        this.displayExtImages = 'col-md-6 d-none';
      if (uploadType == "Logo")
        this.displayExtLogo = 'col-md-6 d-none';

    }
  }

  onextLogoUrldelete(i) {
    this.extLogoUrls.splice(i, 1);
    this.extLogos.pop();
  }
  
  onextextImageurldelete(i) {
    debugger;
    this.extImageurls.splice(i, 1);
    this.extImages.splice(i, 1);
    console.log(this.extImageurls);
    console.log(this.extImages);
  }

  getSmallAdDetailsById(smalladId) {
    debugger;
    this._adDetailsService.getSmallAdDetailsById(smalladId).subscribe(data => {
      // console.log(data);
      ////debugger
      //var data = JSON.parse(data._body);
      // console.log(data);

      this.action = "edit";
      this.saveButtonText = "Update";
     
        this.titleText = "Edit Advertisement";
        this.titlecss = "nav-icon icon-docs";
    
      this.displayText = 'btn btn-sm btn-primary d-none';
      debugger;
      var x = document.getElementById("postedFor");
      x.style.backgroundColor = "#f5f5f5";
      this.FormPostSmallAd.controls['postedFor'].disable(true);
      this.FormPostSmallAd.postedFor = data.postedFor.toString();
      this.PostedForId = data.postedFor.toString();
      if (data.isPaidAd == true) {
        // Ad Images Retrieval
        this.displayExtUploads = 'form-bg-section';
        if (data.imageUrl !== null) {
          this.selected("Paid");
          this.adTypePaid = true;
          this.adTypeFree = false;
          this.RadioPaid = false;
          this.RadioFree = true;
          if (data.imageUrl != null)
          this.renderExistingImages(data.imageUrl, data.smallAdId, "Images",true);
        }
        else {

          this.adTypePaid = false;
          this.adTypeFree = true;
          this.RadioPaid = true;
          this.RadioFree = false;
          this.displayExtImages = 'col-md-6 d-none';
        }
          

        // Ad Logo Retrieval
        if (data.adLogoUrl !== null) {
          this.selected("Paid");
          this.adTypePaid = true;
          this.adTypeFree = false;
          this.RadioPaid = false;
          this.RadioFree = true;
          if (data.adLogoUrl != null)
          this.renderExistingImages(data.adLogoUrl, data.smallAdId, "Logo",true);
        }
        else {
          this.displayExtLogo = 'col-md-6 d-none';
          this.adTypePaid = false;
          this.adTypeFree = true;
          this.RadioPaid = true;
          this.RadioFree = false;
        }
      }
      else {
        this.RadioPaid = true;
      }
      if (data.imageUrl == null && data.adLogoUrl == null) {
        this.RadioPaid = true;
        this.RadioFree = false;
        this.displayExtUploads = 'form-bg-section d-none';
      }
      else if (data.imageUrl == "" && data.adLogoUrl == "") {
        this.RadioPaid = true;
        this.RadioFree = false;
        this.displayExtUploads = 'form-bg-section d-none';
      }


      debugger;
      this._commonService.StateDDL(data.countryId).subscribe(data => {
        debugger
        console.log(data);
        this.states = data;

      });

      this._commonService.BUCityDDL(data.stateId).subscribe(data => {
        debugger;
        console.log(data);
        this.cities = data;
      });

     



      if (data.imageUrl === "" && data.adLogoUrl === "")
        this.displayExtUploads = 'form-bg-section d-none';
      this._allState = this._commonService.StateDDL(data.countryId);
      this._allCity = this._commonService.CityDDL(data.stateId);
      this.countrycode = this.countryCodesNew;
 
      this.FillSubscriptionPlan(data.postedFor);
      this.gettoday();
    
      debugger;
      if (data.validTillDate !== null)
        this.validTill = data.validTillDate.split('T')[0];
      else
        this.validTill = this.minDate;
      if (data.countryCodeContact === null)
        data.countryCodeContact = "1";
   
      debugger;
      this.FormPostSmallAd.patchValue({
        smalladId: data.smallAdId,
        title: data.title,
        email: data.email,
        postedFor: data.postedFor.toString(),
        countryId: data.countryId,
        stateId: data.stateId.toString(),
        cityId: data.cityId.toString(),
       

        // validTillDate:this.validTill,
        faxNumber: data.faxNumber,
        addressStreet1: data.addressStreet1,
        addressStreet2: data.addressStreet2,

        stateName: data.stateName,
        cityName: data.cityName,
        zipCode: data.zipCode,
        tagLine: data.tagLine,

        postedForName: data.postedForName,

       

       
        contactPersonName: data.contactPersonName,
        contactNumber: data.contactNumber,
        countryCodeContact: data.countryCodeContact.toString(),
        website: data.website,
      
        description: data.description,
        alternateContactNumber: data.alternateContactNumber,
        validTillDate: this.validTill,
        adLogoUrl: data.adLogoUrl,
        adImageUrl: data.imageUrl,
        adextLogoUrl: "",
        adextImageUrl: "",
        logoCount: 0,
        adImageCount: 0,
        jobsCount: 0,
        createdBy: data.createdBy,
      
        isPaidAd: data.isPaidAd
      });
      ////debugger;
      // this.gettoday();
    });
  }

  deleteAd(SmallAdId: number) {
    if (confirm('Are you sure to delete this record?')) {
      this._adDetailsService.deleteSmallAd(SmallAdId).subscribe(response => { console.log(response); },
        (error) => {
          console.log(error);

          this.toastr.error('Problem with service. Please try again later!');
        });
    }
  }

  selected(listingType: string) {
    debugger;
    if (listingType == "Free") {
      this.isPaidAd = false;
      this.displayPaidSection = 'col-md-12 d-none';
    }
    if (listingType == "Paid") {
      this.isPaidAd = true;

      this.displayPaidSection = 'col-md-12';
    }
  }

  //  Dropdowns

  //FillRoleDDL() {this._allroles = this._commonService.RoleDDL(); }  
  FillCountryDDL() {
    debugger;
    this._allCountry = this._commonService.CountryDDL();
    this.CountryId = "1";
    this._commonService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      this.states = data;
    });
    this._allState = this._commonService.StateDDL(this.CountryId);
    this._allCity = this._commonService.CityDDL(this.StateId);
  }

  FillUserDDL() {
    debugger;
    this._allUser = this._commonService.UserDDL();
    this._commonService.UserInboxMessageDDL().subscribe(data => {
      debugger;
      this.users = data;


    });

  }

  FillStateDDL(event: any) {
    debugger;
   
    if ((event.target.value == "0") || ((event.target.value == ""))) {
      this.FormPostSmallAd.value.countryId = 0;
      //this._allState = this._commonService.StateDDL("0");
      //var x = document.getElementsByClassName("ng-value-label");
      
      //var i;
      //for (i = 3; i < x.length; i++) {
      //  x[i].innerHTML = "";
      //}
      this.states = [];
      this.cities = [];
      // this.FillCityDDL(0);
      console.log(this.states);
      this.FormPostSmallAd.value.stateId = 0;
      //this._allCity = this._commonService.CityDDL("0");
      this.FormPostSmallAd.value.cityId = 0;
    }

    else {
      this.CountryId = event.countryId;

      this._allState = this._commonService.StateDDL("1");
      debugger;
      this._commonService.StateDDL("1").subscribe(data => {
        console.log(data);
        //debugger;
        this.states = data;
      });

      //this._commonService.CityDDL(this.StateId).subscribe(data => {
      //  console.log(data);
      //  debugger;
      //  this.cities = data;
      //});
      this._allState = this._commonService.StateDDL(this.CountryId);
      this.CountryName = event.target.options[this.CountryId].innerHTML;
      this.FormPostSmallAd.countryName = this.CountryName;
    }
    var x = document.getElementsByClassName("ng-value-label");
    if (x.length > 0) {

      x[2].innerHTML = "--Select State--";
      x[3].innerHTML = "--Select City--";
    }

  }

  FillCityDDL(event: any) {
    debugger;
    var x = document.getElementsByClassName("ng-value-label");
    x[3].innerHTML = "--Select City--";
    if (event != 0) {
      this.StateId = event.stateId;
      this.StateName = event.stateName;
      //this.StateName=event.target.options[this.StateId].innerHTML;
     
      this._allCity = this._commonService.CityDDL(this.StateId);
      this.FormPostSmallAd.stateName = this.StateName;
      this._commonService.CityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
    }
    else {
      var x = document.getElementsByClassName("ng-value-label");
      this.cities = [];
    }
  }
  FillCityName(event: any) {
    this.CityId = event.cityId;
    this.CityName = event.cityName;
    // this.CityName=event.target.options[this.CityId].innerHTML;
    this.FormPostSmallAd.cityName = this.CityName;

  }
  NavigatetoManageSmallAd() {
   
      this._router.navigate(['/theme/managesmallad']);

  }
  FillSubscriptionPlan(userId: number) {
    this._allPlan = this.paymentPlanService.UserPlanDDL(userId);
  }
 

  refresh(): void {
    debugger;
    window.location.reload();
    this.UploadCmpt.ngOnInit();
    this.MultiUploadCmpt.ngOnInit();

  }

  reset() {
    this.ngOnInit();
    this.UploadCmpt.onimagedelete(0);
    this.MultiUploadCmpt.onimagedelete(0);
  }
}
