import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders,HttpClientModule, HttpErrorResponse } from "@angular/common/http";
import { RequestOptions,Http,Response, Headers } from '@angular/http';
import {FormBuilder,Validators} from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import {User} from './user';
import { category } from './category';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})

export class UserService {
  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });
  constructor(private _httpService: Http,private http:HttpClient){ }
  baseUrl = environment.baseUrl;

  UsersListCacheData;
  observable;

  add(user: User) {
    this.observable = null;
    this.UsersListCacheData = null;
    debugger
    let body = JSON.parse(JSON.stringify(user));
    console.log(user);
    //let headers = new Headers({ 'Content-Type': 'application/json' });
    //let options = new RequestOptions({ headers: headers });
    {

      return this._httpService.post(this.baseUrl + 'userdetails', body, this.options);
      console.log(this.options);
    }
  }

  addCategory(user: User) {
    let body = JSON.parse(JSON.stringify(user));
    console.log(user);
    //let headers = new Headers({ 'Content-Type': 'application/json' });
    //let options = new RequestOptions({ headers: headers });
    {

      return this._httpService.post(this.baseUrl + 'ManageCategory', body, this.options);
      console.log(body);
    }
  }

  updateCategory(formData: User) {
    debugger
    return this._httpService.put(this.baseUrl + 'ManageCategory/' + formData.categoryId, formData, this.options);

  }

  getUsersList(): Observable<any>
  {
    debugger;

    if (this.UsersListCacheData) {
      console.log(' first if condition for UsersListData');
      return Observable.of(this.UsersListCacheData);
    }
    //else if (this.observable) {
    //  console.log('2nd if condition for getAdList');
    //  return this.observable;
    //}
    else {
      this.observable = this.http.get(this.baseUrl + 'userdetails',
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          this.UsersListCacheData = null;
          if (response.status === 400) {
            console.log('error while fetching UsersListData from server');
            return ' problem with service';
          }
          else if (response.status === 200) {
            console.log('fetch UsersListData from server successfully');
            this.UsersListCacheData = response.body;
            return this.UsersListCacheData;
          }

        });
      return this.observable;
    }
    //return this._httpService.get(this.baseUrl + 'userdetails');
  }

  getCategoryList(): Observable<any> {
    return this._httpService.get(this.baseUrl + 'ManageCategory');

  }

  FillCategoryList(): Observable<any> {
    return this._httpService.get(this.baseUrl + 'ManageCategory/GetCategoryList');
  }

  deleteCategory(CategoryId: number) {
    return this._httpService.delete(this.baseUrl + 'ManageCategory/' + CategoryId);
  }

  getUserById(userId: number): Observable<any> {
    return this._httpService.get(this.baseUrl + 'userdetails/' + userId);
  }

  getCategoryById(categoryId: User): Observable<any> {

    return this._httpService.get(this.baseUrl + 'manageCategory/' + categoryId);
  }

  UserDDLByRole(roleName:string): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + 'Common/RoleData?roleName=' + roleName);
  }

  UserDDLByRoleForBanner(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + 'Common/UserDDLbyRoleForBanner');
  }

  updateUser(user: User): Observable<any> {
    this.observable = null;
    this.UsersListCacheData = null;
    return this._httpService.put(this.baseUrl + 'userdetails/' + user.userId, user);
  }

  deleteUser(userId: number) {
    this.observable = null;
    this.UsersListCacheData = null;
    return this._httpService.delete(this.baseUrl + 'userdetails/' + userId, this.options);
  }

  ResetPassword(user: User): Observable<any> {
    return this._httpService.put(this.baseUrl + 'login/' + user.userId, user);
  }

  getLoginUserById(userId: User): Observable<any> {

    return this._httpService.get(this.baseUrl + 'userdetails/' + userId);
  }

  activateUserAccount(info: string): Observable<any> {

    return this._httpService.get(this.baseUrl + 'login/ActivateUserAccount?info=' + info, this.options);
  }

// getLoginUserById(userId: User): Observable<any> {

// //Get IP Adress using http://freegeoip.net/json/?callback
// getIpAddress() {
//   return this.http
//         .get('http://freegeoip.net/json/?callback')
//         .map(response => response || {})
//         //.catch(this.handleError);
// }
// private handleError(error: HttpErrorResponse):
// Observable<any> {
//   //Log error in the browser console
//   console.error('observable error: ', error);

//   return Observable.throw(error);
// }

}





