import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { HttpClient, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SubcategoryService } from '../subcategory.service';
import { Subcategory } from '../subcategory';
import { category } from '../category';
import { Observable } from 'rxjs/Observable';
import { first } from 'rxjs/operators';
import { OrderPipe } from 'ngx-order-pipe';
import { ToastrService } from 'ngx-toastr';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
@Component({
  selector: 'app-editsubcategory',
  templateUrl: './editsubcategory.component.html',
  styleUrls: ['./editsubcategory.component.scss']
})
export class EditsubcategoryComponent implements OnInit {
  users(users: any, arg1: string): any[] {
    throw new Error("Method not implemented.");
  }
  subcategories: Subcategory[];
  categoryId: string = "0";
  public _allCategory: Observable<category[]>;
  categories: any[];
  categoryName: string;
  //subCategoryId:string="0";
  submitted = false;
  editSubCategoryform: any;
  public pageSize: number = 10;
  public p: number;
  subcategory: Subcategory;
  filter: string = "";
  isLoadingResults = false;
  order: string;
  reverse: boolean;
  FormSubcategoryAdd:any;


  getSubCategoryList(): void {
    debugger
    //this.SubcategoryService.getSubCategoryList().subscribe(subcategories => {
      this.SubcategoryService.FillSubcategoryList().subscribe(subcategories => {
      this.subcategories = subcategories.json();
      console.log(this.subcategories);

    });
  }
  ngOnInit() {

    this.editSubCategoryform = this.formBuilder.group({
      categoryName: ['', Validators.required],
      description: [''],
      subCategoryId: ['', Validators.required],
      subCategoryName: ['', Validators.required],
      arabicName: ['', Validators.required],
      categoryId: ['', Validators.required]
    });
    this.FillCategoryDDL();
    this.getSubCategoryList();
    this.getSubCategorybyID(this.route.snapshot.queryParams['subCategoryId']);
  }
  getSubCategorybyID(subCategoryId) {
    debugger
    this.SubcategoryService.getSubCategoryById(subCategoryId).subscribe(data => {
      
      var data = JSON.parse(data._body);
      console.log(data);
      //this.FillCategoryDDL();
      debugger;
      this._allCategory = this.SubcategoryService.CategoryDDL();
      this.SubcategoryService.CategoryDDL().subscribe(data => {
        debugger;
        this.categories = data;


      });
      debugger;
      this.editSubCategoryform.setValue({
        
        subCategoryId: data.subCategoryId,
        subCategoryName: data.subCategoryName,
        arabicName: data.arabicName,
        description: data.description,
        categoryId: data.categoryId.toString(),
        categoryName: data.categoryName
       
      });
    });
  }


  FillCategoryDDL() {
    debugger;
    this._allCategory = this.SubcategoryService.CategoryDDL();

    this.SubcategoryService.CategoryDDL().subscribe(data => {
      debugger;
      this.categories = data;


    });

  }
 


  sortedCollection: any[];

  constructor(private toastr: ToastrService,private orderPipe: OrderPipe, private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private activatedRoute: ActivatedRoute,
    private SubcategoryService: SubcategoryService) {
    this.sortedCollection = orderPipe.transform(this.users, 'firstName');
    console.log(this.sortedCollection);
  }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }
  onCancelEdit()
  {
    this.router.navigate(['/theme/managesubcategory']);
  }

  onEditSubmit() {
    debugger
    this.submitted = true;
    //stop here if form is invalid
    if (this.editSubCategoryform.invalid) 
    {
     return;
    }
    debugger
    console.log(this.editSubCategoryform.value);
    this.SubcategoryService.updateSubCategory(this.editSubCategoryform.value)
      .pipe(first())

      .subscribe(
        data => {
          debugger
          if (data.status === 200)
          {
            console.log(data);
            if ((<any>data)._body === "true")
              this.toastr.success('Sub Category updated successfully.');
            else
              this.toastr.info("Sub category is linked with sub category, can't be deleted");
            this.router.navigate(['/theme/managesubcategory']);
          }
          else {
            //alert(data.message);
          }
        },
        error => {
          this.toastr.error(error);
        });
  }

}

