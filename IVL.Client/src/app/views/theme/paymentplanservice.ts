import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User } from './user';
import { HttpClient, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { RequestOptions, Http, Response, Headers } from '@angular/http';
import { Paymentplan } from './paymentplan';
import { from } from 'rxjs';
import { environment } from '../../../environments/environment';
import { UserInfo } from 'os';
import { Transcationdetails } from './transcationdetails';

@Injectable({
  providedIn: 'root'
})

export class Paymentplanservice
{
  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient, private _httpService: Http) { }

  addUserPlan(paymentplan: Paymentplan)
  {
    let body = JSON.parse(JSON.stringify(paymentplan));
    console.log(paymentplan);
    return this._httpService.post(this.baseUrl + 'PaymentPlan', body);
  }

  UserPlanDDL(userId:number): Observable<Paymentplan[]>
  {
    debugger;
    return this.http.get<Paymentplan[]>(this.baseUrl + 'PaymentPlan/PlanListForUser?userId=' + userId);
    //(this.baseUrl + 'Website/GetAdsAdListBycategoryId?categoryId='+categoryId);
  }

  getPlanDetails(planId: number) : Observable<any>
  {
    return this._httpService.get(this.baseUrl + 'PaymentPlan/PlanIdDetails?planId=' + planId);
  }

  OfflinePayment(offlinePayment: any)
  {
    const headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });

    //return this.http.post(this.baseUrl + 'PaymentPlan/OfflinePaymentConfirmation?offlinePayment=' + JSON.stringify(offlinePayment), { headers: headers });

    this.http.post(this.baseUrl + 'PaymentPlan/OfflinePaymentConfirmation?offlinePayment=' +
      JSON.stringify(offlinePayment),
      { headers: headers }).subscribe(data => {
        console.log(data);
      });

    return true;
  }



  //updatePlanDetails(plan: Paymentplan)
  //{
  //  debugger;
  //  let body = JSON.parse(JSON.stringify(plan));
  //  this._httpService.put(this.baseUrl + 'PaymentPlan/' + plan.planId, body);
  //}

  GetAllPlan() :Observable<any>
  {
    return this._httpService.get(this.baseUrl + 'PaymentPlan/PaymentPlanList');
  }
}
