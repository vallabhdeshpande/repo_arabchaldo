import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import { RequestOptions,Http,Response, Headers } from '@angular/http';
import {State} from './state.model';
import { Country } from './country.model';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class StateService 
{
  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });
  baseUrl = environment.baseUrl;
  constructor(private _httpService: Http,private http:HttpClient){ }

  addState(State: State)
  {
   debugger
    let body = JSON.parse(JSON.stringify(State));
    console.log(State);State  
    //let headers = new Headers({ 'Content-Type': 'application/json' });
    //let options = new RequestOptions({ headers: headers });
      
        
    return this._httpService.post(this.baseUrl + 'state/', body, this.options);
  console.log(body);
   
  }
    updateState(formData: State) {
      debugger
      return this._httpService.put(this.baseUrl + 'state/' + formData.stateId, formData, this.options);
    } 

    getStateList(): Observable<any> 
    {
      return this._httpService.get(this.baseUrl + 'state');
    }

    fillStateList(): Observable<any> {
      return this._httpService.get(this.baseUrl + 'state/GetStateList');
    }
    getStateById(stateId: State): Observable<any> 
    {
      return this._httpService.get(this.baseUrl + 'state/'+stateId);
    }

    deleteState(stateId : number)
    {
      debugger;
      return this._httpService.delete(this.baseUrl + 'state/' + stateId, this.options);
    }

     StateDDL(): Observable<State[]>
    {   
      return this.http.get<State[]>(this.baseUrl + 'Common/State');      
    }    
    
    CountryDDL(): Observable<Country[]>
    {   
      return this.http.get<Country[]>(this.baseUrl + 'common/CountryData');
    }
}
