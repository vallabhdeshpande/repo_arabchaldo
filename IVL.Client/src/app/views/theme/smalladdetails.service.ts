import { Injectable, ɵɵresolveBody } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { RequestOptions, Http, Response, Headers } from '@angular/http';
import { Country } from './country.model';
import { State } from './state.model';
import { City } from './city.model';
import { category } from './category';
import { Subcategory } from './subcategory';
import { User } from './user';
import { SmallAdDetails } from './smalladdetails.model';
import { environment } from '../../../environments/environment';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';

@Injectable({
  providedIn: 'root'
})
export class SmallAdDetailsService {
  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });
  baseUrl = environment.baseUrl;
  loggedInUserId = localStorage.getItem('userId');
  loggedInRole = localStorage.getItem('roleName');

  observable;
  getSmallAdListData;
  cachedloggedInUserId;
 

  constructor(private _httpService: Http, private http: HttpClient) { }

  postSmallAd(postad: SmallAdDetails) {
    this.getSmallAdListData = null;
    this.observable = null;
    // alert("hi");
    // alert(postad);
    console.log(postad);
    let body = JSON.parse(JSON.stringify(postad));
    console.log(body);
    //let headers = new Headers({ 'Content-Type': 'application/json' });
    // let options = new RequestOptions({ headers: headers });
    {
      return this._httpService.post(this.baseUrl + 'smalladdetails', body, this.options);
      console.log(body);
    }
  }

  updatewithWorkflow(workflowdetails: string) {
    this.getSmallAdListData = null;
   
    this.observable = null;
    //debugger
    return this._httpService.delete(this.baseUrl + 'smalladdetails/' + workflowdetails, this.options);
  }

  updateSmallAd(formData: SmallAdDetails): Observable<any> {
    //debugger
    this.getSmallAdListData = null;
    this.observable = null;
    return this._httpService.put(this.baseUrl + 'smalladdetails/' + formData.smalladId, formData, this.options);
  }

  deleteSmallAd(SmallAdId: number) {
    this.getSmallAdListData = null;
    this.observable = null;
    return this._httpService.delete(this.baseUrl + 'smalladdetails/' + SmallAdId, this.options);
  }

  getSmallAdList(): Observable<any> {
    debugger;
    this.loggedInUserId = localStorage.getItem('userId');
    if (this.cachedloggedInUserId != this.loggedInUserId) {
      this.getSmallAdListData = null;
      this.observable = null;
    }
    this.loggedInUserId = localStorage.getItem('userId');
    this.loggedInRole = localStorage.getItem('roleName');
    if (this.getSmallAdListData) {
      console.log(' first if condition SA SmallAd list');
      return Observable.of(this.getSmallAdListData);
    }
    //else if (this.observable)
    //{
    //  console.log('2nd if condition SA manage events');
    //  return this.observable;
    //}
    else {
      this.cachedloggedInUserId = this.loggedInUserId;
      this.observable = this.http.get(this.baseUrl + 'smalladdetails/GetAllSmallAdDetails?userInfo=' + this.loggedInUserId + "," + this.loggedInRole,
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          console.log('fetch data from server for SA SmallAd list ');
          if (response.status === 400) {
            console.log('request failed SA SmallAd list');
            return 'request failed';
          }
          else if (response.status === 200) {
            console.log('request successful SA SmallAd list');
            this.getSmallAdListData = response.body;
            console.log(this.getSmallAdListData);
            return this.getSmallAdListData;
          }
        });
      return this.observable;
    }
    //return this.http.get(this.baseUrl + 'smalladdetails/GetAllSmallAdDetails?userInfo='+this.loggedInUserId+","+this.loggedInRole);
  }



  getSmallAdDetailsById(SmallAdId: number): Observable<any> {
    return this.http.get(this.baseUrl + 'smalladdetails/GetSmallAdDetailsById?adId=' + SmallAdId);
  }

  UserDDL(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + 'userdetails');
  }

 
  CountryDDL(): Observable<Country[]> {
    return this.http.get<Country[]>(this.baseUrl + 'common/CountryData');
  }

  StateDDL(CountryId: string): Observable<State[]> {
    return this.http.get<State[]>(this.baseUrl + 'common/StateData?CountryId=' + CountryId);
  }

  CityDDL(StateId: string): Observable<City[]> {
    return this.http.get<City[]>(this.baseUrl + 'common/CityData?StateId=' + StateId);
  }
}
