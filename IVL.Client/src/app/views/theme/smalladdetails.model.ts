import { Observable } from 'rxjs';


export class SmallAdDetails {

  public smalladId: number;
  public UpdatedBy: string;
  public Title: string;
  public TagLine: string;
  public Description: string;
 
  public ContactPersonName: string;
  public CountryCode: string;
  public ValidFromDate: string;
  public ValidTillDate: string;
  public ContactNumber: string;
  public AlternateContactNumber: string;
  public FaxNumber: string;
  public Email: string;
  public Website: string;
  public AddressStreet1: string;
  public AddressStreet2: string;
  public CountryId: string;
  public StateId: string;
  public CityId: string;
  public CountryName: string;
  public StateName: string;
  public CityName: string;
  public ZipCode: string;
  public IsActive: boolean;
  public IsVisible: boolean;
  public Activity: boolean;
  public PostedFor: number;
  public PostedForName: string
  public WorkflowstatusId: string;
  public Remarks: string;
  public AdImageUrl: string;
  public AdLogoUrl: string;
  public CreatedBy: string;
  public SmallAdStatusForDetailsView: string;
  public smalladImageDetails: SmallAdImageDetails[];
  public PlanId: number;
  public LogoCount: number;
  public SmallAdImageCount: number;
  public OtherCity: string
  constructor() {
  }
}


export class SmallAdList {


  public smalladId: string;
  public title: string;
  public description: string;
  constructor() {
  }
}

export class WorkflowstatusDTO {

  public workflowStatusId: number;
  public Status: string;
  public IsActive: boolean;
  public CreatedDate: string;
  public CreatedBy: string;
  public updatedDate: string;
  public updatedBy: string;
  constructor() {
  }

}
export class SmallAdImageDetails {

  //public  SmallAdImageDetailId: number;
  public AdImageUrl: string;
  //public SmallAdId: number;
  // public IsActive: boolean;
  // public CreatedDate: string;
  // public CreatedBy:string;
  // public updatedDate:string;
  // public updatedBy:string;
  constructor() {
  }
}
export class SmallAdStatusDetailsDTO {

  public SmallAdStatusDetailsId: number;
  public SmallAdId: number;
  public workflowStatusId: number;
  public Remark: string;
  public Status: string;
  public IsActive: boolean;
  public CreatedDate: string;
  public CreatedBy: string;
  public updatedDate: string;
  public updatedBy: string;
  constructor() {
  }
}
