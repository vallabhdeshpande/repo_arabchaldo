import { Component, OnInit } from '@angular/core';
import { AdDetailsService } from '../addetails.service';
import { Router } from "@angular/router";
import { AdDetails, WorkflowstatusDTO, AdStatusDetailsDTO } from '../addetails.model';
import { IfStmt, debugOutputAstAsTypeScript } from '@angular/compiler';
import { OrderPipe } from 'ngx-order-pipe';
import { environment } from '../../../../environments/environment';
import { ToastrService } from 'ngx-toastr'; 
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-managejobs',
  templateUrl: './managejobs.component.html',
  styleUrls: ['./managejobs.component.scss'],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }]
})
export class ManagejobsComponent implements OnInit {

  adposts: AdDetails[];
  charLength: string = "0";
  // workflowdetails: WorkflowstatusDTO[];
   adStatusdetails: AdStatusDetailsDTO[];
   workflowdetails:string;
   statusMessage: string;
   setStatus: string = "";
   deleteremark:string;
   activity:boolean;
   visibilitybutton:string;
   visibilityspan:string;
   setStatusActive:string="";
   setStatusInactive:string="";
   adpost = new AdDetails();order: string;
   reverse: any ;
 modalRemark:string;
  public pageSize: number = 10;
  public p: number;
   remark: string;
   workflowstatusId:number;
   action:string="";
   actionmessage:string="";
   adId=0;
 imagepath=environment.imagepath;
 loggedInRole:string;
loggedInUserId:string;
validTill:string;
  validFrom: string;
  maxDate: string;
  minDate: string;
  //minValidTillDate: string;
  //maxValidFromDate: string;
  modalexit:string="";
  filter: string = "";
LoggedInUserId:string;
  showDates: string = "col-md-6";
  approveButtonText: string;
  approveButtonStyle: string;
  form: FormGroup;

  //managemodal:string="modal fade";
  remarkstring: string[];
  poststring: string;
  remarkstring1: string;
  remarkstring2: string;
  remarkstring3: string;
  isLoading: boolean = false;
  adStatus: string;
  minDateModel: NgbDateStruct;
  minValidTillDate: NgbDateStruct;
  maxValidFromDate: NgbDateStruct;

  date: { year: number, month: number };
  hoveredDate: NgbDate;
  modelValidFrom: Date = null;
  modelValidTill: Date = null;

  selectToday() {
    this.minDateModel = this.calendar.getToday();
    this.minValidTillDate = this.calendar.getToday();
  }
  onvalidFromDateSelect(event: any) {
    this.minValidTillDate = event;
    this.validFrom = this.modelValidFrom.toJSON().split('T')[0];

  }
  onvalidTillDateSelect(event: any) {
    this.maxValidFromDate = event;
    this.validTill = this.modelValidTill.toJSON().split('T')[0];

  }
   // constructor(private AdDetailsService: AdDetailsService, private router: Router) { }
   
   createForm() {
    this.form = this.fb.group({
      validFrom: ['', Validators.required],
      validTill: ['', Validators.required]
    }, { validator: this.dateLessThan('validFrom', 'validTill') });
  }


  dateLessThan(from: string, to: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let f = group.controls[from];
      let t = group.controls[to];
      if (f.value > t.value) {
        return {
          dates: "Date from should be less than Date to"
        };
      }
      return {};
    }
  }
  //onvalidFromDateSelect(event: any) {
  //  //debugger;
  //  this.minValidTillDate = event.target.value;

  //}
  //onvalidTillDateSelect(event: any) {
  //  this.maxValidFromDate = event.target.value;

  //}

  ngAfterViewInit(){      
    document.getElementById("#modalvalidFrom").setAttribute("min", this.minDate);
    document.getElementById("#modalvalidTill").setAttribute("min", this.minDate);
   
 
     }
      ngOnInit() {
       //debugger
        this.charLength = "0/200";
       this.loggedInUserId=localStorage.getItem('userId');
       this.getAdList();
        // this.gettoday();
        this.selectToday();
     }  
 
   getAdList(): void {
     
     this.AdDetailsService.getJobList().subscribe(adposts => {
       this.adposts = adposts;
       
       console.log(this.adposts);
       // Status
 //debugger;
       var adlenth = 0;
       if (this.adposts != null) {
         if (adposts.length > 0) {
           adlenth = adposts.length;
           for (let i = 0; i < adlenth; i++) {

             if (adposts[i].isVisible === "") {
               adposts[i].isVisible = "";
               adposts[i].Activity = false;
               this.visibilitybutton = "show";
               this.visibilityspan = "hide";
             }
             else if (adposts[i].isVisible === true) {
               adposts[i].isVisible = "Active";
               adposts[i].Activity = true;
               this.visibilitybutton = "hide";
               this.visibilityspan = "show";

               this.setStatusActive = "badge badge-success";
               this.setStatusInactive = "badge badge-secondary d-none";
             }
             else if (adposts[i].isVisible === false) {
               adposts[i].isVisible = "InActive";
               adposts[i].Activity = true;
               this.visibilitybutton = "hide";
               this.visibilityspan = "show";

               this.setStatusActive = "badge badge-success d-none";
               this.setStatusInactive = "badge badge-secondary";
             }
             else if (adposts[i].isVisible === null) {
               adposts[i].isVisible = "";
               adposts[i].Activity = false;
               this.visibilitybutton = "show";
               this.visibilityspan = "hide";
             }
           }
           this.adposts = adposts;
         }
         else
           this.adposts = [];
       }
       else
         this.adposts = [];
 
    
    
     });
   }
 // openModal()
 // {
 //   //debugger;
 //   //#modalRemarkText
 //   let modalRemarkText = (<HTMLInputElement>document.getElementById('modalRemarkText')).value;
 //   if(modalRemarkText!="")
 //   modalRemarkText="";
 //   return "modal";
   
 // }
 
   getPicUrl(picurl:string,adId)
     {
 ////debugger;
       if(picurl==null)
       return "assets/img/default/no_image_placeholder.jpg";
       else if (picurl=="")
       return "assets/img/default/no_image_placeholder.jpg";
       else     
       return this.imagepath+'Ads/'+adId+'/Logo/'+picurl;
 
     }
 
     getStatus(Status: string) {  
 
       if(Status=="Active")
         return this.setStatus="badge badge-success";
 
           if(Status=="InActive")
           return this.setStatus="badge badge-danger";
 
           if(Status=="Pending")
             return  this.setStatus="badge badge-warning";    
 
   } 
  
  hideActivity(Status: string) {

    if (Status == "Active")
      return "d-none";

    if (Status == "InActive")
      return "d-none";

    if (Status == "Pending")
      return "";

  } 
   
   updatePost(adpost:AdDetails): void {
   
     this.router.navigate(['/theme/postad'],{queryParams:{adId:adpost.adId,Job:"Jobs"}});
     };
     gotoAdDetails(adpost:AdDetails): void {
       //debugger
       this.router.navigate(['/theme/addetailsview'],{queryParams:{adId:adpost.adId}});
       };
     NavigateToAddPost( ): void {
       //debugger
        this.router.navigate(['/theme/postad']);
        };
   
   sortedCollection: any[];
 
  constructor(private toastr: ToastrService, private orderPipe: OrderPipe, private AdDetailsService: AdDetailsService, private router: Router, private fb: FormBuilder, private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private config: NgbDatepickerConfig) {
     this.sortedCollection = orderPipe.transform(this.adposts, 'firstName');
     console.log(this.sortedCollection);
   }
 
   setOrder(value: string) {
     if (this.order === value) {
       this.reverse = !this.reverse;
     }
 
     this.order = value;
   }
   ActionOnApprove(adId,event)
   {
    
 //debugger;
 //$("#modalRemarkText").value="";
 // (<HTMLInputElement>document.getElementById('remark')).value="";
 this.remark = null;
     this.validFrom = null;
     this.validTill = null;
     this.modelValidFrom = null;
     this.modelValidTill = null;
     this.maxValidFromDate = null;
     this.minValidTillDate = this.calendar.getToday();
this.showDates="col-md-6";
this.adId=adId;
this.action = "Approve";
this.approveButtonText = "";
this.approveButtonText = "Approve";
this.approveButtonStyle = "";
this.approveButtonStyle = "btn btn-sm btn-success";
this.actionmessage="Are you sure to Approve this record?";
   }
   ActionOnReject(adId,event)
   {
    //debugger;
    this.remark = null;
     this.validFrom = null;
     this.validTill = null;
     this.modelValidFrom = null;
     this.modelValidTill = null;
     this.maxValidFromDate = null;
     this.minValidTillDate = this.calendar.getToday();
    this.showDates="col-md-6 d-none"; 
    this.adId=adId;
    this.action = "Reject";
    this.approveButtonText = "";
    this.approveButtonText = "Reject";
    this.approveButtonStyle = "";
    this.approveButtonStyle = "btn btn-sm btn-warning";
    this.actionmessage="Are you sure to Reject this record?";
   }
   ActionOnPost(remark) {
    //debugger;
    remark= remark+",false";
    this.poststring = remark;

    this.remarkstring = this.poststring.split(',');
    this.remarkstring1 = this.remarkstring[0];
    this.remarkstring2 = this.remarkstring[1];
    this.remarkstring3 = this.remarkstring[2];

    if ((this.approveButtonText == "Approve") && ((this.remarkstring1 == "null") || (this.remarkstring2 == "null") || (this.remarkstring3 == "null"))) {
      this.toastr.warning("All fields are required to approve");
      return;
    }

    if ((this.approveButtonText == "Reject") && (this.remarkstring3 == "null")) {
      this.toastr.warning("Remark is required to reject");
      return;
    }
    
    this.workflowdetails=this.adId+","+this.loggedInUserId+","+this.action+","+remark;
        
    
       // if (confirm(this.actionmessage)) {
          this.AdDetailsService.updatewithWorkflow( this.workflowdetails).subscribe(response => {
            console.log(response);
            console.log('Updated');
            this.modalexit="modal";
            document.getElementById("closeBtn").click();
           // this.managemodal="modal fade";
            // jQuery('remark').modal('hide');
            this.getAdList();
            if(this.action==="Approve")
            this.toastr.success('Job approved succesfully');
            if(this.action==="Reject")
            this.toastr.warning('Job rejected');
          }, 
    
            (error) => {
              console.log(error);
    
              this.toastr.error('Problem with service. Please try again later!');
            });
       // }
        //this.router.navigate(['/theme/postad'],{queryParams:{adId:adpost.adId}});
      }
   
   
   DeleteAd(adId :number,adStatus :string): void {
  this.actionmessage  ='Are you sure to delete this Job?';
  this.adId=adId;
  this.adStatus=adStatus;
   }
   ActionDelete()
   {
    //debugger;
    if(this.adStatus!=="Active")
   {
    this.action="Delete";
    this.deleteremark="";
    this.workflowdetails=this.adId+","+this.loggedInUserId+","+this.action+","+this.deleteremark;
    //this.workflowdetails=this.adId+","+this.deleteremark+","+this.action;
        this.AdDetailsService.updatewithWorkflow(this.workflowdetails).subscribe(response => {
          if (response.status === 200) {
            //debugger;
            console.log(response);
            if ((<any>response)._body === "true")
              this.toastr.success('Job successfully deleted.');
            else
              this.toastr.info('Job is active on website.');
            //debugger

          }
          else {
            this.toastr.warning('Problem with service. Please try again later!');
          }

          this.getAdList();
        },

          (error) => {
            console.log(error);

            this.toastr.error('Problem with service. Please try again later!');
          });      
    }
      else{
        this.toastr.info('This is active Ad, can not be deleted.');

      }     
  }
   NavigateToPostJob()
  {
    //debugger
    this.router.navigate(['/theme/postad'],{ queryParams: { Job:"Jobs" } });
   }
  
  gettoday(){ 
    //debugger;
    var CurrentDate = new Date();
    this.minDate = CurrentDate.toJSON().split('T')[0];
    CurrentDate.setMonth(CurrentDate.getMonth() + 1);
    this.maxDate = CurrentDate.toJSON().split('T')[0];
  }
  adDetailsView(adId: number) {

    this.router.navigate(['/theme/addetailsview'], { queryParams: { adId: adId,Job: 'Jobs' } });
  }
 }
 

