import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Country } from './country.model';
import { State } from './state.model';
import { City } from './city.model';
import { RequestOptions,Http,Response, Headers } from '@angular/http';
import { environment } from '../../../environments/environment';



@Injectable({providedIn: 'root'})

export class CityService 
{
  baseUrl = environment.baseUrl;
  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });

  constructor(private _httpService: Http,private http:HttpClient){ }

  addcity(City: City){
    debugger
        let body = JSON.parse(JSON.stringify(City));
    console.log(City);City
     
    {    
          
      return this._httpService.post(this.baseUrl + 'city/', body,this.options);
    console.log(body);
      }
    }
  
    updateCity(formData: City) {
      debugger
      return this._httpService.put(this.baseUrl + 'city/' + formData.cityId, formData, this.options);
    } 
  
    getCityList(): Observable<any> {
      return this._httpService.get(this.baseUrl + 'city');
    }

  fillCityList(): Observable<any> {
    return this._httpService.get(this.baseUrl + 'city/GetCityList');
  }

  getCityListWithPagination(paginationFilter:string): Observable<any> {
    return this._httpService.get(this.baseUrl + 'city/GetCityWithPagination?PaginationFilter='+paginationFilter,this.options);
  }
    getCityById(cityId: City): Observable<any> {
      return this._httpService.get(this.baseUrl + 'city/'+cityId);
    }
    deleteCity(cityId : number){
     
      return this._httpService.delete(this.baseUrl + 'city/' + cityId, this.options);
     }
     CountryDDL(): Observable<Country[]>
     {   
       return this.http.get<Country[]>(this.baseUrl + 'common/CountryData');
     }


     StateDDL(CountryId: string): Observable<State[]>
     {
       return this.http.get<State[]>(this.baseUrl + 'common/StateData?CountryId='+CountryId);
     }

     CityDDL(StateId: string): Observable<City[]>
     {
       return this.http.get<City[]>(this.baseUrl + 'common/CityData?StateId='+StateId);
     }
}
