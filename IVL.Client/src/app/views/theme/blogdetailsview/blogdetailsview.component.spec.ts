import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogdetailsviewComponent } from './blogdetailsview.component';

describe('BlogdetailsviewComponent', () => {
  let component: BlogdetailsviewComponent;
  let fixture: ComponentFixture<BlogdetailsviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogdetailsviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogdetailsviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
