import { Observable } from 'rxjs';

/**
* Created by Tareq Boulakjar. from angulartypescript.com
*/
export class EventDetails {

  public eventId: number;
  public Description: string;
  public ImageUrl: string;
  public PostedFor: number;
  public Website: string;
  public EventDate: Date;
  public AddressStreet1: string;
  public AddressStreet2: string;
  public EventStatusForDetailsView: string;
  public CountryId: string;
  public StateId: string;
  public CityId: string;
  public ZipCode: string;
  public IsVisible: boolean;
  public IsActive: boolean;

  //public DateTime? CreatedDate { get; set; }
  //      public long ? CreatedBy { get; set; }
  //      public DateTime ? UpdatedDate { get; set; }
  
  public CreatedBy: string;
  public UpdatedBy: string;
  public PostedForName: string;
  public CityName: string;
  public CategoryId: number;
  public CategoryName: string;
  //public string CategoryName { get; set; }

  public StateName: string;
  public ContactPersonName: string;
  public AlternateContactNumber: number;
  public FaxNumber: number;
  public CountryName: string;
  public ContactNumber: string;
  public validFromDate: Date;
  public validTillDate: Date;



  public Title: string;
  public Activity: boolean;
  public WorkflowstatusId: string;
  public Remarks: string;
  public StartDate: string;
  public EndDate: string;
  public Time: string;
  public createdBy: string;
  public EventImageCount: number;
  public PlanId: number;
  public Email: string;
  public CountryCodeContact: string;
  public OtherCity: string;
  public IsPaidad: boolean;

  constructor() {
  }
}

export class WorkflowstatusDTO {

  public workflowStatusId: number;
  public Status: string;
  public IsActive: boolean;
  public CreatedDate: string;
  public CreatedBy: string;
  public updatedDate: string;
  public updatedBy: string;
  constructor() {
  }

}

