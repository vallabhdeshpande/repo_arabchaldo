import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import { RequestOptions,Http,Response, Headers } from '@angular/http';
import { category}  from './category';
import { environment } from '../../../environments/environment';


import {Subcategory} from './subcategory';

@Injectable({  providedIn: 'root'})

export class SubcategoryService 
{
  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });
  baseUrl = environment.baseUrl;

  constructor(private _httpService: Http,private http:HttpClient){ }

  addSubCategory(Subcategory: Subcategory)
  {
        let body = JSON.parse(JSON.stringify(Subcategory));
    console.log(Subcategory);Subcategory
      //let headers = new Headers({ 'Content-Type': 'application/json' });
      //let options = new RequestOptions({ headers: headers });
    {    
     return this._httpService.post(this.baseUrl + 'SubCategory', body, this.options);
    console.log(body);
      }
  }
  updateSubCategory(formData: Subcategory) {
    return this._httpService.put(this.baseUrl + 'subcategory/' + formData.subCategoryId, formData, this.options);
  } 

  getSubCategoryList(): Observable<any> {
    return this._httpService.get(this.baseUrl + 'SubCategory');
  }


  FillSubcategoryList(): Observable<any> {
    return this._httpService.get(this.baseUrl + 'subcategory/GetSubCategoryList');
  }
  getSubCategoryById(subCategoryId: Subcategory): Observable<any> {
debugger
    return this._httpService.get(this.baseUrl + 'SubCategory/' +subCategoryId);
  }
  deleteSubCategory(CategoryId : number){
    return this._httpService.delete(this.baseUrl + 'SubCategory/' + CategoryId, this.options);
   }
   CategoryDDL(): Observable<category[]>
  {   
    return this.http.get<category[]>(this.baseUrl + 'Common/CategoryData');
    
  }

}





