import { Component, OnInit } from '@angular/core';
import { } from '../buysell.service';
import { Router } from '@angular/router';
import { BuysellService } from '../buysell.service';
import { Buysell} from '../buysell.model';
import { environment } from '../../../../environments/environment';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { OrderPipe } from 'ngx-order-pipe';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-managebuysell',
  templateUrl: './managebuysell.component.html',
  styleUrls: ['./managebuysell.component.scss'],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }]
})
export class ManagebuysellComponent implements OnInit {
  loggedInRole:string;
  loggedInUserId:string;
  buysellposts: Buysell[];
  setType:string="";
  setStatus: string = "";
  commonimagepath = environment.imagepath;
 
  //minValidTillDate: string;
  //maxValidFromDate: string;
  modalexit:string="";
  LoggedInUserId:string;
  showDates: string = "col-md-6";
  approveButtonText: string;
  approveButtonStyle: string;
  showPremium:string="col-md-3";
  remarkstring: string[];
  poststring: string;
  remarkstring1: string;
  remarkstring2: string;
  remarkstring3: string;
  isLoading: boolean = false;
  adStatus:string;
  Ispremium:boolean = false;
  filter: string = "";
  form: FormGroup;
  workflowdetails:string;
  statusMessage: string;
  deleteremark:string;
  activity:boolean;
  visibilitybutton:string;
  visibilityspan:string;
  setStatusActive:string="";
  setStatusInactive:string="";
  buysellpost = new Buysell();
  order: string;
  reverse: any ;
  modalRemark:string;
  public pageSize: number = 10;
  public p: number;
  remark: string;
  workflowstatusId:number;
  action:string="";
  actionmessage:string="";
  buySellId = 0;
  minDateModel: NgbDateStruct;
  minValidTillDate: NgbDateStruct;
  maxValidFromDate: NgbDateStruct;
  validTill: string;
  validFrom: string;
  maxDate: string;
  minDate: string;
  date: { year: number, month: number };
  hoveredDate: NgbDate;
  modelValidFrom: Date = null;
  modelValidTill: Date = null;

  selectToday() {
    this.minDateModel = this.calendar.getToday();
    this.minValidTillDate = this.calendar.getToday();
  }
  onvalidFromDateSelect(event: any) {
    this.minValidTillDate = event;
    this.validFrom = this.modelValidFrom.toJSON().split('T')[0];

  }
  onvalidTillDateSelect(event: any) {
    this.maxValidFromDate = event;
    this.validTill = this.modelValidTill.toJSON().split('T')[0];

  }
  constructor(private router: Router, private BuysellService: BuysellService,
    private toastr: ToastrService, private orderPipe: OrderPipe, private fb: FormBuilder, private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private config: NgbDatepickerConfig) { }
    
    ngAfterViewInit(){      
      document.getElementById("#modalvalidFrom").setAttribute("min", this.minDate);
      document.getElementById("#modalvalidTill").setAttribute("min", this.minDate);
     
   
       }
       toggleVisibility(e){
        this.Ispremium= e.target.checked;
      }
  ngOnInit() {
    debugger;
    this.loggedInUserId=localStorage.getItem('userId');
      this.getBuySellList();
      this.selectToday();
      //this.myDateValue = new Date();
  }
  
  getBuySellList(): void {
    debugger;
    this.BuysellService.getBuySellList().subscribe(buysellposts => {
      this.buysellposts = buysellposts;
      
      console.log(this.buysellposts);
      // Status
     debugger;
      var adlenth=buysellposts.length;
      
     for (let i = 0; i < adlenth; i++) {

     
      //debugger;
        if(buysellposts[i].isFeatured==="True")
        {          
          buysellposts[i].isFeatured="Premium";
          this.setType="badge badge-warning";
        }
        else {
          buysellposts[i].isFeatured=" ";
         this.setType="";
        }
       }
       this.buysellposts = buysellposts;

       if (this.buysellposts != null) {
         adlenth = buysellposts.length;
         if (buysellposts.length > 0) {
           this.buysellposts = buysellposts;
         }
         else
           this.buysellposts = [];
       }
       else
         this.buysellposts = [];
 
   
    });
  }

  updatePost(buysellpost:Buysell): void {
    debugger;
    this.router.navigate(['/theme/addbuysell'],{queryParams:{buysellid:buysellpost.BuySellId}});
    };
  getStatus(Status: string) {  

    if(Status=="Active")
      return this.setStatus="badge badge-success";

        if(Status=="InActive")
        return this.setStatus="badge badge-danger";

        if(Status=="Pending")
          return  this.setStatus="badge badge-warning";    

  }

hideActivity(Status: string) {

  if (Status == "Active")
    return "d-none";

  if (Status == "InActive")
    return "d-none";

  if (Status == "Pending")
    return "";

} 
getType(Premium) {  
  // debugger
       if(Premium=="Premium")
         return this.setStatus="badge badge-warning";
   else
        return this.setStatus=" ";
      
   
   } 
   getPicUrl(picurl:string,buysellid)
   {
     if(picurl==null)
     return "assets/img/default/no_image_placeholder.jpg";
     else if (picurl=="")
     return "assets/img/default/no_image_placeholder.jpg";
     else 
     return this.commonimagepath+'BuySell/'+buysellid+'/Image/'+picurl;
   }
   createForm() {
    this.form = this.fb.group({
      validFrom: ['', Validators.required],
      validTill: ['', Validators.required]
    }, { validator: this.dateLessThan('validFrom', 'validTill') });
  }


  dateLessThan(from: string, to: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let f = group.controls[from];
      let t = group.controls[to];
      if (f.value > t.value) {
        return {
          dates: "Date from should be less than Date to"
        };
      }
      return {};
    }
  }
  sortedCollection: any[];


  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }
  ActionOnApprove(buySellId,isFeatured,event)
  {
   
debugger;
//$("#modalRemarkText").value="";
// (<HTMLInputElement>document.getElementById('remark')).value="";
    this.remark = null;
      this.validFrom = null;
    this.validTill = null;
    this.modelValidFrom = null;
    this.modelValidTill = null;
    this.maxValidFromDate = null;
    this.minValidTillDate = this.calendar.getToday();
     this.showDates="col-md-6";
    this.buySellId=buySellId;
    this.action = "Approve";
    this.approveButtonText = "";
    this.approveButtonText = "Approve";
    this.approveButtonStyle = "";
    this.approveButtonStyle = "btn btn-sm btn-success";
    this.actionmessage ="Are you sure to approve this buy/sell listing?";
    this.showPremium  ="col-md-3"
    if(isFeatured=="Premium")
    this.Ispremium=true;
    else
    this.Ispremium=false;
  }
  ActionOnReject(buySellId,event)
  {
    this.remark = null;
    this.validFrom = null;
    this.validTill = null;
    this.modelValidFrom = null;
    this.modelValidTill = null;
    this.maxValidFromDate = null;
    this.minValidTillDate = this.calendar.getToday();
    this.showDates="col-md-6 d-none"; 
    this.buySellId=buySellId;
    this.action = "Reject";
    this.approveButtonText = "";
    this.approveButtonText = "Reject";
    this.approveButtonStyle = "";
    this.approveButtonStyle = "btn btn-sm btn-warning";
    this.actionmessage="Are you sure to reject this record?";
    this.Ispremium=false;
    this.showPremium = "col-md-3  d-none";
  }

  ActionOnPost(remark)
  {
    debugger;
    this.poststring = remark;
    this.remarkstring = this.poststring.split(',');
     this.remarkstring1 = this.remarkstring[0];
     this.remarkstring2 = this.remarkstring[1];
    //this.remarkstring1 = '2020-30-1';
    //this.remarkstring2 = '2020-2-20';
    this.remarkstring3 = this.remarkstring[2].replace(/,/g, ' ');

    //if (this.remarkstring[3] == "true")
    //  this.Ispremium = true
    //else
    //  this.Ispremium = false;

    if ((this.approveButtonText == "Approve") && ((this.remarkstring1 == "null") || (this.remarkstring2 == "null") )) {
      this.toastr.warning("All fields are required to approve");
      return;
    }

    if ((this.approveButtonText == "Reject") && (this.remarkstring3 == "null")) {
      this.toastr.warning("Remark is required to reject");
      return;
    }
    
    this.workflowdetails=this.buySellId+","+this.loggedInUserId+","+this.action+","+remark;
    

    //if (confirm(this.actionmessage)) {
      this.BuysellService.updatewithWorkflow( this.workflowdetails).subscribe(response => {
        console.log(response);
        console.log('Updated');
        this.modalexit="modal";
        document.getElementById("closeBtn").click();
       // this.managemodal="modal fade";
        // jQuery('remark').modal('hide');
        this.getBuySellList();
        if(this.action==="Approve")
        this.toastr.success('Buy/Sell listing approved succesfully');
        if(this.action==="Reject")
          this.toastr.warning('Buy/Sell listing rejected');
       
      }, 

        (error) => {
          console.log(error);

          this.toastr.error('Problem with service. Please try again later!');
        });
   // }
    //this.router.navigate(['/theme/postad'],{queryParams:{adId:adpost.adId}});
    
  }
// close modal popup
  
DeleteAd(buySellId :number,adStatus :string,event) {
  debugger
  this.actionmessage ='Are you sure to delete this Buy/Sell listing ?';
       this.buySellId=buySellId;
       this.adStatus=adStatus;
   }

  ActionDelete(): void {
    debugger;
    if(this.adStatus!=="Active")
    {
    this.action="Delete";
    this.deleteremark="";
    this.workflowdetails=this.buySellId+","+this.loggedInUserId+","+this.action+","+this.deleteremark;
    //this.workflowdetails=this.adId+","+this.deleteremark+","+this.action;
        this.BuysellService.updatewithWorkflow(this.workflowdetails).subscribe(response => {
          if (response.status === 200) {
            debugger;
            console.log(response);
            if ((<any>response)._body === "true")
              this.toastr.success('Buy/Sell Listing successfully deleted.');
            else
              this.toastr.info('Buy/Sell Listing is active on website.');
            debugger

          }
          else {
            this.toastr.warning('Problem with service. Please try again later!');
          }

          this.getBuySellList();
        },

          (error) => {
            console.log(error);

            this.toastr.error('Problem with service. Please try again later!');
          });      
    }
      else{
      this.toastr.info('This is active Buy/Sell Listing, can not be deleted.');

      }     
  }
  NavigateToAddBuySell( )
  {
      debugger
     this.router.navigate(['/theme/addbuysell']);
     };

    buysellDetailsView(buysellId:number) {

      this.router.navigate(['/theme/buyselldetailsview'], { queryParams: { buysellId: buysellId } });
    }
}
