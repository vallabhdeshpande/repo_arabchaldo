import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagebuysellComponent } from './managebuysell.component';

describe('ManagebuysellComponent', () => {
  let component: ManagebuysellComponent;
  let fixture: ComponentFixture<ManagebuysellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagebuysellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagebuysellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
