import { Component, OnInit } from '@angular/core';
import { MessageBoard,MessageBoardDetails}  from '../../shared/messageboard.model';
import { MessageboardService}  from '../../shared/messageboard.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-message-board',
  templateUrl: './message-board.component.html',
  styleUrls: ['./message-board.component.scss']
})
export class MessageBoardComponent implements OnInit {
  messageboards: MessageBoard[];
  public pageSize: number = 10;
  public p: number;
  sortedCollection: any[];
  ArrMBD: any[]=[];
  ArrMB: any[]; 
  MBData: any[] = [];
  order: string;
  reverse: any ;
  messageboardId: number = 0;
  MessageBoardId: number = 0;
  messageBoardId: number = 0;
  MessageBoardReplyId: number = 0;
  actionmessage: string;
  actionreplymessage: string;
  action: string;
  filter: string = "";
  messageBoardTopic:string="";
  messageboardIdforReply: number;
  MessageBoardDetailsId: number = 0;
  showReply:string="d-none";
  showGrid:string="card-body";
  messageboardsdetails: MessageBoardDetails[];
  showAddTopic:string="row row-stretch d-none";
  showCategory:string="col-sm-12";
  pageTitle:string="Message Board";
  FormMessageBoard: any;
  loggedInUserName: string = "";
  NoReplys: string = "d-none";
  constructor(private _messageBoardService: MessageboardService, private _router: Router,private toastr: ToastrService,) { }

  ngOnInit() {
    this.getTopicList()

  }


  ShowGridPanel() {
    debugger;
    this.showGrid = "card-body";
    this.showAddTopic = "row row-stretch d-none";
    this.getTopicList();
    this.showReply = "d-none";
    this.messageBoardTopic = "";
    //this.loggedInUserName="";
    //document.getElementById("Topic").innerHTML = "";
  }

  getTopicList():void{
    debugger;
  this._messageBoardService.getMessageBoardList().subscribe(data => {
    this.messageboards = data;
  console.log(this.messageboards);
  });
  }

DeleteMessgae(messageboardId)
{
  debugger
  this.messageboardId=messageboardId;
  this.actionmessage="Are you sure to Delete this Messgae?"
}


  DeleteMessgaeReply(MessageBoardDetailsId) {
    debugger;
    //this.MessageBoardId = MessageBoardId;
    this.MessageBoardDetailsId = MessageBoardDetailsId;
    this.actionmessage = "Are you sure to Delete this Messgae?"
  }


setOrder(value: string) {
  if (this.order === value) {
    this.reverse = !this.reverse;
  }

  this.order = value;
}

  getMessageBoardReplyList(messageBoardId, messageBoardTopic)
{
  debugger;
  this.messageboardIdforReply = messageBoardId;
    this.messageBoardTopic = messageBoardTopic;
    this.messageboardId = messageBoardId;
 //document.getElementById("Topic").setAttribute("disabled","true");
  this.showAddTopic="row row-stretch d-none";
  this.showGrid = "card-body d-none";
  this.showReply = "";
 // let mbdetailsdata:any[]=[];
this._messageBoardService.getMessageBoardDetailsListByBoardId(messageBoardId).subscribe(data => {   
//mbdetailsdata = data;
//this.messageboardsdetails = mbdetailsdata;
  this.MBData = data;
  //this.ArrMB = data[0];
  this.ArrMB = [];
  this.ArrMB.push(data[0]);
  var len = data.length - 1;
  this.ArrMBD = [];
  
  for (let i = 1; i <= this.MBData.length-1; i++)
  
  {
    this.ArrMBD.push(this.MBData[i]); 
  }
  console.log(this.ArrMBD);
  if (this.MBData.length-1 == 0) {
    this.NoReplys = "no-record";
 
  }
  else
   
    this.NoReplys = "d-none";
});

   
}


  ActionDelete()
{
  debugger;
    this._messageBoardService.deleteMessageBoard(this.messageboardId,"Topic").subscribe(response => {
      console.log(response);
      this.getTopicList();
      if((<any>response)._body==="true")
      
       {
        this.toastr.success('Message deleted successfully.');
     }
       else
       {
        this.toastr.info('This Message is linked with an active post cant be deleted');
       }
    },

      (error) => {
        console.log(error);

        this.toastr.error( 'Problem with service. Please try again later!');
      });
  
}


  ActionDeleteReply() {
    debugger;
    this._messageBoardService.deleteMessageBoardDetails(this.MessageBoardDetailsId,"Reply").subscribe(response => {
      console.log(response);

      if ((<any>response)._body === "true") {
        this.toastr.success('Reply deleted successfully.');
      }
      else {
        this.toastr.info('This Message is linked with an active post cant be deleted');
      }
      this.getMessageBoardReplyList(this.messageboardId, "");
    },

      (error) => {
        console.log(error);

        this.toastr.error('Problem with service. Please try again later!');
      });
  }
}
