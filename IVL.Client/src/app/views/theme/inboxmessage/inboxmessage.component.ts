import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl,FormBuilder,Validators } from "@angular/forms";
import { Observable } from 'rxjs';
import { User } from '../user'
import { HttpEventType, HttpClient } from '@angular/common/http';
import { CommonService } from '../../shared/common.service';
import {WebsiteService} from '../../../shared/website.service';
import { ToastrService } from 'ngx-toastr';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { environment } from '../../../../environments/environment';
import { Router, ActivatedRoute } from "@angular/router";
import { MultifileuploadComponent } from '../multifileupload/multifileupload.component';
@Component({
  selector: 'app-inboxmessage',
  templateUrl: './inboxmessage.component.html',
  styleUrls: ['./inboxmessage.component.scss']
})
export class InboxmessageComponent implements OnInit {
  private _allUser: Observable<User[]>;
  tousers: string;
  showAddTopic:string="";
  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '100px',

    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['insertImage', 'insertVideo']
    ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };
  constructor(private router: Router,private http: HttpClient, private toastr: ToastrService,private formbulider: FormBuilder, private _commonService: CommonService,private WebsiteService: WebsiteService) { }


  @ViewChild('multiSelect', { read: true, static: false }) multiSelect;
  @ViewChild(MultifileuploadComponent, { static: false }) FileUploadCmpt: MultifileuploadComponent;
  //public form: FormGroup;
  form: any;
  public loadContent: boolean = false;
  public ToUserMessage = 'Cricketers';
  public data : any[]=[];
  public selectedUsers : any[]=[];
  public settings = {};
  baseUrl = environment.baseUrl;
  public selectedItems = [];
  public userNameWithEmail :string;
  mailfor:string  = "multiplemail";
  submitted = false;
  public fileresponse: string[];
  ngOnInit() {
    this.FillUserDDL();
 
  console.log(this.data );
  
    // this.data = [
    //   { item_id: 1, item_text: 'Hanoi' },
    //   { item_id: 2, item_text: 'Lang Son' },
    //   { item_id: 3, item_text: 'Vung Tau' },
    //   { item_id: 4, item_text: 'Hue' },
    //   { item_id: 5, item_text: 'Cu Chi' }
    // ];
    // setting and support i18n

    //userNameWithEmail = this.data.
    debugger;

    this.settings = {
      singleSelection: false,
      idField: 'email',
      textField:'userName',
      enableCheckAll: true,
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      allowSearchFilter: true,
      limitSelection: -1,
      clearSearchFilter: true,
      maxHeight: 197,
      tousers:String,
      itemsShowLimit: 10,
      searchPlaceholderText: 'Select User',
      noDataAvailablePlaceholderText: 'No Data',
      closeDropDownOnSelection: false,
      showSelectedItemsAtTop: false,
      defaultOpen: false
   
    };
    this.setForm();


    this.form = this.formbulider.group({
      mailfor: ['', ''],
      content: ['', [Validators.required]],    
      subject: ['', [Validators.required,Validators.pattern(".*\\S.*[a-zA-z ]")]],
      ToUserMessage:['',Validators.required],
      fileUrl:['','']
    }); 
  }
  public uploadFinishedFile = (event) => { this.fileresponse = event;  }
  public setForm() {
    this.form = new FormGroup({
      ToUserMessage: new FormControl(
        this.data
      ),
      subject:new FormControl(
        this.data),
        content:new FormControl(
          this.data
        )
      
    });
    this.loadContent = true;
  }

  public save() {
    debugger;

    this.submitted = true;
  
    // stop the process here if form is invalid
    if (this.form.invalid) {
// 	//debugger
return;
}


    console.log(this.form.value);
  
   this.form.value.ToUserMessage =this.form.value.ToUserMessage;
   this.tousers="";
    for(let i=0; i<this.form.value.ToUserMessage.length; i++)
    {
      this.tousers+=this.form.value.ToUserMessage[i].email+',';

    }
    this.tousers = this.tousers.substring(0, this.tousers.length - 1);
   
   console.log(this.tousers);
   this.form.value.ToUserMessage=this.tousers;
   this.form.value.subject = this.form.value.subject;
    this.form.value.content = this.form.value.content;
    this.form.value.mailfor = this.mailfor;
    this.form.value.fileUrl=this.form.value.fileUrl;
    if (this.fileresponse !== undefined)
    this.form.value.fileUrl = this.fileresponse;
  else
    this.form.value.fileUrl = "";
    this.WebsiteService.multipleemail(this.form.value)
    .subscribe((response) => {
      console.log(response);
       this.toastr.success('Message sent successfully.');
      //  this.ContactForm.patchValue({
      //   Name:"",    
      //   Email:"",
      //   Comments:"",
      //   Phone:"",
      //   reason:"",
    
    
      // });
 
     this.form.reset();
     this.FillUserDDL();
     this.submitted = false;
    // this.form.resetSelection();
      this.ngOnInit();
      this.FileUploadCmpt.onfiledelete(0);
     },
     (error) => {
      console.log(error);
     this.toastr.error('Problem with service. Please try again later!');
    
    }
    
    );
    }
   
    resetSelection() {
      this.selectedUsers = [];        
      }
  public resetForm() {
    // beacuse i need select all crickter by default when i click on reset button.
    this.setForm();
    this.multiSelect.toggleSelectAll();
    // i try below variable isAllItemsSelected reference from your  repository but still not working
    // this.multiSelect.isAllItemsSelected = true;
  }

  public onFilterChange(item: any) {
    console.log(item);
  }
  public onDropDownClose(item: any) {
    console.log(item);
  }

  public onItemSelect(item: any) {
    console.log(item);
    this.selectedUsers.push(item.email);
    console.log("SelectedUsers "+this.selectedUsers);
  }
  public onDeSelect(item: any) {
    this.selectedUsers.splice(item,item.email);
    console.log("SelectedUsers "+this.selectedUsers);
  }

  public onSelectAll(items: any) {
    console.log(items);
   // this.selectedUsers.push(item.email);
    console.log("SelectedUsers "+this.selectedUsers);
  }
  public onDeSelectAll(items: any) {
    console.log(items);
    //this.selectedUsers.push(item.email);
    console.log("SelectedUsers "+this.selectedUsers);
  }

  FillUserDDL() {

   //this._allUser = this._commonService.UserDDL();
    <any>this._commonService.UserInboxMessageDDL().subscribe(res => {
    this.data=res;
    console.log(this.data);
});
  }

  refresh(): void {
    debugger;
    window.location.reload();
    this.FileUploadCmpt.ngOnInit();

  }
  resetFeilds() {
    debugger;
    this.FileUploadCmpt.ngOnInit();
   // this.router.navigate(['/theme/managecategory']);

  }
  Navigatetodashboard() {
   
    this.router.navigate(['/dashboard']);
  }
}
