import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http'; 
import { RequestOptions,Http,Response, Headers } from '@angular/http';
import { Setting } from './setting'; 
import { environment } from '../../../environments/environment';


@Injectable()
export class ConfigurationService 
{
  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });
  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient,private _httpService: Http, private https: Http){ }



  getConfiguration(): Observable<any>
  {
    return this._httpService.get(this.baseUrl + 'configuration');
  }
      
  updateConfiguration(formData: Setting):Observable<any> 
  {
    return this._httpService.put(this.baseUrl + 'configuration/' + formData.configurationId, formData, this.options);
  }
}
