import { Component, OnInit, ViewChild  } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { Router, ActivatedRoute } from "@angular/router";
import { OrderPipe } from 'ngx-order-pipe';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import {} from '../category';
  import { from } from 'rxjs';
import { category } from '../../shared/common.model';
import { ToastrService } from 'ngx-toastr';
import { Http } from '@angular/http';
import { HttpClient, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { UploadComponent } from '../upload/upload.component';

@Component({
  selector: 'theme-managecategory',
  templateUrl: './managecategory.component.html',
  styleUrls: ['./managecategory.component.scss']
})

export class ManagecategoryComponent implements OnInit {

  //@ViewChild('uploadLogo');
  @ViewChild(UploadComponent, { static: false }) UploadCmpt: UploadComponent;
  // private UploadCmpt: UploadComponent;
  users: User[];
  submitted = false;
  FormCategoryname: any;
  filter: string = "";
  statusMessage: string;
  logopath: string[];
  imagepath: any[];
  public imageresponse: string[];

  categories: category[];
  category = new category();
  user = new User(); order: string;
  reverse: boolean;
  CategoryId: number = 0;
  actionMessage: string;
  public pageSize: number = 10;
  public p: number;

  getCategoryList(): void {
    //this.Userservice.getCategoryList().subscribe(users => {
    this.Userservice.FillCategoryList().subscribe(users => {
      this.users = users.json();
      console.log(this.users);

    });
  }


  sortedCollection: any[];

  constructor(private toastr: ToastrService, private orderPipe: OrderPipe, private Userservice: UserService, private router: Router, private formbulider: FormBuilder, private httpservice: Http) {
    this.sortedCollection = orderPipe.transform(this.users, 'info.firstName');
    // this.UploadCmpt = new UploadComponent();

    console.log(this.sortedCollection);
  }

  //constructor(private OrderPipe: OrderPipe, private Userservice: UserService, private router: Router, private formbulider: FormBuilder) {
  //  this.sortedCollection = OrderPipe.transform(this.users, 'categoryName');
  //  console.log(this.sortedCollection);
  //}



  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
    this.order = value;
  }


  ngOnInit() {
    debugger
    {
      // this.UploadCmpt.ngOnInit();
      this.getCategoryList();

      this.imageresponse = null;
      this.category.CategoryImageUrl = null;


    }
    this.FormCategoryname = this.formbulider.group({
      categoryName: ['', [Validators.required]],
      arabicName: ['', [Validators.required]],
      description: [''],
      CategoryImageUrl: ['']
    });

  }



  public uploadFinishedLogo = (event) => {
    debugger;
    this.imageresponse = event;
  }

  addCategory(): void {
    debugger

    this.submitted = true;
    this.FormCategoryname.value.CategoryImageUrl = this.imageresponse;
    // stop the process here if form is invalid
    if (this.FormCategoryname.invalid) {
      debugger
      return;
    }
    console.log(this.FormCategoryname.value);
    if (this.FormCategoryname.value.CategoryImageUrl == null || this.FormCategoryname.value.CategoryImageUrl == undefined) {
      this.toastr.warning("Category image missing. Please upload an image.");
      return;
    }

    this.category.CategoryName = this.FormCategoryname.value.categoryName;
    this.category.ArabicName = this.FormCategoryname.value.arabicName;
    this.category.CategoryDescription = this.FormCategoryname.value.description;
    this.FormCategoryname.value.CategoryImageUrl = this.imageresponse;
    this.category.CategoryImageUrl = this.FormCategoryname.value.CategoryImageUrl;
    console.log(this.FormCategoryname.value)
    this.Userservice.addCategory(this.FormCategoryname.value)
      .subscribe((response) => {
        console.log(response);
        debugger
        if (response.status === 200) {
          console.log(response);
          if ((<any>response)._body === "true")

            this.toastr.success('Category inserted successfully.');
          else
            this.toastr.info('Category already exist.');


          this.submitted = false;
          //this.getCategoryList();
          this.ngOnInit();
          this.UploadCmpt.onimagedelete(0);

        }

        (error) => {
          console.log(error);

          this.toastr.error('Problem with service. Please try again later!');
        }
      },



      );

  }

  refresh(): void {
    debugger;
    window.location.reload();
    this.UploadCmpt.ngOnInit();

  }
  resetFields() {
    debugger;
    this.UploadCmpt.ngOnInit();
    this.router.navigate(['/theme/managecategory']);

  }


  reset() {
    this.ngOnInit();
    this.UploadCmpt.onimagedelete(0);
  }

  cancel() {

    this.ngOnInit();
    this.UploadCmpt.onimagedelete(0);
  }

  onDelete(CategoryId: number, CategoryName: string, event)
  {
    debugger;
    this.CategoryId = CategoryId;
    this.actionMessage = 'Are you sure to delete Category ' + CategoryName + '?';
  }

  ActionDelete()
  {
    debugger;
    this.Userservice.deleteCategory(this.CategoryId).subscribe(response => {
      console.log(response);
      this.getCategoryList();
      if ((<any>response)._body === "true") {
        this.toastr.success('Category deleted successfully.');
      }
      else {
        this.toastr.info('This category is linked with sub category, cant be deleted');
      }
    },

      (error) => {
        console.log(error);

        this.toastr.error('Problem with service. Please try again later!');
      });

  }
  // // Preview Images

  // public imagePath;
  // imgURL: any;
  // public imagemessage: string;
  // previewimage(files) {

  //   if (files.length === 0)
  //     return;

  //   var mimeType = files[0].type;

  //   if (mimeType.match(/image\/*/) == null) {
  //     this.imagemessage = "Only images are supported.";
  //     return;
  //   }

  //   var reader = new FileReader();
  //   this.imagePath = files;
  //   reader.readAsDataURL(files[0]);
  //   reader.onload = (_event) => {
  //     this.imgURL = reader.result;
  //   }


  // }
  updateCategory(user: category): void
  {
    debugger;
    console.log(user.CategoryId);
    this.router.navigate(['/theme/editcategory'], { queryParams: { categoryId: user.CategoryId } });
  };

}
