import { Component, OnInit } from '@angular/core';
import { UserService} from '../user.service';
import { User} from '../user';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../environments/environment';


@Component({
  selector: 'app-userdetailsview',
  templateUrl: './userdetailsview.component.html',
  styleUrls: ['./userdetailsview.component.scss']
})
export class UserdetailsviewComponent implements OnInit {
  users: User[];

  statusMessage: string;
  ProfilepicUrl: string;
  Profilepicurls = [];
  displayProfilePic: string = "col-md-4";
  displayProfilePicTitle: string = "col-form-label";

  gender:string;
  imagepath = environment.imagepath;
  user = new User();
  constructor(private UserService: UserService,private route: ActivatedRoute, private router: Router) { }

  manageAdsback()
  {this.router.navigate(['/theme/manageuser']);}
   ngOnInit() {
     this.getUserDetailsById(this.route.snapshot.queryParams['userId']);
   }
   getUserDetailsById(userId) {

     this.UserService.getUserById(userId).subscribe(users => {
       console.log(users); 
       var users = JSON.parse(users._body);
       debugger
       this.user.userId=users.userId;
       this.user.FirstName=users.firstName;
       this.user.LastName=users.lastName;
       this.user.roleName=users.roleName;
      
       if(users.gender==1)
       this.user.gender="Male"
       else
       this.user.gender="Female"
       this.user.ContactNumber=users.contactNumber;
       this.user.Email=users.email;
       this.user.alternateContactNumber=users.alternateContactNumber;
       this.user.FaxNumber=users.faxNumber;  
       this.user.AddressStreet1=users.addressStreet1;
       this.user.AddressStreet2=users.addressStreet2;
       this.user.CountryId=users.countryName;
       this.user.CountryName='USA';
       if(users.countryCodeContact===null ||users.countryCodeContact==="1")
       users.countryCodeContact="USA (+1)";
       else
       users.countryCodeContact="(+" +users.countryCodeContact+")";
       this.user.CountryCode=users.countryCodeContact;
       this.user.StateId=users.stateId;
       this.user.StateName=users.stateName;
       this.user.cityName=users.cityName;  
       this.user.ZipCode=users.zipCode;       
       this.user.description=users.description;
       if (users.profilePicUrl!==null) {
         this.ProfilepicUrl = this.imagepath + 'UserProfile/' + userId + '/ProfilePic/' + users.profilePicUrl;
         this.Profilepicurls.push(this.ProfilepicUrl);
         this.displayProfilePic = "col-md-4";
         this.displayProfilePicTitle = "col-form-label";
       }
       else {
         this.displayProfilePic = "col-md-4 d-none";
         this.displayProfilePicTitle = "col-form-label d-none";
       }
        
     });
   }
  }
