import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserdetailsviewComponent } from './userdetailsview.component';

describe('UserdetailsviewComponent', () => {
  let component: UserdetailsviewComponent;
  let fixture: ComponentFixture<UserdetailsviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserdetailsviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserdetailsviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
