import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {User} from './user';
import {HttpClient} from '@angular/common/http'; 
import { RequestOptions,Http,Response, Headers } from '@angular/http';
import {Manageblog} from './manageblog';
import { from } from 'rxjs';
import { environment } from '../../../environments/environment';
import { UserInfo } from 'os';

@Injectable({
  providedIn: 'root'
})
export class ManageblogService {
  data;
  observable;
  //loggedInUserId;
  cachedloggedInUserId;

  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });
  baseUrl = environment.baseUrl;
  loggedInUserId = localStorage.getItem('userId');
  loggedInRole = localStorage.getItem('roleName');
  constructor(private http: HttpClient, private _httpService: Http, private https: Http) { }

  addBlog(manageblog: Manageblog) {
     
    this.data = null;
    this.observable = null;
    debugger;
    let body = JSON.parse(JSON.stringify(manageblog));
    console.log(manageblog);
    //let headers = new Headers({ 'Content-Type': 'application/json' });
    //let options = new RequestOptions({ headers: headers });                     
    return this._httpService.post(this.baseUrl + 'BlogDetails', body, this.options);
    console.log(body);
  }


  getBlogList(): Observable<any> {
    // delete this comment
    debugger;
    this.loggedInUserId = localStorage.getItem('userId');
    this.loggedInRole = localStorage.getItem('roleName');

    if (this.cachedloggedInUserId != this.loggedInUserId) {
      this.data = null;
      this.observable = null;
    }
    
    if (this.data) {
      console.log(' first if condition SA Blogs');
      return Observable.of(this.data);
    }
    else if (this.observable) {
      console.log('2nd if condition blogs');
      console.log(this.observable);
      return this.observable;
    }
    else {
      //this.observable = this.http.get(this.baseUrl + 'BannerDetails/GetAllBanner?=' + userId, {
      //  observe: 'response'
      //})
      this.cachedloggedInUserId = this.loggedInUserId;
      this.observable = this.http.get(this.baseUrl + 'BlogDetails/GetAllBlog?userInfo=' + this.loggedInUserId + "," + this.loggedInRole,
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          console.log('fetch data from server for Blogs');
          if (response.status === 400) {
            console.log('request failed');
            return 'request failed';
          }
          else if (response.status === 200) {
            console.log('request successful');

            this.data = response;
            console.log('last');
            console.log(this.data);
            return this.data;
          }
        });
      console.log('End');
      return this.observable;
    }



    //return this._httpService.get(this.baseUrl + 'BlogDetails?userInfo=' + this.loggedInUserId + "," + this.loggedInRole);
  }

  deleteBlog(BlogId: number) {
    this.data = null;
    this.observable = null;
    return this._httpService.delete(this.baseUrl + 'BlogDetails/' + BlogId, this.options);
  }

  getBlogById(BlogId: number) {
    debugger;
    return this._httpService.put(this.baseUrl + 'BlogDetails/' + BlogId, Manageblog);
  }

  getBlog(blogId: number): Observable<any> {
    return this._httpService.get(this.baseUrl + 'BlogDetails/' + blogId);
  }

  updateBlog(manageblog: Manageblog) {
    debugger
    this.data = null;
    this.observable = null;
    let body = JSON.parse(JSON.stringify(manageblog));
    //let headers = new Headers({ 'Content-Type': 'application/json' });
    //let options = new RequestOptions({ headers: headers });
    return this._httpService.put(this.baseUrl + 'BlogDetails/' + manageblog.blogId, body, this.options);
  }

  UserDDLForBlog(RoleId: number): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + 'common/UserDDLForBlog?RoleId=' + RoleId);
  }

  UserDDL(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + 'common/UserDDLForThirdParty');
  }

  updatewithWorkflow(workflowdetails: string) {
    debugger
    this.data = null;
    this.observable = null;
    return this._httpService.delete(this.baseUrl + 'BlogDetails/' + workflowdetails);
  }
}
  
  
