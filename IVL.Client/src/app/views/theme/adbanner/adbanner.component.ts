import { Component, OnInit, Inject, Output, EventEmitter, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Country, State, City, category, Subcategory, Roles, CountryCodes } from '../../shared/common.model';
import { User } from '../user';
import { UserService } from '../user.service';
import { Managebannerservice } from '../managebannerservice';
import { Managebanner } from '../managebanner';
import { CommonService } from '../../shared/common.service';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { environment } from '../../../../environments/environment';
import { element } from 'protractor';
import { first, debounce } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
//import { Paymentplan } from '../assignplan/assignplan.component';
import { Paymentplan } from '../paymentplan';
import { Paymentplanservice} from '../paymentplanservice';
import { Jsonp } from '@angular/http';
import { exists } from 'fs';
import { ReturnStatement } from '@angular/compiler';
import { UploadComponent } from '../upload/upload.component';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import * as countryCodeData from '../../shared/countrycodes.json';

@Component({
  selector: 'app-adbanner',
  templateUrl: './adbanner.component.html',
  styleUrls: ['./adbanner.component.scss']
})
export class AdbannerComponent implements OnInit
{

  @ViewChild(UploadComponent, { static: false }) UploadCmpt: UploadComponent;
  public _allUser: Observable<User[]>;
  public _allCountry: Observable<Country[]>;
  public _allCountryCodes: Observable<CountryCodes[]>;
  public _allState: Observable<State[]>;
  public _allCity: Observable<City[]>;
  public _allPlan: Observable<Paymentplan[]>;
  public previousPlan: Paymentplan;
  StateName: string = "";
  CityName: string = "";
  postedFor: string = "0";
  CountryId: string = "0";
  StateId: string = "0";
  CityId: string = "0";
  logopath: string[];
  imagepath: any[];
  logoUrl: string = "";
  imageUrl: string = "";
  ABCD: string[];
  states: any[];
  cities: any[];
  users: any[];
  //dvImage: string[];
  FormPostBanner: any;
  HorizontalView: boolean;
  VerticalView: boolean;
  actionButton;
  action = "";
  myData = null;
  formData = null;
  msgs = null;
  public imagefilename: string[];
  public logofilename: string[];
  statusMessage: string;
  saveButtonText: string = "Save";
  displayText = 'btn btn-sm btn-primary';
  displayExtLogo = 'col-md-6 d-none';
  displayExtImages = 'col-md-6 d-none';
  displayExtUploads = 'form-bg-section d-none';
  maxDate: string;
  validTill: string;
  extLogoUrls = [];
  extLogos = [];
  //resetButtonStyle:string="block";
  public logoresponse: string[];

  addbutton: HTMLElement;
  serverpath = environment.serverpath;
  commonimagepath = environment.imagepath;
  submitted = false;
  Id: number;
  remainingBannerImage: number;
  IsFeatured: boolean;
  subscription: any;
  countryCodeList: Array<any> = [];

  managebanners: Managebanner[];
  managebanner = new Managebanner();
  loggedInUserId: string;
  countryCodesNew: any = (countryCodeData as any).default;
  countrycode = this.countryCodesNew;
  constructor(private toastr: ToastrService, private managebannerservice: Managebannerservice, private _userService: UserService,
    private _commonService: CommonService, private paymentPlanService: Paymentplanservice,
    private _http: HttpClient, private _router: Router, private _formbulider: FormBuilder, private route: ActivatedRoute, @Inject(DOCUMENT) document) {
  }


  gettoday() {

    this.maxDate = new Date().toJSON().split('T')[0];
  }

  // 
  public uploadFinishedLogo = (event) => {
    this.logoresponse = event;
  }



  ngOnInit()
  {
    this.VerticalView = true;
    this.loggedInUserId = localStorage.getItem('userId');
    this.FillCountryDDL();
    this.FillUserDDL();
    this.gettoday();
    debugger;
    this.FormPostBanner = this._formbulider.group({
      bannerId: ['', ''],
      title: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      faxNumber: [''],
      addressStreet1: ['', ''],
      addressStreet2: ['', ''],
      countryId: ['1', ''],
      stateId: [this.StateId, ''],
      cityId: [this.CityId, ''],
      cityName: [this.CityName, ''],
      stateName: [this.StateName, ''],
      countryCodeContact: ['1', ''],
      validTill: ['', ''],
      zipCode: ['', Validators.pattern("^[0-9]*$")],
      postedFor: ['', Validators.required],
      postedName: ['null', ''],
      isFeatured:[''],
      contactPersonName: ['', Validators.required],
      contactNumber: ['', [Validators.required, Validators.minLength(10)]],
      website: ['', ''],
      description: ['', ''],
      imageUrl: [],
      alternateContactNumber: [''],

      createdBy: ['', ''],
      bannerImageCount: ['']
    });
  }

  // Get User Details on Posted For selection
  GetUserDetails(event: any) {

    debugger;
    this._userService.getUserById(event.userId).subscribe(data => {
      var userdata = JSON.parse(data._body);
      this._commonService.CityDDL(userdata.stateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });

      //this._allPlan = this.paymentPlanService.UserPlanDDL(user)
      this.FillSubscriptionPlan(userdata.userId);
      //this._allState = this._commonService.StateDDL(userdata.countryId);
      //this._allCity = this._commonService.CityDDL(userdata.stateId);
      debugger;
      this.FormPostBanner.patchValue({

        bannerId: "",
        title: "",
        email: userdata.email,
        faxNumber: userdata.faxNumber,
        addressStreet1: userdata.addressStreet1,
        addressStreet2: userdata.addressStreet2,
        countryId: userdata.countryId,
        stateId: userdata.stateId.toString(),
        cityId: userdata.cityId.toString(),
        validTill: "",
        zipCode: userdata.zipCode,
        postedFor: userdata.userId.toString(),
        postedName: "",
        contactPersonName: userdata.firstName + " " + userdata.lastName,
        contactNumber: userdata.contactNumber,
        countryCodeContact: "1",
        website: "",
        description: "",
        imageUrl: "null",
        alternateContactNumber: userdata.alternateContactNumber,
        createdBy: this.loggedInUserId,
        bannerImageCount: 0
      });
    });
  }

  async delay(ms: number) {
    await new Promise(resolve => setTimeout(() => resolve(), ms)).then(() => console.log("fired"));
  }

  // Insert Event
  saveBanner(): void {
    debugger;
    this.submitted = true;
    this.FormPostBanner.value.imageUrl = this.logoresponse;

    // stop the process here if form is invalid

    if (this.FormPostBanner.invalid) {
      this.toastr.warning("Mandatory fields missing. Please fill all mandatory fields.");
      return;
    }

    this.FormPostBanner.value.isFeatured = this.IsFeatured;

    console.log(this.FormPostBanner.value);
    if (this.FormPostBanner.value.imageUrl == null || this.FormPostBanner.value.imageUrl == undefined) {
      this.toastr.warning("Banner image missing. Please upload an image.");
      return;
    }

    if ((this.FormPostBanner.value.imageUrl != null) && (this.remainingBannerImage == 0)) {
      this.toastr.warning('Banner subscription exausted.');
      return;
    }

    if (this.FormPostBanner.value.imageUrl != null && this.previousPlan == undefined) {
      this.toastr.warning("Please select/buy a subscription plan to post a banner ad.");
    }



    // this.FormPostBanner.value.imageUrl = this.logoresponse;
    //this.FormPostBanner.value.bannerImageCount = this.previousPlan.bannerImageCount;

    this.FormPostBanner.value.createdBy = this.loggedInUserId;
    this.FormPostBanner.value.bannerId = 0;
    console.log(this.FormPostBanner.value);


    this.managebanner.AddressStreet1 = this.FormPostBanner.value.addressStreet1;
    this.managebanner.AddressStreet2 = this.FormPostBanner.value.addressStreet2;
    this.managebanner.AlternateContactNumber = this.FormPostBanner.value.alternateContactNumber;
    this.managebanner.bannerId = this.FormPostBanner.value.bannerId;
    this.managebanner.CityId = this.FormPostBanner.value.cityId;
    this.managebanner.ContactNumber = this.FormPostBanner.value.contactNumber;
    this.managebanner.ContactPersonName = this.FormPostBanner.value.contactPersonName;
    this.managebanner.CountryCodeContact = this.FormPostBanner.value.countryCodeContact;
    this.managebanner.CountryId = this.FormPostBanner.value.countryId;
    this.managebanner.CreatedBy = this.FormPostBanner.value.createdBy;
    this.managebanner.Description = this.FormPostBanner.value.description;
    this.managebanner.Email = this.FormPostBanner.value.email;
    this.managebanner.FaxNumber = this.FormPostBanner.value.faxNumber;
    this.managebanner.ImageUrl = this.FormPostBanner.value.imageUrl;
    this.managebanner.PostedFor = this.FormPostBanner.value.postedFor;
    this.managebanner.PostedForName = this.FormPostBanner.value.postedName;
    this.managebanner.StateId = this.FormPostBanner.value.stateId;
    this.managebanner.Title = this.FormPostBanner.value.title;
    this.managebanner.ValidTillDate = this.FormPostBanner.value.validTill;
    this.managebanner.Website = this.FormPostBanner.value.website;
    this.managebanner.ZipCode = this.FormPostBanner.value.zipCode;
    this.managebanner.IsFeatured = this.FormPostBanner.value.isFeatured;


    if (this.FormPostBanner.value.imageUrl != null) {
      this.managebanner.BannerImageCount = this.FormPostBanner.value.bannerImageCount;
      this.managebanner.PlanId = this.previousPlan.planId;
    }
    this.managebannerservice.add(this.managebanner)
      .subscribe((response) => {
        console.log(response);
        this.toastr.success('Banner posted successfully.');
        this._router.navigate(['/theme/managebanner']);

      },

        (error) => {
          console.log(error);
          this.toastr.error('Problem with service. Please try again later!');
        }
      );
  }

  onextLogoUrldelete(i) {
    this.extLogoUrls.splice(i, 1);
    this.extLogos.pop();
  }

  subscriptionPlanDetails(event: any) {
    debugger;
    console.log(event.target.value);

    this.paymentPlanService.getPlanDetails(event.target.value).subscribe(data => {
      var planIdDetails = JSON.parse(data._body);
      console.log(planIdDetails);
      this.previousPlan = planIdDetails;
      this.remainingBannerImage = planIdDetails.bannerImageCount;
      if (this.remainingBannerImage == 0) {
        this.toastr.warning('Banner subscription exausted. Please buy a new subscription or select other subscription plan.');
      }
      this.FormPostBanner.patchValue({
        bannerImageCount: planIdDetails.bannerImageCount
      });
    });
  }

  //Dropdowns
  FillCountryDDL() {
    this._allCountry = this._commonService.CountryDDL();
    this._allState = this._commonService.StateDDL(this.CountryId);
    this._allCity = this._commonService.CityDDL(this.StateId);
    this._commonService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      this.states = data;
    });
  }

  FillUserDDL() {
    debugger;
    this._allUser = this._commonService.UserDDL();
    this._commonService.UserInboxMessageDDL().subscribe(data => {
      debugger;
      this.users = data;


    });

  }

  FillStateDDL(event: any) {
    debugger;
    debugger;
    if ((event.target.value == "0") || ((event.target.value == ""))) {
      this.FormPostBanner.value.countryId = 0;
      //this._allState = this._commonService.StateDDL("0");
      var x = document.getElementsByClassName("ng-value-label");
      x[2].innerHTML = "--Select State--";
      x[3].innerHTML = "--Select City--";
      //var i;
      //for (i = 3; i < x.length; i++) {
      //  x[i].innerHTML = "";
      //}
      this.states = [];
      this.cities = [];
      //this.FillCityDDL(0);
      console.log(this.states);
      this.FormPostBanner.value.stateId = 0;
      //this._allCity = this._commonService.CityDDL("0");
      this.FormPostBanner.value.cityId = 0;
    }
    else {
      this.CountryId = event.countryId;

      this._allState = this._commonService.StateDDL("1");
      debugger;
      this._commonService.StateDDL("1").subscribe(data => {
        console.log(data);
        //debugger;
        this.states = data;
      });

      this._commonService.CityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
      this._allState = this._commonService.StateDDL(this.CountryId);
      //  this.CountryName = event.target.options[this.CountryId].innerHTML;
      // this.FormPostAd.countryName = this.CountryName;
    }

  }


  FillCityDDL(event: any) {
    //debugger;
    if (event != 0) {
      var x = document.getElementsByClassName("ng-value-label");
      //x[1].innerHTML = "--Select State--";
      x[2].innerHTML = "--Select City--";
      this.StateId = event.stateId;
      this.StateName = event.stateName;
      //this.StateName=event.target.options[this.StateId].innerHTML;
      this._allCity = this._commonService.CityDDL(this.StateId);
      this._commonService.CityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
    }
    else {
      var x = document.getElementsByClassName("ng-value-label");
      this.cities = [];
    }

  }

  FillCityName(event: any) {
    debugger;
    this.CityId = event.cityId;
    this.CityName = event.cityName;
    this.FormPostBanner.value.cityName = this.CityName;
  }

  FillSubscriptionPlan(userId: number) {
    this._allPlan = this.paymentPlanService.UserPlanDDL(userId);
  }

  NavigatetoManageBanner() {
    this._router.navigate(['/theme/managebanner']);
  }

  reset() {
    this.ngOnInit();
    this.UploadCmpt.onimagedelete(0);

  }

  refresh(): void {
    debugger;
    window.location.reload();
    this.UploadCmpt.ngOnInit();

  }

  NavigatetoManageAd() {
    this._router.navigate(['/theme/managebanner']);
  }

  get f() { return this.FormPostBanner.controls; }

  WebsiteLocation(event: any) {
    debugger;
    if (event.target.value == "Horizontal") {
      this.IsFeatured = true;
    }
    else if (event.target.value == "Vertical") {
      this.IsFeatured = false;
    }

  }


}


