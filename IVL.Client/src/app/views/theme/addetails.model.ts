import { Observable } from 'rxjs';

/**
* Created by Tareq Boulakjar. from angulartypescript.com
*/
export class AdDetails {

  public adId: number;
  public UpdatedBy: string;
  public Title: string;
  public TagLine: string;
  public Description: string;
  public ServicesOffered: string;
  public CategoryId: string;
  public CategoryName: string;
  public SubCategoryId: string;
  public SubCategoryName: string;
  public ContactPersonName: string;
  public CountryCode: string;
  public ValidFromDate: string;
  public ValidTillDate: string;
  public ContactNumber: string;
  public AlternateContactNumber: string;
  public FaxNumber: string;
  public Email: string;
  public Website: string;
  public AddressStreet1: string;
  public AddressStreet2: string;
  public CountryId: string;
  public StateId: string;
  public CityId: string;
  public CountryName: string;
  public StateName: string;
  public CityName: string;
  public ZipCode: string;
  public IsActive: boolean;
  public IsVisible: boolean;
  public Activity: boolean;
  public PostedFor: bigint;
  public PostedForName: string
  public WorkflowstatusId: string;
  public Remarks: string;
  public AdImageUrl: string;
  public AdLogoUrl: string;
  public CreatedBy: string;
  public AdStatusForDetailsView: string;
  public adImageDetails: AdImageDetails[];
  public PlanId: number;
  public LogoCount: number;
  public AdImageCount: number;
  public OtherCity: string
  constructor() {
  }
}


export class AdList {


  public adId: string;
  public title: string;
  public description: string;
  constructor() {
  }
}

export class WorkflowstatusDTO {

  public workflowStatusId: number;
  public Status: string;
  public IsActive: boolean;
  public CreatedDate: string;
  public CreatedBy: string;
  public updatedDate: string;
  public updatedBy: string;
  constructor() {
  }

}
export class AdImageDetails {

  //public  AdImageDetailId: number;
  public AdImageUrl: string;
  //public AdId: number;
  // public IsActive: boolean;
  // public CreatedDate: string;
  // public CreatedBy:string;
  // public updatedDate:string;
  // public updatedBy:string;
  constructor() {
  }
}
export class AdStatusDetailsDTO {

  public AdStatusDetailsId: number;
  public AdId: number;
  public workflowStatusId: number;
  public Remark: string;
  public Status: string;
  public IsActive: boolean;
  public CreatedDate: string;
  public CreatedBy: string;
  public updatedDate: string;
  public updatedBy: string;
  constructor() {
  }
}
