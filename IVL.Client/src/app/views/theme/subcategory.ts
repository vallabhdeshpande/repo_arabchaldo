export class Subcategory {
    public subCategoryId: string;
    public subCategoryName: string;
    public categoryId: string;
    public categoryName:string;
    public description: string;
    public IsActive: boolean;
    public CreatedDate: string;
    public CreatedBy: string;
    public UpdatedDate: string;
    public UpdatedBy: string;
    public CategoryImageUrl: string;

constructor() { 
    }
}