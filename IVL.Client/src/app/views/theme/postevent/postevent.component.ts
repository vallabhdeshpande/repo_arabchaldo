import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Country,State,City,category,Subcategory,Roles,CountryCodes } from '../../shared/common.model';
import { User } from '../user';
import { UserService } from '../user.service';
import { EventDetails} from '../eventdetails.model';
import { EventDetailsService } from '../eventdetails.service';
import { CommonService } from '../../shared/common.service';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { environment } from '../../../../environments/environment';
import { element } from 'protractor';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Paymentplan } from '../paymentplan';
import { Paymentplanservice } from '../paymentplanservice';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import * as countryCodeData from '../../shared/countrycodes.json';
@Component({
  selector: 'app-postevent',
  templateUrl: './postevent.component.html',
  styleUrls: ['./postevent.component.scss'],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }]
})




export class PosteventComponent implements OnInit {
  public _allUser: Observable<User[]>;
  public _allCountry: Observable<Country[]>;
  public _allCountryCodes: Observable<CountryCodes[]>;
  public _allState: Observable<State[]>;
  public _allCity: Observable<City[]>;
  public _allCategory: Observable<category[]>;

  StateName: string = "";
  CityName: string = "";
  categoryId: string = "0";
  postedFor: string = "0";
  CountryId: string = "0";
  StateId: string = "0";
  CityId: string = "0";
  logopath: string[];
  imagepath: any[];
  logoUrl: string = "";
  imageUrl: string = "";
  ABCD: string[];
  states: any[];
  cities: any[];
  users: any[];
  categories: any[];
  categoryName: string;
  //dvImage: string[];
  FormPostEvent: any;
  actionButton;
  countrycodes: string[];
  action = "";
  listingType: any;
  myData = null;
  formData = null;
  msgs = null;
  public imagefilename: string[];
  public logofilename: string[];
  EventPost: EventDetails[];
  eventPost = new EventDetails();
  statusMessage: string;
  saveButtonText: string = "Save";
  displayText = 'btn btn-sm btn-primary';
  displayExtLogo = 'col-md-6 d-none';
  displayExtImages = 'col-md-6 d-none';
  displayExtUploads = 'form-bg-section d-none';
  maxDate: string;
  validFrom: string = "";
  validTill: string;
  FormSubcategoryAdd: any;
  extLogoUrls = [];
  extLogos = [];
  isPaidAd: boolean = false;
  displayPaidSection = 'col-md-12 d-none';
  //resetButtonStyle:string="block";
  public logoresponse: string[];
  LoggedInUserId: string;
  minDate: string;
  addbutton: HTMLElement;
  serverpath = environment.serverpath;
  commonimagepath = environment.imagepath;
  submitted = false;
  Id: number;
  adTypeFree: boolean = true;
  adTypePaid: boolean = false;
  subscription: any;
  countryCodeList: Array<any> = [];
  minDateModel: NgbDateStruct;
  modelValidFrom: Date = null;
  ValidFromDate: NgbDateStruct;
  public _allPlan: Observable<Paymentplan[]>;
  public previousPlan: Paymentplan;
  remainingeventImage: number = 0;
  RadioFree: boolean = false;
  RadioPaid: boolean = false;
  tempPlanId: number;
  countryCodesNew: any = (countryCodeData as any).default;
  countrycode = this.countryCodesNew;
  constructor(private toastr: ToastrService, private _eventDetailsService: EventDetailsService, private _userService: UserService,
    private paymentPlanService: Paymentplanservice, private _commonService: CommonService,
    private _http: HttpClient, private _router: Router, private _formbulider: FormBuilder, private route: ActivatedRoute, @Inject(DOCUMENT) document, private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private config: NgbDatepickerConfig) {
  }

  public uploadFinishedLogo = (event) => {
    this.logoresponse = event;
  }

  selectToday() {
    this.minDateModel = this.calendar.getToday();

  }

  onvalidFromDateSelect(event: any) {
    debugger;
    this.ValidFromDate = event;
    //this.minValidTillDate = event;
    this.validFrom = this.modelValidFrom.toJSON().split('T')[0];

    //var abc = this.ValidFromDate.toJSON().split('T')[0];

  }

  reset() { }

  ngOnInit() {
    this.LoggedInUserId = localStorage.getItem('userId');
    this.FillCategoryDDL();
    this.FillCountryDDL();
    this.FillUserDDL();
    this.selectToday();

    // this.gettoday();

    debugger;
    this.FormPostEvent = this._formbulider.group({
      eventId: ['', ''],
      title: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      faxNumber: [''],
      addressStreet1: ['', ''],
      addressStreet2: ['', ''],
      countryId: ['1', ''],
      stateId: [this.StateId, ''],
      cityId: [this.CityId, ''],
      cityName: [this.CityName, ''],
      stateName: [this.StateName, ''],
      countryCodeContact: ['1', ''],
      eventDate: [this.validFrom, Validators.required],
      zipCode: ['', Validators.pattern("^[0-9]*$")],
      postedFor: ['', Validators.required],
      postedName: ['null', ''],
      contactPersonName: ['', Validators.required],
      contactNumber: ['', [Validators.required, Validators.minLength(10)]],
      website: ['', ''],
      categoryId: ['', Validators.required],
      description: ['', ''],
      imageUrl: [],
      alternateContactNumber: [''],
      createdBy: ['', ''],
      isPaidAd: [this.isPaidAd, ''],
      eventImageCount: ['']

    });
  }

  // Get User Details on Posted For selection
  GetUserDetails(event: any) {

    debugger;

    this._userService.getUserById(event.userId).subscribe(data => {
      var userdata = JSON.parse(data._body);
      this.FillSubscriptionPlan(userdata.userId);
      this._commonService.CityDDL(userdata.stateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;



      });


      //this._allState = this._commonService.StateDDL(userdata.countryId);
      //  this._allCity = this._commonService.CityDDL(userdata.stateId);
      debugger;
      this.FormPostEvent.patchValue({

        eventId: "",
        title: "",
        email: userdata.email,
        faxNumber: userdata.faxNumber,
        addressStreet1: userdata.addressStreet1,
        addressStreet2: userdata.addressStreet2,
        countryId: userdata.countryId,
        stateId: userdata.stateId.toString(),
        cityId: userdata.cityId.toString(),
        cityName: userdata.cityName,
        stateName: userdata.stateName,
        eventDate: "",
        zipCode: userdata.zipCode,
        postedFor: userdata.userId.toString(),
        postedName: "",
        contactPersonName: userdata.firstName + " " + userdata.lastName,
        contactNumber: userdata.contactNumber,
        countryCodeContact: "1",
        website: "",
        categoryId: "",
        description: "",
        imageUrl: "null",
        alternateContactNumber: userdata.alternateContactNumber,
        createdBy: this.LoggedInUserId,
        eventImageCount: 0
      });


    });

    //var x = document.getElementById('postedFor');
    //x.innerHTML = event.userFullName;
    //// x.setAttribute("class", "ng-arrow-zone");

    // //x.setAttribute= "ng-arrow-zone"    
  }

  // Insert Event
  saveEvent(): void {
    debugger;
    this.submitted = true;
    this.FormPostEvent.value.isPaidAd = this.isPaidAd;
    this.FormPostEvent.value.createdBy = this.LoggedInUserId;

    this.FormPostEvent.value.imageUrl = this.logoresponse;
    // stop the process here if form is invalid

    if (this.FormPostEvent.invalid) {
      this.toastr.warning("Mandatory fields missing. Please fill all mandatory fields.");
      return;
    }

    console.log(this.FormPostEvent.value)
    if (this.isPaidAd == true) {

      if (this.FormPostEvent.value.imageUrl == null || this.FormPostEvent.value.imageUrl == undefined) {
        this.toastr.warning("Event image missing. Please upload an image.");
        return;
      }
      if (this.FormPostEvent.value.imageUrl != null && this.previousPlan == undefined) {
        this.toastr.warning("please select/buy a subscription plan to post a event ad.");
        return;
      }
      if ((this.FormPostEvent.value.imageUrl != null) && (this.remainingeventImage == 0)) {
        this.toastr.warning('Event subscription exausted.');
        return;
      }

    }
    else {
      this.FormPostEvent.value.planId = null;
      this.FormPostEvent.value.imageUrl = null;

    }



    //this.eventPost.eventId = 0;
    this.eventPost.IsPaidad = this.FormPostEvent.value.isPaidAd;
    this.eventPost.ImageUrl = this.FormPostEvent.value.imageUrl;
    this.eventPost.eventId = this.FormPostEvent.value.eventId;
    this.eventPost.Title = this.FormPostEvent.value.title;
    this.eventPost.Email = this.FormPostEvent.value.email;
    this.eventPost.Description = this.FormPostEvent.value.description;
    this.eventPost.Website = this.FormPostEvent.value.website;
    this.eventPost.AddressStreet1 = this.FormPostEvent.value.addressStreet1;
    this.eventPost.AddressStreet2 = this.FormPostEvent.value.addressStreet2;
    this.eventPost.CountryId = this.FormPostEvent.value.countryId;
    this.eventPost.StateId = this.FormPostEvent.value.stateId;
    this.eventPost.CityId = this.FormPostEvent.value.cityId;
    this.eventPost.ZipCode = this.FormPostEvent.value.zipCode;
    this.eventPost.PostedFor = this.FormPostEvent.value.postedFor;
    this.eventPost.PostedForName = this.FormPostEvent.value.postedName;
    //this.eventPost.CityName = this.FormPostEvent.value.cityId;
    this.eventPost.EventDate = this.FormPostEvent.value.eventDate;
    this.eventPost.CategoryId = this.FormPostEvent.value.categoryId;
    this.eventPost.AlternateContactNumber = this.FormPostEvent.value.alternateContactNumber;
    this.eventPost.ContactNumber = this.FormPostEvent.value.contactNumber;
    this.eventPost.ContactPersonName = this.FormPostEvent.value.contactPersonName;
    this.eventPost.FaxNumber = this.FormPostEvent.value.faxNumber;
    this.eventPost.createdBy = this.FormPostEvent.value.createdBy;
    this.eventPost.CountryCodeContact = this.FormPostEvent.value.countryCodeContact;
    if (this.FormPostEvent.value.imageUrl != null) {
      this.eventPost.EventImageCount = this.FormPostEvent.value.eventImageCount;
      this.eventPost.PlanId = this.previousPlan.planId;
    }

    this.FormPostEvent.value.eventId = 0;
    this._eventDetailsService.postEvent(this.eventPost)
      .subscribe((response) => {
        console.log(response);
        this.toastr.success('Event posted successfully.');
        this._router.navigate(['/theme/manageevent']);
      },

        (error) => {
          console.log(error);
          this.toastr.error('Problem with service. Please try again later!');
        }
      );
  }

  onextLogoUrldelete(i) {
    this.extLogoUrls.splice(i, 1);
    this.extLogos.pop();
  }



  NavigatetoManageEvent() {
    this._router.navigate(['/theme/manageevent']);
  }
  subscriptionPlanDetails(event: any) {
    debugger;
    console.log(event.target.value);

    this.paymentPlanService.getPlanDetails(event.target.value).subscribe(data => {
      var planIdDetails = JSON.parse(data._body);
      console.log(planIdDetails);
      this.previousPlan = planIdDetails;
      this.remainingeventImage = planIdDetails.eventImageCount;
      this.FormPostEvent.patchValue({
        eventImageCount: planIdDetails.eventImageCount
      });
    });
  }

  gettoday() {
    debugger;
    var CurrentDate = new Date();
    this.minDate = CurrentDate.toJSON().split('T')[0];
    CurrentDate.setMonth(CurrentDate.getMonth() + 1);
    this.maxDate = CurrentDate.toJSON().split('T')[0];
  }
  //  Dropdowns
  FillCountryDDL() {
    this._allCountry = this._commonService.CountryDDL();
    this._allState = this._commonService.StateDDL(this.CountryId);
    this._allCity = this._commonService.CityDDL(this.StateId);

    this._commonService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      this.states = data;
    });

  }

  FillCategoryName(event: any) {
    this.categoryId = event.CategoryId;
    this.categoryName = event.CategoryName
    this._commonService.CategoryDDL().subscribe(data => {
      debugger;
      this.categories = data;


    });
    //this.categoryName = event.target.options[this.categoryId].innerHTML;
    //alert(this.categoryName);
  }
  //FillCategoryDDL() 
  //{
  //  this._allCategory = this._commonService.CategoryDDL();
  //}


  FillCategoryDDL() {
    debugger;
    this._allCategory = this._commonService.CategoryDDL();
    this._commonService.CategoryDDL().subscribe(data => {
      debugger;
      this.categories = data;


    });

  }

  FillUserDDL() {
    debugger;
    this._allUser = this._commonService.UserDDL();
    this._commonService.UserInboxMessageDDL().subscribe(data => {
      debugger;
      this.users = data;


    });

  }

  FillStateDDL(event: any) {
    debugger;
    if ((event.target.value == "0") || ((event.target.value == ""))) {
      this.FormPostEvent.value.countryId = 0;
      //this._allState = this._commonService.StateDDL("0");
      var x = document.getElementsByClassName("ng-value-label");
      x[3].innerHTML = "--Select State--";
      x[4].innerHTML = "--Select City--";
      //var i;
      //for (i = 3; i < x.length; i++) {
      //  x[i].innerHTML = "";
      //}
      this.states = [];
      this.cities = [];
      //this.FillCityDDL(0);
      console.log(this.states);
      this.FormPostEvent.value.stateId = 0;
      //this._allCity = this._commonService.CityDDL("0");
      this.FormPostEvent.value.cityId = 0;
    }
    else {
      this.CountryId = event.countryId;

      this._allState = this._commonService.StateDDL("1");
      debugger;
      this._commonService.StateDDL("1").subscribe(data => {
        console.log(data);
        //debugger;
        this.states = data;
      });

      this._commonService.CityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
      this._allState = this._commonService.StateDDL(this.CountryId);
      //this.CountryName = event.target.options[this.CountryId].innerHTML;
      //this.FormPostAd.countryName = this.CountryName;
    }

  }

  selected(listingType: string) {
    debugger;
    if (listingType == "Free") {
      this.isPaidAd = false;
      this.displayPaidSection = 'col-md-12 d-none';
    }
    if (listingType == "Paid") {
      this.isPaidAd = true;

      this.displayPaidSection = 'col-md-12';
    }
  }

  FillCityDDL(event: any) {
    if (event != 0) {
      var x = document.getElementsByClassName("ng-value-label");
      x[4].innerHTML = "--Select City--";
      this.StateId = event.stateId;
      this.StateName = event.stateName;
      //this.StateName=event.target.options[this.StateId].innerHTML;
      this._allCity = this._commonService.CityDDL(this.StateId);

      this._commonService.CityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
    }
    else {
      var x = document.getElementsByClassName("ng-value-label");
      this.cities = [];
    }

  }

  FillCityName(event: any) {
    debugger;
    this.CityId = event.cityId;
    this.CityName = event.cityName;
    this.FormPostEvent.value.cityName = this.CityName;
  }

  NavigatetoManageAd() {
    this._router.navigate(['/theme/manageevent']);
  }

  FillSubscriptionPlan(userId: number) {
    this._allPlan = this.paymentPlanService.UserPlanDDL(userId);
  }

  get f() { return this.FormPostEvent.controls; }
}
