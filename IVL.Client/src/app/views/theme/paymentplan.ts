export class Paymentplan 
{
  public planId: number;
  public PlanName: string;
  public UserId: number;
  public adImageCount: number;
  public AdImagePrice: number;
  public bannerImageCount: number;
  public BannerImagePrice: number;
  public eventImageCount: number;
  public EventImagePrice: number;
  public logoCount: number;
  public LogoPrice: number;
  public JobsAdvCount: number;
  public JobsAdvPrice: number;
  public SmallAdsCount : number;
  public SmallAdsPrice : number;
  public BuySellCount  : number;
  public BuySellPrice  : number;
  public MiscellaneiousTotal: number;
  public Description: string;
  public CreatedBy: number;
  public CreatedDate: Date;
  public UpdatedBy: number;
  public UpdatedDate: Date;
  public IsActive: boolean;
  public isPaid: boolean;
  public IsVisible: boolean;
  public ValidFrom: Date;
  public ValidTill: Date;
  public Amount: number;
  public Discount: number;
  public FinalAmount: number;
  public TokenId: string;
  public transactionId: number;
  //public planId: number;
  public paymentMode: string;
  public stripeTokenId: string;
  public receiptId: string;
  public TransactionDate: Date;
  //public Amount: number;
  public stripeName: string;
  //public userId :number ; 
  public stripeBrand: string;
  public stripeCountry: string;
  public stripecvcCheck: string;
  public stripeFunding: string;
  public stripeClientIp: string;
  public stripeCreated: string;
  public stripeEmail: string;
  public stripeType: string; 
}

export class PaymentplanList
{
  public PlanId: number;
  public PlanName: string;
  public UserId: number;
  public FirstName: string;
  public LastName: string;
  public Email: string;
  public ValidFrom: Date;
  public ValidTill: Date;
  public ValidFromConverted: string;
  public ValidTillConverted: string;
}
