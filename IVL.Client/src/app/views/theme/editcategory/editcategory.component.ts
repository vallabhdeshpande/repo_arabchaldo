import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { Router, ActivatedRoute } from "@angular/router";
import { HttpClient, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { first } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {} from '../category';
import { category } from '../../shared/common.model';
import { environment } from '../../../../environments/environment';
import { ToastrService } from 'ngx-toastr'; 
import { OrderPipe } from 'ngx-order-pipe';
import { Response } from '@angular/http';
@Component({
  selector: 'app-editcategory',
  templateUrl: './editcategory.component.html',
  styleUrls: ['./editcategory.component.scss']
})
export class EditcategoryComponent implements OnInit {

  users: User[];
  categoryId: string = "0";
  //user = new User();;
  editCategoryForm: any;
  submitted = false;
  user: User;
  isLoadingResults = false;
  categoryimage: string;
  reverse: boolean;
  order: string;
  categories: category[]; 
  category = new category();
  filter: string = "";
  // imagepath: any[];
  imagepath=environment.imagepath;
  public imageresponse:string[];
  public extImageresponse:string[];
  displayExtLogo = 'col-md-6 d-none';
  displayNewLogo = 'col-md-6';
  displayExtUploads='form-bg-section d-none';
  extLogoUrls=[];
  extLogos=[];
  logoresponse=[];
  public pageSize: number = 10;
  public p: number;
  sortedCollection: any[];
  constructor(private toastr: ToastrService,private orderPipe: OrderPipe,private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private activatedRoute: ActivatedRoute,
    private UserService: UserService) { 
      this.sortedCollection = orderPipe.transform(this.users, 'info.firstName');
    console.log(this.sortedCollection);
    }

  getCategoryList(): void {
    //this.UserService.getCategoryList().subscribe(users => {
      this.UserService.FillCategoryList().subscribe(users => {
      this.users = users.json();
      console.log(this.users);

    });
  }

  public uploadFinishedLogo = (event) => 
  {
    debugger;
    this.imageresponse=event; 
   }

  ngOnInit() {
    this.getCategoryList();
    this.editCategoryForm = this.formBuilder.group({
      // 'categoryId': ['', Validators.required],
      // 'categoryName': ['', Validators.required],
      // 'description': ['', Validators.required],
      isActive: ['', Validators.required],
      categoryId:[],
      categoryName: ['', Validators.required],
      arabicName:['', Validators.required],
      description: [''],
      categoryImageUrl: []
      //'createdBy' : ['', Validators.required]  
    });
    this.getCategorybyID(this.route.snapshot.queryParams['categoryId']);    
  }
  getCategorybyID(categoryId) {
    debugger
    this.UserService.getCategoryById(categoryId).subscribe(data => {
      var data = JSON.parse(data._body);
      console.log(data);
      this.editCategoryForm.setValue({
        categoryId: categoryId,
        categoryName: data.categoryName,
        arabicName:data.arabicName,
        description: data.description,
        isActive: data.isActive,
        categoryImageUrl: data.categoryImageUrl
        // 'createdBy' : ['', Validators.required] 

      });
      // Ad Logo Retrieval
    this.displayExtUploads='form-bg-section';
    if(data.categoryImageUrl !=null)
    {
      this.renderExistingImages(data.categoryImageUrl,categoryId,"Category");
    }       
    else
    {
      this.displayExtLogo = 'col-md-6 d-none'; 
      this.displayExtUploads='form-bg-section d-none';
    }    
    });
    
  }
  onEditSubmit() 
  {

    debugger;
    this.submitted = true;
   
    //stop here if form is invalid
    if (this.editCategoryForm.invalid) {
      return;
    }


    console.log(this.editCategoryForm.value);
   
    debugger
    //if (this.editCategoryForm.value.CategoryImageUrl == null || this.editCategoryForm.value.CategoryImageUrl == undefined)
    //{
    //  this.toastr.warning("Category Image missing. Please upload an image.");
    //  return;
    //}
    if (this.imageresponse != undefined)
    {
      this.editCategoryForm.value.categoryImageUrl = this.imageresponse;
    }

    if(this.extLogoUrls.length==0 && this.imageresponse==undefined)
    {
      this.toastr.warning("Category image missing. Please upload an image.");
      return;
    }

    this.category.CategoryName= this.editCategoryForm.value.categoryName;
    this.category.ArabicName= this.editCategoryForm.value.arabicName;
    this.category.CategoryDescription=this.editCategoryForm.value.description;   
    this.category.CategoryImageUrl = this.editCategoryForm.value.categoryImageUrl;   

    // if(this.imageresponse!= null)
    // {
    //   this.editCategoryForm.value.imageUrl = this.imageresponse;
    // }
    // else if(this.extLogos.length>0)   
    // {    
    //     this.extImageresponse=this.extLogos.slice();
    //     this.editCategoryForm.value.imageUrl = this.extImageresponse;
    // }
    // else{
    //   this.editCategoryForm.value.imageUrl="";
    //   this.category.CategoryImageUrl=this.editCategoryForm.value.imageUrl;
    //   this.editCategoryForm.value.CategoryImageUrl=this.editCategoryForm.value.imageUrl;
    // }
    
    console.log(this.editCategoryForm.value);
    this.UserService.updateCategory(this.editCategoryForm.value)
      .pipe(first())
      .subscribe(
      data => {
        if (data.status === 200)
        {
          if ((<any>data)._body === "true")
            this.toastr.success('Category updated successfully.');
          else
            this.toastr.info("Category already exist");
          this.router.navigate(['/theme/managecategory']);
        }
        else
        {
          this.toastr.info("Issue with the service. please try again");
        }
        },
        error => {
          this.toastr.error(error);
        });
  }

  OnCancel()
  {
    this.router.navigate(['/theme/managecategory']);
  }

    // Edit Image Section

    // onextLogoUrldelete(i)
    // {
    //   this.extLogoUrls.splice(i,1);
    //   this.extLogos.pop();
    
    // }

    onextLogoUrldelete(i)
    { 
      this.extLogoUrls.splice(i,1);
      this.displayExtLogo = 'col-md-6 d-none';
      this.displayNewLogo='col-md-6';
      this.displayExtUploads='form-bg-section d-none';
    }

  // Images Rendering from database

    renderExistingImages(imageUrls,categoryId,uploadType)
    {
      debugger;
      // if((imageUrls!=="") || (imageUrls ==undefined))
      if (imageUrls !== "") {
      
          var extImages = imageUrls.split(',');
          if(extImages.length>0)
          {
            if(uploadType=="Category")
            {
            for (let i = 0; i < extImages.length; i++) 
            {    
              this.extLogoUrls.push(this.imagepath+'Category/'+categoryId+'/Image/'+extImages[i]);
              this.extLogos.push(extImages[i]);
              this.displayExtLogo = 'col-md-6';
              this.displayNewLogo='col-md-6 d-none';
            }
          }
          
    }
}
}
}
