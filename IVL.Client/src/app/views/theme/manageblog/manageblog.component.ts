import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Manageblog} from '../manageblog';
import { ManageblogService } from '../manageblog.service';
import {FormBuilder,Validators} from '@angular/forms';
import { User } from '../user'; 
import { Observable } from 'rxjs/Observable'; 
import {AdblogComponent} from '../adblog/adblog.component';
import { environment } from '../../../../environments/environment';
import { OrderPipe } from 'ngx-order-pipe';
import { ToastrService } from 'ngx-toastr';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-manageblog',
  templateUrl: './manageblog.component.html',
  styleUrls: ['./manageblog.component.scss'],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }]
})
export class ManageblogComponent implements OnInit {
  public _allUser: Observable<User[]>;
  UserId: string = "0";
  charLength: string = "0";
  manageblogs: Manageblog[];
  statusMessage: string;
  manageblog = new Manageblog(); order: string;
  reverse: boolean;
  users: User[];
  setStatus: string = "";
  user = new User();;
  public pageSize: number = 10;
  public p: number;
  blogId: bigint;
  public response: { 'dbPath': '' };
  FormUserAdd: any;
  remark: string = "";
  workflowstatusId: number;
  action: string = "";
  sortedCollection: any[];
  id: number;
  workflowdetails: string;
  deleteremark: string;
  imagepath = environment.imagepath;
  activity: boolean;
  setStatusActive: string = "";
  setStatusInactive: string = "";
  visibilitybutton: string;
  visibilityspan: string;
  loggedInUserId: string;
  validTill: string;
  validFrom: string;
  maxDate: string;
  minDate: string;
  //minValidTillDate: string;
  //maxValidFromDate: string;
  modalexit: string = "";
  LoggedInUserId: string;
  showDates: string = "col-md-6";
  approveButtonText: string;
  approveButtonStyle: string;
  actionMessage: string;

  remarkstring: string[];
  poststring: string;
  remarkstring1: string;
  remarkstring2: string;
  remarkstring3: string;
  filter: string = "";
  minDateModel: NgbDateStruct;
  minValidTillDate: NgbDateStruct;
  maxValidFromDate: NgbDateStruct;

  date: { year: number, month: number };
  hoveredDate: NgbDate;
  modelValidFrom: Date = null;
  modelValidTill: Date = null;

  selectToday() {
    this.minDateModel = this.calendar.getToday();
    this.minValidTillDate = this.calendar.getToday();
  }
  onvalidFromDateSelect(event: any) {
    this.minValidTillDate = event;
    this.validFrom = this.modelValidFrom.toJSON().split('T')[0];

  }
  onvalidTillDateSelect(event: any) {
    this.maxValidFromDate = event;
    this.validTill = this.modelValidTill.toJSON().split('T')[0];

  }
  //gettoday() {
  //  debugger;
  //  var CurrentDate = new Date();
  //  this.minDate = CurrentDate.toJSON().split('T')[0];
  //  CurrentDate.setMonth(CurrentDate.getMonth() + 1);
  //  this.maxDate = CurrentDate.toJSON().split('T')[0];
  //}
  //onvalidFromDateSelect(event: any) {
  //  debugger;
  //  this.minValidTillDate = event.target.value;

  //}
  //onvalidTillDateSelect(event: any) {
  //  this.maxValidFromDate = event.target.value;

  //}

  ngAfterViewInit() {
    document.getElementById("#modalvalidFrom").setAttribute("min", this.minDate);
    document.getElementById("#modalvalidTill").setAttribute("min", this.minDate);


  }
  public uploadFinished = (event) => {

    this.response = event;
  }

  gotoBlogDetails(blogPost: Manageblog): void {
    debugger
    this.router.navigate(['/theme/blogdetailsview'], { queryParams: { id: blogPost.blogId } });
  };


  Adblog(): void {

    this.router.navigate(['/theme/adblog']);
  }

  updateBlog(manageblog: Manageblog): void {
    debugger
    this.router.navigate(['/theme/adblog'], { queryParams: { blogId: manageblog.blogId } });
  }

  constructor(private toastr: ToastrService, private orderPipe: OrderPipe, private manageblogService: ManageblogService, private formbulider: FormBuilder, private router: Router, private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private config: NgbDatepickerConfig) {
    this.sortedCollection = orderPipe.transform(this.users, 'firstName');
    console.log(this.sortedCollection);
  }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }


  getStatus(Status: string) {  

    if(Status=="Active")
      return this.setStatus="badge badge-success";

        if(Status=="InActive")
        return this.setStatus="badge badge-danger";

        if(Status=="Pending")
          return  this.setStatus="badge badge-warning";    

  }
  hideActivity(Status: string) {

    if (Status == "Active")
      return "d-none";
  
    if (Status == "InActive")
      return "d-none";
  
    if (Status == "Pending")
      return "";
  
  } 
  getBlogList(): void {
    debugger
    // this.managebanner.IsActive= "Active";
    this.manageblogService.getBlogList().subscribe(manageblogs => {
      this.manageblogs = manageblogs.body;      
      console.log(this.manageblogs);

      if (manageblogs.body != null) {
        if (manageblogs.body.length > 0) {
          this.manageblogs = manageblogs.body;
        }
        else
          this.manageblogs = [];
      }
      else
        this.manageblogs = [];
    });
  }


  getPicUrl(picurl: string, blogId) {
    if (picurl == null)
      return "assets/img/default/no_image_placeholder.jpg";
    else if (picurl == "")
      return "assets/img/default/no_image_placeholder.jpg";
    else
      return this.imagepath + 'Blogs/' + blogId + '/Image/' + picurl;
  }

  ngOnInit() {
    //this.gettoday();
    this.charLength = "0/200";
    this.selectToday();
    this.getBlogList();
    this.loggedInUserId = localStorage.getItem('userId');
  }

  onDelete(BlogId: bigint) {
    this.blogId = BlogId;
    this.actionMessage = 'Are you sure to delete this Blog?';
  }

  ActionDelete() {
    this.action = "Delete";
    this.deleteremark = "";
    this.workflowdetails = this.blogId + "," + this.loggedInUserId + "," + this.action + "," + this.deleteremark;
    this.manageblogService.updatewithWorkflow(this.workflowdetails).
      subscribe(response => {
        console.log(response);
        this.getBlogList();

        if (response.status === 200) {
          console.log(response);
          if ((<any>response)._body === "true")
            this.toastr.success('Blog successfully deleted.');
          else
            this.toastr.info('Blog is active on website.');
          debugger

        }
        else {
          this.toastr.warning('Problem with service. Please try again later!');
        }

      },

        (error) => {
          console.log(error);

          this.toastr.error('Problem with service. Please try again later!');
        });

  }

  ActionOnApprove(BlogId, event: any) {
    this.remark = null;
    this.validFrom = null;
    this.validTill = null;
    this.modelValidFrom = null;
    this.modelValidTill = null;
    this.maxValidFromDate = null;
    this.minValidTillDate = this.calendar.getToday();
    this.showDates = "col-md-6";
    this.blogId = BlogId;
    this.action = "Approve";
    this.approveButtonText = "";
    this.approveButtonText = "Approve";
    this.approveButtonStyle = "";
    this.approveButtonStyle = "btn btn-sm btn-success";
    this.actionMessage = "Are you sure to approve this record?";
  }

  ActionOnReject(BlogId, event: any) {
    this.remark = null;
    this.validFrom = null;
    this.validTill = null;
    this.modelValidFrom = null;
    this.modelValidTill = null;
    this.maxValidFromDate = null;
    this.minValidTillDate = this.calendar.getToday();
    this.showDates = "col-md-6 d-none";
    this.blogId = BlogId;
    this.action = "Reject";
    this.approveButtonText = "";
    this.approveButtonText = "Reject";
    this.approveButtonStyle = "";
    this.approveButtonStyle = "btn btn-sm btn-warning";
    this.actionMessage = "Are you sure to Reject this record?";
  }


  ActionOnPost(remark) {
    debugger;
    console.log(this.approveButtonText);

    this.poststring = remark;

    this.remarkstring = this.poststring.split(',');
    this.remarkstring1 = this.remarkstring[0];
    this.remarkstring2 = this.remarkstring[1];
    this.remarkstring3 = this.remarkstring[2];

    if ((this.approveButtonText == "Approve") && ((this.remarkstring1 == "null") || (this.remarkstring2 == "null") || (this.remarkstring3 == "null"))) {
      this.toastr.warning("All fields are required to approve");
      return;
    }

    if ((this.approveButtonText == "Reject") && (this.remarkstring3 == "null")) {
      this.toastr.warning("Remark is required to reject");
      return;
    }

    this.workflowdetails = this.blogId + "," + this.loggedInUserId + "," + this.action + "," + remark;


    // if (confirm(this.actionMessage)) {
    this.manageblogService.updatewithWorkflow(this.workflowdetails).subscribe(response => {
      console.log(response);
      console.log('Updated');
      this.modalexit = "modal";
      document.getElementById("closeBtn").click();
      this.getBlogList();
      if (this.action === "Approve")
        this.toastr.success('Blog approved succesfully');
      if (this.action === "Reject")
        this.toastr.warning('Blog rejected');
    },

      (error) => {
        console.log(error);

        this.toastr.error('Problem with service. Please try again later!');
      });
    // }

  }

  blogDetailsView(blogId: number) {
    debugger;
    this.router.navigate(['/theme/blogdetailsview'], { queryParams: { id: blogId } });
  }
}
   
   
