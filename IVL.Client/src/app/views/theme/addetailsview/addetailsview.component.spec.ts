import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddetailsviewComponent } from './addetailsview.component';

describe('AddetailsviewComponent', () => {
  let component: AddetailsviewComponent;
  let fixture: ComponentFixture<AddetailsviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddetailsviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddetailsviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
