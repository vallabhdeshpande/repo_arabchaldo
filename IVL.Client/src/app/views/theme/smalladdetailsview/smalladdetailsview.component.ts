import { Component, OnInit } from '@angular/core';
import { SmallAdDetailsService } from '../smalladdetails.service';
import { CommonService } from '../../shared/common.service';
import { Router, ActivatedRoute } from "@angular/router";
import { SmallAdDetails } from '../smalladdetails.model';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-smalladdetailsview',
  templateUrl: './smalladdetailsview.component.html',
  styleUrls: ['./smalladdetailsview.component.scss']
})
export class SmalladdetailsviewComponent implements OnInit {

  adposts: SmallAdDetails[];
  statusMessage: string;
  logoUrl: string;
  logourls = [];
  imageurls = [];
  workflowDetails = [];
  multiimageurls = [];
  maxDate: string;
  validTill: string;
  adpost = new SmallAdDetails();
  imagepath = environment.imagepath;
  workflow: string;
  showLogo: string = "col-md-4";
  showImages: string = "col-md-8";
  addivshow = true;
  jobdivshow = false;
  Job: string;
  detailsViewName: string = "Advertisement Details";
  detailsViewTitleCss: string = "nav-icon icon-docs";
  showOtherCity: string = "d-none";
  adType: string = "";
  description:string="";
  constructor(private AdDetailsService: SmallAdDetailsService, private _commonService: CommonService, private route: ActivatedRoute, private router: Router, private http: HttpClient) { }

  manageSmallAdsback() {
    
      this.router.navigate(['/theme/managesmallad']);
  }
  ngOnInit() {
    //debugger
    this.getSmallAdDetailsById(this.route.snapshot.queryParams['smalladId']);
    this.gettoday();

    
  }
  gettoday() {

    this.maxDate = new Date().toJSON().split('T')[0];
  }
  getSmallAdDetailsById(smalladId) {
    debugger;

    this.workflow = smalladId + "," + "SmallAd";
    this.AdDetailsService.getSmallAdDetailsById(smalladId).subscribe(adposts => {
      if (adposts.validTillDate !== null)
        this.validTill = adposts.validTillDate.split('T')[0];
      else
        this.validTill = this.maxDate;
      if (adposts.countryCodeContact === null || adposts.countryCodeContact === "1")
        adposts.countryCodeContact = "USA (+1)";
      console.log(adposts);
      smalladId = adposts.smallAdId;

      this.adpost.PostedForName = adposts.postedForName;
     
      this.adpost.ContactPersonName = adposts.contactPersonName;
      this.adpost.Email = adposts.email;
      this.adpost.ContactNumber = adposts.contactNumber;
      this.adpost.AlternateContactNumber = adposts.alternateContactNumber;
      this.adpost.ContactPersonName = adposts.contactPersonName;
      this.adpost.Website = adposts.website;
      this.adpost.FaxNumber = adposts.faxNumber;
     
      this.adpost.Title = adposts.title;
      this.adpost.TagLine = adposts.tagLine;
      this.description = adposts.description;
      this.adpost.AddressStreet1 = adposts.addressStreet1;
      this.adpost.AddressStreet2 = adposts.addressStreet2;
      if (adposts.countryId == 0) {
        this.adpost.CountryName = "";
      }
      else
        this.adpost.CountryName = adposts.countryName;
      this.adpost.StateName = adposts.stateName;
      this.adpost.CityName = adposts.cityName;
      this.adpost.ZipCode = adposts.zipCode;
      if (adposts.validFromDate !== null)
        this.adpost.ValidFromDate = adposts.validFromDate.split('T')[0];
      if (adposts.validTillDate !== null)
        this.adpost.ValidTillDate = adposts.validTillDate.split('T')[0];
      this.adpost.CountryCode = adposts.countryCodeContact;
      this.adpost.SmallAdStatusForDetailsView = this.getStatus(adposts.isVisible);
      if (adposts.cityId === 9999) {
        this.showOtherCity = "col-md-4";
        this.adpost.OtherCity = adposts.otherCity;
      }
      else
        this.showOtherCity = "d-none";

      //if(adposts.isPaidAd==true)
      //{
      if (adposts.adLogoUrl === null) {
        if (adposts.adLogoUrl === "") {
          this.adpost.AdLogoUrl = "";
          this.showLogo = "col-md-4 d-none";
        }
        else
          this.showLogo = "col-md-4 d-none";

      }
      else {
        if (adposts.adLogoUrl !== "") {
          this.adpost.AdLogoUrl = adposts.adLogoUrl;
          this.logoUrl = this.imagepath + 'SmallAds/' + smalladId + '/Logo/' + this.adpost.AdLogoUrl;
          this.logourls.push(this.logoUrl);
          this.showLogo = "col-md-4";
        }
        else
          this.showLogo = "col-md-4 d-none";
      }
      //if(adposts.imageUrl==="")  
      //{
      //  this.adpost.AdImageUrl="";
      //  this.showLogo="col-md-8 d-none";

      //}
      if (adposts.imageUrl === null) {
        if (adposts.imageUrl === "") {
          this.adpost.AdImageUrl = "";
          this.showImages = "col-md-8 d-none";
        }
        else
          this.showImages = "col-md-8 d-none";

      }
      else {
        if (adposts.imageUrl !== "") {
          this.adpost.AdImageUrl = adposts.imageUrl;
          this.multiimageurls = adposts.imageUrl.split(',');
          for (let i = 0; i < this.multiimageurls.length; i++) {
            this.imageurls.push(this.imagepath + 'SmallAds/' + smalladId + '/Images/' + this.multiimageurls[i]);
          }
          this.showImages = "col-md-8";
        }
        else
          this.showImages = "col-md-8 d-none";

        //alert(this.imageurls);
      }
      if (adposts.isPaidAd) 
        this.adType = "Premium";
      else
        this.adType = "Free";
    });
  }





  getStatus(status: boolean) {
    debugger;
    var currentStatus: string = "";
    if (status == true)
      currentStatus = "Active";
    else if (status == false)
      currentStatus = "Inactive";
    else
      currentStatus = "Pending";
    return currentStatus;


  }


}
