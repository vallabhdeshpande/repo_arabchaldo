import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdblogComponent } from './adblog.component';

describe('AdblogComponent', () => {
  let component: AdblogComponent;
  let fixture: ComponentFixture<AdblogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdblogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdblogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
