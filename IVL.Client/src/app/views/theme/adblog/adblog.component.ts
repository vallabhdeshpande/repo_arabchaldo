import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ManageblogService } from '../manageblog.service';
import { Manageblog } from '../manageblog';
import { Observable } from 'rxjs/Observable';
import { User } from '../user';
import { FormBuilder, Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { UserService } from '../user.service';
import { Router, ActivatedRoute } from "@angular/router";
import { CommonService } from '../../shared/common.service';
import { Alert } from 'selenium-webdriver';
import { first } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { ToastrService } from 'ngx-toastr'; 
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { UploadComponent } from '../upload/upload.component';
@Component({
  selector: 'app-adblog',
  templateUrl: './adblog.component.html',
  styleUrls: ['./adblog.component.scss']
})
export class AdblogComponent implements OnInit {
  @ViewChild(UploadComponent, { static: false }) UploadCmpt: UploadComponent;
  blogForm: FormGroup;
  public _allUser: Observable<User[]>;
  public _UserTypeForBlog: Observable<User[]>;
  public _allUserForBlog: Observable<User[]>;
  UserId: string = "0";
  extLogos = [];
  blogId = 0;
  extImages = [];
  public logoresponse: string[];
  public imageresponse: string[];
  RoleId: bigint;
  Id: bigint;
  submitted = false;
  pagedetail: string;
  manageblogs: Manageblog[];
  saveButtonText: string = "Save";
  statusMessage: string;
  manageblog = new Manageblog();
  action = "";
  // managerBlogComponent = new this.managerBlogComponent();
  users: User[];
  loggedInUserId: string;
  displayExtLogo = 'col-md-6 d-none';
  displayNewLogo = 'col-md-6';
  displayExtUploads = 'form-bg-section d-none';
  extLogoUrls = [];
  public extImageresponse: string[];
  displayText = 'btn btn-sm btn-primary';
  user = new User();
  logopath: string[];
  dateString: string;
  public response: { 'dbPath': '' };
  //FormUserAdd: any;
  imagepath = environment.imagepath;
  htmlContent = '';

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['insertImage', 'insertVideo']
    ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };


  //config: AngularEditorConfig = {
  //  editable: true,
  //  spellcheck: true,
  //  height: '15rem',
  //  minHeight: '5rem',
  //  placeholder: 'Enter text here...',
  //  translate: 'no',
  //  defaultParagraphSeparator: 'p',
  //  defaultFontName: 'Arial',
  //  toolbarHiddenButtons: [
  //    ['']
  //  ],
  //  customClasses: [
  //    {
  //      name: "quote",
  //      class: "quote",
  //    },
  //    {
  //      name: 'redText',
  //      class: 'redText'
  //    },
  //    {
  //      name: "titleText",
  //      class: "titleText",
  //      tag: "h1",
  //    },
  //  ]




  //};


  constructor(private route: ActivatedRoute, private _manageblogservice: ManageblogService, private http: HttpClient, private _router: Router, private formbulider: FormBuilder,
    private _commonService: CommonService, private userService: UserService, private toastr: ToastrService, @Inject(DOCUMENT) document) {
    // demo checkin
  }


  ngOnInit() {
    this.loggedInUserId = localStorage.getItem('userId');
    this.blogForm = this.formbulider.group({
      blogId: ['', ''],
      Title: ['', Validators.required],
      //Title: ['', [Validators.required, Validators.pattern(/^\S*$/)]],
      imageUrl: [],
      Description: ['', ''],
      createdBy: ['', '']
      //ValidTill:['', Validators.required]
    });

    this.getBlog(this.route.snapshot.queryParams['blogId']);
  }


  ngAfterViewInit() {
    debugger
    document.getElementById('#Save').innerHTML = this.saveButtonText;

  }


  public uploadFinishedLogo = (blog) => {
    debugger;
    this.imageresponse = blog;
  }

  getBlog(blogId) {
    debugger
    this.action = "edit";
    this.pagedetail = "Add New Blog";
    this._manageblogservice.getBlog(blogId).subscribe(data => {
      var data = JSON.parse(data._body);
      if (data.validTillDate !== null) {
        this.dateString = data.validTillDate;
        var formattedDate = this.dateString.split('T')[0];
      }
      console.log(data);
      // Ad Logo Retrieval
      this.pagedetail = "Edit Blog";
      this.displayExtUploads = 'form-bg-section';
      if (data.imageUrl !== null)
        this.renderExistingImages(data.imageUrl, blogId, "Blog");
      else
        this.displayExtLogo = 'col-md-6 d-none';
      this.saveButtonText = "Update";
      this.blogForm.setValue
        (
        {
          blogId: data.blogId,
          Title: data.title,
          Description: data.description,
          imageUrl: data.imageUrl,
          //createdby:data.createdby,
          createdBy: data.createdBy
          // ValidTill: formattedDate                
        }
        );

    })
  }

  get f() { return this.blogForm.controls; }

  onSubmit(): void {
    debugger;
    this.submitted = true;

    //stop the process here if form is invalid

    if (this.blogForm.invalid) {
      this.toastr.warning("Mandatory fields missing. Please fill all mandatory fields.");
      return;
    }

    this.manageblog.CreatedBy = this.loggedInUserId;
    if (this.saveButtonText == "Save") {
      this.action = "";
    }
    debugger;
    console.log(this.blogForm.value)
    if (this.imageresponse !== undefined)
      this.blogForm.value.imageUrl = this.imageresponse;
    this.manageblog.ImageUrl = this.blogForm.value.imageUrl;
    this.manageblog.Title = this.blogForm.value.Title;
    this.manageblog.Description = this.blogForm.value.Description;
    //this.manageblog.ValidTill = this.blogForm.value.ValidTill;




    console.log(this.manageblog)
    if (this.saveButtonText == "Save")
      this.action == "";
    else
      this.action = "edit";
    if (this.action == "") {
      this.blogForm.value.blogId = "0";
      this.manageblog.blogId = this.blogForm.value.blogId;
      this._manageblogservice.addBlog(this.manageblog)
        .subscribe((response) => {
          debugger
          console.log(response);
          if ((<any>response)._body === "true")
            this.toastr.success('Blog successfully added');
          else
            this.toastr.info('Blog already exist.');
          this._router.navigate(['/theme/manageblog']);
        },
          (error) => {
            console.log(error);

            this.statusMessage = "Problem with service. Please try again later!";
          }
        );
    }

    else if (this.action = "edit") {
      debugger

      console.log(this.blogForm.value);



      //if (this.imageresponse !== null) {
      //  this.blogForm.value.imageUrl = this.imageresponse;
      //}

      //if (this.imageresponse === undefined) {
      //  this.blogForm.value.imageUrl = this.manageblog.ImageUrl;
      //}

      if (this.extLogoUrls.length === 0 && this.imageresponse == undefined) {
        this.blogForm.value.imageUrl = "";
      }


      //if (this.extLogoUrls.length > 0) {
      //  this.blogForm.value.imageUrl = this.imageresponse;
      //}
      this.blogForm.value.blogId = this.route.snapshot.queryParams['blogId'];
      this.manageblog.blogId = this.blogForm.value.blogId;
      debugger;

      // else if(this.imageresponse===undefined||this.imageresponse == null)
      // {
      // debugger
      //if (this.extLogoUrls.length>0)   
      // {    
      //   this.extImageresponse=this.extLogos.slice();
      //   this.blogForm.value.imageUrl = this.extImageresponse[0];
      //}



      //}




      this._manageblogservice.updateBlog(this.blogForm.value)
        .pipe(first())
        .subscribe(
          data => {
            if (data.status === 200) {
              console.log(data);
              debugger
              this.toastr.success('Blog updated successfully.');
              this._router.navigate(['/theme/manageblog']);
              this.getBlogList();
            } else {
              //alert(data.message);
            }
          },
          error => {
            this.toastr.error(error);
          });
    }
  }



  getBlogList(): void {
    // this.manageblog.IsActive= "Active";
    this._manageblogservice.getBlogList().subscribe(manageblogs => {
      this.manageblogs = manageblogs.json();
      console.log(this.manageblogs);
      // alert(this.manageblogs);
    });
  }

  // GetPostedFor(event: any) 
  // {
  //   this.manageblog.PostedFor = event.target.value;
  // }



  onCancel() {
    this._router.navigate(['/theme/manageblog']);
  }

  // Edit Image Section

  onextLogoUrldelete(i) {
    this.extLogoUrls.splice(i, 1);
    this.displayExtLogo = 'col-md-6 d-none';
    this.displayNewLogo = 'col-md-6';
  }

  reset() {
    this.ngOnInit();
    this.UploadCmpt.onimagedelete(0);

  }


  refresh(): void {
    debugger;
    window.location.reload();
    this.UploadCmpt.ngOnInit();

  }
  NavigatetoManageBlog() {
    this._router.navigate(['/theme/manageblog']);
  }

  // Images Rendering from database

  renderExistingImages(imageUrls, blogId, uploadType) {
    debugger;
    if (imageUrls !== "") {
      var extImages = imageUrls.split(',');
      if (extImages.length > 0) {
        if (uploadType == "Blog") {
          for (let i = 0; i < extImages.length; i++) {
            this.extLogoUrls.push(this.imagepath + 'Blogs/' + blogId + '/Image/' + extImages[i]);
            this.extLogos.push(extImages[i]);
            this.displayExtLogo = 'col-md-6';
            this.displayNewLogo = 'col-md-6 d-none';
          }
        }
      }
    }
  }

}



