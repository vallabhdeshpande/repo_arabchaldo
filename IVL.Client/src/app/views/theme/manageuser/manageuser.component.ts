import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { Router } from "@angular/router";
import { first } from "rxjs/operators";
import { OrderPipe } from 'ngx-order-pipe';
import { environment } from '../../../../environments/environment';
declare let $: any;
import { ToastrService } from 'ngx-toastr';
import { IfStmt } from '@angular/compiler';

@Component({
  selector: 'app-manageuser',
  templateUrl: './manageuser.component.html',
  styleUrls: ['./manageuser.component.scss']
})

export class ManageuserComponent implements OnInit
{
  status: string = "";
  users: User[]=[];
  imagepath = environment.imagepath;
  statusMessage: string;
  actionmessage: string;

  public pageSize: number = 10;
  public p: number;
  user = new User();
  order: string;
  reverse: boolean = false;
  sortedCollection: any[];
  userId: number = 0;
  filter: string = "";

  constructor(private toastr: ToastrService, private orderPipe: OrderPipe, private Userservice: UserService, private router: Router) {
    this.sortedCollection = orderPipe.transform(this.users, 'firstName');
    console.log(this.sortedCollection);
  }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }

  getUsersList(): void {
    this.Userservice.getUsersList().subscribe(users => {
      if (users.length>0) {
        this.users = users;
      }
      else {
        this.users = [];
      }
      //this.users = users.json();
      console.log(this.users);
      //debugger;
    });
  }


  getPicUrl(picurl: string, userId) {

    if (picurl == null)
      return "assets/img/default/nouser.jpg";
    else if (picurl == "")
      return "assets/img/default/nouser.jpg";
    else
      return this.imagepath + 'UserProfile/' + userId + '/ProfilePic/' + picurl;

  }

  updateUser(user: User): void {
    console.log(user.userId);
    this.router.navigate(['/theme/adduser'], { queryParams: { userId: user.userId } });
  };

  ResetPassword(user: User): void {
    debugger
    console.log(user.userId);
    this.router.navigate(['/theme/resetpassword'], { queryParams: { userId: user.userId } });
  };

  AddUser() {
    this.router.navigate(['/theme/adduser']);
  };

  //  onDelete(userId: number,firstName:string,lastName:string) {

  //   if (confirm('Are you sure to delete user '+firstName +" "+lastName+'?')) {
  //     this.Userservice.deleteUser(userId).subscribe(response=> {console.log(response);
  //       console.log('Deleted');
  //       this.toastr.error('User Deleted Successfully!');
  //       this.getUsersList(); },

  //     (error) =>{
  //         console.log(error);

  //         this.toastr.error('Problem with service. Please try again later!');
  //     });
  //   }
  // }
  onDelete(userId: number, firstName: string, lastName: string, event) {
    debugger
    this.actionmessage = 'Are you sure to delete user ' + firstName + " " + lastName + '?';
    this.userId = userId;
  }

  ActionOn() {
    this.Userservice.deleteUser(this.userId).subscribe(response => {
      console.log(response);
      console.log('Deleted');
      this.toastr.success('User deleted successfully!');
      this.getUsersList();
    },
      (error) => {
        console.log(error);

        this.toastr.error('Problem with service. Please try again later!');
      });
  }

  ngOnInit() {
    this.getUsersList();

  }

  gotouserDetails(user: User): void {
    debugger
    this.router.navigate(['/theme/userdetailsview'], { queryParams: { userId: user.userId } });
  }
  userDetailsView(userId) {

    this.router.navigate(['/theme/userdetailsview'], { queryParams: { userId: userId } });
  }


}




