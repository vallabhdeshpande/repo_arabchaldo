import { Component, OnInit } from '@angular/core';
import { Country } from '../country.model';
import { Observable } from 'rxjs';
import { StateService } from '../stateservice';
import { State } from '../state.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { first } from 'rxjs/operators';
import { OrderPipe } from 'ngx-order-pipe';
import { __param } from 'tslib';
import { ToastrService } from 'ngx-toastr'; 

//import {NgSelectModule, NgOption} from '@ng-select/ng-select';

@Component({
  selector: 'app-addstate',
  templateUrl: './addstate.component.html',
  styleUrls: ['./addstate.component.scss']
})

export class AddstateComponent implements OnInit {
  public _allstate: Observable<State[]>;
  public _allCountry: Observable<Country[]>;
  CountryId: string = "0";
  CountryName: string = "";
  FormStateAdd:any;
  saveButtonText:string="Save"; 
  AddStateText:string="Add State";
 displayText='btn btn-sm btn-primary';
 countries :any[];
  stateId=0;
  action = "";
  submitted=false;
  StateName: string = "";
  state = new State(); order: string;
  states: State[];
  States = new State();reverse: boolean;
  actionMessage:string;
;
  statusMessage: string;
  public pageSize: number = 10;
  public p: number;
  filter: string = "";


    getStateList(): void {
     // this.StateService.getStateList().subscribe(states => {
        this.StateService.fillStateList().subscribe(states => {
        this.states = states.json();
        console.log(this.states);
  
      });
    }


    sortedCollection: any[];

    constructor(private toastr: ToastrService,private OrderPipe: OrderPipe, private StateService: StateService, private router: Router, private formbulider: FormBuilder) {
      this.sortedCollection = OrderPipe.transform(this.states, 'stateName');
      console.log(this.sortedCollection);
    }
  

    setOrder(value: string) {
      if (this.order === value) {
        this.reverse = !this.reverse;
      }
  
      this.order = value;
    }
  
ngAfterViewInit(){
document.getElementById('#SaveAd').innerHTML = this.saveButtonText; 
document.getElementById('#AddState').innerHTML = this.AddStateText; 

} 
 

 
    addState(): void {
      debugger

      this.submitted = true;

      // stop the process here if form is invalid
      if (this.FormStateAdd.invalid) {
        debugger
        return;
      }


      if (this.action == "") {
  debugger
  this.FormStateAdd.value.stateId=0;
    
      
      console.log(this.FormStateAdd.value)
      this.StateService.addState(this.FormStateAdd.value)
        .subscribe((response) => {
          console.log(response);
  

          if (response.status === 200) {
          console.log(response);
          if((<any>response)._body==="true")
          this.toastr.success('State Inserted successfully.');
            else
            this.toastr.warning('State Already exist.');
            debugger
            //this.router.navigate(['/theme/addstate']);
            this.ngOnInit();
            this.submitted=false;


        
      
            this.getStateList();
          } else {
            this.statusMessage = "Problem with service. Please try again later!";
          }
        },
        );
    }
    else if (this.action = "edit") {this.StateService.updateState(this.FormStateAdd.value)
      .pipe(first())
      .subscribe(
        data => {
          if (data.status === 200) {
            console.log(data);
            if ((<any>data)._body === "true")
              this.toastr.success('State updated successfully.');
            else
              this.toastr.warning('State already exist.');
            this.ngOnInit();
            this.submitted=false;
          }
        },
        
        error => {
          this.toastr.error(error);
        });
  }
  
  this.getStateList();
}
    onDelete(stateId: number,stateName,event) {
      this.stateId=stateId;
      this.actionMessage='Are you sure to delete State '+stateName+'?';
    }

    ActionDelete()
    {
      debugger;
        this.StateService.deleteState(this.stateId).subscribe(response => {
          console.log(response);

          if((<any>response)._body==="true")
        
          {
           this.toastr.success('State deleted successfully.');
        }
          else
          {
           this.toastr.info('This State is linked with an active City cant be deleted');
          }
          // debugger;
          // this.toastr.success('State deleted successfully.');
          
          this.getStateList();
        },
          (error) => {
            console.log(error);
  
            this.toastr.error('Problem with service. Please try again later!');
          });
    }
    getStateById(stateId) {
  
      this.StateService.getStateById(stateId).subscribe(data => {
        console.log(data);
        //debugger
        this.action = "edit";
        this.saveButtonText="Update"; 
        this.AddStateText="Edit State";
        this.displayText='btn btn-sm btn-primary d-none';

        var data = JSON.parse(data._body);
        console.log(data);
        debugger;
        this.action = "edit";
        data.countryId=1;
        this.FormStateAdd.value.stateId=data.stateId;
//this._allState = this.CityService.StateDDL(data.countryId);
  
  
        this.FormStateAdd.setValue({
          countryId: data.countryId,
          stateId:data.stateId,
          StateName: data.stateName
    
  
        
         // adLogoUrl: data.adLogoUrl,
          //imageUrl: data.imageUrl
        });
      });
    }

  ngOnInit() {
    debugger;
    this.getStateList();
    
    this.FillStateDDL("1");
    this.FillCountryDDL();
    this.saveButtonText = "Save";
    this.action = "";
        this.AddStateText="Add State";
        this.displayText='btn btn-sm btn-primary';
    this.FormStateAdd = this.formbulider.group({
      countryId: ['1', Validators.required],
      stateId: ['', ''],
      StateName: ['', Validators.required]
    });
  }

  // FillCountryDDL() {
  //   this._allCountry = this.StateService.CountryDDL();

  // }

  FillCountryDDL() {
    this._allCountry = this.StateService.CountryDDL();
    this.StateService.CountryDDL().subscribe(data => {
      console.log(data);
      debugger;
      this.countries=data;
    });

  }

  FillStateDDL(event:any) {
    this._allstate = this.StateService.StateDDL();

  }
  NavigatetoAdstate()
  {
    debugger
    this.ngOnInit();
    this.submitted=false;
  }
  get f() { return this.FormStateAdd.controls; }
}
