import { Component, OnInit } from '@angular/core';
import { Managebannerservice } from '../managebannerservice';
import {Router, ActivatedRoute} from "@angular/router";
import {Managebanner} from '../managebanner';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Observable } from 'rxjs';
import { User } from '../user'; 
import { UserService } from '../user.service';

@Component({
  selector: 'app-bannerdetailsview',
  templateUrl: './bannerdetailsview.component.html',
  styleUrls: ['./bannerdetailsview.component.scss']
})
export class BannerdetailsviewComponent implements OnInit {

  //adposts: AdDetails[];
  bannerposts: Managebanner[];
  users: User[]; 
  workflowDetails = [];
  bannerpost = new Managebanner();  
  userDetails = new User();
  statusMessage: string ="";
  Address: string;
  ContactDetails: string;
  FaxNumber: number;
  showOtherCity: string = "d-none";
  bannerForm:any;
  //managebanners: Managebanner[];
  //managebanner = new Managebanner();
  public RoleId: any;
  RoleName:string; 
  logoUrl: string;
  logourls = [];
  imageurls = []; 
  imagepath = environment.imagepath;
  workflow: string;
  type: string = "";
  description: string = "";

  constructor(private managebannerservice: Managebannerservice,private userService: UserService,private route: ActivatedRoute, private router: Router, private http: HttpClient) { }
  //constructor() { }

  // manageAdsback()
  // {
  //  this.router.navigate(['/theme/managebanner']);
  // }
  managebannerback() { this.router.navigate(['/theme/managebanner']); }
  ngOnInit() 
  {
    debugger
    this.getBannerDetailsById(this.route.snapshot.queryParams['id']); 
  }


  getBannerDetailsById(id)
  {
    this.workflow = id + "," + "Banner";
    this.managebannerservice.getBanner(id).subscribe(data => {
      var data = JSON.parse(data._body);

      console.log(data);
      // debugger
      this.bannerpost.ContactPersonName = data.contactPersonName;
      if (data.countryCodeContact == "1") {
        this.bannerpost.ContactNumber = "USA" + "(+" + data.countryCodeContact + ") " + data.contactNumber;
      }
      else if (data.countryCodeContact == "2") {
        this.bannerpost.ContactNumber = "UK" + "(+" + data.countryCodeContact + ") " + data.contactNumber;
      }
      else
        this.bannerpost.ContactNumber = "(+" + data.countryCodeContact + ") " + data.contactNumber;

      if (data.isFeatured) {
        this.type = "Premium";
      }
      else
        this.type = "Featured";

      if (data.isVisible == true)
        this.statusMessage = "Active";
      else if (data.isVisible == false)
        this.statusMessage = "Inactive";
      else if (data.isVisible == null)
        this.statusMessage = "Pending";

      this.bannerpost.AlternateContactNumber = data.alternateContactNumber;
      this.bannerpost.AddressStreet1 = data.addressStreet1;
      this.bannerpost.AddressStreet2 = data.addressStreet2;
      this.bannerpost.StateName = data.stateName;
      this.bannerpost.CityName = data.cityName;
      if (data.cityName == "Other") {
        this.showOtherCity = "col-md-4";
        this.bannerpost.OtherCity = data.otherCity;
      }
      this.bannerpost.ZipCode = data.zipCode;
      if (data.validFromDate !== null)
      this.bannerpost.ValidFromDate=data.validFromDate.split('T')[0];
      if (data.validTillDate !== null)
      this.bannerpost.ValidTillDate=data.validTillDate.split('T')[0];
      //this.bannerpost.ValidTill = data.validTill;
      this.bannerpost.Title = data.title;
      this.bannerpost.Website = data.website;
      this.bannerpost.Description = data.description;   
      this.description = this.bannerpost.Description;   
      this.bannerpost.FaxNumber = data.faxNumber;
      this.bannerpost.Email = data.email;
      if(data.imageUrl != null)
      {
        this.bannerpost.ImageUrl = data.imageUrl;
        this.logoUrl = this.imagepath + 'Banners/' + id + '/Image/' + data.imageUrl;
        this.logourls.push(this.logoUrl);
      }
      
      //debugger

      this.userService.getUserById(data.postedFor).subscribe(detail => {
        var detail = JSON.parse(detail._body);
        //this.userDetails.Email = detail.email;
        //this.userDetails.FaxNumber = detail.faxNumber;
        this.bannerpost.PostedForName = detail.firstName + " " + detail.lastName;
        //this.FaxNumber = detail.faxNumber;

        console.log(detail);

      });
    });
  }   
}
