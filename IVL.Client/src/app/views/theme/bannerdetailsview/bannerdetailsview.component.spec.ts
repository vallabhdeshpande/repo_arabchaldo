import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerdetailsviewComponent } from './bannerdetailsview.component';

describe('BannerdetailsviewComponent', () => {
  let component: BannerdetailsviewComponent;
  let fixture: ComponentFixture<BannerdetailsviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerdetailsviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerdetailsviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
