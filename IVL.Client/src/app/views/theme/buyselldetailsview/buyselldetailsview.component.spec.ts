import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyselldetailsviewComponent } from './buyselldetailsview.component';

describe('BuyselldetailsviewComponent', () => {
  let component: BuyselldetailsviewComponent;
  let fixture: ComponentFixture<BuyselldetailsviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyselldetailsviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyselldetailsviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
