import { Component, OnInit } from '@angular/core';
import { BuysellService } from '../buysell.service';
import { CommonService } from '../../shared/common.service';
import {Router, ActivatedRoute} from "@angular/router";
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Buysell } from '../buysell.model';


@Component({
  selector: 'app-buyselldetailsview',
  templateUrl: './buyselldetailsview.component.html',
  styleUrls: ['./buyselldetailsview.component.scss']
})
export class BuyselldetailsviewComponent implements OnInit {

  buysellposts: Buysell[];
  statusMessage: string;
  ImageUrl: string;
  imageurls = [];
  workflowDetails = [];
  multiimageurls = [];
  maxDate:string;
  validTill:string;
  buysellpost = new Buysell();
  imagepath = environment.imagepath;
  workflow: string;
  showLogo: string = "col-md-4";
  showImages: string = "col-md-8";
  detailsViewName: string = "Buy/Sell Details";
  detailsViewTitleCss: string = "nav-icon icon-docs";
  showOtherCity: string = "d-none";
  AdType:string="";
  description:string="";
  constructor(private _BuysellService: BuysellService, private _commonService: CommonService,private route: ActivatedRoute, private router: Router, private http: HttpClient) { }

 manageAdsback()
 {
   this.router.navigate(['/theme/managebuysell']);
  }
  ngOnInit() {
    //debugger
    this.getbuySellDetailsById(this.route.snapshot.queryParams['buysellId']);  
    this.gettoday();
  }
  gettoday(){ 
 
    this.maxDate =new Date().toJSON().split('T')[0];  
    }
    getbuySellDetailsById(buySellId) {
    debugger ;
    
    this.workflow=buySellId+","+"BuySell";
    this._BuysellService.getbuySellDetailsById(buySellId).subscribe(buysellposts => {
      if(buysellposts.validTillDate!==null)
      this.validTill=buysellposts.validTillDate.split('T')[0];
      else
      this.validTill=this.maxDate;
      if(buysellposts.countryCodeContact===null ||buysellposts.countryCodeContact==="1")
      this.buysellpost.CountryCodeContact="USA (+1)";
      console.log(buysellposts); 
      debugger ;
      this.buysellpost.PostedForName=buysellposts.postedForName;
      this.buysellpost.CategoryName=buysellposts.categoryName;
      
      if (buysellposts.subCategoryName === "null")
        this.buysellpost.SubCategoryName = "";
      else
        this.buysellpost.SubCategoryName = buysellposts.subCategoryName;
      this.buysellpost.ContactPersonName=buysellposts.contactPersonName;
      this.buysellpost.Email=buysellposts.email;
      this.buysellpost.ContactNumber=buysellposts.contactNumber;
      this.buysellpost.AlternateContactNumber=buysellposts.alternateContactNumber;
      this.buysellpost.ContactPersonName=buysellposts.contactPersonName;
      this.buysellpost.Website=buysellposts.website;
      this.buysellpost.FaxNumber=buysellposts.faxNumber;  
      this.buysellpost.Title=buysellposts.title;
      this.description=buysellposts.description;
      this.buysellpost.AddressStreet1=buysellposts.addressStreet1;
      this.buysellpost.AddressStreet2 = buysellposts.addressStreet2;
      this.buysellpost.ExpectedPrice=buysellposts.expectedPrice;
      if(buysellposts.isPaidAd==true)
      this.AdType="Premium"
      else
      this.AdType="Free"
      if (buysellposts.countryId == 0) {
        this.buysellpost.CountryName = "";
      }
      else
      this.buysellpost.CountryName=buysellposts.countryName;
      this.buysellpost.StateName=buysellposts.stateName;
      this.buysellpost.CityName=buysellposts.cityName;  
      this.buysellpost.ZipCode=buysellposts.zipCode;
      if (buysellposts.validFromDate !== null)
      this.buysellpost.ValidFromDate=buysellposts.validFromDate.split('T')[0];
      if (buysellposts.validTillDate !== null)
      this.buysellpost.ValidTillDate=buysellposts.validTillDate.split('T')[0];
    //  this.buysellpost.CountryCodeContact=buysellposts.countryCodeContact;
      this.buysellpost.buySelllStatusForDetailsView = this.getStatus(buysellposts.isVisible);
      if (buysellposts.cityId === 9999) {
        this.showOtherCity = "col-md-4";
        this.buysellpost.OtherCity = buysellposts.otherCity;
      }
      else
        this.showOtherCity = "d-none";
     
    // //if(adposts.isPaidAd==true)
    // //{
    //   if (buysellposts.adLogoUrl === null) {
    //     if (buysellposts.adLogoUrl === "") {
    //       this.buysellpost.ImageUrl = "";
    //       this.showLogo = "col-md-4 d-none";
    //     }
    //     else
    //       this.showLogo = "col-md-4 d-none";

    //   }
    //   else {
    //     if (adposts.adLogoUrl !== "") {
    //       this.adpost.AdLogoUrl = adposts.adLogoUrl;
    //       this.logoUrl = this.imagepath + 'Ads/' + adId + '/Logo/' + this.adpost.AdLogoUrl;
    //       this.logourls.push(this.logoUrl);
    //       this.showLogo = "col-md-4";
    //     }
    //     else
    //       this.showLogo = "col-md-4 d-none";
    //   }
    //   //if(adposts.imageUrl==="")  
    //   //{
    //   //  this.adpost.AdImageUrl="";
    //   //  this.showLogo="col-md-8 d-none";

    //   //}
    //   if (adposts.imageUrl === null) {
    //     if (adposts.imageUrl === "") {
    //       this.adpost.AdImageUrl = "";
    //       this.showImages = "col-md-8 d-none";
    //     }
    //     else
    //       this.showImages = "col-md-8 d-none";

    //   }
      // else {
        if (buysellposts.imageUrl!=="") {
          // this.buysellposts.imageUrl = buysellposts.imageUrl;
          this.multiimageurls = buysellposts.imageUrl.split(',');
          for (let i = 0; i < this.multiimageurls.length; i++) {
            if(buysellposts.isPaidAd==true)
            this.imageurls.push(this.imagepath + 'BuySell/' + buySellId + '/Premium/' + this.multiimageurls[i]);
            else
            this.imageurls.push(this.imagepath + 'BuySell/' + buySellId + '/Free/' + this.multiimageurls[i]);
          }
          this.showImages = "col-md-8";
        }
        else
          this.showImages = "col-md-8 d-none";
        
        //alert(this.imageurls);
    //  }
    
     
    });
  }

  



  getStatus(status: boolean) {
    debugger;
  var currentStatus:string="";
  if(status==true)
  currentStatus="Active";
  else if(status==false)
  currentStatus="Inactive";
 else
  currentStatus="Pending";
return currentStatus;


  }
  

}

