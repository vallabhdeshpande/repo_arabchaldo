/**
 * Created by Tareq Boulakjar. from angulartypescript.com
 */
export class User {
        public LoginDetailsDTO:[];
        public userId: number;
        public FirstName: string;
        public LastName: string;
        public gender: string;
        public roleId: string;
        public roleName:string;
        public profilePicUrl: string;
        public ContactNumber: string;
        public Email: string;
        public FaxNumber: string;
        public alternateContactNumber: string;
        public AddressStreet1: string;
        public AddressStreet2: string;
  public CountryId: string;
  public CountryCode: string;
        public StateId: string;
        public CityId: string;
        public StateName:string;
        public cityName:string;
        public CountryName:string;
        public ZipCode: string
  public Description: string;
  public ActivationUrl: string;
  public password: string;
  public IsPaidAd: boolean;


        
        
        public categoryId: Number;
        public categoryName: string;
        public description: string;
        public status: string;
        public IsActive: boolean;
        public CreatedDate: string;
        public CreatedBy: string;
        public UpdatedDate: string;
        public UpdatedBy: string;
        public CategoryImageUrl: string;

        constructor() { 
        }
        
}

export class LoginDetailsDTO {
public LoginDetailId: number;
public UserId: number;
public password: string;
public Email: string;
public IsActive: boolean;
public CreatedDate: string;
public CreatedBy: string;
public UpdatedDate: string;
public UpdatedBy: string;
}
