import { EventDetails } from './../eventdetails.model';
import { ManageeventComponent } from './../manageevent/manageevent.component';
import { EventDetailsService } from './../eventdetails.service';
import { Component, OnInit } from '@angular/core';
//import { EventDetailsService } from '../managebannerservice';
import {Router, ActivatedRoute} from "@angular/router";
//import { Manageevent} from '../manageevent';
//import { EventDetails} from '../EventDetails';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Observable } from 'rxjs';
import { User } from '../user'; 
import { UserService } from '../user.service';

@Component({
  selector: 'app-eventdetailview',
  templateUrl: './eventdetailview.component.html',
  styleUrls: ['./eventdetailview.component.scss']
})
export class EventdetailviewComponent implements OnInit {

  eventpost: EventDetails[];
  users: User[];
  workflowDetails = [];
  eventpostdetail = new EventDetails();
  userDetails = new User();
  statusMessage: string;
  Address: string;
  workflow: string;
  ContactDetails: string;
  showOtherCity: string = "d-none";
  showImages: string = "col-md-8";
  public RoleId: any;
  logoUrl: string;
  logourls = [];
  imageurls = [];
  adevents: EventDetails[];
  multiimageurls = [];
  isPremium: string;
  description: string = "";

  //adpost = new AdDetails();;
  imagepath = environment.imagepath;

  constructor(private eventDetailsService: EventDetailsService, private userService: UserService, private route: ActivatedRoute, private router: Router, private http: HttpClient) { }


  managebannerback() { this.router.navigate(['/theme/manageevent']); }

  ngOnInit() {
    debugger
    this.getEventDetail(this.route.snapshot.queryParams['id']);

  }

  getEventDetail(id: any) {
    debugger;
    this.workflow = id + "," + "Event";
    //this.eventDetailsService.getEventDetailsById(id).subscribe(eventpostdetail => {console.log(eventpostdetail);});     

    this.eventDetailsService.getEventDetailsById(id).subscribe(eventpost => {
      console.log(eventpost);

             

        this.userService.getUserById(eventpost.postedFor).subscribe(data => {
          var data = JSON.parse(data._body);
          console.log(data);

          if (eventpost.countryCodeContact == "1") {
            this.eventpostdetail.ContactNumber = "USA" + "(+" + eventpost.countryCodeContact + ") " + eventpost.contactNumber;
          }
          else if (eventpost.countryCodeContact == "2") {
            this.eventpostdetail.ContactNumber = "UK" + "(+" + eventpost.countryCodeContact + ") " + eventpost.contactNumber;
          }
          else
            this.eventpostdetail.ContactNumber = "(+" + eventpost.countryCodeContact + ") " + eventpost.contactNumber;

          this.eventpostdetail.PostedForName = eventpost.postedForName;
          this.eventpostdetail.Email = eventpost.email;
          this.eventpostdetail.AlternateContactNumber = eventpost.alternateContactNumber;
          this.eventpostdetail.FaxNumber = eventpost.faxNumber;

          this.eventpostdetail.AddressStreet1 = eventpost.addressStreet1;
          this.eventpostdetail.AddressStreet2 = eventpost.addressStreet2;
          //this.userDetails.countryName =data.countryName;
          this.eventpostdetail.StateName = eventpost.stateName;
          this.eventpostdetail.CityName = eventpost.cityName;
          if (eventpost.cityName == "Other") {
            this.showOtherCity = "col-md-4";
            this.eventpostdetail.OtherCity = eventpost.otherCity;
          }
          this.eventpostdetail.ZipCode = eventpost.zipCode;

         

          this.eventpostdetail.Title = eventpost.title;
          this.eventpostdetail.EventDate = eventpost.eventDate;
          this.eventpostdetail.Website = eventpost.website;
          this.eventpostdetail.validFromDate = eventpost.validFromDate;
          this.eventpostdetail.validTillDate = eventpost.validTillDate;
          this.eventpostdetail.Description = eventpost.description;
          this.description = eventpost.description;
          this.eventpostdetail.EventStatusForDetailsView = this.getStatus(eventpost.isVisible);
          if (eventpost.isPremium == true) {

            eventpost.isPremium = "Premium";
          }
          else {
            eventpost.isPremium = "Free";
          }
          this.eventpostdetail.IsPaidad = eventpost.isPremium;
    
          this.eventpostdetail.Email = eventpost.email;
     
          this.eventpostdetail.CategoryName = eventpost.categoryName;
          //if (eventpost.imageUrl != null)
          //{
          //  this.eventpostdetail.ImageUrl = eventpost.imageUrl;
          //  this.logoUrl = this.imagepath + 'Events/' + id + '/Image/' + this.eventpostdetail.ImageUrl;
          //  this.logourls.push(this.logoUrl);
          //}




          if (eventpost.imageUrl === null) {
            if (eventpost.imageUrl === "") {
              this.eventpostdetail.ImageUrl = "";
              this.showImages = "col-md-4 d-none";
            }
            else
              this.showImages = "col-md-4 d-none";

          }
          else {
            if (eventpost.imageUrl != null) {
              this.eventpostdetail.ImageUrl = eventpost.imageUrl;
              this.logoUrl = this.imagepath + 'Events/' + id + '/Image/' + this.eventpostdetail.ImageUrl;
              this.logourls.push(this.logoUrl);
              this.showImages = "col-md-4";
            }
            else
              this.showImages = "col-md-4 d-none";
          }

        });
      });
   
    }

  getStatus(status: boolean) {
    debugger;
    var currentStatus: string = "";
    if (status == true)
      currentStatus = "Active";
    else if (status == false)
      currentStatus = "Inactive";
    else
      currentStatus = "Pending";
    return currentStatus;


  }
  manageAdsback()
  {
        this.router.navigate(['/theme/manageevent']);
      }


}

  

