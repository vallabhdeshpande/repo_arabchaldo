import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventdetailviewComponent } from './eventdetailview.component';

describe('EventdetailviewComponent', () => {
  let component: EventdetailviewComponent;
  let fixture: ComponentFixture<EventdetailviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventdetailviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventdetailviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
