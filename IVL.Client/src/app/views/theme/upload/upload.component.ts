import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { HttpEventType, HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'theme-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  baseUrl = environment.baseUrl;
  public progress: number;
  public message: string;
  public imagePath;
  imgURL: any;
  uploadedFileName: string;
public imgmessage: string;
urls = [];
imagenames=[];
@Input() imgpatharr:string[];
  url:any;
  @Output() public onuploadFinishedLogo = new EventEmitter();

  constructor(private http: HttpClient, private toastr: ToastrService) { }

  ngOnInit() {
    this.url = "";
  } 

  //Image Upload
  onSelectFile(event) {   
//debugger;
    if (event.target.files && event.target.files[0]) {
       var filesAmount = event.target.files.length;
        for (let i = 0; i < filesAmount; i++) {
          //Image type validation
          if(event.target.files[i].type === 'image/jpeg' || 
          event.target.files[i].type === 'image/png' || 
          event.target.files[i].type ==='image/jpg'|| 
          event.target.files[i].type ==='image/bmp')
          {
            let fileToUpload = <File>event.target.files[i];
            if (fileToUpload.size <= 2097152) {
              var fileNameToUpload = this.getFileNameWithDateTime(fileToUpload.name);
              this.uploadedFileName = fileNameToUpload;
              var reader = new FileReader();
              this.imagenames.pop();
              this.imagenames.push(fileNameToUpload);
              this.urls.pop();
              reader.onload = (event: any) => {

                this.urls.push(event.target.result);
                event.target.result = "";
              }
              debugger;
              const formData = new FormData();

              formData.append('file', fileToUpload, fileNameToUpload);
              this.http.post(this.baseUrl + 'upload', formData, { reportProgress: true, observe: 'events' })
                .subscribe(event => {
                  if (event.type === HttpEventType.UploadProgress)
                    this.progress = Math.round(100 * event.loaded / event.total);
                  else
                    if (event.type === HttpEventType.Response) {
                    debugger;
                    this.message = 'Upload success.';
                    this.onuploadFinishedLogo.emit(this.imagenames.join());
                    debugger;
                  }
                });
              reader.readAsDataURL(event.target.files[i]);
              // reader.removeEventListener();
            }
            else
              this.toastr.warning("file size should be less than 2 MB");
          }
        else{
          this.imgmessage = "Only images are supported.";
            this.toastr.warning("Only images(jpeg/png/jpg/bmp) are supported.");
      return;
        }
      }
        
    }
    this.imgpatharr=this.imagenames;
   
  }

  // Get Fime with current Date time stamp

  getFileNameWithDateTime(fileNameToUpload)
  {
    var ext=(fileNameToUpload.substring(fileNameToUpload.lastIndexOf('.')+1, fileNameToUpload.length) || fileNameToUpload);
    fileNameToUpload=fileNameToUpload.substring(0, fileNameToUpload.lastIndexOf('.'));
    //fileNameToUpload+=
 
    var today = new Date();
 var sToday = (today.getMonth()+1).toString();
 sToday += today.getDate().toString();
 //sToday += today.getYear().toString();
 sToday += today.getHours().toString();
 sToday += today.getMinutes().toString();
 sToday += today.getSeconds().toString();
 return fileNameToUpload+sToday+"."+ext;
  }




// Image Delete
     onimagedelete(i){     
       debugger;
      this.urls.splice(i,1);
       this.imagenames.splice(i, 1);
       this.onuploadFinishedLogo.emit(this.imagenames.join());
       this.http.delete(this.baseUrl + 'upload/DeleteImage?ImageName=' + this.uploadedFileName)
         //?searchCriteria='+searchCriteria
         .subscribe(event => {


         });
}

  
  
  
  
}
