import { LoggedInUser } from './../../../shared/common.model';
import { Component, OnInit } from '@angular/core';
//import { AdDetailsService } from './addetails.service';
import { AdDetailsService } from '../addetails.service';
import { Router } from "@angular/router";
import { AdDetails, WorkflowstatusDTO, AdStatusDetailsDTO } from '../addetails.model';
import { IfStmt, debugOutputAstAsTypeScript } from '@angular/compiler';
import { OrderPipe } from 'ngx-order-pipe';
import { environment } from '../../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';


//import { environment } from '../../../../assets/img/default/no_dp.jpg'

declare var jQuery:any;
@Component({
  selector: 'app-managead',
  templateUrl: './managead.component.html',
  styleUrls: ['./managead.component.scss'],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }]
})
export class ManageadComponent implements OnInit {
  myDateValue: Date;
  charLength: string = "0";
  adposts: AdDetails[];
  // workflowdetails: WorkflowstatusDTO[];
  adStatusdetails: AdStatusDetailsDTO[];
  workflowdetails: string;
  statusMessage: string;
  setStatus: string = "";
  deleteremark: string;
  activity: boolean;
  visibilitybutton: string;
  visibilityspan: string;
  setStatusActive: string = "";
  setStatusInactive: string = "";
  adpost = new AdDetails(); order: string;
  reverse: any;
  modalRemark: string;
  public pageSize: number = 10;
  public p: number;
  remark: string;
  workflowstatusId: number;
  action: string = "";
  actionmessage: string = "";
  adId = 0;
  imagepath = environment.imagepath;
  loggedInRole: string;
  loggedInUserId: string;
  validTill: string;
  validFrom: string;
  maxDate: string;
  minDate: string;
  //minValidTillDate: string;
  //maxValidFromDate: string;
  modalexit: string = "";
  setType: string = "";
  LoggedInUserId: string;
  showDates: string = "col-md-6";
  approveButtonText: string;
  approveButtonStyle: string;
  form: FormGroup;
  showPremium: string = "col-md-3";
  //managemodal:string="modal fade";
  remarkstring: string[];
  poststring: string;
  remarkstring1: string;
  remarkstring2: string;
  remarkstring3: string;
  isLoading: boolean = false;
  adStatus: string;
  Ispremium: boolean = false;
  filter: string = "";

  minDateModel: NgbDateStruct;
  minValidTillDate: NgbDateStruct;
  maxValidFromDate: NgbDateStruct;

  date: { year: number, month: number };
  hoveredDate: NgbDate;
  modelValidFrom: Date = null;
  modelValidTill: Date = null;

  selectToday() {
    this.minDateModel = this.calendar.getToday();
    this.minValidTillDate = this.calendar.getToday();
  }
  onvalidFromDateSelect(event: any) {
    this.minValidTillDate = event;
    this.validFrom = this.modelValidFrom.toJSON().split('T')[0];

  }
  onvalidTillDateSelect(event: any) {
    this.maxValidFromDate = event;
    this.validTill = this.modelValidTill.toJSON().split('T')[0];

  }


  // constructor(private AdDetailsService: AdDetailsService, private router: Router) { }

  //gettoday(){ 
  //  debugger;
  //  var CurrentDate = new Date();
  //  this.minDate = CurrentDate.toJSON().split('T')[0];
  //  CurrentDate.setMonth(CurrentDate.getMonth() + 1);
  //  this.maxDate = CurrentDate.toJSON().split('T')[0];
  //   }

  ngAfterViewInit() {
    document.getElementById("#modalvalidFrom").setAttribute("min", this.minDate);
    document.getElementById("#modalvalidTill").setAttribute("min", this.minDate);


  }
  toggleVisibility(e) {
    this.Ispremium = e.target.checked;
  }
  //  delay(ms: number) {
  //   return new Promise( resolve => setTimeout(resolve, ms) );
  // }
  ngOnInit() {
    //  this.createForm();
    this.charLength = "0/200";
    this.myDateValue = new Date();
    this.loggedInUserId = localStorage.getItem('userId');
    // this.delay(5000);
    //this.gettoday();
    this.selectToday();
    this.getAdList();

  }

  constructor(private toastr: ToastrService, private orderPipe: OrderPipe, private AdDetailsService: AdDetailsService, private router: Router, private fb: FormBuilder, private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private config: NgbDatepickerConfig) {
    //this.sortedCollection = orderPipe.transform(this.adposts, 'title');
    // console.log(this.sortedCollection);
  }
  createForm() {
    this.form = this.fb.group({
      validFrom: ['', Validators.required],
      validTill: ['', Validators.required]
    }, { validator: this.dateLessThan('validFrom', 'validTill') });
  }


  dateLessThan(from: string, to: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let f = group.controls[from];
      let t = group.controls[to];
      if (f.value > t.value) {
        return {
          dates: "Date from should be less than Date to"
        };
      }
      return {};
    }
  }
  getAdList(): void {

    this.AdDetailsService.getAdList().subscribe(adposts => {
      this.adposts = adposts;

      console.log(this.adposts);
      // Status
     // debugger;
      var adlenth = 0;
      if (this.adposts != null) {
        if (adposts.length > 0) {
          for (let i = 0; i < adlenth; i++) {


            if (adposts[i].isFeatured === "True") {
              adposts[i].isFeatured = "Premium";
              this.setType = "badge badge-warning";
            }
            else {
              adposts[i].isFeatured = " ";
              this.setType = "";
            }
          }
          this.adposts = adposts;
        }
        else
          this.adposts = [];
      }

      else
        this.adposts = [];

    });
  }
  // openModal()
  // {
  //   debugger;
  //   //#modalRemarkText
  //   let modalRemarkText = (<HTMLInputElement>document.getElementById('modalRemarkText')).value;
  //   if(modalRemarkText!="")
  //   modalRemarkText="";
  //   return "modal";

  // }
  getType(Premium) {
    // debugger
    if (Premium == "Premium")
      return this.setStatus = "badge badge-warning";
    else
      return this.setStatus = " ";


  }
  getPicUrl(picurl: string, adId) {
    //debugger;
    if (picurl == null)
      return "assets/img/default/no_image_placeholder.jpg";
    else if (picurl == "")
      return "assets/img/default/no_image_placeholder.jpg";
    else
      return this.imagepath + 'Ads/' + adId + '/Logo/' + picurl;

  }

  getStatus(Status: string) {

    if (Status == "Active")
      return this.setStatus = "badge badge-success";

    if (Status == "InActive")
      return this.setStatus = "badge badge-danger";

    if (Status == "Pending")
      return this.setStatus = "badge badge-warning";

  }

  hideActivity(Status: string) {

    if (Status == "Active")
      return "d-none";

    if (Status == "InActive")
      return "d-none";

    if (Status == "Pending")
      return "";
  }


  updatePost(adpost: AdDetails): void {
    debugger;
    this.router.navigate(['/theme/postad'], { queryParams: { adId: adpost.adId } });
  };
  gotoAdDetails(adpost: AdDetails): void {
    debugger
    this.router.navigate(['/theme/addetailsview'], { queryParams: { adId: adpost.adId } });
  };
  NavigateToAddPost(): void {
    debugger
    this.router.navigate(['/theme/postad']);
  };

  sortedCollection: any[];


  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }
  ActionOnApprove(adId, isFeatured, event) {

    debugger;
    //$("#modalRemarkText").value="";
    // (<HTMLInputElement>document.getElementById('remark')).value="";
    this.remark = null;
    this.validFrom = null;
    this.validTill = null;
    this.modelValidFrom = null;
    this.modelValidTill = null;
    this.maxValidFromDate = null;
    this.minValidTillDate = this.calendar.getToday();
    this.showDates = "col-md-6";
    this.adId = adId;
    this.action = "Approve";
    this.approveButtonText = "";
    this.approveButtonText = "Approve";
    this.approveButtonStyle = "";
    this.approveButtonStyle = "btn btn-sm btn-success";
    this.actionmessage = "Are you sure to approve this business listing?";
    this.showPremium = "col-md-3"
    if (isFeatured == "Premium")
      this.Ispremium = true;
    else
      this.Ispremium = false;
  }
  ActionOnReject(adId, event) {
    this.remark = null;
    this.validFrom = null;
    this.validTill = null;
    this.modelValidFrom = null;
    this.modelValidTill = null;
    this.maxValidFromDate = null;
    this.minValidTillDate = this.calendar.getToday();
    this.showDates = "col-md-6 d-none";
    this.adId = adId;
    this.action = "Reject";
    this.approveButtonText = "";
    this.approveButtonText = "Reject";
    this.approveButtonStyle = "";
    this.approveButtonStyle = "btn btn-sm btn-warning";
    this.actionmessage = "Are you sure to reject this record?";
    this.Ispremium = false;
    this.showPremium = "col-md-3  d-none";
  }

  ActionOnPost(remark) {
    debugger;
    this.poststring = remark;
    this.remarkstring = this.poststring.split(',');
    this.remarkstring1 = this.remarkstring[0];
    this.remarkstring2 = this.remarkstring[1];
    this.remarkstring3 = this.remarkstring[2].replace(/,/g, ' ');

    if (this.remarkstring[3] == "true")
      this.Ispremium = true
    else
      this.Ispremium = false;

    if ((this.approveButtonText == "Approve") && ((this.remarkstring1 == "null") || (this.remarkstring2 == "null") || (this.remarkstring3 == "null"))) {
      this.toastr.warning("All fields are required to approve");
      return;
    }

    if ((this.approveButtonText == "Reject") && (this.remarkstring3 == "null")) {
      this.toastr.warning("Remark is required to reject");
      return;
    }

    this.workflowdetails = this.adId + "," + this.loggedInUserId + "," + this.action + "," + remark;


    //if (confirm(this.actionmessage)) {
    this.AdDetailsService.updatewithWorkflow(this.workflowdetails).subscribe(response => {
      console.log(response);
      console.log('Updated');
      this.modalexit = "modal";
      document.getElementById("closeBtn").click();
      // this.managemodal="modal fade";
      // jQuery('remark').modal('hide');
      this.getAdList();
      if (this.action === "Approve")
        this.toastr.success('Business listing approved succesfully');
      if (this.action === "Reject")
        this.toastr.warning('Business listing rejected');

    },

      (error) => {
        console.log(error);

        this.toastr.error('Problem with service. Please try again later!');
      });
    // }
    //this.router.navigate(['/theme/postad'],{queryParams:{adId:adpost.adId}});

  }
  // close modal popup

  DeleteAd(adId: number, adStatus: string, event) {
    debugger
    this.actionmessage = 'Are you sure to delete this business listing ?';
    this.adId = adId;
    this.adStatus = adStatus;
  }

  ActionDelete(): void {
    debugger;
    if (this.adStatus !== "Active") {
      this.action = "Delete";
      this.deleteremark = "";
      this.workflowdetails = this.adId + "," + this.loggedInUserId + "," + this.action + "," + this.deleteremark;
      //this.workflowdetails=this.adId+","+this.deleteremark+","+this.action;
      this.AdDetailsService.updatewithWorkflow(this.workflowdetails).subscribe(response => {
        if (response.status === 200) {
          debugger;
          console.log(response);
          if ((<any>response)._body === "true")
            this.toastr.success('Business Listing successfully deleted.');
          else
            this.toastr.info('Business Listing is active on website.');
          debugger

        }
        else {
          this.toastr.warning('Problem with service. Please try again later!');
        }

        this.getAdList();
      },

        (error) => {
          console.log(error);

          this.toastr.error('Problem with service. Please try again later!');
        });
    }
    else {
      this.toastr.info('This is active Business Listing, can not be deleted.');

    }
  }


  adDetailsView(adId: number) {

    this.router.navigate(['/theme/addetailsview'], { queryParams: { adId: adId } });
  }


}
