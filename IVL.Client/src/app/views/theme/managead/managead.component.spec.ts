import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageadComponent } from './managead.component';

describe('ManageadComponent', () => {
  let component: ManageadComponent;
  let fixture: ComponentFixture<ManageadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
