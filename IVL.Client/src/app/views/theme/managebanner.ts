export class Managebanner
{   
    public bannerId : number;
    public Title : string;
    public Description : string;
    public ImageUrl : string;
    public IsActive : boolean;
    public isVisible:boolean;
    public Activity: boolean;
    public CreatedDate : Date;
    public CreatedBy : number;
    public UpdatedDate : Date;
    public UpdatedBy : bigint;
    public PostedFor : bigint;
    public PostedForName : string;
    public ValidFromDate: Date;
    public ValidTillDate: Date;
    public Website: string;
    public AddressStreet1: string;
    public AddressStreet2: string;
    public CountryId: string;
    public StateId: string;
    public CityId: string;
    public ZipCode: string;
    public CityName: string;
    public StateName: string;
    public ContactPersonName: string;
    public ContactNumber: string;
    public AlternateContactNumber: number;
    public BannerImageCount: number;
    public PlanId: number;
    public FaxNumber: number;
    public Email: string;
    public IsFeatured: boolean;
    public CountryCodeContact: string;
    public OtherCity: string;


    constructor()
    {}
}

export class WorkflowstatusDTO {

  public workflowStatusId: number;
  public Status: string;
  public IsActive: boolean;
  public CreatedDate: string;
  public CreatedBy: string;
  public updatedDate: string;
  public updatedBy: string;
  constructor() {
  }

}
