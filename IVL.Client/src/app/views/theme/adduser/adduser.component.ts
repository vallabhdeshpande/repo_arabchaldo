import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { City } from '../city.model';
import { State } from '../state.model';
import { Country } from '../country.model';
import { User, LoginDetailsDTO } from '../user';
import { UserService } from '../user.service';
import { Http } from '@angular/http';
import { HttpClient, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { AdDetailsService } from '../addetails.service';
import { Router, ActivatedRoute } from "@angular/router";
import { first } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
//import { MustMatch } from './_helpers/must-match.validator';
import { ToastrService } from 'ngx-toastr';
import { DefaultLayoutComponent } from '../../../containers/default-layout/default-layout.component';
import { UploadComponent } from '../upload/upload.component';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import * as countryCodeData from '../../shared/countrycodes.json';
@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.scss']
})
export class AdduserComponent implements OnInit {
  @ViewChild(UploadComponent, { static: false }) UploadCmpt: UploadComponent;
  statusMessage: string; user = new User();;
  public _allUserRole: Observable<User[]>;
  public _allCountry: Observable<Country[]>;
  public _allState: Observable<State[]>;
  public _allCity: Observable<City[]>;
  public _roleDDL: Observable<any[]>;
  CountryId: string = "0";
  StateId: string = "0";
  CityId: string = "0";
  states: any[];
  cities: any[];
  StateName: string = "";
  CityName: string = "";
  bindStateId: number = 0;
  CategoryId: string = "0";
  SubCategory: string = "0";
  //roleId: string="0";
  userId = 0;
  gendor: number = 0;
  action: string = "";
  submitted = false;
  FormUserAdd: any;
  LoginDetailsDTO: any;
  visibilityreset: boolean;
  visibilitysavebutton: boolean;
  visibilityeditButton: boolean;
  public logoresponse: string[];
  saveButtonText: string = "Save";
  AddUserText: string = "Add New User";
  displayText = 'btn btn-sm btn-primary';
  displayBrowse = 'form-bg-section';

  displayExtLogo = 'col-md-6 d-none';
  displayNewLogo = 'col-md-6';
  displayExtUploads = 'form-bg-section d-none';
  updateProfile: string;
  roleName: string = "";
  extLogoUrls = [];
  extLogos = [];
  imagepath = environment.imagepath;
  countryCodesNew: any = (countryCodeData as any).default;
  countrycode = this.countryCodesNew;
  constructor(private toastr: ToastrService, private userService: UserService, private AdDetailsService: AdDetailsService, private route: ActivatedRoute, private router: Router, private http: HttpClient, private formbulider: FormBuilder, private httpservice: Http, private saro: DefaultLayoutComponent) { }




  FillUserRoleDDL() {
    debugger;
    this.roleName = localStorage.getItem('roleName');
    this._allUserRole = this.userService.UserDDLByRole(this.roleName);

    console.log(this._allUserRole);


  }


  FillCountryDDL() {
    this._allCountry = this.AdDetailsService.CountryDDL();
    this._allCountry = this.AdDetailsService.CountryDDL();
    this.CountryId = "1";
    this._allState = this.AdDetailsService.StateDDL(this.CountryId);
    //this.StateId = "22";
    //this.StateName=event.target.options[this.StateId].innerHTML;
    this._allCity = this.AdDetailsService.CityDDL(this.StateId);

    this.AdDetailsService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      this.states = data;
    });

  }

  FillStateDDL(event: any) {
    if ((event.target.value == "0") || ((event.target.value == ""))) {
      this.FormUserAdd.value.countryId = 0;
      //this._allState = this._commonService.StateDDL("0");
      var x = document.getElementsByClassName("ng-value-label");
      x[1].innerHTML = "--Select State--";
      x[2].innerHTML = "--Select City--";
      //var i;
      //for (i = 3; i < x.length; i++) {
      //  x[i].innerHTML = "";
      //}
      this.states = [];
      this.cities = [];
      // this.FillCityDDL(0);
      console.log(this.states);
      this.FormUserAdd.value.stateId = 0;
      //this._allCity = this._commonService.CityDDL("0");
      this.FormUserAdd.value.cityId = 0;
    }

    else {
      this.CountryId = event.countryId;

      this._allState = this.AdDetailsService.StateDDL(this.CountryId);
      debugger;
      this.AdDetailsService.StateDDL("1").subscribe(data => {
        console.log(data);
        //debugger;
        this.states = data;
      });

      this.AdDetailsService.CityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
      this._allState = this.AdDetailsService.StateDDL(this.CountryId);
      //this.CountryName = event.target.options[this.CountryId].innerHTML;
      //this.FormPostAd.countryName = this.CountryName;
    }

  }

  FillCityDDL(event: any) {
    if (event != 0) {
      var x = document.getElementsByClassName("ng-value-label");
      //x[1].innerHTML = "--Select State--";
      x[2].innerHTML = "--Select City--";
      this.StateId = event.stateId;
      this.StateName = event.stateName;

      this._allCity = this.AdDetailsService.CityDDL(this.StateId);
      this.FormUserAdd.value.stateName = this.StateName;

      this.AdDetailsService.CityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
    }
    else {
      var x = document.getElementsByClassName("ng-value-label");
      this.cities = [];
    }

  }

  FillCityName(event: any) {
    debugger;
    this.CityId = event.cityId;
    this.CityName = event.cityName;
    this.FormUserAdd.value.cityName = this.CityName;
  }
  public logoimagePath;
  logoimgURL: any;
  public logomessage: string;
  previewlogo(files) {

    if (files.length === 0)
      return;

    var mimeType = files[0].type;

    if (mimeType.match(/image\/*/) == null) {
      this.logomessage = "Only images are supported.";
      return;
    }

    var reader = new FileReader();
    this.logoimagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.logoimgURL = reader.result;
    }
  }

  ngOnInit() {
    this.FillUserRoleDDL();
    this.FillCountryDDL();
    this.FormUserAdd = this.formbulider.group({
      userId: ['', ''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      gender: ['', Validators.required],
      profilePicUrl: [null, ''],
      //roleId: ['', Validators.required],
      roleName: ['', Validators.required],
      countryCodeContact: ['1', Validators.required],
      contactNumber: ['', [Validators.required, Validators.minLength(10)]],
      email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      alternateContactNumber: [''],
      faxNumber: [''],
      addressStreet1: ['', ''],
      addressStreet2: ['', ''],
      countryId: ['1', ''],
      stateId: [this.StateId, ''],
      cityId: [this.CityId, ''],
      cityName: [this.CityName, ''],
      zipCode: ['', ''],
      stateName: [this.StateName, ''],
      description: ['', ''],
      password: ['', '']
    });
    debugger
    if (this.route.snapshot.queryParams['userId'] != undefined) {
      debugger;
      this.updateProfile = this.route.snapshot.queryParams['UpdateProfile'];
      if (this.updateProfile == "UpdateProfile") {
        this.AddUserText = "Update Profile";
        document.getElementById("roleName").setAttribute("disabled", "disabled");
        document.getElementById("email").setAttribute("disabled", "disabled");
      }

      this.getUser(this.route.snapshot.queryParams['userId']);
    }

  }
  ngAfterViewInit() {
    debugger
    document.getElementById('#Save').innerHTML = this.saveButtonText;
    document.getElementById('#AddUser').innerHTML = this.AddUserText;

  }
  getUser(userId) {
    debugger
    this.saveButtonText = "Update";
    if (this.updateProfile == "UpdateProfile")
      this.AddUserText = "Update Profile";
    else
      this.AddUserText = "Update user";
    this.displayText = 'btn btn-sm btn-primary d-none';
    this.displayBrowse = 'col-md-4 d-none';




    this.userService.getUserById(userId).subscribe(data => {
      debugger
      var data = JSON.parse(data._body);
      console.log(data);

      this.action = "edit";
      this.AdDetailsService.StateDDL(data.countryId).subscribe(data => {
        debugger
        console.log(data);
        this.states = data;

      });

      this.AdDetailsService.CityDDL(data.stateId).subscribe(data => {
        debugger;
        console.log(data);
        this.cities = data;
      });
      this.countrycode = this.countryCodesNew;
   
      this._allState = this.AdDetailsService.StateDDL(data.countryId);
      this._allCity = this.AdDetailsService.CityDDL(data.stateId);
      var genderreceived;
      if (data.gender == 1)
        data.gender = "1";
      else
        data.gender = "2";


      // Ad Logo Retrieval
      this.displayExtUploads = 'form-bg-section';
      if (data.profilePicUrl !== null)
        this.renderExistingImages(data.profilePicUrl, data.userId, "Logo");
      else
        this.displayExtLogo = 'col-md-6 d-none';


      if (data.imageUrl === null && data.profilePicUrl === null)
        this.displayExtUploads = 'form-bg-section d-none';

      if (data.countryCodeContact === null)
        data.countryCodeContact = "1";
      this.FormUserAdd.setValue({
        userId: data.userId,
        firstName: data.firstName,
        lastName: data.lastName,
        gender: data.gender,
        // 'roleId': data.roleId,
        profilePicUrl: data.profilePicUrl,
        roleName: data.roleName,
        contactNumber: data.contactNumber,
        countryCodeContact: data.countryCodeContact.toString(),
        alternateContactNumber: data.alternateContactNumber,
        email: data.email,
        faxNumber: data.faxNumber,
        addressStreet1: data.addressStreet1,
        addressStreet2: data.addressStreet2,
        countryId: data.countryId,
        stateId: data.stateId.toString(),
        cityId: data.cityId.toString(),
        cityName: data.cityName,

        stateName: data.stateName,
        description: data.description,
        zipCode: data.zipCode,
        password: ''

      });
    });
  }

  adduser(): void {
    debugger
    this.submitted = true;
    // stop the process here if form is invalid
    if (this.FormUserAdd.invalid) {
      this.toastr.warning("Mandatory fields missing. Please fill all mandatory fields.");
      return;
    }


    if (this.action == "") {
      this.FormUserAdd.value.userId = 0;
      if (this.FormUserAdd.value.cityId == null || this.FormUserAdd.value.cityId == "")
        this.FormUserAdd.value.cityId = 0;
      if (this.FormUserAdd.value.stateId == null || this.FormUserAdd.value.stateId == "")
        this.FormUserAdd.value.stateId = 0;
      this.FormUserAdd.value.stateName = this.StateName;
      this.FormUserAdd.value.cityName = this.CityName;
      if (this.FormUserAdd.value.countryId == null || this.FormUserAdd.value.countryId == "")
        this.FormUserAdd.value.countryId = 0;
      this.FormUserAdd.value.profilePicUrl = this.logoresponse;
      this.FormUserAdd.value.password = "admin@1234";

      console.log(this.FormUserAdd.value)
      this.userService.add(this.FormUserAdd.value)
        .subscribe((response) => {
          console.log(response);
          if ((<any>response)._body === "true")
            this.toastr.success('User inserted successfully.');
          else
            this.toastr.info('User already exist.');

          this.Navigatetomanageuser();
        },

          (error) => {
            console.log(error);

            this.toastr.error('Problem with service. Please try again later!');
          }
        );
    }
    else if (this.action == "edit") {
      debugger;
      console.log(this.FormUserAdd.value);
      if (this.FormUserAdd.value.gender == 1)
        this.FormUserAdd.value.gender = "1";
      else
        this.FormUserAdd.value.gender = "2";
      //render images

      if (this.extLogos.length > 0) {
        //this.logoresponse.pop();
        this.logoresponse = this.extLogos[0];
      }
      else if (this.logoresponse !== undefined)
        this.FormUserAdd.value.profilePicUrl = this.logoresponse;
      else
        this.FormUserAdd.value.profilePicUrl = "";
      if (this.FormUserAdd.value.cityId == null || this.FormUserAdd.value.cityId == "")
        this.FormUserAdd.value.cityId = 0;

      if (this.FormUserAdd.value.stateId == null || this.FormUserAdd.value.stateId == "")
        this.FormUserAdd.value.stateId = 0;
      if (this.FormUserAdd.value.countryId == null || this.FormUserAdd.value.countryId == "")
        this.FormUserAdd.value.countryId = 0;
      this.FormUserAdd.value.stateName = this.StateName;
      this.FormUserAdd.value.cityName = this.CityName;
      this.userService.updateUser(this.FormUserAdd.value)
        .pipe(first())
        .subscribe(
          data => {
            if (data.status === 200) {
              console.log(data);
              if (this.updateProfile == "UpdateProfile") {
                localStorage.setItem('firstName', this.FormUserAdd.value.firstName);
                localStorage.setItem('lastName', this.FormUserAdd.value.lastName);
                localStorage.setItem('profilePic', this.FormUserAdd.value.profilePicUrl);
                // let saro = new DefaultLayoutComponent(this.httpservice);
                this.saro.ngOnInit();
                this.toastr.success('User profile updated successfully.');
                //this.router.navigate(['/dashboard']);
              }
              else {
                this.toastr.success('User updated successfully.');
                this.router.navigate(['/theme/manageuser']);
              }

            } else {
              this.toastr.info(data.message);
            }
          },
          error => {
            this.toastr.error(error);
          });
    }
  }
  public uploadFinishedLogo = (event) => {
    debugger;
    this.logoresponse = event;
  }

  
  Navigatetomanageuser() {
    if (this.updateProfile == "UpdateProfile") {
      this.router.navigate(['/dashboard']);
    }
    else
      this.router.navigate(['/theme/manageuser']);
  }
  get f() { return this.FormUserAdd.controls; }

  // Render Images 

  onextLogoUrldelete(i) {
    this.extLogoUrls.splice(i, 1);
    this.extLogos.pop();

    this.displayNewLogo = 'col-md-6';
  }

  reset() {
    this.ngOnInit();
    this.UploadCmpt.onimagedelete(0);

  }


  refresh(): void {
    debugger;
    window.location.reload();
    this.UploadCmpt.ngOnInit();

  }

  renderExistingImages(imageUrls, userId, uploadType) {
    debugger;
    if (imageUrls !== "") {
      debugger
      var extImages = imageUrls.split(',');
      if (extImages.length > 0) {
        if (uploadType == "Logo") {
          for (let i = 0; i < extImages.length; i++) {
            this.extLogoUrls.push(this.imagepath + 'UserProfile/' + userId + '/ProfilePic/' + extImages[i]);
            this.extLogos.push(extImages[i]);
            this.displayExtLogo = 'col-md-6';
            this.displayNewLogo = 'col-md-6 d-none';
          }


        }

      }

    }
  }

  
}


