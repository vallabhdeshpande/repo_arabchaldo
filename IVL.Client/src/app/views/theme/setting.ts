
export class Setting {

  public configurationId: string;
  public logoCharge: string;
  public imageCharge: string;
  public bannerCharge: string;
  public smtp: string;
  public port: string;
  public fromMailId: string;
  public domain: string;
  public password: string;
  public approveMailTemplate: string;
  public registrationMailTemplate: string;
  public jobsMailTemplate: string;
  public contactMailTemplate: string;
  public feedbackMailTemplate: string;
  public isActive: boolean;
  public aboutUstemplate: string;
  public contactUstemplate: string;
  public termsOfUseTemplate: string;
  public privacyPolicyTemplate: string;
  public disclaimerTemplate: string;
  public faqtemplate: string;
  public aboutUstemplateArabic: string;
  public contactUsTemplateArabic: string;
  public termsOfUseTemplateArabic: string;
  public privacyPolicyTemplateArabic: string;
  public disclaimerTemplateArabic: string;
  public faqtemplateArabic: string;


  constructor() {
  }

}
