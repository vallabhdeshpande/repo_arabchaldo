import { TestBed } from '@angular/core/testing';

import { ManageblogService } from './manageblog.service';

describe('ManageblogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageblogService = TestBed.get(ManageblogService);
    expect(service).toBeTruthy();
  });
});
