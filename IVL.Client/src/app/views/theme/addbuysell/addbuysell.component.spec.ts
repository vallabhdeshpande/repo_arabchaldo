import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddbuysellComponent } from './addbuysell.component';

describe('AddbuysellComponent', () => {
  let component: AddbuysellComponent;
  let fixture: ComponentFixture<AddbuysellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddbuysellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddbuysellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
