import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Buysell } from '../buysell.model';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs-compat/Observable';
import { Paymentplan } from '../paymentplan';
import { ToastrService } from 'ngx-toastr';
import { BuysellService } from '../buysell.service';
import { Paymentplanservice } from '../paymentplanservice';
import { UserService } from '../user.service';
import { CommonService } from '../../shared/common.service';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Router, ActivatedRoute } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { UploadComponent } from '../../../Website/upload/upload.component';
import { MultiUploadComponent } from '../../../Website/multiupload/multiupload.component';
import { User } from '../user';
import { Country,State,City,category,Subcategory,Roles,CountryCodes } from '../../shared/common.model';
import * as countryCodeData from '../../shared/countrycodes.json';
@Component({
  selector: 'app-addbuysell',
  templateUrl: './addbuysell.component.html',
  styleUrls: ['./addbuysell.component.scss']
})
export class AddbuysellComponent implements OnInit {
  @ViewChild(UploadComponent, { static: false }) UploadCmpt: UploadComponent;
  @ViewChild(MultiUploadComponent, { static: false }) MultiUploadCmpt: MultiUploadComponent;
  public _allUser: Observable<User[]>;
  public _allCountry: Observable<Country[]>;
  public _allCountryCodes: Observable<CountryCodes[]>;
  public _allState: Observable<State[]>;
  public _allStateNew: Observable<State[]>;
  public _allCity: Observable<City[]>;
  public _allCategory: Observable<category[]>;
  public _allSubCategory: Observable<Subcategory[]>;
  public _allroles: Observable<Roles[]>;
  FormPostBuySell: any;
  postedFor: string = "0";
  CountryId: string = "0";
  StateId: string = "0";
  CityId: string = "0";
  states: any[];
  cities: any[];
  CategoryId: string = "0";
  SubCategoryId: string = "0";
  RoleId: string = "0";
  PostedForName: string = "";
  CountryName: string = "";
  StateName: string = "";
  CityName: string = "";
  categoryName: string = "";
  DefaultCategoryId: string = "";
  actionButton;
  action = "";
  myData = null;
  formData = null;
  msgs = null;
  public imagefilename: string[];
  public logofilename: string[];
  buysellPost: Buysell[];
  buysellPosts = new Buysell();
  maxDate: string;
  minDate: string;
  validTill: string;
  loggedInUserId: string;
  serverpath = environment.serverpath;
  commonimagepath = environment.imagepath;
  users: any[];
  categories: any[];
  isPaidAd: boolean = false;
  displayPaidSection: boolean = false;
  displayFreeSection: boolean = false;
  adTypeFree: boolean = true;
  adTypePaid: boolean = false;
  //resetButtonStyle:string="block";
  public logoresponse: string[];
  public imageresponse: string[];
  roleList1: any[] = [];
  roleData: any[] = [];
  addbutton: HTMLElement;
  submitted = false;
  lastkeydown1: number = 0;
  subscription: any;
  countryCodeList: Array<any> = [];
  titleText: string = "Post New Buy/Sell Listing";
  titlecss: string = "nav-icon icon-tag";

  private _allPlan: Observable<Paymentplan[]>;
  private previousPlan: Paymentplan;
  tempPlanId: number;
  tempLogoImageUrl: any;
  tempAdImageUrl: any;

  remainingAdImage: number = 0;
  remainingLogoImage: number = 0;
  adBuySellCount: number;
  remainingbuySellCount: number = 0;
  adimagefornewpost: string[];

  RadioFree: boolean = false;
  RadioPaid: boolean = false;
  statusMessage: string;
  saveButtonText: string = "Save";
  displayText = 'btn btn-sm btn-primary';
  ShowextImageUrl: boolean = false;
  showImageUrl: boolean = true;
  extImageUrl: string = "";
  extImageurls = [];
  extImages = [];
  UploadType: string = "";
  displayExtUploads = 'form-bg-section';
  displayExtImages = 'col-md-6';
  showextuploads: boolean = false;
  countryCodesNew: any = (countryCodeData as any).default;
  countrycode = this.countryCodesNew;
  constructor(private toastr: ToastrService, private _buySellService: BuysellService, private paymentPlanService: Paymentplanservice,
    private _userService: UserService, private _commonService: CommonService, private _http: HttpClient, private _router: Router,
    private _formbulider: FormBuilder, private _route: ActivatedRoute, @Inject(DOCUMENT) document) { }


  // 
  public uploadFinishedLogo = (event) => { this.imageresponse = event; }
  public uploadFinishedImage = (event) => { this.imageresponse = event; }
  ngOnInit() {
    debugger
    this.loggedInUserId = localStorage.getItem('userId');
    this.FillCountryDDL();
    this.FillUserDDL();
    this.FillCategoryDDL();
    this.DefaultCategoryId = environment.defaultbuysellCategoryId;
    // this.gettoday();

    ////debugger;
    this.FormPostBuySell = this._formbulider.group({

      buySellId: ['', ''],
      title: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      faxNumber: [''],
      addressStreet1: ['', ''],
      addressStreet2: ['', ''],
      countryId: ['1', ''],
      countryCodeContact: ['1', ''],
      ImageUrl: ['', ''],
      extImageUrl: ['', ''],
      stateId: [this.StateId, ''],
      cityId: [this.CityId, ''],
      stateName: [this.StateName, ''],
      cityName: [this.CityName, ''],
      zipCode: ['', Validators.pattern("^[0-9]*$")],
      postedFor: ['', Validators.required],
      postedName: ['null', ''],
      categoryId: [this.DefaultCategoryId, ''],
      categoryName: ['', ''],
      subCategoryId: ['', Validators.required],
      subCategoryName: ['', ''],
      contactPersonName: ['', Validators.required],
      contactNumber: ['', [Validators.required, Validators.minLength(10)]],
      description: ['', ''],
      expectedPrice: ['', ''],
      validTillDate: ['', ''],
      alternateContactNumber: [''],
      createdBy: ['', ''],
      isPaidAd: ['', ''],
      BuySellCount: [''],
      planId: ['']
    });
    this.displayFreeSection = true;
    this.tempPlanId = 0;
    this.remainingbuySellCount = 0;
    this.getbuySellDetailsById(this._route.snapshot.queryParams['buysellid']);
  }
  FillCountryDDL() {
    debugger;
    this._allCountry = this._commonService.CountryDDL();
    this.CountryId = "1";
    this._commonService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      this.states = data;
    });
    this._allState = this._commonService.StateDDL(this.CountryId);
    this._allCity = this._commonService.CityDDL(this.StateId);
  }
  FillUserDDL() {
    debugger;
    this._allUser = this._commonService.UserDDL();
    this._commonService.UserInboxMessageDDL().subscribe(data => {
      debugger;
      this.users = data;


    });

  }
  FillStateDDL(event: any) {

    debugger;
    if ((event.target.value == "0") || ((event.target.value == ""))) {
      this.FormPostBuySell.value.countryId = 0;
      //this._allState = this._commonService.StateDDL("0");
      var x = document.getElementsByClassName("ng-value-label");
      x[3].innerHTML = "--Select State--";
      x[4].innerHTML = "--Select City--";
      //var i;
      //for (i = 3; i < x.length; i++) {
      //  x[i].innerHTML = "";
      //}
      this.states = [];
      this.cities = [];

      console.log(this.states);
      this.FormPostBuySell.value.stateId = 0;
      this.FormPostBuySell.value.cityId = 0;
    }
    else {


      this.CountryId = event.countryId;

      this._allState = this._commonService.StateDDL("1");
      debugger;
      this._commonService.StateDDL("1").subscribe(data => {
        console.log(data);
        //debugger;

        var x = document.getElementsByClassName("ng-value-label");
        x[3].innerHTML = "--Select State--";
        x[4].innerHTML = "--Select City--";
        this.states = data;
      });

      //this._commonService.BUCityDDL("1").subscribe(data => {
      //  console.log(data);
      //  debugger;
      //  this.cities = data;
      //});
      // this._allState = this._commonService.StateDDL(this.CountryId);
      this.CountryName = event.target.options[this.CountryId].innerHTML;
      this.FormPostBuySell.countryName = this.CountryName;

    }

  }

  FillCityDDL(event: any) {
    debugger;
    var x = document.getElementsByClassName("ng-value-label");
    x[4].innerHTML = "--Select City--";
    if (event != 0) {
      this.StateId = event.stateId;
      this.StateName = event.stateName;
      this._allCity = this._commonService.BUCityDDL(this.StateId);
      //this.StateName = event.target.options[this.StateId].innerHTML;  
      this.FormPostBuySell.stateName = this.StateName;

      this._commonService.BUCityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
    }
    else {
      var x = document.getElementsByClassName("ng-value-label");
      this.cities = [];
    }

  }
  FillCityName(event: any) {
    this.CityId = event.cityId;
    this.CityName = event.cityName;
    // this.CityName=event.target.options[this.CityId].innerHTML;
    this.FormPostBuySell.cityName = this.CityName;

  }
  FillCategoryDDL() {
    debugger;
    this._allCategory = this._commonService.CategoryDDL();

    // this._commonService.CategoryDDL().subscribe(data => {
    //   debugger;
    //   this.categories = data;


    //  });

  }
  GetUserDetails(event: any) {

    debugger;

    this._userService.getUserById(event.userId).subscribe(data => {
      var userdata = JSON.parse(data._body);

      this._commonService.CityDDL(userdata.stateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });


      this.FillSubscriptionPlan(userdata.userId);
      this.countrycode = this.countryCodesNew;
      // debugger;
      if (this.action == "") {
        this.StateId = userdata.stateId;
        this.CityId = userdata.cityId;
        this.FormPostBuySell.patchValue({
          email: userdata.email,
          faxNumber: userdata.faxNumber,
          addressStreet1: userdata.addressStreet1,
          addressStreet2: userdata.addressStreet2,
          countryId: userdata.countryId,
          stateId: userdata.stateId.toString(),
          cityId: userdata.cityId.toString(),
          zipCode: userdata.zipCode,
          postedFor: userdata.userId.toString(),
          contactPersonName: userdata.firstName + " " + userdata.lastName,
          contactNumber: userdata.contactNumber,
          alternateContactNumber: userdata.alternateContactNumber,
          buySellId: "",
          title: "",
          stateName: "",
          cityName: "",
          postedName: "",
          categoryId: this.FormPostBuySell.value.categoryId,
          categoryName: "",
          subCategoryId: "",
          subCategoryName: "",
          countryCodeContact: "1",
          expectedPrice: "",
          description: "",
          validTillDate: "",
          ImageUrl: "",
          createdBy: "",
          isPaidAd: "",
          BuySellCount: 0,
        });
      }
      else if (this.action == "edit") {

        this.FormPostBuySell.patchValue({


          email: userdata.email,
          faxNumber: userdata.faxNumber,
          addressStreet1: userdata.addressStreet1,
          addressStreet2: userdata.addressStreet2,
          countryId: userdata.countryId,
          stateId: userdata.stateId,
          cityId: userdata.cityId,
          zipCode: userdata.zipCode,
          postedFor: userdata.userId.toString(),
          contactPersonName: userdata.firstName + " " + userdata.lastName,
          contactNumber: userdata.contactNumber,
          alternateContactNumber: userdata.alternateContactNumber,
          buySellId: this.FormPostBuySell.value.buySellId,
          title: this.FormPostBuySell.value.title,
          stateName: this.FormPostBuySell.value.stateName,
          cityName: this.FormPostBuySell.value.cityName,
          postedName: this.FormPostBuySell.value.postedName,
          categoryId: this.FormPostBuySell.value.categoryId,
          categoryName: this.FormPostBuySell.value.categoryName,
          subCategoryId: this.FormPostBuySell.value.subCategoryId,
          subCategoryName: this.FormPostBuySell.value.subCategoryName,

          countryCodeContact: "1",
          expectedPrice: this.FormPostBuySell.value.expectedPrice,
          servicesOffered: this.FormPostBuySell.value.servicesOffered,
          description: this.FormPostBuySell.value.description,
          validTillDate: this.FormPostBuySell.value.validTillDate,

          ImageUrl: this.FormPostBuySell.value.imageUrl,
          //  adImageUrl:this.FormPostAd.value.adImageUrl,  

          extImageUrl: this.FormPostBuySell.value.imageUrl,
          //  adextImageUrl:this.FormPostAd.value.adextImageUrl,
          createdBy: this.FormPostBuySell.value.createdBy,
          isPaidAd: this.FormPostBuySell.value.isPaidAd,
        });
      }
    });
  }
  getbuySellDetailsById(buySellId) {
    debugger;
    this._buySellService.getbuySellDetailsById(buySellId).subscribe(data => {

      this.action = "edit";
      this.saveButtonText = "Update";
      this.titleText = "Edit Buy/Sell Listing";
      this.titlecss = "nav-icon icon-tag";
      this.displayText = 'btn btn-sm btn-primary d-none';
      this.RadioPaid = true;
      this.showImageUrl = false;
      // var x = document.getElementById("postedFor");
      // x.style.backgroundColor = "#f5f5f5";
      // this.FormPostBuySell.controls['postedFor'].disable(true); 
      this.DefaultCategoryId = environment.defaultbuysellCategoryId;
      var x = document.getElementById("postedFor");
      x.style.backgroundColor = "#f5f5f5";
      this.FormPostBuySell.controls['postedFor'].disable(true);
      if (data.isPaidAd == true) {
        this.displayPaidSection = true;
        this.displayFreeSection = false;
        this.showextuploads = true;
        this.UploadType = "Premium";
        this.adTypePaid = true;
        this.adTypeFree = false;
        this.RadioPaid = false;
        this.RadioFree = true;
        this.isPaidAd = true;

      }
      else {
        this.displayPaidSection = false;
        this.displayFreeSection = true;
        this.ShowextImageUrl = true;
        this.UploadType = "Free";
        this.adTypePaid = false;
        this.adTypeFree = true;
        this.RadioPaid = true;
        this.RadioFree = false;
        this.isPaidAd = false;
      }
      this.renderExistingImages(data.imageUrl, data.buySellId, this.UploadType)
      //this.extImageUrl= this.commonimagepath+'BuySell/'+data.buySellId+'/Image/' + data.imageUrl;
      debugger;
      this._commonService.StateDDL(data.countryId).subscribe(data => {
        debugger
        console.log(data);
        this.states = data;

      });

      this._commonService.BUCityDDL(data.stateId).subscribe(data => {
        debugger;
        console.log(data);
        this.cities = data;
      });

      // this._commonService.SubCategoryDDL(data.categoryId).subscribe(data => {
      //   debugger;
      //   this.subcategories = data;

      // });

      this.FillSubscriptionPlan(data.postedFor);
      debugger;
      if (data.validTillDate !== null)
        this.validTill = data.validTillDate.split('T')[0];
      else
        this.validTill = this.minDate;
      if (data.countryCodeContact === null)
        data.countryCodeContact = "1";
      this.FormPostBuySell.value.subCategoryId = data.subCategoryId;
      debugger;
      this.FormPostBuySell.value.buySellId = data.buySellId;
      this.FormPostBuySell.patchValue({
        buySellId: data.buySellId,
        title: data.title,
        email: data.email,
        postedFor: data.postedFor.toString(),
        countryId: data.countryId,
        stateId: data.stateId.toString(),
        cityId: data.cityId.toString(),
        categoryId: data.categoryId.toString(),

        // validTillDate:this.validTill,
        faxNumber: data.faxNumber,
        addressStreet1: data.addressStreet1,
        addressStreet2: data.addressStreet2,

        stateName: data.stateName,
        cityName: data.cityName,
        zipCode: data.zipCode,
        tagLine: data.tagLine,

        postedName: data.postedForName,

        categoryName: data.categoryName,

        subCategoryName: data.subCategoryName,
        contactPersonName: data.contactPersonName,
        contactNumber: data.contactNumber,
        countryCodeContact: data.countryCodeContact.toString(),
        expectedPrice: data.expectedPrice,
        servicesOffered: data.servicesOffered,
        description: data.description,
        alternateContactNumber: data.alternateContactNumber,
        validTillDate: this.validTill,
        ImageUrl: data.imageUrl,
        extImageUrl: this.commonimagepath + 'BuySell/' + data.buySellId + '/Image/' + data.imageUrl,
        createdBy: data.createdBy,
        subCategoryId: data.subCategoryId.toString(),
        isPaidAd: data.isPaidAd
      });
      ////debugger;
      // this.gettoday();
      console.log(this.extImageUrl);
    });
  }
  selected(listingType: string) {
    debugger;
    if (listingType == "Free") {
      this.isPaidAd = false;
      this.displayFreeSection = true;
      this.displayPaidSection = false;
    }
    if (listingType == "Paid") {
      this.isPaidAd = true;
      this.displayPaidSection = true;
      this.displayFreeSection = false;
      this.showextuploads = false;
    }
  }
  renderExistingImages(imageUrls, buySellId, uploadType) {
    debugger;
    if (imageUrls !== null) {
      if (imageUrls !== "") {
        var extImages = imageUrls.split(',');
        if (extImages.length > 0) {
          if (uploadType == "Free") {
            for (let i = 0; i < extImages.length; i++) {
              this.extImageurls.push(this.commonimagepath + 'BuySell/' + buySellId + '/Free/' + extImages[i]);
              this.extImages.push(extImages[i]);
            }
          }
          else if (uploadType == "Premium") {
            for (let i = 0; i < extImages.length; i++) { ////debugger;
              this.extImageurls.push(this.commonimagepath + 'BuySell/' + buySellId + '/Premium/' + extImages[i]);
              this.extImages.push(extImages[i]);
              // this.displayExtImages = 'col-md-6';
            }
          }
          // this.imagepath+'Ads/'+adId+'/Logo/'+picurl;
        }
      }
      // else  
      // {
      //   if(uploadType=="Free")
      //   this.displayExtImages = 'col-md-6 d-none';
      //   if(uploadType=="Premium")
      //   this.displayExtLogo = 'col-md-6 d-none';

      // }

      //   }
      //   else  
      // {
      //   if(uploadType=="Images")
      //   this.displayExtImages = 'col-md-6 d-none';
      //   if(uploadType=="Logo")
      //   this.displayExtLogo = 'col-md-6 d-none';

      // }
    }
  }
  reset() { }
  get f() { return this.FormPostBuySell.controls; }
  // Insert Post
  postBuySell(): void {
    debugger;
    this.submitted = true;
    if (this.saveButtonText == "Save") {
      this.action = "";
    }
    // stop the process here if form is invalid

    if (this.FormPostBuySell.invalid) {
      this.toastr.warning("Mandatory fields missing. Please fill all mandatory fields.");
      return;
    }
    var x = document.getElementsByClassName("ng-value-label");
    this.PostedForName = x[0].innerHTML;
    this.StateName = x[3].innerHTML;
    this.CityName = x[4].innerHTML;
    this.FormPostBuySell.value.createdBy = this.loggedInUserId;
    this.FormPostBuySell.value.categoryName = this.categoryName;
    if (this.FormPostBuySell.value.subCategoryId == 1)
      this.FormPostBuySell.value.subCategoryName = "Buy";
    else
      this.FormPostBuySell.value.subCategoryName = "Sell";
    console.log(this.FormPostBuySell.value);
    if (this.action == "") {
      if (this.isPaidAd == true) {
        if (this.remainingbuySellCount - 1 < 0) {
          this.toastr.warning("Please buy a plan for buy/sell posting or select another plan.");
          return;
        }
        else {
          this.remainingbuySellCount = this.remainingbuySellCount - 1;
        }
        this.FormPostBuySell.value.BuySellCount = this.remainingbuySellCount;
        this.FormPostBuySell.value.planId = this.tempPlanId;

      }
      this.FormPostBuySell.value.isPaidAd = this.isPaidAd;
      this.FormPostBuySell.value.buySellId = 0;
      this.FormPostBuySell.value.imageUrl = this.imageresponse;
      this._buySellService.postBuySell(this.FormPostBuySell.value)
        .subscribe((response) => {
          console.log(response);
          if (response.status === 200) {
            if ((<any>response)._body === "true") {

              this.toastr.success('Buy/Sell listing posted successfully.');
              this._router.navigate(['/theme/managebuysell']);
            }
          }
        },
          (error) => {
            console.log(error);
            this.toastr.error('Problem with service. Please try again later!');
          }
        );
    }

    else if (this.action == "edit") {
      debugger;
      //this.FormPostBuySell.value.BuySellId = this.BuySellId;
      this.FormPostBuySell.value.categoryName = this.categoryName;
      this.FormPostBuySell.value.stateName = this.StateName;
      this.FormPostBuySell.value.cityName = this.CityName;
      this.FormPostBuySell.value.imageUrl = this.imageresponse;
      this.FormPostBuySell.value.BuySellCount = this.remainingbuySellCount;
      this.FormPostBuySell.value.planId = this.tempPlanId;
       this.FormPostBuySell.value.postedFor = this.postedFor;
      if (this.extImages.length > 0) {
        if (this.imageresponse === undefined)
          this.FormPostBuySell.value.imageUrl = this.extImages.join();
        else {
          var splitsEditImage = new String(this.imageresponse);
          var afterEditSplit = splitsEditImage.split(",");
          var count = afterEditSplit.length;
          this.FormPostBuySell.value.imageUrl = this.imageresponse + "," + this.extImages.join();
           var finalcount =count + this.extImages.length;
        }
          if(finalcount >5)
          {
            this.toastr.warning('Only 5 image  allowed to upload');
            count=0;
            return;
          }
      }
      else {
        if (this.imageresponse === undefined)
          this.FormPostBuySell.value.imageUrl = "";
        else {
          this.FormPostBuySell.value.imageUrl = this.imageresponse;
        }
      }
      this._buySellService.updatebuySellRecord(this.FormPostBuySell.value)
        .subscribe((response) => {
          console.log(response);
          if (response.status === 200) {
            if ((<any>response)._body === "true") {

              this.toastr.success('Buy/Sell listing updated successfully.');
              this._router.navigate(['/theme/managebuysell']);
            }
          }
        },
          (error) => {
            console.log(error);
            this.toastr.error('Problem with service. Please try again later!');
          }
        );
    }
  }
  subscriptionPlanDetails(event: any) {
    debugger;
    console.log(event.target.value);

    this.paymentPlanService.getPlanDetails(event.target.value).subscribe(data => {
      var planIdDetails = JSON.parse(data._body);
      console.log(planIdDetails);
      this.previousPlan = planIdDetails;
      this.tempPlanId = planIdDetails.planId;
      this.remainingbuySellCount = planIdDetails.buySellCount;
      this.FormPostBuySell.patchValue({
        adBuySellCount: planIdDetails.adBuySellCount,
        BuySellCount: planIdDetails.buySellCount
      });
    });
  }
  FillSubscriptionPlan(userId: number) {
    this._allPlan = this.paymentPlanService.UserPlanDDL(userId);
  }
  onextLogoUrldelete(i) {
    this.extImageUrl = null;
    this.showImageUrl = true;
    this.ShowextImageUrl = false;
    this.extImageurls.splice(i, 1);
    this.extImages.splice(i, 1);
  }
  onextextImageurldelete(i) {
    debugger;
    this.extImageurls.splice(i, 1);
    this.extImages.splice(i, 1);
    console.log(this.extImageurls);
    console.log(this.extImages);
  }
  subcategories = [
    { subCategoryId: "1", subCategoryName: "Buy" },
    { subCategoryId: "2", subCategoryName: "Sell" },

  ]
  NavigatetoManagebuysell() {
    this._router.navigate(['/theme/managebuysell']);
  }

}
