export class Buysell {

        public  BuySellId : Number;
        public  Title : string;
        public  ValidTillDate :string;
        public  Description : string;
        public  PostedForName : string;
        public  PostedFor: string;
        public  CategoryName : string;
        public  CategoryId :string;
        public  SubCategoryName: string;
        public  SubCategoryId :string;
        public  ImageUrl : string;
        public  ContactPersonName : string;
        public  CountryCodeContact: string;
        public CountryName:string;
        public  ContactNumber : string;
        public  AlternateContactNumber: string;
        public  FaxNumber : string;
        public  Email : string;
        public  Website : string;
        public  AddressStreet1 : string;
        public  AddressStreet2 : string;
        public  CountryId : string;
        public  StateName : string;
        public  StateId :string;
        public  CityName : string;
        public  CityId :string;
        public  ZipCode: string;
        public  AssignedTo : string;
        public  IsPremium : boolean;
        public  IsVisible :boolean;
        public  IsPaidAd :boolean;
        public  IsActive :boolean;
        public  CreatedDate: string; 
        public  CreatedBy : string;
        public  UpdatedDate : string;
        public UpdatedBy : string;
        public  ValidFromDate : string;
        public  VisitorCount : string;
        public  OtherCity : string;
        public buySelllStatusForDetailsView: string;

        public ExpectedPrice:number;


}
