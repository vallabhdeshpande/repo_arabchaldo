import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import {User} from './user';
import {HttpClient} from '@angular/common/http'; 
import { RequestOptions,Http,Response, Headers } from '@angular/http';
import {Managebanner} from './managebanner';
//import { from } from 'rxjs';
import { environment } from '../../../environments/environment';
import { UserInfo } from 'os';
import { request } from 'https';

@Injectable({
    providedIn: 'root'
  })

export class Managebannerservice 
{
  data;
  observable;
  cachedloggedInUserId;

  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });
  baseUrl = environment.baseUrl;

    constructor(private http: HttpClient,private _httpService: Http, private https: Http){ }

  FillBannerList(userId: number): Observable<any>
  {
    //debugger;
    if (this.cachedloggedInUserId != userId) {
      this.data = null;
      this.observable = null;
    }
    
    if (this.data) {
      console.log(' first if condition SA manage banner');
      return Observable.of(this.data);
    }
    else if (this.observable) {
      console.log('2nd if condition');
      console.log(this.observable);
      return this.observable;
    }
    else {
      //this.observable = this.http.get(this.baseUrl + 'BannerDetails/GetAllBanner?=' + userId, {
      //  observe: 'response'
      //})
      this.cachedloggedInUserId = userId;
      debugger;
      //this.observable = this._httpService.get(this.baseUrl + 'BannerDetails/GetAllBanner?=' + userId)
      this.observable = this.http.get(this.baseUrl + 'BannerDetails/GetAllBanner?=' + userId,
        { observe: 'response' })
        .map(response => {
          debugger;
          this.observable = null;
          console.log('fetch data from server');
          if (response.status === 400) {
            console.log('request failed');
            return 'request failed';
          }
          else if (response.status === 200) {
            console.log('request successful');

            this.data = response.body;
            console.log('last');
            console.log(this.data);
            return this.data;
          }
        });
      console.log('End');
      return this.observable;
    }
    
  }

  add(managebanner: Managebanner)
  {
    this.data = null;
    this.observable = null;
    let body = JSON.parse(JSON.stringify(managebanner));
    console.log(managebanner);
    this.data = null;
    this.observable = null;

    return this._httpService.post(this.baseUrl + 'BannerDetails', body, this.options);
    }
    
  getBannerList(userId: number): Observable<any>
  {
    // delete this comment 
    return this._httpService.get(this.baseUrl + 'BannerDetails/GetAllBanner?=' + userId);
  }
 
  deleteBanner(BannerId: number)
  {
    this.data = null;
    this.observable = null;
    return this._httpService.delete(this.baseUrl + 'BannerDetails/' + BannerId, this.options);
  }

  getBannerById(BannerId: number): Observable<any> 
    {
    return this._httpService.get(this.baseUrl +'BannerDetails/' + BannerId);
    
    } 

  //getBannerById( BannerId: bigint)
  //{
  //  debugger;
  //  return this._httpService.put(this.baseUrl + 'BannerDetails/'+ BannerId, Managebanner);
  //}

  getBanner(bannerId: number): Observable<any>
  {
    return this._httpService.get(this.baseUrl + 'BannerDetails/' + bannerId);
  }

  updateBanner(managebanner : Managebanner )
  {
    this.data = null;
    this.observable = null;
   //debugger;
   let body = JSON.parse(JSON.stringify(managebanner));
   //let headers = new Headers({ 'Content-Type': 'application/json' });
   //let options = new RequestOptions({ headers: headers });

    this.data = null;
    this.observable = null;

    return this._httpService.put(this.baseUrl + 'BannerDetails/' + managebanner.bannerId, body );
  }

  UserDDLForBanner(RoleId: number): Observable<User[]> 
  {
   return this.http.get<User[]>(this.baseUrl + 'common/UserDDLForBanner?RoleId='+ RoleId );
  }    

  UserDDL(): Observable<User[]>
  {
    return this.http.get<User[]>(this.baseUrl + 'common/UserDDLForThirdParty' );
  } 

  updatewithWorkflow(workflowdetails: string) 
  {
    //debugger
    this.data = null;
    this.observable = null;
    return this._httpService.delete(this.baseUrl + 'BannerDetails/' + workflowdetails, this.options);
  }
}


