import { Injectable, ɵɵresolveBody } from '@angular/core';
import { HttpClient, HttpHeaders,HttpClientModule } from "@angular/common/http";
import { RequestOptions,Http,Response, Headers } from '@angular/http';
import { Country } from './country.model';
import { State } from './state.model';
import { City } from './city.model';
import { User } from './user';
import { EventDetails } from './eventdetails.model';
import { environment } from '../../../environments/environment';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';

@Injectable({
  providedIn: 'root'
})
export class EventDetailsService
{
  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });
  baseUrl = environment.baseUrl;
  loggedInUserId = localStorage.getItem('userId');
  loggedInRole = localStorage.getItem('roleName');
  data;
  observable;
  cachedloggedInUserId;

  constructor(private _httpService: Http, private http: HttpClient) { }

  postEvent(postevent: EventDetails)
  {
    this.data = null;
    this.observable = null;
    // alert("hi");
    // alert(postad);
    console.log(postevent);
    let body = JSON.parse(JSON.stringify(postevent));
    console.log(body);
    //let headers = new Headers({ 'Content-Type': 'application/json' });
    //let options = new RequestOptions({ headers: headers });
    {
      return this._httpService.post(this.baseUrl + 'eventdetails', body, this.options);
      console.log(body);
    }
  }

  updatewithWorkflow(workflowdetails: string)
  {
    this.data = null;
    this.observable = null;
    debugger
    return this._httpService.delete(this.baseUrl + 'eventdetails/' + workflowdetails, this.options);
  }

  updateEvent(formData: EventDetails): Observable<any>
  {
    this.data = null;
    this.observable = null;
    debugger
    return this._httpService.put(this.baseUrl + 'eventdetails/' + formData.eventId, formData, this.options);
  }
 //deleteEvent(EventId : number){
 //  return this._httpService.delete(this.baseUrl + 'eventdetails/' + EventId, this.options);
 // }

  getEventList(): Observable<any> 
  {
    //debugger;
    this.loggedInUserId = localStorage.getItem('userId');
    this.loggedInRole = localStorage.getItem('roleName');
    if (this.cachedloggedInUserId != this.loggedInUserId) {
      this.data = null;
      this.observable = null;
    }
    if (this.data) {
      console.log(' first if condition SA manage events');
      return Observable.of(this.data);
    }
    else if (this.observable) {
      console.log('2nd if condition SA manage events');
      return this.observable;
    }
    else
    {
      this.cachedloggedInUserId = this.loggedInUserId;
      this.observable = this.http.get(this.baseUrl + 'eventdetails/GetAllEvent?userInfo=' + this.loggedInUserId + "," + this.loggedInRole,
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          console.log('fetch data from server for SA manage events ');
          if (response.status === 400) {
            console.log('request failed SA manage events');
            return 'request failed';
          }
          else if (response.status === 200) {
            console.log('request successful SA manage events');
            this.data = response.body;
            console.log(this.data);
            return this.data;
          }
        });
      console.log('End');
      return this.observable;
    }

    //return this.http.get(this.baseUrl + 'eventdetails/GetAllEvent?=' + userId);
  }

  getEventDetailsById(EventId : number): Observable<any> 
  {
    debugger;
    return this.http.get(this.baseUrl + 'eventdetails/GetEventDetails?eventId=' + EventId);
  }
  UserDDL(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl +'userdetails');
  }
  
  CountryDDL(): Observable<Country[]>
  {   
    return this.http.get<Country[]>(this.baseUrl + 'common/CountryData');
  }
  StateDDL(CountryId: string): Observable<State[]>
  {
    return this.http.get<State[]>(this.baseUrl + 'common/StateData?CountryId='+CountryId);
  }
  CityDDL(StateId: string): Observable<City[]>
  {
    return this.http.get<City[]>(this.baseUrl + 'common/CityData?StateId='+StateId);
  }
}
