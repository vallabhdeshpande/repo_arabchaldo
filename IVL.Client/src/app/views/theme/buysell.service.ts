import { Injectable,ɵɵresolveBody } from '@angular/core';
import { HttpClient, HttpHeaders,HttpClientModule } from "@angular/common/http";
import { RequestOptions,Http,Response, Headers } from '@angular/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs-compat/Observable';
import { Buysell } from './buysell.model';

@Injectable({
  providedIn: 'root'
})
export class BuysellService {
  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });
  baseUrl = environment.baseUrl;
  loggedInUserId = localStorage.getItem('userId');
  loggedInRole = localStorage.getItem('roleName');
  observable;
  getBuySellListData;
  cachedloggedInUserId;

  constructor(private _httpService: Http, private http: HttpClient) { }
  getBuySellList(): Observable<any>
  {
    //debugger;
    this.loggedInUserId = localStorage.getItem('userId');
    if (this.cachedloggedInUserId != this.loggedInUserId) {
      this.getBuySellListData = null;
      this.observable = null;
    }

    this.loggedInUserId = localStorage.getItem('userId');
    this.loggedInRole = localStorage.getItem('roleName');
    if (this.getBuySellListData) {
      console.log(' first if condition SA Ad list');
      return Observable.of(this.getBuySellListData);
    }
    //else if (this.observable)
    //{
    //  console.log('2nd if condition SA manage events');
    //  return this.observable;
    //}
    else {
      this.cachedloggedInUserId = this.loggedInUserId;
      this.observable = this.http.get(this.baseUrl + 'buyselldetails/GetAllBuySellDetails?userInfo=' + this.loggedInUserId + "," + this.loggedInRole,
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          console.log('fetch data from server for SA Ad list ');
          if (response.status === 400) {
            console.log('request failed SA Ad list');
            return 'request failed';
          }
          else if (response.status === 200) {
            console.log('request successful SA Ad list');
            this.getBuySellListData = response.body;
            console.log(this.getBuySellListData);
            return this.getBuySellListData;
          }
        });
      return this.observable;
    }   
  }

  postBuySell(postbuysell: Buysell) {
    this.getBuySellListData = null;
    this.observable = null;
    console.log(postbuysell);
    let body = JSON.parse(JSON.stringify(postbuysell));
    console.log(body);
    {
      return this._httpService.post(this.baseUrl + 'BuySellDetails', body, this.options);
      console.log(body);
    }
  }

  getbuySellDetailsById(buysellId : number): Observable<any> {
    return this.http.get(this.baseUrl + 'buyselldetails/GetbuySellDetailsById?buysellId='+buysellId);
  }

  updatewithWorkflow(workflowdetails: string)
  {
    this.getBuySellListData = null;
    this.observable = null;
    //debugger
    return this._httpService.delete(this.baseUrl + 'buyselldetails/' + workflowdetails, this.options);
  }

  updatebuySellRecord(formData: any): Observable<any>
  {
    debugger
    this.getBuySellListData = null;
    this.observable = null;
    return this._httpService.put(this.baseUrl + 'buyselldetails/' + formData.buySellId, formData, this.options);
  }

  deleteAd(BuySellId: number)
  {
    this.getBuySellListData = null;
    this.observable = null;
    return this._httpService.delete(this.baseUrl + 'buyselldetails/' + BuySellId, this.options);
  }

}
