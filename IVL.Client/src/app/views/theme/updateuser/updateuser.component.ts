import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService } from '../user.service'
import { User } from '../user';
import {first} from "rxjs/operators";
import {Router, ActivatedRoute} from "@angular/router";
import { Observable } from 'rxjs';
import { City } from '../city.model';
import { State } from '../state.model';
import { Country } from '../country.model';
import { HttpClient, HttpHeaders,HttpClientModule } from "@angular/common/http";
import { AdDetailsService } from '../addetails.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-updateuser',
  templateUrl: './updateuser.component.html',
  styleUrls: ['./updateuser.component.scss']
})

export class UpdateuserComponent implements OnInit {
  UserId: string = "0";
  CountryId: string = "0";
  StateId: string = "0";
  CityId: string = "0";
  CategoryId: string = "0";
  SubCategory: string = "0";
  //roleId: string="0";
  editForm: any;
  user: User;
  isLoadingResults = false;
  state: any;
  public _allUserRole: Observable<User[]>;
  public _allCountry: Observable<Country[]>;
  public _allState: Observable<State[]>;
  public _allCity: Observable<City[]>;
  submitted = false;
  roleName: string = "";
  //FormUserAdd: any; 

  constructor(private toastr: ToastrService, private formBuilder: FormBuilder, private router: Router, private AdDetailsService: AdDetailsService, private route: ActivatedRoute, private userService: UserService, private activatedRoute: ActivatedRoute
  ) { }
  ngOnInit() {
    this.FillCountryDDL();
    this.FillUserRoleDDL();
    this.editForm = this.formBuilder.group({
      'userId': ['', Validators.required],
      'firstName': ['', Validators.required],
      'lastName': ['', Validators.required],
      'gender': ['', Validators.required],
      //'roleId': ['', Validators.required],
      'roleName': ['', Validators.required],
      'profilePicUrl': ['', Validators.required],
      'contactNumber': ['', Validators.required],
      'alternateContactNumber': ['', Validators.required],
      'email': ['', Validators.required],
      'faxNumber': ['', Validators.required],
      'addressStreet1': ['', Validators.required],
      'addressStreet2': ['', Validators.required],
      'countryId': ['', Validators.required],
      'stateId': ['', Validators.required],
      'cityId': ['', Validators.required],
      'zipCode': ['', Validators.required]


    });
    this.getProduct(this.route.snapshot.queryParams['userId']);
  }
  get f() { return this.editForm.controls; }
  getProduct(userId) {
    this.userService.getUserById(userId).subscribe(data => {
      var data = JSON.parse(data._body);
      console.log(data);
      this._allState = this.AdDetailsService.StateDDL(data.countryId);
      this._allCity = this.AdDetailsService.CityDDL(data.stateId);
      var genderreceived;
      if (data.gender == 1)
        genderreceived = "Male";
      else
        genderreceived = "Female";
      this.editForm.setValue({
        'userId': userId,
        'firstName': data.firstName,
        'lastName': data.lastName,
        'gender': genderreceived,
        // 'roleId': data.roleId,
        'roleName': data.roleName,
        'profilePicUrl': data.profilePicUrl,
        'contactNumber': data.contactNumber,
        'alternateContactNumber': data.alternateContactNumber,
        'email': data.email,
        'faxNumber': data.faxNumber,
        'addressStreet1': data.addressStreet1,
        'addressStreet2': data.addressStreet2,
        'countryId': data.countryId,
        'stateId': data.stateId,
        'cityId': data.cityId,
        'zipCode': data.zipCode

      });
    });
  }
  uploadFinished(event: any) { }
  FillUserRoleDDL() {
    this.roleName = localStorage.getItem('roleName');
    this._allUserRole = this.userService.UserDDLByRole(this.roleName);

  }
  FillCountryDDL() {
    this._allCountry = this.AdDetailsService.CountryDDL();

  }
  FillStateDDL(event: any) {
    this.CountryId = event.target.value;

    this._allState = this.AdDetailsService.StateDDL(this.CountryId);
  }
  FillCityDDL(event: any) {
    this.StateId = event.target.value;
    this._allCity = this.AdDetailsService.CityDDL(this.StateId);
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    // if (this.editForm.invalid) {
    //     return;
    // }
    console.log(this.editForm.value);
    if (this.editForm.value.gender == "Male")
      this.editForm.value.gender = 1;
    else
      this.editForm.value.gender = 2;
    this.userService.updateUser(this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
          if (data.status === 200) {
            console.log(data);
            this.toastr.success('User updated successfully.');
            this.router.navigate(['/theme/manageuser']);
          } else {
            this.toastr.info(data.message);
          }
        },
        error => {
          this.toastr.error(error);
        });
  }

}
