import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Country,State,City,category,Subcategory,Roles,CountryCodes } from '../../shared/common.model';
import { User } from '../user';
import { UserService } from '../user.service';
import { EventDetails} from '../eventdetails.model';
import { EventDetailsService } from '../eventdetails.service';
import { CommonService } from '../../shared/common.service';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { environment } from '../../../../environments/environment';
import { element } from 'protractor';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Paymentplan } from '../paymentplan';
import { Paymentplanservice } from '../paymentplanservice';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-updateevent',
  templateUrl: './updateevent.component.html',
  styleUrls: ['./updateevent.component.scss'],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }]
})




export class UpdateeventComponent implements OnInit {
  public _allUser: Observable<User[]>;
  public _allCountry: Observable<Country[]>;
  public _allCountryCodes: Observable<CountryCodes[]>;
  public _allState: Observable<State[]>;
  public _allCity: Observable<City[]>;
  public _allCategory: Observable<category[]>;
  categoryId: string = "0";
  postedFor: string = "0";
  CountryId: string = "0";
  StateId: string = "0";
  CityId: string = "0";
  logopath: string[];
  imagepath: any[];
  logoUrl: string = "";
  imageUrl: string = "";
  ABCD: string[];
  states: any[];
  cities: any[];
  users: any[];
  categories: any[];
  StateName: string = "";
  CityName: string = "";
  //dvImage: string[];
  FormPostEvent: any;
  actionButton;
  action = "";
  myData = null;
  formData = null;
  msgs = null;
  public imagefilename: string[];
  public logofilename: string[];
  EventPost: EventDetails[];
  eventPost = new EventDetails();
  statusMessage: string;
  saveButtonText: string = "Save";
  displayText = 'btn btn-sm btn-primary';
  displayExtLogo = 'col-md-6 d-none';
  displayNewLogo = 'col-md-6';
  displayExtImages = 'col-md-6 d-none';
  displayExtUploads = 'form-bg-section d-none';
  maxDate: string;
  validTill: string;
  categoryName: string;
  extLogoUrls = [];
  extLogos = [];
  isPaidAd: boolean = false;
  displayPaidSection = 'col-md-12 d-none';
  //resetButtonStyle:string="block";
  public imageresponse: string;
  public logoresponse: string[];
  FormSubcategoryAdd: any;
  addbutton: HTMLElement;
  serverpath = environment.serverpath;
  commonimagepath = environment.imagepath;
  submitted = false;
  Id: number;
  eventimage: string;
  loggedInUserId: string;
  subscription: any;
  countryCodeList: Array<any> = [];
  datePlaceholder: string = "yyyy-mm-dd";
  public _allPlan: Observable<Paymentplan[]>;
  public previousPlan: Paymentplan;
  remainingeventImage: number;
  tempImageUrl: any;
  minDateModel: NgbDateStruct;
  modelValidFrom: Date = null;
  ValidFromDate: NgbDateStruct;
  validFrom: string = "";
  RadioFree: boolean = false;
  RadioPaid: boolean = false;
  LoggedInUserId: string;
  minDate: string;
  adTypePaid: boolean = false;
  adTypeFree: boolean = true;

  tempPlanId: number;
  constructor(private toastr: ToastrService, private _eventDetailsService: EventDetailsService, private _userService: UserService,
    private paymentPlanService: Paymentplanservice, private _commonService: CommonService,
    private _http: HttpClient, private _router: Router, private _formbulider: FormBuilder, private route: ActivatedRoute, @Inject(DOCUMENT) document, private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private config: NgbDatepickerConfig) {

  }
  GetUserDetails(event: any) { }
  ngAfterViewInit() {
    document.getElementById("eventDate").setAttribute("min", this.maxDate);
    document.getElementById('#SaveEvent').innerHTML = this.saveButtonText;
  }

  selectToday() {
    this.minDateModel = this.calendar.getToday();

  }
  onvalidFromDateSelect(event: any) {
    debugger;
    this.ValidFromDate = event;
    //this.minValidTillDate = event;
    this.validFrom = this.modelValidFrom.toJSON().split('T')[0];
    this.validTill = this.validFrom;
    //var abc = this.ValidFromDate.toJSON().split('T')[0];

  }
  gettoday() {
    this.maxDate = new Date().toJSON().split('T')[0];
  }

  FillSubCategoryDDL(event: any) { }
  public uploadFinishedLogo = (event) => {
    this.logoresponse = event;
    //this.displayNewLogo = 'col-md-6 d-none';
  }

  ngOnInit() {
    this.loggedInUserId = localStorage.getItem('userId');
    this.FillCategoryDDL();
    this.FillCountryDDL();
    this.FillUserDDL();
    // this.gettoday();
    this.selectToday();
    //debugger;
    this.FormPostEvent = this._formbulider.group({
      eventId: ['', ''],
      title: ['', [Validators.required, Validators.pattern(".*\\S.*[a-zA-z ]")]],
      email: ['', [Validators.required, Validators.email]],
      faxNumber: ['', Validators.pattern("^[0-9]*$")],
      addressStreet1: ['', ''],
      addressStreet2: ['', ''],
      countryId: ['1', ''],
      countryCodeContact: ['1', ''],
      eventDate: [this.validFrom, Validators.required],
      stateId: [this.StateId, ''],
      cityId: [this.CityId, ''],
      zipCode: ['', Validators.pattern("^[0-9]*$")],
      postedFor: ['', Validators.required],
      postedName: ['null', ''],
      contactPersonName: ['', [Validators.required, Validators.pattern(".*\\S.*[a-zA-z ]")]],
      contactNumber: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      website: ['', ''],
      description: ['', ''],
      categoryId: ['', Validators.required],
      imageUrl: [],
      alternateContactNumber: ['', [Validators.pattern("^[0-9]*$")]],
      createdBy: ['', ''],
      isPaidAd: ['', ''],
      eventImageCount: [''],

    });

    this.getEventDetailsById(this.route.snapshot.queryParams['id']);
  }

  get f() { return this.FormPostEvent.controls; }

  postEvent(): void {

    debugger;
    this.submitted = true;

    this.FormPostEvent.value.imageUrl = this.logoresponse;
    this.FormPostEvent.value.createdBy = this.loggedInUserId;
    this.FormPostEvent.value.isPaidAd = this.isPaidAd;
    if (this.previousPlan == undefined) {
      this.remainingeventImage = 0;
    }

    if (this.extLogos.length > 0) {
      if (this.logoresponse === undefined || this.logoresponse === null) {
        this.logoresponse = this.extLogos.slice();
        this.eventimage = this.logoresponse[0];
        this.FormPostEvent.value.imageUrl = this.eventimage;
      }
    }

    if (this.FormPostEvent.value.imageUrl != this.logoresponse) {
      this.FormPostEvent.value.imageUrl = this.logoresponse;
    }
    if (this.isPaidAd == true) {

      if (this.FormPostEvent.value.imageUrl == null || this.FormPostEvent.value.imageUrl == undefined) {
        this.toastr.warning("Event image missing. Please upload an image.");
        return;
      }

      if ((this.FormPostEvent.value.imageUrl != null) && (this.remainingeventImage == 0)
        && (this.FormPostEvent.value.imageUrl != this.tempImageUrl)) {
        this.toastr.warning('Event subscription exausted.');
        return;
      }
    }
    else {
      this.FormPostEvent.value.planId = null;
      this.FormPostEvent.value.imageUrl = null;
    }

    //if (this.FormPostEvent.invalid) 
    //{
    //   return;
    //}

    console.log(this.FormPostEvent.value)

    this.eventPost.ImageUrl = this.FormPostEvent.value.imageUrl;
    this.eventPost.eventId = this.FormPostEvent.value.eventId;
    this.eventPost.Title = this.FormPostEvent.value.title;
    this.eventPost.Description = this.FormPostEvent.value.description;
    this.eventPost.Website = this.FormPostEvent.value.website;
    this.eventPost.AddressStreet1 = this.FormPostEvent.value.addressStreet1;
    this.eventPost.AddressStreet2 = this.FormPostEvent.value.addressStreet2;
    if (this.FormPostEvent.value.countryId === "") {
      this.eventPost.CountryId = '0';
      this.eventPost.StateId = '0';
      this.eventPost.CityId = '0';
    }
    else {
      this.eventPost.CountryId = this.FormPostEvent.value.countryId;
      this.eventPost.StateId = this.FormPostEvent.value.stateId;
      this.eventPost.CityId = this.FormPostEvent.value.cityId;
    }
    this.eventPost.ZipCode = this.FormPostEvent.value.zipCode;
    this.eventPost.PostedFor = this.FormPostEvent.value.postedFor;
    this.eventPost.PostedForName = this.FormPostEvent.value.postedName;
    //this.eventPost.CityName = this.FormPostEvent.value.cityId;
    this.eventPost.EventDate = this.FormPostEvent.value.eventDate;
    this.eventPost.CategoryId = this.FormPostEvent.value.categoryId;
    this.eventPost.AlternateContactNumber = this.FormPostEvent.value.alternateContactNumber;
    this.eventPost.ContactNumber = this.FormPostEvent.value.contactNumber;
    this.eventPost.ContactPersonName = this.FormPostEvent.value.contactPersonName;
    this.eventPost.FaxNumber = this.FormPostEvent.value.faxNumber;
    this.eventPost.createdBy = this.loggedInUserId;
    this.eventPost.Email = this.FormPostEvent.value.email;

    this.eventPost.CountryCodeContact = this.FormPostEvent.value.countryCodeContact;


    if ((this.previousPlan != undefined) && (this.FormPostEvent.value.imageUrl != this.tempImageUrl)) {
      this.eventPost.EventImageCount = this.FormPostEvent.value.eventImageCount;
      this.eventPost.PlanId = this.previousPlan.planId;
    }

    this._eventDetailsService.updateEvent(this.eventPost)
      .pipe(first())
      .subscribe(
        data => {
          if (data.status === 200) {
            console.log(data);
            this.toastr.success('Event updated successfully.');
            this._router.navigate(['/theme/manageevent']);
          } else {
            this.toastr.info(data.message);
          }
        },
        error => {
          this.toastr.error(error);
        });

  }

  renderExistingImages(imageUrls, eventId, uploadType) {
    debugger;
    if (imageUrls !== "") {
      var extImages = imageUrls.split(',');
      if (extImages.length > 0) {
        if (uploadType == "Images") {
          for (let i = 0; i < extImages.length; i++) {
            this.extLogoUrls.push(this.commonimagepath + 'Events/' + eventId + '/Image/' + extImages[i]);
            this.extLogos.push(extImages[i]);
            this.displayExtLogo = 'col-md-6';
          }
        }

      }

    }
  }

  onextLogoUrldelete(i) {
    this.extLogoUrls.splice(i, 1);
    this.extLogos.pop();
    this.displayExtLogo = 'col-md-6 d-none';
    this.displayNewLogo = 'col-md-6';
  }

  NavigatetoManageEvent() {
    this._router.navigate(['/theme/manageevent']);
  }

  getEventDetailsById(id) {
    debugger;
    this._eventDetailsService.getEventDetailsById(id).subscribe(data => {
      console.log(data);
      this.action = "edit";
      this.saveButtonText = "Update";
      this.displayText = 'btn btn-sm btn-primary d-none';

      this._userService.getUserById(data.postedFor).subscribe(userdetail => {
        var userdetail = JSON.parse(userdetail._body);

        console.log(userdetail);
        this.FillSubscriptionPlan(userdetail.userId);

        // Ad Images Retrieval
        this.displayExtUploads = 'form-bg-section';
        if (data.imageUrl !== null) {
          this.renderExistingImages(data.imageUrl, data.eventId, "Images");
          this.displayNewLogo = 'col-md-6 d-none';
        }
        else {
          this.displayExtImages = 'col-md-6 d-none';
        }



        // Ad Logo Retrieval
        //   if(data.adLogoUrl !==null)      
        //   this.renderExistingImages(data.adLogoUrl,data.eventId,"Logo");
        //  else
        //  this.displayExtLogo = 'col-md-6 d-none';
        this.FormPostEvent.value.isPaidAd = this.isPaidAd;

        //if (this.isPaidAd == true) {
        //  if (this.FormPostEvent.value.imageUrl == null || this.FormPostEvent.value.imageUrl == undefined) {
        //    this.toastr.warning("Event image missing. Please upload an image.");
        //    return;
        //  }
        //  if (this.FormPostEvent.value.imageUrl != null && this.previousPlan == undefined) {
        //    this.toastr.warning("please select/buy a subscription plan to post a event ad.");
        //    return;
        //  }
        //  if ((this.FormPostEvent.value.imageUrl != null) && (this.remainingeventImage == 0)) {
        //    this.toastr.warning('Event subscription exausted.');
        //    return;
        //  }

        //}

        //else {
        //  this.FormPostEvent.value.planId = null;
        //  this.FormPostEvent.value.imageUrl = null;

        //}
        if (data.isPaidAd == true) {
          // Ad Images Retrieval
          this.displayExtUploads = 'form-bg-section';
          if (data.imageUrl !== null) {
            this.selected("Paid");
            this.adTypePaid = true;
            this.adTypeFree = false;
            this.RadioFree = false;
            this.RadioPaid = true;

          }
          else
            this.displayExtImages = 'col-md-6 d-none';

          // Ad Logo Retrieval
          if (data.adLogoUrl !== null) {
            this.selected("Paid");
            this.adTypePaid = true;
            this.adTypeFree = false;
            this.RadioFree = true;
            this.RadioPaid = false;

          }
          else {
            this.displayExtLogo = 'col-md-6 d-none';
            this.RadioFree = true;
          }
        }
        else {
          this.RadioPaid = true;
        }


        if (data.imageUrl === null)
          this.displayExtUploads = 'form-bg-section d-none';
        //this.resetButtonStyle="none";
        //  this.resetButtonStyle="{display:none}";
        this._commonService.StateDDL(data.countryId).subscribe(data => {
          //debugger
          console.log(data);
          this.states = data;

        });

        this._commonService.CityDDL(data.stateId).subscribe(data => {
          //debugger;
          console.log(data);
          this.cities = data;
        });

        this._commonService.CategoryDDL().subscribe(data => {
          // debugger;
          this.categories = data;


        });
        this.countrycode = [

          { countryCodeContact: "1", name: "USA (+1)" },
          { countryCodeContact: "44", name: "UK (+44)" },
          { countryCodeContact: "213", name: "Algeria (+213)" },
          { countryCodeContact: "376", name: "Andorra (+376)" },
          { countryCodeContact: "244", name: "Angola (+244)" },
          { countryCodeContact: "1264", name: "Anguilla (+1264)" },
          { countryCodeContact: "1268", name: "Antigua &amp; Barbuda (+1268)" },
          { countryCodeContact: "54", name: "Argentina (+54)" },
          { countryCodeContact: "374", name: "Armenia (+374)" },
          { countryCodeContact: "297", name: "Aruba (+297)" },
          { countryCodeContact: "61", name: "Australia (+61)" },
          { countryCodeContact: "43", name: "Austria (+43)" },
          { countryCodeContact: "994", name: "Azerbaijan (+994)" },
          { countryCodeContact: "1242", name: "Bahamas (+1242)" },
          { countryCodeContact: "973", name: "Bahrain (+973)" },
          { countryCodeContact: "880", name: "Bangladesh (+880)" },
          { countryCodeContact: "1246", name: "Barbados (+1246)" },
          { countryCodeContact: "375", name: "Belarus (+375)" },
          { countryCodeContact: "32", name: "Belgium (+32)" },
          { countryCodeContact: "501", name: "Belize (+501)" },
          { countryCodeContact: "229", name: "Benin (+229)" },
          { countryCodeContact: "1441", name: "Bermuda (+1441)" },
          { countryCodeContact: "975", name: "Bhutan (+975)" },
          { countryCodeContact: "591", name: "Bolivia (+591)" },
          { countryCodeContact: "387", name: "Bosnia Herzegovina (+387)" },
          { countryCodeContact: "267", name: "Botswana (+267)" },
          { countryCodeContact: "55", name: "Brazil (+55)" },
          { countryCodeContact: "673", name: "Brunei (+673)" },
          { countryCodeContact: "359", name: "Bulgaria (+359)" },
          { countryCodeContact: "226", name: "Burkina Faso (+226)" },
          { countryCodeContact: "257", name: "Burundi (+257)" },
          { countryCodeContact: "855", name: "Cambodia (+855)" },
          { countryCodeContact: "237", name: "Cameroon (+237)" },
          { countryCodeContact: "1", name: "Canada (+1)" },
          { countryCodeContact: "238", name: "Cape Verde Islands (+238)" },
          { countryCodeContact: "1345", name: "Cayman Islands (+1345)" },
          { countryCodeContact: "236", name: "Central African Republic (+236)" },
          { countryCodeContact: "56", name: "Chile (+56)" },
          { countryCodeContact: "86", name: "China (+86)" },
          { countryCodeContact: "57", name: "Colombia (+57)" },
          { countryCodeContact: "269", name: "Comoros (+269)" },
          { countryCodeContact: "242", name: "Congo (+242)" },
          { countryCodeContact: "682", name: "Cook Islands (+682)" },
          { countryCodeContact: "506", name: "Costa Rica (+506)" },
          { countryCodeContact: "385", name: "Croatia (+385)" },
          { countryCodeContact: "53", name: "Cuba (+53)" },
          { countryCodeContact: "90392", name: "Cyprus North (+90392)" },
          { countryCodeContact: "357", name: "Cyprus South (+357)" },
          { countryCodeContact: "42", name: "Czech Republic (+42)" },
          { countryCodeContact: "45", name: "Denmark (+45)" },
          { countryCodeContact: "253", name: "Djibouti (+253)" },
          { countryCodeContact: "1809", name: "Dominica (+1809)" },
          { countryCodeContact: "1809", name: "Dominican Republic (+1809)" },
          { countryCodeContact: "593", name: "Ecuador (+593)" },
          { countryCodeContact: "20", name: "Egypt (+20)" },
          { countryCodeContact: "503", name: "El Salvador (+503)" },
          { countryCodeContact: "240", name: "Equatorial Guinea (+240)" },
          { countryCodeContact: "291", name: "Eritrea (+291)" },
          { countryCodeContact: "372", name: "Estonia (+372)" },
          { countryCodeContact: "251", name: "Ethiopia (+251)" },
          { countryCodeContact: "500", name: "Falkland Islands (+500)" },
          { countryCodeContact: "298", name: "Faroe Islands (+298)" },
          { countryCodeContact: "679", name: "Fiji (+679)" },
          { countryCodeContact: "358", name: "Finland (+358)" },
          { countryCodeContact: "33", name: "France (+33)" },
          { countryCodeContact: "594", name: "French Guiana (+594)" },
          { countryCodeContact: "689", name: "French Polynesia (+689)" },
          { countryCodeContact: "241", name: "Gabon (+241)" },
          { countryCodeContact: "220", name: "Gambia (+220)" },
          { countryCodeContact: "7880", name: "Georgia (+7880)" },
          { countryCodeContact: "49", name: "Germany (+49)" },
          { countryCodeContact: "233", name: "Ghana (+233)" },
          { countryCodeContact: "350", name: "Gibraltar (+350)" },
          { countryCodeContact: "30", name: "Greece (+30)" },
          { countryCodeContact: "299", name: "Greenland (+299)" },
          { countryCodeContact: "1473", name: "Grenada (+1473)" },
          { countryCodeContact: "590", name: "Guadeloupe (+590)" },
          { countryCodeContact: "671", name: "Guam (+671)" },
          { countryCodeContact: "502", name: "Guatemala (+502)" },
          { countryCodeContact: "224", name: "Guinea (+224)" },
          { countryCodeContact: "245", name: "Guinea - Bissau (+245)" },
          { countryCodeContact: "592", name: "Guyana (+592)" },
          { countryCodeContact: "509", name: "Haiti (+509)" },
          { countryCodeContact: "504", name: "Honduras (+504)" },
          { countryCodeContact: "852", name: "Hong Kong (+852)" },
          { countryCodeContact: "36", name: "Hungary (+36)" },
          { countryCodeContact: "354", name: "Iceland (+354)" },
          { countryCodeContact: "91", name: "India (+91)" },
          { countryCodeContact: "62", name: "Indonesia (+62)" },
          { countryCodeContact: "98", name: "Iran (+98)" },
          { countryCodeContact: "964", name: "Iraq (+964)" },
          { countryCodeContact: "353", name: "Ireland (+353)" },
          { countryCodeContact: "972", name: "Israel (+972)" },
          { countryCodeContact: "39", name: "Italy (+39)" },
          { countryCodeContact: "1876", name: "Jamaica (+1876)" },
          { countryCodeContact: "81", name: "Japan (+81)" },
          { countryCodeContact: "962", name: "Jordan (+962)" },
          { countryCodeContact: "7", name: "Kazakhstan (+7)" },
          { countryCodeContact: "254", name: "Kenya (+254)" },
          { countryCodeContact: "686", name: "Kiribati (+686)" },
          { countryCodeContact: "850", name: "Korea North (+850)" },
          { countryCodeContact: "82", name: "Korea South (+82)" },
          { countryCodeContact: "965", name: "Kuwait (+965)" },
          { countryCodeContact: "996", name: "Kyrgyzstan (+996)" },
          { countryCodeContact: "856", name: "Laos (+856)" },
          { countryCodeContact: "371", name: "Latvia (+371)" },
          { countryCodeContact: "961", name: "Lebanon (+961)" },
          { countryCodeContact: "266", name: "Lesotho (+266)" },
          { countryCodeContact: "231", name: "Liberia (+231)" },
          { countryCodeContact: "218", name: "Libya (+218)" },
          { countryCodeContact: "417", name: "Liechtenstein (+417)" },
          { countryCodeContact: "370", name: "Lithuania (+370)" },
          { countryCodeContact: "352", name: "Luxembourg (+352)" },
          { countryCodeContact: "853", name: "Macao (+853)" },
          { countryCodeContact: "389", name: "Macedonia (+389)" },
          { countryCodeContact: "261", name: "Madagascar (+261)" },
          { countryCodeContact: "265", name: "Malawi (+265)" },
          { countryCodeContact: "60", name: "Malaysia (+60)" },
          { countryCodeContact: "960", name: "Maldives (+960)" },
          { countryCodeContact: "223", name: "Mali (+223)" },
          { countryCodeContact: "356", name: "Malta (+356)" },
          { countryCodeContact: "692", name: "Marshall Islands (+692)" },
          { countryCodeContact: "596", name: "Martinique (+596)" },
          { countryCodeContact: "222", name: "Mauritania (+222)" },
          { countryCodeContact: "269", name: "Mayotte (+269)" },
          { countryCodeContact: "52", name: "Mexico (+52)" },
          { countryCodeContact: "691", name: "Micronesia (+691)" },
          { countryCodeContact: "373", name: "Moldova (+373)" },
          { countryCodeContact: "377", name: "Monaco (+377)" },
          { countryCodeContact: "976", name: "Mongolia (+976)" },
          { countryCodeContact: "1664", name: "Montserrat (+1664)" },
          { countryCodeContact: "212", name: "Morocco (+212)" },
          { countryCodeContact: "258", name: "Mozambique (+258)" },
          { countryCodeContact: "95", name: "Myanmar (+95)" },
          { countryCodeContact: "264", name: "Namibia (+264)" },
          { countryCodeContact: "674", name: "Nauru (+674)" },
          { countryCodeContact: "977", name: "Nepal (+977)" },
          { countryCodeContact: "31", name: "Netherlands (+31)" },
          { countryCodeContact: "687", name: "New Caledonia (+687)" },
          { countryCodeContact: "64", name: "New Zealand (+64)" },
          { countryCodeContact: "505", name: "Nicaragua (+505)" },
          { countryCodeContact: "227", name: "Niger (+227)" },
          { countryCodeContact: "234", name: "Nigeria (+234)" },
          { countryCodeContact: "683", name: "Niue (+683)" },
          { countryCodeContact: "672", name: "Norfolk Islands (+672)" },
          { countryCodeContact: "670", name: "Northern Marianas (+670)" },
          { countryCodeContact: "47", name: "Norway (+47)" },
          { countryCodeContact: "968", name: "Oman (+968)" },
          { countryCodeContact: "680", name: "Palau (+680)" },
          { countryCodeContact: "507", name: "Panama (+507)" },
          { countryCodeContact: "675", name: "Papua New Guinea (+675)" },
          { countryCodeContact: "595", name: "Paraguay (+595)" },
          { countryCodeContact: "51", name: "Peru (+51)" },
          { countryCodeContact: "63", name: "Philippines (+63)" },
          { countryCodeContact: "48", name: "Poland (+48)" },
          { countryCodeContact: "351", name: "Portugal (+351)" },
          { countryCodeContact: "1787", name: "Puerto Rico (+1787)" },
          { countryCodeContact: "974", name: "Qatar (+974)" },
          { countryCodeContact: "262", name: "Reunion (+262)" },
          { countryCodeContact: "40", name: "Romania (+40)" },
          { countryCodeContact: "7", name: "Russia (+7)" },
          { countryCodeContact: "250", name: "Rwanda (+250)" },
          { countryCodeContact: "378", name: "San Marino (+378)" },
          { countryCodeContact: "239", name: "Sao Tome &amp; Principe (+239)" },
          { countryCodeContact: "966", name: "Saudi Arabia (+966)" },
          { countryCodeContact: "221", name: "Senegal (+221)" },
          { countryCodeContact: "381", name: "Serbia (+381)" },
          { countryCodeContact: "248", name: "Seychelles (+248)" },
          { countryCodeContact: "232", name: "Sierra Leone (+232)" },
          { countryCodeContact: "65", name: "Singapore (+65)" },
          { countryCodeContact: "421", name: "Slovak Republic (+421)" },
          { countryCodeContact: "386", name: "Slovenia (+386)" },
          { countryCodeContact: "677", name: "Solomon Islands (+677)" },
          { countryCodeContact: "252", name: "Somalia (+252)" },
          { countryCodeContact: "27", name: "South Africa (+27)" },
          { countryCodeContact: "34", name: "Spain (+34)" },
          { countryCodeContact: "94", name: "Sri Lanka (+94)" },
          { countryCodeContact: "290", name: "St. Helena (+290)" },
          { countryCodeContact: "1869", name: "St. Kitts (+1869)" },
          { countryCodeContact: "1758", name: "St. Lucia (+1758)" },
          { countryCodeContact: "249", name: "Sudan (+249)" },
          { countryCodeContact: "597", name: "Suriname (+597)" },
          { countryCodeContact: "268", name: "Swaziland (+268)" },
          { countryCodeContact: "46", name: "Sweden (+46)" },
          { countryCodeContact: "41", name: "Switzerland (+41)" },
          { countryCodeContact: "963", name: "Syria (+963)" },
          { countryCodeContact: "886", name: "Taiwan (+886)" },
          { countryCodeContact: "7", name: "Tajikstan (+7)" },
          { countryCodeContact: "66", name: "Thailand (+66)" },
          { countryCodeContact: "228", name: "Togo (+228)" },
          { countryCodeContact: "676", name: "Tonga (+676)" },
          { countryCodeContact: "1868", name: "Trinidad &amp; Tobago (+1868)" },
          { countryCodeContact: "216", name: "Tunisia (+216)" },
          { countryCodeContact: "90", name: "Turkey (+90)" },
          { countryCodeContact: "7", name: "Turkmenistan (+7)" },
          { countryCodeContact: "993", name: "Turkmenistan (+993)" },
          { countryCodeContact: "1649", name: "Turks &amp; Caicos Islands (+1649)" },
          { countryCodeContact: "688", name: "Tuvalu (+688)" },
          { countryCodeContact: "256", name: "Uganda (+256)" },
          { countryCodeContact: "44", name: "UK (+44)" },
          { countryCodeContact: "380", name: "Ukraine (+380)" },
          { countryCodeContact: "971", name: "United Arab Emirates (+971)" },
          { countryCodeContact: "598", name: "Uruguay (+598)" },
          { countryCodeContact: "1", name: "USA (+1)" },
          { countryCodeContact: "7", name: "Uzbekistan (+7)" },
          { countryCodeContact: "678", name: "Vanuatu (+678)" },
          { countryCodeContact: "379", name: "Vatican City (+379)" },
          { countryCodeContact: "58", name: "Venezuela (+58)" },
          { countryCodeContact: "84", name: "Vietnam (+84)" },
          { countryCodeContact: "84", name: "Virgin Islands - British (+1284)" },
          { countryCodeContact: "84", name: "Virgin Islands - US (+1340)" },
          { countryCodeContact: "681", name: "Wallis &amp; Futuna (+681)" },
          { countryCodeContact: "969", name: "Yemen (North)(+969)" },
          { countryCodeContact: "967", name: "Yemen (South)(+967)" },
          { countryCodeContact: "260", name: "Zambia (+260)" },
          { countryCodeContact: "263", name: "Zimbabwe (+263)" }
        ]
        this._allState = this._commonService.StateDDL(data.countryId);
        this._allCity = this._commonService.CityDDL(data.stateId);

        // this._roles = this._commonService.RoleDDL();
        // debugger;
        if (data.eventDate !== null)
          this.validTill = data.eventDate.split('T')[0];
        else
          this.validTill = this.maxDate;
        // document.getElementById("eventDate").placeholder = this.validTill;
        if (data.countryCodeContact === null)
          data.countryCodeContact = "1";
        this.tempImageUrl = data.imageUrl;
        this.validFrom = this.validTill;
        this.datePlaceholder = this.validFrom;

        this.FormPostEvent.setValue({
          eventId: data.eventId,
          title: data.title,
          email: data.email,
          faxNumber: data.faxNumber,
          addressStreet1: data.addressStreet1,
          addressStreet2: data.addressStreet2,
          countryId: data.countryId,
          stateId: data.stateId.toString(),
          cityId: data.cityId.toString(),
          zipCode: data.zipCode,
          postedFor: data.postedFor,
          postedName: data.postedForName,
          contactPersonName: data.postedForName,
          contactNumber: data.contactNumber,
          countryCodeContact: data.countryCodeContact.toString(),
          website: data.website,
          description: data.description,
          alternateContactNumber: data.alternateContactNumber,
          eventDate: this.validTill,
          categoryId: data.categoryId.toString(),
          imageUrl: data.imageUrl,
          createdBy: this.loggedInUserId,
          isPaidAd: this.isPaidAd,

          eventImageCount: 0
        });
      });
    });
  }

  subscriptionPlanDetails(event: any) {
    console.log(event.target.value);
    this.paymentPlanService.getPlanDetails(event.target.value).subscribe(data => {
      var planIdDetails = JSON.parse(data._body);
      console.log(planIdDetails);
      this.previousPlan = planIdDetails;
      this.remainingeventImage = planIdDetails.eventImageCount;
      if (this.remainingeventImage == 0) {
        this.toastr.info("subscription plan exuausted. Please buy a new plan.")
      }
      this.FormPostEvent.patchValue({
        eventImageCount: planIdDetails.eventImageCount
      });
    });
  }

  //  Dropdowns
  //FillCategoryDDL() 
  //{
  //  this._allCategory = this._commonService.CategoryDDL();
  //}


  FillCategoryDDL() {
    //debugger;
    this._allCategory = this._commonService.CategoryDDL();

    this._commonService.CategoryDDL().subscribe(data => {
      //debugger;
      this.categories = data;


    });

  }


  FillCategoryName(event: any) {
    this.categoryId = event.CategoryId;
    this.categoryName = event.CategoryName
    this._commonService.CategoryDDL().subscribe(data => {
      //debugger;
      this.categories = data;


    });
    //this.categoryName = event.target.options[this.categoryId].innerHTML;
    //alert(this.categoryName);
  }
  FillCountryDDL() {
    this._allCountry = this._commonService.CountryDDL();
    this._allState = this._commonService.StateDDL(this.CountryId);
    this._allCity = this._commonService.CityDDL(this.StateId);
    this._commonService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      this.states = data;
    });
  }



  FillUserDDL() {
    //debugger;
    this._allUser = this._commonService.UserDDL();
    //this._commonService.UserInboxMessageDDL().subscribe(data => {
    //  debugger;
    //  this.users = data;


    //});

  }
  selected(listingType: string) {
    debugger;
    if (listingType == "Free") {
      this.isPaidAd = false;
      this.displayPaidSection = 'col-md-12 d-none';
    }
    if (listingType == "Paid") {
      this.isPaidAd = true;

      this.displayPaidSection = 'col-md-12';
    }
  }

  FillStateDDL(event: any) {
    //debugger;
    if ((event.target.value == "0") || ((event.target.value == ""))) {
      this.FormPostEvent.value.countryId = 0;
      //this._allState = this._commonService.StateDDL("0");
      var x = document.getElementsByClassName("ng-value-label");
      x[2].innerHTML = "--Select State--";
      x[3].innerHTML = "--Select City--";
      //var i;
      //for (i = 3; i < x.length; i++) {
      //  x[i].innerHTML = "";
      //}
      this.states = [];
      this.cities = [];
      //this.FillCityDDL(0);
      console.log(this.states);
      this.FormPostEvent.value.stateId = 0;
      //this._allCity = this._commonService.CityDDL("0");
      this.FormPostEvent.value.cityId = 0;
    }
    else {
      this.CountryId = event.countryId;

      this._allState = this._commonService.StateDDL("1");
      //debugger;
      this._commonService.StateDDL("1").subscribe(data => {
        console.log(data);
        //debugger;
        this.states = data;
      });

      this._commonService.CityDDL(this.StateId).subscribe(data => {
        console.log(data);
        //debugger;
        this.cities = data;
      });
      this._allState = this._commonService.StateDDL(this.CountryId);
      //this.CountryName = event.target.options[this.CountryId].innerHTML;
      //this.FormPostAd.countryName = this.CountryName;
    }

  }


  FillCityDDL(event: any) {
    if (event != 0) {
      var x = document.getElementsByClassName("ng-value-label");
      //x[1].innerHTML = "--Select State--";
      debugger;
      x[3].innerHTML = "--Select City--";
      this.StateId = event.stateId;
      this.StateName = event.stateName;
      //this.StateName=event.target.options[this.StateId].innerHTML;
      this._allCity = this._commonService.CityDDL(this.StateId);
      this._commonService.CityDDL(this.StateId).subscribe(data => {
        console.log(data);
        // debugger;
        this.cities = data;
      });
    }
    else {
      var x = document.getElementsByClassName("ng-value-label");
      this.cities = [];
    }

  }

  FillCityName(event: any) {
    this.CityId = event.cityId;
    this.CityName = event.cityName;
    // this.CityName=event.target.options[this.CityId].innerHTML;


  }

  NavigatetoManageAd() {
    this._router.navigate(['/theme/manageevent']);
  }
  FillSubscriptionPlan(userId: number) {
    this._allPlan = this.paymentPlanService.UserPlanDDL(userId);

  }

  countrycode = [

    { countryCodeContact: "1", name: "USA (+1)" },
    { countryCodeContact: "44", name: "UK (+44)" },
    { countryCodeContact: "213", name: "Algeria (+213)" },
    { countryCodeContact: "376", name: "Andorra (+376)" },
    { countryCodeContact: "244", name: "Angola (+244)" },
    { countryCodeContact: "1264", name: "Anguilla (+1264)" },
    { countryCodeContact: "1268", name: "Antigua &amp; Barbuda (+1268)" },
    { countryCodeContact: "54", name: "Argentina (+54)" },
    { countryCodeContact: "374", name: "Armenia (+374)" },
    { countryCodeContact: "297", name: "Aruba (+297)" },
    { countryCodeContact: "61", name: "Australia (+61)" },
    { countryCodeContact: "43", name: "Austria (+43)" },
    { countryCodeContact: "994", name: "Azerbaijan (+994)" },
    { countryCodeContact: "1242", name: "Bahamas (+1242)" },
    { countryCodeContact: "973", name: "Bahrain (+973)" },
    { countryCodeContact: "880", name: "Bangladesh (+880)" },
    { countryCodeContact: "1246", name: "Barbados (+1246)" },
    { countryCodeContact: "375", name: "Belarus (+375)" },
    { countryCodeContact: "32", name: "Belgium (+32)" },
    { countryCodeContact: "501", name: "Belize (+501)" },
    { countryCodeContact: "229", name: "Benin (+229)" },
    { countryCodeContact: "1441", name: "Bermuda (+1441)" },
    { countryCodeContact: "975", name: "Bhutan (+975)" },
    { countryCodeContact: "591", name: "Bolivia (+591)" },
    { countryCodeContact: "387", name: "Bosnia Herzegovina (+387)" },
    { countryCodeContact: "267", name: "Botswana (+267)" },
    { countryCodeContact: "55", name: "Brazil (+55)" },
    { countryCodeContact: "673", name: "Brunei (+673)" },
    { countryCodeContact: "359", name: "Bulgaria (+359)" },
    { countryCodeContact: "226", name: "Burkina Faso (+226)" },
    { countryCodeContact: "257", name: "Burundi (+257)" },
    { countryCodeContact: "855", name: "Cambodia (+855)" },
    { countryCodeContact: "237", name: "Cameroon (+237)" },
    { countryCodeContact: "1", name: "Canada (+1)" },
    { countryCodeContact: "238", name: "Cape Verde Islands (+238)" },
    { countryCodeContact: "1345", name: "Cayman Islands (+1345)" },
    { countryCodeContact: "236", name: "Central African Republic (+236)" },
    { countryCodeContact: "56", name: "Chile (+56)" },
    { countryCodeContact: "86", name: "China (+86)" },
    { countryCodeContact: "57", name: "Colombia (+57)" },
    { countryCodeContact: "269", name: "Comoros (+269)" },
    { countryCodeContact: "242", name: "Congo (+242)" },
    { countryCodeContact: "682", name: "Cook Islands (+682)" },
    { countryCodeContact: "506", name: "Costa Rica (+506)" },
    { countryCodeContact: "385", name: "Croatia (+385)" },
    { countryCodeContact: "53", name: "Cuba (+53)" },
    { countryCodeContact: "90392", name: "Cyprus North (+90392)" },
    { countryCodeContact: "357", name: "Cyprus South (+357)" },
    { countryCodeContact: "42", name: "Czech Republic (+42)" },
    { countryCodeContact: "45", name: "Denmark (+45)" },
    { countryCodeContact: "253", name: "Djibouti (+253)" },
    { countryCodeContact: "1809", name: "Dominica (+1809)" },
    { countryCodeContact: "1809", name: "Dominican Republic (+1809)" },
    { countryCodeContact: "593", name: "Ecuador (+593)" },
    { countryCodeContact: "20", name: "Egypt (+20)" },
    { countryCodeContact: "503", name: "El Salvador (+503)" },
    { countryCodeContact: "240", name: "Equatorial Guinea (+240)" },
    { countryCodeContact: "291", name: "Eritrea (+291)" },
    { countryCodeContact: "372", name: "Estonia (+372)" },
    { countryCodeContact: "251", name: "Ethiopia (+251)" },
    { countryCodeContact: "500", name: "Falkland Islands (+500)" },
    { countryCodeContact: "298", name: "Faroe Islands (+298)" },
    { countryCodeContact: "679", name: "Fiji (+679)" },
    { countryCodeContact: "358", name: "Finland (+358)" },
    { countryCodeContact: "33", name: "France (+33)" },
    { countryCodeContact: "594", name: "French Guiana (+594)" },
    { countryCodeContact: "689", name: "French Polynesia (+689)" },
    { countryCodeContact: "241", name: "Gabon (+241)" },
    { countryCodeContact: "220", name: "Gambia (+220)" },
    { countryCodeContact: "7880", name: "Georgia (+7880)" },
    { countryCodeContact: "49", name: "Germany (+49)" },
    { countryCodeContact: "233", name: "Ghana (+233)" },
    { countryCodeContact: "350", name: "Gibraltar (+350)" },
    { countryCodeContact: "30", name: "Greece (+30)" },
    { countryCodeContact: "299", name: "Greenland (+299)" },
    { countryCodeContact: "1473", name: "Grenada (+1473)" },
    { countryCodeContact: "590", name: "Guadeloupe (+590)" },
    { countryCodeContact: "671", name: "Guam (+671)" },
    { countryCodeContact: "502", name: "Guatemala (+502)" },
    { countryCodeContact: "224", name: "Guinea (+224)" },
    { countryCodeContact: "245", name: "Guinea - Bissau (+245)" },
    { countryCodeContact: "592", name: "Guyana (+592)" },
    { countryCodeContact: "509", name: "Haiti (+509)" },
    { countryCodeContact: "504", name: "Honduras (+504)" },
    { countryCodeContact: "852", name: "Hong Kong (+852)" },
    { countryCodeContact: "36", name: "Hungary (+36)" },
    { countryCodeContact: "354", name: "Iceland (+354)" },
    { countryCodeContact: "91", name: "India (+91)" },
    { countryCodeContact: "62", name: "Indonesia (+62)" },
    { countryCodeContact: "98", name: "Iran (+98)" },
    { countryCodeContact: "964", name: "Iraq (+964)" },
    { countryCodeContact: "353", name: "Ireland (+353)" },
    { countryCodeContact: "972", name: "Israel (+972)" },
    { countryCodeContact: "39", name: "Italy (+39)" },
    { countryCodeContact: "1876", name: "Jamaica (+1876)" },
    { countryCodeContact: "81", name: "Japan (+81)" },
    { countryCodeContact: "962", name: "Jordan (+962)" },
    { countryCodeContact: "7", name: "Kazakhstan (+7)" },
    { countryCodeContact: "254", name: "Kenya (+254)" },
    { countryCodeContact: "686", name: "Kiribati (+686)" },
    { countryCodeContact: "850", name: "Korea North (+850)" },
    { countryCodeContact: "82", name: "Korea South (+82)" },
    { countryCodeContact: "965", name: "Kuwait (+965)" },
    { countryCodeContact: "996", name: "Kyrgyzstan (+996)" },
    { countryCodeContact: "856", name: "Laos (+856)" },
    { countryCodeContact: "371", name: "Latvia (+371)" },
    { countryCodeContact: "961", name: "Lebanon (+961)" },
    { countryCodeContact: "266", name: "Lesotho (+266)" },
    { countryCodeContact: "231", name: "Liberia (+231)" },
    { countryCodeContact: "218", name: "Libya (+218)" },
    { countryCodeContact: "417", name: "Liechtenstein (+417)" },
    { countryCodeContact: "370", name: "Lithuania (+370)" },
    { countryCodeContact: "352", name: "Luxembourg (+352)" },
    { countryCodeContact: "853", name: "Macao (+853)" },
    { countryCodeContact: "389", name: "Macedonia (+389)" },
    { countryCodeContact: "261", name: "Madagascar (+261)" },
    { countryCodeContact: "265", name: "Malawi (+265)" },
    { countryCodeContact: "60", name: "Malaysia (+60)" },
    { countryCodeContact: "960", name: "Maldives (+960)" },
    { countryCodeContact: "223", name: "Mali (+223)" },
    { countryCodeContact: "356", name: "Malta (+356)" },
    { countryCodeContact: "692", name: "Marshall Islands (+692)" },
    { countryCodeContact: "596", name: "Martinique (+596)" },
    { countryCodeContact: "222", name: "Mauritania (+222)" },
    { countryCodeContact: "269", name: "Mayotte (+269)" },
    { countryCodeContact: "52", name: "Mexico (+52)" },
    { countryCodeContact: "691", name: "Micronesia (+691)" },
    { countryCodeContact: "373", name: "Moldova (+373)" },
    { countryCodeContact: "377", name: "Monaco (+377)" },
    { countryCodeContact: "976", name: "Mongolia (+976)" },
    { countryCodeContact: "1664", name: "Montserrat (+1664)" },
    { countryCodeContact: "212", name: "Morocco (+212)" },
    { countryCodeContact: "258", name: "Mozambique (+258)" },
    { countryCodeContact: "95", name: "Myanmar (+95)" },
    { countryCodeContact: "264", name: "Namibia (+264)" },
    { countryCodeContact: "674", name: "Nauru (+674)" },
    { countryCodeContact: "977", name: "Nepal (+977)" },
    { countryCodeContact: "31", name: "Netherlands (+31)" },
    { countryCodeContact: "687", name: "New Caledonia (+687)" },
    { countryCodeContact: "64", name: "New Zealand (+64)" },
    { countryCodeContact: "505", name: "Nicaragua (+505)" },
    { countryCodeContact: "227", name: "Niger (+227)" },
    { countryCodeContact: "234", name: "Nigeria (+234)" },
    { countryCodeContact: "683", name: "Niue (+683)" },
    { countryCodeContact: "672", name: "Norfolk Islands (+672)" },
    { countryCodeContact: "670", name: "Northern Marianas (+670)" },
    { countryCodeContact: "47", name: "Norway (+47)" },
    { countryCodeContact: "968", name: "Oman (+968)" },
    { countryCodeContact: "680", name: "Palau (+680)" },
    { countryCodeContact: "507", name: "Panama (+507)" },
    { countryCodeContact: "675", name: "Papua New Guinea (+675)" },
    { countryCodeContact: "595", name: "Paraguay (+595)" },
    { countryCodeContact: "51", name: "Peru (+51)" },
    { countryCodeContact: "63", name: "Philippines (+63)" },
    { countryCodeContact: "48", name: "Poland (+48)" },
    { countryCodeContact: "351", name: "Portugal (+351)" },
    { countryCodeContact: "1787", name: "Puerto Rico (+1787)" },
    { countryCodeContact: "974", name: "Qatar (+974)" },
    { countryCodeContact: "262", name: "Reunion (+262)" },
    { countryCodeContact: "40", name: "Romania (+40)" },
    { countryCodeContact: "7", name: "Russia (+7)" },
    { countryCodeContact: "250", name: "Rwanda (+250)" },
    { countryCodeContact: "378", name: "San Marino (+378)" },
    { countryCodeContact: "239", name: "Sao Tome &amp; Principe (+239)" },
    { countryCodeContact: "966", name: "Saudi Arabia (+966)" },
    { countryCodeContact: "221", name: "Senegal (+221)" },
    { countryCodeContact: "381", name: "Serbia (+381)" },
    { countryCodeContact: "248", name: "Seychelles (+248)" },
    { countryCodeContact: "232", name: "Sierra Leone (+232)" },
    { countryCodeContact: "65", name: "Singapore (+65)" },
    { countryCodeContact: "421", name: "Slovak Republic (+421)" },
    { countryCodeContact: "386", name: "Slovenia (+386)" },
    { countryCodeContact: "677", name: "Solomon Islands (+677)" },
    { countryCodeContact: "252", name: "Somalia (+252)" },
    { countryCodeContact: "27", name: "South Africa (+27)" },
    { countryCodeContact: "34", name: "Spain (+34)" },
    { countryCodeContact: "94", name: "Sri Lanka (+94)" },
    { countryCodeContact: "290", name: "St. Helena (+290)" },
    { countryCodeContact: "1869", name: "St. Kitts (+1869)" },
    { countryCodeContact: "1758", name: "St. Lucia (+1758)" },
    { countryCodeContact: "249", name: "Sudan (+249)" },
    { countryCodeContact: "597", name: "Suriname (+597)" },
    { countryCodeContact: "268", name: "Swaziland (+268)" },
    { countryCodeContact: "46", name: "Sweden (+46)" },
    { countryCodeContact: "41", name: "Switzerland (+41)" },
    { countryCodeContact: "963", name: "Syria (+963)" },
    { countryCodeContact: "886", name: "Taiwan (+886)" },
    { countryCodeContact: "7", name: "Tajikstan (+7)" },
    { countryCodeContact: "66", name: "Thailand (+66)" },
    { countryCodeContact: "228", name: "Togo (+228)" },
    { countryCodeContact: "676", name: "Tonga (+676)" },
    { countryCodeContact: "1868", name: "Trinidad &amp; Tobago (+1868)" },
    { countryCodeContact: "216", name: "Tunisia (+216)" },
    { countryCodeContact: "90", name: "Turkey (+90)" },
    { countryCodeContact: "7", name: "Turkmenistan (+7)" },
    { countryCodeContact: "993", name: "Turkmenistan (+993)" },
    { countryCodeContact: "1649", name: "Turks &amp; Caicos Islands (+1649)" },
    { countryCodeContact: "688", name: "Tuvalu (+688)" },
    { countryCodeContact: "256", name: "Uganda (+256)" },
    { countryCodeContact: "44", name: "UK (+44)" },
    { countryCodeContact: "380", name: "Ukraine (+380)" },
    { countryCodeContact: "971", name: "United Arab Emirates (+971)" },
    { countryCodeContact: "598", name: "Uruguay (+598)" },
    { countryCodeContact: "1", name: "USA (+1)" },
    { countryCodeContact: "7", name: "Uzbekistan (+7)" },
    { countryCodeContact: "678", name: "Vanuatu (+678)" },
    { countryCodeContact: "379", name: "Vatican City (+379)" },
    { countryCodeContact: "58", name: "Venezuela (+58)" },
    { countryCodeContact: "84", name: "Vietnam (+84)" },
    { countryCodeContact: "84", name: "Virgin Islands - British (+1284)" },
    { countryCodeContact: "84", name: "Virgin Islands - US (+1340)" },
    { countryCodeContact: "681", name: "Wallis &amp; Futuna (+681)" },
    { countryCodeContact: "969", name: "Yemen (North)(+969)" },
    { countryCodeContact: "967", name: "Yemen (South)(+967)" },
    { countryCodeContact: "260", name: "Zambia (+260)" },
    { countryCodeContact: "263", name: "Zimbabwe (+263)" }
  ]



}
