import { Component, OnInit } from '@angular/core';
import { EventDetailsService } from '../eventdetails.service';
import { Router } from "@angular/router";
import { EventDetails, WorkflowstatusDTO } from '../eventdetails.model';
import { IfStmt, debugOutputAstAsTypeScript } from '@angular/compiler';
import { OrderPipe } from 'ngx-order-pipe';
import { environment } from '../../../../environments/environment';
import { ToastrService } from 'ngx-toastr'; 
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-manageevent',
  templateUrl: './manageevent.component.html',
  styleUrls: ['./manageevent.component.scss'],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }]
})
export class ManageeventComponent implements OnInit 
{
  charLength: string = "0";
    adevents: EventDetails[];   
    setStatus: string = ""; 
    workflowdetails:string;
    statusMessage: string;
    deleteremark:string;
    activity:boolean;
    visibilitybutton:string;
    visibilityspan:string;
    adevent = new EventDetails();order: string;
    reverse: any ;
    modalRemark:string;
  public pageSize: number = 10;
  public p: number;
    remark: string;
    workflowstatusId:number;
    action:string="";
    eventId=0;
    imagepath=environment.imagepath;
    sortedCollection: any[];
    id : number;
    setStatusActive:string="";
  setStatusInactive:string="";
    actionMessage : string;
  validTill: string;
  validFrom: string;
  maxDate: string;
  minDate: string;
  //minValidTillDate: string;
  //maxValidFromDate: string;
  modalexit: string = "";
  LoggedInUserId: string;
  showDates: string = "col-md-6";
  loggedInRole: string;
  approveButtonText: string;
  approveButtonStyle: string;
  filter: string = "";
  remarkstring: string[];
  poststring: string;
  remarkstring1: string;
  remarkstring2: string;
  remarkstring3: string;
  actionmessage: string;
  minDateModel: NgbDateStruct;
  minValidTillDate: NgbDateStruct;
  maxValidFromDate: NgbDateStruct;
  setType: string = "";
  date: { year: number, month: number };
  hoveredDate: NgbDate;
  modelValidFrom: Date = null;
  modelValidTill: Date = null;
  IsPremium: boolean = false;
  eventtype: string;
  showPremium: string = "col-md-3";
  selectToday() {
    this.minDateModel = this.calendar.getToday();
    this.minValidTillDate = this.calendar.getToday();
  }
  onvalidFromDateSelect(event: any) {
    this.minValidTillDate = event;
    this.validFrom = this.modelValidFrom.toJSON().split('T')[0];

  }
  onvalidTillDateSelect(event: any) {
    this.maxValidFromDate = event;
    this.validTill = this.modelValidTill.toJSON().split('T')[0];

  }

  getStatus(Status: string) {

    if (Status == "Active") 
      return this.setStatus = "badge badge-success";

    if (Status == "InActive")
      return this.setStatus = "badge badge-danger";

    if (Status == "Pending")
      return this.setStatus = "badge badge-warning";

  }
  onDelete(eventId: number, event) {
    this.actionmessage = 'Are you sure to delete this Event?';
    this.eventId = eventId;
  }
  ActionDelete() {
    this.action = "Delete";
    this.deleteremark = "";
    this.workflowdetails = this.eventId + "," + this.LoggedInUserId + "," + this.action + "," + this.deleteremark;
    this.EventDetailsService.updatewithWorkflow(this.workflowdetails).
      subscribe(response => {
        console.log(response);
        this.getEventList();
        //debugger;
        if (response.status === 200) {
          //debugger;
          console.log(response);
          if ((<any>response)._body === "true")
            this.toastr.success('Event successfully deleted.');
          else
            this.toastr.info('Event is active on website.');
          //debugger

        }
        else {
          this.toastr.warning('Problem with service. Please try again later!');
        }
      });
  }

  // constructor(private EventDetailsService: EventDetailsService, private router: Router) { }

  updatePost(adevent: EventDetails): void {
    //debugger
    //this.router.navigate(['/theme/editbanner'],{queryParams:{bannerId:managebanner.bannerId}});  
    this.router.navigate(['/theme/updateevent'], { queryParams: { id: adevent.eventId } });
  };

  getEventList(): void {
debugger
    this.EventDetailsService.getEventList().subscribe(adevents => {
      this.adevents = adevents;

      console.log(this.adevents);
      // Status
      debugger;
      var adlenth = adevents.length;

      for (let i = 0; i < adlenth; i++) {
        debugger;
        if (adevents[i].isPremium === true || adevents[i].isPremium === "Premium" || adevents[i].isPremium === "True") {
 
          //this.eventtype = "Premium"
          adevents[i].isPremium = "Premium";
          this.setType = "badge badge-warning";
        }
        else {
          adevents[i].isPremium = "Free";
         // this.eventtype = ""
          this.setType = "";
        }

        if (adevents[i].isVisible === true || adevents[i].isVisible === "True") {
          adevents[i].isVisible = "Active";
          adevents[i].Activity = true;
          this.visibilitybutton = "hide";
          this.visibilityspan = "show";
        }
        else if (adevents[i].isVisible === false || adevents[i].isVisible === "False") {
          adevents[i].isVisible = "InActive";
          adevents[i].Activity = true;
          this.visibilitybutton = "hide";
          this.visibilityspan = "show";
        }
        else if (adevents[i].isVisible === null || adevents[i].isVisible === "") {
          adevents[i].isVisible = "";
          adevents[i].Activity = false;
          this.visibilitybutton = "show";
          this.visibilityspan = "hide";
        }
    

      }
      this.adevents = adevents;



    });
  }

  openModal()
  {
    //debugger;
    //#modalRemarkText
    let modalRemarkText = (<HTMLInputElement>document.getElementById('modalRemarkText')).value;
    if (modalRemarkText != "")
      modalRemarkText = "";
    return "modal";

  }

  getPicUrl(picurl:string,eventId)
    {
      //debugger;
      if(picurl==null)
      return "assets/img/default/no_image_placeholder.jpg";
      else if (picurl=="")
      return "assets/img/default/no_image_placeholder.jpg";
      else     
      return this.imagepath+'Events/'+eventId+'/Image/'+picurl;

  }

  updateEvent(adevent: EventDetails): void {
    //debugger;
    this.router.navigate(['/theme/postevent'], { queryParams: { eventId: adevent.eventId } });
  };

  gotoEventDetails(adevent: EventDetails): void {
    //debugger
    this.router.navigate(['/theme/eventdetailsview'], { queryParams: { eventId: adevent.eventId } });
  };
  NavigateToAddEvent(): void {
    //debugger
    this.router.navigate(['/theme/postevent']);
  };

  constructor(private toastr: ToastrService, private orderPipe: OrderPipe, private EventDetailsService: EventDetailsService, private router: Router, private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private config: NgbDatepickerConfig) {
    this.sortedCollection = orderPipe.transform(this.adevents, 'info.title');
    console.log(this.sortedCollection);
  }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }
  ActionOnApprove(eventId, IsPremium, event: any) {

    debugger;
    this.remark = null;
    this.validFrom = null;
    this.validTill = null;
    this.modelValidFrom = null;
    this.modelValidTill = null;
    this.maxValidFromDate = null;
    this.minValidTillDate = this.calendar.getToday();
    this.showDates = "col-md-6";
    this.eventId = eventId;
    this.action = "Approve";
    this.approveButtonText = "";
    this.approveButtonText = "Approve";
    this.approveButtonStyle = "";
    this.approveButtonStyle = "btn btn-sm btn-success";
    this.actionMessage = "Are you sure to approve this record?";
    this.showPremium = "col-md-3"
    if (IsPremium == "Premium")
      this.IsPremium = true;
    else
      this.IsPremium = false;
  }

  ActionOnReject(eventId, event) {
    this.remark = null;
    this.validFrom = null;
    this.validTill = null;
    this.modelValidFrom = null;
    this.modelValidTill = null;
    this.maxValidFromDate = null;
    this.minValidTillDate = this.calendar.getToday();
    this.showDates = "col-md-6 d-none";
    this.eventId = eventId;
    this.action = "Reject";
    this.approveButtonText = "";
    this.approveButtonText = "Reject";
    this.approveButtonStyle = "";
    this.approveButtonStyle = "btn btn-sm btn-warning";
    this.actionMessage = "Are you sure to Reject this record?";
    this.IsPremium = false;
    this.showPremium = "col-md-3  d-none";
  }

  ActionOnPost(remark) {
    //debugger;

    console.log(this.approveButtonText);

    this.poststring = remark;

    this.remarkstring = this.poststring.split(',');
    this.remarkstring1 = this.remarkstring[0];
    this.remarkstring2 = this.remarkstring[1];
    this.remarkstring3 = this.remarkstring[2].replace(/,/g, ' ');
    
    if (this.remarkstring[3] == "true")
      this.IsPremium = true
    else
      this.IsPremium = false;
    if ((this.approveButtonText == "Approve") && ((this.remarkstring1 == "null") || (this.remarkstring2 == "null") || (this.remarkstring3 == "null"))) {
      this.toastr.warning("All fields are required to approve");
      return;
    }

    if ((this.approveButtonText == "Reject") && (this.remarkstring3 == "null")) {
      this.toastr.warning("Remark is required to reject");
      return;
    }

    this.workflowdetails = this.eventId + "," + this.LoggedInUserId + "," + this.action + "," + remark;

    // string message = Are you sure to "remark" this record?;

    //  if (confirm(this.actionMessage)) {
    this.EventDetailsService.updatewithWorkflow(this.workflowdetails).subscribe(response => {
      console.log(response);
      console.log('Updated');
      this.modalexit = "modal";
      document.getElementById("closeBtn").click();
      this.getEventList();
      if (this.action === "Approve")
        this.toastr.success('Event approved succesfully');
      if (this.action === "Reject")
        this.toastr.warning('Event rejected');
    },

      (error) => {
        console.log(error);

        this.toastr.error('Problem with service. Please try again later!');
      });
    //  }
    //this.router.navigate(['/theme/postevent'],{queryParams:{eventId:adevent.eventId}});
  }

  DeleteEvent(eventId: number): void {
    //debugger;
    this.eventId = eventId;
    this.action = "Delete";
    this.deleteremark = "";
    this.workflowdetails = this.eventId + "," + this.LoggedInUserId + "," + this.action + "," + this.deleteremark;
    if (confirm('Are you sure to delete this record?')) {
      this.EventDetailsService.updatewithWorkflow(this.workflowdetails).
        subscribe(response => {
          console.log(response);
          this.getEventList();
          //debugger;
          if (response.status === 200) {
            //debugger;
            console.log(response);
            if ((<any>response)._body === "true")
              this.toastr.success('Event successfully deleted.');
            else
              this.toastr.info('Event is active on website.');
            //debugger

          }
          else {
            this.toastr.warning('Problem with service. Please try again later!');
          }

        },

          (error) => {
            console.log(error);

            this.toastr.error('Problem with service. Please try again later!');
          });
    }
  }

  ngOnInit() {
    //this.gettoday();
    this.charLength = "0/200";
    this.selectToday();
    this.getEventList();
    this.LoggedInUserId = localStorage.getItem('userId');

  }
  //gettoday() {
  //  //debugger;
  //  var CurrentDate = new Date();
  //  this.minDate = CurrentDate.toJSON().split('T')[0];
  //  CurrentDate.setMonth(CurrentDate.getMonth() + 1);
  //  this.maxDate = CurrentDate.toJSON().split('T')[0];
  //}
  //onvalidFromDateSelect(event: any) {
  //  //debugger;
  //  this.minValidTillDate = event.target.value;

  //}
  //onvalidTillDateSelect(event: any) {
  //  this.maxValidFromDate = event.target.value;

  //}

  ngAfterViewInit() {
    document.getElementById("#modalvalidFrom").setAttribute("min", this.minDate);
    document.getElementById("#modalvalidTill").setAttribute("min", this.minDate);


  }

  eventDetailsView(eventId: number) {
    debugger;
    this.router.navigate(['/theme/eventdetailview'], { queryParams: { id: eventId} });
  }
  
}
