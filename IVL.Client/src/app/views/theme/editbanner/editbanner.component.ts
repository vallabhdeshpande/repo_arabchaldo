import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Country, State, City, category, Subcategory, Roles, CountryCodes } from '../../shared/common.model';
import { User } from '../user';
import { UserService } from '../user.service';
import { CommonService } from '../../shared/common.service';
import { HttpClient, HttpEventType, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { environment } from '../../../../environments/environment';
import { element } from 'protractor';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Managebannerservice } from '../managebannerservice';
import { Managebanner } from '../managebanner';
import { Paymentplan } from '../paymentplan';
import { Paymentplanservice } from '../paymentplanservice';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import * as countryCodeData from '../../shared/countrycodes.json';
@Component({
  selector: 'app-editbanner',
  templateUrl: './editbanner.component.html',
  styleUrls: ['./editbanner.component.scss']
})
export class EditbannerComponent implements OnInit {
  editForm: FormGroup;
  manageBanner: Managebanner;
  managebanners: Managebanner[];
  managebanner = new Managebanner();
  loggedInUserId: string;
  //constructor(private toastr: ToastrService, private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, 
  //  private _commonService: CommonService, private userService: UserService, private activatedRoute: ActivatedRoute) { }

  public _allUser: Observable<User[]>;
  public _allCountry: Observable<Country[]>;
  public _allCountryCodes: Observable<CountryCodes[]>;
  public _allState: Observable<State[]>;
  public _allCity: Observable<City[]>;

  postedFor: string = "0";
  CountryId: string = "0";
  StateId: string = "0";
  CityId: string = "0";
  logopath: string[];
  states: any[];
  cities: any[];
  users: any[];
  StateName: string = "";
  CityName: string = "";
  imagepath: any[];
  logoUrl: string = "";
  imageUrl: string = "";
  ABCD: string[];
  bannerimage: string;
  //dvImage: string[];
  FormPostBanner: any;
  HorizontalView: boolean;
  VerticalView: boolean;
  actionButton;
  action = "";
  myData = null;
  formData = null;
  msgs = null;
  public imagefilename: string[];
  public logofilename: string[];
  statusMessage: string;
  saveButtonText: string = "Save";
  displayText = 'btn btn-sm btn-primary';
  displayExtLogo = 'col-md-6 d-none';
  displayNewLogo = 'col-md-6';
  displayExtImages = 'col-md-6 d-none';
  displayExtUploads = 'form-bg-section d-none';
  maxDate: string;
  validTill: string;
  extLogoUrls = [];
  extLogos = [];
  //resetButtonStyle:string="block";
  public logoresponse: string[];

  addbutton: HTMLElement;
  serverpath = environment.serverpath;
  commonimagepath = environment.imagepath;
  submitted = false;
  Id: number;

  public _allPlan: Observable<Paymentplan[]>;
  previousPlan: Paymentplan;
  remainingBannerImage: number;
  tempImageUrl: any;

  subscription: any;
  countryCodeList: Array<any> = [];
  countryCodesNew: any = (countryCodeData as any).default;
  countrycode = this.countryCodesNew;
  constructor(private toastr: ToastrService, private managebannerservice: Managebannerservice, private paymentPlanService: Paymentplanservice,
    private _userService: UserService, private _commonService: CommonService,
    private _http: HttpClient, private _router: Router, private _formbulider: FormBuilder, private route: ActivatedRoute, @Inject(DOCUMENT) document) {

  }

  //ngAfterViewInit() {
  //  document.getElementById("eventDate").setAttribute("min", this.maxDate);
  //  document.getElementById('#SaveEvent').innerHTML = this.saveButtonText;
  //}

  gettoday() {
    this.maxDate = new Date().toJSON().split('T')[0];
  }


  public uploadFinishedLogo = (event) => {
    this.logoresponse = event;
    //this.displayNewLogo = 'col-md-6 d-none';
  }

  // AutoComplete

  ngOnInit() {
    this.loggedInUserId = localStorage.getItem('userId');
    this.FillCountryDDL();
    this.FillUserDDL();
    this.gettoday();
    debugger;
    this.FormPostBanner = this._formbulider.group({
      bannerId: ['', ''],
      title: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      faxNumber: [''],
      addressStreet1: ['', ''],
      addressStreet2: ['', ''],
      countryId: ['1', ''],
      countryCodeContact: ['1', ''],
      // validTill: ['', Validators.required],
      stateId: [this.StateId, ''],
      cityId: [this.CityId, ''],
      //cityName: [this.CityName, ''],
      //stateName: [this.StateName, ''],
      zipCode: ['', Validators.pattern("^[0-9]*$")],
      postedFor: ['', Validators.required],
      postedName: ['null', ''],
      contactPersonName: ['', Validators.required],
      contactNumber: ['', [Validators.required, Validators.minLength(10)]],
      website: ['', ''],
      description: ['', ''],
      imageUrl: [],
      alternateContactNumber: [''],

      createdBy: ['', ''],
      bannerImageCount: ['']
    });
    this.getBannerDetailsById(this.route.snapshot.queryParams['bannerId']);
  }

  renderExistingImages(imageUrls, bannerId, uploadType) {
    debugger;
    if (imageUrls !== "") {
      var extImages = imageUrls.split(',');
      if (extImages.length > 0) {
        if (uploadType == "Image") {
          for (let i = 0; i < extImages.length; i++) {
            this.extLogoUrls.push(this.commonimagepath + 'Banners/' + bannerId + '/Image/' + extImages[i]);
            this.extLogos.push(extImages[i]);
            this.displayExtLogo = 'col-md-6';
          }
        }

      }

    }
  }

  onextLogoUrldelete(i) {
    this.extLogoUrls.splice(i, 1);
    this.extLogos.pop();
    this.displayExtLogo = 'col-md-6 d-none';
    this.displayNewLogo = 'col-md-6';
  }

  NavigatetoManageEvent() {
    this._router.navigate(['/theme/managebanner']);
  }

  GetUserDetails(event: any) { }

  getBannerDetailsById(id) {
    //debugger;    
    this.managebannerservice.getBannerById(id).subscribe(data => {
      //console.log(data);
      //debugger
      var data = JSON.parse(data._body);
      console.log(data);
      debugger;
      this.action = "edit";
      this.saveButtonText = "Update";
      this.displayText = 'btn btn-sm btn-primary d-none';

      this._userService.getUserById(data.postedFor).subscribe(userdetail => {
        var userdetail = JSON.parse(userdetail._body);

        console.log(userdetail);
        this.FillSubscriptionPlan(userdetail.userId);
        this.countrycode = this.countryCodesNew;
        // Ad Images Retrieval
        this.displayExtUploads = 'form-bg-section';
        if (data.imageUrl !== null) {
          this.renderExistingImages(data.imageUrl, data.bannerId, "Image");
          this.displayNewLogo = 'col-md-6 d-none';
        }
        else {
          this.displayExtImages = 'col-md-6 d-none';
        }

        if (data.isFeatured === true) {
          this.HorizontalView = true;
        }
        else if (data.isFeatured === false || data.isFeatured === null) {
          this.VerticalView = true;
        }


        // Ad Logo Retrieval
        //   if(data.adLogoUrl !==null)      
        //   this.renderExistingImages(data.adLogoUrl,data.bannerId,"Logo");
        //  else
        //  this.displayExtLogo = 'col-md-6 d-none';


        if (data.imageUrl === null)
          this.displayExtUploads = 'form-bg-section d-none';
        //this.resetButtonStyle="none";
        //  this.resetButtonStyle="{display:none}";





        this._commonService.StateDDL(data.countryId).subscribe(data => {
          debugger
          console.log(data);
          this.states = data;

        });

        this._commonService.CityDDL(data.stateId).subscribe(data => {
          debugger;
          console.log(data);
          this.cities = data;
        });

        this.countrycode = this.countryCodesNew;
     
        this._allState = this._commonService.StateDDL(data.countryId);
        this._allCity = this._commonService.CityDDL(data.stateId);

        // this._roles = this._commonService.RoleDDL();
        debugger;
        // if (data.validTill !== null)
        //   this.validTill = data.validTill.split('T')[0];
        // else
        //   this.validTill = this.maxDate;
        if (data.countryCodeContact === null)
          data.countryCodeContact = "1";

        this.tempImageUrl = data.imageUrl;

        this.FormPostBanner.setValue({
          bannerId: data.bannerId,
          title: data.title,
          email: data.email,
          faxNumber: data.faxNumber,
          addressStreet1: data.addressStreet1,
          addressStreet2: data.addressStreet2,
          countryId: data.countryId,
          stateId: data.stateId.toString(),
          cityId: data.cityId.toString(),
          zipCode: data.zipCode,
          postedFor: data.postedFor,
          postedName: data.postedForName,
          contactPersonName: data.contactPersonName,
          contactNumber: data.contactNumber,
          countryCodeContact: data.countryCodeContact.toString,
          website: data.website,
          description: data.description,
          alternateContactNumber: data.alternateContactNumber,
          // validTill: this.validTill,
          imageUrl: data.imageUrl,
          createdBy: this.loggedInUserId,
          bannerImageCount: 0
        });
      });
    });
  }

  // Insert Event
  postBanner(): void {
    debugger;
    this.submitted = true;
    this.FormPostBanner.value.adLogoUrl = this.logoresponse;
    //this.FormPostEvent.value.createdBy = this.LoggedInUserId;
    if (this.previousPlan == undefined) {
      this.remainingBannerImage = 0;
    }

    if (this.FormPostBanner.invalid) {
      this.toastr.warning("Mandatory fields missing. Please fill all mandatory fields.");
      return;
    }

    //console.log(this.FormPostBanner.value)

    debugger;
    if (this.extLogos.length > 0) {
      if (this.logoresponse == undefined || this.logoresponse == null) {
        this.logoresponse = this.extLogos.slice();
        this.bannerimage = this.logoresponse[0];
        this.FormPostBanner.value.imageUrl = this.bannerimage;
      }
    }

    if (this.FormPostBanner.value.imageUrl != this.logoresponse) {
      this.FormPostBanner.value.imageUrl = this.logoresponse;
    }

    if (this.FormPostBanner.value.imageUrl == null || this.FormPostBanner.value.imageUrl == undefined) {
      this.toastr.warning("Banner image missing. Please upload an image.");
      return;
    }

    if ((this.FormPostBanner.value.imageUrl != null) && (this.remainingBannerImage == 0)
      && (this.FormPostBanner.value.imageUrl != this.tempImageUrl)) {
      this.toastr.warning('Banner subscription exausted.');
      return;
    }

    this.managebanner.ImageUrl = this.FormPostBanner.value.imageUrl;
    this.managebanner.bannerId = this.FormPostBanner.value.bannerId;
    this.managebanner.Title = this.FormPostBanner.value.title;
    this.managebanner.Description = this.FormPostBanner.value.description;
    this.managebanner.PostedFor = this.FormPostBanner.value.postedFor;
    this.managebanner.Website = this.FormPostBanner.value.website;
    this.managebanner.AddressStreet1 = this.FormPostBanner.value.addressStreet1;
    this.managebanner.AddressStreet2 = this.FormPostBanner.value.addressStreet2;
    if ((this.FormPostBanner.value.countryId === "") || (this.FormPostBanner.value.countryId === "0")) {
      this.managebanner.CountryId = '0';
      this.managebanner.StateId = '0';
      this.managebanner.CityId = '0';
    }
    else {
      this.managebanner.CountryId = this.FormPostBanner.value.countryId;
      this.managebanner.StateId = this.FormPostBanner.value.stateId;
      this.managebanner.CityId = this.FormPostBanner.value.cityId;
    }
    this.managebanner.ZipCode = this.FormPostBanner.value.zipCode;
    this.managebanner.ContactNumber = this.FormPostBanner.value.contactNumber;
    this.managebanner.ContactPersonName = this.FormPostBanner.value.contactPersonName;
    this.managebanner.AlternateContactNumber = this.FormPostBanner.value.alternateContactNumber;
    this.managebanner.UpdatedBy = localStorage.userId;
    this.managebanner.CreatedBy = parseFloat(this.loggedInUserId);
    this.managebanner.FaxNumber = this.FormPostBanner.value.faxNumber;
    this.managebanner.Email = this.FormPostBanner.value.email;
    this.managebanner.CountryCodeContact = this.FormPostBanner.value.countryCodeContact;

    if ((this.previousPlan != undefined) && (this.FormPostBanner.value.imageUrl != this.tempImageUrl)) {
      this.managebanner.BannerImageCount = this.FormPostBanner.value.bannerImageCount;
      this.managebanner.PlanId = this.previousPlan.planId;
    }

    this.managebannerservice.updateBanner(this.managebanner)
      .pipe(first())
      .subscribe(
        data => {
          if (data.status === 200) {
            console.log(data);
            this.toastr.success('Banner updated successfully.');
            this._router.navigate(['/theme/managebanner']);
          } else {
            this.toastr.info("Issue with the service. please try again");
          }
        },
        error => {
          this.toastr.error(error);
        });
  }

  subscriptionPlanDetails(event: any) {
    debugger;
    console.log(event.target.value);

    this.paymentPlanService.getPlanDetails(event.target.value).subscribe(data => {
      var planIdDetails = JSON.parse(data._body);
      console.log(planIdDetails);
      this.previousPlan = planIdDetails;
      this.remainingBannerImage = planIdDetails.bannerImageCount;
      if (this.remainingBannerImage == 0) {
        this.toastr.info("Subscription plan exuausted. Please buy a new plan.")
      }
      this.FormPostBanner.patchValue({
        bannerImageCount: planIdDetails.bannerImageCount
      });
    });
  }

  //  Dropdowns
  FillCountryDDL() {
    debugger;
    this._allCountry = this._commonService.CountryDDL();
    this._allState = this._commonService.StateDDL(this.CountryId);
    this._allCity = this._commonService.CityDDL(this.StateId);
    this._commonService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      this.states = data;
    });
  }

  FillUserDDL() {
    debugger;
    this._allUser = this._commonService.UserDDL();
    //this._commonService.UserInboxMessageDDL().subscribe(data => {
    //  debugger;
    //  this.users = data;


    //});

  }

  FillStateDDL(event: any) {

    debugger;
    if ((event.target.value == "0") || ((event.target.value == ""))) {
      this.FormPostBanner.value.countryId = 0;
      //this._allState = this._commonService.StateDDL("0");
      var x = document.getElementsByClassName("ng-value-label");
      x[1].innerHTML = " Select State";
      x[2].innerHTML = " Select City";
      //var i;
      //for (i = 3; i < x.length; i++) {
      //  x[i].innerHTML = "";
      //}
      this.states = [];
      this.cities = [];
      this.FillCityDDL(0);
      console.log(this.states);
      this.FormPostBanner.value.stateId = 0;
      //this._allCity = this._commonService.CityDDL("0");
      this.FormPostBanner.value.cityId = 0;
    }
    else {
      this.CountryId = event.countryId;

      this._allState = this._commonService.StateDDL("1");
      debugger;
      this._commonService.StateDDL("1").subscribe(data => {
        console.log(data);
        //debugger;
        this.states = data;
      });

      this._commonService.CityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
      this._allState = this._commonService.StateDDL(this.CountryId);
      //this.CountryName = event.target.options[this.CountryId].innerHTML;
      //this.FormPostBanner.countryName = this.CountryName;
    }

  }
  
  FillCityDDL(event: any) {
    debugger;
    if (event != 0) {
      var x = document.getElementsByClassName("ng-value-label");
      //x[1].innerHTML = "--Select State--";
      x[2].innerHTML = "--Select City--";
      this.StateId = event.stateId;
      this.StateName = event.stateName;
      //this.StateName=event.target.options[this.StateId].innerHTML;
      this._allCity = this._commonService.CityDDL(this.StateId);
      this._commonService.CityDDL(this.StateId).subscribe(data => {
        console.log(data);
        debugger;
        this.cities = data;
      });
    }
    else {
      var x = document.getElementsByClassName("ng-value-label");
      this.cities = [];
    }
  }

  FillCityName(event: any) {
    debugger;
    this.CityId = event.cityId;
    this.CityName = event.cityName;
    this.FormPostBanner.value.cityName = this.CityName;
  }

  NavigatetoManageAd() {
    this._router.navigate(['/theme/managebanner']);
  }

  FillSubscriptionPlan(userId: number) {
    this._allPlan = this.paymentPlanService.UserPlanDDL(userId);
  }

  get f() { return this.FormPostBanner.controls; }


}





