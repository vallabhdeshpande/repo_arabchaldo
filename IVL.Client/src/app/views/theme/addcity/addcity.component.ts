import { Component, OnInit } from '@angular/core';
import { Country } from '../country.model';
import { Observable } from 'rxjs';
import { CityService } from '../city.service';
import { State } from '../state.model';
import { City } from '../city.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { StateService } from '../stateservice';
import { first } from 'rxjs/operators';
import { OrderPipe } from 'ngx-order-pipe';
import { ToastrService } from 'ngx-toastr';
import {NgSelectModule, NgOption} from '@ng-select/ng-select';


@Component({
  selector: 'app-addcity',
  templateUrl: './addcity.component.html',
  styleUrls: ['./addcity.component.scss']
})
export class AddcityComponent implements OnInit {

  public _allState: Observable<State[]>;
  public _allCountry: Observable<Country[]>;
  public _allCity: Observable<City[]>;  

states :any[];
countries :any[];
  CountryId: string = "0";
  CountryName: string = "";
  FormCityAdd: any;
  public logoresponse:string[];
  action = "";
  statusMessage: string;
  message:string;
  cityId=0;
  StateName: string = "";
  submitted=false;
  StateId: string = "0";
  city = new City(); order: string;
  cities: City[];
  saveButtonText:string="Save"; 
  AddCityText:string="Add City";
  displayText='btn btn-sm btn-primary';
  Cities = new City();reverse: boolean;
  actionMessage:string;
;
  public pageSize: number = 10;
  public p: number;
  public pageNumber: number = 1;
  filter: string = "";
  paginationFilter:string=this.pageSize+","+this.pageNumber;
  
  
  getCityList(): void {
    //debugger
    this.CityService.fillCityList().subscribe(cities => {
    //this.CityService.getCityList().subscribe(cities => {
      this.cities = cities.json();
      

    });
  }
       

  sortedCollection: any[];

  constructor(private OrderPipe: OrderPipe, private CityService: CityService, private router: Router, private formbulider: FormBuilder, private toastr: ToastrService) {
      this.sortedCollection = OrderPipe.transform(this.cities, 'firstName');
     
    }
  
    setOrder(value: string) {
      if (this.order === value) {
        this.reverse = !this.reverse;
      }
  
      this.order = value;
    }
  addcity(): void {
    debugger

    this.submitted = true;

    // stop the process here if form is invalid
    if (this.FormCityAdd.invalid) {
      //debugger
      return;
    }



    if (this.action == "") {
  
    debugger
    this.FormCityAdd.value.cityId=0;
   
    this.CityService.addcity(this.FormCityAdd.value)
      .subscribe((response) => {
        
        //debugger
        if (response.status === 200) {
         
          if((<any>response)._body==="true")
        
            this.toastr.success('City inserted successfully.');
          else
            this.toastr.warning('City already exist.');
            this.NavigatetoAdcity();
         
        } else {
          this.toastr.error('Problem with service. Please try again later!');
        }
      },
      );
  }

  else if (this.action = "edit") {this.CityService.updateCity(this.FormCityAdd.value)
    .pipe(first())
    .subscribe(
      data => {
        if (data.status === 200) {
         
          if((<any>data)._body==="true")
        
            this.toastr.success('City updated successfully.');
          else
            this.toastr.warning('City already exist.');
            this.NavigatetoAdcity();
         
        } else {
          this.toastr.error('Problem with service. Please try again later!');
        }
      },
      error => {
        this.toastr.error(error);
      });
      this.action="";
}

this.getCityList();
}
  
  onDelete(cityId: number,cityName,event) {
    this.cityId=cityId;
    this.actionMessage='Are you sure to delete City '+cityName+'?';
  }
  ActionDelete()
  {
    //debugger
      this.CityService.deleteCity(this.cityId).subscribe(response => {
        this.toastr.success('City deleted successfully.');
  
        this.NavigatetoAdcity();
      },
        (error) => {
          console.log(error);

          this.statusMessage = "Problem with service. Please try again later!";
        });
  }


  getCityById(cityId) {

    this.CityService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      this.states = data;
    });
    this.CityService.getCityById(cityId).subscribe(data => {
   
      ////debugger
      this.action = "edit";
      this.saveButtonText="Update"; 
      this.AddCityText="Edit City";
      this.displayText='btn btn-sm btn-primary d-none';
      var data = JSON.parse(data._body);
   
      //debugger;
      this.action = "edit";
      data.countryId=1;
      this._allState = this.CityService.StateDDL(data.countryId);


      this.FormCityAdd.setValue({
        stateId: data.stateId.toString(),
        countryId:1,
        cityName: data.cityName,
        cityId: data.cityId

      
       // adLogoUrl: data.adLogoUrl,
        //imageUrl: data.imageUrl
      });
    });
  }


  FillCountryDDL() {
    this._allCountry = this.CityService.CountryDDL();
    this._allState = this.CityService.StateDDL("1");

    this._allState = this.CityService.StateDDL(this.CountryId);
    // this.CityService.CountryDDL().subscribe(data => {
    //   console.log(data);
    //   //debugger;
    //   this.countries=data;
    // });
    this.CityService.StateDDL("1").subscribe(data => {
      console.log(data);
      //debugger;
      this.states = data;
    });
  }

  FillStateDDL(event: any) {
    //debugger;
    debugger;
    if ((event.target.value == "0") || ((event.target.value == ""))) {
      this.FormCityAdd.value.countryId = 0;
      //this._allState = this._commonService.StateDDL("0");
      var x = document.getElementsByClassName("ng-value-label");
      x[0].innerHTML = "--Select City--";
      //var i;
      //for (i = 3; i < x.length; i++) {
      //  x[i].innerHTML = "";
      //}
      this.states = [];
      this.cities = [];
      //this.FillCityDDL(0);
      console.log(this.states);
      this.FormCityAdd.value.stateId = 0;
      //this._allCity = this._commonService.CityDDL("0");
      this.FormCityAdd.value.cityId = 0;
    }
    else {
      this.CountryId = event.countryId;

      this._allState = this.CityService.StateDDL("1");
      debugger;
      this.CityService.StateDDL("1").subscribe(data => {
        console.log(data);
        //debugger;
        this.states = data;
      });
      this._allState = this.CityService.StateDDL(this.CountryId);
      this.CountryName = event.target.options[this.CountryId].innerHTML;
      this.FormCityAdd.countryName = this.CountryName;
    }

  }


  FillCityDDL(event: any) {
    if (event != 0) {
    this.StateId = event.target.value;
    //this.StateName=event.target.options[this.StateId].innerHTML;
    this._allCity = this.CityService.CityDDL(this.StateId);
    this.FormCityAdd.stateName = this.StateName;
    }
    else {
      var x = document.getElementsByClassName("ng-value-label");
      this.cities = [];
    }

  }

  
  NavigatetoAdcity()
  {
    this.ngOnInit();
    this.submitted=false;
  }
 
  ngOnInit() {
    //debugger
    this.getCityList();
    this.FillCountryDDL();
    this.saveButtonText="Save"; 
    this.AddCityText="Add City";
    this.displayText='btn btn-sm btn-primary';
 this.FormCityAdd = this.formbulider.group({
  stateId: ['22', Validators.required],
  countryId: ['1', Validators.required],
  cityName: ['', Validators.required],
  cityId:['','']
 
});
}

ngAfterViewInit(){
  document.getElementById('#SaveAd').innerHTML = this.saveButtonText; 
  document.getElementById('#AddCity').innerHTML = this.AddCityText; 
  
  } 

  getpagenum(event:any){
this.pageNumber=event;
this.getCityList();
return event;
  }
  getpagesize(event:any){
    
       this.pageSize=event.target.value.split(':')[1];
       this.getCityList();
      }

 
}
