import { Component, OnInit } from '@angular/core';
import { SubcategoryService } from '../subcategory.service';
import { Subcategory } from '../subcategory';
import { category } from '../category';
import { Observable } from 'rxjs/Observable';
import { OrderPipe } from 'ngx-order-pipe';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
@Component({
  selector: 'app-managesubcategory',
  templateUrl: './managesubcategory.component.html',
  styleUrls: ['./managesubcategory.component.scss']
})
export class ManagesubcategoryComponent implements OnInit {

  public _allCategory: Observable<category[]>;
  categoryId: string = "0";
  categoryName: string;
  public pageSize: number = 10;
  public p: number;
  //subCategoryId:string="0"
  Subcategories: Subcategory[];
  //categories: category[];
  FormCategoryAdd: any;
  submitted = false;
  categories: any[];
  FormSubcategoryAdd: any;
  statusMessage: string;
  subcategory = new Subcategory(); order: string;
  reverse: boolean;
  actionMessage:string;
  SubCategoryId:number=0;
  Subcategory = new Subcategory();;
  filter: string = "";

  constructor(private toastr: ToastrService,private OrderPipe: OrderPipe, private formbulider: FormBuilder, private router: Router, private route: ActivatedRoute, private activatedRoute: ActivatedRoute,
    private SubcategoryService: SubcategoryService) {
    this.sortedCollection = OrderPipe.transform(this.Subcategories, 'categoryName');
    console.log(this.sortedCollection);
  }


  getSubCategoryList(): void {
    //this.SubcategoryService.getSubCategoryList().subscribe(Subcategories => {
      this.SubcategoryService.FillSubcategoryList().subscribe(Subcategories => {
      this.Subcategories = Subcategories.json();
      console.log(this.Subcategories);

    });
  }

  sortedCollection: any[];
  
  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }


  addSubCategory(): void {
    debugger
    this.submitted = true;

    // stop the process here if form is invalid
    if (this.FormSubcategoryAdd.invalid) {
    debugger
    return;
    }
    console.log(this.FormSubcategoryAdd.value);
    this.FormSubcategoryAdd.value.subCategoryId= 0;
    this.SubcategoryService.addSubCategory(this.FormSubcategoryAdd.value)
      .subscribe((response) => {
        console.log(response);
        if((<any>response)._body==="true")
        this.toastr.success('Sub Category inserted successfully.');
        else
        this.toastr.info('Sub Category already exist.');
            this.ngOnInit();
            this.submitted=false;
        this.getSubCategoryList();
      },
        (error) => {
          console.log(error);

          this.statusMessage = "Problem with service. Please try again later!";
        }
      );
  }

  onDelete(SubCategoryId: number,SubCategoryName,event) {
    this.SubCategoryId=SubCategoryId;
    this.actionMessage= 'Are you sure to delete Sub Category '+SubCategoryName+'?';
  }
  ActionDelete()
  {
      this.SubcategoryService.deleteSubCategory(this.SubCategoryId).subscribe(response => {
        console.log(response);
        this.getSubCategoryList();
        if((<any>response)._body==="true")
        
        {
          this.toastr.success('Sub Category deleted successfully.');
      }
        else
        {
          this.toastr.info('This Sub Category is linked with an active post cant be deleted');
        }
     },
      
        (error) => {
          console.log(error);

          this.statusMessage = "Problem with service. Please try again later!";
        });  
  }
  updateSubCategory(Subcategory: Subcategory): void {
    debugger
    console.log(Subcategory.subCategoryId);
    //alert(Subcategory.subCategoryId);
    this.router.navigate(['/theme/editsubcategory'], { queryParams: { subCategoryId: Subcategory.subCategoryId } });
  };


  FillCategoryDDL() {
    debugger;
    this._allCategory = this.SubcategoryService.CategoryDDL();

    this.SubcategoryService.CategoryDDL().subscribe(data => {
      debugger;
      this.categories = data;


    });

  }
  FillCategoryName(event: any) {
    this.categoryId = event.CategoryId;
    this.categoryName = event.CategoryName
    this.SubcategoryService.CategoryDDL().subscribe(data => {
      debugger;
      this.categories = data;


    });
    //this.categoryName = event.target.options[this.categoryId].innerHTML;
    //alert(this.categoryName);
  }

  ngOnInit() 
  {
    {
      this.getSubCategoryList();
    }
    this.FillCategoryDDL();
    this.FormSubcategoryAdd = this.formbulider.group({
      categoryId: ['', Validators.required],
      SubCategoryName: ['', [Validators.required]],
      arabicName: ['', [Validators.required]],
      Description: [''],
      subCategoryId : []

    });
  }

}
