import { Component, OnInit } from '@angular/core';
import { Managebannerservice } from '../managebannerservice';
import {Managebanner} from '../managebanner';
import { Observable } from 'rxjs/Observable'; 
import { User } from '../user'; 
import {FormBuilder,Validators} from '@angular/forms';
import {Router} from "@angular/router";
import {AdbannerComponent} from '../adbanner/adbanner.component';
import { OrderPipe } from 'ngx-order-pipe';
import { environment } from '../../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-managebanner',
  templateUrl: './managebanner.component.html',
  styleUrls: ['./managebanner.component.scss'],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }]
})
export class ManagebannerComponent implements OnInit 
{
  public _allUser: Observable<User[]>; 
  UserId: string = "0";
  charLength: string = "0";
   managebanners: Managebanner[] =[];  
  statusMessage: string;
  managebanner = new Managebanner();order: string;
  reverse: boolean;
  users: User[];  
  user = new User();;
  setStatus: string = "";
  setType:string="";
  public pageSize: number = 10;
  public p: number;
  bannderId:bigint;
  public response: {'dbPath': ''};
  FormUserAdd: any;
  remark: string="";
  workflowstatusId:number;
  action:string="";
  sortedCollection: any[];
  id:number;
  workflowdetails:string;
  deleteremark:string;
  imagepath = environment.imagepath;
  activity:boolean;
  visibilitybutton:string;
  visibilityspan:string;
  loggedInRole:string;
  loggedInUserId:string;
  validTill: string;
  validFrom: string;
  maxDate: string;
  minDate: string;
  //minValidTillDate: string;
  //maxValidFromDate: string;
  modalexit: string = "";
  LoggedInUserId:string;
  showDates: string = "col-md-6";
  //showPremium:string="col-md-3";
  approveButtonText: string;
  approveButtonStyle: string;
  actionMessage: string;

  remarkstring: string[];
  poststring: string;
  remarkstring1: string;
  remarkstring2: string;
  remarkstring3: string;
  Ispremium = false;
  theCheckbox = false;
  filter: string = "";
  minDateModel: NgbDateStruct;
  minValidTillDate: NgbDateStruct;
  maxValidFromDate: NgbDateStruct;

  date: { year: number, month: number };
  hoveredDate: NgbDate;
  modelValidFrom: Date = null;
  modelValidTill: Date = null;

  selectToday() {
    this.minDateModel = this.calendar.getToday();
    this.minValidTillDate = this.calendar.getToday();
  }

  onvalidFromDateSelect(event: any) {
    this.minValidTillDate = event;
    this.validFrom = this.modelValidFrom.toJSON().split('T')[0];
  }

  onvalidTillDateSelect(event: any) {
    this.maxValidFromDate = event;
    this.validTill = this.modelValidTill.toJSON().split('T')[0];

  }

  public uploadFinished = (event) => 
  {
    this.response = event;
  }
 
  toggleVisibility(e){
    this.Ispremium= e.target.checked;
  }

  ngAfterViewInit() {
    document.getElementById("#modalvalidFrom").setAttribute("min", this.minDate);
    document.getElementById("#modalvalidTill").setAttribute("min", this.minDate);


  }

  gotoBannerDetails(bannerPost:Managebanner): void 
    {
      debugger
      this.router.navigate(['/theme/bannerdetailsview'],{queryParams:{id:bannerPost.bannerId}}); 
    };

  Addbanner(): void {
    this.router.navigate(['/theme/adbanner']);
  }

  updateBanner(managebanner: Managebanner): void {
    //debugger
    console.log(managebanner.bannerId);
    this.router.navigate(['/theme/editbanner'], { queryParams: { bannerId: managebanner.bannerId } });
  }

  constructor(private toastr: ToastrService, private orderPipe: OrderPipe, private managebannerservice: Managebannerservice, private formbulider: FormBuilder, private router: Router, private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private config: NgbDatepickerConfig) 
      {
        this.sortedCollection = orderPipe.transform(this.users, 'firstName');
        console.log(this.sortedCollection);
      }
  
  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }

  getStatus(Status: string) {
    //debugger;
    if (Status == "Active" || Status == "True")
      return this.setStatus = "badge badge-success";

    if (Status == "InActive" || Status == "False")
      return this.setStatus = "badge badge-danger";

    if (Status == "Pending")
      return this.setStatus = "";

  }

  getType(Premium) {
    debugger
    if (Premium == "Premium")
      return this.setStatus = "";
    else
      return this.setStatus = " ";


  }

  getBannerList(): void {
    debugger
    // this.managebanner.IsActive= "Active";
    this.managebannerservice.FillBannerList(0).subscribe(managebanners => {
      //this.managebannerservice.getBannerList(0).subscribe(managebanners => {

      if (managebanners != null) {
        //managebanners = managebanners.json();
        console.log(this.managebanners);
        // alert(this.managebanners);
        var adlenth = managebanners.length;

        //this.setType = "badge badge-warning";
        for (let i = 0; i < adlenth; i++) {
          if (managebanners[i].isVisible === true || managebanners[i].isVisible === "True") {
            managebanners[i].isVisible = "Active";
            managebanners[i].Activity = true;
            this.visibilitybutton = "hide";
            this.visibilityspan = "show";
          }
          else if (managebanners[i].isVisible === false || managebanners[i].isVisible === "False") {
            managebanners[i].isVisible = "InActive";
            managebanners[i].Activity = true;
            this.visibilitybutton = "hide";
            this.visibilityspan = "show";
          }
          else if (managebanners[i].isVisible === null || managebanners[i].isVisible === "") {
            managebanners[i].isVisible = "";
            managebanners[i].Activity = false;
            this.visibilitybutton = "show";
            this.visibilityspan = "hide";
          }
          if (managebanners[i].isFeatured === true || managebanners[i].isFeatured === "Premium" || managebanners[i].isFeatured === "True") {
            managebanners[i].isFeatured = "Premium";
            //this.setType = "badge badge-warning";
          }
          else {
            managebanners[i].isFeatured = "Featured";
            //this.setType = "badge badge-warning";
          }
        }
        this.managebanners = managebanners;
      }
      else {
        this.managebanners = [];
      }
     
    });
  }


  getPicUrl(picurl: string, bannerId) {
    if (picurl == null)
      return "assets/img/default/no_image_placeholder.jpg";
    else if (picurl == "")
      return "assets/img/default/no_image_placeholder.jpg";
    else
      return this.imagepath + 'Banners/' + bannerId + '/Image/' + picurl;
  }

  ngOnInit() {
    this.charLength = "0/200";
    this.loggedInUserId = localStorage.getItem('userId');
    //this.gettoday();
    this.selectToday();
    this.getBannerList();
  }
 
  onDelete(BannerId: bigint, event)
  {
    debugger
    this.actionMessage = 'Are you sure to delete this banner?';
    this.bannderId = BannerId;
  }

  ActionDelete()
  {
    this.action = "Delete";
    this.deleteremark = "";
    //this.workflowdetails=this.bannderId+","+this.deleteremark+","+this.action;
    this.workflowdetails = this.bannderId + "," + this.loggedInUserId + "," + this.action + "," + this.deleteremark;
    this.managebannerservice.updatewithWorkflow(this.workflowdetails).
      subscribe(response => {
        console.log(response);
        this.getBannerList();

        if (response.status === 200) {
          console.log(response);
          if ((<any>response)._body === "true")
            this.toastr.success('Banner successfully deleted.');
          else
            this.toastr.info('Banner is active on website.');
          debugger

        }
        else {
          this.toastr.error('Problem with service. Please try again later!');
        }

      },

        (error) => {
          console.log(error);

          this.toastr.error('Problem with service. Please try again later!');
        });
  }
  
  ActionOnApprove(BannerId,isFeatured, event:any)
  {
    debugger
    this.remark = null;
    this.validFrom = null;
    this.validTill = null;
    this.modelValidFrom = null;
    this.modelValidTill = null;
    this.maxValidFromDate = null;
    this.minValidTillDate = this.calendar.getToday();
    this.showDates="col-md-6";
    //this.showPremium="col-md-3";
    this.bannderId = BannerId;
    this.action = "Approve"
    this.approveButtonText = "";
    this.approveButtonText = "Approve";
    this.approveButtonStyle = "";
    this.approveButtonStyle = "btn btn-sm btn-success";
    this.actionMessage = "Are you sure to approve this record?";
    //if(isFeatured=="Premium")
    //this.Ispremium=true;
    //else
    //this.Ispremium=false;
    //this.remark=" "
  }

  ActionOnReject(BannerId, event:any)
  {
    this.remark = null;
    this.validFrom = null;
    this.validTill = null;
    this.modelValidFrom = null;
    this.modelValidTill = null;
    this.maxValidFromDate = null;
    this.minValidTillDate = this.calendar.getToday();
    this.showDates="col-md-6 d-none"; 
    //this.showPremium  ="col-md-3 d-none"
    this.bannderId = BannerId;
    this.action = "Reject"
    this.approveButtonText = "";
    this.approveButtonText = "Reject";
    this.approveButtonStyle = "";
    this.approveButtonStyle = "btn btn-sm btn-warning";
    this.actionMessage = "Are you sure to Reject this record?";
    //this.Ispremium=false;
    //this.remark=" "
  }

  ActionOnPost(remark) {
    debugger;
    console.log(this.approveButtonText);

    this.poststring = remark;

    this.remarkstring = this.poststring.split(',');
    this.remarkstring1 = this.remarkstring[0];
    this.remarkstring2 = this.remarkstring[1];
    this.remarkstring3 = this.remarkstring[2];

    //if (this.remarkstring[3] == "true")
    //  this.Ispremium = true
    //else
    //  this.Ispremium = false;
    //this.Ispremium= Boolean(this.remarkstring[3]);

    if ((this.approveButtonText == "Approve") && ((this.remarkstring1 == "null") || (this.remarkstring2 == "null") || (this.remarkstring3 == "null"))) {
      this.toastr.warning("All fields are required to approve");
      return;
    }

    if ((this.approveButtonText == "Reject") && (this.remarkstring3 == "null")) {
      this.toastr.warning("Remark is required to reject");
      return;
    }

    // this.workflowdetails=this.bannderId+","+remark+","+this.action;
    this.workflowdetails = this.bannderId + "," + this.loggedInUserId + "," + this.action + "," + remark;
   
    this.managebannerservice.updatewithWorkflow(this.workflowdetails).subscribe(response => {
      console.log(response);
      console.log('Updated');
      this.modalexit = "modal";
      document.getElementById("closeBtn").click();
      if (this.action === "Approve")
        this.toastr.success("Banner successfully approved.");
      if (this.action === "Reject")
        this.toastr.success("Banner successfully rejected.");
      this.getBannerList();
    },

      (error) => {
        console.log(error);

        this.toastr.error('Problem with service. Please try again later!');
      });
    // }
    //this.router.navigate(['/theme/postad'],{queryParams:{adId:adpost.adId}});
  }
 
  GetPostedFor(event: any)
  { 
        this.managebanner.PostedFor=event.target.value;  
  } 
}



