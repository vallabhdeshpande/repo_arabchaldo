import { Injectable, ɵɵresolveBody } from '@angular/core';
import { HttpClient, HttpHeaders,HttpClientModule } from "@angular/common/http";
import { RequestOptions,Http,Response, Headers } from '@angular/http';
import { Country } from './country.model';
import { State } from './state.model';
import { City } from './city.model';
import { category } from './category';
import { Subcategory } from './subcategory';
import { User } from './user';
import { AdDetails } from './addetails.model';
import { environment } from '../../../environments/environment';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';

@Injectable({
  providedIn: 'root'
})
export class AdDetailsService
{
  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  optionss = { headers: new HttpHeaders().set('Content-Type', 'application/json').set('Access-Control-Allow-Origin', '*').set('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS') };
  options = new RequestOptions({ headers: this.headers });
  baseUrl = environment.baseUrl;
  loggedInUserId = localStorage.getItem('userId');
  loggedInRole = localStorage.getItem('roleName');

  observable;
  getAdListData;
  getJobListData;
  cachedloggedInUserId;
  
  constructor(private _httpService: Http, private http: HttpClient) { }

  postAd(postad: AdDetails) {
    this.getAdListData = null;
    this.getJobListData = null;
    this.observable = null;
    // alert("hi");
    // alert(postad);
    console.log(postad);
    let body = JSON.parse(JSON.stringify(postad));
    console.log(body);
    //let headers = new Headers({ 'Content-Type': 'application/json' });
    // let options = new RequestOptions({ headers: headers });
    {
      return this._httpService.post(this.baseUrl + 'addetails', body, this.options);
      //return this.http.post<any>(this.baseUrl + 'addetails', body, this.optionss);
      console.log(body);
    }
  }

  updatewithWorkflow(workflowdetails: string)
  {
    this.getAdListData = null;
    this.getJobListData = null;
    this.observable = null;
    //debugger
    return this._httpService.delete(this.baseUrl + 'addetails/' + workflowdetails, this.options);
  }

  updateAd(formData: AdDetails): Observable<any>
  {
    debugger
    this.getAdListData = null;
    this.getJobListData = null;
    this.observable = null;
    let body = JSON.parse(JSON.stringify(formData));
    return this._httpService.put(this.baseUrl + 'addetails/' + formData.adId, formData, this.options);
   // return this.http.put<any>(this.baseUrl + 'addetails' + formData.adId, body, this.optionss);
  }

  deleteAd(AdId: number)
  {
    this.getAdListData = null;
    this.getJobListData = null;
    this.observable = null;
    return this._httpService.delete(this.baseUrl + 'addetails/' + AdId, this.options);
  }

  getAdList(): Observable<any>
  {
    //debugger;
    this.loggedInUserId = localStorage.getItem('userId');
    if (this.cachedloggedInUserId != this.loggedInUserId) {
      this.getAdListData = null;
      this.getJobListData = null;
      this.observable = null;
    }
    this.loggedInUserId = localStorage.getItem('userId');
    this.loggedInRole = localStorage.getItem('roleName');
    if (this.getAdListData) {
      console.log(' first if condition SA Ad list');
      return Observable.of(this.getAdListData);
    }
    //else if (this.observable)
    //{
    //  console.log('2nd if condition SA manage events');
    //  return this.observable;
    //}
    else {
      this.cachedloggedInUserId = this.loggedInUserId;
      this.observable = this.http.get(this.baseUrl + 'addetails/GetAllAdDetails?userInfo=' + this.loggedInUserId + "," + this.loggedInRole,
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          console.log('fetch data from server for SA Ad list ');
          if (response.status === 400) {
            console.log('request failed SA Ad list');
            return 'request failed';
          }
          else if (response.status === 200) {
            console.log('request successful SA Ad list');
            this.getAdListData = response.body;
            console.log(this.getAdListData);
            return this.getAdListData;
          }
        });
      return this.observable;
    }
    //return this.http.get(this.baseUrl + 'addetails/GetAllAdDetails?userInfo='+this.loggedInUserId+","+this.loggedInRole);
  }

  getJobList(): Observable<any>
  {
    //debugger;
    this.loggedInUserId = localStorage.getItem('userId');
    if (this.cachedloggedInUserId != this.loggedInUserId) {
      this.getAdListData = null;
      this.getJobListData = null;
      this.observable = null;
    }
    this.loggedInUserId = localStorage.getItem('userId');
    this.loggedInRole = localStorage.getItem('roleName');
    if (this.getJobListData)
    {
      console.log(' first if condition SA job list');
      return Observable.of(this.getJobListData);
    }
    //else if (this.observable)
    //{
    //  console.log('2nd if condition SA manage events');
    //  return this.observable;
    //}
    else
    {
      this.cachedloggedInUserId = this.loggedInUserId;
      this.observable = this.http.get(this.baseUrl + 'addetails/GetAllJobDetails?userInfo=' + this.loggedInUserId + "," + this.loggedInRole,
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          console.log('fetch data from server for SA job list ');
          if (response.status === 400)
          {
            console.log('request failed SA job list');
            return 'request failed';
          }
          else if (response.status ===200)
          {
            console.log('request successful SA job list');
            this.getJobListData = response.body;
            console.log(this.getJobListData);
            return this.getJobListData;
          }
        });
      return this.observable;
    }
    //return this.http.get(this.baseUrl + 'addetails/GetAllJobDetails?userInfo='+this.loggedInUserId+","+this.loggedInRole);
  }

  getAdDetailsById(AdId : number): Observable<any> {
    return this.http.get(this.baseUrl + 'addetails/GetAdDetailsById?adId='+AdId);
  }

  UserDDL(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl +'userdetails');
  }

  CategoryDDL(): Observable<category[]> {
     return this.http.get<category[]>(this.baseUrl + 'common/CategoryData');
  }

  SubCategoryDDL(CategoryId: string): Observable<Subcategory[]> {
    return this.http.get<Subcategory[]>(this.baseUrl + 'common/SubcategoryData?CategoryId='+CategoryId);
  }

  CountryDDL(): Observable<Country[]>
  {   
    return this.http.get<Country[]>(this.baseUrl + 'common/CountryData');
  }

  StateDDL(CountryId: string): Observable<State[]>
  {
    return this.http.get<State[]>(this.baseUrl + 'common/StateData?CountryId='+CountryId);
  }

  CityDDL(StateId: string): Observable<City[]>
  {
    return this.http.get<City[]>(this.baseUrl + 'common/CityData?StateId='+StateId);
  }
}
