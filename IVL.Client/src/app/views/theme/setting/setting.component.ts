import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import {Router, ActivatedRoute} from "@angular/router";
import { Setting } from '../setting'; 
import { ConfigurationService } from '../configurationservice';

import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {

  FormConfiguration: FormGroup;
  settings: Setting[];
  setting = new Setting();;
  htmlContent = '';
  isLoadingResults = false;
  configurationId: number = 0;
  //FormConfiguration: any;
  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '100px',

    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['insertImage', 'insertVideo']
    ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };

  constructor(private formBuilder: FormBuilder, private configurationservice: ConfigurationService, private route: ActivatedRoute, private router: Router) { }


  //   getConfiguration(): void {
  //     debugger
  //   this.configurationservice.getConfiguration().subscribe(Setting => {
  //     this.settings=Setting.json();
  //     debugger      
  //   });
  // }

  ngOnInit() {
    debugger
    this.FormConfiguration = this.formBuilder.group({
      configurationId: ['', Validators.required],
      LogoCharge: ['', Validators.required],
      ImageCharge: ['', Validators.required],
      BannerCharge: ['', Validators.required],
      Smtp: ['', Validators.required],
      Port: ['', Validators.required],
      Domain: ['', Validators.required],
      FromMailId: ['', Validators.required],
      Password: ['', Validators.required],
      ApproveMailTemplate: ['', Validators.required],
      RegistrationMailTemplate: ['', Validators.required],
      ContactMailTemplate: ['', Validators.required],
      FeedbackMailTemplate: ['', Validators.required],
      JobsMailTemplate: ['', Validators.required],
      AboutUstemplate: ['', Validators.required],
      ContactUSTemplate: ['', Validators.required],
      TermsOfUseTemplate: ['', Validators.required],
      PrivacyPolicyTemplate: ['', Validators.required],
      DisclaimerTemplate: ['', Validators.required],
      FAQTemplate: ['', Validators.required],
      AboutUstemplateArabic: ['', Validators.required],
      ContactUSTemplateArabic: ['', Validators.required],
      TermsOfUseTemplateArabic: ['', Validators.required],
      PrivacyPolicyTemplateArabic: ['', Validators.required],
      DisclaimerTemplateArabic: ['', Validators.required],
      FAQTemplateArabic: ['', Validators.required],


    });


    this.getConfiguration();
    debugger


  }

  getConfiguration() {
    debugger
    this.configurationservice.getConfiguration().subscribe(Setting => {
      this.settings = Setting.json();
      debugger
      console.log(this.settings);
      debugger
      this.FormConfiguration.setValue({
        configurationId: this.settings[0].configurationId,
        LogoCharge: this.settings[0].logoCharge,
        ImageCharge: this.settings[0].imageCharge,
        BannerCharge: this.settings[0].bannerCharge,
        Smtp: this.settings[0].smtp,
        Domain: this.settings[0].domain,
        Port: this.settings[0].port,
        FromMailId: this.settings[0].fromMailId,
        Password: this.settings[0].password,
        ApproveMailTemplate: this.settings[0].approveMailTemplate,
        ContactMailTemplate: this.settings[0].contactMailTemplate,
        FeedbackMailTemplate: this.settings[0].feedbackMailTemplate,
        JobsMailTemplate: this.settings[0].jobsMailTemplate,
        RegistrationMailTemplate: this.settings[0].registrationMailTemplate,
        AboutUstemplate: this.settings[0].aboutUstemplate,
        ContactUSTemplate: this.settings[0].contactUstemplate,
        TermsOfUseTemplate: this.settings[0].termsOfUseTemplate,
        PrivacyPolicyTemplate: this.settings[0].privacyPolicyTemplate,
        DisclaimerTemplate: this.settings[0].disclaimerTemplate,
        FAQTemplate: this.settings[0].faqtemplate,
        AboutUstemplateArabic: this.settings[0].aboutUstemplateArabic,
        ContactUSTemplateArabic: this.settings[0].contactUsTemplateArabic,
        TermsOfUseTemplateArabic: this.settings[0].termsOfUseTemplateArabic,
        PrivacyPolicyTemplateArabic: this.settings[0].privacyPolicyTemplateArabic,
        DisclaimerTemplateArabic: this.settings[0].disclaimerTemplateArabic,
        FAQTemplateArabic: this.settings[0].faqtemplateArabic      
      });
    });

  }

  updateConfiguration() {
    debugger
    this.configurationservice.updateConfiguration(this.FormConfiguration.value)
      .pipe(first())
      .subscribe(
        data => {
          if (data.status === 200) {
            console.log(data);
            alert('Configuration settings updated successfully.');
            this.router.navigate(['/theme/setting']);
          } else {
            alert(data.message);
          }
        },
        error => {
          alert(error);
        });
  }
  cancelsettingchange() {
    this.router.navigate(['/dashboard']);
  }
}

 


