import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../theme/user.service';
@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['./activation.component.scss']
})
export class ActivationComponent implements OnInit {
  confirmedEmailMessage:string;
  formModel = {
    Info: ''
  }
  constructor(private _route: ActivatedRoute, private _userService: UserService, private toastr: ToastrService) { }

  ngOnInit() {
    this.confirmedEmailMessage="Account Activated Succesfully";
    this.activateUserAccount(this._route.snapshot.queryParams['info']);
  

  }

  activateUserAccount(info) {   
    debugger;
    this._userService.activateUserAccount(info).subscribe(data => {

    });
  }

}
