import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { UserService } from '../shared/user.service';
import { AuthenticationService } from '../../views/login/auth/authentication.service';
import {WebsiteService} from '../../shared/website.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { User } from '../../shared/user';
import { LoggedInUser } from '../shared/common.model';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-dashboard',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss']
})
export class ForgotpasswordComponent implements OnInit {
  formModel = {
    Name:'',
    Email: '',
    Phone:0,
    stateId:'',
    cityId:'',
    stateName:'',
    cityName:'',
    reason:'',
    Comments:'',
    description:'',
    subject:'',
    activationCode:'',
    mailFor: '',   
    mailConfirmationUrl:'' 
  }
  
  constructor(private service: AuthenticationService, public translate: TranslateService,  private router: Router,private route: ActivatedRoute,private WebsiteService: WebsiteService, private toastr: ToastrService) { 

     // redirect to home if already logged in
    //  if (this.service.currentUserValue) { 
    //   this.router.navigate(['/dashboard']);
 // }
  }
  
  ngOnInit() 
  {
    if (localStorage.getItem('language') !== null) {
      this.translate.setDefaultLang(localStorage.getItem('language'));
      this.langselected = localStorage.getItem('language');
      this.translate.use(localStorage.getItem('language'));
      //catcontent.ngOnInit();
    }
    else {
      this.translate.setDefaultLang('English');
      this.langselected = "English";
      this.translate.use("English");

    }
  }
  langselected: string = "English";
  switchLanguage(event: any) {
    debugger;
    this.translate.use(event.target.value);
    localStorage.setItem('language', event.target.value);
    //var langselected = document.getElementById('langSelect');
    if (event.target.value == "English")
      this.langselected = "English";
    else
      this.langselected = "Arabic";
    localStorage.setItem('language', this.langselected);
    this.langselected = localStorage.getItem('language');


  }
  onSubmit(form: NgForm) 
  {
    debugger;
    this.service.GetUserInfoByEmail(form.value.Email).subscribe(
      (res: any) => {
       
    
      console.log(res);
        if (res!= null) {
          form.value.Email=res[0].Email;
          form.value.Name=res[0].FirstName+' '+res[0].LastName;
          form.value.activationCode=res[0].EncUserId;
          form.value.mailFor="ForgotPassword";
          form.value.mailConfirmationUrl=environment.mailConfirmationUrl+"#/changepassword?info="+res[0].Email+ "," +res[0].EncUserId;
          console.log(this.formModel);
          debugger;
          this.WebsiteService.forgotPassword(form.value)
          .subscribe((response) => {
           console.log(response);
            this.toastr.success('Check your mailbox to proceed further.');          
          },
          err => {
            console.log(err.status);
            if (err.status == 400)
            {
              this.toastr.error('Incorrect username or password.', 'Authentication failed.');
            
             // alert('Incorrect username or password');
            }
            else
              console.log(err);
          }
        );
          
        }
        else 
        {
          this.toastr.error('Incorrect Email Id.');

        }
      
      
      
     
      },
      err => {
        console.log(err.status);
        if (err.status == 400)
        {
          this.toastr.error('Incorrect username or password.', 'Authentication failed.');
        
         // alert('Incorrect username or password');
        }
        else
          console.log(err);
      }
    );
  }

 
  
}

  




