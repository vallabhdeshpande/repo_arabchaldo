USE [Offshore_Arabchaldo]
GO
/****** Object:  StoredProcedure [dbo].[updateAdDetails]    Script Date: 26-07-2019 15:37:31 ******/
DROP PROCEDURE [dbo].[updateAdDetails]
GO
ALTER TABLE [dbo].[UserRoles] DROP CONSTRAINT [FK_UserRoles_UserDetails]
GO
ALTER TABLE [dbo].[UserRoles] DROP CONSTRAINT [FK_UserRoles_Roles]
GO
ALTER TABLE [dbo].[UserDetails] DROP CONSTRAINT [FK_UserDetails_City]
GO
ALTER TABLE [dbo].[SubCategory] DROP CONSTRAINT [FK_SubCategory_Category]
GO
ALTER TABLE [dbo].[State] DROP CONSTRAINT [FK_State_Country]
GO
ALTER TABLE [dbo].[RolePermissions] DROP CONSTRAINT [FK_RolePermissions_Roles]
GO
ALTER TABLE [dbo].[RolePermissions] DROP CONSTRAINT [FK_RolePermissions_Permissions]
GO
ALTER TABLE [dbo].[LoginDetails] DROP CONSTRAINT [FK_LoginDetails_UserDetails]
GO
ALTER TABLE [dbo].[City] DROP CONSTRAINT [FK_City_State]
GO
ALTER TABLE [dbo].[BannerStatusDetails] DROP CONSTRAINT [FK_BannerStatusDetails_WorkflowStatus]
GO
ALTER TABLE [dbo].[BannerStatusDetails] DROP CONSTRAINT [FK_BannerStatusDetails_BannerDetails]
GO
ALTER TABLE [dbo].[AdStatusDetails] DROP CONSTRAINT [FK_AdStatusDetails_WorkflowStatus]
GO
ALTER TABLE [dbo].[AdStatusDetails] DROP CONSTRAINT [FK_AdStatusDetails_AdDetails]
GO
ALTER TABLE [dbo].[AdImageDetails] DROP CONSTRAINT [FK_AdImageDetails_AdDetails]
GO
ALTER TABLE [dbo].[AdDetails] DROP CONSTRAINT [FK_AdDetails_UserDetails]
GO
ALTER TABLE [dbo].[AdDetails] DROP CONSTRAINT [FK_AdDetails_City]
GO
ALTER TABLE [dbo].[AdDetails] DROP CONSTRAINT [FK_AdDetails_Category]
GO
/****** Object:  Table [dbo].[WorkflowStatus]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[WorkflowStatus]
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[UserRoles]
GO
/****** Object:  Table [dbo].[UserDetails]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[UserDetails]
GO
/****** Object:  Table [dbo].[SubCategory]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[SubCategory]
GO
/****** Object:  Table [dbo].[State]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[State]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[Roles]
GO
/****** Object:  Table [dbo].[RolePermissions]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[RolePermissions]
GO
/****** Object:  Table [dbo].[Permissions]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[Permissions]
GO
/****** Object:  Table [dbo].[LoginDetails]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[LoginDetails]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[Country]
GO
/****** Object:  Table [dbo].[Configuration]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[Configuration]
GO
/****** Object:  Table [dbo].[City]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[City]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[Category]
GO
/****** Object:  Table [dbo].[BannerStatusDetails]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[BannerStatusDetails]
GO
/****** Object:  Table [dbo].[BannerDetails]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[BannerDetails]
GO
/****** Object:  Table [dbo].[AdStatusDetails]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[AdStatusDetails]
GO
/****** Object:  Table [dbo].[AdImageDetails]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[AdImageDetails]
GO
/****** Object:  Table [dbo].[AdDetails]    Script Date: 26-07-2019 15:37:31 ******/
DROP TABLE [dbo].[AdDetails]
GO
/****** Object:  User [arabchaldo_user]    Script Date: 26-07-2019 15:37:31 ******/
DROP USER [arabchaldo_user]
GO
/****** Object:  User [INFOVISIONLABS\Geeta.Chauhan]    Script Date: 26-07-2019 15:37:31 ******/
DROP USER [INFOVISIONLABS\Geeta.Chauhan]
GO
/****** Object:  User [INFOVISIONLABS\Kunal.Karan]    Script Date: 26-07-2019 15:37:31 ******/
DROP USER [INFOVISIONLABS\Kunal.Karan]
GO
/****** Object:  User [INFOVISIONLABS\Ritu.Dhiria]    Script Date: 26-07-2019 15:37:31 ******/
DROP USER [INFOVISIONLABS\Ritu.Dhiria]
GO
/****** Object:  User [INFOVISIONLABS\Vallabh.Deshpande]    Script Date: 26-07-2019 15:37:31 ******/
DROP USER [INFOVISIONLABS\Vallabh.Deshpande]
GO
USE [master]
GO
/****** Object:  Database [Offshore_Arabchaldo]    Script Date: 26-07-2019 15:37:31 ******/
DROP DATABASE [Offshore_Arabchaldo]
GO
/****** Object:  Database [Offshore_Arabchaldo]    Script Date: 26-07-2019 15:37:31 ******/
CREATE DATABASE [Offshore_Arabchaldo]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Offshore_Arabchaldo', FILENAME = N'E:\SQL2017Databases\OffshoreArabchaldo.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Offshore_Arabchaldo_log', FILENAME = N'E:\SQL2017Databases\OffshoreArabchaldo_log.ldf' , SIZE = 3976KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Offshore_Arabchaldo] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Offshore_Arabchaldo].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Offshore_Arabchaldo] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET ARITHABORT OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET RECOVERY FULL 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET  MULTI_USER 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Offshore_Arabchaldo] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Offshore_Arabchaldo] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Offshore_Arabchaldo', N'ON'
GO
ALTER DATABASE [Offshore_Arabchaldo] SET QUERY_STORE = OFF
GO
USE [Offshore_Arabchaldo]
GO
/****** Object:  User [INFOVISIONLABS\Vallabh.Deshpande]    Script Date: 26-07-2019 15:37:31 ******/
CREATE USER [INFOVISIONLABS\Vallabh.Deshpande] FOR LOGIN [INFOVISIONLABS\Vallabh.Deshpande] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [INFOVISIONLABS\Ritu.Dhiria]    Script Date: 26-07-2019 15:37:31 ******/
CREATE USER [INFOVISIONLABS\Ritu.Dhiria] FOR LOGIN [INFOVISIONLABS\Ritu.Dhiria] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [INFOVISIONLABS\Kunal.Karan]    Script Date: 26-07-2019 15:37:31 ******/
CREATE USER [INFOVISIONLABS\Kunal.Karan] FOR LOGIN [INFOVISIONLABS\Kunal.Karan] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [INFOVISIONLABS\Geeta.Chauhan]    Script Date: 26-07-2019 15:37:31 ******/
CREATE USER [INFOVISIONLABS\Geeta.Chauhan] FOR LOGIN [INFOVISIONLABS\Geeta.Chauhan] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [arabchaldo_user]    Script Date: 26-07-2019 15:37:31 ******/
CREATE USER [arabchaldo_user] FOR LOGIN [arabchaldo_user] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [INFOVISIONLABS\Vallabh.Deshpande]
GO
ALTER ROLE [db_owner] ADD MEMBER [INFOVISIONLABS\Ritu.Dhiria]
GO
ALTER ROLE [db_owner] ADD MEMBER [INFOVISIONLABS\Kunal.Karan]
GO
ALTER ROLE [db_owner] ADD MEMBER [INFOVISIONLABS\Geeta.Chauhan]
GO
ALTER ROLE [db_owner] ADD MEMBER [arabchaldo_user]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [arabchaldo_user]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [arabchaldo_user]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [arabchaldo_user]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [arabchaldo_user]
GO
ALTER ROLE [db_datareader] ADD MEMBER [arabchaldo_user]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [arabchaldo_user]
GO
ALTER ROLE [db_denydatareader] ADD MEMBER [arabchaldo_user]
GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [arabchaldo_user]
GO
/****** Object:  Table [dbo].[AdDetails]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdDetails](
	[AdId] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](150) NOT NULL,
	[TagLine] [varchar](150) NULL,
	[Description] [nvarchar](max) NULL,
	[ServicesOffered] [varchar](500) NULL,
	[PostedForName] [varchar](50) NULL,
	[PostedFor] [bigint] NULL,
	[CategoryName] [varchar](50) NULL,
	[CategoryId] [bigint] NOT NULL,
	[SubCategoryName] [varchar](50) NULL,
	[SubCategoryId] [bigint] NOT NULL,
	[AdLogoUrl] [nvarchar](500) NULL,
	[ImageURL] [nvarchar](500) NULL,
	[ContactPersonName] [varchar](60) NOT NULL,
	[ContactNumber] [varchar](20) NOT NULL,
	[AlternateContactNumber] [varchar](20) NULL,
	[FaxNumber] [varchar](20) NULL,
	[Email] [varchar](50) NOT NULL,
	[Website] [nvarchar](150) NULL,
	[AddressStreet1] [varchar](150) NOT NULL,
	[AddressStreet2] [varchar](150) NULL,
	[CountryId] [bigint] NOT NULL,
	[StateName] [varchar](50) NULL,
	[StateId] [bigint] NOT NULL,
	[CityName] [varchar](50) NULL,
	[CityId] [bigint] NOT NULL,
	[ZipCode] [varchar](10) NULL,
	[AssignedTo] [bigint] NULL,
	[IsFeatured] [bit] NULL,
	[IsVisible] [bit] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_AdDetails] PRIMARY KEY CLUSTERED 
(
	[AdId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdImageDetails]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdImageDetails](
	[AdImageDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[AdImageUrl] [varchar](250) NOT NULL,
	[AdId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_AdImageDetails] PRIMARY KEY CLUSTERED 
(
	[AdImageDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdStatusDetails]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdStatusDetails](
	[AdStatusDetailsId] [bigint] IDENTITY(1,1) NOT NULL,
	[AdId] [bigint] NOT NULL,
	[WorkflowStatusId] [bigint] NOT NULL,
	[Remark] [varchar](500) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_AdStatusDetails] PRIMARY KEY CLUSTERED 
(
	[AdStatusDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BannerDetails]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BannerDetails](
	[BannerId] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](250) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[ImageUrl] [nvarchar](500) NULL,
	[PostedFor] [bigint] NOT NULL,
	[Website] [nvarchar](150) NULL,
	[IsVisible] [bit] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[PostedForName] [varchar](50) NULL,
 CONSTRAINT [PK_BannerDetails] PRIMARY KEY CLUSTERED 
(
	[BannerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BannerStatusDetails]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BannerStatusDetails](
	[BannerStatusDetailsId] [bigint] IDENTITY(1,1) NOT NULL,
	[BannerId] [bigint] NOT NULL,
	[WorkflowStatusId] [bigint] NOT NULL,
	[Remark] [varchar](500) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_BannerStatusDetails] PRIMARY KEY CLUSTERED 
(
	[BannerStatusDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryId] [bigint] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](150) NULL,
	[Description] [varchar](500) NULL,
	[CategoryImageUrl] [varchar](250) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[City]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[CityId] [bigint] IDENTITY(1,1) NOT NULL,
	[CityName] [varchar](50) NOT NULL,
	[Description] [varchar](500) NULL,
	[StateId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[CityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Configuration]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Configuration](
	[ConfigurationId] [int] IDENTITY(1,1) NOT NULL,
	[LogoCharge] [decimal](18, 2) NULL,
	[ImageCharge] [decimal](18, 2) NULL,
	[BannerCharge] [decimal](18, 2) NULL,
	[Smtp] [varchar](20) NULL,
	[Port] [int] NULL,
	[FromMailId] [varchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[RegistrationMailTemplate] [nvarchar](max) NULL,
	[ResetPasswordMailTemplate] [nvarchar](max) NULL,
	[ChangePasswordMailTemplate] [nvarchar](max) NULL,
	[ForgotPasswordMailTemplate] [nvarchar](max) NULL,
	[PostMailTemplate] [nvarchar](max) NULL,
	[ApproveMailTemplate] [nvarchar](max) NULL,
	[RejectMailTemplate] [nvarchar](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED 
(
	[ConfigurationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[CountryId] [bigint] IDENTITY(1,1) NOT NULL,
	[CountryName] [varchar](50) NOT NULL,
	[Description] [varchar](500) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoginDetails]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoginDetails](
	[LoginDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Password] [varchar](200) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_LoginDetails] PRIMARY KEY CLUSTERED 
(
	[LoginDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permissions]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permissions](
	[PermissionId] [bigint] IDENTITY(1,1) NOT NULL,
	[PermissionName] [varchar](50) NOT NULL,
	[Description] [varchar](200) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED 
(
	[PermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RolePermissions]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolePermissions](
	[RolePermissionId] [bigint] IDENTITY(1,1) NOT NULL,
	[RoleId] [bigint] NOT NULL,
	[PermissionId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_RolePermissions] PRIMARY KEY CLUSTERED 
(
	[RolePermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleId] [bigint] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](50) NOT NULL,
	[Description] [varchar](200) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[State]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[State](
	[StateId] [bigint] IDENTITY(1,1) NOT NULL,
	[StateName] [varchar](50) NOT NULL,
	[Description] [varchar](500) NULL,
	[CountryId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED 
(
	[StateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubCategory]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCategory](
	[SubCategoryId] [bigint] IDENTITY(1,1) NOT NULL,
	[SubCategoryName] [varchar](150) NULL,
	[Description] [varchar](500) NULL,
	[CategoryId] [bigint] NOT NULL,
	[CategoryName] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_SubCategory] PRIMARY KEY CLUSTERED 
(
	[SubCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserDetails]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserDetails](
	[UserId] [bigint] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](30) NOT NULL,
	[LastName] [varchar](30) NOT NULL,
	[Gender] [tinyint] NOT NULL,
	[ProfilePicUrl] [varchar](250) NULL,
	[ContactNumber] [varchar](20) NOT NULL,
	[AlternateContactNumber] [varchar](20) NULL,
	[FaxNumber] [varchar](20) NULL,
	[Email] [varchar](50) NOT NULL,
	[AddressStreet1] [varchar](150) NOT NULL,
	[AddressStreet2] [varchar](150) NULL,
	[CountryId] [bigint] NOT NULL,
	[StateName] [varchar](50) NULL,
	[CityName] [varchar](50) NULL,
	[StateId] [bigint] NOT NULL,
	[CityId] [bigint] NOT NULL,
	[RoleName] [varchar](50) NULL,
	[ZipCode] [varchar](10) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_UserDetails] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 26-07-2019 15:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[UserRoleId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[RoleId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkflowStatus]    Script Date: 26-07-2019 15:37:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkflowStatus](
	[WorkflowStatusId] [bigint] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_WorkflowStatus] PRIMARY KEY CLUSTERED 
(
	[WorkflowStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AdDetails]  WITH CHECK ADD  CONSTRAINT [FK_AdDetails_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[AdDetails] CHECK CONSTRAINT [FK_AdDetails_Category]
GO
ALTER TABLE [dbo].[AdDetails]  WITH CHECK ADD  CONSTRAINT [FK_AdDetails_City] FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([CityId])
GO
ALTER TABLE [dbo].[AdDetails] CHECK CONSTRAINT [FK_AdDetails_City]
GO
ALTER TABLE [dbo].[AdDetails]  WITH CHECK ADD  CONSTRAINT [FK_AdDetails_UserDetails] FOREIGN KEY([PostedFor])
REFERENCES [dbo].[UserDetails] ([UserId])
GO
ALTER TABLE [dbo].[AdDetails] CHECK CONSTRAINT [FK_AdDetails_UserDetails]
GO
ALTER TABLE [dbo].[AdImageDetails]  WITH CHECK ADD  CONSTRAINT [FK_AdImageDetails_AdDetails] FOREIGN KEY([AdId])
REFERENCES [dbo].[AdDetails] ([AdId])
GO
ALTER TABLE [dbo].[AdImageDetails] CHECK CONSTRAINT [FK_AdImageDetails_AdDetails]
GO
ALTER TABLE [dbo].[AdStatusDetails]  WITH CHECK ADD  CONSTRAINT [FK_AdStatusDetails_AdDetails] FOREIGN KEY([AdId])
REFERENCES [dbo].[AdDetails] ([AdId])
GO
ALTER TABLE [dbo].[AdStatusDetails] CHECK CONSTRAINT [FK_AdStatusDetails_AdDetails]
GO
ALTER TABLE [dbo].[AdStatusDetails]  WITH CHECK ADD  CONSTRAINT [FK_AdStatusDetails_WorkflowStatus] FOREIGN KEY([WorkflowStatusId])
REFERENCES [dbo].[WorkflowStatus] ([WorkflowStatusId])
GO
ALTER TABLE [dbo].[AdStatusDetails] CHECK CONSTRAINT [FK_AdStatusDetails_WorkflowStatus]
GO
ALTER TABLE [dbo].[BannerStatusDetails]  WITH CHECK ADD  CONSTRAINT [FK_BannerStatusDetails_BannerDetails] FOREIGN KEY([BannerId])
REFERENCES [dbo].[BannerDetails] ([BannerId])
GO
ALTER TABLE [dbo].[BannerStatusDetails] CHECK CONSTRAINT [FK_BannerStatusDetails_BannerDetails]
GO
ALTER TABLE [dbo].[BannerStatusDetails]  WITH CHECK ADD  CONSTRAINT [FK_BannerStatusDetails_WorkflowStatus] FOREIGN KEY([WorkflowStatusId])
REFERENCES [dbo].[WorkflowStatus] ([WorkflowStatusId])
GO
ALTER TABLE [dbo].[BannerStatusDetails] CHECK CONSTRAINT [FK_BannerStatusDetails_WorkflowStatus]
GO
ALTER TABLE [dbo].[City]  WITH CHECK ADD  CONSTRAINT [FK_City_State] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([StateId])
GO
ALTER TABLE [dbo].[City] CHECK CONSTRAINT [FK_City_State]
GO
ALTER TABLE [dbo].[LoginDetails]  WITH CHECK ADD  CONSTRAINT [FK_LoginDetails_UserDetails] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserDetails] ([UserId])
GO
ALTER TABLE [dbo].[LoginDetails] CHECK CONSTRAINT [FK_LoginDetails_UserDetails]
GO
ALTER TABLE [dbo].[RolePermissions]  WITH CHECK ADD  CONSTRAINT [FK_RolePermissions_Permissions] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[Permissions] ([PermissionId])
GO
ALTER TABLE [dbo].[RolePermissions] CHECK CONSTRAINT [FK_RolePermissions_Permissions]
GO
ALTER TABLE [dbo].[RolePermissions]  WITH CHECK ADD  CONSTRAINT [FK_RolePermissions_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[RolePermissions] CHECK CONSTRAINT [FK_RolePermissions_Roles]
GO
ALTER TABLE [dbo].[State]  WITH CHECK ADD  CONSTRAINT [FK_State_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([CountryId])
GO
ALTER TABLE [dbo].[State] CHECK CONSTRAINT [FK_State_Country]
GO
ALTER TABLE [dbo].[SubCategory]  WITH CHECK ADD  CONSTRAINT [FK_SubCategory_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[SubCategory] CHECK CONSTRAINT [FK_SubCategory_Category]
GO
ALTER TABLE [dbo].[UserDetails]  WITH CHECK ADD  CONSTRAINT [FK_UserDetails_City] FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([CityId])
GO
ALTER TABLE [dbo].[UserDetails] CHECK CONSTRAINT [FK_UserDetails_City]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Roles]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_UserDetails] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserDetails] ([UserId])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_UserDetails]
GO
/****** Object:  StoredProcedure [dbo].[updateAdDetails]    Script Date: 26-07-2019 15:37:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from AdDetails

CREATE procedure [dbo].[updateAdDetails]

AS
Begin

update AdDetails  Set CategoryName=ct.CategoryName
					,SubCategoryName=sct.SubCategoryName	
					,CityName=c.CityName
					,StateName=s.StateName
					,PostedForName=u.FirstName+' '+u.LastName
					,IsActive=1,CreatedBy=1,CreatedDate=getdate()
					FROM AdDetails ad
					INNER JOIN  Category ct ON ad.CategoryId=ct.CategoryId
					INNER JOIN  SubCategory sct ON ad.SubCategoryId=sct.SubCategoryId
					INNER JOIN  city c ON ad.CityId=c.CityId
					INNER JOIN state s on ad.StateId=s.StateId
					INNER JOIN UserDetails u on ad.postedfor=u.UserId

					

end

GO
USE [master]
GO
ALTER DATABASE [Offshore_Arabchaldo] SET  READ_WRITE 
GO
