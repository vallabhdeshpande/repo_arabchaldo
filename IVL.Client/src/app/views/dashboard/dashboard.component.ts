import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { Dashboard,BargraphData }  from '../shared/dashboard.model';
import { DashboardService} from '../shared/dashboard.service';
import { Observable } from 'rxjs/Observable';
import "rxjs/add/operator/delay";
import {DefaultLayoutComponent} from '../../../app/containers/default-layout';
import { BaseChartDirective } from 'ng2-charts';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  constructor(private router: Router, private route: ActivatedRoute,private DashboardService:DashboardService,
    private defaultlayout:DefaultLayoutComponent ) { }
  radioModel: string = 'Month';
  dashboardCountList: Dashboard[];
  BargraphDataLsit: any[] ;
  dashboard= new Dashboard;
  AdsLits: number[];
  lineChartData:any[];
  chart:any;
  //years: any[] ;
  //pie
  pieChartLabels: string[]= ['Ads', 'Banners', 'Events','Jobs','Blogs','Buy/Sell','SmallAds'];
  pieChartData: number[]=[0,0,0,0,0,0,0];
  pieChartType:string= 'pie'; 
 //donut
 doughnutChartLabels: string[]=['Online', 'Offline'];
 doughnutChartData: number[]= [0,0,0];
 doughnutChartType: string= 'doughnut';


// barChart
public barChartOptions: any = {
  scaleShowVerticalLines: false,
  responsive: true
};
public barChartLabels: string[] = ['JAN', 'FEB', 'MAR','APR', 'MAY', 'JUN', 'JUL', 'AUG','SEP','OCT', 'NOV', 'DEC'];
public barChartType = 'bar';
public barChartLegend = true;

public barChartData: any[] = [
  {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Business Listing'},
  {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Events'},
  { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Banners' },
  { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: '' },
  { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Jobs' },

  {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Advertisement'},
  {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'BuySell'}
];

  ngOnInit(){ 
  //  debugger;
    //this.FillYearDDL();
    if (localStorage.getItem('token') === null)
      this.router.navigate(['/login']);
    else {
      this.DashboardService.GetotalCount().subscribe(dashboardCountList => {
        console.log(dashboardCountList);
        debugger
        this.dashboard.ActiveBusiLitingCount = dashboardCountList[0].ActiveBusiLitingCount;
        this.dashboard.PendingBusiLitingCount = dashboardCountList[0].PendingBusiLitingCount;
        this.dashboard.ActiveSmallAdsCount = dashboardCountList[0].ActiveSmallAdsCount;
        this.dashboard.PendingSmallAdsCount = dashboardCountList[0].PendingSmallAdsCount;
        this.dashboard.ActiveBannerCount = dashboardCountList[0].ActiveBannerCount;
        this.dashboard.PendingBannerCount = dashboardCountList[0].PendingBannerCount;
        this.dashboard.ActiveEventCount = dashboardCountList[0].ActiveEventCount;
        this.dashboard.PendingEventCount = dashboardCountList[0].PendingEventCount;
        this.dashboard.ActiveJobsCount = dashboardCountList[0].ActiveJobsCount;
        this.dashboard.PendingJobsCount = dashboardCountList[0].PendingJobsCount;
        this.dashboard.ActiveBuysellCount = dashboardCountList[0].ActiveBuysellCount;
        this.dashboard.PendingBuysellCount = dashboardCountList[0].PendingBuysellCount;
        this.dashboard.ActiveBlogsCount = dashboardCountList[0].ActiveBlogsCount;
        this.dashboard.PendingBlogsCount = dashboardCountList[0].PendingBlogsCount;
        this.dashboard.UserCount = dashboardCountList[0].UserCount;
        this.dashboard.AdsCount = dashboardCountList[0].AdsCount;
        this.dashboard.BannerCount = dashboardCountList[0].BannerCount;
        this.dashboard.EventCount = dashboardCountList[0].EventCount;
        this.dashboard.JobsCount = dashboardCountList[0].JobsCount;
        this.dashboard.BlogsCount = dashboardCountList[0].BlogsCount;
        this.dashboard.buysellCount = dashboardCountList[0].buysellCount;
        this.dashboard.smallAdsCount = dashboardCountList[0].smallAdsCount;
        this.dashboard.TotalRevenueAmount = dashboardCountList[0].TotalRevenueAmount;
        this.dashboard.OnlineAmount = dashboardCountList[0].OnlineAmount;
        this.dashboard.OfflineAmount = dashboardCountList[0].OfflineAmount;
        this.dashboard.paidUser = dashboardCountList[0].paidUser;
        this.dashboard.FreeUser = dashboardCountList[0].FreeUser;
        this.dashboard.TotalAdvertimentCount = dashboardCountList[0].TotalAdvertimentCount;
        this.dashboard.paidAdvertisementCount = dashboardCountList[0].paidAdvertisementCount;
        this.dashboard.freeAdvertisementCount = dashboardCountList[0].freeAdvertisementCount;
        this.dashboard.MessageTopicCount = dashboardCountList[0].MessageTopicCount;
        this.dashboard.MessageReplyCount = dashboardCountList[0].MessageReplyCount;
        //Pie Chart
        this.pieChartData = [Number(this.dashboard.AdsCount), Number(this.dashboard.BannerCount), Number(this.dashboard.EventCount), Number(this.dashboard.JobsCount), Number(this.dashboard.BlogsCount) , Number(this.dashboard.buysellCount), Number(this.dashboard.smallAdsCount)];

        // Dount
        this.doughnutChartData = [Number(this.dashboard.OnlineAmount), Number(this.dashboard.OfflineAmount)];



      });
      //   // Bar graph 
    var year = new Date().getFullYear();
     this.GetDashboardBargraphdata(year);

      console.log(this.pieChartData);
    }
    debugger;
    var x = document.getElementById("barchatdatagraph");
    
  } 

  GetDashboardBargraphdata(year:number)
  {
    this.DashboardService.GetBargraphdata(year).subscribe(BargraphDataLsit => {
      console.log(BargraphDataLsit);
      this.barChartData = [
        { data: JSON.parse(BargraphDataLsit[0].businessAdscountList), label: 'Business Listing' },
        { data: JSON.parse(BargraphDataLsit[0].bannerCountList), label: 'Events' },
        { data: JSON.parse(BargraphDataLsit[0].eventsCountList), label: 'Banners' },
        { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: '' },
        { data: JSON.parse(BargraphDataLsit[0].jobsCountList), label: 'Jobs' },
        { data: JSON.parse(BargraphDataLsit[0].smallAdsCountList), label: 'Advertisement' },
        { data: JSON.parse(BargraphDataLsit[0].buySellCountList), label: 'BuySell' }
      ];
      //this.defaultlayout.ngOnInit();

    });
  }
  years=[  
    { yearid: 19, yeardata: "2019" },
    { yearid: 20, yeardata: "2020" },
    { yearid: 21, yeardata: "2021" }
    ] 
  FillDataYearWise(event: any) {
   debugger;
    //var year = new Date().getFullYear();
    var year=  event.target.value;   
    this.GetDashboardBargraphdata(Number(year));
    }
      
 
}


