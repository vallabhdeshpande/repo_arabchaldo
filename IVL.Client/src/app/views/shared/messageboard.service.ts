import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders,HttpClientModule, HttpErrorResponse } from "@angular/common/http";
import { RequestOptions,Http,Response, Headers } from '@angular/http';
import {FormBuilder,Validators} from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/merge';



import { category} from '../../views/shared/common.model';
import { MessageBoard,MessageBoardDetails} from '../../Website/messageboard/messageboard.model';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})

export class MessageboardService {
  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });
  constructor(private _httpService: Http,private http:HttpClient){ }
  baseUrl = environment.baseUrl;

  postTopic(messageboard: MessageBoard)
  {
    debugger
    let body = JSON.parse(JSON.stringify(messageboard));
     console.log(messageboard);
       {          
        return this._httpService.post(this.baseUrl +'messageboard', body, this.options);    
    }
  }
  getMessageBoardList(): Observable<any> {
    return this.http.get(this.baseUrl+'messageboard');
  } 
  getMessageBoardById(messageboardid: number): Observable<any>
  {
    return this.http.get(this.baseUrl + 'messageboard/' + messageboardid);
  }
deleteMessageBoard(messageboardid: number,topic:string){
  return this._httpService.delete(this.baseUrl + 'messageboard/' + messageboardid + "," + topic, this.options);
}


// Post Reply
postReply(messageboarddetails: MessageBoardDetails)
  {
    debugger;
    let body = JSON.parse(JSON.stringify(messageboarddetails));
     console.log(messageboarddetails);
       {          
        return this._httpService.post(this.baseUrl +'messageboard/InsertMessageBoardDetails', body, this.options);    
    }
  }

  getMessageBoardDetailsListByBoardId(messageboardid: number): Observable<any> {
    let httpOptions = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=UTF-8').set('Access-Control-Allow-Origin', '*').set('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS') };
    return this.http.get(this.baseUrl+'messageboard/GetMessageBoardReplyByMessageBoardId?MessageBoardId='+ messageboardid, httpOptions);
  }
  deleteMessageBoardDetails(messageBoardId: number, reply: string) {
    return this._httpService.delete(this.baseUrl + 'messageboard/' + messageBoardId + "," + reply, this.options);
  }




}





