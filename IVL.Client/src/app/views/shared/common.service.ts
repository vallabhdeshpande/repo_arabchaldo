import { Injectable, ɵɵresolveBody } from '@angular/core';
import { HttpClient, HttpHeaders,HttpClientModule } from "@angular/common/http";
import { RequestOptions,Http,Response, Headers } from '@angular/http';
import { Country,State,City,category,Subcategory,Roles } from '../shared/common.model';
import { User } from '../theme/user';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';


@Injectable({
  providedIn: 'root'
})
export class CommonService
{  
  baseUrl = environment.baseUrl;
  observable;
  PopularEventcategoriesData;
  PopularJobSubcategoriesData;
  
  constructor(private _httpService: Http, private http: HttpClient)
  {
   
  }
  
  RoleDDL(): Observable<Roles[]> {return this.http.get<Roles[]>(this.baseUrl +'common/RoleData'); } // Get All Roles
  UserDDL(): Observable<User[]> {return this.http.get<User[]>(this.baseUrl +'userdetails'); }
  CategoryDDL(): Observable<category[]> {return this.http.get<category[]>(this.baseUrl + 'common/CategoryData');} // Get All Categories
  SubCategoryDDL(CategoryId: string): Observable<Subcategory[]> { return this.http.get<Subcategory[]>(this.baseUrl + 'common/SubcategoryData?CategoryId=' + CategoryId); }
  getUserById(userId: number): Observable<any> {  return this._httpService.get(this.baseUrl + 'userdetails/' + userId); }
  CountryDDL(): Observable<Country[]>{return this.http.get<Country[]>(this.baseUrl + 'common/CountryData');} // Get All Countries
  StateDDL(CountryId: string): Observable<State[]>{return this.http.get<State[]>(this.baseUrl + 'common/StateData?CountryId='+CountryId);} // Get All States by CountryId
  StateDataByCountry(CountryId: string): Observable<State[]>{return this.http.get<State[]>(this.baseUrl + 'common/StateDataByCountry?CountryId='+CountryId);} // Get All States by CountryId
  CityDDL(StateId: string): Observable<City[]>{return this.http.get<City[]>(this.baseUrl + 'common/CityData?StateId='+StateId);} // Get All Cities by StateId
  BUCityDDL(StateId: string): Observable<City[]> { return this.http.get<City[]>(this.baseUrl + 'common/BUCityData?StateId=' + StateId); } // Get All Cities by StateId
  UserByRoleDDL(RoleId: string): Observable<User[]> { return this.http.get<User[]>(this.baseUrl + '/Common/UserDDLbyRole?RoleId=' + RoleId); }
  Popularcategories(): Observable<category[]> { return this.http.get<category[]>(this.baseUrl + 'common/popularcategory'); }

  PopularEventcategories(): Observable<category[]>
  {
    //C9 change
    //debugger;
    if (this.PopularEventcategoriesData)
    {

      console.log(' first if condition for PopularEventcategories');
      return Observable.of(this.PopularEventcategoriesData);
    }
    //else if (this.observable) {

    //}
    //else
    {
      //debugger;
      console.log('3rd if condition PopularEventcategories ');
      this.PopularEventcategoriesData = this.http.get<category[]>(this.baseUrl + 'common/PopularEventsCategory',
        { observe: 'response' }).
        map(response => {
          this.observable = null;
          this.PopularEventcategoriesData = null;
          console.log('fetch PopularcategoriesData from backend ');
          if (response.status === 400)
          {
            console.log('fetch data from server for PopularEventcategories()');
            return ' problem with service for PopularEventcategories';
          }
          else if (response.status === 200)
          {
            console.log('fetched PopularEventcategories from backend successfully');
            this.PopularEventcategoriesData = response.body;
            return this.PopularEventcategoriesData;
          }
        });
      console.log('outside else cond in PopularEventcategories for observable return');
      return this.PopularEventcategoriesData;
    }

    //return this.http.get<category[]>(this.baseUrl + 'common/PopularEventsCategory');
  }


  PopularJobSubCategories(): Observable<Subcategory[]> {
    //C9 change
    //debugger;
    if (this.PopularJobSubcategoriesData) {

      console.log(' first if condition for PopularJobsubcategories');
      return Observable.of(this.PopularJobSubcategoriesData);
    }
    //else if (this.observable) {

    //}
    //else
    {
      //debugger;
      console.log('3rd if condition PopularJobssubcategories ');
      this.PopularJobSubcategoriesData = this.http.get<category[]>(this.baseUrl + 'common/PopularJobsubCategory',
        { observe: 'response' }).
        map(response => {
          this.observable = null;
          this.PopularJobSubcategoriesData = null;
          console.log('fetch PopularJobsubcategoriesData from backend ');
          if (response.status === 400) {
            console.log('fetch data from server for PopularJobssubcategories()');
            return ' problem with service for PopularJobssubcategories';
          }
          else if (response.status === 200) {
            console.log('fetched PopularJobsubcategories from backend successfully');
            this.PopularJobSubcategoriesData = response.body;
            return this.PopularJobSubcategoriesData;
          }
        });
      console.log('outside else cond in PopularJobsubcategories for observable return');
      return this.PopularJobSubcategoriesData;
    }

    //return this.http.get<category[]>(this.baseUrl + 'common/PopularEventsCategory');
  }


  JobCategoryDDL(): Observable<category[]> {return this.http.get<category[]>(this.baseUrl + 'common/JobCategoryData');} // Get All Categories
  DisplayWorkFlow(Id:string,PostTypeId:string): Observable<any>{return this.http.get(this.baseUrl + '/Common/DisplayWorkFlow?workflowparams='+Id+","+PostTypeId);}
  UserInboxMessageDDL(): Observable<any[]> {return this.http.get<any[]>(this.baseUrl +'common/UserData'); }    
}
