import { Injectable } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { RequestOptions, Http, Response, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });
  constructor(private fb: FormBuilder, private http: HttpClient, private _httpService: Http) { }
  readonly BaseURI = 'http://localhost:57284/api';

  formModel = this.fb.group({
    UserName: ['', Validators.required],
    Email: ['', Validators.email],
    FullName: [''],
    Passwords: this.fb.group({
      Password: ['', [Validators.required, Validators.minLength(4)]],
      ConfirmPassword: ['', Validators.required]
    }, { validator: this.comparePasswords })

  });

  comparePasswords(fb: FormGroup) {
    let confirmPswrdCtrl = fb.get('ConfirmPassword');
    //passwordMismatch
    //confirmPswrdCtrl.errors={passwordMismatch:true}
    if (confirmPswrdCtrl.errors == null || 'passwordMismatch' in confirmPswrdCtrl.errors) {
      if (fb.get('Password').value != confirmPswrdCtrl.value)
        confirmPswrdCtrl.setErrors({ passwordMismatch: true });
      else
        confirmPswrdCtrl.setErrors(null);
    }
  }

  register() {
    var body = {
      UserName: this.formModel.value.UserName,
      Email: this.formModel.value.UserName,
      FullName: this.formModel.value.FullName,
      Password: this.formModel.value.Passwords.Password
    };
    return this._httpService.post(this.BaseURI + '/ApplicationUser/Register', body,this.options);
  }

  login(formData) {
   
    return this._httpService.post(this.BaseURI + '/Login/Login', formData, this.options);
  }

  getUserProfile() {
    return this.http.get(this.BaseURI + '/UserProfile');
  }
  onLogout() {
    return this.http.get(this.BaseURI + '/Login/Logout');
  }
}
