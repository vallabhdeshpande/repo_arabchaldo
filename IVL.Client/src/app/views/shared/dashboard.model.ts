export class Dashboard {

    public ActiveBusiLitingCount: number;
    public PendingBusiLitingCount: number;
    public ActiveSmallAdsCount: number;
    public PendingSmallAdsCount: number;
    public ActiveBannerCount: number;
    public PendingBannerCount: number;
    public ActiveEventCount: number;
    public PendingEventCount: number;
    public ActiveJobsCount: number;
    public PendingJobsCount: number;
    public ActiveBlogsCount: number;
    public PendingBlogsCount: number;
    public ActiveBuysellCount: number;
    public PendingBuysellCount: number;
    public  UserCount: number;
    public  AdsCount:number;
    public  BannerCount:number;
    public  JobsCount:number;
    public  EventCount:number;
    public  BlogsCount:number;
    public  buysellCount:number;
    public  smallAdsCount:number;
    public  TotalRevenueAmount:number;
    public  OfflineAmount:number;
    public  OnlineAmount:number;
    public  freeAdvertisementCount:number;
    public  FreeUser:number;
    public  paidAdvertisementCount:number;
    public  paidUser:number;
    public  TotalAdvertimentCount:number;
    public  adscountList: string;
    public  bannerCountList:string;
    public  eventsCountList:string;
    public  FreeAds:number;
    public  PaidAds:number;
    public  ApprovedAds:number;
    public  RejectedAds:number;
    public  PendingAds:number;
    public  FreeBanner :number;
    public  PaidBanner:number;
    public  ApprovedBanner:number;
    public  RejectedBanner:number;
    public  PendingBanner :number;
    public  FreeEvents:number;
    public  PaidEvents:number;
    public  ApprovedEvents:number;
    public  RejectedEvents:number;
    public  PendingEvents:number;
    public  FreeJobs:number;
    public  PaidJobs:number;
    public  ApprovedJobs:number;
    public  RejectedJobs:number;
    public  PendingJobs:number;
    public  AdsImageCount:number;
    public  BannerImageCount:number;
    public  EventImageCount:number;
    public  LogoCount:number;
    public  JobsAdvCount:number;
    public  BuySellCount:number;
    public  SmallAdsCount:number;
    public  MessageTopicCount:number;
    public  MessageReplyCount:number;
    constructor() { 
    }
}
export class BargraphData {

    public  adscountList: string;
    public  bannerCountList:string;
    public  eventsCountList:string;

    constructor() { 
    }
}
