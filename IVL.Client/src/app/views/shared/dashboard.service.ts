import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Dashboard,BargraphData} from '../shared/dashboard.model';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { User } from '../theme/user';
import { Paymentplan } from '../../views/theme/paymentplan';
import { Paymentplanservice } from '../../views/theme/paymentplanservice';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  baseUrl = environment.baseUrl;
  
  constructor(private _httpService: Http, private http: HttpClient)
  {
   
  }
  GetotalCount(): Observable<Dashboard[]> { return this.http.get<Dashboard[]>(this.baseUrl + 'DashboardDetails/GetotalCount'); }

  GetBUtotalCount(userId: string): Observable<Dashboard[]> { return this.http.get<Dashboard[]>(this.baseUrl + 'DashboardDetails/GetBUtotalCount?UserId=' + userId); }

  GetBargraphdata(Year:Number): Observable<any[]> { return this.http.get<any[]>(this.baseUrl + 'DashboardDetails/GetBargraphdata?Year='+Year); }

  UserPlanList(userId: string, location?: string): Observable<Paymentplan[]>
  {
    if (location)
      return this.http.get<Paymentplan[]>(this.baseUrl + 'PaymentPlan/PlanListForUser?userId=' + userId + "&loginUser=" + location);
    else
      return this.http.get<Paymentplan[]>(this.baseUrl + 'PaymentPlan/PlanListForUser?userId=' + userId);
  }

  getPlanDetails(planId: number): Observable<any> {
    return this._httpService.get(this.baseUrl + 'PaymentPlan/PlanIdDetails?planId=' + planId);
  }
}
