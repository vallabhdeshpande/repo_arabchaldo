import { Component } from '@angular/core';
import { UserService } from './../theme/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { NgForm, FormGroup , FormBuilder, Validators} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {Managebanner} from '../theme/managebanner'
import { environment } from '../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from '../../views/login/auth/authentication.service';
import { AuthService, SocialUser, GoogleLoginProvider, FacebookLoginProvider, LinkedinLoginProvider } from 'ng4-social-login';
// import {UserService}



@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})

export class RegisterComponent 
{
  registerForm: any;
  submitted = false;
  loggedInRole: string;
  returnUrl: string;
  public user: any = SocialUser
  UserName: [];
  langselected: string = "English";

  constructor(private service: AuthenticationService,private router: Router, public translate: TranslateService, private route: ActivatedRoute, private formbulider: FormBuilder, private toastr: ToastrService, private userService: UserService, private SocialAuthserice: AuthService) 
  { }

  //Social login
  SocialLogin(user) {
    debugger;
    user.name = user.name.split(" ");
    user.FirstName = user.name[0];
    user.LastName = user.name[1];
    user.ProfilePicurl = user.photoUrl;
    this.service.Sociallogin(user).pipe(first()).subscribe(
      (res: any) => {
        debugger;
        this.router.navigate(['/budashboard']);
      });
  }
  Googlelogin() {
    debugger
    this.SocialAuthserice.signIn(GoogleLoginProvider.PROVIDER_ID).then((userData) => {
    this.user = userData;
      this.SocialLogin(this.user);
    });
    //this.SocialLogin(this.user);
  }
  facebooklogin() {
    debugger
    this.SocialAuthserice.signIn(FacebookLoginProvider.PROVIDER_ID).then((userData) => {
    this.user = userData;
      console.log(this.user);

    });
    //this.SocialLogin(this.user);
  }
  Linkdenlogin() {
    debugger
    this.SocialAuthserice.signIn(LinkedinLoginProvider.PROVIDER_ID).then((userData) => {
    this.user = userData;
      console.log(this.user);
    });
    //this.SocialLogin(this.user);
  }

  Twitterlogin() {
    debugger
    this.SocialAuthserice.signIn(LinkedinLoginProvider.PROVIDER_ID).then((userData) => {
      this.user = userData;
      console.log(this.user);
    });
  }

  ngOnInit()
  {
    debugger;

    this.registerForm = this.formbulider.group({
      userId :[''],
      firstName: ['', [Validators.required,Validators.maxLength(50), Validators.pattern(".*\\S.*[a-zA-z ]")]],  
      lastName: ['', [Validators.required,Validators.maxLength(50), Validators.pattern(".*\\S.*[a-zA-z ]")]],      
      gender: ['', [Validators.required]],
      contactNumber: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      email: ['', [Validators.required, Validators.email]],
      password:['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],
      confirmPassword:['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],
      activationUrl:['','']
    });  
    if (localStorage.getItem('language') !== null) {
      this.translate.setDefaultLang(localStorage.getItem('language'));
      this.langselected = localStorage.getItem('language');
      this.translate.use(localStorage.getItem('language'));
      //catcontent.ngOnInit();
    }
    else {
      this.translate.setDefaultLang('English');
      this.langselected = "English";
      this.translate.use("English");

    }
  }
  
  switchLanguage(event: any) {
    debugger;
    this.translate.use(event.target.value);
    localStorage.setItem('language', event.target.value);
    //var langselected = document.getElementById('langSelect');
    if (event.target.value == "English")
      this.langselected = "English";
    else
      this.langselected = "Arabic";
    localStorage.setItem('language', this.langselected);
    this.langselected = localStorage.getItem('language');


  }
  get fun() 
  { 
    return this.registerForm.controls; 
  }
   

  onSubmit(): void
  {
    debugger;
    console.log(this.registerForm.value);
    this.submitted = true;
      if (this.registerForm.invalid) {
        return;
      }
   

    this.registerForm.value.userId = 0;
    this.registerForm.value.activationUrl=environment.mailConfirmationUrl+"activation?info="+this.registerForm.value.email;

    //if(this.registerForm.invalid)
    //{
    //  //alert("Missing/Incorrect fields in registration form");
    //  this.toastr.warning("Missing/Incorrect data entered.");
    //  return;
    //}

    if(this.registerForm.value.password != this.registerForm.value.confirmPassword)
    {
      this.toastr.warning("Password does not match. Please enter same password.");
      return;
    }

    

    if (this.registerForm.value.gender == 1)
        this.registerForm.value.gender = "1";
      else
        this.registerForm.value.gender = "2";
     
    
    this.userService.add(this.registerForm.value)
        .subscribe((response) => {
          console.log(response);

          if((<any>response)._body==="true")
          {
            this.toastr.success('You are successfully registered with arabchaldo.com. Activation Link is sent on your registered mail.  ');
            //this.router.navigate['/login'];
            this.router.navigate(['/login']);
          }
          else
          this.toastr.info("User Already exist.");      

          //this.Navigatetomanageuser();
        },

          (error) => {
            console.log(error);

            this.toastr.error("Problem with service. Please try again later!");
          }
        );  
  }
}
