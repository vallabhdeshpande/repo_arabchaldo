import { Component, OnInit } from '@angular/core';
import { UserService } from '../theme/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/internal/operators/first';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangepasswordComponent implements OnInit {
  FormResetPassword: any;
  userId = 0;
  submitted = false;
  userInfo: string = "";
  useremail: string = "";
  constructor(private toastr: ToastrService, private UserService: UserService, private route: ActivatedRoute, private formbulider: FormBuilder, private router: Router) { }

  ngOnInit() {
    debugger;
    this.userInfo = this.route.snapshot.queryParams['info'];
    this.getLoginUserById(this.userInfo.split(',')[1]);
    this.FormResetPassword = this.formbulider.group({
      userId: ['', ''],

      email: [this.useremail, Validators.required],
      password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],
      confirmpassword: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]]

    });
    
  }
  getLoginUserById(userId) {

    this.UserService.getLoginUserById(userId).subscribe(data => {
      var data = JSON.parse(data._body);
      console.log(data);
      this.FormResetPassword.setValue({
        userId: data.userId,
        email: data.email,
        password: '',
        confirmpassword: ''
      });
    });

  }
  get f() {
    return this.FormResetPassword.controls;
  }

  ResetPassword(): void {
    this.submitted = true;
    // stop here if form is invalid
    if (this.FormResetPassword.invalid) {
      return;
    }
    debugger

    if (this.FormResetPassword.value.password == this.FormResetPassword.value.confirmpassword) {
      this.FormResetPassword.value.password;
    }
    else {
      this.toastr.warning("Password does not match")
      return;
    }
    this.UserService.ResetPassword(this.FormResetPassword.value)
      .pipe(first())
      .subscribe(
        data => {
          if (data.status === 200) {
            console.log(data);
            this.toastr.success('Password updated successfully.');
            this.router.navigate(['/theme/manageuser']);
          } else {
            this.toastr.info(data.message);
          }
        },
        error => {
          this.toastr.error(error);
        });
  }

  Navigatetomanageuser() {
    this.router.navigate(['/theme/manageuser']);
  }

}
