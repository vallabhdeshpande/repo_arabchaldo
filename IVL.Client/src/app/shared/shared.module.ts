import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { LoaderComponent } from '../loader/loader.component';
import { LoaderService } from '../loader/loader.service';
import { LoaderInterceptor } from '../loader/loader.interceptor';

@NgModule({
  declarations: [LoaderComponent],
  imports: [
    CommonModule
    
  ],
  exports: [LoaderComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }],
 
})
export class SharedModule { }  
