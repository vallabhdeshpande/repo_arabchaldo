import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import{ User } from '../shared/user.model';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService 
{
  baseUrl = environment.baseUrl;
  constructor(private _httpService: Http,private http:HttpClient){ }

  getUserContactdetailsById(userId: User): Observable<any> 
  {
    debugger
    return this._httpService.get(this.baseUrl + 'userdetails/' +userId);
  }
}