import { Injectable, ɵɵresolveBody } from '@angular/core';
import { HttpClient, HttpHeaders,HttpClientModule } from "@angular/common/http";
import { RequestOptions,Http,Response, Headers } from '@angular/http';
import { Country,State,City,category,Subcategory,Roles } from '../shared/common.model';
import { User } from '../../app/shared/user.model';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';
//import { observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CommonService {
  
  baseUrl = environment.baseUrl;
  StateDDLData;
  CategoryDDLData;
  PopularcategoriesData;
  observable;
  
  constructor(private _httpService: Http,private http:HttpClient ){
   
   }
  
  RoleDDL(): Observable<Roles[]> {return this.http.get<Roles[]>(this.baseUrl +'common/RoleData'); } // Get All Roles
  UserDDL(): Observable<User[]> {return this.http.get<User[]>(this.baseUrl +'userdetails'); }
  //CategoryDDL(): Observable<category[]> {return this.http.get<category[]>(this.baseUrl + 'common/CategoryData');} // Get All Categories
  SubCategoryDDL(CategoryId: string): Observable<Subcategory[]> { return this.http.get<Subcategory[]>(this.baseUrl + 'common/SubcategoryData?CategoryId=' + CategoryId); }

  CountryDDL(): Observable<Country[]>{ return this.http.get<Country[]>(this.baseUrl + 'common/CountryData'); } // Get All Countries

  StateDDL(CountryId: string): Observable<State[]>
  {
    // C4 change
    if (this.StateDDLData) {
      //debugger;
      console.log('StateDDLData  first if condtn');
      console.log(this.StateDDLData);
      console.log(Observable.of(this.StateDDLData));
      return Observable.of(this.StateDDLData);
    }
    //else if (this.observable) {
    //  console.log('2nd if condition for AllCity DDL');
    //  return this.observable;
    //}
    else {
      //debugger;
      console.log('3rd if condition CategoryDDL ');
      this.observable = this.http.get<State[]>(this.baseUrl + 'common/StateData?CountryId=' + CountryId,
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          this.StateDDLData = null;
          console.log('fetch CategoryDDL from backend ');
          if (response.status === 400) {
            console.log('fetch data from server');
            return ' problem with service';
          }
          else if (response.status === 200) {
            console.log('fetched CategoryDDL from backend successfully');
            this.StateDDLData = response.body;
            return this.StateDDLData;
          }
        });
      console.log('outside else cond in CategoryDDL ');
      return this.observable;
    }
    //return this.http.get<State[]>(this.baseUrl + 'common/StateData?CountryId=' + CountryId);
  } // Get All States by CountryId

  CategoryDDL(): Observable<category[]>
  {
    // C4 change
    if (this.CategoryDDLData) {
      //debugger;
      console.log('CategoryDDLData  first if condtn');
      console.log(this.CategoryDDLData);
      console.log(Observable.of(this.CategoryDDLData));
      return Observable.of(this.CategoryDDLData);
    }
    //else if (this.observable) {
    //  console.log('2nd if condition for AllCity DDL');
    //  return this.observable;
    //}
    else {
      debugger;
      console.log('3rd if condition CategoryDDL ');
      this.observable = this.http.get<category[]>(this.baseUrl + 'common/CategoryData',
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          this.CategoryDDLData = null;
          console.log('fetch CategoryDDL from backend ');
          if (response.status === 400) {
            console.log('fetch data from server');
            return ' problem with service';
          }
          else if (response.status === 200) {
            console.log('fetched CategoryDDL from backend successfully');
            this.CategoryDDLData = response.body;            
            return this.CategoryDDLData;
          }
        });
      console.log('outside else cond in CategoryDDL ');
      return this.observable;
    }
    //return this.http.get<State[]>(this.baseUrl + 'common/StateData?CountryId=' + CountryId);
  } // Get All States by CountryId

  StateDataByCountry(CountryId: string): Observable<State[]>{ return this.http.get<State[]>(this.baseUrl + 'common/StateDataByCountry?CountryId=' + CountryId); } // Get All States by CountryId
  CityDDL(StateId: string): Observable<City[]>{ return this.http.get<City[]>(this.baseUrl + 'common/CityData?StateId=' + StateId); } // Get All Cities by StateId
  BUCityDDL(StateId: string): Observable<City[]> { return this.http.get<City[]>(this.baseUrl + 'common/BUCityData?StateId=' + StateId); } // Get All Cities by StateId
  UserByRoleDDL(RoleId: string): Observable<User[]> { return this.http.get<User[]>(this.baseUrl + '/Common/UserDDLbyRole?RoleId=' + RoleId); }

  Popularcategories(): Observable<category[]>
  {
    // C5 change
    if (this.PopularcategoriesData) {
      //debugger;
      console.log('PopularcategoriesData  first if condtn');
      console.log(this.PopularcategoriesData);
      console.log(Observable.of(this.PopularcategoriesData));
      return Observable.of(this.PopularcategoriesData);
    }
    //else if (this.observable) {
    //  console.log('2nd if condition for AllCity DDL');
    //  return this.observable;
    //}
    else {
      //debugger;
      console.log('3rd if condition PopularcategoriesData ');
      this.observable = this.http.get<category[]>(this.baseUrl + 'common/popularcategory',
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          this.PopularcategoriesData = null;
          console.log('fetch PopularcategoriesData from backend ');
          if (response.status === 400) {
            console.log('fetch data from server');
            return ' problem with service';
          }
          else if (response.status === 200) {
            console.log('fetched PopularcategoriesData from backend successfully');
            this.PopularcategoriesData = response.body;
            return this.PopularcategoriesData;
          }
        });
      console.log('outside else cond in CategoryDDL ');
      return this.observable;
    }
    //return this.http.get<category[]>(this.baseUrl + 'common/popularcategory');
  }
  //getCategoryList():<any[]>{return this.http.get<any[]>(this.baseUrl + 'common/CategoryData');} // Get All Countries

  // getCategoryList(): Observable<any> {
  //   return this._httpService.get(this.baseUrl + 'common/manageCategory');
  // }
}



//}

