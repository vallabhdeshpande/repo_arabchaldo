import { Observable } from 'rxjs';

/**
* Created by Tareq Boulakjar. from angulartypescript.com
*/
export class EventDetails {

  public eventId: number;
  public UpdatedBy: string;
  public Title: string;
  public Description: string;
  public Website: string;
  public AddressStreet1: string;
  public AddressStreet2: string;
  public CountryId: string;
  public StateId: string;
  public CityId: string;
  public ZipCode: string;
  public IsActive: boolean;
  public IsVisible: boolean;
  public Activity: boolean;
  public PostedFor: number;
  public PostedForName: string
  public WorkflowstatusId: string;
  public Remarks: string;
  public ImageUrl: string;
  public CreatedDate: string;
  public EventDate: string;
  public CreatedBy: string;
  public Time: string;
  public visitorCount: number;
 
  public ContactPersonName: string;
  public CountryCode: string;
  public ValidFromDate: Date;
  public ValidTillDate: Date;
  public ContactNumber: string;
  public AlternateContactNumber: string;
  public FaxNumber: string;
  public Email: string;
 
  public CountryName: string;
  public StateName: string;
  public CityName: string;
    IsPaidAd: string;
 
  constructor() {
  }
}

export class WorkflowstatusDTO {

  public workflowStatusId: number;
  public Status: string;
  public IsActive: boolean;
  public CreatedDate: string;
  public CreatedBy: string;
  public updatedDate: string;
  public updatedBy: string;
  constructor() {
  }

}

