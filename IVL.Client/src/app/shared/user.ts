/**
 * Created by Tareq Boulakjar. from angulartypescript.com
 */
export class User {
    
	    public Id: number;
        public FirstName: string;
        public LastName: string;
        public Gender: number;
        public ProfilePicUrl: string;
        public ContactNumber: number;
		public Email: string;
		public FaxNumber: string;
        public AddressStreet1: string;
        public AddressStreet2: string;
		public CountryId: string;
        public StateId: string;
        public CityId: string;
		public ZipCode: string
        public CreatedBy: string;
        public CreatedDate:string;
        constructor() { 
        }

     

        
}
