
export class Common {}
export class Country {
    CountryId: number;
    CountryName: string;
   
   }

   export class State {
    //CountryId: number;
    StateId: number;
    StateName: string;
   
   }


   export class City {
    StateId: number;
    CityId: number;
    CityName: string;
     }

   export class category {  
     CategoryId: string;
     CategoryName: string;
     ArabicName: string;
     CategoryDescription: string;
     CategoryImageUrl: string;
    } 
     
    export class Subcategory {
      public subCategoryId: string;
      public subCategoryName: string;
      public categoryId: string;
      public ArabicName: string;
           }    

    export class Roles {  
        RoleId:string;
        RoleName: string;
    }


export class LoggedInUser {
  Token: string;
  Email: string;
  FirstName: string;
  LastName:string;
  UserId:string;
  RoleId:string;
  RoleName:string;
  ProfilePic:string;

}

export class CountryCodes {
  name: string;
  dial_code: string;
  code: string;

}

export enum AppRole {
  SuperAdmin = 'SuperAdmin',
					Admin = 'Admin',
					BusinessUser = 'Business User',
					ThirdPartyUser = 'Third Party User',
					RegisteredUser = 'Registered User'

}
