export class User {
    public userId: number;
    public FirstName: string;
    public LastName: string;
    public gender: number;
    public roleId: string;
    public roleName:string;
    public ProfilePicUrl: string;
    public ContactNumber: number;
    public Email: string;
    public FaxNumber: string;
    public AlternateContactNumber: string;
    public AddressStreet1: string;
    public AddressStreet2: string;
    public CountryId: string;
    public StateId: string;
    public CityId: string;
    public cityName:string;
    public ZipCode: string
}



export class EmailSender {
    Name:string;
    Email: string;
    Phone:number;
    stateId:string;
    cityId:string;
    stateName:string;
    cityName:string;
    reason:string;
    Comments:string;
    description:string;
    categoryName:string;
    subcategoryName:string;
    categoryId:string;
    subcategoryId:string;
    subject:string;
    activationCode:string;
  mailFor: string;
    content:string;
  ToUserMessage: [];
  confirmationEmailUrl: string;
  }


