export class Manageblog {

   
    public blogId : bigint;
    public Title : string;
    public Description : string;
    public ImageUrl : string;
    public IsActive : boolean;
    public isVisible:boolean;
    public Activity:boolean;
    public CreatedDate : Date;
    public CreatedBy : bigint;
    public UpdatedDate : Date;
    public UpdatedBy : bigint;
    //public PostedFor : bigint;
    public PostedForName : string;
    public ValidTill : Date;
    public VisitorCount : number;

    constructor()
    {}
}
