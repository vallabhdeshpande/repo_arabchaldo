import { Injectable, ɵɵresolveBody } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';
import { observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpClientModule } from "@angular/common/http";
import { RequestOptions, Http, Response, Headers } from '@angular/http';
import { AdDetails } from '../views/theme/addetails.model';
import { environment } from '../../environments/environment';
import { User, EmailSender } from '../shared/user.model';
import { Country, State, City, category, Subcategory, Roles, CountryCodes } from '../shared/common.model';




@Injectable({
  providedIn: 'root'
})


export class WebsiteService
{
  PremiumBannerData;
  CategoryData;
  CategorySearchData;
  CityData;
  CitySearchData;
  CategoryDDLData;
  AdListData;
  JobsListData;
  BannerData;
  FeatureBannerData;
  BuySellListingData;
  EventsBycategoryIdData;
  BlogListData;
  SmallAdsListData;
  observable;
  data;

  headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS' });
  options = new RequestOptions({ headers: this.headers });
  baseUrl = environment.baseUrl;
  imagepath = environment.imagepath;
  constructor(private _httpService: Http, private http: HttpClient) { }

  add(EmailSender: User) {
    console.log(EmailSender);
    let body = (EmailSender);
    console.log(EmailSender);
    {
      return this._httpService.post(this.baseUrl + 'Website', body, this.options);
      console.log(body);
    }
  }

  contact(EmailSender: User) {
    console.log(EmailSender);
    let body = (EmailSender);
    console.log(EmailSender);
    {
      return this._httpService.post(this.baseUrl + 'Website', body, this.options);
      console.log(body);
    }
  }

  multipleemail(EmailSender: User) {
    console.log(EmailSender);
    let body = (EmailSender);
    console.log(EmailSender);
    {
      return this._httpService.post(this.baseUrl + 'Website', body, this.options);
      console.log(body);
    }
  }

  buyplan(EmailSender: User) {
    console.log(EmailSender);
    let body = (EmailSender);
    console.log(EmailSender);
    {
      return this._httpService.post(this.baseUrl + 'Website', body, this.options);
      console.log(body);
    }
  }

  forgotPassword(EmailSender: User) {
    debugger;
    console.log(EmailSender);
    let body = (EmailSender);
    console.log(EmailSender);
    {
      return this._httpService.post(this.baseUrl + 'Website', body, this.options);
      console.log(body);
    }
  }

  getAdList(): Observable<any>
  {
    //C6 change
    ////debugger
    if (this.AdListData) {
      console.log(' first if condition for getAdList');
      return Observable.of(this.AdListData);
    }
    //else if (this.observable) {
    //  console.log('2nd if condition for getAdList');
    //  return this.observable;
    //}
    else {
      this.AdListData = this.http.get(this.baseUrl + 'Website/GetAllAdDetails?imgurl=' + this.imagepath + ',0,0',
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          if (response.status === 400)
          {
            console.log('error while fetching PremiumBannerList from server');
            return ' problem with service';
          }
          else if (response.status === 200)
          {
            console.log('fetch AdList from server successfully');
            this.AdListData = response.body;
            return this.AdListData;
          }

        });
      
    }
    console.log('AdList end')
    return this.AdListData;

    //return this.http.get(this.baseUrl + 'Website/GetAllAdDetails?imgurl='+this.imagepath+',0,0');
  }

  getJobsList(): Observable<any>
  {
    //C7 change
    //debugger;
    if (this.JobsListData)
    {
      console.log(' first if condition for JobsList');
      return Observable.of(this.JobsListData);
    }
    //else if (this.observable) {
    //  console.log('2nd if condition for JobsList');
    //  return this.observable;
    //}
    else { 
      this.observable = this.http.get(this.baseUrl + 'Website/GetAllJobDetails?imgurl=' + this.imagepath + ',0,0',
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          this.JobsListData = null;
          if (response.status === 400) {
            console.log('error while fetching JobsList from server');
            return ' problem with service';
          }
          else if (response.status === 200) {
            console.log('fetch JobsList from server successfully');
            this.JobsListData = response.body;
            return this.JobsListData;
          }

        });
      return this.observable;
    }
    //console.log('JobsList end')
    
    //return this.http.get(this.baseUrl + 'Website/GetAllJobDetails?imgurl='+this.imagepath+',0,0');
  }

  getAdDetailsById(AdId: number): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetAdDetailsById?adId=' + AdId);
  }

  getAdListByCategoryId(categoryId: number): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetAllAdDetails?imgurl='+this.imagepath+','+categoryId+',0');
  }

  getAdListBySubcategoryId(SubcategoryId: number): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetAllAdDetails?imgurl='+this.imagepath+',0,'+SubcategoryId);
  }

  getJobListBySubcategoryId(SubcategoryId: number): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetAllJobDetails?imgurl=' + this.imagepath + ',0,' + SubcategoryId);
  }

  //Small Ads Section

  getSmallAdList(type: string): Observable<any> {
    ////C6 change
    //////debugger
    //if (this.SmallAdsListData) {
    //  console.log(' first if condition for getSmallAdList');
    //  return Observable.of(this.SmallAdsListData);
    //}
    ////else if (this.observable) {
    ////  console.log('2nd if condition for getAdList');
    ////  return this.observable;
    ////}
    //else {
    //  this.SmallAdsListData = this.http.get(this.baseUrl + 'Website/GetAllSmallAdDetails?imgurl=' + this.imagepath + ',' + type,
    //    { observe: 'response' })
    //    .map(response => {
    //      this.observable = null;
    //      if (response.status === 400) {
    //        console.log('error while fetching Premium Small Ads list from server');
    //        return ' problem with service';
    //      }
    //      else if (response.status === 200) {
    //        console.log('fetch Small AdList from server successfully');
    //        this.SmallAdsListData = response.body;
    //        return this.SmallAdsListData;
    //      }

    //    });

    //}
    //console.log('Small AdList end')
    //return this.SmallAdsListData;

    //return this.http.get(this.baseUrl + 'Website/GetAllAdDetails?imgurl='+this.imagepath+',0,0');
    return this.http.get(this.baseUrl + 'Website/GetAllSmallAdDetails?imgurl=' + this.imagepath + ',' + type);
  }

  getbuySellrecentList(type: string): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetAllBuysellRecentData?imgurl=' + this.imagepath + ',' + type);
  }
  //getSmallAdList(): Observable<any> {
  //  return this.http.get(this.baseUrl + 'Website/GetAllSmallAdDetails?imgurl=' + this.imagepath);
  //}
  getSmallAdDetailsById(SmallAdId: number): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetSmallAdDetailsById?smalladId=' + SmallAdId);
  }


  getBannerList(): Observable<any>
  {
    //C8 change
    if (this.BannerData)
    {
      console.log(' first if condition for BannerData');
      //console.log(this.BannerData);
      return Observable.of(this.BannerData);
    }
    //else if (this.observable) {
    //  console.log('2nd if condition for BannerData');
    //  return this.observable;
    //}
    else {
      this.BannerData = this.http.get(this.baseUrl + 'Website/GetAllBanner?imgurl=' + this.imagepath,
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          if (response.status === 400) {
            console.log('error while fetching BannerData from server');
            return ' problem with service';
          }
          else if (response.status === 200) {
            console.log('fetch BannerData from server successfully');
           // console.log(this.BannerData);
            this.BannerData = response.body;
            return this.BannerData;
          }

        });
    }
    console.log('BannerData end')
    return this.BannerData;
    //return this.http.get(this.baseUrl + 'Website/GetAllBanner?imgurl=' + this.imagepath);
  }

  getFeaturedBannerListing(): Observable<any> {
    //C8 change
    if (this.FeatureBannerData) {
      console.log(' first if condition for FeatureBannerData');
      console.log(this.FeatureBannerData);
      return Observable.of(this.FeatureBannerData);
    }
    //else if (this.observable) {
    //  console.log('2nd if condition for BannerData');
    //  return this.observable;
    //}
    else {
      this.FeatureBannerData = this.http.get(this.baseUrl + 'Website/GetAllBanner?imgurl=' + this.imagepath,
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          if (response.status === 400) {
            console.log('error while fetching FeatureBannerData from server');
            return ' problem with service';
          }
          else if (response.status === 200) {
            console.log('fetch FeatureBannerData from server successfully');
            console.log(this.FeatureBannerData);
            this.FeatureBannerData = response.body;
            return this.FeatureBannerData;
          }

        });
    }
    console.log('FeatureBannerData end')
    return this.FeatureBannerData;
    //return this.http.get(this.baseUrl + 'Website/GetAllBanner?imgurl=' + this.imagepath);
  }

  getPremiumBannerList(): Observable<any>
  {
    //return this.http.get(this.baseUrl + 'Website/getPremiumBannerList?imgurl=' + this.imagepath);
    //C1 change
    //debugger

    if (this.PremiumBannerData) {
      console.log(' first if condition for Premium Banner');
      return Observable.of(this.PremiumBannerData);
    }
    //else if (this.observable) {
    //  console.log('2nd if condition for Premium Banner');
    //  return this.observable;
    //}
    else {
      console.log('3rd if condition');
      this.PremiumBannerData = this.http.get(this.baseUrl + 'Website/getPremiumBannerList?imgurl=' + this.imagepath
        , { observe: 'response' })
        .map(response => {
          this.observable = null;
          console.log('fetch PremiumBannerList from server');
          if (response.status === 400)
          {
            console.log('error while fetching PremiumBannerList from server');
            return ' problem with service';
          }
          else if (response.status === 200)
          {
            console.log('fetch PremiumBannerList from server successfully');
            this.PremiumBannerData = response.body;
            return this.PremiumBannerData;
          }
        })
    };
    console.log('End');
    return this.PremiumBannerData;
    //return this.observable;
    //return this.observable;
    //return this.http.get(this.baseUrl + 'Website/getPremiumBannerList?imgurl=' + this.imagepath);
  }


  getBannerById(BannerId: number): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetBannerById?BannerId=' + BannerId);
  }

  getBlogList(): Observable<any>
  {
    //C6 change
    ////debugger
    if (this.BlogListData) {
      console.log(' first if condition for BlogListData');
      return Observable.of(this.BlogListData);
    }
    //else if (this.observable) {
    //  console.log('2nd if condition for getAdList');
    //  return this.observable;
    //}
    else {
      this.observable = this.http.get(this.baseUrl + 'Website/GetAllBlog',
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          this.BlogListData = null;
          if (response.status === 400) {
            console.log('error while fetching BlogListData from server');
            return ' problem with service';
          }
          else if (response.status === 200) {
            console.log('fetch BlogListData from server successfully');
            this.BlogListData = response.body;
            return this.BlogListData;
          }

        });
      return this.observable;
    }

    //return this.http.get(this.baseUrl + 'Website/GetAllBlog');
  }

  getBlogbyId(blogId: number): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetBlogById?BlogId=' + blogId);
  }

  getEventList(): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetAllEvent');
  }

  getEventDetailsById(EventId: number): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetEventDetails?eventId=' + EventId);
  }

  getEventsBycategoryId(categoryId: number): Observable<any>
  {
    //C10 change 
    //debugger;
    //if (this.EventsBycategoryIdData) {
    //  console.log('EventsBycategoryId  first if condtn');
    //  return Observable.of(this.EventsBycategoryIdData);
    //}
    ////else if (this.observable) {

    ////}
    //else {
    //  console.log('3rd if condition EventsBycategoryId');
    //  this.observable = this.http.get(this.baseUrl + 'Website/GetEventsByCategoryId?categoryId=' + categoryId,
    //    { observe: 'response' })
    //    .map(response => {
    //      this.observable = null;
    //      this.EventsBycategoryIdData = null;
    //      console.log('fetch EventsBycategoryId from backend ');
    //      if (response.status === 400) {
    //        console.log('fetch data from server');
    //        return ' problem with service';
    //      }
    //      else if (response.status === 200) {
    //        console.log('fetched EventsBycategoryId from backend successfully on website');
    //        this.EventsBycategoryIdData = response.body;
    //        return this.EventsBycategoryIdData;
    //      }
    //    });
    //  console.log('outside else cond in EventsBycategoryId');
    //  return this.observable;
    //}
    return this.http.get(this.baseUrl + 'Website/GetEventsByCategoryId?categoryId=' + categoryId);
  }

  // Get All Categories
  CategoryDDL(): Observable<category[]>
  {
    //C2 change
    if (this.CategoryDDLData) {
      //debugger;
      console.log('CategoryDDL  first if condtn');
      console.log(this.CategoryDDLData);
      console.log(Observable.of(this.CategoryDDLData));
      return Observable.of(this.CategoryDDLData);
    }
    //else if (this.observable) {
    //  console.log('2nd if condition for AllCity DDL');
    //  return this.observable;
    //}
    else {
      //debugger;
      console.log('3rd if condition CategoryDDL ');
      this.observable = this.http.get<category[]>(this.baseUrl + 'Website/GetAllCategoryData',
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          this.CategoryDDLData = null;
          console.log('fetch CategoryDDL from backend ');
          if (response.status === 400) {
            console.log('fetch data from server');
            return ' problem with service';
          }
          else if (response.status === 200) {
            console.log('fetched CategoryDDL from backend successfully');
            this.CategoryDDLData = response.body;
            return this.CategoryDDLData;
          }
        });
      console.log('outside else cond in CategoryDDL ');
      return this.observable;
    }
    //debugger;
   
    console.log(' category End');
    //return this.observable;
    //return this.http.get<category[]>(this.baseUrl + 'Website/GetAllCategoryData');
  }  

   // Get All Categories in Search DDL
  CategorySearchDDL(): Observable<category[]> {
    //C2 change
    if (this.CategorySearchData) {
      //debugger;
      console.log('CategorySearchDDL  first if condtn');
      console.log(this.CategorySearchData);
      console.log(Observable.of(this.CategorySearchData));
      return Observable.of(this.CategorySearchData);
    }
    //else if (this.observable) {
    //  console.log('2nd if condition for AllCity DDL');
    //  return this.observable;
    //}
    else {
      //debugger;
      console.log('3rd if condition CategorySearchDDL ');
      this.observable = this.http.get<category[]>(this.baseUrl + 'Website/GetCategorySearchDDL',
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          this.CategorySearchData = null;
          console.log('fetch CategorySearchDDL from backend ');
          if (response.status === 400) {
            console.log('fetch data from server');
            return ' problem with service';
          }
          else if (response.status === 200) {
            console.log('fetched CategorySearchDDL from backend successfully');
            this.CategorySearchData = response.body;
            return this.CategorySearchData;
          }
        });
      console.log('outside else cond in CategorySearchDDL ');
      return this.observable;
    }
    //debugger;

    console.log(' category Search End');
    //return this.observable;
    //return this.http.get<category[]>(this.baseUrl + 'Website/GetAllCategoryData');
  } // Get All Categories

  // Get All Cities by StateId
  AllCityDDL(): Observable<City[]>
  {
    //C3 change 
    if (this.CityData) {
      //debugger;
      console.log('AllCityDDL  first if condtn');
      return Observable.of(this.CityData);
    }
    //else if (this.observable) {
    //  console.log('2nd if condition for AllCity DDL');
    //  return this.observable;
    //}
    else
    {
      //debugger;
      console.log('3rd if condition AllCity DDl');
      this.observable = this.http.get<City[]>(this.baseUrl + 'Website/GetAllCityData',
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          this.CityData = null;
          console.log('fetch AllCityDDL from backend ');
          if (response.status === 400) {
            console.log('fetch data from server');
            return ' problem with service';
          }
          else if (response.status === 200) {
            console.log('fetched AllCityDDL from backend successfully');
            this.CityData = response.body;
            return this.CityData;
          }
        });
      console.log('outside else cond in city ddl');
      return this.observable;
    }
    
    //return this.http.get<City[]>(this.baseUrl + 'Website/GetAllCityData');
  }

  // Get All Cities in Search DDL

  CitySearchDDL(): Observable<City[]> {
    //C3 change 
    if (this.CitySearchData) {
      //debugger;
      console.log('CitySearchDDL  first if condtn');
      return Observable.of(this.CitySearchData);
    }
    //else if (this.observable) {
    //  console.log('2nd if condition for AllCity DDL');
    //  return this.observable;
    //}
    else {
      //debugger;
      console.log('3rd if condition City Search DDl');
      this.observable = this.http.get<City[]>(this.baseUrl + 'Website/GetCitySearchDDL',
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          this.CitySearchData = null;
          console.log('fetch CitySearchDDL from backend ');
          if (response.status === 400) {
            console.log('fetch data from server');
            return ' problem with service';
          }
          else if (response.status === 200) {
            console.log('fetched CitySearchDDL from backend successfully');
            this.CitySearchData = response.body;
            return this.CitySearchData;
          }
        });
      console.log('outside else cond in city search ddl');
      return this.observable;
    }

    //return this.http.get<City[]>(this.baseUrl + 'Website/GetAllCityData');
  }

  getAdListBySearchCriteria(searchCriteria: string): Observable<any> {
    let httpOptions = { headers: new HttpHeaders().set('Content-Type', 'application/json').set('Access-Control-Allow-Origin', '*').set('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS') };
    return this.http.get(this.baseUrl + 'Website/GetAdListBySearchCriteria?searchCriteria=' + searchCriteria+',' +this.imagepath , httpOptions);
  }

  getAboutUsData(): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetAboutUSData');
  }

  GetContactUSData(): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetContactUSData');
  }

  GetPrivacyPolicyData(): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetPrivacyPolicyData');
  }

  GetTermsofUseData(): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetTermsofUseData');
  }

  GetFAQData(): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetFAQData');
  }

  GetDisclaimerData(): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetDisclaimerData');
  }

  getBuySellListing(): Observable<any>
  {
    //C10 change 
    //debugger;
    if (this.BuySellListingData)
    {
      console.log('BuySellListing  first if condtn');
      return Observable.of(this.BuySellListingData);
    }
    //else if (this.observable) {

    //}
    else
    {
      console.log('3rd if condition BuySellListing');
      this.observable = this.http.get(this.baseUrl + 'Website/GetAllBuysellData?imgurl=' + this.imagepath,
        { observe: 'response' })
        .map(response => {
          this.observable = null;
          this.BuySellListingData = null;
          console.log('fetch AllCityDDL from backend ');
          if (response.status === 400)
          {
            console.log('fetch data from server');
            return ' problem with service';
          }
          else if (response.status === 200)
          {
            console.log('fetched BuySellListing from backend successfully on website');
            this.BuySellListingData = response.body;
            return this.BuySellListingData;
          }
        });
      console.log('outside else cond in BuySellListing');
      return this.observable;
    }
    //return this.http.get(this.baseUrl + 'Website/GetAllBuysellData?imgurl=' + this.imagepath);
  }

  getBuyellById(buysellId: number): Observable<any> {
    return this.http.get(this.baseUrl + 'Website/GetBuysellById?buysellId=' + buysellId);
  }
}
