export class Managebanner {

    
  public bannerId: number;
    public title : string;
    public description : string;
    public imageUrl : string;
    public isActive : boolean;
    public website: string;
    public createdDate : Date;
  public createdBy: number;
    public updatedDate : Date;
  public updatedBy: number;
  public postedFor: number;
    public postedForName : string;
    public BannerImageUrl: [];
  public visitorCount: number;
  public ContactPersonName: string;
  public CountryCode: string;
  public ValidFromDate: Date;
  public ValidTillDate: Date;
  public ContactNumber: string;
  public AlternateContactNumber: string;
  public FaxNumber: string;
  public Email: string;
  public Website: string;
  public AddressStreet1: string;
  public AddressStreet2: string;
  public CountryId: string;
  public StateId: string;
  public CityId: string;
  public CountryName: string;
  public StateName: string;
  public CityName: string;
  public ZipCode: string;
    constructor()
    {}
}
