import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
//import { LocationStrategy, HashLocationStrategy, PathLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';import { HttpModule } from '@angular/http';
import { SharedModule } from '../app/shared/shared.module';
import { ToastrModule } from 'ngx-toastr';
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider, LinkedinLoginProvider } from 'ng4-social-login';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
import { BubannerdetailsviewComponent } from './Website/bubannerdetailsview/bubannerdetailsview.component';
import { BueventdetailviewComponent } from './Website/bueventdetailview/bueventdetailview.component';
//import { BueditbannerComponent } from './Website/bueditbanner/bueditbanner.component';
//import { BuediteventComponent } from './Website/bueditevent/bueditevent.component';
import { UploadComponent } from './Website/upload/upload.component'
import { MultiUploadComponent } from './Website/multiupload/multiupload.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { BumanageblogComponent } from './Website/bumanageblog/bumanageblog.component'; 
import { BuadblogComponent } from './Website/buadblog/buadblog.component';
import { BumanagejobsComponent } from './Website/bumanagejobs/bumanagejobs.component';
import { BublogdetailsviewComponent } from './Website/bublogdetailsview/bublogdetailsview.component';
import { BuysellComponent } from './Website/buysell/buysell.component';
import { NgxStripeModule } from 'ngx-stripe';
import { StripeComponent } from './Website/stripe/stripe.component';
import { BuyselldetailsComponent } from './Website/buyselldetails/buyselldetails.component';
import { ComboBoxComponent } from './Website/combo-box/combo-box.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
const config= new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    //provider: new GoogleLoginProvider("41544443339-03uu5d7emhqfi8cmsvsc12r0lh9ss3jd.apps.googleusercontent.com")
   provider: new GoogleLoginProvider("986299493849-565d6cr16dr912m4rheridi7ru6dlrvk.apps.googleusercontent.com")  
  }  ,

  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("193855155242004")
  } ,

  {
    id: LinkedinLoginProvider.PROVIDER_ID,
    provider: new LinkedinLoginProvider("81sowxf0dwqj1b")
  },

  
],true);

export function getAuthServiceConfigs() {
  return config;
}
import { AppComponent } from './app.component';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';

import { RegisterComponent } from './views/register/register.component';
const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,

  AppSidebarModule,
  

 
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { ForgotpasswordComponent } from './views/forgotpassword/forgotpassword.component';
import { themeComponent } from './views/theme/theme.component';
import { EditcategoryComponent } from './editcategory/editcategory.component';
import { EditsubcategoryComponent } from './editsubcategory/editsubcategory.component';


import { WebsiteComponent } from './Website/website.component';
import { MainheadComponent } from './Website/mainhead/mainhead.component';
import { BannerComponent } from './Website/banner/banner.component';
import { ContentComponent } from './Website/content/content.component';
import { MainfooterComponent } from './Website/mainfooter/mainfooter.component';
import { AboutComponent } from './Website/about/about.component';
import { EventsComponent } from './Website/events/events.component';
import { JobsComponent } from './Website/jobs/jobs.component';
import { PopularCategoryComponent } from './Website/popular-category/popular-category.component';
import { CategoryComponent } from './Website/category/category.component';
import { InnerContentComponent } from './Website/inner-content/inner-content.component';
import { InnerBannerComponent } from './Website/inner-banner/inner-banner.component';
import { AdDetailComponent } from './Website/ad-detail/ad-detail.component';
import { BannerDetailComponent } from './Website/banner-detail/banner-detail.component';
import { FaqComponent } from './Website/faq/faq.component';
import { ByplanComponent } from './Website/byplan/byplan.component';
import { BuyplanComponent } from './Website/buyplan/buyplan.component';
import { ContactComponent } from './Website/contact/contact.component';
import { PrivacyPolicyComponent } from './Website/privacy-policy/privacy-policy.component';
import { TermsUseComponent } from './Website/terms-use/terms-use.component';
import { DisclaimerComponent } from './Website/disclaimer/disclaimer.component';

import { SlickModule } from 'ngx-slick';
import { BudashboardComponent } from './Website/budashboard/budashboard.component';
import { BupostadComponent } from './Website/bupostad/bupostad.component';
import { BupostpaidadComponent } from './Website/bupostpaidad/bupostpaidad.component';
import { BupostbannerComponent } from './Website/bupostbanner/bupostbanner.component';
import { BuprofileComponent } from './Website/buprofile/buprofile.component';
import { BusidebarComponent } from './Website/busidebar/busidebar.component';
import { DashboardBannerComponent } from './Website/dashboard-banner/dashboard-banner.component';
import { BuposteventComponent } from './Website/bupostevent/bupostevent.component';
import { BuresetpasswordComponent } from './Website/buresetpassword/buresetpassword.component';
import { BumanageadComponent } from './Website/bumanagead/bumanagead.component';
import { BumanagebannerComponent } from './Website/bumanagebanner/bumanagebanner.component';
import { BumanageeventComponent } from './Website/bumanageevent/bumanageevent.component';
import { EventDetailComponent } from './Website/event-detail/event-detail.component';
import { BuaddetailsviewComponent } from './Website/buaddetailsview/buaddetailsview.component';
//import { AdsCategorywiseComponent } from './Website/ads-categorywise/ads-categorywise.component';
import { AdsCategorywiseComponent } from './Website/ads-categorywise/ads-categorywise.component';
import { GmapComponent } from './Website/gmap/gmap.component';
import { WorkflowdetailsComponent } from './Website/workflowdetails/workflowdetails.component';
import { BlogComponent } from './Website/blog/blog.component';
import { BlogDetailComponent } from './Website/blog-detail/blog-detail.component';
import { AdsSubcategorywiseComponent } from './Website/ads-subcategorywise/ads-subcategorywise.component';
import { SearchresultComponent } from './Website/searchresult/searchresult.component';
//import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { UserlogoutComponent } from './Website/userlogout/userlogout.component';

import { ActivationComponent } from './views/activation/activation.component';
import { MessageboardComponent } from './Website/messageboard/messageboard.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FileuploadComponent } from './Website/fileupload/fileupload.component';
import { ChangepasswordComponent } from './views/changepassword/changepassword.component';
//import { LoaderComponent } from './loader/loader.component';
//import { LoaderService } from '../app/loader/loader.service';
//import { LoaderInterceptor } from '../app/loader/loader.interceptor';

// Arabic Language Integration
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BusmalladdetailsviewComponent } from './Website/busmalladdetailsview/busmalladdetailsview.component';
import { BupostsmalladComponent } from './Website/bupostsmallad/bupostsmallad.component';
import { BumanagesmalladComponent } from './Website/bumanagesmallad/bumanagesmallad.component';
import {BumanagebuysellComponent} from './Website/bumanagebuysell/bumanagebuysell.component';
import { BubuyselldetailsviewComponent } from './Website/bubuyselldetailsview/bubuyselldetailsview.component';
import { BuaddbuysellComponent } from './Website/buaddbuysell/buaddbuysell.component';
import { SmalladlistingComponent } from './Website/smalladlisting/smalladlisting.component';
import { SmalladdetailsComponent } from './Website/smalladdetails/smalladdetails.component';
import { BannerlistingComponent } from './Website/bannerlisting/bannerlisting.component';
import { AdvertiseusComponent } from './Website/advertiseus/advertiseus.component';


//import { NgbdDatepickerBasic } from './datepicker-basic';

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,

    NgxPaginationModule,
    NgSelectModule,
    NgMultiSelectDropDownModule,
    Ng2SearchPipeModule,
    OrderModule,
    AppAsideModule,
    AngularEditorModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
	 FormsModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    //NgxStripeModule.forRoot('pk_test_3Ql4eF5IpR5oVXpP8ZIIh0Lj'),
    NgxStripeModule.forRoot('pk_live_7RVQdTYUQPt27T4DY5S3vI1q'),
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    TabsModule.forRoot(),
    ToastrModule.forRoot({
     // progressBar: true,
      // timeOut:5000,
      disableTimeOut: true,
      closeButton: true,
      tapToDismiss: true,
      positionClass: 'toast-center-center',
     // messageClass: 'toast-container'
     // toastClass:'toast-container'
      //positionClass: 'toast-top-center'
      //toastClass: 'toast toast-bootstrap-compatibility-fix'
    }),
    ChartsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SlickModule,
    HttpModule,
    SocialLoginModule,
    SharedModule,
    NgbModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
    

  ],
  entryComponents: [
    
  ],
  declarations:  [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
   WorkflowdetailsComponent,
    ForgotpasswordComponent,
    EditcategoryComponent,
    EditsubcategoryComponent,
    StripeComponent,
    // Newly Added Components Starts
    BudashboardComponent,
    BubannerdetailsviewComponent,
    WebsiteComponent,
    AboutComponent,
    AdDetailComponent,
    // Newly Added Components Ends
    MainheadComponent,
    BannerComponent,
    ContentComponent,
    UploadComponent,
    BuaddetailsviewComponent,
    MultiUploadComponent,
    MainfooterComponent,
 
    PopularCategoryComponent,
    CategoryComponent,
    InnerContentComponent,
    InnerBannerComponent,
  
    BannerDetailComponent,
    FaqComponent,
    ContactComponent,
    PrivacyPolicyComponent,
    TermsUseComponent,
    DisclaimerComponent,
    BupostadComponent,
    BupostbannerComponent,
    BuprofileComponent,
    BusidebarComponent,
    DashboardBannerComponent,
   
    BuposteventComponent,
    BuresetpasswordComponent,
    BumanageadComponent,
    BupostpaidadComponent,
    ByplanComponent,
    BuyplanComponent,
    BumanagebannerComponent,
    BumanageeventComponent,
    EventsComponent,
    JobsComponent,
    AdsCategorywiseComponent,
    EventDetailComponent,
    BubannerdetailsviewComponent,
    BueventdetailviewComponent,
    //BueditbannerComponent,
    //BuediteventComponent
    GmapComponent,
    BlogComponent,
    BlogDetailComponent,
    AdsSubcategorywiseComponent,
    //AutocompleteComponent,
     BumanageblogComponent,
    BuadblogComponent,
    BublogdetailsviewComponent,
    SearchresultComponent,
    UserlogoutComponent,
  
    ActivationComponent,
  
    MessageboardComponent,

    BumanagejobsComponent,
    themeComponent,
    FileuploadComponent,
    BuysellComponent,
   // NgbdDatepickerBasic,
    ChangepasswordComponent,
   BusmalladdetailsviewComponent,
   BupostsmalladComponent,
   BumanagesmalladComponent,//,    LoaderComponent
   BumanagebuysellComponent,
   BubuyselldetailsviewComponent,
  
   BuyselldetailsComponent,
   BuaddbuysellComponent,
   SmalladlistingComponent,
SmalladdetailsComponent,
    BannerlistingComponent,
    ComboBoxComponent,
    AdvertiseusComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [//LoaderService, { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    //{
    //provide: LocationStrategy,
    //  // useClass: HashLocationStrategy
    // useClass: PathLocationStrategy
    
    //},
    {
    provide: AuthServiceConfig,
    useFactory: getAuthServiceConfigs
    }],
 // exports: [NgbdDatepickerBasic],
  bootstrap: [AppComponent]//, NgbdDatepickerBasic ]
})
export class AppModule { }

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);

  //return new TranslateHttpLoader(httpClient, 
  //environment.feServerUrl + '/assets/i18n/', '.json'); 
}
