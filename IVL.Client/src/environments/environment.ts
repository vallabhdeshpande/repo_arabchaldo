// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
 baseUrl: 'http://localhost:57284/api/' ,
 serverpath: 'http://localhost:57284/',
  imagepath: 'http://localhost:57284/StaticFiles/',
   defaultJobCategoryId: '32',
   defaultbuysellCategoryId: '7',
  mailConfirmationUrl: 'http://localhost:4200/',
  paymentkey: 'pk_test_3Ql4eF5IpR5oVXpP8ZIIh0Lj'


//  baseUrl: 'https://arabchaldo.azurewebsites.net/api/',
//   serverpath: 'https://arabchaldo.azurewebsites.net/',
//   imagepath:'https://arabchaldo.azurewebsites.net/StaticFiles/',
 
};
  

