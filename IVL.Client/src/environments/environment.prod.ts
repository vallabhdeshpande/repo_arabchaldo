export const environment = {
  production: true,
  // baseUrl: 'http://10.0.40.3:8088/api/',
  // serverpath: 'http://10.0.40.3:8088/',
  // imagepath:'http://10.0.40.3:8088/StaticFiles/' 

  //IIS_Vallabh

  // baseUrl: 'http://10.0.40.3:9090/api/',
  // serverpath: 'http://10.0.40.3:9090/',
  // imagepath: 'http://10.0.40.3:9090/StaticFiles/',
  // defaultJobCategoryId: '32',
  // defaultbuysellCategoryId: '7',
  // mailConfirmationUrl: 'http://10.0.40.3:7070/',
  // paymentkey:'pk_test_3Ql4eF5IpR5oVXpP8ZIIh0Lj'



   // IIS_Ravi Localhost

  // baseUrl: 'http://localhost:9091/api/',
  // serverpath: 'http://localhost:9091/',
  // imagepath: 'http://localhost:9091/StaticFiles/',
  // defaultJobCategoryId: '33',
  // mailConfirmationUrl:'http://localhost:9094/',
  // paymentkey:'pk_live_7RVQdTYUQPt27T4DY5S3vI1q' 

   // IIS_Ravi UAT

  // baseUrl: 'http://api.arabchaldo.com/api/',
  // serverpath: 'http://api.arabchaldo.com/',
  // imagepath: 'http://api.arabchaldo.com/StaticFiles/',
  // defaultJobCategoryId: '32',
  // defaultbuysellCategoryId: '7',
  // mailConfirmationUrl:'http://client.arabchaldo.com/',
  // paymentkey:'pk_live_7RVQdTYUQPt27T4DY5S3vI1q'
  
  // IIS_Ravi UAT with HTTPS

  // baseUrl: 'https://api.arabchaldo.com/api/',
  // serverpath: 'https://api.arabchaldo.com/',
  // imagepath: 'https://api.arabchaldo.com/StaticFiles/',
  // defaultJobCategoryId: '32',
  // defaultbuysellCategoryId: '7',
  // mailConfirmationUrl:'https://client.arabchaldo.com/',
  // paymentkey:'pk_live_7RVQdTYUQPt27T4DY5S3vI1q' 

   // IIS_Ravi Production with HTTPs

   baseUrl: 'https://prodapi.arabchaldo.com/api/',
   serverpath: 'https://prodapi.arabchaldo.com/',
   imagepath: 'https://prodapi.arabchaldo.com/StaticFiles/',
   defaultJobCategoryId: '32',
   defaultbuysellCategoryId: '7',
   mailConfirmationUrl:'https://arabchaldo.com/',
   paymentkey:'pk_live_7RVQdTYUQPt27T4DY5S3vI1q' 

   // IIS_Ravi Production with HTTPS

  //  baseUrl: 'https://prodapi.arabchaldo.com/api/',
  //  serverpath: 'https://prodapi.arabchaldo.com/',
  //  imagepath: 'https://prodapi.arabchaldo.com/StaticFiles/',
  //  defaultJobCategoryId: '32',
  //  defaultbuysellCategoryId: '7',
  //  mailConfirmationUrl:'https://arabchaldo.com/',
  //  paymentkey:'pk_live_7RVQdTYUQPt27T4DY5S3vI1q' 

  // Azure Deepak  with HTTP

  //  baseUrl: 'https://apiarabchaldo.azurewebsites.net/api/',
  //  serverpath: 'https://apiarabchaldo.azurewebsites.net/',
  //  imagepath: 'https://apiarabchaldo.azurewebsites.net/StaticFiles/',
  //  defaultJobCategoryId: '32',
  //  defaultbuysellCategoryId: '7',
  //  mailConfirmationUrl:'https://apiarabchaldo.azurewebsites.net/',
  //  paymentkey:'pk_live_7RVQdTYUQPt27T4DY5S3vI1q' 
};

