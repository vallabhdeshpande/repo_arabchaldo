﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;

namespace IVL.ArabChaldo.BAL
{
   public class RolesBAL : IDisposable
    {
        public RolesBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public RolesBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

        public List<RolesDTO> GetAllUserRole()
        {

            List<Roles> UserRoleList = new List<Roles>();
            UserRoleList = obj.Roles.GetAllUserRoles();

            List<RolesDTO> UserRoleListBoxig = new List<RolesDTO>();

            foreach (var listUserRole in UserRoleList)
            {
                UserRoleListBoxig.Add(AutoMapperToGetUserRole(listUserRole));
            }

            return UserRoleListBoxig;
        }
        public RolesDTO AutoMapperToGetUserRole(IVL.ArabChaldo.DAL.Models.Roles Roles)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Roles, RolesDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<Roles, RolesDTO>(Roles);
            return destination;
        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }

    }
}
