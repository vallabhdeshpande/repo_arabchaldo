﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Repository;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;
using System.Data;

namespace IVL.ArabChaldo.BAL
{
    public class BlogDetailsBAL : IDisposable
    {
        public BlogDetailsBAL(IUnitOfWork objOfWork)
        {
            this._unitOfWork = objOfWork;
        }

        public BlogDetailsBAL() : this(new UnitOfWork())
        {

        }

        private readonly IUnitOfWork _unitOfWork;
   

        public object EmailDetails { get; private set; }

        public bool AddBlog(BlogDetailsDTO blogDetails)
        {
            blogDetails.CreatedDate = DateTime.Now;
            blogDetails.IsActive = true;
            BlogDetails addBlog = AutoMapperToAddBlog(blogDetails);
            bool check = _unitOfWork.BlogDetails.Insert(addBlog);
            _unitOfWork.Commit();

            if (check == true)
            {
                EmailSenderDetailsDTO edMail = new EmailSenderDetailsDTO();
                
                ConfigurationDTO configDetail = new ConfigurationDTO();
                ConfigurationBAL configBal = new ConfigurationBAL();
  
               
                configDetail = configBal.GetExistingConfigDetails();
                edMail.mailfor = "Blogdetails";
               // edMail.subject = "Arab Chaldo - Blog Added";
                edMail.port = configDetail.Port;
                edMail.smtp = configDetail.Smtp;
                edMail.domain = configDetail.Domain;
                edMail.password = configDetail.Password;
                //edMail.subject = configDetail.Subject;
                edMail.fromMailId = configDetail.FromMailId;
                edMail.disclaimerTemplate = configDetail.DisclaimerTemplate;
                //UserDetailsDTO userDetail = new UserDetailsDTO();
                UserDetailsBal userBAL = new UserDetailsBal();
                var Userfethed = userBAL.GeUserbyId(Convert.ToInt32(blogDetails.CreatedBy));
                edMail.subject = "Arabchaldo - Blog Submitted: #" + Userfethed.UserId.ToString(); //+ "- " + AdDetails.Title; //"Arab Chaldo - Blog Approved "
                edMail.email = Userfethed.Email;
                edMail.title = blogDetails.Title;
                edMail.name = Userfethed.FirstName + " " + Userfethed.LastName;
                EmailSender sendEmail = new EmailSender();
                sendEmail.SendEmail(edMail);
                sendEmail.SendAcknowledgement(edMail);

            }
            return check;
        }

        public bool UpdateBlog(long id, BlogDetailsDTO blogDetails)
        {
            blogDetails.UpdatedDate = DateTime.Now;
            blogDetails.IsActive = true;
            
            BlogDetailsDTO blogDetailsDTO = GetBlogById(id);
            if (blogDetailsDTO != null)
            {
                blogDetails.UpdatedDate = DateTime.Now;

                blogDetails.IsVisible = null;
                BlogDetails updateBlog = AutoMapperToAddBlog(blogDetails);
                bool check = _unitOfWork.BlogDetails.Update(id, updateBlog);
                _unitOfWork.Commit();


                if (check == true)
                {
                    EmailSenderDetailsDTO edMail = new EmailSenderDetailsDTO();

                    ConfigurationDTO configDetail = new ConfigurationDTO();
                    ConfigurationBAL configBal = new ConfigurationBAL();
                    configDetail = configBal.GetExistingConfigDetails();
                    edMail.mailfor = "Blogupdatedetails";
                    //edMail.subject = "Arab Chaldo - Blog Updated";
                    edMail.port = configDetail.Port;
                    edMail.smtp = configDetail.Smtp;
                    edMail.domain = configDetail.Domain;
                    edMail.password = configDetail.Password;
                   // edMail.subject = configDetail.Subject;
                    edMail.fromMailId = configDetail.FromMailId;
                    edMail.disclaimerTemplate = configDetail.DisclaimerTemplate;
                    //UserDetailsDTO userDetail = new UserDetailsDTO();
                    UserDetailsBal userBAL = new UserDetailsBal();
                    var Userfethed = userBAL.GeUserbyId(Convert.ToInt32(blogDetails.CreatedBy));
                    edMail.subject = "Arabchaldo - Blog Updated: #" + Userfethed.UserId.ToString(); //+ "- " + AdDetails.Title; //"Arab Chaldo - Blog Approved "
                    edMail.email = Userfethed.Email;
                    edMail.title = blogDetails.Title;
                    edMail.name = Userfethed.FirstName + " " + Userfethed.LastName;
                    EmailSender sendEmail = new EmailSender();
                    sendEmail.SendEmail(edMail);
                    sendEmail.SendAcknowledgement(edMail);

                }
                return check;
            }
            return false;
        }

        public DataTable GetAllBlog(long loggedInUserId, string loggedInRole)
        {

            return _unitOfWork.BlogDetails.GetAllBlog(loggedInUserId, loggedInRole);
        }

        public BlogDetailsDTO GetBlogById(long id)
        {
            BlogDetails getBlogDetails = _unitOfWork.BlogDetails.GetBlogById(id);
            BlogDetailsDTO convertToBlogDTO = AutoMapperToGetBlog(getBlogDetails);
            _unitOfWork.Commit();
            return convertToBlogDTO;
        }

        public bool BlogStatus(long BlogId, string remarks, string Action, long CreatedBy)
        {
            UserDetailsBal userBAL = new UserDetailsBal();
            BlogDetails getBlogDetails = _unitOfWork.BlogDetails.GetBlogById(BlogId);
            var Userfethed = userBAL.GeUserbyId(getBlogDetails.CreatedBy);
            bool isStatus = _unitOfWork.BlogDetails.BlogStatus(BlogId, remarks, Action, CreatedBy);
         
            _unitOfWork.Commit();

            if (isStatus)
            {

                // Mail Functionality
                EmailSenderDetailsDTO edMail = new EmailSenderDetailsDTO();

                ConfigurationDTO configDetail = new ConfigurationDTO();
                ConfigurationBAL configBal = new ConfigurationBAL();
                configDetail = configBal.GetExistingConfigDetails();
             
         
        
                string subject = "";
                string title = "";
                edMail.email = Userfethed.Email;
                edMail.name = Userfethed.FirstName + " " + Userfethed.LastName;
                if (Action == "Approve")
                {
                    subject = "Arabchaldo - Blog Approved: #" + Userfethed.UserId.ToString(); //+ "- " + AdDetails.Title; //"Arab Chaldo - Blog Approved "
                    title = getBlogDetails.Title;
                    configBal.SetMailDetails("ApprovedBlogdetails", Userfethed.Email, Userfethed.FirstName, subject, configDetail.DisclaimerTemplate, "",title);
                }
                else if (Action == "Reject")
                {
                    subject = "Arabchaldo - Blog Rejected: #" + Userfethed.UserId.ToString(); //+ "- " + AdDetails.Title; //"Arab Chaldo - Blog Rejected "
                    title = getBlogDetails.Title;
                    string RejectRemark = remarks.Split(',')[2].ToString();
                    configBal.SetMailDetails("RejectedBlogdetails", Userfethed.Email, Userfethed.FirstName, subject, configDetail.DisclaimerTemplate, RejectRemark,title);

                }

                else if (Action == "Delete")
                {
                    subject = "Arabchaldo - Blog Deleted: #" + Userfethed.UserId.ToString(); //+ "- " + AdDetails.Title; // "Arab Chaldo - Blog Rejected "
                    title = getBlogDetails.Title;

                    configBal.SetMailDetails("DeletedBlogdetails", Userfethed.Email, Userfethed.FirstName, subject, configDetail.DisclaimerTemplate, "",title);

                }
            }
            return isStatus;
        }

        public bool DeleteBlog(long id)
        {
            bool isDeleted = _unitOfWork.BlogDetails.Delete(id);
            _unitOfWork.Commit();
            return isDeleted;
        }

        public BlogDetailsDTO AutoMapperToGetBlog(BlogDetails blogDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BlogDetails, BlogDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<BlogDetails, BlogDetailsDTO>(blogDetails);
            return destination;
        }

        public BlogDetails AutoMapperToAddBlog(BlogDetailsDTO blogDetails)
        {
           

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BlogDetailsDTO, BlogDetails>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<BlogDetailsDTO, BlogDetails>(blogDetails);
            return destination;
        }

        public void Dispose()
        {
            if (_unitOfWork != null)
            {
                this._unitOfWork.Dispose();
            }
        }

    }
}
