﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;
using System.Data;

namespace IVL.ArabChaldo.BAL
{
    public class StateBAL : IDisposable
    {
        public StateBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public StateBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

        public bool CheckStateExits(string StateName, long StateId)
        {
            return obj.State.CheckStateExits(StateName, StateId);
        }

        public bool DeleteState(int StateID)
        {
            bool isDeleted = obj.State.DeleteState(StateID);
            obj.Commit();
            return isDeleted;
        }
        public bool UpdateState(int StateID, StateDTO State)
        {
            bool stateexist = CheckStateExits(State.StateName, State.StateId);
            bool check = false;
            if (!stateexist)
            {

                State updateState = AutoMapperToUpdateState(StateID, State);
                check = obj.State.UpdateState(StateID, updateState);
            obj.Commit();
           
            }
            return check;





        }

        
        public bool InsertState(StateDTO State)
        {
            bool stateexist = CheckStateExits(State.StateName,State.StateId);
            bool check = false;
            if (!stateexist)
            {
                State insertState = AutoMapperToInsertState(State);
                check = obj.State.InsertState(insertState);
                obj.Commit();
            }
            return check;
        }


        public StateDTO GetStateById(int stateId)
        {
            var statefetched = obj.State.GetStateById(stateId);
            StateDTO getState = AutoMapperToGetState(statefetched);

            return getState;
        }

        public List<StateDTO> GetAllState()
        {

            List<State> StateList = new List<State>();
            StateList = obj.State.GetAllState();

            List<StateDTO> StateListBoxig = new List<StateDTO>();

            foreach (var listState in StateList)
            {
                StateListBoxig.Add(AutoMapperToGetState(listState));
            }

            return StateListBoxig;
        }

        public StateDTO AutoMapperToGetState(IVL.ArabChaldo.DAL.Models.State State)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<State, StateDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<State, StateDTO>(State);
            return destination;
        }

        public State AutoMapperToInsertState(StateDTO State)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<StateDTO, State>();
            });
            IMapper iMapper = config.CreateMapper();
            var destination = iMapper.Map<StateDTO, State>(State);
            return destination;
        }

        public State AutoMapperToUpdateState(int StateID, StateDTO State)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<StateDTO, State>();
            });
            IMapper iMapper = config.CreateMapper();
            var Statefethed = obj.State.GetStateById(StateID);
            var destination = iMapper.Map<StateDTO, State>(State);
            return destination;
        }

        // FillState DDl By CountryId

        public List<StateDTO> GetAllStatesByCountryId(int CountryId)
        {

            List<State> StateList = new List<State>();
            StateList = obj.State.GetAllStatesByCountryId(CountryId);

            List<StateDTO> StateListBoxig = new List<StateDTO>();

            foreach (var listState in StateList)
            {
                StateListBoxig.Add(AutoMapperToGetState(listState));
            }

            return StateListBoxig;
        }

        public DataTable FillStateDDLByCountryId(int countryId)
        {

            DataTable dtState = new DataTable();
            dtState = obj.State.FillStateDDLByCountryId(countryId);

           
            return dtState;
        }



        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }

        public DataTable FillStateList()
        {
            DataTable dtState = new DataTable();
            dtState = obj.State.FillStateList();
            return dtState;


        }

    }
}