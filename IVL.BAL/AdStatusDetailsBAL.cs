﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;

namespace IVL.ArabChaldo.BAL
{
    public class AdStatusDetailsBAL : IDisposable
    {
        public AdStatusDetailsBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public AdStatusDetailsBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

        public bool CheckAdStatusDetailsExits(long AdStatusDetailsId)
        {
            //return obj.AdStatusDetails.CheckAdStatusDetailsExits(AdStatusDetailsId);
            return false;
        }

        public bool DeleteAdStatusDetails(int AdStatusDetailsID)
        {
            //bool isDeleted = obj.AdStatusDetails.DeleteAdStatusDetails(AdStatusDetailsID);
            // obj.Commit();
            // return isDeleted;
            return false;
        }
        public bool UpdateAdStatusDetails(int AdStatusDetailsID, AdStatusDetailsDTO AdStatusDetails)
        {
            //AdStatusDetails updateAdStatusDetails = AutoMapperToUpdateUser(AdStatusDetailsID, AdStatusDetails);
            //bool isUpdated = obj.AdStatusDetails.UpdateAdStatusDetails(AdStatusDetailsID, updateAdStatusDetails);
            //obj.Commit();
            //return isUpdated;
            return false;
        }
        public bool InsertAdStatusDetails(AdStatusDetailsDTO AdStatusDetails)
        {
            bool adStatusDetailsexist = CheckAdStatusDetailsExits(AdStatusDetails.AdStatusDetailsId);
            bool check = false;
            if (!adStatusDetailsexist)
            {
                AdStatusDetails insertAdStatusDetails = AutoMapperToInsertAdStatusDetails(AdStatusDetails);
                check = obj.AdStatusDetails.InsertAdStatusDetails(insertAdStatusDetails);
                obj.Commit();
            }
            return check;
        }


        //public AdStatusDetailsDTO GetAdStatusDetailsById(int adStatusDetailsId)
        //{
        //    //var adStatusDetailsfetched = obj.AdStatusDetails.GetAdStatusDetailsById(adStatusDetailsId);
        //   // AdStatusDetailsDTO getAdStatusDetails = AutoMapperToGetAdStatusDetails(adStatusDetailsfetched);

        //    return getAdStatusDetails;
           
        //}

        public List<AdStatusDetailsDTO> GetAllAdStatusDetails()
        {

            List<AdStatusDetails> AdStatusDetailsList = new List<AdStatusDetails>();
            //AdStatusDetailsList = obj.AdStatusDetails.GetAllAdStatusDetails();

            List<AdStatusDetailsDTO> AdStatusDetailsListBoxig = new List<AdStatusDetailsDTO>();

            foreach (var listAdStatusDetails in AdStatusDetailsList)
            {
                AdStatusDetailsListBoxig.Add(AutoMapperToGetAdStatusDetails(listAdStatusDetails));
            }

            return AdStatusDetailsListBoxig;
        }

        public AdStatusDetailsDTO AutoMapperToGetAdStatusDetails(IVL.ArabChaldo.DAL.Models.AdStatusDetails AdStatusDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AdStatusDetails, AdStatusDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<AdStatusDetails, AdStatusDetailsDTO>(AdStatusDetails);
            return destination;
        }

        public AdStatusDetails AutoMapperToInsertAdStatusDetails(AdStatusDetailsDTO AdStatusDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AdStatusDetailsDTO, AdStatusDetails>();
            });
            IMapper iMapper = config.CreateMapper();
            var destination = iMapper.Map<AdStatusDetailsDTO, AdStatusDetails>(AdStatusDetails);
            return destination;
        }

        public AdStatusDetails AutoMapperToUpdateUser(int AdStatusDetailsID, AdStatusDetailsDTO AdStatusDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AdStatusDetailsDTO, AdStatusDetails>();
            });
            IMapper iMapper = config.CreateMapper();
           // var AdStatusDetailsfethed = obj.AdStatusDetails.GetAdStatusDetailsById(AdStatusDetailsID);
            var destination = iMapper.Map<AdStatusDetailsDTO, AdStatusDetails>(AdStatusDetails);
            return destination;
        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }

    }
}