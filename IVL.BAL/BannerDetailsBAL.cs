﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Repository;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.DAL.Interfaces;
using System.Data;

namespace IVL.ArabChaldo.BAL
{
    public class BannerDetailsBAL : IDisposable
    {
        private readonly IUnitOfWork _unitOfWork;

        private object obj;

        public BannerDetailsBAL(IUnitOfWork objOfWork)
        {
            this._unitOfWork = objOfWork;
        }

        public BannerDetailsBAL() : this(new UnitOfWork())
        {

        }

        public bool AddBanner(BannerDetailsDTO bannerDetails)
        {
            PaymentPlanBAL paymentPlanBAL = new PaymentPlanBAL();
            StateBAL stateBAL = new StateBAL();
            StateDTO stateDto = stateBAL.GetStateById((int)bannerDetails.StateId);
            if (stateDto != null)
                bannerDetails.StateName = stateDto.StateName;

            //  CityBAL cityBAL = new CityBAL();
            //  CityDTO city = cityBAL.GetCityById((int)bannerDetails.CityId);
            //if(city!=null)
            //  bannerDetails.CityName = city.CityName;
            if (bannerDetails.CityId != 9999)
            {
                CityBAL cityBAL = new CityBAL();
                CityDTO city = cityBAL.GetCityById((int)bannerDetails.CityId);
                if (city != null)
                    bannerDetails.CityName = city.CityName;
            }
            else
            {
                bannerDetails.CityName = "Other";
            }

            bannerDetails.CreatedDate = DateTime.Now;
            bannerDetails.UpdatedDate = DateTime.Now;
            bannerDetails.IsActive = true;
            long postedForWhom = bannerDetails.PostedFor;

            UserDetailsBal userDetailsBal = new UserDetailsBal();

            bannerDetails.PostedForName = userDetailsBal.GetThirdPartyName(postedForWhom);

            BannerDetails addBanner = AutoMapperToAddBanner(bannerDetails);
            bool check = _unitOfWork.BannerDetails.Insert(addBanner);
            _unitOfWork.Commit();

            if (check == true)


            {
                // Mail Functionality
                ConfigurationBAL configBal = new ConfigurationBAL();

                ConfigurationDTO configDetail = new ConfigurationDTO();
                string subject = "";
                string title = "";
                configDetail = configBal.GetExistingConfigDetails();
                subject = "Arabchaldo - Banner Submitted: #" + addBanner.BannerId.ToString(); //+ "- " + adDetailsfetched.Title; //"Arab Chaldo - Bannner Added "
                title = addBanner.Title;
                configBal.SetMailDetails("Bannerdetails", addBanner.Email, addBanner.ContactPersonName, subject, configDetail.DisclaimerTemplate, "", title);
            }

            if (check && (bannerDetails.PlanId != null))
            {
                paymentPlanBAL.updatePlanForBanner((long)bannerDetails.PlanId, (long)bannerDetails.BannerImageCount);
            }
            return check;
        }

        public bool UpdateBanner(long id, BannerDetailsDTO bannerDetails)
        {
            PaymentPlanBAL paymentPlanBAL = new PaymentPlanBAL();
            StateBAL stateBAL = new StateBAL();
            StateDTO stateDto = stateBAL.GetStateById((int)bannerDetails.StateId);
            if (stateDto != null)
                bannerDetails.StateName = stateDto.StateName;

            if (bannerDetails.CityId != 9999)
            {
                CityBAL cityBAL = new CityBAL();
                CityDTO city = cityBAL.GetCityById((int)bannerDetails.CityId);
                if (city != null)
                    bannerDetails.CityName = city.CityName;
            }
            else
            {
                bannerDetails.CityName = "Other";
            }

            bannerDetails.UpdatedDate = DateTime.Now;
            bannerDetails.IsActive = true;
            long postedForWhom = bannerDetails.PostedFor;

            UserDetailsBal userDetailsBal = new UserDetailsBal();

            bannerDetails.PostedForName = userDetailsBal.GetThirdPartyName(postedForWhom);


            BannerDetailsDTO bannerDetailsDTO = GetBannerById(id);
            if (bannerDetailsDTO != null)
            {
                bannerDetails.UpdatedDate = DateTime.Now;

                bannerDetails.IsVisible = null;
                BannerDetails updateBanner = AutoMapperToAddBanner(bannerDetails);
                bool check = _unitOfWork.BannerDetails.Update(id, updateBanner);
                _unitOfWork.Commit();

                if (check == true)
                {
                    // Mail Functionality
                    ConfigurationBAL configBal = new ConfigurationBAL();
                    ConfigurationDTO configDetail = new ConfigurationDTO();
                    string subject = "";
                    string title = "";
                    configDetail = configBal.GetExistingConfigDetails();

                    subject = "Arabchaldo - Banner Updated: #" + updateBanner.BannerId.ToString(); //+ "- " + adDetailsfetched.Title; //"Arab Chaldo - Bannner Updated"
                    title = updateBanner.Title;
                    configBal.SetMailDetails("Bannerupdatedetails", updateBanner.Email, updateBanner.ContactPersonName, subject, configDetail.DisclaimerTemplate, "", title);
                }
                if (check && (bannerDetails.PlanId != null))
                {
                    paymentPlanBAL.updatePlanForBanner((long)bannerDetails.PlanId, (long)bannerDetails.BannerImageCount);
                }
                return check;
            }
            return false;
        }

        public DataTable GetAllBanner(int userId)
        {
            //List<BannerDetails> bannerList = new List<BannerDetails>();
            //bannerList = _unitOfWork.BannerDetails.GetAllBanner(userId);

            //List<BannerDetailsDTO> bannerDtoList = new List<BannerDetailsDTO>();

            //foreach (var item in bannerList)
            //{
            //    bannerDtoList.Add(AutoMapperToGetBanner(item));
            //}
            //return bannerDtoList;
            DataTable dtBanner = new DataTable();
            dtBanner = _unitOfWork.BannerDetails.GetAllBanner(userId);
            return dtBanner;
        }

        public BannerDetailsDTO GetBannerById(long id)
        {

            BannerDetails getBannerDetails = _unitOfWork.BannerDetails.GetBannerById(id);
            BannerDetailsDTO convertToBannerDTO = AutoMapperToGetBanner(getBannerDetails);
            _unitOfWork.Commit();
            return convertToBannerDTO;
        }

        public bool BannerStatus(long bannerId, string remarks, string Action, long CreatedBy)
        {
            bool isStatus = _unitOfWork.BannerDetails.BannerStatus(bannerId, remarks, Action, CreatedBy);

            _unitOfWork.Commit();

            if (isStatus)
            {

                // Mail Functionality
                var bannerDetailsfetched = _unitOfWork.BannerDetails.GetBannerById(bannerId);
                ConfigurationBAL configBal = new ConfigurationBAL();
                ConfigurationDTO configDetail = new ConfigurationDTO();
                configDetail = configBal.GetExistingConfigDetails();
                string subject = "";
                string title = "";
                if (Action == "Approve")
                {
                    subject = "Arabchaldo - Banner Approved: #" + bannerDetailsfetched.BannerId.ToString(); //+ "- " + adDetailsfetched.Title; //"Arab Chaldo - Bannner Approved"
                    title = bannerDetailsfetched.Title;
                    configBal.SetMailDetails("ApprovedBannerdetails", bannerDetailsfetched.Email, bannerDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, "", title);
                }
                else if (Action == "Reject")
                {

                    string RejectRemark = remarks.Split(',')[2].ToString();
                    subject = subject = "Arabchaldo - Banner Rejected: #" + bannerDetailsfetched.BannerId.ToString(); //+ "- " + adDetailsfetched.Title; //"Arab Chaldo - Bannner Rejected "
                    title = bannerDetailsfetched.Title;
                    configBal.SetMailDetails("RejectedBannerdetails", bannerDetailsfetched.Email, bannerDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, RejectRemark, title);

                }

                else if (Action == "Delete")
                {


                    subject = subject = "Arabchaldo - Banner Deleted: #" + bannerDetailsfetched.BannerId.ToString(); //+ "- " + adDetailsfetched.Title; //"Arab Chaldo - Bannner Rejected "
                    title = bannerDetailsfetched.Title;
                    configBal.SetMailDetails("DeleteBannerdetails", bannerDetailsfetched.Email, bannerDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, "", title);

                }

            }

            return isStatus;
        }

        public bool DeleteBanner(long id)
        {
            bool isDeleted = _unitOfWork.BannerDetails.Delete(id);
            _unitOfWork.Commit();
            return isDeleted;
        }

        public BannerDetailsDTO AutoMapperToGetBanner(BannerDetails bannerDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BannerDetails, BannerDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<BannerDetails, BannerDetailsDTO>(bannerDetails);
            return destination;
        }

        public BannerDetails AutoMapperToAddBanner(BannerDetailsDTO bannerDetails)
        {
            long postedForWhom = Convert.ToInt32(bannerDetails.PostedFor);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BannerDetailsDTO, BannerDetails>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<BannerDetailsDTO, BannerDetails>(bannerDetails);
            return destination;
        }

        public void Dispose()
        {
            if (_unitOfWork != null)
            {
                this._unitOfWork.Dispose();
            }
        }

        public DataTable FillBannerList()
        {
            DataTable dtBanner = new DataTable();
            dtBanner = _unitOfWork.BannerDetails.FillBannerList();
            return dtBanner;


        }
    }
}
