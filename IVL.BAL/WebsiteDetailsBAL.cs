﻿using AutoMapper;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace IVL.ArabChaldo.BAL
{
    public class WebsiteDetailsBAL : IDisposable
    {
        public WebsiteDetailsBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public WebsiteDetailsBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

       #region Advertisement
        public AdDetailsDTO GetAdDetailsById(long adDetailsId)
        {
            var adDetailsfetched = obj.WebsiteDetails.GetAdDetailsById(adDetailsId);
            AdDetailsDTO getAdDetails = AutoMapperToGetAdDetails(adDetailsfetched);
           // long StateIdForCity = 0;
           //   CommonBAL commonBal = new CommonBAL();
            //CountryName
            //if (getAdDetails.CountryId!=null || getAdDetails.CountryId!=0)
            //{
            //    DataTable countryTable = new DataTable();
            //    countryTable = commonBal.FillCountryDDL();
            //    DataRow foundRowCountry = countryTable.Select("CountryId=" + getAdDetails.CountryId)[0];
            //    if (foundRowCountry != null)
            //        getAdDetails.CountryName = foundRowCountry["CountryName"].ToString();
            //    else
            //        getAdDetails.CountryName = "USA";
            //}
            //else
            //    getAdDetails.CountryName = "";


            //StateName
            //if (getAdDetails.StateId != null || getAdDetails.StateId != 0)
            //{
            //    DataTable stateTable = new DataTable();
            //    stateTable = commonBal.FillStateDDLByCountryId(1);
            //    DataRow foundRowState = stateTable.Select("StateId=" + getAdDetails.StateId)[0];
            //    if (foundRowState != null)
            //    {
            //        StateIdForCity = Convert.ToInt64(foundRowState["StateId"]);
            //        getAdDetails.StateName = foundRowState["StateName"].ToString();
            //    }
            //    else
            //        getAdDetails.CountryName = "";
            //}
            //else
            //    getAdDetails.CountryName = "";


            //CityName
            //if (getAdDetails.StateId != null || getAdDetails.StateId != 0)
            //{
            //    DataTable cityTable = new DataTable();
            //    cityTable = commonBal.FillCityDDLByStateId(StateIdForCity);
            //    DataRow foundRowCity = cityTable.Select("CityId=" + getAdDetails.CityId)[0];
            //    if (foundRowCity != null)
            //        getAdDetails.CityName = foundRowCity["CityName"].ToString();
            //    else
            //        getAdDetails.CityName = "";
            //}
            //else
            //    getAdDetails.CityName = "";
            //CategoryName
            //DataTable categoryTable = new DataTable();
            //categoryTable = commonBal.FillCategoryDDL();
            //DataRow foundRowCategory = categoryTable.Select("CategoryId=" + getAdDetails.CategoryId)[0];
            //if (foundRowCategory != null)
            //    getAdDetails.CategoryName = foundRowCategory["CategoryName"].ToString();


            ////SubcategoryName
            //DataTable subcategoryTable = new DataTable();
            //if (getAdDetails.SubCategoryId != 0)
            //{
            //    subcategoryTable = commonBal.FillSubCategoryDDLByCategoryId(getAdDetails.CategoryId);
            //    if (subcategoryTable.Rows.Count > 0)
            //    {
            //        DataRow foundRowSubCategory = subcategoryTable.Select("SubCategoryId=" + getAdDetails.SubCategoryId)[0];
            //        if (foundRowSubCategory != null)
            //            getAdDetails.SubCategoryName = foundRowSubCategory["SubCategoryName"].ToString(); //--Select SubCategory--
            //                                                                                              //else
            //                                                                                              // getAdDetails.SubCategoryName = "--Select SubCategory--";
            //    }
            //    else
            //    {
            //        getAdDetails.SubCategoryName = "";
            //       //getAdDetails.SubCategoryName = "--Select SubCategory--";
            //       getAdDetails.SubCategoryId = 0;
            //    }

            //}

            ////UserName
            //DataTable userTable = new DataTable();
            //userTable = commonBal.FillUserDDL();
            //DataRow foundRowUser = userTable.Select("UserId=" + getAdDetails.PostedFor)[0];
            //if (foundRowUser != null)
            //    getAdDetails.PostedForName = foundRowUser["FirstName"].ToString() + " " + foundRowUser["LastName"].ToString();






            return getAdDetails;
        }

        public DataTable GetCitySearchDDL()
        {
            return obj.WebsiteDetails.GetCitySearchDDL();
        }

        public DataTable GetCategorySearchDDL()
        {
            return obj.WebsiteDetails.GetCategorySearchDDL();
        }

        public DataTable GetAllAdDetails(string imgurl)
        {
            return obj.WebsiteDetails.GetAllAdDetails(imgurl);
        }
        public DataTable GetAllJobDetails(string imgurl)
        {
            return obj.WebsiteDetails.GetAllJobDetails(imgurl);
        }
        public List<AdDetailsDTO> GetAdsAdListBycategoryId(long categoryId)
        {

            List<AdDetails> AdDetailsList = new List<AdDetails>();
            AdDetailsList = obj.WebsiteDetails.GetAdsAdListBycategoryId(categoryId);

            List<AdDetailsDTO> AdDetailsListBoxig = new List<AdDetailsDTO>();

            foreach (var listAdDetails in AdDetailsList)
            {
                AdDetailsListBoxig.Add(AutoMapperToGetAdDetails(listAdDetails));
            }

            return AdDetailsListBoxig;
        }
        public List<AdDetailsDTO> GetAdsAdListBySubcategoryId(long SubcategoryId)
        {

            List<AdDetails> AdDetailsList = new List<AdDetails>();
            AdDetailsList = obj.WebsiteDetails.GetAdsAdListBySubcategoryId(SubcategoryId);

            List<AdDetailsDTO> AdDetailsListBoxig = new List<AdDetailsDTO>();

            foreach (var listAdDetails in AdDetailsList)
            {
                AdDetailsListBoxig.Add(AutoMapperToGetAdDetails(listAdDetails));
            }

            return AdDetailsListBoxig;
        }
        public AdDetailsDTO AutoMapperToGetAdDetails(IVL.ArabChaldo.DAL.Models.AdDetails AdDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AdDetails, AdDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<AdDetails, AdDetailsDTO>(AdDetails);
            return destination;
        }

        #endregion

        #region Small Ads
        public SmallAdDetailsDTO GetSmallAdDetailsById(long smallAdId)
        {
            var smalladDetailsfetched = obj.WebsiteDetails.GetSmallAdDetailsById(smallAdId);
            SmallAdDetailsDTO getSmallAdDetails = AutoMapperToGetSmallAdDetails(smalladDetailsfetched);
            return getSmallAdDetails;
        }

        public DataTable GetAllSmallAdDetails(string imgurl)
        {
            return obj.WebsiteDetails.GetAllSmallAdDetails(imgurl);
        }
        public SmallAdDetailsDTO AutoMapperToGetSmallAdDetails(IVL.ArabChaldo.DAL.Models.SmallAdDetails SmallAdDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SmallAdDetails, SmallAdDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<SmallAdDetails, SmallAdDetailsDTO>(SmallAdDetails);
            return destination;
        }
        #endregion

        #region Banners
        public DataTable GetAllBanner(string imgurl)
        {

            return obj.WebsiteDetails.GetAllBanner(imgurl);
        }
        public DataTable getPremiumBannerList(string imgurl)
        {
              return   obj.WebsiteDetails.getPremiumBannerList(imgurl);
        }
        

        public BannerDetailsDTO GetBannerById(long id)
        {
            BannerDetails getBannerDetails = obj.WebsiteDetails.GetBannerById(id);
            BannerDetailsDTO convertToBannerDTO = AutoMapperToGetBanner(getBannerDetails);
            obj.Commit();
            return convertToBannerDTO;
        }

        public BannerDetailsDTO AutoMapperToGetBanner(BannerDetails bannerDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BannerDetails, BannerDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<BannerDetails, BannerDetailsDTO>(bannerDetails);
            return destination;
        }
        #endregion

       #region Blogs

            public DataTable GetAllBlog()
            {

                return obj.WebsiteDetails.GetAllBlog();
            }
        

        public BlogDetailsDTO GetBlogById(long id)
        {
            BlogDetails getBlogDetails = obj.WebsiteDetails.GetBlogById(id);
            BlogDetailsDTO convertToBlogDTO = AutoMapperToGetBlog(getBlogDetails);
            obj.Commit();
            return convertToBlogDTO;
        }
        public BlogDetailsDTO AutoMapperToGetBlog(BlogDetails blogDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BlogDetails, BlogDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<BlogDetails, BlogDetailsDTO>(blogDetails);
            return destination;
        }
        #endregion

       #region Events
        public List<EventDetailsDTO> GetAllEvent()
        {
            List<EventDetails> eventList = new List<EventDetails>();
            eventList = obj.WebsiteDetails.GetAllEvent();

            List<EventDetailsDTO> eventDtoList = new List<EventDetailsDTO>();

            foreach (var item in eventList)
            {
                eventDtoList.Add(AutoMapperToGetEvent(item));
            }
            return eventDtoList;
        }

        public EventDetailsDTO GetEventById(long id)
        {
            EventDetails getEventDetails = obj.WebsiteDetails.GetEventById(id);
            EventDetailsDTO convertToEventDTO = AutoMapperToGetEvent(getEventDetails);
            obj.Commit();
            return convertToEventDTO;
        }
        public DataTable GetEventsByCategoryId(long cateoryId)
        {
            //return _unitOfWork.EventDetails.GetEventsByCategoryId(cateoryId);
            //List<EventDetails> eventListbycategory = new List<EventDetails>();
            //eventListbycategory = obj.WebsiteDetails.GetEventsByCategoryId(cateoryId);
            //List<EventDetailsDTO> eventDtoList = new List<EventDetailsDTO>();

            //foreach (var item in eventListbycategory)
            //{
            //    eventDtoList.Add(AutoMapperToGetEvent(item));
            //}
            //return eventDtoList;

            return obj.WebsiteDetails.GetEventsByCategoryId(cateoryId);
        }
        public EventDetailsDTO AutoMapperToGetEvent(EventDetails eventDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<EventDetails, EventDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<EventDetails, EventDetailsDTO>(eventDetails);
            return destination;
        }

        public bool SendEmail( EmailSenderDetailsDTO EmailDetails)
        {
            ConfigurationDTO configDetail = new ConfigurationDTO();
            ConfigurationBAL configBal = new ConfigurationBAL();
            configDetail = configBal.GetExistingConfigDetails();
            EmailDetails.port = configDetail.Port;
            EmailDetails.smtp = configDetail.Smtp;
            EmailDetails.domain = configDetail.Domain;
            EmailDetails.fromMailId = configDetail.FromMailId;
            EmailDetails.password = configDetail.Password;
            EmailDetails.contactMailTemplate = configDetail.ContactMailTemplate;
            EmailDetails.disclaimerTemplate = configDetail.DisclaimerTemplate;
            EmailDetails.feedbackMailTemplate = configDetail.FeedbackMailTemplate;
            EmailDetails.jobsMailTemplate = configDetail.JobsMailTemplate;

            EmailSender emailSender = new EmailSender();
            emailSender.SendEmail(EmailDetails);
          
         
            return true;
            //emailSender.SendEmail(userDetails.Email);
           
        }

        public bool SendAcknowledgement(EmailSenderDetailsDTO EmailDetails)
        {

            ConfigurationDTO configDetail = new ConfigurationDTO();
            ConfigurationBAL configBal = new ConfigurationBAL();
            configDetail = configBal.GetExistingConfigDetails();
            EmailDetails.port = configDetail.Port;
            EmailDetails.smtp = configDetail.Smtp;
            EmailDetails.domain = configDetail.Domain;
            EmailDetails.disclaimerTemplate = configDetail.DisclaimerTemplate;
            EmailSender emailSender = new EmailSender();
            emailSender.SendAcknowledgement(EmailDetails);
            return true;
            //emailSender.SendEmail(userDetails.Email);
        }


        public bool ContactDetails(EmailSenderDetailsDTO ContactDetails)
        {
            ConfigurationDTO configDetail = new ConfigurationDTO();
            ConfigurationBAL configBal = new ConfigurationBAL();
            configDetail = configBal.GetExistingConfigDetails();
            ContactDetails.port = configDetail.Port;
            ContactDetails.smtp = configDetail.Smtp;
            ContactDetails.domain = configDetail.Domain;
            ContactDetails.jobsMailTemplate = configDetail.JobsMailTemplate;
            ContactDetails.disclaimerTemplate = configDetail.DisclaimerTemplate;
            EmailSender emailSender = new EmailSender();
            emailSender.SendEmail(ContactDetails);
            

            return true;
            //emailSender.SendEmail(userDetails.Email);
        }


        public bool InboxMessageDetails(EmailSenderDetailsDTO InboxMessageDetails)
        {
            ConfigurationDTO configDetail = new ConfigurationDTO();
            ConfigurationBAL configBal = new ConfigurationBAL();
            configDetail = configBal.GetExistingConfigDetails();
            InboxMessageDetails.port = configDetail.Port;
            InboxMessageDetails.smtp = configDetail.Smtp;
            //InboxMessageDetails.domain = configDetail.Domain;
            InboxMessageDetails.multipleMailTemplate= configDetail.multipleMailTemplate;
            InboxMessageDetails.disclaimerTemplate = configDetail.DisclaimerTemplate;
            EmailSender emailSender = new EmailSender();
            emailSender.SendEmail(InboxMessageDetails);


            return true;
            //emailSender.SendEmail(userDetails.Email);
        }


        public bool BuyplanDetails(EmailSenderDetailsDTO BuyplanDetails)
        {
            ConfigurationDTO configDetail = new ConfigurationDTO();
            ConfigurationBAL configBal = new ConfigurationBAL();
            configDetail = configBal.GetExistingConfigDetails();
            BuyplanDetails.port = configDetail.Port;
            BuyplanDetails.smtp = configDetail.Smtp;
            //BuyplanDetails.domain = configDetail.Domian;
            BuyplanDetails.BuyPlanMailTemplate = configDetail.BuyPlanMailTemplate;
            BuyplanDetails.disclaimerTemplate = configDetail.DisclaimerTemplate;
            EmailSender emailSender = new EmailSender();
            emailSender.SendEmail(BuyplanDetails);


            return true;
            //emailSender.SendEmail(userDetails.Email);
        }

     
        #endregion

        #region GlobalSearh

        // Get Category Data
        public DataTable GetAllCityData()
        {
            return obj.WebsiteDetails.GetAllCityData();

        }

        // Get City Data

        public DataTable GetAllCategoryData()
        {
            return obj.WebsiteDetails.GetAllCategoryData();
        }

        public DataTable GetAdListBySearchCriteria(string searchCriteria)
        {
            return obj.WebsiteDetails.GetAdListBySearchCriteria(searchCriteria);
        }

        #endregion

        #region Template records of website
        public DataTable GetAboutUSData()
        {
            return obj.WebsiteDetails.GetAboutUSData();
        }
        public DataTable GetContactUSData()
        {
            return obj.WebsiteDetails.GetContactUSData();
        }
        public DataTable GetPrivacyPolicyData()
        {
            return obj.WebsiteDetails.GetPrivacyPolicyData();
        }
        public DataTable GetTermsofUseData()
        {
            return obj.WebsiteDetails.GetTermsofUseData();
        }
        public DataTable GetFAQData()
        {
            return obj.WebsiteDetails.GetFAQData();
        }
        public DataTable GetDisclaimerData()
        {
            return obj.WebsiteDetails.GetDisclaimerData();
        }
        #endregion

        #region Buysell
       public DataTable GetAllBuySellData(string imgurl)
        {
            return obj.WebsiteDetails.GetAllBuysellData(imgurl);
        }

        public DataTable GetAllBuySellRecentData(string imgurl)
        {
            return obj.WebsiteDetails.GetAllBuysellRecentData(imgurl);
        }
        public BuySellDetailsDTO GetBuysellById(long id)
        {
            BuySellDetails getbuuySellDetails = obj.WebsiteDetails.GetBuysellById(id);
            BuySellDetailsDTO convertToBuySellDTO = AutoMapperToGetBuysell(getbuuySellDetails);
            obj.Commit();
            return convertToBuySellDTO;
        }
        public BuySellDetailsDTO AutoMapperToGetBuysell(BuySellDetails buysellDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BuySellDetails, BuySellDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<BuySellDetails, BuySellDetailsDTO>(buysellDetails);
            return destination;
        }
        #endregion
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }
    }
}
