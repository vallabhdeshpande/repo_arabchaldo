﻿using AutoMapper;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace IVL.ArabChaldo.BAL
{
   public  class BuySellDetailsBAL : IDisposable
    {
        public BuySellDetailsBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public BuySellDetailsBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

        public bool CheckBuySellDetailsExits(string Title, long CategoryId, long buySellId)
        {
            return obj.BuySellDetails.CheckbuySellDetailsExits(Title, CategoryId, buySellId);
        }
        public bool buySellAdStatus(long buySellId, string remarks, string Action, long CreatedBy)
        {
            var adDetailsfetched = obj.BuySellDetails.GetbuySellDetailsById(buySellId);
            bool isStatus = obj.BuySellDetails.buySellAdStatus(buySellId, remarks, Action, CreatedBy);
            obj.Commit();
            if (isStatus)
            {

                // Mail Functionality
       
                ConfigurationBAL configBal = new ConfigurationBAL();
                ConfigurationDTO configDetail = new ConfigurationDTO();
                string subject = "";
                string title = "";
                subject = "Arabchaldo - Buy/Sell Approved: #" + adDetailsfetched.BuySellId.ToString(); //+ "- " + adDetailsfetched.Title; // "Arab Chaldo : Buy/Sell Advertisement Approved"
                title = adDetailsfetched.Title;
                configDetail = configBal.GetExistingConfigDetails();
                if (Action == "Approve")

                    configBal.SetMailDetails("ApprovedBuyselldetails", adDetailsfetched.Email, adDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, "",title);
              

                if (Action == "Reject")
                {
                    subject = "Arabchaldo - Buy/Sell Rejected: #" + adDetailsfetched.BuySellId.ToString(); //+ "- " + adDetailsfetched.Title; // "Arab Chaldo : Buy/Sell Rejected"
                    title = adDetailsfetched.Title;
                    string RejectRemark = remarks.Split(',')[2].ToString();

                    configBal.SetMailDetails("RejectedBuyselldetails", adDetailsfetched.Email, adDetailsfetched.ContactPersonName,subject, configDetail.DisclaimerTemplate, RejectRemark,title);
                }


                if (Action == "Delete")
                {
                    subject = "Arabchaldo - Buy/Sell Deleted: #" + adDetailsfetched.BuySellId.ToString(); //+ "- " + adDetailsfetched.Title; // "Arab Chaldo : Buy/Sell Rejected"
                    title = adDetailsfetched.Title;
                   

                    configBal.SetMailDetails("DeletedBuyselldetails", adDetailsfetched.Email, adDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, "",title);
                }

            }




            return isStatus;
        }
        public DataTable GetAllBuySellDetails(long loggedInUserId, string loggedInRole)
        {
            return obj.BuySellDetails.GetAllBuySellDetails(loggedInUserId, loggedInRole);

        }
        public BuySellDetails AutoMapperToInsertBuySellDetails(BuySellDetailsDTO buysellDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BuySellDetailsDTO, BuySellDetails>();
            });
            IMapper iMapper = config.CreateMapper();
            var destination = iMapper.Map<BuySellDetailsDTO, BuySellDetails>(buysellDetails);
            return destination;
        }
        public bool InsertbuysellDetails(BuySellDetailsDTO BuySellDetails)
        {
           PaymentPlanBAL paymentPlanBAL = new PaymentPlanBAL();
            bool buysellDetailsexist = CheckBuySellDetailsExits(BuySellDetails.Title, BuySellDetails.CategoryId, BuySellDetails.BuySellId);
            bool check = false;
            if (!buysellDetailsexist)
            {
                BuySellDetails insertBuysellDetails = AutoMapperToInsertBuySellDetails(BuySellDetails);
                check = obj.BuySellDetails.InsertBuySellDetails(insertBuysellDetails);
                obj.Commit();

                if (check == true)
                {
                    // Mail Functionality
                    ConfigurationDTO configDetail = new ConfigurationDTO();
                    ConfigurationBAL configBal = new ConfigurationBAL();
                    configDetail = configBal.GetExistingConfigDetails();
                    string subject = "";
                    string title = "";
                    subject = "Arabchaldo - Buy/Sell Submitted: #" + insertBuysellDetails.BuySellId.ToString(); //+ "- " + adDetailsfetched.Title; //  "Arab Chaldo - Buy/Sell Advertisement Submitted"
                    title = BuySellDetails.Title;
                    configBal.SetMailDetails("Buy Sell", BuySellDetails.Email, BuySellDetails.ContactPersonName,subject, configDetail.DisclaimerTemplate, "",title);
                   
                }           
            }
            if (check && (BuySellDetails.PlanId != null))
            {
                if (BuySellDetails.PlanId != 0)
                {
                    if (BuySellDetails.BuySellCount == null)
                        BuySellDetails.BuySellCount = 0;
                    paymentPlanBAL.updatePlanForbuySell((long)BuySellDetails.PlanId, (long)BuySellDetails.BuySellCount);


                }
            }

            return check;
        }
        public bool UpdatebuySellDetails(long buysellId, BuySellDetailsDTO buySellDetails)
        {
            PaymentPlanBAL paymentPlanBAL = new PaymentPlanBAL();
            bool buysellDetailsexist = CheckBuySellDetailsExits(buySellDetails.Title, buySellDetails.CategoryId, buySellDetails.BuySellId);
            bool check = false;
            if (!buysellDetailsexist)
            {
                BuySellDetails updatebuysellDetails = AutoMapperToUpdateBuySellData(buysellId, buySellDetails);
                check = obj.BuySellDetails.UpdateBuySellDetails(buysellId, updatebuysellDetails);
                obj.Commit();
            }
            if (check == true)
            {
                // Mail Functionality
                ConfigurationDTO configDetail = new ConfigurationDTO();
                ConfigurationBAL configBal = new ConfigurationBAL();
                string subject = "";
                string title = "";
                subject = "Arabchaldo - Buy/Sell Updated: #" + buySellDetails.BuySellId.ToString(); //+ "- " + adDetailsfetched.Title; //  "Arab Chaldo - Buy/Sell Advertisement Updated"
                title = buySellDetails.Title;
                configDetail = configBal.GetExistingConfigDetails();
                 configBal.SetMailDetails("UpdateBuysellDetails", buySellDetails.Email, buySellDetails.ContactPersonName, subject, configDetail.DisclaimerTemplate, "",title);
                
            }


            //if (check && (AdDetails.PlanId != null))
            //{
            //    if (AdDetails.PlanId != 0)
            //        paymentPlanBAL.updatePlanForAds((long)AdDetails.PlanId, (long)AdDetails.LogoCount, (long)AdDetails.AdImageCount, (long)AdDetails.JobsCount);
            //}
            return check;
        }
        public BuySellDetailsDTO GetbuySellDetailsById(long buysellDetailsId)
        {
            var buysellDetailsfetched = obj.BuySellDetails.GetbuySellDetailsById(buysellDetailsId);
            BuySellDetailsDTO getDetails = AutoMapperToGetBuySellDetails(buysellDetailsfetched);

            CommonBAL commonBal = new CommonBAL();

            getDetails.CountryName = "USA";

            if (buysellDetailsfetched.StateName == "null")
                getDetails.StateName = "";

            if (buysellDetailsfetched.CityName == "null")
                getDetails.CityName = "";


            //CategoryName
            DataTable categoryTable = new DataTable();
            categoryTable = commonBal.FillCategoryDDL();
            DataRow foundRowCategory = categoryTable.Select("CategoryId=" + getDetails.CategoryId)[0];
            if (foundRowCategory != null)
                getDetails.CategoryName = foundRowCategory["CategoryName"].ToString();

            return getDetails;
        }

        public BuySellDetailsDTO AutoMapperToGetBuySellDetails(IVL.ArabChaldo.DAL.Models.BuySellDetails buysellDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BuySellDetails, BuySellDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<BuySellDetails, BuySellDetailsDTO>(buysellDetails);
            return destination;
        }

        public BuySellDetails AutoMapperToUpdateBuySellData(long buySellId, BuySellDetailsDTO buySellDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BuySellDetailsDTO, BuySellDetails>();
            });
            IMapper iMapper = config.CreateMapper();
            var AdDetailsfethed = obj.AdDetails.GetAdDetailsById(buySellId);
            var destination = iMapper.Map<BuySellDetailsDTO, BuySellDetails>(buySellDetails);
            return destination;
        }

        public List<BuySellDetailsDTO> GetAllBuySellsDetails(long loggedInUserId, string loggedInRole)
        {

            List<BuySellDetails> buySelllDetailsList = new List<BuySellDetails>();
            buySelllDetailsList = obj.BuySellDetails.GetAllBuySellsDetails(loggedInUserId, loggedInRole);

            List<BuySellDetailsDTO> buySellDetailsListBoxig = new List<BuySellDetailsDTO>();

            foreach (var listAdDetails in buySelllDetailsList)
            {
                buySellDetailsListBoxig.Add(AutoMapperToGetBuySellDetails(listAdDetails));
            }

            return buySellDetailsListBoxig;
        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }
    }
}
