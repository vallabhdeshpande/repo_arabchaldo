﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace IVL.ArabChaldo.BAL
{
    public class DashboardBAL
    {
        public DashboardBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public DashboardBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

        public DataTable GetotalCount()
        {
            return obj.Dashboard.GetotalCount();
        }

        public DataTable GetBargraphdata(long year)
        {
            return obj.Dashboard.GetBargraphdata(year);
        }
        public DataTable GeBUtotalCount(long UserId)
        {
            return obj.Dashboard.GeBUtotalCount(UserId);
        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }
    }
}
