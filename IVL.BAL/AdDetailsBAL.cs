﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;
using System.Data;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;

namespace IVL.ArabChaldo.BAL
{
    public class AdDetailsBAL : IDisposable
    {
        public AdDetailsBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public AdDetailsBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

        public bool CheckAdDetailsExits(string Title, long CategoryId, long AdId)
        {
            return obj.AdDetails.CheckAdDetailsExits(Title, CategoryId, AdId);
        }

        public bool AdStatus(long AdDetailsID, string remarks, string Action, long CreatedBy)
        {
            var adDetailsfetched = obj.AdDetails.GetAdDetailsById(AdDetailsID);
            bool isStatus = obj.AdDetails.AdStatus(AdDetailsID, remarks, Action, CreatedBy);
            obj.Commit();
            if (isStatus)
            {


                //var adDetailsfetched = obj.AdDetails.GetAdDetailsById(AdDetailsID);
                //ConfigurationBAL configBal = new ConfigurationBAL();
                //if(Action=="Approve")
                //configBal.SetMailDetails("ApprovedAddetails",adDetailsfetched.Email, adDetailsfetched.ContactPersonName, "Ad Approved Successfully");
                //else if (Action == "Reject")
                //configBal.SetMailDetails("RejectedAddetails", adDetailsfetched.Email, adDetailsfetched.ContactPersonName, "Ad Rejected Successfully");



                // Mail Functionality
                
                ConfigurationBAL configBal = new ConfigurationBAL();
                ConfigurationDTO configDetail = new ConfigurationDTO();

                configDetail = configBal.GetExistingConfigDetails();
                string subject = "";
                string title = "";
                if
                    (Action == "Approve" && configDetail.JobCategoryId != adDetailsfetched.CategoryId)
                {
                    subject = "Arabchaldo - Business Listing Approved: #" + adDetailsfetched.AdId.ToString(); //+ "- " + adDetailsfetched.Title; //"Arab Chaldo - Advertisement Submitted"
                    title = adDetailsfetched.Title;
                    configBal.SetMailDetails("ApprovedAddetails", adDetailsfetched.Email, adDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate,"", title);
                }
                else if (Action == "Reject" && configDetail.JobCategoryId != adDetailsfetched.CategoryId)
                {
                    subject = "Arabchaldo - Business Listing Rejected: #" + adDetailsfetched.AdId.ToString(); //+ "- " + adDetailsfetched.Title; //"Arab Chaldo - Advertisement Submitted"
                    title = adDetailsfetched.Title;
                    string RejectRemark = remarks.Split(',')[2].ToString();
                    configBal.SetMailDetails("RejectedAddetails", adDetailsfetched.Email, adDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, RejectRemark, title);
                }

                else if (Action == "Delete" && isStatus==true && configDetail.JobCategoryId != adDetailsfetched.CategoryId)
                {
                    subject = "Arabchaldo - Business Listing Deleted : #" + adDetailsfetched.AdId.ToString(); //+ "- " + adDetailsfetched.Title; //"Arab Chaldo - Advertisement Submitted"
                    title = adDetailsfetched.Title;
      
                    configBal.SetMailDetails("DeleteAddetails", adDetailsfetched.Email, adDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, "", title);
                }

                if (Action == "Approve" && configDetail.JobCategoryId == adDetailsfetched.CategoryId)
                {
                    subject = "Arabchaldo - Job Approved: #" + adDetailsfetched.AdId.ToString(); //+ "- " + adDetailsfetched.Title; //"Arab Chaldo : Advertisement Rejected"
                    title = adDetailsfetched.Title;
                    string RejectRemark = remarks.Split(',')[2].ToString();

                    configBal.SetMailDetails("ApprovedJobdetails", adDetailsfetched.Email, adDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, RejectRemark,title);
                }
                else if (Action == "Reject"&& configDetail.JobCategoryId == adDetailsfetched.CategoryId)

                {
                    subject = "Arabchaldo - Job Rejected: #" + adDetailsfetched.AdId.ToString(); //+ "- " + adDetailsfetched.Title; // "Arab Chaldo : Job Rejected "
                    title = adDetailsfetched.Title;
                    string RejectRemark = remarks.Split(',')[2].ToString();
                    configBal.SetMailDetails("RejectedJobdetails", adDetailsfetched.Email, adDetailsfetched.ContactPersonName,subject, configDetail.DisclaimerTemplate, RejectRemark, title);
                }


                else if (Action == "Delete" && isStatus == true && configDetail.JobCategoryId == adDetailsfetched.CategoryId)
                {
                    subject = "Arabchaldo - Job Deleted : #" + adDetailsfetched.AdId.ToString(); //+ "- " + adDetailsfetched.Title; //"Arab Chaldo - Advertisement Submitted"
                    title = adDetailsfetched.Title;

                    configBal.SetMailDetails("DeleteJobdetails", adDetailsfetched.Email, adDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, "", title);
                }
            }




            return isStatus;
        }



        public bool UpdateAdDetails(long AdDetailsID, AdDetailsDTO AdDetails)
        {

            var adDetailsfetched = obj.AdDetails.GetAdDetailsById(AdDetailsID);
            PaymentPlanBAL paymentPlanBAL = new PaymentPlanBAL();
            bool adDetailsexist = CheckAdDetailsExits(AdDetails.Title, AdDetails.CategoryId, AdDetails.AdId);
            bool check = false;
            if (!adDetailsexist)
            {
                AdDetails updateAdDetails = AutoMapperToUpdateUser(AdDetailsID, AdDetails);
                check = obj.AdDetails.UpdateAdDetails(AdDetailsID, updateAdDetails);
                obj.Commit();
            }


            //if (check == true)
            //{
            //    // Mail Functionality
            //    ConfigurationBAL configBal = new ConfigurationBAL();               
            //    configBal.SetMailDetails("Listing Details", AdDetails.Email, AdDetails.ContactPersonName, "Ad Updated Successfully");

            //}



            if (check == true)
            {
                // Mail Functionality
                ConfigurationDTO configDetail = new ConfigurationDTO();
                ConfigurationBAL configBal = new ConfigurationBAL();
                configDetail = configBal.GetExistingConfigDetails();

                string subject = "";
                string title = "";
                if (configDetail.JobCategoryId == AdDetails.CategoryId)
                {
                    subject = "Arabchaldo - Job Updated: #" + AdDetails.AdId.ToString(); //+ "- " + AdDetails.Title; //"Arab Chaldo - Job Updated"
                    title = AdDetails.Title;
                    configBal.SetMailDetails("JobupdateDetails", AdDetails.Email, AdDetails.ContactPersonName, subject, configDetail.DisclaimerTemplate, "", title);
                }





                else
                {
                    subject = "Arabchaldo - Business Listing Updated: #" + AdDetails.AdId.ToString(); //+ "- " + AdDetails.Title; //"Arab Chaldo : Advertisement Updated"
                    title = AdDetails.Title;
                    configBal.SetMailDetails("UpdateAdDetails", AdDetails.Email +","+ adDetailsfetched.Email , AdDetails.ContactPersonName, subject, configDetail.DisclaimerTemplate, "", title);
                   // configBal.SetMailDetails("UpdateAdDetails", adDetailsfetched.Email, adDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, "", title);
                }


                //else{
                //    subject = "Arabchaldo - Business Listing Updated: #" + adDetailsfetched.AdId.ToString(); //+ "- " + AdDetails.Title; //"Arab Chaldo : Advertisement Updated"
                //    title = AdDetails.Title;
                //    configBal.SetMailDetails("UpdateAdDetails", adDetailsfetched.Email, adDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, "", title);


                

              
            }
        

            if (check && (AdDetails.PlanId!= null))
            {
                if (AdDetails.PlanId != 0)
                    paymentPlanBAL.updatePlanForAds((long)AdDetails.PlanId, (long)AdDetails.LogoCount, (long)AdDetails.AdImageCount, (long)AdDetails.JobsCount);
            }
            return check;
        }
        public bool InsertAdDetails(AdDetailsDTO AdDetails)
        {
            PaymentPlanBAL paymentPlanBAL = new PaymentPlanBAL();
            bool adDetailsexist = CheckAdDetailsExits(AdDetails.Title, AdDetails.CategoryId, AdDetails.AdId);
            bool check = false;
            if (!adDetailsexist)
            {
                AdDetails insertAdDetails = AutoMapperToInsertAdDetails(AdDetails);
                check = obj.AdDetails.InsertAdDetails(insertAdDetails);
                obj.Commit();

                if (check == true)
                {
                    // Mail Functionality
                    ConfigurationDTO configDetail = new ConfigurationDTO();
                    ConfigurationBAL configBal = new ConfigurationBAL();
                    configDetail = configBal.GetExistingConfigDetails();

                    string subject = "";
                    string title = "";

                    if (configDetail.JobCategoryId != AdDetails.CategoryId)
                    {



                        subject = "Arabchaldo - Business Listing Submitted: #" + insertAdDetails.AdId.ToString();// + "- " + insertAdDetails.Title; //"Arab Chaldo - Advertisement Submitted"
                        title = AdDetails.Title;
                        configBal.SetMailDetails("Business Listing Details", AdDetails.Email, AdDetails.ContactPersonName, subject, configDetail.DisclaimerTemplate, "", title);


                    }

                    //else if (configDetail.BuySellCategoryId == AdDetails.CategoryId)

                    //    configBal.SetMailDetails("Buy Sell", AdDetails.Email, AdDetails.ContactPersonName, "", "", "", "");
                    else
                    {
                        subject = "Arabchaldo - Job Submitted: #" + insertAdDetails.AdId.ToString(); //+ "- " + insertAdDetails.Title; //"Arab Chaldo - Jobs Submitted"
                        title = AdDetails.Title;
                        configBal.SetMailDetails("Job Details", AdDetails.Email, AdDetails.ContactPersonName, subject, configDetail.DisclaimerTemplate, "", title);

                    }
                }


            }


      

            if (check && (AdDetails.PlanId!= null))
            {
                if (AdDetails.PlanId != 0)
                {
                    if (AdDetails.JobsCount == null)
                        AdDetails.JobsCount = 0;
                        paymentPlanBAL.updatePlanForAds((long)AdDetails.PlanId, (long)AdDetails.LogoCount, (long)AdDetails.AdImageCount, (long)AdDetails.JobsCount);
                   
                    
                }
            }
            return check;
        }


        public AdDetailsDTO GetAdDetailsById(long adDetailsId)
        {
            var adDetailsfetched = obj.AdDetails.GetAdDetailsById(adDetailsId);
            AdDetailsDTO getAdDetails = AutoMapperToGetAdDetails(adDetailsfetched);

            CommonBAL commonBal = new CommonBAL();
            //CountryName
            //DataTable countryTable = new DataTable();
            //countryTable = commonBal.FillCountryDDL();
            //DataRow foundRowCountry = countryTable.Select("CountryId=" + getAdDetails.CountryId)[0];
            //if (foundRowCountry != null)
                getAdDetails.CountryName = "USA";

            //StateName
            //DataTable stateTable = new DataTable();
            //stateTable = commonBal.FillStateDDLByCountryId(getAdDetails.CountryId);
            //DataRow foundRowState = stateTable.Select("StateId=" + getAdDetails.StateId)[0];
            //if (foundRowState != null)
            //    getAdDetails.StateName = foundRowState["StateName"].ToString();
            if(adDetailsfetched.StateName=="null")
            getAdDetails.StateName = "";

            //CityName
            //DataTable cityTable = new DataTable();
            //cityTable = commonBal.FillCityDDLByStateId(getAdDetails.StateId);
            //DataRow foundRowCity = cityTable.Select("CityId=" + getAdDetails.CityId)[0];
            //if (foundRowCity != null)
            //    getAdDetails.CityName = foundRowCity["CityName"].ToString();
            if (adDetailsfetched.CityName == "null")
                getAdDetails.CityName = "";


            //CategoryName
            DataTable categoryTable = new DataTable();
            categoryTable = commonBal.FillCategoryDDL();
            DataRow foundRowCategory = categoryTable.Select("CategoryId=" + getAdDetails.CategoryId)[0];
            if (foundRowCategory != null)
                getAdDetails.CategoryName = foundRowCategory["CategoryName"].ToString();

            //SubcategoryName
            //DataTable subcategoryTable = new DataTable();
            //if (getAdDetails.SubCategoryId != 0)
            //{
            //    subcategoryTable = commonBal.FillSubCategoryDDLByCategoryId(getAdDetails.CategoryId);
            //    if (subcategoryTable.Rows.Count>0)
            //    {
            //        DataRow foundRowSubCategory = subcategoryTable.Select("SubCategoryId=" + getAdDetails.SubCategoryId)[0];
            //        if (foundRowSubCategory != null)
            //            getAdDetails.SubCategoryName = foundRowSubCategory["SubCategoryName"].ToString(); //--Select SubCategory--
            //        //else
            //           // getAdDetails.SubCategoryName = "--Select SubCategory--";
            //    }
            //    else
            //    {
            //        //getAdDetails.SubCategoryName = "--Select SubCategory--";
            //        getAdDetails.SubCategoryId = 0;
            //    }
                
            //}

            //UserName
            //DataTable userTable = new DataTable();
            //userTable = commonBal.FillUserDDL();
            //DataRow foundRowUser = userTable.Select("UserId=" + getAdDetails.PostedFor)[0];
            //if (foundRowUser != null)
            //    getAdDetails.PostedForName = foundRowUser["FirstName"].ToString()+" "+ foundRowUser["LastName"].ToString();






            return getAdDetails;
        }

        public List<AdDetailsDTO> GetAllAdDetails(long loggedInUserId, string loggedInRole)
        {

            List<AdDetails> AdDetailsList = new List<AdDetails>();
            AdDetailsList = obj.AdDetails.GetAllAdDetails(loggedInUserId, loggedInRole);

            List<AdDetailsDTO> AdDetailsListBoxig = new List<AdDetailsDTO>();

            foreach (var listAdDetails in AdDetailsList)
            {
                AdDetailsListBoxig.Add(AutoMapperToGetAdDetails(listAdDetails));
            }

            return AdDetailsListBoxig;
        }

        public DataTable GetAllAdsDetails(long loggedInUserId, string loggedInRole, string categoryName)
        {
            return obj.AdDetails.GetAllAdsDetails(loggedInUserId, loggedInRole, categoryName);
           
        }
        public List<AdDetailsDTO> GetAllJobDetails(long loggedInUserId, string loggedInRole)
        {

            List<AdDetails> AdDetailsList = new List<AdDetails>();
            AdDetailsList = obj.AdDetails.GetAllJobDetails(loggedInUserId, loggedInRole);

            List<AdDetailsDTO> AdDetailsListBoxig = new List<AdDetailsDTO>();

            foreach (var listAdDetails in AdDetailsList)
            {
                AdDetailsListBoxig.Add(AutoMapperToGetAdDetails(listAdDetails));
            }

            return AdDetailsListBoxig;
        }
        public List<AdDetailsDTO> GetAdsAdListBycategoryId(long categoryId)
        {

            List<AdDetails> AdDetailsList = new List<AdDetails>();
            AdDetailsList = obj.AdDetails.GetAdsAdListBycategoryId(categoryId);

            List<AdDetailsDTO> AdDetailsListBoxig = new List<AdDetailsDTO>();

            foreach (var listAdDetails in AdDetailsList)
            {
                AdDetailsListBoxig.Add(AutoMapperToGetAdDetails(listAdDetails));
            }

            return AdDetailsListBoxig;
        }
        public List<AdDetailsDTO> GetAdsAdListBySubcategoryId(long SubcategoryId)
        {

            List<AdDetails> AdDetailsList = new List<AdDetails>();
            AdDetailsList = obj.AdDetails.GetAdsAdListBySubcategoryId(SubcategoryId);

            List<AdDetailsDTO> AdDetailsListBoxig = new List<AdDetailsDTO>();

            foreach (var listAdDetails in AdDetailsList)
            {
                AdDetailsListBoxig.Add(AutoMapperToGetAdDetails(listAdDetails));
            }

            return AdDetailsListBoxig;
        }
        public AdDetailsDTO AutoMapperToGetAdDetails(IVL.ArabChaldo.DAL.Models.AdDetails AdDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AdDetails, AdDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<AdDetails, AdDetailsDTO>(AdDetails);
            return destination;
        }

        public AdDetails AutoMapperToInsertAdDetails(AdDetailsDTO AdDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AdDetailsDTO, AdDetails>();
            });
            IMapper iMapper = config.CreateMapper();
            var destination = iMapper.Map<AdDetailsDTO, AdDetails>(AdDetails);
            return destination;
        }

        public AdDetails AutoMapperToUpdateUser(long AdDetailsID, AdDetailsDTO AdDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AdDetailsDTO, AdDetails>();
            });
            IMapper iMapper = config.CreateMapper();
            var AdDetailsfethed = obj.AdDetails.GetAdDetailsById(AdDetailsID);
            var destination = iMapper.Map<AdDetailsDTO, AdDetails>(AdDetails);
            return destination;
        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }

    }
}