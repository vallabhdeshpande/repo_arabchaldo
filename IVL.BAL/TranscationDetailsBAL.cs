﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;
using IVL.ArabChaldo.DAL.Repository;

namespace IVL.ArabChaldo.BAL
{
    public class TranscationDetailsBAL: IDisposable
    {
        private readonly IUnitOfWork _unitOfWork;

        public TranscationDetailsBAL(IUnitOfWork objOfWork)
        {
            this._unitOfWork = objOfWork;
        }

        public TranscationDetailsBAL() : this(new UnitOfWork())
        {

        }

        public bool InsertTransaction(TranscationDetailsDTO transactionDetailsDto)
        {
            TransactionDetails transactionDetails = new TransactionDetails();

            transactionDetails= AutoMapperToAddTransaction(transactionDetailsDto);

            bool success = this._unitOfWork.TransactionDetails.InsertTransaction(transactionDetails);
            return true;
        }

        public TransactionDetails AutoMapperToAddTransaction(TranscationDetailsDTO transcation)
        {
            //long postedForWhom = Convert.ToInt32(paymentPlan.UserId);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TranscationDetailsDTO, TransactionDetails>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<TranscationDetailsDTO, TransactionDetails>(transcation);
            return destination;
        }

        public void Dispose()
        {
            if (_unitOfWork != null)
            {
                this._unitOfWork.Dispose();
            }
        }
    }
}
