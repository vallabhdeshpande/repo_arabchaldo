﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;

namespace IVL.ArabChaldo.BAL
{
    public class CountryBAL : IDisposable
    {
        public CountryBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public CountryBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

        public bool CheckCountryExits(string CountryName)
        {
            return obj.Country.CheckCountryExits(CountryName);
        }

        public bool DeleteCountry(int CountryID)
        {
            bool isDeleted = obj.Country.DeleteCountry(CountryID);
            obj.Commit();
            return isDeleted;
        }
        public bool UpdateCountry(int CountryID, CountryDTO Country)
        {
            Country updateCountry = AutoMapperToUpdateUser(CountryID, Country);
            bool isUpdated = obj.Country.UpdateCountry(CountryID, updateCountry);
            obj.Commit();
            return isUpdated;
        }
        public bool InsertCountry(CountryDTO Country)
        {
            bool countryexist = CheckCountryExits(Country.CountryName);
            bool check = false;
            if (!countryexist)
            {
                Country insertCountry = AutoMapperToInsertCountry(Country);
                check = obj.Country.InsertCountry(insertCountry);
                obj.Commit();
            }
            return check;
        }


        public CountryDTO GetCountryById(int countryId)
        {
            var countryfetched = obj.Country.GetCountryById(countryId);
            CountryDTO getCountry = AutoMapperToGetCountry(countryfetched);

            return getCountry;
        }

        public List<CountryDTO> GetAllCountry()
        {

            List<Country> CountryList = new List<Country>();
            CountryList = obj.Country.GetAllCountry();

            List<CountryDTO> CountryListBoxig = new List<CountryDTO>();

            foreach (var listCountry in CountryList)
            {
                CountryListBoxig.Add(AutoMapperToGetCountry(listCountry));
            }

            return CountryListBoxig;
        }

        public CountryDTO AutoMapperToGetCountry(IVL.ArabChaldo.DAL.Models.Country Country)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Country, CountryDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<Country, CountryDTO>(Country);
            return destination;
        }

        public Country AutoMapperToInsertCountry(CountryDTO Country)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CountryDTO, Country>();
            });
            IMapper iMapper = config.CreateMapper();
            var destination = iMapper.Map<CountryDTO, Country>(Country);
            return destination;
        }

        public Country AutoMapperToUpdateUser(int CountryID, CountryDTO Country)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CountryDTO, Country>();
            });
            IMapper iMapper = config.CreateMapper();
            var Countryfethed = obj.Country.GetCountryById(CountryID);
            var destination = iMapper.Map<CountryDTO, Country>(Country);
            return destination;
        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }

    }
}