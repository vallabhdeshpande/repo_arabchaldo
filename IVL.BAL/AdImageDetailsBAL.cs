﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;

namespace IVL.ArabChaldo.BAL
{
    public class AdImageDetailsBAL : IDisposable
    {
        public AdImageDetailsBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public AdImageDetailsBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

        public bool CheckAdImageDetailsExits(string AdImageDetailsName)
        {
            return obj.AdImageDetails.CheckAdImageDetailsExits(AdImageDetailsName);
        }

        public bool DeleteAdImageDetails(int AdImageDetailsID)
        {
            bool isDeleted = obj.AdImageDetails.DeleteAdImageDetails(AdImageDetailsID);
            obj.Commit();
            return isDeleted;
        }
        public bool UpdateAdImageDetails(int AdImageDetailsID, AdImageDetailsDTO AdImageDetails)
        {
            AdImageDetails updateAdImageDetails = AutoMapperToUpdateUser(AdImageDetailsID, AdImageDetails);
            bool isUpdated = obj.AdImageDetails.UpdateAdImageDetails(AdImageDetailsID, updateAdImageDetails);
            obj.Commit();
            return isUpdated;
        }
        public bool InsertAdImageDetails(AdImageDetailsDTO AdImageDetails)
        {
            bool adImageDetailsexist = CheckAdImageDetailsExits(AdImageDetails.AdImageUrl);
            bool check = false;
            if (!adImageDetailsexist)
            {
                AdImageDetails insertAdImageDetails = AutoMapperToInsertAdImageDetails(AdImageDetails);
                check = obj.AdImageDetails.InsertAdImageDetails(insertAdImageDetails);
                obj.Commit();
            }
            return check;
        }


        public AdImageDetailsDTO GetAdImageDetailsById(int adImageDetailsId)
        {
            var adImageDetailsfetched = obj.AdImageDetails.GetAdImageDetailsById(adImageDetailsId);
            AdImageDetailsDTO getAdImageDetails = AutoMapperToGetAdImageDetails(adImageDetailsfetched);

            return getAdImageDetails;
        }

        public List<AdImageDetailsDTO> GetAllAdImageDetails()
        {

            List<AdImageDetails> AdImageDetailsList = new List<AdImageDetails>();
            AdImageDetailsList = obj.AdImageDetails.GetAllAdImageDetails();

            List<AdImageDetailsDTO> AdImageDetailsListBoxig = new List<AdImageDetailsDTO>();

            foreach (var listAdImageDetails in AdImageDetailsList)
            {
                AdImageDetailsListBoxig.Add(AutoMapperToGetAdImageDetails(listAdImageDetails));
            }

            return AdImageDetailsListBoxig;
        }

        public AdImageDetailsDTO AutoMapperToGetAdImageDetails(IVL.ArabChaldo.DAL.Models.AdImageDetails AdImageDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AdImageDetails, AdImageDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<AdImageDetails, AdImageDetailsDTO>(AdImageDetails);
            return destination;
        }

        public AdImageDetails AutoMapperToInsertAdImageDetails(AdImageDetailsDTO AdImageDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AdImageDetailsDTO, AdImageDetails>();
            });
            IMapper iMapper = config.CreateMapper();
            var destination = iMapper.Map<AdImageDetailsDTO, AdImageDetails>(AdImageDetails);
            return destination;
        }

        public AdImageDetails AutoMapperToUpdateUser(int AdImageDetailsID, AdImageDetailsDTO AdImageDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AdImageDetailsDTO, AdImageDetails>();
            });
            IMapper iMapper = config.CreateMapper();
            var AdImageDetailsfethed = obj.AdImageDetails.GetAdImageDetailsById(AdImageDetailsID);
            var destination = iMapper.Map<AdImageDetailsDTO, AdImageDetails>(AdImageDetails);
            return destination;
        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }

    }
}