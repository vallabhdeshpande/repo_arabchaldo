﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;
using System.Data;

namespace IVL.ArabChaldo.BAL
{
    public class SubCategoryBAL : IDisposable
    {
        public SubCategoryBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public SubCategoryBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

        public bool CheckSubCategoryExits(string SubCategoryName, long CategoryId)
        {
            return obj.SubCategory.CheckSubCategoryExits(SubCategoryName, CategoryId);
        }

        public bool DeleteSubCategory(int SubCategoryID)
        {
            bool isDeleted = obj.SubCategory.DeleteSubCategory(SubCategoryID);
            obj.Commit();
            return isDeleted;
        }
        public bool UpdateSubCategory(int SubCategoryID, SubCategoryDTO SubCategory)
        {
            SubCategory updateSubCategory = AutoMapperToUpdateUser(SubCategoryID, SubCategory);
            bool isUpdated = obj.SubCategory.UpdateSubCategory(SubCategoryID, updateSubCategory);
            obj.Commit();
            return isUpdated;
        }

        public bool UpdateSubcategoryWhenCategoryUpdated(int categoryId, string updatedCategoryName)
        {
            return obj.SubCategory.UpdateSubcategoryWhenCategoryUpdated(categoryId, updatedCategoryName);
        }

        public bool InsertSubCategory(SubCategoryDTO SubCategory)
        {
            bool subCategoryexist = CheckSubCategoryExits(SubCategory.SubCategoryName,SubCategory.CategoryId);
            bool check = false;
            ManageCategoryBAL manageCategoryBAL = new ManageCategoryBAL();
            int categoryID =(int) SubCategory.CategoryId;
            CategoryDTO categoryDTO = manageCategoryBAL.GetCategorybyId(categoryID);


            if (!subCategoryexist)
            {
                SubCategory.CategoryName = categoryDTO.CategoryName;
                SubCategory insertSubCategory = AutoMapperToInsertSubCategory(SubCategory);
                check = obj.SubCategory.InsertSubCategory(insertSubCategory);
                obj.Commit();
            }
            return check;
        }

        public bool CountOfSubcategory(int id)
        {
           return obj.SubCategory.GetSubCategoryCountForCategory(id);
        }

        public SubCategoryDTO GetSubCategoryById(int subCategoryId)
        {
            var subCategoryfetched = obj.SubCategory.GetSubCategoryById(subCategoryId);
            SubCategoryDTO getSubCategory = AutoMapperToGetSubCategory(subCategoryfetched);

            return getSubCategory;
        }

        public List<SubCategoryDTO> GetAllSubCategory()
        {

            List<SubCategory> SubCategoryList = new List<SubCategory>();
            SubCategoryList = obj.SubCategory.GetAllSubCategory();

            List<SubCategoryDTO> SubCategoryListBoxig = new List<SubCategoryDTO>();
            
            foreach (var listSubCategory in SubCategoryList)
            {
                SubCategoryListBoxig.Add(AutoMapperToGetSubCategory(listSubCategory));
            }

            return SubCategoryListBoxig;
        }

        public SubCategoryDTO AutoMapperToGetSubCategory(IVL.ArabChaldo.DAL.Models.SubCategory SubCategory)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SubCategory, SubCategoryDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<SubCategory, SubCategoryDTO>(SubCategory);
            return destination;
        }

        public SubCategory AutoMapperToInsertSubCategory(SubCategoryDTO SubCategory)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SubCategoryDTO, SubCategory>();
            });
            IMapper iMapper = config.CreateMapper();
            var destination = iMapper.Map<SubCategoryDTO, SubCategory>(SubCategory);
            return destination;
        }

        public SubCategory AutoMapperToUpdateUser(int SubCategoryID, SubCategoryDTO SubCategory)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SubCategoryDTO, SubCategory>();
            });
            IMapper iMapper = config.CreateMapper();
            var SubCategoryfethed = obj.SubCategory.GetSubCategoryById(SubCategoryID);
            var destination = iMapper.Map<SubCategoryDTO, SubCategory>(SubCategory);
            return destination;
        }

        // FillSubCategory DDl By CountryId

        public List<SubCategoryDTO> GetAllSubCategoriesByCategoryId(int categoryId)
        {

            List<SubCategory> SubCategoryList = new List<SubCategory>();
            SubCategoryList = obj.SubCategory.GetAllSubCategoriesByCategoryId(categoryId);

            List<SubCategoryDTO> SubCategoryListBoxig = new List<SubCategoryDTO>();

            foreach (var listSubCategory in SubCategoryList)
            {
                SubCategoryListBoxig.Add(AutoMapperToGetSubCategory(listSubCategory));
            }

            return SubCategoryListBoxig;
        }



        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }


        public DataTable FillSubcategoryList()
        {
            DataTable dtSubCategory = new DataTable();
            dtSubCategory = obj.SubCategory.FillSubcategoryList();
            return dtSubCategory;


        }

    }
}