﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;
using System.Data;

namespace IVL.ArabChaldo.BAL
{
   public class ManageCategoryBAL : IDisposable
    {
        public ManageCategoryBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public ManageCategoryBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;
  
        public bool CheckCategoryExits(string CategoryName)
        {
            return obj.categoryDetails.CheckCategoryExits(CategoryName);
        }

        public bool DeleteCategory(int CategoryID)
        {
            SubCategoryBAL subCategoryBAL = new SubCategoryBAL();
            bool exist = subCategoryBAL.CountOfSubcategory(CategoryID);            
            if(exist)
            {
                return false;
            }
            bool isDeleted = obj.categoryDetails.DeleteCategory(CategoryID);
            obj.Commit();
            return isDeleted;
        }
        public bool UpdateCategory(int CategoryID, CategoryDTO Categorydetails)
        {
            Category updateCategory = AutoMapperToUpdateUser(CategoryID, Categorydetails);
            bool isUpdated = obj.categoryDetails.UpdateCategory(CategoryID, updateCategory);
            SubCategoryBAL subCategoryBAL = new SubCategoryBAL();
            subCategoryBAL.UpdateSubcategoryWhenCategoryUpdated(CategoryID, Categorydetails.CategoryName);
            obj.Commit();
            return isUpdated;
        }
        public bool InsertCategory(CategoryDTO CategoryDetails)
        {

                bool categoryexist = CheckCategoryExits(CategoryDetails.CategoryName);
            bool check = false;
            if (!categoryexist)
            {
                Category insertCategory = AutoMapperToInsertCategory(CategoryDetails);
                check = obj.categoryDetails.InsertCategory(insertCategory);
                obj.Commit();
                //Int64 id = obj.categoryDetails.;
            }
            return check;
        }

        public CategoryDTO GetCategorybyId(int categoryId)
        {
            var categoryfetched = obj.categoryDetails.GetCategorybyId(categoryId);
            CategoryDTO getCategory = AutoMapperToGetCategory(categoryfetched);

            return getCategory;
        }

        public List<CategoryDTO> GetAllCategory()
        {

            List<Category> CategoryList = new List<Category>();
            CategoryList = obj.categoryDetails.GetAllCategory();

            List<CategoryDTO> CategoryListBoxig = new List<CategoryDTO>();

            foreach (var listCategory in CategoryList)
            {
                CategoryListBoxig.Add(AutoMapperToGetCategory(listCategory));
            }

            return CategoryListBoxig;
        }

        public CategoryDTO AutoMapperToGetCategory(IVL.ArabChaldo.DAL.Models.Category CategoryDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Category, CategoryDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<Category, CategoryDTO>(CategoryDetails);
            return destination;
        }

        public Category AutoMapperToInsertCategory(CategoryDTO CategoryDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CategoryDTO, Category>();
            });
            IMapper iMapper = config.CreateMapper();
            var destination = iMapper.Map<CategoryDTO, Category>(CategoryDetails);
            return destination;
        }

        public Category AutoMapperToUpdateUser(int CategoryID, CategoryDTO CategoryDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CategoryDTO, Category>();
            });
            IMapper iMapper = config.CreateMapper();
            var Categoryfethed = obj.categoryDetails.GetCategorybyId(CategoryID);
            var destination = iMapper.Map<CategoryDTO,Category>(CategoryDetails);
            return destination;
        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }




        public DataTable FillCategoryList()
        {
            DataTable dtCategory = new DataTable();
            dtCategory = obj.categoryDetails.FillCategoryList();
            return dtCategory;


        }

    }
}