﻿using AutoMapper;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.BAL
{
    public class ConfigurationBAL : IDisposable
    {
        public ConfigurationBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public ConfigurationBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

        public bool DeleteConfig(int ID)
        {
            bool isDeleted = obj.ConfigDetails.DeleteConfig(ID);
            obj.Commit();
            return isDeleted;
        }
        public bool UpdateConfig(int ID, ConfigurationDTO configdetails)
        {
            Configuration updateConfig = AutoMapperToUpdateConfig(ID, configdetails);
            bool isUpdated = obj.ConfigDetails.UpdateConfig(ID, updateConfig);
            obj.Commit();
            return isUpdated;
        }



        public List<ConfigurationDTO> GetAllConfigDetails()
        {

            List<Configuration> ConfigList = new List<Configuration>();
            ConfigList = obj.ConfigDetails.GetAllConfigDetails();

            List<ConfigurationDTO> ConfigListBoxig = new List<ConfigurationDTO>();

            foreach (var listConfig in ConfigList)
            {
                ConfigListBoxig.Add(AutoMapperToGetConfig(listConfig));
            }

            return ConfigListBoxig;
        }

        public ConfigurationDTO GetConfigDetailsbyId(int id)
        {
            var Userfethed = obj.ConfigDetails.GetConfigDetailsbyId(id);
            ConfigurationDTO configuration = AutoMapperToGetConfig(Userfethed);

            return configuration;
        }


        public ConfigurationDTO GetExistingConfigDetails()
        {
            var Userfethed = obj.ConfigDetails.GetExistingConfigDetails();
            ConfigurationDTO configuration = AutoMapperToGetConfig(Userfethed);

            return configuration;
        }


        public ConfigurationDTO AutoMapperToGetConfig(IVL.ArabChaldo.DAL.Models.Configuration configdetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Configuration, ConfigurationDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<Configuration, ConfigurationDTO>(configdetails);
            return destination;
        }

        public Configuration AutoMapperToUpdateConfig(int ID, ConfigurationDTO configDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ConfigurationDTO, Configuration>();
            });
            IMapper iMapper = config.CreateMapper();
            var Userfethed = obj.userDetails.GetUserbyId(ID);
            var destination = iMapper.Map<ConfigurationDTO, Configuration>(configDetails);
            return destination;
        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }


        public void SetMailDetails(string mailfor, string toMaliId, string UserName, string Subject,string Disclaimer,string RejectRemark,string Title)
        {
            EmailSenderDetailsDTO edMail = new EmailSenderDetailsDTO();
            ConfigurationDTO configDetail = new ConfigurationDTO();
            ConfigurationBAL configBal = new ConfigurationBAL();
            configDetail = configBal.GetExistingConfigDetails();
            edMail.mailfor = mailfor;
            edMail.email = toMaliId;
            edMail.subject = Subject;
            edMail.title = Title;
            edMail.remark = RejectRemark;
            edMail.disclaimerTemplate = Disclaimer;
            edMail.name = UserName;
            edMail.port = configDetail.Port;
            edMail.smtp = configDetail.Smtp;
            edMail.domain = configDetail.Domain;
            edMail.password = configDetail.Password;
            edMail.fromMailId = configDetail.FromMailId;
            EmailSender sendEmail = new EmailSender();
            sendEmail.SendAcknowledgement(edMail);

            sendEmail.SendEmail(edMail);


        }

    }
}

