﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;
using IVL.ArabChaldo.DAL.Repository;
using System.Data;
using Stripe;
using Newtonsoft.Json;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;

namespace IVL.ArabChaldo.BAL
{
    public class PaymentPlanBAL :IDisposable
    {
        private readonly IUnitOfWork _unitOfWork;
        private bool check;

        public PaymentPlanBAL(IUnitOfWork objOfWork )
        {
            this._unitOfWork = objOfWork;
        }

        public PaymentPlanBAL() : this(new UnitOfWork())
        {

        }

        public DataTable getAllPlan()
        {
            return this._unitOfWork.PaymentPlan.GetAllPlan();
        }

        public PaymentPlanDTO getPlanIdDetails(int planId)
        {
            PaymentPlan paymentPlan = new PaymentPlan();
            PaymentPlanDTO paymentPlanDTO = new PaymentPlanDTO();
            paymentPlan = this._unitOfWork.PaymentPlan.getPlanIdDetails(planId);
            paymentPlanDTO = AutoMapperToGetPlan(paymentPlan);
            return paymentPlanDTO;
        }

        public List<PaymentPlanDTO> GetPlanListForUser(int userId, string location)
        {
            List<PaymentPlan> paymentPlans = new List<PaymentPlan>();
            paymentPlans= this._unitOfWork.PaymentPlan.GetPlanListForUser(userId, location);
            List<PaymentPlanDTO> paymentPlanDTOs = new List<PaymentPlanDTO>();

            foreach (PaymentPlan paymentPlan in paymentPlans)
            {
                paymentPlanDTOs.Add(AutoMapperToGetPlan(paymentPlan));                
            }
            return paymentPlanDTOs;
        }

        public string InsertPlan(PaymentPlanDTO paymentPlanDTO)
        {
            //TranscationDetailsDTO transcation = new TranscationDetailsDTO();
            //TranscationDetailsBAL transcationBal = new TranscationDetailsBAL();
            var reply ="";
            PaymentPlan repDto = new PaymentPlan();

            //string date = (DateTime.Now).ToString;
            //string dateVal = DateTime.Parse(DateTime.Now, "yyyy-MM-dd").ToString; ;
            DateTime validFrom = ((DateTime)paymentPlanDTO.ValidFrom).Date;
            paymentPlanDTO.ValidFrom = validFrom;

            DateTime validTill = ((DateTime)paymentPlanDTO.ValidTill).Date;
            paymentPlanDTO.ValidTill = validTill.AddHours(23).AddMinutes(59).AddSeconds(59);

            paymentPlanDTO.CreatedDate = DateTime.Now;
            paymentPlanDTO.IsActive = true;
            paymentPlanDTO.IsVisible = true;
            paymentPlanDTO.IsPaid = false;
            paymentPlanDTO.UpdatedDate = DateTime.Now;
            DateTime d = DateTime.UtcNow;
            string todayDate = d.ToString("ddMMMyyyy");
            paymentPlanDTO.PlanName = paymentPlanDTO.PlanName +"/" + todayDate;

            repDto = AutoMapperToAddPlan(paymentPlanDTO);

            long planId = this._unitOfWork.PaymentPlan.InsertPlan(repDto);

            if (planId != 0)
            {
                List<object> savedPlanDetails = new List<object>();
                savedPlanDetails.Add(new {planSaved= true,planId = planId });
                reply = JsonConvert.SerializeObject(savedPlanDetails[0]);
                //transcation.PlanId = planId;
                //transcation.Amout = paymentPlanDTO.FinalAmount;
                //transcation.StripeBrand = paymentPlanDTO.StripeBrand;
                //transcation.StripeClientIp = paymentPlanDTO.StripeClientIp;
                //transcation.StripeCountry = paymentPlanDTO.StripeCountry;
                //transcation.StripeCreated = paymentPlanDTO.StripeCreated;
                //transcation.StripeEmail = paymentPlanDTO.StripeEmail;
                //transcation.StripeTokenId = paymentPlanDTO.StripeTokenId;
                //transcation.StripeName = paymentPlanDTO.StripeName;
                //transcation.TransactionDate = DateTime.UtcNow;
                //transcation.StripeType = paymentPlanDTO.StripeType;
                //transcation.StripeFunding = paymentPlanDTO.StripeFunding;
                //transcation.PaymentMode = paymentPlanDTO.PaymentMode;
                //transcation.StripecvcCheck = paymentPlanDTO.StripecvcCheck;
                //transcation.ReceiptId = paymentPlanDTO.ReceiptId;

                //transcationBal.InsertTransaction(transcation);
            }
            else
            {
                List<object> savedPlanDetails = new List<object>();
                savedPlanDetails.Add(new { planSaved = false, planId = planId });
                reply = JsonConvert.SerializeObject(savedPlanDetails[0]);
            }

            this._unitOfWork.Commit();
            if (planId != 0)
            {
                // Mail Functionality
                EmailSenderDetailsDTO edMail = new EmailSenderDetailsDTO();
                ConfigurationDTO configDetail = new ConfigurationDTO();
                ConfigurationBAL configBal = new ConfigurationBAL();
                configDetail = configBal.GetExistingConfigDetails();
                edMail.mailfor = "Payment Details";
                edMail.subject = "Arabchaldo - Plan Created";
                edMail.port = configDetail.Port;
                edMail.smtp = configDetail.Smtp;
                edMail.domain = configDetail.Domain;
                edMail.password = configDetail.Password;
               edMail.disclaimerTemplate = configDetail.DisclaimerTemplate;
                edMail.fromMailId = configDetail.FromMailId;
                UserDetailsBal userBAL = new UserDetailsBal();
                var Userfethed = userBAL.GeUserbyId(Convert.ToInt32(paymentPlanDTO.UserId));
                edMail.email = Userfethed.Email;
                edMail.name = Userfethed.FirstName + " " + Userfethed.LastName;
                //edMail.email = paymentPlanDTO.StripeEmail;
                EmailSender sendEmail = new EmailSender();
                sendEmail.SendEmail(edMail);
                sendEmail.SendAcknowledgement(edMail);
            }
            //List<object> savedPlanDetails = new List<object>();
            //savedPlanDetails.Add(new { });

            return reply;
        }

        public void UpdatePlanDetails(long planId, PaymentPlanDTO paymentPlan)
        {
            PaymentPlan plan = new PaymentPlan();
            plan = AutoMapperToAddPlan(paymentPlan);
            this._unitOfWork.PaymentPlan.UpdatePlanDetails(planId, plan);
            this._unitOfWork.Commit();
        }

        public void updatePlanForBanner(long planId, long bannerImageCount)
        {
            PaymentPlan plan = new PaymentPlan();
            bannerImageCount = bannerImageCount - 1;
            this._unitOfWork.PaymentPlan.UpdatePlanDetailsForBanner(planId, bannerImageCount);
            this._unitOfWork.Commit();
        }

        public void updatePlanForEvent(long planId, long eventImageCount)
        {
            PaymentPlan plan = new PaymentPlan();
            eventImageCount = eventImageCount - 1;
            this._unitOfWork.PaymentPlan.UpdatePlanDetailsForEvent(planId, eventImageCount);
            this._unitOfWork.Commit();
        }

        public void updatePlanForAds(long planId, long logoImageCount,long adsImageCount, long jobsCount)
        {
            PaymentPlan plan = new PaymentPlan();
            this._unitOfWork.PaymentPlan.UpdatePlanDetailsForAds(planId, logoImageCount, adsImageCount, jobsCount);
            this._unitOfWork.Commit();
        }

        public void updatePlanForbuySell(long planId, long buysellCount)
        {
            PaymentPlan plan = new PaymentPlan();
            this._unitOfWork.PaymentPlan.UpdatePlanDetailsForBuySell(planId,buysellCount);
            this._unitOfWork.Commit();
        }

        public void updatePlanSmallAds(long planId, long logoImageCount, long smalladsImageCount)
        {
            PaymentPlan plan = new PaymentPlan();
            this._unitOfWork.PaymentPlan.UpdatePlanDetailsSmallAds(planId, logoImageCount, smalladsImageCount);
            this._unitOfWork.Commit();
        }

        public void updatePlan(long planId, long logoImageCount, long adsImageCount, long jobsCount)
        {
            PaymentPlan plan = new PaymentPlan();
            this._unitOfWork.PaymentPlan.UpdatePlanDetailsForAds(planId, logoImageCount, adsImageCount, jobsCount);
            this._unitOfWork.Commit();
        }


        public string StripePaymentConfirmation(string tokenResponse)
        {
            // Set your secret key: remember to change this to your live secret key in production
            // See your keys here: https://dashboard.stripe.com/account/apikeys
            //StripeConfiguration.ApiKey = "sk_test_Hbbv2mbdn7d03C7bneDUhz0k00YilaceKB";
            StripeConfiguration.ApiKey = "sk_live_HAPCd4ZgnnD6ectKqGnLrnQq00joiiDl7b";
            

            Dictionary<string, string> tokenkeyPair = JsonConvert.DeserializeObject<Dictionary<string, string>>(tokenResponse);

            string token = tokenkeyPair["token"];
            long amount = Convert.ToInt64(tokenkeyPair["amount"]);
           
            string email = tokenkeyPair["customerEmail"];
            string name = tokenkeyPair["customerName"];
            string stripePlanId = tokenkeyPair["planId"];
            long planId = int.Parse(stripePlanId);

            string description = "Customer Name = " + tokenkeyPair["customerName"] + " ,CustomerEmail= " + tokenkeyPair["customerEmail"];

            var options = new ChargeCreateOptions
            {
                Amount = amount,
                Currency = "usd",
                Description = description,
                Source = token,
                ReceiptEmail= email,
            };
            
            try
            {
                var service = new ChargeService();
                Charge charge = service.Create(options);

                if (charge.Id != null)
                {
                    bool planUpdated = this.UpdatepaymentForPlanId(planId);

                    TranscationDetailsDTO transcation = new TranscationDetailsDTO();
                    TranscationDetailsBAL transcationBal = new TranscationDetailsBAL();
                    transcation.PlanId = planId;
                    transcation.PaymentMode = "Online";
                    transcation.StripeTokenId = token;
                    transcation.Amount = amount / 100;
                    transcation.TransactionDate = DateTime.UtcNow;
                    transcation.Name = tokenkeyPair["customerName"];
                    transcation.Email = tokenkeyPair["customerEmail"];
                    transcation.Address1 = tokenkeyPair["address1"];
                    transcation.Address2 = tokenkeyPair["address2"];
                    transcation.Phone = tokenkeyPair["phone"];
                    transcation.State = tokenkeyPair["state"];
                    transcation.Zip = tokenkeyPair["zip"];
                    transcation.StripeChargeId = charge.Id;
                    transcationBal.InsertTransaction(transcation);
                }

                List<object> successPaymentResponse = new List<object>();
                successPaymentResponse.Add(new { paymentStatus= "true",paymentchargeid = charge.Id, paymenttokenid = token, paymentamount = amount });
                var reply = JsonConvert.SerializeObject(successPaymentResponse[0]);

                if (charge.Id != null)
                {
                    // Mail Functionality
                    EmailSenderDetailsDTO edMail = new EmailSenderDetailsDTO();
                    ConfigurationDTO configDetail = new ConfigurationDTO();
                    ConfigurationBAL configBal = new ConfigurationBAL();
                    configDetail = configBal.GetExistingConfigDetails();
                    edMail.mailfor = "Online Payment Details";
                    edMail.subject = "Arabchaldo - Online Payment successfully submitted.";
                    edMail.port = configDetail.Port;
                    edMail.smtp = configDetail.Smtp;
                    edMail.domain = configDetail.Domain;
                    edMail.password = configDetail.Password;
                    edMail.disclaimerTemplate = configDetail.DisclaimerTemplate;
                    edMail.fromMailId = configDetail.FromMailId;
                    
                    edMail.email = tokenkeyPair["customerEmail"];
                    edMail.name = tokenkeyPair["customerName"];
                    edMail.address1 = tokenkeyPair["address1"];
                    edMail.address2= tokenkeyPair["address2"];

                    edMail.phone = tokenkeyPair["phone"];
                    edMail.stateName = tokenkeyPair["state"];
                    edMail.zipCode = tokenkeyPair["zip"];


                    EmailSender sendEmail = new EmailSender();
                    sendEmail.SendEmail(edMail);
                    sendEmail.SendAcknowledgement(edMail);
                }

                return reply;

            }
            catch(Exception ex)
            {
                List<object> failedPaymentResponse = new List<object>();
                failedPaymentResponse.Add(new { paymentStatus= "false", errorMessage= ex.Message });
                var errorMessage = JsonConvert.SerializeObject(failedPaymentResponse[0]);
                return errorMessage;
            }
        }

        public bool OfflinePaymentConfirmation(string OfflinePayment)
        {
            Dictionary<string, string> offlinePaymentDetailsPair = JsonConvert.DeserializeObject<Dictionary<string, string>>(OfflinePayment);

            string name = offlinePaymentDetailsPair["name"];
            string email = offlinePaymentDetailsPair["email"];
            string offlinePlanId = offlinePaymentDetailsPair["planId"];
            string receiptId = offlinePaymentDetailsPair["receiptId"];
            string finalAmount = offlinePaymentDetailsPair["amount"];
            string phone = offlinePaymentDetailsPair["phone"];
            string address1 = offlinePaymentDetailsPair["address1"];
            string address2 = offlinePaymentDetailsPair["address2"];
            string state = offlinePaymentDetailsPair["state"];
            string ZIP = offlinePaymentDetailsPair["ZIP"];


            long planId = int.Parse(offlinePlanId);
            long amount = int.Parse(finalAmount);
            long phoneNo = Convert.ToInt64(phone);
            //long zip = int.Parse(ZIP);

            bool isUpdated = this.UpdatepaymentForPlanId(planId);

            TranscationDetailsDTO transcation = new TranscationDetailsDTO();
            TranscationDetailsBAL transcationBal = new TranscationDetailsBAL();

            transcation.PlanId = planId;
            transcation.Amount = amount;
            transcation.Email = email;
            transcation.Name = name;
            transcation.TransactionDate = DateTime.UtcNow;
            transcation.PaymentMode = "Offline";
            transcation.ReceiptId = receiptId;
            transcation.Phone = phone;
            transcation.Address1 = address1;
            transcation.Address2 = address2;
            transcation.State = state;
            transcation.Zip = ZIP;


            transcationBal.InsertTransaction(transcation);

            if (offlinePlanId != null)
            {

                // Mail Functionality
                EmailSenderDetailsDTO edMail = new EmailSenderDetailsDTO();
                ConfigurationDTO configDetail = new ConfigurationDTO();
                ConfigurationBAL configBal = new ConfigurationBAL();
                configDetail = configBal.GetExistingConfigDetails();
                edMail.mailfor = "Offline Payment Details";
                edMail.subject = "Arabchaldo - Offline Payment successfully submitted.";
                edMail.port = configDetail.Port;
                edMail.smtp = configDetail.Smtp;
                edMail.domain = configDetail.Domain;
                edMail.password = configDetail.Password;
                edMail.disclaimerTemplate = configDetail.DisclaimerTemplate;
                edMail.fromMailId = configDetail.FromMailId;

                
                edMail.email = offlinePaymentDetailsPair["email"];
                edMail.name = offlinePaymentDetailsPair["name"];
                edMail.address1 = offlinePaymentDetailsPair["address1"];
                edMail.address2 = offlinePaymentDetailsPair["address2"];
                edMail.phone = offlinePaymentDetailsPair["phone"];
                edMail.stateName = offlinePaymentDetailsPair["state"];
                edMail.zipCode = offlinePaymentDetailsPair["ZIP"];
                edMail.receiptNo = offlinePaymentDetailsPair["receiptId"];

                EmailSender sendEmail = new EmailSender();
                sendEmail.SendEmail(edMail);
                sendEmail.SendAcknowledgement(edMail);

            }


            return true;
        }

        public bool UpdatepaymentForPlanId(long planId)
        {
            this._unitOfWork.PaymentPlan.UpdatePlanAfterPayment(planId);
            return true;
        }

        public PaymentPlanDTO AutoMapperToGetPlan(PaymentPlan paymentPlan)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PaymentPlan, PaymentPlanDTO>();
            });
            IMapper iMapper = config.CreateMapper();

            var destination = iMapper.Map<PaymentPlan, PaymentPlanDTO>(paymentPlan);
            return destination;
        }

        public PaymentPlan AutoMapperToAddPlan(PaymentPlanDTO paymentPlan)
        {
            //long postedForWhom = Convert.ToInt32(paymentPlan.UserId);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PaymentPlanDTO, PaymentPlan>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<PaymentPlanDTO, PaymentPlan>(paymentPlan);
            return destination;
        }

        public void Dispose()
        {
            if (_unitOfWork != null)
            {
                this._unitOfWork.Dispose();
            }
        }
    }
}
