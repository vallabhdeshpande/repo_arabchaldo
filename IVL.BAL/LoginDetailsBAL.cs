﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;
using System.Data;

namespace IVL.ArabChaldo.BAL
{
    public class LoginDetailsBAL: IDisposable
    {
        public LoginDetailsBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public LoginDetailsBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

        

        public bool RegisterNewUser(LoginDetailsDTO registerNewUser)
        {


            LoginDetails registeringUser = AutoMapperToRegisterNewUser(registerNewUser);


            this.obj.LoginDetails.RegisterUser(registeringUser);
            this.obj.Commit();

            //if(checkDuplicateEmail(registeringUser.Email))
            //{
            //    return true;
            //    //return "Successfully registered";
            //}
            //return "Registeration failed";
            return true;            
        }

        public bool checkDuplicateEmail(string emailId)
        {
            return obj.LoginDetails.CheckUserRegistered(emailId);
        }


        public bool ActivateUserAccount(string info)
        {
           
            bool isActivated = obj.LoginDetails.ActivateUserAccount(info);
            obj.Commit();
            return isActivated;
        }

        public DataTable GetUserInfoByEmail(string email)
        {
            return obj.LoginDetails.GetUserInfoByEmail(email);
        }

        public LoginDetailsDTO GetLogin(string UserName, string Password)
        {
            // return obj.LoginDetails.GetLogin(UserName, Password);

            var loginfetched = obj.LoginDetails.GetLogin(UserName, Password);
            LoginDetailsDTO getLogin = AutoMapperToGetLogin(loginfetched);

            return getLogin;
        }

        public string GetLoginInfo(string userName, string password)
        {
            return obj.LoginDetails.GetLoginInfo(userName, password);
        }

        public LoginDetailsDTO AutoMapperToGetLogin(LoginDetails LoginDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<LoginDetails, LoginDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<LoginDetails, LoginDetailsDTO>(LoginDetails);
            return destination;
        }
        public LoginDetails AutoMapperToUpdatePassword(long userId, LoginDetailsDTO LoginDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<LoginDetailsDTO, LoginDetails>();
            });
            IMapper iMapper = config.CreateMapper();
            var Userfethed = obj.LoginDetails.GetLoginByUserID(userId);
            var destination = iMapper.Map<LoginDetailsDTO, LoginDetails>(LoginDetails);
            return destination;
        }

        public LoginDetails AutoMapperToRegisterNewUser( LoginDetailsDTO LoginDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<LoginDetailsDTO, LoginDetails>();
            });
            IMapper iMapper = config.CreateMapper();
            //var Userfethed = obj.LoginDetails.GetLoginByUserID(userId);
            var destination = iMapper.Map<LoginDetailsDTO, LoginDetails>(LoginDetails);
            return destination;
        }

        public LoginDetailsDTO GetLoginByUserID(long id)
        {
            var Userfethed = obj.LoginDetails.GetLoginByUserID(id);
        
            LoginDetailsDTO getUser = AutoMapperToGetLogin(Userfethed);

            return getUser;
        }
    public bool ResetPassword(long userId, LoginDetailsDTO updatedDetails)
        {
            
            bool passwordresetexist = checkDuplicateEmail(updatedDetails.Password);
            bool isUpdated = false;
            if (!passwordresetexist)
            {
           
             
               
                LoginDetails updateUser = AutoMapperToUpdatePassword(userId, updatedDetails);
              
                isUpdated = obj.LoginDetails.ResetPassword(userId, updateUser);
                obj.Commit();
            }
            if (isUpdated == true)
            {
                ConfigurationDTO configDetail = new ConfigurationDTO();
                ConfigurationBAL configBal = new ConfigurationBAL();
                configDetail = configBal.GetExistingConfigDetails();
                var Userfethed = obj.userDetails.GetUserbyId(userId);
                string subject = "";
                
                {
                    subject = "Arabchaldo - Reset Password: #" + Userfethed.UserId.ToString(); //+ "- " + AdDetails.Title; //"Arab Chaldo - Job Updated"
                    configBal.SetMailDetails("resetpassword", Userfethed.Email, Userfethed.FirstName +" "+ Userfethed.LastName, subject, configDetail.DisclaimerTemplate, "", "");
                }


            }

            return isUpdated;
        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }
    }
}
