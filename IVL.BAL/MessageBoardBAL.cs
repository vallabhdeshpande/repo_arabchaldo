﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;
using System.Data;

namespace IVL.ArabChaldo.BAL
{
    public class MessageBoardBAL : IDisposable
    {
        public MessageBoardBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public MessageBoardBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

        public bool CheckMessageBoardExits(string Name,long CategoryId)
        {
            return obj.MessageBoard.CheckMessageBoardExits(Name, CategoryId);
        }

        public bool DeleteMessageBoard(long MessageBoardID)
        {
            bool isDeleted = obj.MessageBoard.DeleteMessageBoard(MessageBoardID);
            obj.Commit();
            return isDeleted;
        }



        public bool DeleteMessageBoardDetails(long MessageBoardID)
        {
            bool isDeleted = obj.MessageBoard.DeleteMessageBoardDetails(MessageBoardID);
            obj.Commit();
            return isDeleted;
        }
        public bool UpdateMessageBoard(long MessageBoardID, MessageBoardDTO MessageBoard)
        {
            MessageBoard updateMessageBoard = AutoMapperToUpdateUser(MessageBoardID, MessageBoard);
            bool isUpdated = obj.MessageBoard.UpdateMessageBoard(MessageBoardID, updateMessageBoard);
            obj.Commit();
            return isUpdated;
        }

       

        public bool InsertMessageBoard(MessageBoardDTO MessageBoard)
        {
            bool MessageBoardexist = CheckMessageBoardExits(MessageBoard.Topic,MessageBoard.CategoryId);
            bool check = false;
            if (!MessageBoardexist)
            {
                MessageBoard insertMessageBoard = AutoMapperToInsertMessageBoard(MessageBoard);
                check = obj.MessageBoard.InsertMessageBoard(insertMessageBoard);
                obj.Commit();
            }
            return check;
        }


        public MessageBoardDTO GetMessageBoardById(long cityId)
        {
            var cityfetched = obj.MessageBoard.GetMessageBoardById(cityId);
            MessageBoardDTO getMessageBoard = AutoMapperToGetMessageBoard(cityfetched);

            return getMessageBoard;
        }

        public DataTable GetAllMessageBoard()
        {

            //List<MessageBoard> MessageBoardList = new List<MessageBoard>();
            //MessageBoardList = obj.MessageBoard.GetAllMessageBoard();

            //List<MessageBoardDTO> MessageBoardListBoxig = new List<MessageBoardDTO>();

            //foreach (var listMessageBoard in MessageBoardList)
            //{
            //    MessageBoardListBoxig.Add(AutoMapperToGetMessageBoard(listMessageBoard));
            //}

            //return MessageBoardListBoxig;
            return obj.MessageBoard.GetAllMessageBoard();
        }

        
        public MessageBoardDTO AutoMapperToGetMessageBoard(IVL.ArabChaldo.DAL.Models.MessageBoard MessageBoard)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MessageBoard, MessageBoardDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<MessageBoard, MessageBoardDTO>(MessageBoard);
            return destination;
        }

        public MessageBoard AutoMapperToInsertMessageBoard(MessageBoardDTO MessageBoard)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MessageBoardDTO, MessageBoard>();
            });
            IMapper iMapper = config.CreateMapper();
            var destination = iMapper.Map<MessageBoardDTO, MessageBoard>(MessageBoard);
            return destination;
        }

        public MessageBoard AutoMapperToUpdateUser(long MessageBoardID, MessageBoardDTO MessageBoard)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MessageBoardDTO, MessageBoard>();
            });
            IMapper iMapper = config.CreateMapper();
            var MessageBoardfethed = obj.MessageBoard.GetMessageBoardById(MessageBoardID);
            var destination = iMapper.Map<MessageBoardDTO, MessageBoard>(MessageBoard);
            return destination;
        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }


        public DataTable FillMessageBoardList()
        {
            DataTable dtMessageBoard = new DataTable();
            dtMessageBoard = obj.MessageBoard.FillMessageBoardList();
            return dtMessageBoard;


        }

        //MessageBoardDetails
        public List<MessageBoardDetailsDTO> GetMessageBoardReplyByMessageBoardId(long MessageBoardId)
        {
            List<MessageBoardDetails> MessageBoardDetailsList = new List<MessageBoardDetails>();
            MessageBoardDetailsList = obj.MessageBoard.GetMessageBoardReplyByMessageBoardId(MessageBoardId);

            List<MessageBoardDetailsDTO> MessageBoardDetailsListBoxig = new List<MessageBoardDetailsDTO>();

            foreach (var listMessageBoardDetails in MessageBoardDetailsList)
            {
                MessageBoardDetailsListBoxig.Add(AutoMapperToGetMessageBoardDetails(listMessageBoardDetails));
            }

            return MessageBoardDetailsListBoxig;
        }

        public MessageBoardDetailsDTO AutoMapperToGetMessageBoardDetails(IVL.ArabChaldo.DAL.Models.MessageBoardDetails MessageBoardDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MessageBoardDetails, MessageBoardDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<MessageBoardDetails, MessageBoardDetailsDTO>(MessageBoardDetails);
            return destination;
        }

        public MessageBoardDetails AutoMapperToInsertMessageBoardDetails(MessageBoardDetailsDTO MessageBoardDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MessageBoardDetailsDTO, MessageBoardDetails>();
            });
            IMapper iMapper = config.CreateMapper();
            var destination = iMapper.Map<MessageBoardDetailsDTO, MessageBoardDetails>(MessageBoardDetails);
            return destination;
        }

        public bool InsertMessageBoardDetails(MessageBoardDetailsDTO MessageBoardDetails)
        {
           
            bool check = false;
         
                MessageBoardDetails insertMessageBoardDetails = AutoMapperToInsertMessageBoardDetails(MessageBoardDetails);
                check = obj.MessageBoard.InsertMessageBoardDetails(insertMessageBoardDetails);
                obj.Commit();
          
            return check;
        }
        public DataTable FillMessageBoardReplyList(long MessageBoardId)
        {
            DataTable dtMessageBoard = new DataTable();
            dtMessageBoard = obj.MessageBoard.FillMessageBoardReplyList( MessageBoardId);
            return dtMessageBoard;


        }
        //public bool DeleteMessageBoardDetails(long MessageBoardDetailsID)
        //{
        //    bool isDeleted = obj.MessageBoard.DeleteMessageBoardDetails(MessageBoardDetailsID);
        //    obj.Commit();
        //    return isDeleted;
        //}

    }
}