﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;
using System.Data;

namespace IVL.ArabChaldo.BAL
{
    public class CommonBAL : IDisposable
    {

        public CommonBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public CommonBAL() : this(new UnitOfWork())
        {
            //Country
                       
        }

        private IUnitOfWork obj;

        // Get Country Data
        public DataTable FillCountryDDL()
        {
            return obj.CommonDetails.FillCountryDDL(); 
            
        }
        // Get State Data
        public DataTable FillStateDDLByCountryId(long countryId )
        {
            return obj.CommonDetails.FillStateDDLByCountryId(countryId);
        }

        // Get City Data

        public DataTable FillCityDDLByStateId(long stateId)
        {
            return obj.CommonDetails.FillCityDDLByStateId(stateId);
        }

        // Get Category Data
        public DataTable FillCategoryDDL()
        {
            return obj.CommonDetails.FillCategoryDDL();

        }

        public DataTable FillBUCityDDLByStateId(long stateId)
        {
            return obj.CommonDetails.FillBUCityDDLByStateId(stateId);
        }

        // Get Subcategory Data
        public DataTable FillSubCategoryDDLByCategoryId(long categoryId)
        {
            return obj.CommonDetails.FillSubCategoryDDLByCategoryId(categoryId);

        }

        // Get User Data
        public DataTable FillUserDDL()
        {
            return obj.CommonDetails.FillUserDDL();

        }
        // Get Roles Data
        public DataTable FillRoleDDL(string roleName)
        {
            return obj.CommonDetails.FillRoleDDL(roleName);

        }

        // Get User Roles Data
        public DataTable FillUserByRoleDDL(long RoleId)
        {
            return obj.CommonDetails.FillUserByRoleDDL(RoleId);

        }
        public DataTable FillPopularCategory()
        {
            return obj.CommonDetails.FillPopularCategory();

        }
        public DataTable FillPopularEventsCategory()
        {
            return obj.CommonDetails.FillPopularEventsCategory();

        }

        public DataTable FillPopularJobSubCategory()
        {
            return obj.CommonDetails.FillPopularJobSubCategory();

        }

        // Get Workflow Details
        public DataTable DisplayWorkFlow(long Id, long PostTypeId)
        {
            return obj.CommonDetails.DisplayWorkFlow(Id,PostTypeId);

        }

        // Get Category Data
        public DataTable JobCategoryDDL()
        {
            return obj.CommonDetails.JobCategoryDDL();

        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }
    }
}
