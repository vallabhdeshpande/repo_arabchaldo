﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Repository;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.DAL.Interfaces;
using System.Data;

namespace IVL.ArabChaldo.BAL
{
    public class EventDetailsBAL : IDisposable
    {
        public EventDetailsBAL(IUnitOfWork objOfWork)
        {
            this._unitOfWork = objOfWork;
        }

        public EventDetailsBAL() : this(new UnitOfWork())
        {

        }

        private readonly IUnitOfWork _unitOfWork;


        public bool AddEvent(EventDetailsDTO eventDetails)
        {
            PaymentPlanBAL paymentPlanBAL = new PaymentPlanBAL();
            eventDetails.CreatedDate = DateTime.Now;
            eventDetails.UpdatedDate = DateTime.Now;
            eventDetails.IsActive = true;
            long postedForWhom = eventDetails.PostedFor;

            ManageCategoryBAL manageCategoryBAL = new ManageCategoryBAL();
            CategoryDTO category =  manageCategoryBAL.GetCategorybyId((int)eventDetails.CategoryId);
            eventDetails.CategoryName = category.CategoryName;
            UserDetailsBal userDetailsBal = new UserDetailsBal();
            //CityBAL eventCity = new CityBAL();
            //CityDTO eventCityName = eventCity.GetCityById((int)eventDetails.CityId);
            //if(eventCityName!=null)
            //eventDetails.CityName = eventCityName.CityName;
            if (eventDetails.CityId != 9999)
            {
                CityBAL cityBAL = new CityBAL();
                CityDTO city = cityBAL.GetCityById((int)eventDetails.CityId);
                if (city != null)
                    eventDetails.CityName = city.CityName;
            }
            else
            {
                eventDetails.CityName = "Other";
            }
            StateBAL stateBAL = new StateBAL();
            StateDTO stateDto = stateBAL.GetStateById((int)eventDetails.StateId);
            if(stateDto!=null)
            eventDetails.StateName = stateDto.StateName;
            eventDetails.PostedForName = userDetailsBal.GetThirdPartyName(postedForWhom);
            
            EventDetails addEvent = AutoMapperToAddEvent(eventDetails);
            bool check = _unitOfWork.EventDetails.Insert(addEvent);
            _unitOfWork.Commit();

            if (check == true)
            {
                // Mail Functionality
                ConfigurationBAL configBal = new ConfigurationBAL();
                ConfigurationDTO configDetail = new ConfigurationDTO();
             

                configDetail = configBal.GetExistingConfigDetails();
                string subject = "";
                string title = "";
                subject = "Arabchaldo - Event Submitted: #" + addEvent.EventId.ToString(); //+ "- " + adDetailsfetched.Title; //"Arab Chaldo - Event Submitted"
                title = addEvent.Title;
                configBal.SetMailDetails("Eventdetails", addEvent.Email, addEvent.ContactPersonName, subject, configDetail.DisclaimerTemplate,"",title);
            }
            if (check && (eventDetails.PlanId != null))
            {
                paymentPlanBAL.updatePlanForEvent((long)eventDetails.PlanId, (long)eventDetails.EventImageCount);
            }
            return check;
        }

        public bool UpdateEvent(long id, EventDetailsDTO eventDetails)
        {


   
            PaymentPlanBAL paymentPlanBAL = new PaymentPlanBAL();
            //CityBAL cityBAL = new CityBAL();
            //CityDTO city = cityBAL.GetCityById((int)eventDetails.CityId);
            //if(city!=null)
            //eventDetails.CityName = city.CityName;

            if (eventDetails.CityId != 9999)
            {
                CityBAL cityBAL = new CityBAL();
                CityDTO city = cityBAL.GetCityById((int)eventDetails.CityId);
                if (city != null)
                    eventDetails.CityName = city.CityName;
            }
            else
            {
                eventDetails.CityName = "Other";
            }

            StateBAL stateBAL = new StateBAL();
            StateDTO stateDto = stateBAL.GetStateById((int)eventDetails.StateId);
            if (stateDto != null)
                eventDetails.StateName = stateDto.StateName;

            eventDetails.IsActive = true;
            long postedForWhom = eventDetails.PostedFor;

            UserDetailsBal userDetailsBal = new UserDetailsBal();

            ManageCategoryBAL manageCategoryBAL = new ManageCategoryBAL();
            CategoryDTO category = manageCategoryBAL.GetCategorybyId((int)eventDetails.CategoryId);
            eventDetails.CategoryName = category.CategoryName;

            //eventDetails.PostedForName = userDetailsBal.GetThirdPartyName(postedForWhom);

            //EventDetailsDTO eventDetailsDTO = GetEventById(id);
            if (eventDetails != null)
            {
                eventDetails.UpdatedDate = DateTime.Now;
                eventDetails.IsVisible = null;
                EventDetails updateEvent = AutoMapperToAddEvent(eventDetails);
                bool check = _unitOfWork.EventDetails.Update(id, updateEvent);
                _unitOfWork.Commit();

                if (check == true)
                {
                    // Mail Functionality
                    ConfigurationBAL configBal = new ConfigurationBAL();
                    ConfigurationDTO configDetail = new ConfigurationDTO();
                    configDetail = configBal.GetExistingConfigDetails();
                    
                    UserDetailsBal userBAL = new UserDetailsBal();  
                    var Userfethed = userBAL.GeUserbyId(Convert.ToInt32(eventDetails.CreatedBy));
                    string subject = "";
                    string title = "";
                    subject = "Arabchaldo - Event Updated: #" + updateEvent.EventId.ToString(); //+ "- " + adDetailsfetched.Title; // "Arab Chaldo - Event Updated"
                    title = updateEvent.Title;
                    configBal.SetMailDetails("Eventupdatedetails", updateEvent.Email + "," + Userfethed.Email, updateEvent.ContactPersonName,subject, configDetail.DisclaimerTemplate,"",title);
                }
                if (check && (eventDetails.PlanId != null))
                {
                    paymentPlanBAL.updatePlanForEvent((long)eventDetails.PlanId, (long)eventDetails.EventImageCount);
                }
                return check;
            }
            return false;
        }

        public DataTable GetAllEvent(long loggedInUserId, string loggedInRole)
        {
            DataTable dtEvent = new DataTable();
            dtEvent = _unitOfWork.EventDetails.GetAllEvent(loggedInUserId, loggedInRole);
            return dtEvent;

            //List<EventDetails> eventList = new List<EventDetails>();
            //eventList = _unitOfWork.EventDetails.GetAllEvent(userId);

            //List<EventDetailsDTO> eventDtoList = new List<EventDetailsDTO>();

            //foreach (var item in eventList)
            //{
            //    eventDtoList.Add(AutoMapperToGetEvent(item));
            //}
            //return eventDtoList;
        }

        public EventDetailsDTO GetEventById(long id)
        {
            EventDetails getEventDetails = _unitOfWork.EventDetails.GetEventById(id);
            EventDetailsDTO convertToEventDTO = AutoMapperToGetEvent(getEventDetails);
            _unitOfWork.Commit();
            return convertToEventDTO;
        }

        public bool EventStatus(long EventDetailsID, string remarks, string Action, long CreatedBy)
        {
            var EventDetailsfetched = _unitOfWork.EventDetails.GetEventById(EventDetailsID);
            bool isStatus = _unitOfWork.EventDetails.EventStatus(EventDetailsID, remarks, Action, CreatedBy);
            _unitOfWork.Commit();


            if (isStatus)
            {

                // Mail Functionality
               
                ConfigurationBAL configBal = new ConfigurationBAL();
                ConfigurationDTO configDetail = new ConfigurationDTO();
                configDetail = configBal.GetExistingConfigDetails();
                string subject = "";
                string title = "";
                if (Action == "Approve")
                {
                    subject = "Arabchaldo - Event Approved: #" + EventDetailsfetched.EventId.ToString(); //+ "- " + adDetailsfetched.Title; // "Arab Chaldo - Event Approved"
                    title = EventDetailsfetched.Title;
                    configBal.SetMailDetails("ApprovedEventdetails", EventDetailsfetched.Email, EventDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, "",title);
                }
                else if (Action == "Reject")
                                       
                {
                    subject = "Arabchaldo - Event Rejected: #" + EventDetailsfetched.EventId.ToString(); //+ "- " + adDetailsfetched.Title; // "Arab Chaldo - Event Rejected"
                    title = EventDetailsfetched.Title;
                    string RejectRemark = remarks.Split(',')[2].ToString();
                    configBal.SetMailDetails("RejectedEventdetails", EventDetailsfetched.Email, EventDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, RejectRemark,title);

                }

                else if (Action == "Delete")

                {
                    subject = "Arabchaldo - Event Deleted: #" + EventDetailsfetched.EventId.ToString(); //+ "- " + adDetailsfetched.Title; // "Arab Chaldo - Event Rejected"
                    title = EventDetailsfetched.Title;
                    configBal.SetMailDetails("DeleteEventdetails", EventDetailsfetched.Email, EventDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, "", title);

                }

            }

            return isStatus;
        }

        public bool DeleteEvent(long id)
        {
            bool isDeleted = _unitOfWork.EventDetails.Delete(id);
            _unitOfWork.Commit();
            return isDeleted;
        }

        public EventDetailsDTO AutoMapperToGetEvent(EventDetails eventDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<EventDetails, EventDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<EventDetails, EventDetailsDTO>(eventDetails);
            return destination;
        }

        public EventDetails AutoMapperToAddEvent(EventDetailsDTO eventDetails)
        {
            long postedForWhom = Convert.ToInt32(eventDetails.PostedFor);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<EventDetailsDTO, EventDetails>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<EventDetailsDTO, EventDetails>(eventDetails);
            return destination;
        }
        public List<EventDetailsDTO> GetEventsByCategoryId(long cateoryId)
        {
            //return _unitOfWork.EventDetails.GetEventsByCategoryId(cateoryId);
            List<EventDetails> eventListbycategory = new List<EventDetails>();
            eventListbycategory = _unitOfWork.EventDetails.GetEventsByCategoryId(cateoryId);
            List<EventDetailsDTO> eventDtoList = new List<EventDetailsDTO>();

            foreach (var item in eventListbycategory)
            {
                eventDtoList.Add(AutoMapperToGetEvent(item));
            }
            return eventDtoList;

        }
        public void Dispose()
        {
            if (_unitOfWork != null)
            {
                this._unitOfWork.Dispose();
            }
        }

    }
}
