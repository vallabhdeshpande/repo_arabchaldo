﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;
using Service.contract;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.Data;

namespace IVL.ArabChaldo.BAL
{
    public class UserDetailsBal : IDisposable
    {
        public UserDetailsBal(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public UserDetailsBal() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

      

        // private EmailSender emailSender;
        public bool CheckUserExits(string EmailID)
        {
            return obj.userDetails.CheckUserExits(EmailID);
        }

        public string GetThirdPartyName(long id)
        {
            string username = obj.userDetails.ThirdPartyUserName(id);
            return username;
        }

        public bool DeleteUser(long ID)
        {
            var Userfethed = obj.userDetails.GetUserbyId(ID);
            bool isDeleted = obj.userDetails.DeleteUser(ID);
            obj.Commit();
            if (isDeleted == true)
            {
                ConfigurationDTO configDetail = new ConfigurationDTO();
                ConfigurationBAL configBal = new ConfigurationBAL();
                configDetail = configBal.GetExistingConfigDetails();

                string subject = "";

                {
                    subject = "Arabchaldo - User Deleted: #" + Userfethed.UserId.ToString(); //+ "- " + AdDetails.Title; //"Arab Chaldo - Job Updated"
                    configBal.SetMailDetails("UserdeleteDetails", Userfethed.Email, Userfethed.FirstName + " " + Userfethed.LastName, subject, configDetail.DisclaimerTemplate, "", "");
                }

            }
            return isDeleted;
        }
        public bool UpdateUsers(long ID, UserDetailsDTO userdetails)
        {

            bool userDetailexist = CheckUserExits(userdetails.RoleName);
                bool isUpdated = false;
                if (!userDetailexist)
                {
                UserDetails updateUser = AutoMapperToUpdateUser(ID, userdetails);
                isUpdated = obj.userDetails.UpdateUsers(ID, updateUser);
                obj.Commit();
               
                }


            if (isUpdated == true)
            {
                ConfigurationDTO configDetail = new ConfigurationDTO();
                ConfigurationBAL configBal = new ConfigurationBAL();
                configDetail = configBal.GetExistingConfigDetails();

                string subject = "";
                
                {
                    subject = "Arabchaldo - Profile Updated: #" + userdetails.UserId.ToString(); //+ "- " + AdDetails.Title; //"Arab Chaldo - Job Updated"
                    configBal.SetMailDetails("UserupdateDetails", userdetails.Email, userdetails.FirstName+" "+userdetails.LastName, subject, configDetail.DisclaimerTemplate, "","");
                }


            }
            return isUpdated;
            
        }

        public string EncryptText(string password)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(password);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public bool InsertUser(UserDetailsDTO registerNewUser)
        {
            LoginDetailsBAL loginDetailsBAL = new LoginDetailsBAL();
            UserRoles userRoles = new UserRoles();
            // UserDetailsDTO userDetailsDTO = new UserDetailsDTO();
            LoginDetailsDTO loginDetailsDTO = new LoginDetailsDTO();
            //RolesBAL rolesBAL = new RolesBAL();

            bool userexist=CheckUserExits(registerNewUser.Email);
            
            bool isRegistered = false;
            if (!userexist)
            {
                Guid actCode = new Guid();
               // string activationCode = actCode.ToString();
              
               // registerNewUser.ActivationUrl = "http://localhost:4200" + "/#/activation?info=" + registerNewUser.Email + "," + activationCode;
                //registerNewUser.ActivationUrl = "http://10.0.40.3:7070" + "/#/activation?info=" + registerNewUser.Email + "," + activationCode;
               // registerNewUser.ActivationUrl = registerNewUser.ActivationUrl + "," + activationCode;
                registerNewUser.IsActive = true;
                UserDetails insertUser = AutoMapperToInsertUser(registerNewUser);
                isRegistered =obj.userDetails.InsertUser(insertUser);

                //userRoles.UserId = insertUser.UserId;
                //userRoles.RoleId = 8;
                //userRoles.IsActive = true;
                // userRoles.CreatedBy = insertUser.FirstName + " " + insertUser.LastName;
                // userRoles.CreatedDate = DateTime.Now;

                


                loginDetailsDTO.FirstName = insertUser.FirstName;
                loginDetailsDTO.LastName = insertUser.LastName;
                //loginDetailsDTO.ContactNumber = insertUser.ContactNumber;
                loginDetailsDTO.Email = insertUser.Email;
            
                loginDetailsDTO.UserId = insertUser.UserId;
                loginDetailsDTO.Gender = insertUser.Gender;
                loginDetailsDTO.Password = registerNewUser.Password;
                loginDetailsDTO.IsActive = true;
                loginDetailsDTO.CreatedDate = DateTime.Now;
               

                // need to change created by
                loginDetailsDTO.CreatedBy = insertUser.UserId;
                string activationCode = EncryptText(insertUser.UserId.ToString());
                loginDetailsDTO.ActivationCode = activationCode;





                if (isRegistered)
                {
                    //insertUser.UserId = userDetailsBal.GeUserbyEmail(registerNewUser.Email);
                    //registerNewUser.IsActive = true;
                    //registerNewUser.CreatedDate = DateTime.Now;
                    //status= loginDetailsBAL.RegisterNewUser(registerNewUser);
                    if (loginDetailsBAL.RegisterNewUser(loginDetailsDTO))
                    {
                        //userRoles.RoleId = 8;
                        ////userRoles.UserId =(long)registerNewUser.UserId;
                        //userRoles.IsActive = true;
                        //userRoles.CreatedDate = DateTime.Now;
                        //userRoles.CreatedBy = registerNewUser.UserId;

                    }
                    bool loggedin = loginDetailsBAL.RegisterNewUser(loginDetailsDTO);
                    obj.Commit();
                    if (loggedin)
                    {
                        EmailSenderDetailsDTO edMail = new EmailSenderDetailsDTO();
                        ConfigurationDTO configDetail = new ConfigurationDTO();
                        ConfigurationBAL configBal = new ConfigurationBAL();
                        configDetail = configBal.GetConfigDetailsbyId(21);
                        registerNewUser.ActivationUrl = registerNewUser.ActivationUrl + "," + activationCode;
                        edMail.confirmationEmailUrl = registerNewUser.ActivationUrl;
                        edMail.mailfor = "Registration";
                        edMail.port = configDetail.Port;
                        edMail.smtp = configDetail.Smtp;
                        edMail.password = configDetail.Password;
                        edMail.subject = "Arabchaldo";
                        edMail.fromMailId = configDetail.FromMailId;
                        edMail.disclaimerTemplate = configDetail.DisclaimerTemplate;
                        edMail.registrationMailTemplate = configDetail.RegistrationMailTemplate;
                        edMail.disclaimerTemplate = configDetail.DisclaimerTemplate;
                        edMail.email = loginDetailsDTO.Email;
                        edMail.name = loginDetailsDTO.FirstName + " " + loginDetailsDTO.LastName;
                        EmailSender sendEmail = new EmailSender();
                        sendEmail.SendEmail(edMail);
                        sendEmail.SendAcknowledgement(edMail);


                    }
                    
                }

              

                    List<Configuration> ConfigList = new List<Configuration>();
                    ConfigList = obj.ConfigDetails.GetAllConfigDetails();

                    //if (check)
                    //{
                    //    EmailSender emailSender = new EmailSender();
                    //    emailSender.SendEmail(userDetails.Email);
                    //    //emailSender.SendEmail(userDetails.Email);
                    //}
                    return true;
            }
            return false;
        }

        
        public UserDetailsDTO GeUserbyId(long id)
        {
            var Userfethed = obj.userDetails.GetUserbyId(id);
            UserDetailsDTO getUser = AutoMapperToGetUser(Userfethed);

            return getUser;
        }


        public long GeUserbyEmail(string email)
        {
            var Userfethed = obj.userDetails.GetUserbyEmail(email);
            UserDetailsDTO getUser = AutoMapperToGetUser(Userfethed);
            long registeredUserid = getUser.UserId;

            return registeredUserid;
        }



        //public List<UserDetailsDTO> GetAllUser()
        //{

        //    List<UserDetails> UserList = new List<UserDetails>();
        //    UserList = obj.userDetails.GetAllUser();

        //    List<UserDetailsDTO> UserListBoxig = new List<UserDetailsDTO>();

        //    foreach (var listUser in UserList)
        //    {
        //        UserListBoxig.Add(AutoMapperToGetUser(listUser));
        //    }

        //    return UserListBoxig;
        //}

        public DataTable GetAllUser()
        {
            return obj.userDetails.GetAllUser();
        }

        //public List<UserDetailsDTO> GetUsersForThirdParty(long id)
        //{            List<UserDetails> UserList = new List<UserDetails>();
        //    UserList = null; //obj.userDetails.GetAllUser();

        //    List<UserDetails> UserListForBanner = new List<UserDetails>();

        //    foreach (var items in UserList)
        //    {
        //        if(id ==3 & items.RoleName == "Business User")
        //        {
        //            UserListForBanner.Add(items);
        //        }
        //        else if(id == 4 & items.RoleName == "Third Party User")
        //        {
        //            UserListForBanner.Add(items);
        //        }
        //    }


        //    List<UserDetailsDTO> UserListBoxig = new List<UserDetailsDTO>();
        //    foreach (var listUser in UserListForBanner)
        //    {
        //        UserListBoxig.Add(AutoMapperToGetUser(listUser));
        //    }
        //    return UserListBoxig;
        //}

        public UserDetailsDTO AutoMapperToGetUser(IVL.ArabChaldo.DAL.Models.UserDetails userDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserDetails, UserDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<UserDetails, UserDetailsDTO>(userDetails);
            return destination;
        }

        public UserDetails AutoMapperToInsertUser(UserDetailsDTO userDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserDetailsDTO, UserDetails>();
            });
            IMapper iMapper = config.CreateMapper();
            var destination=iMapper.Map<UserDetailsDTO, UserDetails>(userDetails);           
            return destination;
        }

        public UserDetails AutoMapperToUpdateUser(long ID, UserDetailsDTO userDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserDetailsDTO, UserDetails>();
            });
            IMapper iMapper = config.CreateMapper();
            var Userfethed = obj.userDetails.GetUserbyId(ID);
            var destination = iMapper.Map<UserDetailsDTO, UserDetails>(userDetails);
            return destination;
        }
        public string SocialloginInfo(UserDetailsDTO userDetails)
        {
            bool getuser = obj.userDetails.CheckUserExits(userDetails.Email);
            UserDetails userfeachted = AutoMapperToInsertUser(userDetails);
            LoginDetails logindetails = new LoginDetails();
            
            if (getuser != true)
            {
                obj.userDetails.InsertUser(userfeachted);
                return obj.LoginDetails.GetLoginInfo(userDetails.Email, "Admin1234");
                //return logindetails.Password;
            }
            else
            {

                return obj.LoginDetails.GetLoginInfo(userDetails.Email, "Admin1234");
               // obj.LoginDetails.GetLoginInfo(userDetails.Email,"Admin1234");
            }
        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }     
     
    }
}

