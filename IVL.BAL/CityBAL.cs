﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;
using System.Data;

namespace IVL.ArabChaldo.BAL
{
    public class CityBAL : IDisposable
    {
        public CityBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public CityBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

        public bool CheckCityExits(string CityName,long StateId, long CityId)
        {
            return obj.City.CheckCityExits(CityName, StateId,CityId);
        }

        public bool DeleteCity(int CityID)
        {
            bool isDeleted = obj.City.DeleteCity(CityID);
            obj.Commit();
            return isDeleted;
        }
        public bool UpdateCity(int CityID, CityDTO City)
        {
            bool cityexist = CheckCityExits(City.CityName, City.StateId,City.CityId);
            bool isUpdated = false;
            if (!cityexist)
            {
                City updateCity = AutoMapperToUpdateCity(CityID, City);
                 isUpdated = obj.City.UpdateCity(CityID, updateCity);
                obj.Commit();
            }
            return isUpdated;
        }

        public List<CityDTO> GetCityWithPagination(string paginationFilter)
        {
            List<City> CityList = new List<City>();
            //CityList = obj.City.GetCityWithPagination(paginationFilter);

            List<CityDTO> CityListBoxig = new List<CityDTO>();

            foreach (var listCity in CityList)
            {
                CityListBoxig.Add(AutoMapperToGetCity(listCity));
            }

            return CityListBoxig;
        }

        public bool InsertCity(CityDTO City)
        {
            bool cityexist = CheckCityExits(City.CityName,City.StateId,City.CityId);
            bool check = false;
            if (!cityexist)
            {
                City insertCity = AutoMapperToInsertCity(City);
                check = obj.City.InsertCity(insertCity);
                obj.Commit();
            }
            return check;
        }


        public CityDTO GetCityById(int cityId)
        {
            var cityfetched = obj.City.GetCityById(cityId);
            CityDTO getCity = AutoMapperToGetCity(cityfetched);

            return getCity;
        }

        public List<CityDTO> GetAllCity()
        {

            List<City> CityList = new List<City>();
            CityList = obj.City.GetAllCity();

            List<CityDTO> CityListBoxig = new List<CityDTO>();

            foreach (var listCity in CityList)
            {
                CityListBoxig.Add(AutoMapperToGetCity(listCity));
            }

            return CityListBoxig;
        }

        public List<CityDTO> GetAllCitiesByStateId(int stateId)
        {
            List<City> CityList = new List<City>();
            CityList = obj.City.GetAllCitiesByStateId(stateId);

            List<CityDTO> CityListBoxig = new List<CityDTO>();

            foreach (var listCity in CityList)
            {
                CityListBoxig.Add(AutoMapperToGetCity(listCity));
            }

            return CityListBoxig;
        }

        public CityDTO AutoMapperToGetCity(IVL.ArabChaldo.DAL.Models.City City)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<City, CityDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<City, CityDTO>(City);
            return destination;
        }

        public City AutoMapperToInsertCity(CityDTO City)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CityDTO, City>();
            });
            IMapper iMapper = config.CreateMapper();
            var destination = iMapper.Map<CityDTO, City>(City);
            return destination;
        }

        public City AutoMapperToUpdateCity(int CityID, CityDTO City)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CityDTO, City>();
            });
            IMapper iMapper = config.CreateMapper();
            var Cityfethed = obj.City.GetCityById(CityID);
            var destination = iMapper.Map<CityDTO, City>(City);
            return destination;
        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }


        public DataTable FillCityList()
        {
            DataTable dtCity = new DataTable();
            dtCity = obj.City.FillCityList();
            return dtCity;


        }

    }
}