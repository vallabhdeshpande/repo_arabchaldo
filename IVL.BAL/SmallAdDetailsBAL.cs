﻿using IVL.ArabChaldo.DAL.Interfaces;
using IVL.ArabChaldo.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using AutoMapper;
using System.Data;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;

namespace IVL.ArabChaldo.BAL
{
    public class SmallAdDetailsBAL : IDisposable
    {
        public SmallAdDetailsBAL(IUnitOfWork objOfWork)
        {
            this.obj = objOfWork;
        }

        public SmallAdDetailsBAL() : this(new UnitOfWork())
        {

        }

        private IUnitOfWork obj;

        public bool CheckSmallAdDetailsExits(string Title, long SmallAdId)
        {
            return obj.SmallAdDetails.CheckSmallAdDetailsExits(Title, SmallAdId);
        }

        public bool SmallAdStatus(long SmallAdDetailsID, string remarks, string Action, long CreatedBy)
        {
            var SmallAdDetailsfetched = obj.SmallAdDetails.GetSmallAdDetailsById(SmallAdDetailsID);
            bool isStatus = obj.SmallAdDetails.SmallAdStatus(SmallAdDetailsID, remarks, Action, CreatedBy);
            obj.Commit();
            if (isStatus)
            {


                //var SmallAdDetailsfetched = obj.SmallAdDetails.GetSmallAdDetailsById(SmallAdDetailsID);
                //ConfigurationBAL configBal = new ConfigurationBAL();
                //if(Action=="Approve")
                //configBal.SetMailDetails("ApprovedSmallAddetails",SmallAdDetailsfetched.Email, SmallAdDetailsfetched.ContactPersonName, "SmallAd Approved Successfully");
                //else if (Action == "Reject")
                //configBal.SetMailDetails("RejectedSmallAddetails", SmallAdDetailsfetched.Email, SmallAdDetailsfetched.ContactPersonName, "SmallAd Rejected Successfully");



                // Mail Functionality
               
                ConfigurationBAL configBal = new ConfigurationBAL();
                ConfigurationDTO configDetail = new ConfigurationDTO();

                configDetail = configBal.GetExistingConfigDetails();
                string subject = "";
                string title = "";
                subject = "Arabchaldo - Advertisement Approved: #" + SmallAdDetailsfetched.SmallAdId.ToString(); //+ "- " + adDetailsfetched.Title; //"Arab Chaldo :  Advertisement Approved"
                title = SmallAdDetailsfetched.Title;
                if (Action == "Approve" )
                configBal.SetMailDetails("ApprovedSmallAddetails", SmallAdDetailsfetched.Email, SmallAdDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, "",title);

                if (Action == "Reject" )
                {
                    string RejectRemark = remarks.Split(',')[2].ToString();
                    subject = "Arabchaldo - Advertisement Rejected: #" + SmallAdDetailsfetched.SmallAdId.ToString(); //+ "- " + adDetailsfetched.Title; //"Arab Chaldo :  Advertisement Rejected"
                    title = SmallAdDetailsfetched.Title;
                    configBal.SetMailDetails("RejectedSmallAddetails", SmallAdDetailsfetched.Email, SmallAdDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, RejectRemark,title);
                }


                if (Action == "Delete")
                {
                   
                    subject = "Arabchaldo - Advertisement Deleted: #" + SmallAdDetailsfetched.SmallAdId.ToString(); //+ "- " + adDetailsfetched.Title; //"Arab Chaldo :  Advertisement Rejected"
                    title = SmallAdDetailsfetched.Title;
                    configBal.SetMailDetails("DeleteSmallAddetails", SmallAdDetailsfetched.Email, SmallAdDetailsfetched.ContactPersonName, subject, configDetail.DisclaimerTemplate, "", title);
                }
            }

            return isStatus;
        }



        public bool UpdateSmallAdDetails(long SmallAdDetailsID, SmallAdDetailsDTO SmallAdDetails)
        {
            PaymentPlanBAL paymentPlanBAL = new PaymentPlanBAL();
            bool SmallAdDetailsexist = CheckSmallAdDetailsExits(SmallAdDetails.Title, SmallAdDetails.SmallAdId);
            bool check = false;
            if (!SmallAdDetailsexist)
            {
                SmallAdDetails updateSmallAdDetails = AutoMapperToUpdateUser(SmallAdDetailsID, SmallAdDetails);
                check = obj.SmallAdDetails.UpdateSmallAdDetails(SmallAdDetailsID, updateSmallAdDetails);
                obj.Commit();
            }


            //if (check == true)
            //{
            //    // Mail Functionality
            //    ConfigurationBAL configBal = new ConfigurationBAL();               
            //    configBal.SetMailDetails("Listing Details", SmallAdDetails.Email, SmallAdDetails.ContactPersonName, "SmallAd Updated Successfully");


            if (check == true)
            {
                // Mail Functionality
                ConfigurationDTO configDetail = new ConfigurationDTO();
                ConfigurationBAL configBal = new ConfigurationBAL();
                configDetail = configBal.GetExistingConfigDetails();
                UserDetailsBal userBAL = new UserDetailsBal();
                var Userfethed = userBAL.GeUserbyId(Convert.ToInt32(SmallAdDetails.CreatedBy));
                string subject = "";
                string title = "";
                subject = "Arabchaldo - Advertisement Updated: #" + SmallAdDetails.SmallAdId.ToString(); //+ "- " + adDetailsfetched.Title; // "Arab Chaldo :  Advertisement Updated"
                title = SmallAdDetails.Title;
                configBal.SetMailDetails("UpdateSmallAdDetails", SmallAdDetails.Email + "," + Userfethed.Email, SmallAdDetails.ContactPersonName,subject, configDetail.DisclaimerTemplate, "",title);
            }


            if (check && (SmallAdDetails.PlanId != null))
            {
                if (SmallAdDetails.PlanId != 0)
                    paymentPlanBAL.updatePlanSmallAds((long)SmallAdDetails.PlanId, (long)SmallAdDetails.LogoCount, (long)SmallAdDetails.AdImageCount);
            }
            return check;
        }
        public bool InsertSmallAdDetails( SmallAdDetailsDTO SmallAdDetails)
        {
            PaymentPlanBAL paymentPlanBAL = new PaymentPlanBAL();
            bool SmallAdDetailsexist = CheckSmallAdDetailsExits(SmallAdDetails.Title, SmallAdDetails.SmallAdId);
            bool check = false;
            if (!SmallAdDetailsexist)
            {
                SmallAdDetails insertSmallAdDetails = AutoMapperToInsertSmallAdDetails(SmallAdDetails);
                check = obj.SmallAdDetails.InsertSmallAdDetails(insertSmallAdDetails);
                obj.Commit();

                if (check == true)
                {
                    // Mail Functionality
                    ConfigurationDTO configDetail = new ConfigurationDTO();
                    ConfigurationBAL configBal = new ConfigurationBAL();
                    configDetail = configBal.GetExistingConfigDetails();
                    UserDetailsBal userBAL = new UserDetailsBal();
                    var Userfethed = userBAL.GeUserbyId(Convert.ToInt32(SmallAdDetails.CreatedBy));
                    string subject = "";
                    string title = "";
                    subject = "Arabchaldo - Advertisement submitted: #" + insertSmallAdDetails.SmallAdId.ToString(); //+ "- " + adDetailsfetched.Title; // "Arab Chaldo - Advertisement Submitted"
                    title = SmallAdDetails.Title;
                    configBal.SetMailDetails("SmallAds Listing Details", SmallAdDetails.Email + "," + Userfethed.Email, SmallAdDetails.ContactPersonName, subject, configDetail.DisclaimerTemplate, "",title);
                }
            }

            if (check && (SmallAdDetails.PlanId != null))
            {
                if (SmallAdDetails.PlanId != 0)
                {
                    paymentPlanBAL.updatePlanSmallAds((long)SmallAdDetails.PlanId, (long)SmallAdDetails.LogoCount, (long)SmallAdDetails.AdImageCount);
                }
            }
            return check;
        }


        public SmallAdDetailsDTO GetSmallAdDetailsById(long SmallAdDetailsId)
        {
            var SmallAdDetailsfetched = obj.SmallAdDetails.GetSmallAdDetailsById(SmallAdDetailsId);
            SmallAdDetailsDTO getSmallAdDetails = AutoMapperToGetSmallAdDetails(SmallAdDetailsfetched);

            CommonBAL commonBal = new CommonBAL();
            //CountryName
            //DataTable countryTable = new DataTable();
            //countryTable = commonBal.FillCountryDDL();
            //DataRow foundRowCountry = countryTable.Select("CountryId=" + getSmallAdDetails.CountryId)[0];
            //if (foundRowCountry != null)
            getSmallAdDetails.CountryName = "USA";

            //StateName
            //DataTable stateTable = new DataTable();
            //stateTable = commonBal.FillStateDDLByCountryId(getSmallAdDetails.CountryId);
            //DataRow foundRowState = stateTable.Select("StateId=" + getSmallAdDetails.StateId)[0];
            //if (foundRowState != null)
            //    getSmallAdDetails.StateName = foundRowState["StateName"].ToString();
            if (SmallAdDetailsfetched.StateName == "null")
                getSmallAdDetails.StateName = "";

            //CityName
            //DataTable cityTable = new DataTable();
            //cityTable = commonBal.FillCityDDLByStateId(getSmallAdDetails.StateId);
            //DataRow foundRowCity = cityTable.Select("CityId=" + getSmallAdDetails.CityId)[0];
            //if (foundRowCity != null)
            //    getSmallAdDetails.CityName = foundRowCity["CityName"].ToString();
            if (SmallAdDetailsfetched.CityName == "null")
                getSmallAdDetails.CityName = "";

            return getSmallAdDetails;
        }

        public List<SmallAdDetailsDTO> GetAllSmallAdDetails(long loggedInUserId, string loggedInRole)
        {

            List<SmallAdDetails> SmallAdDetailsList = new List<SmallAdDetails>();
            SmallAdDetailsList = obj.SmallAdDetails.GetAllSmallAdDetails(loggedInUserId, loggedInRole);

            List<SmallAdDetailsDTO> SmallAdDetailsListBoxig = new List<SmallAdDetailsDTO>();

            foreach (var listSmallAdDetails in SmallAdDetailsList)
            {
                SmallAdDetailsListBoxig.Add(AutoMapperToGetSmallAdDetails(listSmallAdDetails));
            }

            return SmallAdDetailsListBoxig;
        }

        public DataTable GetAllSmallAdsDetails(long loggedInUserId, string loggedInRole)
        {
            return obj.SmallAdDetails.GetAllSmallAdsDetails(loggedInUserId, loggedInRole);

        }
       
        public SmallAdDetailsDTO AutoMapperToGetSmallAdDetails(IVL.ArabChaldo.DAL.Models.SmallAdDetails SmallAdDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SmallAdDetails, SmallAdDetailsDTO>();
            });
            IMapper iMapper = config.CreateMapper();


            var destination = iMapper.Map<SmallAdDetails, SmallAdDetailsDTO>(SmallAdDetails);
            return destination;
        }

        public SmallAdDetails AutoMapperToInsertSmallAdDetails(SmallAdDetailsDTO SmallAdDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SmallAdDetailsDTO, SmallAdDetails>();
            });
            IMapper iMapper = config.CreateMapper();
            var destination = iMapper.Map<SmallAdDetailsDTO, SmallAdDetails>(SmallAdDetails);
            return destination;
        }

        public SmallAdDetails AutoMapperToUpdateUser(long SmallAdDetailsID, SmallAdDetailsDTO SmallAdDetails)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SmallAdDetailsDTO, SmallAdDetails>();
            });
            IMapper iMapper = config.CreateMapper();
            var SmallAdDetailsfethed = obj.SmallAdDetails.GetSmallAdDetailsById(SmallAdDetailsID);
            var destination = iMapper.Map<SmallAdDetailsDTO, SmallAdDetails>(SmallAdDetails);
            return destination;
        }
        public void Dispose()
        {
            if (obj != null)
            {
                this.obj.Dispose();
            }
        }

    }
}