﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace IVL.ArabChaldo.WebApi.Models
{
    public partial class Offshore_ArabchaldoContext : DbContext
    {
        public Offshore_ArabchaldoContext()
        {
        }

        public Offshore_ArabchaldoContext(DbContextOptions<Offshore_ArabchaldoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AdDetails> AdDetails { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=HIJ-SQLSRV01;Database=Offshore_Arabchaldo;Persist Security Info=True;User ID=arabchaldo_user;Password=admin@123;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdDetails>(entity =>
            {
                entity.HasKey(e => e.AdId);

                entity.Property(e => e.AdLogoUrl).HasMaxLength(500);

                entity.Property(e => e.AddressStreet1)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AddressStreet2)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AlternateContactNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CityName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPersonName)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CountryCodeContact).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FaxNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ImageUrl)
                    .HasColumnName("ImageURL")
                    .HasMaxLength(900);

                entity.Property(e => e.PostedForName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ServicesOffered)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.StateName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubCategoryName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TagLine)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.ValidFromDate).HasColumnType("datetime");

                entity.Property(e => e.ValidTillDate).HasColumnType("datetime");

                entity.Property(e => e.Website).HasMaxLength(150);

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });
        }
    }
}
