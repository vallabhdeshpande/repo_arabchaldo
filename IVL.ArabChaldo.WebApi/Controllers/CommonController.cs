﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.DAL.Interfaces;
using Service.contract;
using Service.Implement;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.Implement.Implement;
using System.IO;
using System.Net.Http.Headers;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;
using System.Text;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        private readonly ICommonService _common;
        DataTableToJsonList dataList = new DataTableToJsonList();
        private CommonController(ICommonService context)
        {
            _common = context;
        }

        public CommonController() : this(new CommonService())
        {

        }

        #region FillDDLs
        [Route("RoleData")]
        [HttpGet]
        public string RoleData( string roleName)
        {          
            return dataList.DataTableToJSONWithStringBuilder(this._common.FillRoleDDL(roleName));
        }

        [Route("UserData")]
        [HttpGet]
        public string UserData()
        {
            return dataList.DataTableToJSONWithStringBuilder(this._common.FillUserDDL());
        }

        [Route("CountryData")]
        [HttpGet]
        public string CountryData()
        {
           
           
            return   dataList.DataTableToJSONWithStringBuilder(this._common.FillCountryDDL());
        }
        [Route("StateData")]
        [HttpGet]
        public string StateData(long CountryId)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._common.FillStateDDLByCountryId(CountryId));
        }

        

        [Route("CityData")]
        [HttpGet]
        public string CityData(long StateId)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._common.FillCityDDLByStateId(StateId));
        }

        [Route("BUCityData")]
        [HttpGet]
        public string BUCityData(long StateId)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._common.FillBUCityDDLByStateId(StateId));
        }

        [Route("CategoryData")]
        [HttpGet]
        public string CategoryData()
        {
            return dataList.DataTableToJSONWithStringBuilder(this._common.FillCategoryDDL());
        }

        [Route("SubCategoryData")]
        [HttpGet]
        public string SubCategoryData(long CategoryId)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._common.FillSubCategoryDDLByCategoryId(CategoryId));
        }

        [Route("UserDDLbyRole")]
        [HttpGet]
        public string UserDDLbyRole(long RoleId)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._common.FillUserByRoleDDL(RoleId));
        }

        [Route("PopularCategory")]
        [HttpGet]
        public string FillPopularCategory()
        {
            return dataList.DataTableToJSONWithStringBuilder(this._common.FillPopularCategory());
        }

        [Route("PopularEventsCategory")]
        [HttpGet]
        public string FillPopularEventsCategory()
        {
            return dataList.DataTableToJSONWithStringBuilder(this._common.FillPopularEventsCategory());
        }

        [Route("PopularJobSubCategory")]
        [HttpGet]
        public string FillPopularJobSubCategory()
        {
            return dataList.DataTableToJSONWithStringBuilder(this._common.FillPopularJobSubCategory());
        }


        [Route("JobCategoryData")]
        [HttpGet]
        public string JobCategoryData()
        {
            return dataList.DataTableToJSONWithStringBuilder(this._common.JobCategoryDDL());
        }

        #endregion

        #region Imageupload
        [HttpPost, DisableRequestSizeLimit]
        public IActionResult Upload()
        {
            try
            {
                long FileLen;

                var file = Request.Form.Files[0];
                //Stream strm = file.InputStream;
                var folderName = Path.Combine("StaticFiles", "Images");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);


                FileLen = file.Length;
                byte[] input = new byte[FileLen];

                // Initialize the stream.
                Stream MyStream = file.OpenReadStream();
                MyStream.Read(input, 0, Convert.ToInt32(FileLen));
                if (file.Length > 0)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderName, fileName);
                    ReduceImageSize(0.5, MyStream, fullPath);
                    //using (var stream = new FileStream(fullPath, FileMode.Create))
                    //{
                    //   // ReduceImageSize(0.5, MyStream, fullPath);
                    //    file.CopyTo(stream);
                    //}

                    return Ok(new { dbPath });
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        public void ReduceImageSize(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = System.Drawing.Image.FromStream(sourcePath))
            {
                var newWidth = (int)(image.Width * scaleFactor);
                var newHeight = (int)(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);

            }
        }
        #endregion

        #region Display Workflow
        [Route("DisplayWorkFlow")]
        [HttpGet]
        public string DisplayWorkFlow(string workflowparams)
        {
            long Id = 0;
            long PostTypeId = 0;
          
            var wfparam = workflowparams.Split(",");

            Id = Convert.ToInt64(wfparam[0]);
            PostTypeId = Convert.ToInt64(wfparam[1]);
           
            return dataList.DataTableToJSONWithStringBuilder(this._common.DisplayWorkFlow(Id,PostTypeId));
        }

        #endregion

    }
}