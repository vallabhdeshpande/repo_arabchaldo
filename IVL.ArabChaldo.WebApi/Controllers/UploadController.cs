﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Net.Http.Headers;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadController : Controller
    {
        #region Imageupload
        // [HttpPost, DisableRequestSizeLimit]
        [HttpPost]
         public IActionResult Upload()
        {
            try
            {
                long FileLen;

                var file = Request.Form.Files[0];
                //Stream strm = file.InputStream;
                string StaticFilesDirectory = "StaticFiles";
                if (!Directory.Exists(StaticFilesDirectory))
                {
                    Directory.CreateDirectory(StaticFilesDirectory);
                }
                string TempImagesDirectory = "StaticFiles\\Images";
                if (!Directory.Exists(TempImagesDirectory))
                {
                    Directory.CreateDirectory(TempImagesDirectory);
                }
                var folderName = Path.Combine("StaticFiles", "Images");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);


                FileLen = file.Length;
                byte[] input = new byte[FileLen];

                // Initialize the stream.
                Stream MyStream = file.OpenReadStream();
                MyStream.Read(input, 0, Convert.ToInt32(FileLen));
                if (file.Length > 0)
                {
                    var myList = new List<string>();
                    var fileNamewithext = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    //FileInfo fi = new FileInfo(fileNamewithext);
                    //var fileName=Path.GetFileNameWithoutExtension(fileNamewithext);
                    //fileName = fileName+DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    //var fileextension = fi.Extension;
                    //fileName = fileName + fileextension;
                    var fullPath = Path.Combine(pathToSave, fileNamewithext);
                    var dbPath = Path.Combine(folderName, fileNamewithext);
                    ReduceImageSize(0.75, MyStream, fullPath);
                    //using (var stream = new FileStream(fullPath, FileMode.Create))
                    //{
                    //   // ReduceImageSize(0.5, MyStream, fullPath);
                    //    file.CopyTo(stream);
                    //}

                    return Ok(new { fileNamewithext });
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }



        public void ReduceImageSize(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = System.Drawing.Image.FromStream(sourcePath))
            {
                //var newWidth = (int)(image.Width * scaleFactor);
                //var newHeight = (int)(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(image.Width, image.Height);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, image.Width, image.Height);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);

            }
        }
        #endregion

        // Image Delete from StaticFiles/Images Folder
        [HttpDelete]
        public IActionResult DeleteImage(string ImageName)
        {
            var folderName = Path.Combine("StaticFiles", "Images");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var filePath = pathToSave + "\\" + ImageName;
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
                return Ok(new { filePath });
            }
            else
            {
                return BadRequest();
            }
        }


    }
}