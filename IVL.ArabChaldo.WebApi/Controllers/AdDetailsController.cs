﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.DAL.Interfaces;
using Service.contract;
using Service.Implement;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.Implement.Implement;
using System.Data;
using IVL.ArabChaldo.BAL;
using Microsoft.AspNetCore.Cors;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;
using Microsoft.AspNetCore.Authorization;

namespace IVL.ArabChaldo.WebApi.Controllers
{
   //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
   
    public class AdDetailsController : ControllerBase
    {
        private readonly IAdDetailsService _adDetails;
        DataTableToJsonList dataList = new DataTableToJsonList();
        private AdDetailsController(IAdDetailsService context)
        {
            _adDetails = context;
        }

        public AdDetailsController() : this(new AdDetailsService())
        {

        }

        // GET: api/AdDetails
       // [Authorize]
        [Route("GetAllAdDetails")]
        [HttpGet]
        public string GetAllAdDetails(string userInfo)
        {
            long loggedInUserId = 0;
            string loggedInRole = "";
            string categoryName = "";

            var userIdnRole = userInfo.Split(",");

            loggedInUserId = Convert.ToInt64(userIdnRole[0]);
            loggedInRole = userIdnRole[1].ToString();
            categoryName = "";
            return dataList.DataTableToJSONWithStringBuilder(this._adDetails.GetAllAdsDetails(loggedInUserId, loggedInRole, categoryName));
        }

        //public IList<AdDetailsDTO> GetAllAdDetails(string userInfo)
        //{
        //    long loggedInUserId = 0;
        //    string loggedInRole = "";

        //    var userIdnRole = userInfo.Split(",");

        //    loggedInUserId = Convert.ToInt64(userIdnRole[0]);
        //    loggedInRole = userIdnRole[1].ToString();

        //    return this._adDetails.GetAllAdDetails(loggedInUserId, loggedInRole);

        //}
        // GET: api/AdDetails
        //[AllowAnonymous]
        [Route("GetAdsAdListBycategoryId")]
        [HttpGet]
        public IList<AdDetailsDTO> GetAdsAdListBycategoryId(long categoryId)
        {
            return this._adDetails.GetAdsAdListBycategoryId(categoryId);

        }
        // GET: api/AdDetails
       // [AllowAnonymous]
        [Route("GetAdsAdListBySubcategoryId")]
        [HttpGet]
        public IList<AdDetailsDTO> GetAdsAdListBySubcategoryId(long SubcategoryId)
        {
            return this._adDetails.GetAdsAdListBySubcategoryId(SubcategoryId);

        }
        // GET: api/AdDetails/5
       // [AllowAnonymous]
        [Route("GetAdDetailsById")]
        [HttpGet]
        public AdDetailsDTO GetAd(long adId)
        {
            AdDetailsDTO adDetails = this._adDetails.GetAdDetailsById(adId);           
            return adDetails;
        }

        // PUT: api/AdDetails/5
       // [AllowAnonymous]
        [HttpPut("{id}")]

        public bool AdDetails([FromRoute] int id, [FromBody] AdDetailsDTO adDetails)
        {
            // Todo implementation for update
            return this._adDetails.UpdateAdDetails(id, adDetails);
        }

        // POST: api/AdDetails
       // [AllowAnonymous]
        [HttpPost]
        public bool PostAdDetails([FromBody] AdDetailsDTO adDetails)
        {
            return this._adDetails.InsertAdDetails(adDetails);

        }

        // DELETE: api/AdDetails/5  
        // Delete, Approve and Reject
       // [AllowAnonymous]
        [HttpDelete("{workflowdetails}")]
        public bool AdStatus([FromRoute] string workflowdetails)
        {
            long id=0;
            string remarks="";
            string action="";
            long createdBy = 0;
            var workflowparam = workflowdetails.Split(",");

                id = Convert.ToInt64(workflowparam[0]);
            createdBy = Convert.ToInt64(workflowparam[1]);
            action = workflowparam[2].ToString();
            if(action!="Delete")
            remarks = workflowparam[3].ToString()+","+ workflowparam[4].ToString()+","+ workflowparam[5].ToString() + "," + workflowparam[6]; 
            else
                remarks = workflowparam[3].ToString();


            return this._adDetails.AdStatus(id,remarks, action, createdBy);
        }
        // GET: api/AdDetails
        [Route("GetAllJobDetails")]
       // [Authorize]
        [HttpGet]

        public string GetAllJobDetails(string userInfo)
        {
            long loggedInUserId = 0;
            string loggedInRole = "";
            string categoryName = "";

            var userIdnRole = userInfo.Split(",");

            loggedInUserId = Convert.ToInt64(userIdnRole[0]);
            loggedInRole = userIdnRole[1].ToString();
            categoryName = "Jobs";
            return dataList.DataTableToJSONWithStringBuilder(this._adDetails.GetAllAdsDetails(loggedInUserId, loggedInRole, categoryName));
        }
        //public IList<AdDetailsDTO> GetAllJobDetails(string userInfo)
        //{
        //    long loggedInUserId = 0;
        //    string loggedInRole = "";

        //    var userIdnRole = userInfo.Split(",");

        //    loggedInUserId = Convert.ToInt64(userIdnRole[0]);
        //    loggedInRole = userIdnRole[1].ToString();

        //    return this._adDetails.GetAllJobDetails(loggedInUserId, loggedInRole);

        //}
        //private bool AdExists(string EmailID)
        //{
        //    return this._adDetails.CheckAdDetailsExits(EmailID);
        //}
    }
}