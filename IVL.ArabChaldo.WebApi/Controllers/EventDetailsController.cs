﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IVL.ArabChaldo.DAL.Models;
using Service.contract;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.Implement;
using System.Data;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventDetailsController : ControllerBase
    {
        // private readonly Offshore_ArabchaldoContext _context;

        private readonly IEventDetailsService _eventDetailsService;
        DataTableToJsonList dataList = new DataTableToJsonList();
        private EventDetailsController(IEventDetailsService context)
        {
            _eventDetailsService = context;
        }

        public EventDetailsController() : this(new EventDetailsService())
        {

        }

        // GET: api/EventDetails
        [Route("GetAllEvent")]
        [HttpGet]
        public string GetAllEvent(string userInfo)
        {
            long loggedInUserId = 0;
            string loggedInRole = "";

            var userIdnRole = userInfo.Split(",");

            loggedInUserId = Convert.ToInt64(userIdnRole[0]);
            loggedInRole = userIdnRole[1].ToString();
            return dataList.DataTableToJSONWithStringBuilder(this._eventDetailsService.GetAllEvent(loggedInUserId, loggedInRole));
        }

        // GET: api/EventDetails/5
        [Route("GetEventDetails")]
        [HttpGet]
        public EventDetailsDTO GetEventDetails(long eventId)
        {
            return this._eventDetailsService.GetEventById(eventId);
        }

        // PUT: api/EventDetails/5
        [HttpPut("{id}")]
        public bool PutEventDetails([FromRoute] long id, [FromBody] EventDetailsDTO eventDetails)
        {
            return this._eventDetailsService.Update(id, eventDetails);
        }

        // POST: api/EventDetails
        [HttpPost]
        public bool PostEventDetails([FromBody]EventDetailsDTO eventDetails)
        {
            return this._eventDetailsService.Insert(eventDetails);
        }

        // DELETE: api/EventDetails/5  
        // Delete, Approve and Reject
        [HttpDelete("{workflowdetails}")]
        public bool EventStatus([FromRoute] string workflowdetails)
        {
            long id = 0;
            string remarks = "";
            string action = "";
            long createdBy = 0;
            var workflowparam = workflowdetails.Split(",");

            id = Convert.ToInt64(workflowparam[0]);
            createdBy = Convert.ToInt64(workflowparam[1]);
            action = workflowparam[2].ToString();
            if (action != "Delete")
                remarks = workflowparam[3].ToString() + "," + workflowparam[4].ToString() + "," + workflowparam[5].ToString();
            else
                remarks = workflowparam[3].ToString();

            return this._eventDetailsService.EventStatus(id, remarks, action, createdBy);
        }
        [Route("GetEventsByCategoryId")]
        [HttpGet]
        public IList<EventDetailsDTO> GetEventsByCategoryId(long categoryId)
        {
            return this._eventDetailsService.GetEventsByCategoryId(categoryId);
        }
        // DELETE: api/EventDetails/5
        //[HttpDelete("{id}")]
        //public bool DeleteEventDetails([FromRoute] long id)
        //{
        //    return this._eventDetailsService.Delete(id);
        //}
    }
}