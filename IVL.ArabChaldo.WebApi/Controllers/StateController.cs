﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.Implement.Implement;
using Service.contract;
using System.Data;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StateController : ControllerBase
    {
        private readonly IStateService _StateDetails;

        private StateController(IStateService context)
        {
            _StateDetails = context;
        }

        public StateController() : this(new StateService())
        {

        }

        // GET: api/state
        [HttpGet]
        public IList<StateDTO> GetAllState()
        {
            return this._StateDetails.GetAllState();

        }

        // GET: api/StateId/5
        [HttpGet("{StateId}")]
        public StateDTO GetStatebyId([FromRoute] int StateId)
        {
            return this._StateDetails.GetStateById(StateId);

        }

        // PUT: api/StateId/5
        [HttpPut("{StateId}")]
        public bool UpdateState([FromRoute] int StateId, [FromBody] StateDTO StateDetails)
        {
            // Todo implementation for update
            return this._StateDetails.UpdateState(StateId, StateDetails);
        }

        // POST: api/StateId
        [HttpPost]
        public bool InsertState([FromBody] StateDTO StateDetails)
        {
            return this._StateDetails.InsertState(StateDetails);

        }

        // DELETE: api/StateId/5
        [HttpDelete("{StateId}")]
        public bool DeleteState([FromRoute] int StateId)
        {
            return this._StateDetails.DeleteState(StateId);
        }

        private bool CheckCategoryExits(string StateName, long StateId)
        {
            return this._StateDetails.CheckStateExits(StateName, StateId);
        }



        // GET: api/state
        [Route("GetStateList")]
        [HttpGet]
        public string GetStateList()
        {
            DataTable dtState = new DataTable();
            dtState = this._StateDetails.FillStateList();
            DataTableToJsonList stateList = new DataTableToJsonList();
            return stateList.DataTableToJSONWithStringBuilder(dtState);

        }
    }
}