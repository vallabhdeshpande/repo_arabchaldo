﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.DAL.Interfaces;
using Service.contract;
using Service.Implement;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.Implement.Implement;
using IVL.ArabChaldo.BAL;
using System.Data;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserDetailsController : ControllerBase
    {
        private readonly IUserDetailsService _userDetails;
        ApplicationSettings appsetting;
        DataTableToJsonList dataList = new DataTableToJsonList();

        private UserDetailsController(IUserDetailsService context)
        {
            _userDetails = context;
        }
        //public UserDetailsController(ApplicationSettings appsetting)
        //{
        //    this.appsetting = appsetting;
        //}

        public UserDetailsController() : this(new UserDetailsService())
        {

        }

        // GET: api/UserDetails
        [HttpGet]
        public string GetAllUser()
        {
            return dataList.DataTableToJSONWithStringBuilder(this._userDetails.GetAllUser());

        }

        // GET: api/UserDetails/5
        [HttpGet("{id}")]
        public UserDetailsDTO GetUser([FromRoute] long id)
        {
            //return this._userDetails.GetUserbyId(id);
            UserDetailsDTO userDetails = this._userDetails.GetUserbyId(id);
            CommonController common = new CommonController();
            CommonBAL commonBal = new CommonBAL();
            

            //StateName
            DataTable stateTable = new DataTable();
            stateTable = commonBal.FillStateDDLByCountryId(userDetails.CountryId);
            if (stateTable.Rows.Count>0 && userDetails.StateId !=0)
            {
                DataRow foundRowState = stateTable.Select("StateId=" + userDetails.StateId)[0];
                            if (foundRowState != null)
                                userDetails.StateName = foundRowState["StateName"].ToString();
            }
            

            //CityName
            DataTable cityTable = new DataTable();
            cityTable = commonBal.FillCityDDLByStateId(userDetails.StateId);
            if (cityTable.Rows.Count>0 && userDetails.CityId !=0)
            {
                DataRow foundRowCity = cityTable.Select("CityId=" + userDetails.CityId)[0];
                if (foundRowCity != null)
                    userDetails.CityName = foundRowCity["CityName"].ToString();
            }
           
            return userDetails;

        }

        // PUT: api/UserDetails/5
        [HttpPut("{id}")]
        public bool UserDetails([FromRoute] int id, [FromBody] UserDetailsDTO userDetails)
        {
            // Todo implementation for update
            return this._userDetails.UpdateUsers(id, userDetails);
        }

        // POST: api/UserDetails
        [HttpPost]
        public bool PostUserDetails([FromBody] UserDetailsDTO userDetails)
        {
          
            return this._userDetails.InsertUser(userDetails);          
             
        }
        public string EncryptText(string password)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(password);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        // DELETE: api/UserDetails/5
        [HttpDelete("{id}")]
        public bool DeleteUserDetails([FromRoute] int id)
        {
            return this._userDetails.DeleteUser(id);
        }

        private bool UserExists(string EmailID)
        {
            return this._userDetails.CheckUserExits(EmailID);
        }
        [HttpPost]
        [Route("SocialloginInfo")]
        public async Task<IActionResult> SocialloginInfo(UserDetailsDTO userDetails)
        {
            string loggedInUserInfo =  this._userDetails.SocialloginInfo(userDetails);

            if (loggedInUserInfo != null || loggedInUserInfo != "")
            {
                // Session

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("UserID",loggedInUserInfo.Split(":")[2].ToString())
                    }),
                    Expires = DateTime.UtcNow.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes("1234567890123456")), SecurityAlgorithms.HmacSha256Signature)
                };
                var tokenHandler = new JwtSecurityTokenHandler();
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(securityToken);
                var userId = loggedInUserInfo.Split(":")[2].ToString();
                return Ok(new { token, loggedInUserInfo });
            }
            else
                return BadRequest(new { message = "Username or password is incorrect." });
        }
    }
}