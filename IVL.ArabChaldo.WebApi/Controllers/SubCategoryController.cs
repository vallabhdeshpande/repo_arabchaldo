﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.Implement.Implement;
using Service.contract;
using System.Data;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubCategoryController : ControllerBase
    {
        private readonly ISubCategoryService _SubCategoryDetails;

        private SubCategoryController(ISubCategoryService context)
        {
            _SubCategoryDetails = context;
        }

        public SubCategoryController() : this(new SubCategoryService())
        {

        }

        // GET: api/Category
        [HttpGet]
        public IList<SubCategoryDTO> GetAllSubCategory()
        {
            return this._SubCategoryDetails.GetAllSubCategory();

        }

        // GET: api/SubCategoryId/5
        [HttpGet("{SubCategoryId}")]
        public SubCategoryDTO GetSubCategorybyId([FromRoute] int SubCategoryId)
        {
            return this._SubCategoryDetails.GetSubCategorybyId(SubCategoryId);

        }

        // PUT: api/SubCategoryId/5
        [HttpPut("{SubCategoryId}")]
        public bool UpdateSubCategory([FromRoute] int SubCategoryId, [FromBody] SubCategoryDTO SubcategoryDetails)
        {
            // Todo implementation for update
            return this._SubCategoryDetails.UpdateSubCategory(SubCategoryId, SubcategoryDetails);
        }

        // POST: api/SubCategoryId
        [HttpPost]
        public bool InsertSubCategory([FromBody] SubCategoryDTO SubcategoryDetails)
        {
            return this._SubCategoryDetails.InsertSubCategory(SubcategoryDetails);

        }

        // DELETE: api/SubCategoryId/5
        [HttpDelete("{SubCategoryId}")]
        public bool DeleteSubCategory([FromRoute] int SubCategoryId)
        {
            return this._SubCategoryDetails.DeleteSubCategory(SubCategoryId);
        }

        private bool CheckCategoryExits(string subCategoryName, long categoryId)
        {
            return this._SubCategoryDetails.CheckSubCategoryExits(subCategoryName, categoryId);
        }



        // GET: api/managesubcategory
        [Route("GetSubCategoryList")]
        [HttpGet]
        public string GetSubCategoryList()
        {
            DataTable dtSubcategory = new DataTable();
            dtSubcategory = this._SubCategoryDetails.FillSubcategoryList();
            DataTableToJsonList subcategoryList = new DataTableToJsonList();
            return subcategoryList.DataTableToJSONWithStringBuilder(dtSubcategory);

        }
    }
}
