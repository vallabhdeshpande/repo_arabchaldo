﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Service.contract;
using Service.Implement;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.Implement.Implement;
using System.Data;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManageCategoryController : ControllerBase
    {
        private readonly IManageCategoryService _CategoryDetails;

        private ManageCategoryController(IManageCategoryService context)
        {
            _CategoryDetails = context;
        }

        public ManageCategoryController() : this(new ManageCategoryService())
        {

        }

        // GET: api/Category
        [HttpGet]
        public IList<CategoryDTO> GetAllCategory()
        {
            return this._CategoryDetails.GetAllCategory();

        }

        // GET: api/Category/5
        [HttpGet("{CategoryId}")]
        public CategoryDTO GetCategorybyId([FromRoute] int CategoryId)
        {
            return this._CategoryDetails.GetCategorybyId(CategoryId);

        }

        // PUT: api/Category/5
        [HttpPut("{CategoryId}")]
        public bool updateCategory([FromRoute] int CategoryId, [FromBody] CategoryDTO categoryDetails)
        {
            // Todo implementation for update
            return this._CategoryDetails.UpdateCategory(CategoryId, categoryDetails);
        }

        // POST: api/Category
        [HttpPost]
        public bool InsertCategory([FromBody] CategoryDTO categoryDetails)
        {
            return this._CategoryDetails.InsertCategory(categoryDetails);

        }

        // DELETE: api/Category/5
        [HttpDelete("{CategoryId}")]
        public bool DeleteCategory([FromRoute] int CategoryId)
        {
            return this._CategoryDetails.DeleteCategory(CategoryId);
        }

        private bool CheckCategoryExits(string CategoryName)
        {
            return this._CategoryDetails.CheckCategoryExits(CategoryName);
        }

        // GET: api/managecategory
        [Route("GetCategoryList")]
        [HttpGet]
        public string GetCategoryList()
        {
            DataTable dtCategory = new DataTable();
            dtCategory = this._CategoryDetails.FillCategoryList();
            DataTableToJsonList categoryList = new DataTableToJsonList();
            return categoryList.DataTableToJSONWithStringBuilder(dtCategory);

        }

    }
}
