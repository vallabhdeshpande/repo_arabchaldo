﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Net.Http.Headers;
using System.Drawing;
using System.Drawing.Drawing2D;
namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadFileController : ControllerBase
    {
        #region Imageupload
        [HttpPost, DisableRequestSizeLimit]
        public IActionResult Upload()
        {
            try
            {
                long FileLen;

                var file = Request.Form.Files[0];
                //Stream strm = file.InputStream;
                string StaticFilesDirectory = "StaticFiles";
                if (!Directory.Exists(StaticFilesDirectory))
                {
                    Directory.CreateDirectory(StaticFilesDirectory);
                }



                FileLen = file.Length;
                byte[] input = new byte[FileLen];

                // Initialize the stream.
                Stream MyStream = file.OpenReadStream();
                MyStream.Read(input, 0, Convert.ToInt32(FileLen));
                if (file.Length > 0)
                {
                    string TempDirectory = "";
                    var folderName = "";
                    var myList = new List<string>();
                    var fileNamewithext = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    FileInfo fi = new FileInfo(fileNamewithext);                    
                    var fileextension = fi.Extension;                  
                    if (fileextension == ".doc" || fileextension == ".docx" || fileextension == ".pdf" || fileextension == ".txt" || fileextension == ".jpeg" || fileextension == ".jpg" || fileextension == ".png" || fileextension == ".bmp" || fileextension == ".xls" || fileextension == ".xlsx")
                    {
                        TempDirectory = "StaticFiles\\Docs";
                        folderName = Path.Combine("StaticFiles", "Docs");
                    }
                   

                    if (!Directory.Exists(TempDirectory))
                    {
                        Directory.CreateDirectory(TempDirectory);
                    }

                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    var fullPath = Path.Combine(pathToSave, fileNamewithext);
                    var dbPath = Path.Combine(folderName, fileNamewithext);
                    if (fileextension == ".doc" || fileextension == ".docx" || fileextension == ".pdf" || fileextension == ".txt" || fileextension == ".jpeg" || fileextension == ".jpg" || fileextension == ".png" || fileextension == ".bmp" || fileextension == ".xls" || fileextension == ".xlsx")

                    {
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {

                            file.CopyTo(stream);
                        }
                    }
                   // else { ReduceImageSize(0.75, MyStream, fullPath); }


                    if (TempDirectory == "StaticFiles\\Docs")
                        return Ok(new { fullPath });
                    else
                        return Ok(new { fileNamewithext });
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }



        public void ReduceImageSize(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = System.Drawing.Image.FromStream(sourcePath))
            {
                //var newWidth = (int)(image.Width * scaleFactor);
                //var newHeight = (int)(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(image.Width, image.Height);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, image.Width, image.Height);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);

            }
        }


        // Image Delete from StaticFiles/Images Folder
        
       
        [Route("DeleteFile")]
        [HttpDelete]
        public IActionResult DeleteFile(string FileName)
        {
            var folderName = Path.Combine("StaticFiles", "Docs");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var filePath = pathToSave + "\\" + FileName;
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
                return Ok(new { filePath });
            }
            else
            {
                return BadRequest();
            }
        }
        #endregion

    }
}