﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using IVL.ArabChaldo.DAL.Repository;
using IVL.ArabChaldo.DAL.Models;
using Microsoft.Extensions.Options;
using Service.contract;
using Service.Implement;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Authentication;
using System.Text.RegularExpressions;
using System.Data;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private Offshore_ArabchaldoContext Acdcontext;
        private readonly ILoginService _loginDetails;
        private readonly ApplicationSettings _appSettings;
        DataTableToJsonList dataList = new DataTableToJsonList();

        private LoginController(ILoginService context)
        {
            _loginDetails = context;
        }

        public LoginController() : this(new LoginService())
        {

        }
          
        [HttpPost]
        [Route("Login")]
        // POST : /api/LoginController/Login
        public async Task<IActionResult> Login(LoginDetails model)
           
        {
             //bool IsSocialuser = this._loginDetails.checkDuplicateEmail(model.Email);     
           // LoginDetailsDTO _loginDetails = this._loginDetails.GetLogin(model.Email, model.Password);
           
            string loggedInUserInfo = this._loginDetails.GetLoginInfo(model.Email, model.Password);
             if (loggedInUserInfo != "" )
            {
                // Session
                //try
                //{
                //    HttpContext.Session.SetString("UserID", loggedInUserInfo.Split(":")[2].ToString());

                //}
                //catch (Exception ex)
                //{

                //    throw ex;
                //}


                //var tokenDescriptor = new SecurityTokenDescriptor
                //{
                //    Subject = new ClaimsIdentity(new Claim[]
                //    {
                //        new Claim("UserID",loggedInUserInfo.Split(":")[2].ToString()),
                //       new Claim("RoleName",loggedInUserInfo.Split(":")[5].ToString())
                //    }),
                //    Expires = DateTime.UtcNow.AddDays(1),
                //    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes("1234567890123456")), SecurityAlgorithms.HmacSha256Signature)


                //};
                //var tokenHandler = new JwtSecurityTokenHandler();
                //var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                //var token = tokenHandler.WriteToken(securityToken);
                //var userId = loggedInUserInfo.Split(":")[2].ToString();
                // authentication successful so generate jwt token
                var tokenHandler = new JwtSecurityTokenHandler();
                //var key = Encoding.ASCII.GetBytes(_appSettings.JWT_Secret);
                var key = Encoding.UTF8.GetBytes("1234567890123456");
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                         new Claim("UserID",loggedInUserInfo.Split(":")[2].ToString()),
                    new Claim(ClaimTypes.Role,loggedInUserInfo.Split(":")[5].ToString())
                    }),
                   // Expires = DateTime.UtcNow.AddDays(1),
                 Expires = DateTime.UtcNow.AddMinutes(5),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(securityToken);
                return Ok(new { token,loggedInUserInfo });
            }
            else
                return BadRequest(new { message = "Username or password is incorrect." });
        }


        [HttpGet("{userId}")]
        public LoginDetailsDTO GetLoginByUserID([FromRoute] long userId)
        {
            return this._loginDetails.GetLoginByUserID(userId);

        }
        // PUT: api/UserDetails/5
        [HttpPut("{userId}")]
        public bool ResetPassword([FromRoute] int userId, [FromBody] LoginDetailsDTO loginDetails)
        {
            // Todo implementation for update
            return this._loginDetails.ResetPassword(userId, loginDetails);
        }

        // PUT: api/UserDetails/5
        [Route("ActivateUserAccount")]
        [HttpGet]
        public bool ActivateUserAccount(string info)
        {
            return this._loginDetails.ActivateUserAccount(info);
        }


        // GET: api/city
        [Route("GetUserInfoByEmail")]
        [HttpGet]
        public string GetUserInfoByEmail(string Email)
        {
            string UserInfoByEmail = "";
            DataTable dtUserInfo = new DataTable();
            dtUserInfo = this._loginDetails.GetUserInfoByEmail(Email);
            DataTableToJsonList cityList = new DataTableToJsonList();

            UserInfoByEmail = cityList.DataTableToJSONWithStringBuilder(dtUserInfo);
            UserInfoByEmail = Regex.Replace(UserInfoByEmail, @"\t|\n|\r", "");            
            return UserInfoByEmail;

        }




    }
}