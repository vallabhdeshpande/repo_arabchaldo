﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IVL.ArabChaldo.CommonDTO.Models;
using Microsoft.AspNetCore.Mvc;
using Service.contract;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;
using Service.Implement.Implement;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WebsiteController : ControllerBase
    {
        private readonly IWebsiteDetailsService _WebsiteDetails;
        DataTableToJsonList dataList = new DataTableToJsonList();
        private WebsiteController(IWebsiteDetailsService context)
        {
            _WebsiteDetails = context;
        }

        public WebsiteController() : this(new WebsiteDetailsService())
        {

        }
        #region Business Listings
        // GET: api/AdDetails
        [Route("GetAllAdDetails")]
        [HttpGet]
        public string GetAllAdDetails(string imgurl)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetAllAdDetails(imgurl));

        }
        // GET: api/AdDetails
        [Route("GetAllJobDetails")]
        [HttpGet]
        public string GetAllJobDetails(string imgurl)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetAllJobDetails(imgurl));

        }
        // GET: api/AdDetails
        [Route("GetAdsAdListBycategoryId")]
        [HttpGet]
        public IList<AdDetailsDTO> GetAdsAdListBycategoryId(long categoryId)
        {
            return this._WebsiteDetails.GetAdsAdListBycategoryId(categoryId);

        }
        // GET: api/AdDetails
        [Route("GetAdsAdListBySubcategoryId")]
        [HttpGet]
        public IList<AdDetailsDTO> GetAdsAdListBySubcategoryId(long SubcategoryId)
        {
            return this._WebsiteDetails.GetAdsAdListBySubcategoryId(SubcategoryId);

        }
        // GET: api/AdDetails/5
        [Route("GetAdDetailsById")]
        [HttpGet]
        public AdDetailsDTO GetAd(long adId)
        {
            AdDetailsDTO adDetails = this._WebsiteDetails.GetAdDetailsById(adId);
            return adDetails;
        }
        #endregion

        #region Small Advertisements
        // GET: api/AdDetails
        [Route("GetAllSmallAdDetails")]
        [HttpGet]
        public string GetAllSmallAdDetails(string imgurl)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetAllSmallAdDetails(imgurl));

        }        
       
        // GET: api/AdDetails/5
        [Route("GetSmallAdDetailsById")]
        [HttpGet]
        public SmallAdDetailsDTO GetSmallAdDetailsById(long smalladId)
        {
            SmallAdDetailsDTO smalladDetails = this._WebsiteDetails.GetSmallAdDetailsById(smalladId);
            return smalladDetails;
        }
        #endregion

        #region Banners
        // GET: api/BannerDetails
        [Route("GetAllBanner")]  
        [HttpGet]
        public string GetAllBanner(string imgurl)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetAllBanner(imgurl));
        }
        // GET: api/BannerDetails
        [Route("getPremiumBannerList")]
        [HttpGet]
        public string getAllPremiumBannerList(string imgurl)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.getPremiumBannerList(imgurl));
        }
        
        // GET: api/BannerDetails/5
        [Route("GetBannerById")]
        [HttpGet]
        public BannerDetailsDTO GetBannerById(long BannerId)
        {
            return this._WebsiteDetails.GetBannerById(BannerId);
        }
        #endregion

        #region Blogs
        // GET: api/BlogDetails
        [Route("GetAllBlog")]
        [HttpGet]
        public string GetAllBlog()
        {
            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetAllBlog());
        }

        // GET: api/BlogDetails/5
        [Route("GetBlogById")]
        [HttpGet]
        public BlogDetailsDTO GetBlogById(long BlogId)
        {
            return this._WebsiteDetails.GetBlogById(BlogId);

        }
        #endregion

        #region Events
        // GET: api/EventDetails
        [Route("GetAllEvent")]
        [HttpGet]
        public IList<EventDetailsDTO> GetAllEvent()
        {
            return this._WebsiteDetails.GetAllEvent();
        }
        // GET: api/EventDetails/5
        [Route("GetEventDetails")]
        [HttpGet]
        public EventDetailsDTO GetEventDetails(long eventId)
        {
            return this._WebsiteDetails.GetEventById(eventId);
        }

        [Route("GetEventsByCategoryId")]
        [HttpGet]
        public string GetEventsByCategoryId(long categoryId)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetEventsByCategoryId(categoryId));
            //return this._WebsiteDetails.GetEventsByCategoryId(categoryId);
        }
        #endregion


        #region additional Info
        // GET: api/FeedbackDetails
        //[Route("Feedbackemail")]
        [HttpPost]
        public bool SendEmail([FromBody] EmailSenderDetailsDTO EmailDetails)
         {
            bool mailResponse = false;

            mailResponse= this._WebsiteDetails.SendEmail(EmailDetails);
            if (mailResponse)
            {
                if (EmailDetails.mailfor == "Jobs")
                {
                    EmailDetails.email = EmailDetails.emailUser;
                }

                    return this._WebsiteDetails.SendAcknowledgement(EmailDetails);
            }
            else
                return false;
        }


        //[Route("ContactDetails")]
        //[HttpPost]
        //public bool ContactDetails([FromBody] EmailSenderDetailsDTO ContactDetails)
        //{
        //    return this._WebsiteDetails.ContactDetails(ContactDetails);
        //}




        #endregion


        #region GlobalSearch
        // GET:  Categories in approved BL
        [Route("GetCategorySearchDDL")]
        [HttpGet]
        public string GetCategorySearchDDL()
        {

            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetCategorySearchDDL());

        }

        // GET: Cities in approved BL
        [Route("GetCitySearchDDL")]
        [HttpGet]
        public string GetCitySearchDDL()
        {
            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetCitySearchDDL());

        }
        // GET: 
        [Route("GetAllCategoryData")]
        [HttpGet]
        public string GetAllCategoryData()
        {
           
            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetAllCategoryData());

        }

        // GET: 
        [Route("GetAllCityData")]
        [HttpGet]
        public string GetAllCityData()
        {
            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetAllCityData());

        }

        // GET: api/AdDetails
        [Route("GetAdListBySearchCriteria")]
        [HttpGet]
        public string GetAdListBySearchCriteria(string searchCriteria)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetAdListBySearchCriteria(searchCriteria));

        }

        #endregion

        #region website template data
        [Route("GetAboutUSData")]
        [HttpGet]
        public string GetAboutUSData()
        {

            return  dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetAboutUSData());
            
        }
        [Route("GetContactUSData")]
        [HttpGet]
        public string GetContactUSData()
        {

            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetContactUSData());

        }
        [Route("GetPrivacyPolicyData")]
        [HttpGet]
        public string GetPrivacyPolicyData()
        {

            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetPrivacyPolicyData());

        }
        [Route("GetTermsofUseData")]
        [HttpGet]
        public string GetTermsofUseData()
        {

            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetTermsofUseData());

        }
        [Route("GetFAQData")]
        [HttpGet]
        public string GetFAQData()
        {

            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetFAQData());

        }
        [Route("GetDisclaimerData")]
        [HttpGet]
        public string GetDisclaimerData()
        {

            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetDisclaimerData());

        }
        #endregion

        #region BuySell
        // GET: api/BannerDetails
        [Route("GetAllBuysellData")]
        [HttpGet]
        public string GetAllBuysellData(string imgurl)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetAllBuysellData(imgurl));
        }

        // GET: api/BannerDetails
        [Route("GetAllBuysellRecentData")]
        [HttpGet]
        public string GetAllBuysellRecentData(string imgurl)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._WebsiteDetails.GetAllBuysellRecentData(imgurl));
        }
        // GET: api/BuysellDetails/5
        [Route("GetBuysellById")]
        [HttpGet]
        public BuySellDetailsDTO GetBuysellById(long buysellId)
        {
            return this._WebsiteDetails.GetBuysellById(buysellId);
        }
        #endregion

    }
}