﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IVL.ArabChaldo.DAL.Models;
using Service.contract;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.Implement;
using System.Data;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BannerDetailsController : ControllerBase
    {
        // private readonly Offshore_ArabchaldoContext _context;

        private readonly IBannerDetailsService _bannerDetailsService;

        private BannerDetailsController(IBannerDetailsService context)
        {
            _bannerDetailsService = context;
        }

        public BannerDetailsController():this(new BannerDetailsService())
        {

        }

        // GET: api/BannerDetails
        [Route("GetAllBanner")]
        [HttpGet]
        public string GetAllBanner(int userId )
        {
            DataTable dtBanner = new DataTable();
            dtBanner = this._bannerDetailsService.GetAllBanner(userId);
            DataTableToJsonList bannerList = new DataTableToJsonList();
            return bannerList.DataTableToJSONWithStringBuilder(dtBanner);
            //return this._bannerDetailsService.GetAllBanner(userId);
        }

        // GET: api/BannerDetails/5
        [HttpGet("{id}")]
        public BannerDetailsDTO GetBannerDetails([FromRoute] long id)
        {
            return this._bannerDetailsService.GetBannerById(id);
        }

        // GET: api/BannerDetails/5
        //[HttpGet("{id}")]
        //public IList<BannerDetailsDTO> GetBannerDetailsByUserId([FromRoute] long id, string email)
        //{
        //    return this._bannerDetailsService.GetBannerById(id);
        //}


        // PUT: api/BannerDetails/5
        [HttpPut("{id}")]
        public bool PutBannerDetails([FromRoute] long id, [FromBody] BannerDetailsDTO bannerDetails)
        {
            return this._bannerDetailsService.Update(id, bannerDetails);
        }

        // POST: api/BannerDetails
        [HttpPost]
        public bool PostBannerDetails([FromBody]BannerDetailsDTO bannerDetails)
        {
            return this._bannerDetailsService.Insert(bannerDetails);
        }

        // DELETE: api/BannerDetails/5  
        // Delete, Approve and Reject
        [HttpDelete("{workflowdetails}")]
        public bool BannerStatus([FromRoute] string workflowdetails)
        {
            long id = 0;
            string remarks = "";
            string action = "";
            long createdBy = 0;
            var workflowparam = workflowdetails.Split(",");

            id = Convert.ToInt64(workflowparam[0]);
            createdBy = Convert.ToInt64(workflowparam[1]);
            action = workflowparam[2].ToString();
            if (action != "Delete")
                remarks = workflowparam[3].ToString() + "," + workflowparam[4].ToString() + "," + workflowparam[5].ToString() + "," + workflowparam[6];
            else
                remarks = workflowparam[3].ToString();

            return this._bannerDetailsService.BannerStatus(id, remarks, action, createdBy);
        }

        // DELETE: api/BannerDetails/5
        //[HttpDelete("{id}")]
        //public bool DeleteBannerDetails([FromRoute] long id)
        //{            
        //    return this._bannerDetailsService.Delete(id);
        //}

        // GET: api/Banner
        [Route("GetBannerList")]
        [HttpGet]
        public string GetCategoryList()
        {
            DataTable dtBanner = new DataTable();
            dtBanner = this._bannerDetailsService.FillBannerList();
            DataTableToJsonList bannerList = new DataTableToJsonList();
            return bannerList.DataTableToJSONWithStringBuilder(dtBanner);

        }


    }
}