﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.DAL.Interfaces;
using Service.contract;
using Service.Implement;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.Implement.Implement;
using System.Data;
using IVL.ArabChaldo.BAL;
using Microsoft.AspNetCore.Cors;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class SmallAdDetailsController : ControllerBase
    {
        private readonly ISmallAdDetailsService _smallAdDetails;
        DataTableToJsonList dataList = new DataTableToJsonList();
        private SmallAdDetailsController(ISmallAdDetailsService context)
        {
            _smallAdDetails = context;
        }

        public SmallAdDetailsController() : this(new SmallAdDetailsService())
        {

        }

        // GET: api/SmallAdDetails
        [Route("GetAllSmallAdDetails")]
        [HttpGet]
        public string GetAllSmallAdDetails(string userInfo)
        {
            long loggedInUserId = 0;
            string loggedInRole = "";
            string categoryName = "";

            var userIdnRole = userInfo.Split(",");

            loggedInUserId = Convert.ToInt64(userIdnRole[0]);
            loggedInRole = userIdnRole[1].ToString();
            categoryName = "";
            return dataList.DataTableToJSONWithStringBuilder(this._smallAdDetails.GetAllSmallAdsDetails(loggedInUserId, loggedInRole));
        }

      
        // GET: api/SmallAdDetails/5
        [Route("GetSmallAdDetailsById")]
        [HttpGet]
        public SmallAdDetailsDTO GetSmallAd(long adId)
        {
            SmallAdDetailsDTO adDetails = this._smallAdDetails.GetSmallAdDetailsById(adId);
            return adDetails;
        }

        // PUT: api/SmallAdDetails/5
        [HttpPut("{id}")]
        public bool SmallAdDetails([FromRoute] int id, [FromBody] SmallAdDetailsDTO adDetails)
        {
            // Todo implementation for update
            return this._smallAdDetails.UpdateSmallAdDetails(id, adDetails);
        }

        // POST: api/SmallAdDetails
        [HttpPost]
        public bool PostSmallAdDetails([FromBody] SmallAdDetailsDTO adDetails)
        {
            return this._smallAdDetails.InsertSmallAdDetails(adDetails);

        }

        // DELETE: api/SmallAdDetails/5  
        // Delete, Approve and Reject
        [HttpDelete("{workflowdetails}")]
        public bool SmallAdStatus([FromRoute] string workflowdetails)
        {
            long id = 0;
            string remarks = "";
            string action = "";
            long createdBy = 0;
            var workflowparam = workflowdetails.Split(",");

            id = Convert.ToInt64(workflowparam[0]);
            createdBy = Convert.ToInt64(workflowparam[1]);
            action = workflowparam[2].ToString();
            if (action != "Delete")
                remarks = workflowparam[3].ToString() + "," + workflowparam[4].ToString() + "," + workflowparam[5].ToString() + "," + workflowparam[6];
            else
                remarks = workflowparam[3].ToString();


            return this._smallAdDetails.SmallAdStatus(id, remarks, action, createdBy);
        }
       
    }
}