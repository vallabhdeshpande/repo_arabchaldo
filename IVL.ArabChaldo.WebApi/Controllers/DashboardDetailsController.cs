﻿using IVL.ArabChaldo.CommonDTO.Common_Utilities;
using IVL.ArabChaldo.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Service.contract;
using Service.Implement;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardDetailsController : ControllerBase
    {
        private readonly IDashBoardService _dashboardService;
        DataTableToJsonList dataList = new DataTableToJsonList();
        private DashboardDetailsController(IDashBoardService context)
        {
            _dashboardService = context;
        }

        public DashboardDetailsController() : this(new DashBoardService())
        {
        }
            // GET: api/EventDetails
        [Route("GetotalCount")]
        [HttpGet]
        public String GetotalCount()
        {
            return dataList.DataTableToJSONWithStringBuilder(this._dashboardService.GetotalCount());
        }

        // GET: api/EventDetails
        [Route("GetBargraphdata")]
        [HttpGet]
        public DataTable GetBargraphdata(int year) 
        {
            return this._dashboardService.GetBargraphdata(year);
        }
        // GET: api/EventDetails
        [Route("GetBUtotalCount")]
        [HttpGet]
        public String GetBUtotalCount(long UserId)
        {
            return dataList.DataTableToJSONWithStringBuilder(this._dashboardService.GeBUtotalCount(UserId));
        }
        // Get Disk Space
        [Route("GetDiskSpace")]
        [HttpGet]
        public String GetDiskSpace()
        {
            DriveInfo[] allDrives = DriveInfo.GetDrives();

            string Name;
            string DriveType;
            string VolumeLable;
            string DriveFormat;
            long AvailableFreeSpace;
            long TotalFreeSpace;
            long TotalSize;


            foreach (DriveInfo d in allDrives)
            {
                Console.WriteLine("Drive {0}", d.Name);
                Name = d.Name;
                Console.WriteLine("  File type: {0}", d.DriveType);
               // DriveType = d.DriveType;
                if (d.IsReady == true)
                {
                    Console.WriteLine("  Volume label: {0}", d.VolumeLabel);
                    Console.WriteLine("  File system: {0}", d.DriveFormat);
                    Console.WriteLine(
                        "  Available space to current user:{0, 15} bytes",
                        d.AvailableFreeSpace);
                    AvailableFreeSpace = d.AvailableFreeSpace;
                    Console.WriteLine(
                        "  Total available space:          {0, 15} bytes",
                        d.TotalFreeSpace);
                    TotalFreeSpace = d.TotalFreeSpace;
                    Console.WriteLine(
                        "  Total size of drive:            {0, 15} bytes ",
                        d.TotalSize);
                    TotalSize = d.TotalSize;
                }
            }
            return "";
        }
    }
}
