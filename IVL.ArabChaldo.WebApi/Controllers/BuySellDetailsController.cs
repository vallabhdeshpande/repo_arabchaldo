﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;
using IVL.ArabChaldo.CommonDTO.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.contract;
using Service.Implement;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BuySellDetailsController : ControllerBase
    {
        private readonly IBuySellDetailsService _buySellDetails;

        DataTableToJsonList dataList = new DataTableToJsonList();
        private BuySellDetailsController(IBuySellDetailsService context)
        {
            _buySellDetails = context;
        }

        public BuySellDetailsController() : this(new BuySellDetailsService())
        {

        }

        // GET: api/BuySellDetails
        [Route("GetAllBuySellDetails")]
        [HttpGet]
        public string GetAllBuySellDetails(string userInfo)
        {
            long loggedInUserId = 0;
            string loggedInRole = "";

            var userIdnRole = userInfo.Split(",");

            loggedInUserId = Convert.ToInt64(userIdnRole[0]);
            loggedInRole = userIdnRole[1].ToString();
            return dataList.DataTableToJSONWithStringBuilder(this._buySellDetails.GetAllBuySellDetails(loggedInUserId, loggedInRole));
        }

        // POST: api/BuySellDetails
        [HttpPost]
        public bool PostAdDetails([FromBody] BuySellDetailsDTO buysellDetails)
        {
            return this._buySellDetails.InsertBuySellDetails(buysellDetails);

        }

        // GET: api/AdDetails/5
        [Route("GetbuySellDetailsById")]
        [HttpGet]
        public BuySellDetailsDTO GetbuySellDetailsById(long buysellId)
        {
            BuySellDetailsDTO adDetails = this._buySellDetails.GetbuySellDetailsById(buysellId);
            return adDetails;
        }

        // PUT: api/AdDetails/5
        [HttpPut("{id}")]
        public bool updateBuysellDetails([FromRoute] int id, [FromBody] BuySellDetailsDTO buysellDetails)
        {
            // Todo implementation for update
            return this._buySellDetails.UpdatebuySellDetails(id, buysellDetails);
        }



        // DELETE: api/AdDetails/5  
        // Delete, Approve and Reject
        [HttpDelete("{workflowdetails}")]
        public bool buySellAdStatus([FromRoute] string workflowdetails)
        {
            long id = 0;
            string remarks = "";
            string action = "";
            long createdBy = 0;
            var workflowparam = workflowdetails.Split(",");

            id = Convert.ToInt64(workflowparam[0]);
            createdBy = Convert.ToInt64(workflowparam[1]);
            action = workflowparam[2].ToString();
            if (action != "Delete")
                remarks = workflowparam[3].ToString() + "," + workflowparam[4].ToString() + "," + workflowparam[5].ToString() + "," + workflowparam[6];
            else
                remarks = workflowparam[3].ToString();


            return this._buySellDetails.buySellAdStatus(id, remarks, action, createdBy);
        }

    }
}
