﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IVL.ArabChaldo.CommonDTO.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.contract;
using Service.Implement;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConfigurationController : ControllerBase
    { 
 
    private readonly IConfigurationService _ConfigDetails;

    private ConfigurationController(IConfigurationService context)
    {
            _ConfigDetails = context;
    }

    public ConfigurationController() : this(new ConfigurationService())
    {

    }

    // GET: api/UserDetails
    [HttpGet]
    public IList<ConfigurationDTO> GetAllConfigDetails()
    {
        return this._ConfigDetails.GetAllConfigDetails();

    }


    // PUT: api/UserDetails/5
    [HttpPut("{id}")]
    public bool updateConfigDetails([FromRoute] int id, [FromBody] ConfigurationDTO configDetails)
    {
        // Todo implementation for update
        return this._ConfigDetails.UpdateConfig(id, configDetails);
    }


    // DELETE: api/UserDetails/5
    [HttpDelete("{id}")]
    public bool DeleteConfigDetails([FromRoute] int id)
    {
        return this._ConfigDetails.DeleteConfig(id);
    }
}
}