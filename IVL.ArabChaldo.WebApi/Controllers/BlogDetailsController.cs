﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using IVL.ArabChaldo.DAL.Models;
using Service.contract;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.Implement;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogDetailsController : ControllerBase
    {
        // private readonly Offshore_ArabchaldoContext _context;
        DataTableToJsonList dataList = new DataTableToJsonList();
        private readonly IBlogDetailsService _blogDetailsService;

        private BlogDetailsController(IBlogDetailsService context)
        {
            _blogDetailsService = context;
        }

        public BlogDetailsController() : this(new BlogDetailsService())
        {

        }

        //// GET: api/BlogDetails
        //[HttpGet]
        //public IList<BlogDetailsDTO> GetAllBlog(string userInfo)
        //{
        //    long loggedInUserId = 0;
        //    string loggedInRole = "";

        //    var userIdnRole = userInfo.Split(",");

        //    loggedInUserId = Convert.ToInt64(userIdnRole[0]);
        //    loggedInRole = userIdnRole[1].ToString();
        //    return this._blogDetailsService.GetAllBlog(loggedInUserId, loggedInRole);
        //}

        // GET: api/BlogDetails
        [Route("GetAllBlog")]
        [HttpGet]
        public string GetAllBlog(string userInfo)
        {
            long loggedInUserId = 0;
            string loggedInRole = "";

            var userIdnRole = userInfo.Split(",");

            loggedInUserId = Convert.ToInt64(userIdnRole[0]);
            loggedInRole = userIdnRole[1].ToString();
            return dataList.DataTableToJSONWithStringBuilder(this._blogDetailsService.GetAllBlog(loggedInUserId, loggedInRole));
        }

        // GET: api/BlogDetails/5
        [HttpGet("{id}")]
        public BlogDetailsDTO GetBlogDetails([FromRoute] long id)
        {
            return this._blogDetailsService.GetBlogById(id);
        }

        // PUT: api/BlogDetails/5
        [HttpPut("{id}")]
        public bool PutBlogDetails([FromRoute] long id, [FromBody] BlogDetailsDTO blogDetails)
        {
            return this._blogDetailsService.Update(id, blogDetails);
        }

        // POST: api/BlogDetails
        [HttpPost]
        public bool PostBlogDetails([FromBody]BlogDetailsDTO blogDetails)
        {
            return this._blogDetailsService.Insert(blogDetails);
        }

        // DELETE: api/BlogDetails/5  
        // Delete, Approve and Reject
        [HttpDelete("{workflowdetails}")]
        public bool BlogStatus([FromRoute] string workflowdetails)
        {
            long id = 0;
            string remarks = "";
            string action = "";
            long createdBy = 0;
            var workflowparam = workflowdetails.Split(",");

            id = Convert.ToInt64(workflowparam[0]);
            createdBy = Convert.ToInt64(workflowparam[1]);
            action = workflowparam[2].ToString();
            if (action != "Delete")
                remarks = workflowparam[3].ToString() + "," + workflowparam[4].ToString() + "," + workflowparam[5].ToString();
            else
                remarks = workflowparam[3].ToString();

            return this._blogDetailsService.BlogStatus(id, remarks, action, createdBy);
        }

        // DELETE: api/BlogDetails/5
        //[HttpDelete("{id}")]
        //public bool DeleteBlogDetails([FromRoute] long id)
        //{            
        //    return this._blogDetailsService.Delete(id);
        //}
    }
}