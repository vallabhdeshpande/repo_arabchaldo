﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IVL.ArabChaldo.DAL.Models;
using IVL.ArabChaldo.DAL.Interfaces;
using Service.contract;
using Service.Implement;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.Implement.Implement;
using System.Data;
using IVL.ArabChaldo.BAL;
using Microsoft.AspNetCore.Cors;

using IVL.ArabChaldo.CommonDTO.Common_Utilities;
using Microsoft.AspNetCore.Authorization;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageBoardController : ControllerBase
    {
        private readonly IMessageBoardService _MessageBoard;
        DataTableToJsonList dataList = new DataTableToJsonList();
        private MessageBoardController(IMessageBoardService context)
        {
            _MessageBoard = context;
        }

        public MessageBoardController() : this(new MessageBoardService())
        {

        }

        // GET: api/MessageBoard
        [HttpGet]
        public string GetAllMessageBoard()
        {
            return dataList.DataTableToJSONWithStringBuilder(this._MessageBoard.GetAllMessageBoard());
            //return this._MessageBoard.GetAllMessageBoard();

        }
        

        // GET: api/MessageBoardId/5
        [HttpGet("{MessageBoardId}")]
        public MessageBoardDTO GetMessageBoardbyId([FromRoute] long MessageBoardId)
        {
            return this._MessageBoard.GetMessageBoardById(MessageBoardId);

        }

        // PUT: api/MessageBoardId/5
        [HttpPut("{MessageBoardId}")]
        public bool UpdateMessageBoard([FromRoute] long MessageBoardId, [FromBody] MessageBoardDTO MessageBoardDetails)
        {
            // Todo implementation for update
            return this._MessageBoard.UpdateMessageBoard(MessageBoardId, MessageBoardDetails);
        }

        // POST: api/MessageBoardId
        [HttpPost]
        public bool InsertMessageBoard([FromBody] MessageBoardDTO MessageBoard)
        {
            MessageBoard.CreatedDate = DateTime.Now;
            MessageBoard.UpdatedDate = DateTime.Now;
            MessageBoard.IsActive = true;
            return this._MessageBoard.InsertMessageBoard(MessageBoard);

        }

        // DELETE: api/MessageBoardId/5
        [HttpDelete("{MessageBoardId}")]
        public bool DeleteMessageBoard([FromRoute] string MessageBoardId)
        {
            var mbTobic = MessageBoardId.Split(',');
            long MbId = Convert.ToInt64(mbTobic[0]);
            if (mbTobic[1]=="Topic")
            {
                return this._MessageBoard.DeleteMessageBoard(MbId);
            }
            else
            {
                return this._MessageBoard.DeleteMessageBoardDetails(MbId);
            }
        }

        private bool CheckCategoryExits(string Name,long CategoryId)
        {
            return this._MessageBoard.CheckMessageBoardExits(Name, CategoryId);
        }




        // DELETE: api/MessageBoardId/5
        [HttpDelete("{MessageBoardDetailsId}")]
        public bool DeleteMessageBoardDetails([FromRoute] string MessageBoardDetailsId)
        {
            var mbTobic = MessageBoardDetailsId.Split(',');
            long MbId = Convert.ToInt64(mbTobic[0]);
            if (mbTobic[1] == "Topic")
            {
                return this._MessageBoard.DeleteMessageBoard(MbId);
            }
            else
            {
                return this._MessageBoard.DeleteMessageBoardDetails(MbId);
            }
        }


        //MessageBoardDetails

        // GET: api/MessageBoard
        [Route("GetMessageBoardReplyByMessageBoardId")]
        [HttpGet]
        //public IList<MessageBoardDetailsDTO> GetMessageBoardReplyByMessageBoardId(long MessageBoardId)
        //{
        //    return this._MessageBoard.GetMessageBoardReplyByMessageBoardId(MessageBoardId);

        //}
        public string GetMessageBoardReplyByMessageBoardId(long MessageBoardId)
        {
            DataTable dtmessageBoardReplyList = new DataTable();
            dtmessageBoardReplyList = this._MessageBoard.FillMessageBoardReplyList(MessageBoardId);
            DataTableToJsonList messageBoardReplyList = new DataTableToJsonList();
            return messageBoardReplyList.DataTableToJSONWithStringBuilder(dtmessageBoardReplyList);
        }

        // POST:
        [Route("InsertMessageBoardDetails")]
        [HttpPost]

        public bool InsertMessageBoardDetails([FromBody] MessageBoardDTO MessageBoard)
       
        {
            MessageBoardDetailsDTO MessageBoardDetails = new MessageBoardDetailsDTO();
            MessageBoardDetails.MessageBoardId = MessageBoard.MessageBoardId;
            MessageBoardDetails.MessageBoardDetailsId = 0;
            MessageBoardDetails.Name = MessageBoard.Name;
            MessageBoardDetails.Description = MessageBoard.Description;
            MessageBoardDetails.CreatedBy = MessageBoard.CreatedBy;
            MessageBoardDetails.UpdatedBy = MessageBoard.CreatedBy;
            MessageBoardDetails.CreatedDate = DateTime.Now;
            MessageBoardDetails.UpdatedDate = DateTime.Now;
            MessageBoardDetails.IsActive = true;
           
           return this._MessageBoard.InsertMessageBoardDetails(MessageBoardDetails);

        }
    }
}