﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;
using Service.Implement.Implement;
using Service.contract;
using System.Data;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
[ApiController]
public class CityController : ControllerBase
{
    private readonly ICityService _CityDetails;

    private CityController(ICityService context)
    {
            _CityDetails = context;
    }

    public CityController() : this(new CityService())
    {

    }

    // GET: api/city
    [HttpGet]
    public IList<CityDTO> GetAllCity()
    {
        return this._CityDetails.GetAllCity();

    }
        // GET: api/city
        [Route("GetCityWithPagination")]
        [HttpGet]
        public IList<CityDTO> GetCityWithPagination(string paginationFilter)
        {
            return this._CityDetails.GetCityWithPagination(paginationFilter);

        }

        // GET: api/CityId/5
        [HttpGet("{CityId}")]
    public CityDTO GetCitybyId([FromRoute] int CityId)
    {
        return this._CityDetails.GetCityById(CityId);

    }

    // PUT: api/CityId/5
    [HttpPut("{CityId}")]
    public bool UpdateCity([FromRoute] int CityId, [FromBody] CityDTO CityDetails)
    {
        // Todo implementation for update
        return this._CityDetails.UpdateCity(CityId, CityDetails);
    }

    // POST: api/CityId
    [HttpPost]
    public bool InsertCity([FromBody] CityDTO CityDetails)
    {
        return this._CityDetails.InsertCity(CityDetails);

    }

    // DELETE: api/CityId/5
    [HttpDelete("{CityId}")]
    public bool DeleteCity([FromRoute] int CityId)
    {
        return this._CityDetails.DeleteCity(CityId);
    }

    private bool CheckCategoryExits(string CityName,long StateId, long CityId)
    {
        return this._CityDetails.CheckCityExits(CityName, StateId,CityId);
    }

        // GET: api/city
        [Route("GetCityList")]
        [HttpGet]
        public string GetCityList()
        {
            string CityList = "";
            DataTable dtCity = new DataTable();
            dtCity = this._CityDetails.FillCityList();
            DataTableToJsonList cityList = new DataTableToJsonList();
           
            CityList =  cityList.DataTableToJSONWithStringBuilder(dtCity);
            CityList = Regex.Replace(CityList, @"\t|\n|\r", "");
            // CityList= JsonConvert.SerializeObject(cityList.DataTableToJSONWithStringBuilder(dtCity));
            // return CityList.Substring(1, CityList.Length - 2);
            return CityList;

        }
    }
}