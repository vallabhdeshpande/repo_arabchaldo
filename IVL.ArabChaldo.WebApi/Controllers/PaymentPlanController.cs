﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.contract;
using Service.Implement;
using Microsoft.EntityFrameworkCore;
using System.Data;
using IVL.ArabChaldo.CommonDTO.Common_Utilities;

namespace IVL.ArabChaldo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentPlanController : ControllerBase
    {

        private readonly IPaymentPlanService _paymentDetailsService;

        DataTableToJsonList dataList = new DataTableToJsonList();

        private PaymentPlanController(IPaymentPlanService context)
        {
            _paymentDetailsService = context;
        }

        public PaymentPlanController() : this(new PaymentPlanService())
        {

        }

        // GET: api/PaymentPlan
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}


        //Get:api/PaymentPlan/9
        [Route("PlanListForUser")]
        [HttpGet]
        public List<PaymentPlanDTO> PlanListForUser(int userId, string loginUser = "SA")
        {
            string location = loginUser;
            List<PaymentPlanDTO> paymentPlanDTO = new List<PaymentPlanDTO>();
            paymentPlanDTO = this._paymentDetailsService.GetPlanListForUser(userId, location);
            return paymentPlanDTO;
        }

        //Get:api/PaymentPlan
        [Route("PaymentPlanList")]
        [HttpGet]
        public string GetAllPlans()
        {
            DataTable dt= this._paymentDetailsService.GetAllPlan();
            return dataList.DataTableToJSONWithStringBuilder(dt);
        }

        // GET: api/PaymentPlan/5
        [Route("PlanIdDetails")]
        [HttpGet("{id}")]
        public PaymentPlanDTO GetPlanIdDetails(int planId)
        {
            return this._paymentDetailsService.GetPlanId(planId);

        }

        // POST: api/PaymentPlan
        [HttpPost]
        public string Post([FromBody]PaymentPlanDTO value)
        {
            string isSuccess = this._paymentDetailsService.InsertPlan(value);
            return isSuccess;
        }

        // PUT: api/PaymentPlan/5
        [HttpPut("{id}")]
        public void UpdatePlanDetails([FromRoute]long id, [FromBody] PaymentPlanDTO paymentPlanDTO)
        {
            this._paymentDetailsService.UpdatePlanDetails(id, paymentPlanDTO);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [Route("StripePaymentConfirmation")]
        //[HttpGet("{tokenDetails}")]
        [HttpPost]
        public string StripePayment(string tokenDetails)
        {
            string paymentResponse = this._paymentDetailsService.StripePaymentConfirmation(tokenDetails);
            return paymentResponse;
        }

        [Route("OfflinePaymentConfirmation")]
        [HttpPost]
        public bool OfflinePaymentConfirmation(string offlinePayment)
        {
            bool paymentResponse = this._paymentDetailsService.OfflinePaymentConfirmation(offlinePayment);
            return true;
        }
    }
}
