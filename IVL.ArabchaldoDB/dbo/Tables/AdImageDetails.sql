﻿CREATE TABLE [dbo].[AdImageDetails] (
    [AdImageDetailId] BIGINT        IDENTITY (1, 1) NOT NULL,
    [AdImageUrl]      VARCHAR (250) NOT NULL,
    [AdId]            BIGINT        NOT NULL,
    [IsActive]        BIT           NOT NULL,
    [CreatedDate]     DATETIME      NULL,
    [CreatedBy]       BIGINT        NULL,
    [UpdatedDate]     DATETIME      NULL,
    [UpdatedBy]       BIGINT        NULL,
    CONSTRAINT [PK_AdImageDetails] PRIMARY KEY CLUSTERED ([AdImageDetailId] ASC),
    CONSTRAINT [FK_AdImageDetails_AdDetails] FOREIGN KEY ([AdId]) REFERENCES [dbo].[AdDetails] ([AdId])
);

