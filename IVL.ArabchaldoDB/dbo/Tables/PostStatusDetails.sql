﻿CREATE TABLE [dbo].[PostStatusDetails] (
    [PostStatusDetailsId] BIGINT        IDENTITY (1, 1) NOT NULL,
    [PostDetailsId]       BIGINT        NOT NULL,
    [WorkflowStatusId]    BIGINT        NOT NULL,
    [Remark]              VARCHAR (500) NULL,
    [IsActive]            BIT           NOT NULL,
    [CreatedDate]         DATETIME      NULL,
    [CreatedBy]           BIGINT        NULL,
    [UpdatedDate]         DATETIME      NULL,
    [UpdatedBy]           BIGINT        NULL,
    CONSTRAINT [PK_PostStatusDetails] PRIMARY KEY CLUSTERED ([PostStatusDetailsId] ASC),
    CONSTRAINT [FK_PostStatusDetails_PostDetails] FOREIGN KEY ([PostDetailsId]) REFERENCES [dbo].[PostDetails] ([PostDetailsId]),
    CONSTRAINT [FK_PostStatusDetails_WorkflowStatus] FOREIGN KEY ([WorkflowStatusId]) REFERENCES [dbo].[WorkflowStatus] ([WorkflowStatusId])
);

