﻿CREATE TABLE [dbo].[Country] (
    [CountryId]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [CountryName] VARCHAR (50)  NOT NULL,
    [Description] VARCHAR (500) NULL,
    [IsActive]    BIT           NOT NULL,
    [CreatedDate] DATETIME      NULL,
    [CreatedBy]   BIGINT        NULL,
    [UpdatedDate] DATETIME      NULL,
    [UpdatedBy]   BIGINT        NULL,
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([CountryId] ASC)
);

