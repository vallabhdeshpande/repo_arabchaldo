﻿CREATE TABLE [dbo].[Configuration] (
    [ConfigurationId]            INT             IDENTITY (1, 1) NOT NULL,
    [LogoCharge]                 DECIMAL (18, 2) NULL,
    [ImageCharge]                DECIMAL (18, 2) NULL,
    [BannerCharge]               DECIMAL (18, 2) NULL,
    [Smtp]                       VARCHAR (20)    NULL,
    [Port]                       INT             NULL,
    [FromMailId]                 VARCHAR (50)    NULL,
    [Password]                   NVARCHAR (50)   NULL,
    [RegistrationMailTemplate]   NVARCHAR (MAX)  NULL,
    [ResetPasswordMailTemplate]  NVARCHAR (MAX)  NULL,
    [ChangePasswordMailTemplate] NVARCHAR (MAX)  NULL,
    [ForgotPasswordMailTemplate] NVARCHAR (MAX)  NULL,
    [PostMailTemplate]           NVARCHAR (MAX)  NULL,
    [ApproveMailTemplate]        NVARCHAR (MAX)  NULL,
    [RejectMailTemplate]         NVARCHAR (MAX)  NULL,
    [IsActive]                   BIT             NULL,
    [CreatedBy]                  BIGINT          NULL,
    [CreatedDate]                DATETIME        NULL,
    [UpdatedBy]                  BIGINT          NULL,
    [UpdatedDate]                DATETIME        NULL,
    CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED ([ConfigurationId] ASC)
);

