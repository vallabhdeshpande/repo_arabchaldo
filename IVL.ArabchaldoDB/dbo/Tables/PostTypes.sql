﻿CREATE TABLE [dbo].[PostTypes] (
    [PostTypeId]  INT          IDENTITY (1, 1) NOT NULL,
    [PostType]    VARCHAR (30) NOT NULL,
    [IsActive]    BIT          NOT NULL,
    [CreatedDate] DATETIME     NULL,
    [CreatedBy]   BIGINT       NULL,
    [UpdatedDate] DATETIME     NULL,
    [UpdatedBy]   BIGINT       NULL,
    CONSTRAINT [PK_PostType] PRIMARY KEY CLUSTERED ([PostTypeId] ASC)
);

