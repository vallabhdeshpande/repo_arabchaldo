﻿CREATE TABLE [dbo].[WorkflowStatus] (
    [WorkflowStatusId] BIGINT       IDENTITY (1, 1) NOT NULL,
    [Status]           VARCHAR (50) NOT NULL,
    [IsActive]         BIT          NOT NULL,
    [CreatedDate]      DATETIME     NULL,
    [CreatedBy]        BIGINT       NULL,
    [UpdatedDate]      DATETIME     NULL,
    [UpdatedBy]        BIGINT       NULL,
    CONSTRAINT [PK_WorkflowStatus] PRIMARY KEY CLUSTERED ([WorkflowStatusId] ASC)
);

