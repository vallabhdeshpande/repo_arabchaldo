﻿CREATE TABLE [dbo].[RolePermissions] (
    [RolePermissionId] BIGINT   IDENTITY (1, 1) NOT NULL,
    [RoleId]           BIGINT   NOT NULL,
    [PermissionId]     BIGINT   NOT NULL,
    [IsActive]         BIT      NOT NULL,
    [CreatedDate]      DATETIME NULL,
    [CreatedBy]        BIGINT   NULL,
    [UpdatedDate]      DATETIME NULL,
    [UpdatedBy]        BIGINT   NULL,
    CONSTRAINT [PK_RolePermissions] PRIMARY KEY CLUSTERED ([RolePermissionId] ASC),
    CONSTRAINT [FK_RolePermissions_Permissions] FOREIGN KEY ([PermissionId]) REFERENCES [dbo].[Permissions] ([PermissionId]),
    CONSTRAINT [FK_RolePermissions_Roles] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Roles] ([RoleId])
);

