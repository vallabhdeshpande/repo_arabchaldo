﻿CREATE TABLE [dbo].[SubCategory] (
    [SubCategoryId]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [SubCategoryName] VARCHAR (150) NULL,
    [Description]     VARCHAR (500) NULL,
    [CategoryId]      BIGINT        NOT NULL,
    [CategoryName]    VARCHAR (50)  NULL,
    [IsActive]        BIT           NOT NULL,
    [CreatedDate]     DATETIME      NULL,
    [CreatedBy]       BIGINT        NULL,
    [UpdatedDate]     DATETIME      NULL,
    [UpdatedBy]       BIGINT        NULL,
    CONSTRAINT [PK_SubCategory] PRIMARY KEY CLUSTERED ([SubCategoryId] ASC),
    CONSTRAINT [FK_SubCategory_Category] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Category] ([CategoryId])
);

