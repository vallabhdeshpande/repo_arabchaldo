﻿CREATE TABLE [dbo].[AdStatusDetails] (
    [AdStatusDetailsId] BIGINT        IDENTITY (1, 1) NOT NULL,
    [AdId]              BIGINT        NOT NULL,
    [WorkflowStatusId]  BIGINT        NOT NULL,
    [Remark]            VARCHAR (500) NULL,
    [IsActive]          BIT           NOT NULL,
    [CreatedDate]       DATETIME      NULL,
    [CreatedBy]         BIGINT        NULL,
    [UpdatedDate]       DATETIME      NULL,
    [UpdatedBy]         BIGINT        NULL,
    CONSTRAINT [PK_AdStatusDetails] PRIMARY KEY CLUSTERED ([AdStatusDetailsId] ASC),
    CONSTRAINT [FK_AdStatusDetails_AdDetails] FOREIGN KEY ([AdId]) REFERENCES [dbo].[AdDetails] ([AdId]),
    CONSTRAINT [FK_AdStatusDetails_WorkflowStatus] FOREIGN KEY ([WorkflowStatusId]) REFERENCES [dbo].[WorkflowStatus] ([WorkflowStatusId])
);

