﻿CREATE TABLE [dbo].[State] (
    [StateId]     BIGINT        IDENTITY (1, 1) NOT NULL,
    [StateName]   VARCHAR (50)  NOT NULL,
    [Description] VARCHAR (500) NULL,
    [CountryId]   BIGINT        NOT NULL,
    [IsActive]    BIT           NOT NULL,
    [CreatedDate] DATETIME      NULL,
    [CreatedBy]   BIGINT        NULL,
    [UpdatedDate] DATETIME      NULL,
    [UpdatedBy]   BIGINT        NULL,
    CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED ([StateId] ASC),
    CONSTRAINT [FK_State_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([CountryId])
);

