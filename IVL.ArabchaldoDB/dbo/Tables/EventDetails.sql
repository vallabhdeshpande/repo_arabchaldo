﻿CREATE TABLE [dbo].[EventDetails] (
    [EventId]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [Title]          VARCHAR (250)  NOT NULL,
    [Description]    NVARCHAR (MAX) NULL,
    [ImageUrl]       NVARCHAR (500) NULL,
    [PostedFor]      BIGINT         NOT NULL,
    [Website]        NVARCHAR (150) NULL,
    [EventDate]      DATETIME       NOT NULL,
    [AddressStreet1] VARCHAR (150)  NOT NULL,
    [AddressStreet2] VARCHAR (150)  NULL,
    [CountryId]      BIGINT         NOT NULL,
    [StateId]        BIGINT         NOT NULL,
    [CityId]         BIGINT         NOT NULL,
    [ZipCode]        VARCHAR (10)   NOT NULL,
    [IsVisible]      BIT            NULL,
    [IsActive]       BIT            NOT NULL,
    [CreatedDate]    DATETIME       NULL,
    [CreatedBy]      BIGINT         NULL,
    [UpdatedDate]    DATETIME       NULL,
    [UpdatedBy]      BIGINT         NULL,
    [PostedForName]  VARCHAR (50)   NULL,
    [CityName]       VARCHAR (50)   NULL,
    [CategoryId]     BIGINT         NULL,
    [CategoryName]   VARCHAR (50)   NULL,
    CONSTRAINT [PK_EventDetails] PRIMARY KEY CLUSTERED ([EventId] ASC),
    CONSTRAINT [FK_Event_Category] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Category] ([CategoryId]),
    CONSTRAINT [FK_EventDetails_EventDetails] FOREIGN KEY ([EventId]) REFERENCES [dbo].[EventDetails] ([EventId])
);

