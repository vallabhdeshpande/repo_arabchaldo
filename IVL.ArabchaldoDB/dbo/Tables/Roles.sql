﻿CREATE TABLE [dbo].[Roles] (
    [RoleId]      BIGINT        IDENTITY (1, 1) NOT NULL,
    [RoleName]    VARCHAR (50)  NOT NULL,
    [Description] VARCHAR (200) NULL,
    [IsActive]    BIT           NOT NULL,
    [CreatedDate] DATETIME      NULL,
    [CreatedBy]   BIGINT        NULL,
    [UpdatedDate] DATETIME      NULL,
    [UpdatedBy]   BIGINT        NULL,
    CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED ([RoleId] ASC)
);

