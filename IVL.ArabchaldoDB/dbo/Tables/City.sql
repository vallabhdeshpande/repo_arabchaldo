﻿CREATE TABLE [dbo].[City] (
    [CityId]      BIGINT        IDENTITY (1, 1) NOT NULL,
    [CityName]    VARCHAR (50)  NOT NULL,
    [Description] VARCHAR (500) NULL,
    [StateId]     BIGINT        NOT NULL,
    [StateName]   VARCHAR (50)  NULL,
    [IsActive]    BIT           NOT NULL,
    [CreatedDate] DATETIME      NULL,
    [CreatedBy]   BIGINT        NULL,
    [UpdatedDate] DATETIME      NULL,
    [UpdatedBy]   BIGINT        NULL,
    CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED ([CityId] ASC),
    CONSTRAINT [FK_City_State] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([StateId])
);

