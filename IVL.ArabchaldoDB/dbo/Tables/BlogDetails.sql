﻿CREATE TABLE [dbo].[BlogDetails] (
    [BlogId]      BIGINT         IDENTITY (1, 1) NOT NULL,
    [Title]       VARCHAR (250)  NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    [ImageUrl]    NVARCHAR (500) NULL,
    [IsVisible]   BIT            NULL,
    [IsActive]    BIT            NOT NULL,
    [CreatedDate] DATETIME       NULL,
    [CreatedBy]   BIGINT         NULL,
    [UpdatedDate] DATETIME       NULL,
    [UpdatedBy]   BIGINT         NULL,
    [ValidTill]   DATETIME       NULL,
    CONSTRAINT [PK_BlogDetails] PRIMARY KEY CLUSTERED ([BlogId] ASC)
);

