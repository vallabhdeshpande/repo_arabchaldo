﻿CREATE TABLE [dbo].[BannerStatusDetails] (
    [BannerStatusDetailsId] BIGINT        IDENTITY (1, 1) NOT NULL,
    [BannerId]              BIGINT        NOT NULL,
    [WorkflowStatusId]      BIGINT        NOT NULL,
    [Remark]                VARCHAR (500) NULL,
    [IsActive]              BIT           NOT NULL,
    [CreatedDate]           DATETIME      NULL,
    [CreatedBy]             BIGINT        NULL,
    [UpdatedDate]           DATETIME      NULL,
    [UpdatedBy]             BIGINT        NULL,
    CONSTRAINT [PK_BannerStatusDetails] PRIMARY KEY CLUSTERED ([BannerStatusDetailsId] ASC),
    CONSTRAINT [FK_BannerStatusDetails_BannerDetails] FOREIGN KEY ([BannerId]) REFERENCES [dbo].[BannerDetails] ([BannerId]),
    CONSTRAINT [FK_BannerStatusDetails_WorkflowStatus] FOREIGN KEY ([WorkflowStatusId]) REFERENCES [dbo].[WorkflowStatus] ([WorkflowStatusId])
);

