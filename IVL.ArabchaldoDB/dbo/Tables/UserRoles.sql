﻿CREATE TABLE [dbo].[UserRoles] (
    [UserRoleId]  BIGINT   IDENTITY (1, 1) NOT NULL,
    [UserId]      BIGINT   NOT NULL,
    [RoleId]      BIGINT   NOT NULL,
    [IsActive]    BIT      NOT NULL,
    [CreatedDate] DATETIME NULL,
    [CreatedBy]   BIGINT   NULL,
    [UpdatedDate] DATETIME NULL,
    [UpdatedBy]   BIGINT   NULL,
    CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED ([UserRoleId] ASC),
    CONSTRAINT [FK_UserRoles_Roles] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Roles] ([RoleId]),
    CONSTRAINT [FK_UserRoles_UserDetails] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserDetails] ([UserId])
);

