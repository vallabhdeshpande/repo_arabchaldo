﻿CREATE TABLE [dbo].[Category] (
    [CategoryId]       BIGINT        IDENTITY (1, 1) NOT NULL,
    [CategoryName]     VARCHAR (150) NULL,
    [Description]      VARCHAR (500) NULL,
    [CategoryImageUrl] VARCHAR (250) NULL,
    [IsActive]         BIT           NOT NULL,
    [CreatedDate]      DATETIME      NULL,
    [CreatedBy]        BIGINT        NULL,
    [UpdatedDate]      DATETIME      NULL,
    [UpdatedBy]        BIGINT        NULL,
    CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED ([CategoryId] ASC)
);

