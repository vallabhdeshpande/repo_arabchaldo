﻿CREATE TABLE [dbo].[LoginDetails] (
    [LoginDetailId] BIGINT        IDENTITY (1, 1) NOT NULL,
    [UserId]        BIGINT        NOT NULL,
    [Password]      VARCHAR (200) NOT NULL,
    [Email]         VARCHAR (50)  NOT NULL,
    [IsActive]      BIT           NOT NULL,
    [CreatedDate]   DATETIME      NULL,
    [CreatedBy]     BIGINT        NULL,
    [UpdatedDate]   DATETIME      NULL,
    [UpdatedBy]     BIGINT        NULL,
    CONSTRAINT [PK_LoginDetails] PRIMARY KEY CLUSTERED ([LoginDetailId] ASC),
    CONSTRAINT [FK_LoginDetails_UserDetails] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserDetails] ([UserId])
);

