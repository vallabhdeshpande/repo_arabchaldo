﻿CREATE TABLE [dbo].[Permissions] (
    [PermissionId]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [PermissionName] VARCHAR (50)  NOT NULL,
    [Description]    VARCHAR (200) NULL,
    [IsActive]       BIT           NOT NULL,
    [CreatedDate]    DATETIME      NULL,
    [CreatedBy]      BIGINT        NULL,
    [UpdatedDate]    DATETIME      NULL,
    [UpdatedBy]      BIGINT        NULL,
    CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED ([PermissionId] ASC)
);

