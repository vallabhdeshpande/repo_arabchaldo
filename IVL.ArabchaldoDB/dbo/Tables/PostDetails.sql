﻿CREATE TABLE [dbo].[PostDetails] (
    [PostDetailsId] BIGINT   IDENTITY (1, 1) NOT NULL,
    [PostTypeId]    INT      NOT NULL,
    [PostId]        BIGINT   NOT NULL,
    [IsActive]      BIT      NOT NULL,
    [CreatedDate]   DATETIME NULL,
    [CreatedBy]     BIGINT   NULL,
    [UpdatedDate]   DATETIME NULL,
    [UpdatedBy]     BIGINT   NULL,
    CONSTRAINT [PK_PostDetails] PRIMARY KEY CLUSTERED ([PostDetailsId] ASC),
    CONSTRAINT [FK_PostDetails_PostType] FOREIGN KEY ([PostTypeId]) REFERENCES [dbo].[PostTypes] ([PostTypeId])
);

