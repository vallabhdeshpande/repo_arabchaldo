﻿ALTER ROLE [db_owner] ADD MEMBER [INFOVISIONLABS\Geeta.Chauhan];


GO
ALTER ROLE [db_owner] ADD MEMBER [INFOVISIONLABS\Vallabh.Deshpande];


GO
ALTER ROLE [db_owner] ADD MEMBER [INFOVISIONLABS\Kunal.Karan];


GO
ALTER ROLE [db_owner] ADD MEMBER [INFOVISIONLABS\Ritu.Dhiria];


GO
ALTER ROLE [db_owner] ADD MEMBER [arabchaldo_user];


GO
ALTER ROLE [db_accessadmin] ADD MEMBER [arabchaldo_user];


GO
ALTER ROLE [db_securityadmin] ADD MEMBER [arabchaldo_user];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [arabchaldo_user];


GO
ALTER ROLE [db_backupoperator] ADD MEMBER [arabchaldo_user];


GO
ALTER ROLE [db_datareader] ADD MEMBER [arabchaldo_user];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [arabchaldo_user];


GO
ALTER ROLE [db_denydatareader] ADD MEMBER [arabchaldo_user];


GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [arabchaldo_user];

