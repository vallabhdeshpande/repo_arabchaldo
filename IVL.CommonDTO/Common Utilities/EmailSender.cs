﻿using IVL.ArabChaldo.CommonDTO.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Common_Utilities
{
    public class EmailSender
    {
        public void SendEmailold(EmailSenderDetailsDTO maildetails)
        {
            try
            {

                string Email = maildetails.email;
                string body = maildetails.comments;
                string subject = maildetails.mailfor + ":- " + maildetails.reason;
                int portNumber = 587;





                SmtpClient sm = new SmtpClient("smtp.office365.com", portNumber);
                NetworkCredential NetCry = new NetworkCredential("ritu.dhiria@ivlglobal.com", "applicationsystem@123", "ivlglobal.com");
                sm.Credentials = NetCry;
                sm.EnableSsl = true;

                MailMessage mm = new MailMessage("ritu.dhiria@ivlglobal.com", Email, subject, body);

                mm.IsBodyHtml = true;
                sm.Send(mm);




                //var smtp = new System.Net.Mail.SmtpClient();
                //{
                //    smtp.Host = "smtp.office365.com";
                //   //smtp.Port = 587;

                //    smtp.EnableSsl = true;
                //    SmtpClient sm = new SmtpClient("smtp.office365.com", 587);
                //    smtp.UseDefaultCredentials = true;
                //    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //    smtp.Credentials = new NetworkCredential("geeta.chauhan@ivlglobal.com", "Presarioc7##");


                //    smtp.Timeout = 600000;
                //}
                //// Passing values to smtp object
                // smtp.Send("ivl.arabchaldo@ivlglobal.com", Email, subject, body);            
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendEmail(EmailSenderDetailsDTO maildetails)
        {
            try
            {
                // Common setting for email sender
                //string UserName = "info@arabchaldo.com";
                //string Password = "ArabChaldo@28551";
                //string Domain = "arabchaldo.com";
                //string host = "smtp.office365.com";
                //string port


                string body = "";
                string mailsubject = "";
                string title = "";
                string multipleemail = "";
                string activationContent = "";
                string paymentlink = "";
                string forgotPasswordContent = "";
                string disclaimer = "";


                if (maildetails.mailfor == "Contact Us")
                //  body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for contacting us. We will contact you soon.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";
                {
                    //mailsubject = "Contact";

                    mailsubject= maildetails.mailfor +" " +"-"+" " + maildetails.reason;



                    body = "<h1> " + maildetails.name + "</h1>"   +"<p>"+ maildetails.contactMailTemplate + "</p>"  ;
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }
                else if (maildetails.mailfor == "ForgotPassword")
                {
                    mailsubject = maildetails.mailfor;
                    body = "<h1>Hello " + maildetails.name + "</h1>" ;
                    //maildetails.confirmationEmailUrl = "http://10.0.40.3:7070" + "/#/changepassword?info=" + maildetails.email + "," + maildetails.activationCode;
                    //maildetails.confirmationEmailUrl = "http://localhost:9094" + "/#/changepassword?info=" + maildetails.email + "," + maildetails.activationCode;
                    forgotPasswordContent = "<br /><br />Please click the following link to reset your password <br /><a href = '" + maildetails.mailConfirmationUrl + "'>Click here to Reset your Password.</a>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }
                else if (maildetails.mailfor == "Feedback")
                {
                   
                    mailsubject = maildetails.mailfor + ":- " + maildetails.reason;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";
                    body = "<h1>Hello " + maildetails.name + "</h1>" + maildetails.feedbackMailTemplate + "";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }
                else if (maildetails.mailfor == "Registration")
                {
                   // mailsubject = maildetails.subject +"-"+ maildetails.mailfor /*+ ":- " + maildetails.reason*/;
                    mailsubject = maildetails.subject + " " + "-" + " " + maildetails.mailfor;
                    body = "<h1>Hello " + maildetails.name + "</h1>" + maildetails.registrationMailTemplate + "   ";
           

                    activationContent = "<br /><br />Please click the following link to activate your account <br /><a href = '" + maildetails.confirmationEmailUrl + "'>Click here to activate your account.</a>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }
              
                else if (maildetails.mailfor == "Jobs")
                {
                 
                    mailsubject = "Jobs:" + maildetails.adId + "_" + maildetails.title;
                    body = "<h1>Jobs Reply From " + maildetails.name + "</h1><table><tbody><tr><th>Name:</th><td>" + maildetails.name + "</td></tr><tr><th>Phone:</th><td>" + maildetails.phone + "</td></tr><tr><th>Email:</th><td>" + maildetails.emailUser + "</td></tr><tr><th>Description:</th><td>" + maildetails.comments + "</td></tr><tr><th>Title:</th><td>" + maildetails.title + "</td></tr></tbody></table></div></div></body>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }
            

                else if (maildetails.mailfor == "user")
                {
                    mailsubject = "New User:";
                    body = "<h1>Hello " + maildetails.name + "</h1> " + maildetails.email +"";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


              

                else if (maildetails.mailfor=="multiplemail")
                {
                    mailsubject = maildetails.subject;
                    maildetails.email = maildetails.toUserMessage;

                                //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";



                                body = "<h1>" + maildetails.subject + "</h1>" + maildetails.content+ "";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


                else if (maildetails.mailfor == "buyplanmail")
                {
                    mailsubject = maildetails.name;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";



                    body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your reaching us.</p><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";


                    //body = "<h1>" + maildetails.name + "</h1><table><tbody><tr><th>Name:</th><td>" + maildetails.name + "</td></tr><tr><th>Phone:</th><td>" + maildetails.phone + "</td></tr><tr><th>Email:</th><td>" + maildetails.email + "</td></tr><tr><th>City:</th><td>" + maildetails.city + "</td></tr><tr><th>Category Name:</th><td>" + maildetails.categoryName + "</td></tr><tr><th>Subcategory name:</th><td>" + maildetails.subCategoryName + "</td></tr><tr><th>Business Name:</th><td>" + maildetails.businessName + "</td></tr><tr><th>Business Purpose:</th><td>" + maildetails.businessPurpose + "</td></tr></tbody></table></div></div></body>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "Business Listing Details")
                {
        
                    mailsubject = maildetails.subject;
                    title = maildetails.title;

                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your business listing has been successfully submitted. Thank you! We will contact you soon once your post has been activated.</p> <p><b> Business Name : " + maildetails.title + " </b></p> ";
                      disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }



                else if (maildetails.mailfor == "SmallAds Listing Details")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your advertisement has been successfully submitted. Thank you! We will contact you soon once your post has been activated.</p> <p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "ApprovedSmallAddetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your advertisement has been approved! Kindly visit the website to view your post.</p><p><b> Title : " + maildetails.title + " </b></p> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }
                


                else if (maildetails.mailfor == "UpdateAdDetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your business listing has been updated! We will contact you soon once your changes has been approved.</p><p><b> Business Name : " + maildetails.title + " </b></p> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }


                else if (maildetails.mailfor == "Buy Sell")
                {
                    mailsubject = maildetails.subject;

                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your buy sell post has been successfully submitted. Thank you! We will contact you soon once your post has been activated.</p> <p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }


                else if (maildetails.mailfor == "UpdateBuysellDetails")
                {
                    mailsubject = maildetails.subject;

                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your buy/sell post has been updated! We will contact you soon once your changes has been approved.</p> <p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }
  
                else if (maildetails.mailfor == "Payment Details")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";

                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your Plan has successfully submitted. Thank You!</p><br>";
                    paymentlink = "Please follow the following path to pay the amount : <br /><p>Home < Dashboard</p>";
                    //paymentlink = "Please click the following link to pay the amount <br /><a href = http://10.0.40.3:7070/#/budashboard >Click here to pay the amount.</a>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "Online Payment Details")
                {
                    mailsubject = maildetails.subject;

                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your Payment has successfully submitted.<br>Billing Details of Payment are - </p><br><p><b> Name : " + maildetails.name + " </b></p><br><p><b> Email : " + maildetails.email + " </b></p><br><p><b> Phone No : " + maildetails.phone + " </b></p><br><p><b> Address : " + maildetails.address1 + "," + maildetails.address2 + "," + maildetails.stateName + " - " + maildetails.zipCode + "</b></p><br><p>Thank you!</p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "Offline Payment Details")
                {
                    mailsubject = maildetails.subject;

                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your Payment has successfully submitted.<br>Billing Details of Payment are - </p><br><p><b> Name : " + maildetails.name + " </b></p><br><p><b> Email : " + maildetails.email + " </b></p><br><p><b> Phone No : " + maildetails.phone + " </b></p><br><p><b> Address : " + maildetails.address1 + "," + maildetails.address2 + "," + maildetails.stateName + " - " + maildetails.zipCode + "</b></p><br><p><b> Receipt No : " + maildetails.receiptNo + "</b></p><br><p>Thank you!</p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "ApprovedAddetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your business listing has been approved! Kindly visit the website to view your post.</p><p><b> Business Name : " + maildetails.title + " </b></p> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }



                else if (maildetails.mailfor == "ApprovedBuyselldetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your buy/sell post has been approved! Kindly visit the website to view your post.</p> <p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }


                else if (maildetails.mailfor == "RejectedBuyselldetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p> Your buy sell post has been rejected because of following reason- " + maildetails.remark + " </ p><br><p><b> Title : " + maildetails.title + " </b></p>";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }



                else if (maildetails.mailfor == "RejectedSmallAddetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your advertisement has been rejected because of following reason- "+maildetails.remark+" </p><br><p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "RejectedAddetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p> Your business listing has been rejected because of following reason- "+maildetails.remark + " </ p><br><p><b> Business Name : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


                else if (maildetails.mailfor == "DeleteAddetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p> Your business listing has been deleted. </p><br><p><b> Business Name : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


                else if (maildetails.mailfor == "DeleteJobdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p> Your job post has been deleted.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "DeleteSmallAddetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your advertisement has been deleted. </p><br><p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }
                else if (maildetails.mailfor == "ApprovedJobdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your job has been approved! Kindly visit the website to view your post.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "RejectedJobdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1><p>Your job has been rejected because of following reason- "+maildetails.remark+ " </ p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "JobupdateDetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h ><p>Your job has been updated! We will contact you soon once your changes has been approved.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }


                else if (maildetails.mailfor == "UpdateSmallAdDetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your advertisement has been updated! We will contact you soon once your changes has been approved.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "Bannerdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your banner has been successfully submitted. Thank you! We will contact you soon once your post has been activated.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "Bannerupdatedetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your banner has been updated! We will contact you soon once your changes has been approved.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }
                
                else if (maildetails.mailfor == "ApprovedBannerdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your banner has been approved! Kindly visit the website to view your post.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


                else if (maildetails.mailfor == "RejectedBannerdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1><p>Your banner has been rejected because of following reason- "+maildetails.remark+ "</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "DeleteBannerdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your banner has been deleted.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "Eventdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your event has been successfully submitted. Thank you! We will contact you soon once your post has been activated.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }



                else if (maildetails.mailfor == "DeleteEventdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your event post has been deleted. </p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "Eventupdatedetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your event has been updated! We will contact you soon once your changes has been approved.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }



                else if(maildetails.mailfor== "UserupdateDetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>'Your profile details has been updated.</p>";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }
                else if (maildetails.mailfor == "UserdeleteDetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>User has been deleted.</p>";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }
                

                else if (maildetails.mailfor == "ApprovedEventdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your event has been approved! Kindly visit the website to view your post.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "RejectedEventdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p> Your event has been rejected because of following reason- "+maildetails.remark+ " </p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "Job Details")
                {
                    
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1><p>Your job has been successfully submitted. Thank you! We will contact you soon once your post has been activated.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "Blogdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your blog has been successfully submitted. Thank you! We will contact you soon once your post has been activated.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


                else if (maildetails.mailfor == "Blogupdatedetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your blog has been updated! We will contact you soon once your changes has been approved.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }
                else if (maildetails.mailfor == "RejectedBlogdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your blog has been rejected because of following reason- "+maildetails.remark+ ".</p><br><p><b> Title : " + maildetails.title + " </b></p> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }


                else if (maildetails.mailfor == "ApprovedBlogdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your blog has been approved! Kindly visit the website to view your post.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


                else if (maildetails.mailfor == "DeletedBlogdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your blog has been deleted.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "resetpassword")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your password has been updated!! </p>";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "DeletedBuyselldetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your buy/sell has been deleted.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }
                StringBuilder sb = new StringBuilder();

                sb.Append(@"<html>
                                            <head>
                                              <meta charset=""utf-8"">
                                              <meta http-equiv=""X-UA-Compatible"" content=""IE=edge"">
                                              <meta name=""description"" content="""">
                                              <meta name=""author"" content="""">
                                              <meta name=""keyword"" content="""">
                                              <style>
                                                body {background: #f5f5f5; position: relative;margin:0;color: #a4a9c1;font-size: 20px;}
                                                .email-block {width: 700px;margin: 0 auto;background: #fff;}
                                                .email-bg {background: #f88a00;width: 100%;padding-top: 50px;padding-bottom: 50px;}
                                                .email-box {width: 600px;margin: 0 auto;background: #fff;border-radius: 8px;box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.1);text-align: center;padding-bottom: 1px;}
                                                .email-box h1 {padding: 10px;font-size: 28px;color: #505050;margin: 10px 0 20px;border-bottom: 3px solid #f88a00;float: left;width: calc(100% - 20px)}
                                                .email-box h2 {color: #666;padding: 0 20px 20px;font-size: 30px;margin: 0;}
                                                p { display: inline-block;padding:0 20px;}
                                                .email-footer {font-size: 10px;border-top: 1px solid #f5f5f5;padding-top: 20px;display: inline-block;margin: 0 0 20px;}
                                                table {border: 0;margin: 20px;text-align: left;width:100%}
                                                td, th {border:0; padding: 5px; vertical-align: top;}
                                              

                                              </style>
                                            </head>
                                            <body>
                                              <div class=""email-block"">
                                                <div class=""email-bg"">
                                                <div class=""email-box"">
                                                <p class=""email-footer"">
");

                sb.Append(body);
           
              
                if (maildetails.mailfor == "Registration")
                    sb.Append(activationContent);
                if (maildetails.mailfor == "Payment Details")
                    sb.Append(paymentlink);
                if (maildetails.mailfor == "ForgotPassword")
                    sb.Append(forgotPasswordContent);

                sb.Append(disclaimer);
                sb.Append("<br><br></div></div></div></p></body></html> ");

            

                //var myvar = "<head><meta charset='"utf-8"'><meta http-equiv=""X-UA-Compatible"" content=""IE=edge""><meta name=""description"" content=""""><meta name=""author"" content=""""><meta name=""keyword"" content=""""><style>body{background:#f5f5f5;position:relative;margin:0;color:#a4a9c1;font-size:20px}.email-block{width:700px;margin:0 auto;background:#fff;padding-bottom:50px}.email-bg{background:#f88a00;height:200px;width:100%}.email-box{width:600px;margin:0 auto;margin-top:-150px;background:#fff;border-radius:8px;box-shadow:0px 2px 5px 0px rgba(0, 0, 0, 0.1);text-align:center;padding-bottom:1px}.email-box h1{padding:10px;font-size:28px;color:#505050;margin:10px 0 20px;border-bottom:3px solid #f88a00;float:left;width:calc(100% - 20px)}.email-box h2{color:#666;padding:0 20px 20px;font-size:30px;margin:0}p{display:inline-block;padding:0 20px}.email-footer{font-size:12px;border-top:1px solid #f5f5f5;padding-top:20px;display:inline-block;margin:0 0 20px}table{border:1px solid #d2d2d2;margin:20px;text-align:left}td,th{border:1px solid #d2d2d2;padding:5px;vertical-align:top}</style></head><body><div class=""email-block""><div class=""email-bg""></div><div class=""email-box"">";






                string Email = maildetails.email;

                if (Email.Contains(','))
                {
                    if (Email.Split(",")[1] == "")
                        Email = Email.Split(",")[0];

                }


                //string body = maildetails.Comments;
                string subject = mailsubject;

                
               
                SmtpClient sm = new SmtpClient(maildetails.smtp, maildetails.port);

                NetworkCredential NetCry = new NetworkCredential(maildetails.fromMailId, maildetails.password,maildetails.domain );
                sm.Credentials = NetCry;
                sm.EnableSsl = true;
                MailMessage mm = new MailMessage(maildetails.fromMailId, Email, subject,sb.ToString());
               
                if (maildetails.fileUrl!=null)
                {
                    if (maildetails.fileUrl!="")
                    {
                        var multiFileUrls = maildetails.fileUrl.Split(",");
                        if (multiFileUrls.Length > 0)
                        {
                            for (int i = 0; i < multiFileUrls.Length; i++)
                            {
                            
                            string TempDirectory = "StaticFiles\\Docs";
                            var folderName = Path.Combine("StaticFiles", "Docs");
                            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                            var fullPath = Path.Combine(pathToSave, multiFileUrls[i]);
                            var dbPath = Path.Combine(folderName, multiFileUrls[i]);
                            System.Net.Mail.Attachment attachment;
                            attachment = new System.Net.Mail.Attachment(dbPath);
                            mm.Attachments.Add(attachment);
                        }
                        }
                                       
                    }
                  
                }
                mm.IsBodyHtml = true;
               // sm.Send(mm);
                sm.SendAsync(mm,"");
          

        
                //MailMessage m = new MailMessage(UserName, "ritud1990@gmail.com", subject, sb.ToString());
                //mm.IsBodyHtml = true;
                //sm.Send(m);







                //var smtp = new System.Net.Mail.SmtpClient();
                //{
                //    smtp.Host = "smtp.office365.com";
                //   //smtp.Port = 587;

                //    smtp.EnableSsl = true;
                //    SmtpClient sm = new SmtpClient("smtp.office365.com", 587);
                //    smtp.UseDefaultCredentials = true;
                //    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //    smtp.Credentials = new NetworkCredential("geeta.chauhan@ivlglobal.com", "Presarioc7##");


                //    smtp.Timeout = 600000;
                //}
                //// Passing values to smtp object
                // smtp.Send("ivl.arabchaldo@ivlglobal.com", Email, subject, body);   
                

                
            }
            catch (Exception ex)
            {
                throw ex;
            }

              
        }
              

        public void SendAcknowledgement(EmailSenderDetailsDTO maildetails)
        {
            try
            {
                // Common setting for email sender
            
                //string UserName = "geeta.chauhan@ivlglobal.com";
                //string Password = "Presarioc7^^";
                string Domain = "ivlglobal.com";
          


                string body = "";
                string mailsubject = "";
            
                string activationContent = "";
                string disclaimer = "";
                if (maildetails.mailfor == "Contact Us")
                {
                    //mailsubject = "Contact";
                    //mailsubject = maildetails.mailfor + ":- " + maildetails.reason;
                    mailsubject = maildetails.mailfor + " " + "-" +""+ maildetails.reason;

                    body = "<h1>Enquiry from " + maildetails.name + "</h1><table><tbody><tr><th>Name:</th><td>" + maildetails.name + "</td></tr><tr><th>Phone:</th><td>" + maildetails.phone + "</td></tr><tr><th>Email:</th><td>" + maildetails.email + "</td></tr><tr><th>Subject:</th><td>" + maildetails.reason + "</td></tr><tr><th>Description:</th><td>" + maildetails.comments + "</td></tr></tbody></table></div></div></body>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }
                else if (maildetails.mailfor == "Feedback")
                {
                    mailsubject = maildetails.mailfor + ":- " + maildetails.reason;
                    body = "<h1>Feedback from " + maildetails.name + "</h1><table><tbody><tr><th>Subject:</th><td>" + maildetails.reason + "</td></tr> <tr><th>Name:</th><td>" + maildetails.name + "</td></tr><tr><th>Phone:</th><td>" + maildetails.phone + "</td></tr><tr><th>Email:</th><td>" + maildetails.email + "</td></tr> <tr><th>State:</th><td>" + maildetails.stateName + "</td></tr> <tr><th>City:</th><td>" + maildetails.cityName + "</td></tr>     <tr><th>Description:</th><td>" + maildetails.comments + "</td></tr></tbody></table></div></div></body>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }
                else if (maildetails.mailfor == "Registration")

                ///body = "<h1>Hello " + maildetails.name + "</h1><p>You have successfully registered on our website.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";
                {
                    mailsubject = maildetails.subject +" " + "-" + " "+ maildetails.mailfor;
                    body = "<h1>Hello " + maildetails.name + "</h1> <p>You have successfully registered on our website.</p>";
                  
                    activationContent = "<br /><br />Please click the following link to activate your account <br /><a href = '" + maildetails.confirmationEmailUrl + "'>Click here to activate your account.</a>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }
                else if (maildetails.mailfor == "Jobs")
                {

                    mailsubject = "Jobs:" + maildetails.adId + "_" + maildetails.title;
                    //body = "<h1>Jobs Reply From " + maildetails.name + "</h1><table><tbody><tr><th>Name:</th><td>" + maildetails.name + "</td></tr><tr><th>Phone:</th><td>" + maildetails.phone + "</td></tr><tr><th>Email:</th><td>" + maildetails.email + "</td></tr><tr><th>Description:</th><td>" + maildetails.comments + "</td></tr></tbody></table></div></div></body>";
                    body = "<h1>Hello Super Admin </h1><p>Your job application has been submitted succesfully.</p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


             


                else if (maildetails.mailfor == "user")
                {
                    mailsubject = "New User:";
                    body = "<h1>Hello Super Admin </h1> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


                else if (maildetails.mailfor == "UserupdateDetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 >User " + maildetails.name + " has updated there profile! </p>";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }
                
                else if (maildetails.mailfor == "resetpassword")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>User " + maildetails.name + " has updated there password! </p>";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "multiplemail")
                {
                    mailsubject = maildetails.subject;
                    maildetails.email = maildetails.toUserMessage;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";


                    body = "<h1>" + maildetails.subject + "</h1>" + maildetails.content + "";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "buyplanmail")
                {
                    mailsubject = maildetails.name;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";


                    //body = "<h1>" + maildetails.name + "</h1><table><tbody><tr><th>Name:</th><td>" + maildetails.name + "</td></tr><tr><th>Phone:</th><td>" + maildetails.phone + "</td></tr><tr><th>Email:</th><td>" + maildetails.email + "</td></tr><tr><th>State:</th><td>" + maildetails.stateName + "</td></tr><tr><th>City:</th><td>" + maildetails.cityName + "</td></tr><tr><th>Business Name:</th><td>" + maildetails.businessName + "</td></tr><tr><th>Business Purpose:</th><td>" + maildetails.businessPurpose + "</td></tr></tbody></table></div></div></body>";
                    body = "<h1>Hello Super Admin </h1><table><tbody><tr><th>Name:</th><td>" + maildetails.name + "</td></tr><tr><th>Phone:</th><td>" + maildetails.phone + "</td></tr><tr><th>Email:</th><td>" + maildetails.email + "</td></tr><tr><th>City:</th><td>" + maildetails.city + "</td></tr><tr><th>Category Name:</th><td>" + maildetails.categoryName + "</td></tr><tr><th>Subcategory name:</th><td>" + maildetails.subCategoryName + "</td></tr><tr><th>Business Name:</th><td>" + maildetails.businessName + "</td></tr><tr><th>Business Purpose:</th><td>" + maildetails.businessPurpose + "</td></tr></tbody></table></div></div></body>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "Business Listing Details")
                {
                   mailsubject = maildetails.subject;
         
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";



                    //body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your Ad has successfully summited. Thank You! We will Contact You soon once your post has been Activated.</p>";

                    body = " <h1> Hello Super Admin </h1 ><p>Business listing post has been added.</p><br><p><b> Business Name : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "UpdateAdDetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Business listing post has been updated.</p><br><p><b> Business Name : " + maildetails.title + " </b></p> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }
                else if (maildetails.mailfor == "ApprovedAddetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1><p>Business listing post has been approved.</p><br><p><b> Business Name : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


                else if (maildetails.mailfor == "RejectedAddetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1><p>Business listing post has been rejected.</p><br><p><b> Business Name : " + maildetails.title + " </b></p> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "DeleteAddetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1><p> Business listing post has been deleted.</p><br><p><b> Business Name : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


                


                  else if (maildetails.mailfor == "DeleteJobdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p> Job post has been deleted. </p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }
                else if (maildetails.mailfor == "ApprovedJobdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";

                     


                    body = "<h1> Hello Super Admin </h1 ><p>Job post has been approved.</p><br><p><b> Title : " + maildetails.title + " </b></p>";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


                else if (maildetails.mailfor == "RejectedJobdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p> Job post has been rejected.</p><br><p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "JobupdateDetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Job post has been updated.</p><br><p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "Bannerdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";



                    //body = "<h1> Hello " + maildetails.name + " </h1 ><p>Your Ad has successfully summited. Thank You! We will Contact You soon once your post has been Activated.</p>";

                    body = " <h1> Hello Super Admin </h1 ><p>Banner post has been added.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }


                else if (maildetails.mailfor == "Bannerupdatedetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Banner post has been updated.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }
                else if (maildetails.mailfor == "ApprovedBannerdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p> Banner post has been approved.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


                else if (maildetails.mailfor == "RejectedBannerdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p> Banner post has been rejected.</ p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "DeleteBannerdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Banner post has been deleted.</ p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


                else if (maildetails.mailfor == "DeletedBuyselldetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Buy/sell post has been deleted.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "Eventdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Event post has been added.</ p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }


                

                else if (maildetails.mailfor == "DeleteEventdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Event post has been deleted.</ p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "Eventupdatedetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Event post has been updated.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }


                else if (maildetails.mailfor == "UserdeleteDetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>User Name: " + maildetails.name + " has been deleted.</p>";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "ApprovedEventdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Event post has been approved.</ p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "RejectedEventdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p> Event post has been rejected.</ p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }



                else if (maildetails.mailfor == "Buy Sell")
                {
                    mailsubject = maildetails.subject;

                    body = "<h1> Hello Super Admin </h1 ><p>Buy/Sell post has been added.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "SmallAds Listing Details")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Small ad has been added.</p><br><p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "DeleteSmallAddetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Small ad has been deleted.</p><br><p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }
                


                else if (maildetails.mailfor == "ApprovedSmallAddetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Small ad has been approved.</p><br><p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }


                else if (maildetails.mailfor == "RejectedSmallAddetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Small ad has been rejected.</p><br><p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "Payment Details")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";

                    body = "<h1> Hello Super Admin </h1 ><p>Your Plan has successfully submitted. Thank You!</p>";
                    activationContent = "<br /><br />Please click the following link to pay the amount <br /><a href = http://localhost:4200/#/budashboard >Click here to pay the amount.</a>";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "Online Payment Details")
                {
                    mailsubject = maildetails.subject;

                    body = "<h1> Hello Super Admin </h1 ><p>Online payment for " + maildetails.name + " has successfully submitted.<br>Billing Details of " + maildetails.name + " are - </p><br><p><b> Name : " + maildetails.name + " </b></p><br><p><b> Email : " + maildetails.email + " </b></p><br><p><b> Phone No : " + maildetails.phone + " </b></p><br><p><b> Address : " + maildetails.address1 + "," + maildetails.address2 + "," + maildetails.stateName + " - " + maildetails.zipCode + "</b></p><br><p>Thank you!</p>";
  
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "Offline Payment Details")
                {
                    mailsubject = maildetails.subject;

                    body = "<h1> Hello Super Admin </h1 ><p>Offline payment for " + maildetails.name + " has successfully submitted.<br>Billing Details of " + maildetails.name + " are - </p><br><p><b> Name : " + maildetails.name + " </b></p><br><p><b> Email : " + maildetails.email + " </b></p><br><p><b> Phone No : " + maildetails.phone + " </b></p><br><p><b> Address : " + maildetails.address1 + "," + maildetails.address2 + "," + maildetails.stateName + " - " + maildetails.zipCode + "</b></p><br><p><b>Receipt No : " + maildetails.receiptNo +"</b></p><br><p>Thank you!</p>";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }


                else if (maildetails.mailfor == "Job Details")
                {
                   // mailsubject = "Jobs:" + maildetails.adId + "_" + maildetails.title;
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Job post has been added.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "Jobupdatedetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Job post has been updated.</p><br><p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "UpdateSmallAdDetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Small ad post has been updated.</p><br><p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "UpdateBuysellDetails")
                {
                    mailsubject = maildetails.subject;

                    body = "<h1> Hello Super Admin </h1 ><p>Buy/Sell post has been updated.</p><br><p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }

                else if (maildetails.mailfor == "ApprovedBuyselldetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Buy/Sell post has been approved.</p><br><p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }


                else if (maildetails.mailfor == "RejectedBuyselldetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p> Buy/Sell post has been rejected.</p><br><p><b> Title : " + maildetails.title + " </b></p>";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }



                else if (maildetails.mailfor == "Blogdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Blog has been added.</p><br><p><b>Title : " + maildetails.title + " </b></p>";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "Blogupdatedetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Blog has been updated.</p><br><p><b> Title : " + maildetails.title + " </b></p>";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }
                else if (maildetails.mailfor == "ApprovedBlogdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Blog has been approved.</p><br><p><b> Title : " + maildetails.title + " </b></p>";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "DeletedBlogdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p> Blog has been deleted.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";

                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";
                }

                else if (maildetails.mailfor == "RejectedBlogdetails")
                {
                    mailsubject = maildetails.subject;
                    //body = "<h1>Hello " + maildetails.name + "</h1><p>Thanks for your valuable feedback.</p><h2> Thank You!</h2 ><p>You have received this email because you registered on arabchaldo website.Please do not reply on this email.</p>";




                    body = "<h1> Hello Super Admin </h1 ><p>Blog has been rejected.</p><br><p><b> Title : " + maildetails.title + " </b></p> ";
                    disclaimer = "<p class=email-footer>" + maildetails.disclaimerTemplate + "</p>";

                }
                StringBuilder sb = new StringBuilder();
                sb.Append(@"<html>
                                            <head>
                                              <meta charset=""utf-8"">
                                              <meta http-equiv=""X-UA-Compatible"" content=""IE=edge"">
                                              <meta name=""description"" content="""">
                                              <meta name=""author"" content="""">
                                              <meta name=""keyword"" content="""">
                                              <style>
                                                body {background: #f5f5f5; position: relative;margin:0;color: #a4a9c1;font-size: 20px;}
                                                .email-block {width: 700px;margin: 0 auto;background: #fff;}
                                                .email-bg {background: #f88a00;width: 100%;padding-top: 50px;padding-bottom: 50px;}
                                                .email-box {width: 600px;margin: 0 auto;background: #fff;border-radius: 8px;box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.1);text-align: center;padding-bottom: 1px;}
                                                .email-box h1 {padding: 10px;font-size: 28px;color: #505050;margin: 10px 0 20px;border-bottom: 3px solid #f88a00;float: left;width: calc(100% - 20px)}
                                                .email-box h2 {color: #666;padding: 0 20px 20px;font-size: 30px;margin: 0;}
                                                p { display: inline-block;padding:0 20px;}
                                                .email-footer {font-size: 12px;border-top: 1px solid #f5f5f5;padding-top: 20px;display: inline-block;margin: 0 0 20px;}
                                                table {border: 0;margin: 20px;text-align: left;width:100%}
                                                td, th {border:0; padding: 5px; vertical-align: top;}

                                              </style>
                                            </head>

<body>
                                              <div class=""email-block"">
                                                <div class=""email-bg"">
                                                <div class=""email-box"">
                                                <p class=""email-footer"">
");

                sb.Append(body);
                sb.Append(disclaimer);


                sb.Append("<br><br></div></ div ></ div ></p></ body ></ html > ");

                              



                //var myvar = "<head><meta charset='"utf-8"'><meta http-equiv=""X-UA-Compatible"" content=""IE=edge""><meta name=""description"" content=""""><meta name=""author"" content=""""><meta name=""keyword"" content=""""><style>body{background:#f5f5f5;position:relative;margin:0;color:#a4a9c1;font-size:20px}.email-block{width:700px;margin:0 auto;background:#fff;padding-bottom:50px}.email-bg{background:#f88a00;height:200px;width:100%}.email-box{width:600px;margin:0 auto;margin-top:-150px;background:#fff;border-radius:8px;box-shadow:0px 2px 5px 0px rgba(0, 0, 0, 0.1);text-align:center;padding-bottom:1px}.email-box h1{padding:10px;font-size:28px;color:#505050;margin:10px 0 20px;border-bottom:3px solid #f88a00;float:left;width:calc(100% - 20px)}.email-box h2{color:#666;padding:0 20px 20px;font-size:30px;margin:0}p{display:inline-block;padding:0 20px}.email-footer{font-size:12px;border-top:1px solid #f5f5f5;padding-top:20px;display:inline-block;margin:0 0 20px}table{border:1px solid #d2d2d2;margin:20px;text-align:left}td,th{border:1px solid #d2d2d2;padding:5px;vertical-align:top}</style></head><body><div class=""email-block""><div class=""email-bg""></div><div class=""email-box"">";






                string Email = maildetails.email;
                
                //string body = maildetails.Comments;
                string subject = mailsubject;
          

                SmtpClient sm = new SmtpClient(maildetails.smtp, maildetails.port);

                NetworkCredential NetCry = new NetworkCredential(maildetails.fromMailId, maildetails.password, Domain);
                sm.Credentials = NetCry;
                sm.EnableSsl = true;
                MailMessage m;
                if (maildetails.mailfor == "Jobs")
                     m = new MailMessage(maildetails.fromMailId, maildetails.emailUser, subject, sb.ToString());
                else
                     m = new MailMessage(maildetails.fromMailId, maildetails.fromMailId, subject, sb.ToString());


                //if (maildetails.fileUrl != null)
                //{
                //    if (maildetails.fileUrl != "")
                //    {
                //        var multiFileUrls = maildetails.fileUrl.Split(",");
                //        if (multiFileUrls.Length > 0)
                //        {
                //            for (int i = 0; i < multiFileUrls.Length; i++)
                //            {

                //                string TempDirectory = "StaticFiles\\Docs";
                //                var folderName = Path.Combine("StaticFiles", "Docs");
                //                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                //                var fullPath = Path.Combine(pathToSave, multiFileUrls[i]);
                //                var dbPath = Path.Combine(folderName, multiFileUrls[i]);
                //                System.Net.Mail.Attachment attachment;
                //                attachment = new System.Net.Mail.Attachment(dbPath);
                //                m.Attachments.Add(attachment);
                //            }
                //        }

                //    }

                //}
                m.IsBodyHtml = true;
               
                // sm.Send(m);
                sm.SendAsync(m, "");







                //var smtp = new System.Net.Mail.SmtpClient();
                //{
                //    smtp.Host = "smtp.office365.com";
                //   //smtp.Port = 587;

                //    smtp.EnableSsl = true;
                //    SmtpClient sm = new SmtpClient("smtp.office365.com", 587);
                //    smtp.UseDefaultCredentials = true;
                //    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //    smtp.Credentials = new NetworkCredential("geeta.chauhan@ivlglobal.com", "Presarioc7##");


                //    smtp.Timeout = 600000;
                //}
                //// Passing values to smtp object
                // smtp.Send("ivl.arabchaldo@ivlglobal.com", Email, subject, body);            
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       


    }



}



