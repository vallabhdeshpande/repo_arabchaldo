﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
//using Newtonsoft.Json;
using System.Text.RegularExpressions;
//using Newtonsoft.Json;
namespace IVL.ArabChaldo.CommonDTO.Common_Utilities
{
    public class DataTableToJsonList
    {

        # region DataTableToJsonList

        public string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ').Trim() + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ').Trim() + "\"");
                        }
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
           // JSONString= Regex.Replace(JSONString.ToString(), @"\t|\n|\r", "");
            return Regex.Replace(JSONString.ToString(), @"\t|\n|\r", ""); 
         // return JsonConvert.SerializeObject(JSONString.ToString());
        }

        private string SanitizeReceivedJson(string uglyJson)
        {
            var sb = new StringBuilder(uglyJson);
           
            sb.Replace("\\\b", "\b");
            sb.Replace("\\\f", "\f");
            sb.Replace("\\\t", "\t");
            sb.Replace("\\\n", "\n");
            sb.Replace("\\\r", "\r");
            return sb.ToString();
        }

        #endregion
    }
}
