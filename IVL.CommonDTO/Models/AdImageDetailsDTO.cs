﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class AdImageDetailsDTO
    {
        public AdImageDetailsDTO()
        {
           
         
        }
        public long AdImageDetailId { get; set; }
        public string AdImageUrl { get; set; }
        public long AdId { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public AdDetailsDTO Ad { get; set; }
    }
}
