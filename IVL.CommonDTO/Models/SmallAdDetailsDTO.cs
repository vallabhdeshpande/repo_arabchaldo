﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class SmallAdDetailsDTO
    {
        public SmallAdDetailsDTO()
        {
           
        }

        public long SmallAdId { get; set; }
        public string Title { get; set; }
        public string TagLine { get; set; }
        public string Description { get; set; }
       
        public string AdLogoUrl { get; set; }
        public string ImageUrl { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactNumber { get; set; }
        public string CountryCodeContact { get; set; }
        public DateTime? ValidTillDate { get; set; }
        public string AlternateContactNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string AddressStreet1 { get; set; }
        public string AddressStreet2 { get; set; }
        public long? CountryId { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public long? StateId { get; set; }
        public string CityName { get; set; }
        public long? CityId { get; set; }
        public string ZipCode { get; set; }
        public long? AssignedTo { get; set; }
        public bool? IsFeatured { get; set; }
        public bool IsActive { get; set; }
        public bool? IsVisible { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? ValidFromDate { get; set; }
     
        public string PostedForName { get; set; }
        public long? PostedFor { get; set; }
        public bool? IsPaidAd { get; set; }
        public long? AdImageCount { get; set; }
        public long? LogoCount { get; set; }
        public long? JobsCount { get; set; }
        public long? PlanId { get; set; }
        public long? VisitorCount { get; set; }
        public string OtherCity { get; set; }
      
    }
}
