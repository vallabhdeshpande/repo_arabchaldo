﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class SubCategoryDTO
    {
        public long SubCategoryId { get; set; }
        public string SubCategoryName { get; set; }
        public string ArabicName { get; set; }
        public string Description { get; set; }
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public CategoryDTO Category { get; set; }
    }
}