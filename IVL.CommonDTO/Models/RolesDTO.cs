﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class RolesDTO
    {
        public RolesDTO()
        {
            RolePermissions = new HashSet<RolePermissionsDTO>();
            UserRoles = new HashSet<UserRolesDTO>();
        }

        public long RoleId { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public ICollection<RolePermissionsDTO> RolePermissions { get; set; }
        public ICollection<UserRolesDTO> UserRoles { get; set; }
    }
}
