﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class WorkflowStatusDTO
    {
        public WorkflowStatusDTO()
        {
            AdStatusDetails = new HashSet<AdStatusDetailsDTO>();
        }

        public long WorkflowStatusId { get; set; }
        public string Status { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public ICollection<AdStatusDetailsDTO> AdStatusDetails { get; set; }
    }
}
