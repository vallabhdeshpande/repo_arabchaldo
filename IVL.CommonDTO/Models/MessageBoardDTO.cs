﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class MessageBoardDTO
    {
        public long MessageBoardId { get; set; }
        public string Topic { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public long CategoryId { get; set; }
        public long? Views { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
