﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class UserRolesDTO
    {
        public long UserRoleId { get; set; }
        public long UserId { get; set; }
        public long RoleId { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public RolesDTO Role { get; set; }
        public UserDetailsDTO User { get; set; }
    }
}
