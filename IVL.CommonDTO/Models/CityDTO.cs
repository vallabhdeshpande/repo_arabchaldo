﻿using IVL.ArabChaldo.CommonDTO.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class CityDTO
    {
        public CityDTO()
        {
            AdDetails = new HashSet<AdDetailsDTO>();
            UserDetails = new HashSet<UserDetailsDTO>();
        }

        public long CityId { get; set; }
        public string CityName { get; set; }
        public string Description { get; set; }
        public long StateId { get; set; }
        public string StateName { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public StateDTO State { get; set; }
        public ICollection<AdDetailsDTO> AdDetails { get; set; }
        public ICollection<UserDetailsDTO> UserDetails { get; set; }
    }
}