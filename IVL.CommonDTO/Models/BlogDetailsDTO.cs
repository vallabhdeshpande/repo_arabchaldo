﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class BlogDetailsDTO
    {
        public long? BlogId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsVisible { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }
        //public string PostedForName { get; set; }
        public DateTime? ValidFromDate { get; set; }
        public DateTime? ValidTillDate { get; set; }
        public long? VisitorCount { get; set; }
    }
}





