﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class CountryDTO
    {
        public CountryDTO()
        {
            State = new HashSet<StateDTO>();
        }

        public long CountryId { get; set; }
        public string CountryName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public ICollection<StateDTO> State { get; set; }
    }
}
