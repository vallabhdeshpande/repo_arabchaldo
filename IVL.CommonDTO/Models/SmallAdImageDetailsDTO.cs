﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    class SmallAdImageDetailsDTO
    {
        public long SmallAdImageDetailId { get; set; }
        public string SmallAdImageUrl { get; set; }
        public long SmallAdId { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public SmallAdDetailsDTO SmallAd { get; set; }
    }

   
}
