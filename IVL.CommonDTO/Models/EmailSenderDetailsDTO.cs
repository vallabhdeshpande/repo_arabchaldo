﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class EmailSenderDetailsDTO
    {
        internal string disclaimertemplate;

        public long Id { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public long adId { get; set; }
        public string title { get; set; }
        public long stateId { get; set; }
        public long cityId { get; set; }
        public string stateName { get; set; }
       public string cityName { get; set; }
        public string categoryName { get; set; }
        public string subCategoryName { get; set; }
        public string city { get; set; }
        public string reason { get; set; }
        public string comments { get; set; }
        public string emailUser { get; set; }
        public string remark { get; set; }
        public string subject { get; set; }
        public string mailfor { get; set; }
        public string description { get; set; }
        public string businessName { get; set; }
        public string businessPurpose { get; set; }
        public string confirmationEmailUrl { get; set; }
        public string mailConfirmationUrl { get; set; }
        public string smtp { get; set; }
        public string domain { get; set; }
        public int port { get; set; }
        public string fromMailId { get; set; }
        public string password { get; set; }
        public string registrationMailTemplate { get; set; }
        public string UserMailTemplate { get; set; }
        public string jobsMailTemplate { get; set; }
        public string multipleMailTemplate { get; set; }
        public string disclaimerTemplate { get; set; }
        public string BuyPlanMailTemplate { get; set; }
        public string contactMailTemplate { get; set; }
        public string feedbackMailTemplate { get; set; }
        public string resetPasswordMailTemplate { get; set; }
        public string changePasswordMailTemplate { get; set; }
        public string forgotPasswordMailTemplate { get; set; }
        public string fileUrl { get; set; }
        public string activationCode { get; set; }
        public string content { get; set; }
        public string toUserMessage { get; set; }


        //For Online Payment Super Admin

        public string address1 { get; set; }
        public string address2 { get; set; }
        public string zipCode { get; set; }
        public string receiptNo { get; set; }
    }
}




