﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class RolePermissionsDTO
    {

        public long RolePermissionId { get; set; }
        public long RoleId { get; set; }
        public long PermissionId { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public PermissionsDTO Permission { get; set; }
        public RolesDTO Role { get; set; }
    }
}
