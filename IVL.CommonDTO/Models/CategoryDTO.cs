﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public  class CategoryDTO
    {
        public CategoryDTO()
        {
            AdDetails = new HashSet<AdDetailsDTO>();
            SubCategory = new HashSet<SubCategoryDTO>();
        }

        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string ArabicName { get; set; }
        public string Description { get; set; }
        public string CategoryImageUrl { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public ICollection<AdDetailsDTO> AdDetails { get; set; }
        public ICollection<SubCategoryDTO> SubCategory { get; set; }
    }
}