﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class PermissionsDTO
    {
        public PermissionsDTO()
        {
            RolePermissions = new HashSet<RolePermissionsDTO>();
        }

        public long PermissionId { get; set; }
        public string PermissionName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public ICollection<RolePermissionsDTO> RolePermissions { get; set; }
    }
}
