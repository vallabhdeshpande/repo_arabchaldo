﻿using System;
using System.Collections.Generic;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class AdStatusDetailsDTO
    {

        public AdStatusDetailsDTO()
        {

        }
        public long AdStatusDetailsId { get; set; }
        public long AdId { get; set; }
        public long WorkflowStatusId { get; set; }
        public string Remark { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public AdDetailsDTO Ad { get; set; }
        public WorkflowStatusDTO WorkflowStatus { get; set; }
    }
}
