﻿using System;
using System.Collections.Generic;
using System.Text;
namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class LoginDetailsDTO
    {
        public long? LoginDetailId { get; set; }
        public long? UserId { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public UserDetailsDTO User { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }
        public byte Gender { get; set; }
        public string ActivationCode { get; set; }
        public bool? IsActivated { get; set; }
    }
}
