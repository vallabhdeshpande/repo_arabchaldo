﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class PaymentPlanDTO
    {
        public long? PlanId { get; set; }
        public string PlanName { get; set; }
        public long UserId { get; set; }
        public long? AdImageCount { get; set; }
        public decimal? AdImagePrice { get; set; }
        public long? BannerImageCount { get; set; }
        public decimal? BannerImagePrice { get; set; }
        public long? EventImageCount { get; set; }
        public decimal? EventImagePrice { get; set; }
        public long? LogoCount { get; set; }
        public decimal? LogoPrice { get; set; }
        public long? JobsAdvCount { get; set; }
        public decimal? JobsAdvPrice { get; set; }
        public long? SmallAdsCount { get; set; }
        public decimal? SmallAdsPrice { get; set; }
        public long? BuySellCount { get; set; }
        public decimal? BuySellPrice { get; set; }
        public decimal? MiscellaneiousTotal { get; set; }
        public string Description { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool IsActive { get; set; }
        public bool? IsPaid { get; set; }
        public bool? IsVisible { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTill { get; set; }
        public decimal Amount { get; set; }
        public decimal? Discount { get; set; }
        public decimal FinalAmount { get; set; }        


        public string Name { get; set; }
        public long TransactionId { get; set; }
        public string PaymentMode { get; set; }
        public string StripeTokenId { get; set; }
        public string ReceiptId { get; set; }
        public DateTime? TransactionDate { get; set; }
        //public decimal Amout { get; set; }
        public string StripeEmail { get; set; }
        public string StripeCreated { get; set; }
        public string StripeType { get; set; }
        public string StripecvcCheck { get; set; }
        public string StripeName { get; set; }
        public string StripeFunding { get; set; }
        public string StripeClientIp { get; set; }
        public string StripeBrand { get; set; }
        public string StripeCountry { get; set; }


        //   public string created { get; set; }

        //    public transactionId: number;
        //public planId :number ; 

        //public TransactionDate: Date;
        //public Amount :number ; 
        //public Name :string; 
        //public userId :number ; 

        //public paymentMode :string; 
        //public tokenId :string; 
        //public receiptId:string;
        //public brand :string; 
        //public country:string; 
        //public cvc_check :string; 
        //public funding :string; 
        //public client_ip :string; 
        //public created :string; 
        //public stripeemail :string; 
        //public type :string;
    }
}
