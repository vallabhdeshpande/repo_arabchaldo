﻿using IVL.ArabChaldo.CommonDTO.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
   public class UserDetailsDTO
    {
        public UserDetailsDTO()
        {
            AdDetails = new HashSet<AdDetailsDTO>();
            LoginDetails = new HashSet<LoginDetailsDTO>();
            UserRoles = new HashSet<UserRolesDTO>();
        }

        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public byte Gender { get; set; }
        public string ProfilePicUrl { get; set; }
        public string ContactNumber { get; set; }
        public string AlternateContactNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Email { get; set; }
        public string AddressStreet1 { get; set; }
        public string AddressStreet2 { get; set; }
        public long CountryId { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public long StateId { get; set; }
        public long CityId { get; set; }
        public string RoleName { get; set; }
        public string ZipCode { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }
        public string Description { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public string CountryCodeContact { get; set; }
        public string ActivationUrl { get; set; }

        public string ConfirmationToken { get; set; }
        public CityDTO City { get; set; }
        public bool? IsSocialMedia { get; set; }

        public string Provider { get; set; }
        public ICollection<AdDetailsDTO> AdDetails { get; set; }
        public ICollection<LoginDetailsDTO> LoginDetails { get; set; }
        public ICollection<UserRolesDTO> UserRoles { get; set; }
    }
}
