﻿using IVL.ArabChaldo.CommonDTO.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    public class StateDTO
    {
        public StateDTO()
        {
            City = new HashSet<CityDTO>();
        }

        public long StateId { get; set; }
        public string StateName { get; set; }
        public string Description { get; set; }
        public long CountryId { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }

        public CountryDTO Country { get; set; }
        public ICollection<CityDTO> City { get; set; }

    }
}