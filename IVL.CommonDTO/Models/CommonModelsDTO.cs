﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVL.ArabChaldo.CommonDTO.Models
{
    class CommonModelsDTO
    {
    }

    public class StateDdlDTO
    {
        public long StateId { get; set; }
        public string StateName { get; set; }
    }
}
