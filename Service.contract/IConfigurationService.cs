﻿using AutoMapper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Configuration = IVL.ArabChaldo.CommonDTO.Models.ConfigurationDTO;

namespace Service.contract
{
    public interface IConfigurationService
    {
        bool DeleteConfig(int ID);

        bool UpdateConfig(int ID, Configuration Configdetails);

        List<Configuration> GetAllConfigDetails();
    }
}
