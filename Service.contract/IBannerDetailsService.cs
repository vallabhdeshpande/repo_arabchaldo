﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;

namespace Service.contract
{
    public interface IBannerDetailsService
    {
        bool Insert(BannerDetailsDTO bannerDetails);
        bool Update(long id,BannerDetailsDTO bannerDetails);
        bool Delete(long bannerId);

        bool BannerStatus(long bannerId, string remarks, string Action,long CreatedBy);
        BannerDetailsDTO GetBannerById(long id);

        DataTable GetAllBanner(int id);
        DataTable FillBannerList();
    }
}
