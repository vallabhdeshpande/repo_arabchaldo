﻿using IVL.ArabChaldo.CommonDTO.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Service.contract
{
   public interface IWebsiteDetailsService
    {
        AdDetailsDTO GetAdDetailsById(long AdId);
        DataTable GetAllAdDetails(string imgurl);

        DataTable GetAllJobDetails(string imgurl);
        List<AdDetailsDTO> GetAdsAdListBycategoryId(long categoryId);
        List<AdDetailsDTO> GetAdsAdListBySubcategoryId(long SubcategoryId);
        BannerDetailsDTO GetBannerById(long id);
        DataTable GetAllBanner(string imgurl); 
            
       DataTable getPremiumBannerList(string imgurl);
        BlogDetailsDTO GetBlogById(long id);
        DataTable GetAllBlog();
        Boolean SendEmail(EmailSenderDetailsDTO EmailDetails);
        Boolean SendAcknowledgement(EmailSenderDetailsDTO EmailDetails);
        EventDetailsDTO GetEventById(long id);
        List<EventDetailsDTO> GetAllEvent();
        DataTable GetEventsByCategoryId(long categoryId);
        DataTable GetAllCategoryData();
        DataTable GetAllCityData();
        DataTable GetAdListBySearchCriteria(string searchCriteria);

        DataTable GetAboutUSData();
        DataTable GetContactUSData();
        DataTable GetTermsofUseData();
        DataTable GetPrivacyPolicyData();
        DataTable GetFAQData();
        DataTable GetDisclaimerData();

        DataTable GetAllBuysellData(string imgurl); 
       DataTable GetAllBuysellRecentData(string imgurl);
        BuySellDetailsDTO GetBuysellById(long id);
        DataTable GetAllSmallAdDetails(string imgurl);
        SmallAdDetailsDTO GetSmallAdDetailsById(long smalladId);
        DataTable GetCategorySearchDDL();
        DataTable GetCitySearchDDL();
    }
}
