﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Service.contract
{
   public interface IDashBoardService
    {
        DataTable GetotalCount();
        DataTable GetBargraphdata(long year);

        DataTable GeBUtotalCount(long UserId);
    }
}
