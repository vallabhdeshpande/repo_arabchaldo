﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;


namespace Service.contract
{
    public interface ICountryService
    {
        bool InsertCountry(CountryDTO Country);
        bool CheckCountryExits(string CountryName);
        CountryDTO GetCountryById(int CountryID);
        bool DeleteCountry(int CountryID);

        bool UpdateCountry(int CountryID, CountryDTO Country);

        List<CountryDTO> GetAllCountry();

    }
}