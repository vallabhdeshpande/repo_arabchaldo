﻿using IVL.ArabChaldo.CommonDTO.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Service.contract
{
  public  interface IBuySellDetailsService
    {
        bool InsertBuySellDetails(BuySellDetailsDTO buysellDetails);
        DataTable GetAllBuySellDetails(long loggedInUserId, string loggedInRole);

        BuySellDetailsDTO GetbuySellDetailsById(long buysellId);

        bool UpdatebuySellDetails(long buySellId, BuySellDetailsDTO buysellDetails);

        bool buySellAdStatus(long buySellId, string remarks, string Action, long CreatedBy);

        List<BuySellDetailsDTO> GetAllBuySellsDetails(long loggedInUserId, string loggedInRole);


    }
}
