﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;

namespace Service.contract
{
    public interface IEventDetailsService
    {
        bool Insert(EventDetailsDTO eventDetails);
        bool Update(long id, EventDetailsDTO eventDetails);
        bool Delete(long eventId);

        bool EventStatus(long EventId, string remarks, string Action,long CreatedBy);
        EventDetailsDTO GetEventById(long id);

        DataTable GetAllEvent(long loggedInUserId, string loggedInRole);
        List<EventDetailsDTO> GetEventsByCategoryId(long categoryId);
    }
}
