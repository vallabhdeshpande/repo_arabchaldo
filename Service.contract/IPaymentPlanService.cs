﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using System.Data;

namespace Service.contract
{
    public interface IPaymentPlanService
    {
        string InsertPlan(PaymentPlanDTO paymentDetails);
        void UpdatePlanDetails(long id, PaymentPlanDTO paymentPlan);
        bool Delete(long planId);

        //bool BannerStatus(int AdId, string remarks, string Action);
        PaymentPlanDTO GetPlanId(int planId);

        DataTable GetAllPlan();
        List<PaymentPlanDTO> GetPlanListForUser(int userId, string location);
        string StripePaymentConfirmation(string tokenResoponse);
        bool OfflinePaymentConfirmation(string OfflineResponse);
    }
}
