﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
namespace Service.contract
{
    public interface IBlogDetailsService

    {

        bool Insert(BlogDetailsDTO blogDetails);
        bool Update(long id, BlogDetailsDTO blogDetails);
        bool Delete(long blogId);

        bool BlogStatus(long BlogId, string remarks, string Action, long CreatedBy);
        BlogDetailsDTO GetBlogById(long id);

        DataTable GetAllBlog(long loggedInUserId, string loggedInRole);
    }
}





