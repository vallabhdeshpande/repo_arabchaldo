﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;


namespace Service.contract
{
    public interface IAdDetailsService
    {
        bool InsertAdDetails(AdDetailsDTO AdDetails);
        bool CheckAdDetailsExits(string Title, long CategoryId,long AdId);
        AdDetailsDTO GetAdDetailsById(long AdId);
        bool AdStatus(long AdId,string remarks,string Action, long CreatedBy);

        bool UpdateAdDetails(long AdId, AdDetailsDTO AdDetails);

        List<AdDetailsDTO> GetAllAdDetails(long loggedInUserId, string loggedInRole);

        List<AdDetailsDTO> GetAllJobDetails(long loggedInUserId, string loggedInRole);
        List<AdDetailsDTO> GetAdsAdListBycategoryId(long categoryId);
        List<AdDetailsDTO> GetAdsAdListBySubcategoryId(long SubcategoryId);
        DataTable GetAllAdsDetails(long loggedInUserId, string loggedInRole, string categoryName);
    }
}