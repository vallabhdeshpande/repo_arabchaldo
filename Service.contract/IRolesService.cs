﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
namespace Service.contract
{
    public interface IRolesService
    {
        List<RolesDTO> GetAllUserRole();
    }
}
