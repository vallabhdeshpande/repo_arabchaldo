﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;

namespace Service.contract
{
    public interface ICommonService
    {
        DataTable FillCategoryDDL();
        DataTable FillSubCategoryDDLByCategoryId(long CategoryId);
        DataTable FillCountryDDL();
        DataTable FillStateDDLByCountryId(long CountryId);
        DataTable FillCityDDLByStateId(long StateId);

        DataTable FillUserByRoleDDL(long RoleId);
        DataTable FillRoleDDL(string roleName);    
        DataTable FillUserDDL();
        DataTable FillPopularCategory();
        DataTable FillPopularEventsCategory();

        DataTable FillPopularJobSubCategory();
        DataTable DisplayWorkFlow(long Id, long PostTypeId);
        DataTable JobCategoryDDL();
        DataTable FillBUCityDDLByStateId(long stateId);
    }
}
