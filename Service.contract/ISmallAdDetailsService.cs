﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using IVL.ArabChaldo.CommonDTO.Models;
namespace Service.contract
{
    public interface ISmallAdDetailsService
    {
        bool InsertSmallAdDetails(SmallAdDetailsDTO SmallAdDetails);
        bool CheckSmallAdDetailsExits(string Title, long SmallAdId);
        SmallAdDetailsDTO GetSmallAdDetailsById(long SmallAdId);
        bool SmallAdStatus(long SmallAdId, string remarks, string Action, long CreatedBy);

        bool UpdateSmallAdDetails(long SmallAdId, SmallAdDetailsDTO SmallAdDetails);

        List<SmallAdDetailsDTO> GetAllSmallAdDetails(long loggedInUserId, string loggedInRole);
       
        DataTable GetAllSmallAdsDetails(long loggedInUserId, string loggedInRole);
    }
}
