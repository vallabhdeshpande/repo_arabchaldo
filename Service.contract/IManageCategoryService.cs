﻿using IVL.ArabChaldo.CommonDTO.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Service.contract
{
    public interface IManageCategoryService
    {
        bool InsertCategory(CategoryDTO catgorydetails);
        bool CheckCategoryExits(string CategoryName);
        CategoryDTO GetCategorybyId(int CategoryID);
        bool DeleteCategory(int CategoryID);

        bool UpdateCategory(int CategoryID, CategoryDTO Categorydetails);

        List<CategoryDTO> GetAllCategory();
      
        DataTable FillCategoryList();

    }
}
