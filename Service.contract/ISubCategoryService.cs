﻿using IVL.ArabChaldo.CommonDTO.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Service.contract
{
   public interface ISubCategoryService
    {
        bool InsertSubCategory(SubCategoryDTO Subcatgorydetails);
        bool CheckSubCategoryExits(string subCategoryName, long categoryId);
        SubCategoryDTO GetSubCategorybyId(int SubCategoryID);
        bool DeleteSubCategory(int SubCategoryID);

        bool UpdateSubCategory(int SubCategoryID, SubCategoryDTO Subcatgorydetails);

        List<SubCategoryDTO> GetAllSubCategory();
        DataTable FillSubcategoryList();
    }
}
