﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;

namespace Service.contract
{
    public interface IMessageBoardService
    {
        bool InsertMessageBoard(MessageBoardDTO messageBoard);
        bool CheckMessageBoardExits(string Name, long CategoryId);
        MessageBoardDTO GetMessageBoardById(long messageBoardId);
        bool DeleteMessageBoard(long messageBoardId);

 
       

        bool UpdateMessageBoard(long MessageBoardID, MessageBoardDTO messageBoard);

        DataTable GetAllMessageBoard();
        DataTable FillMessageBoardList();



        // Message Board Details
        List<MessageBoardDetailsDTO> GetMessageBoardReplyByMessageBoardId(long messageBoardId);
        bool DeleteMessageBoardDetails(long messageBoardId);
        bool InsertMessageBoardDetails(MessageBoardDetailsDTO messageBoardDetails);

        DataTable FillMessageBoardReplyList(long MessageBoardId);

    }
}
