﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;


namespace Service.contract
{
    public interface ICityService
    {

        bool InsertCity(CityDTO City);
        bool CheckCityExits(string CityName,long StateId, long CityId);
        CityDTO GetCityById(int CityID);
        bool DeleteCity(int CityID);

        bool UpdateCity(int CityID, CityDTO City);

        List<CityDTO> GetAllCity();
        List<CityDTO> GetCityWithPagination(string paginationFilter);
        DataTable FillCityList();
    }
}