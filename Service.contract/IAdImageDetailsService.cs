﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;


namespace Service.contract
{
    public interface IAdImageDetailsService
    {
        bool InsertAdImageDetails(AdImageDetailsDTO AdImageDetails);
        bool CheckAdImageDetailsExits(string AdImageDetailsName);
        AdImageDetailsDTO GetAdImageDetailsById(int AdImageDetailsID);
        bool DeleteAdImageDetails(int AdImageDetailsID);

        bool UpdateAdImageDetails(int AdImageDetailsID, AdImageDetailsDTO AdImageDetails);

        List<AdImageDetailsDTO> GetAllAdImageDetails();

    }
}