﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;


namespace Service.contract
{
    public interface IStateService
    {

        bool InsertState(StateDTO State);
        bool CheckStateExits(string StateName,long StateId);
        StateDTO GetStateById(int StateID);
        bool DeleteState(int StateID);

        bool UpdateState(int StateID, StateDTO State);

        List<StateDTO> GetAllState();

        DataTable FillStateList();

    }
}