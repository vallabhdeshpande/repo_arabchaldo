﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
namespace Service.contract
{
    public interface ILoginService
    {

        LoginDetailsDTO GetLogin(string UserName,string Password);
        string GetLoginInfo(string UserName, string Password);
        LoginDetailsDTO GetLoginByUserID(long userId);
        bool ResetPassword(int ID, LoginDetailsDTO loginDetails);
        bool ActivateUserAccount(string info);
        DataTable GetUserInfoByEmail(string email);
    }
}
