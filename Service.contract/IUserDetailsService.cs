﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;


namespace Service.contract
{
   public interface IUserDetailsService
    {
        bool InsertUser(UserDetailsDTO userdetails);
        bool CheckUserExits(string EmailID);
        UserDetailsDTO GetUserbyId(long ID);
        bool DeleteUser(long ID);

        bool UpdateUsers(long ID, UserDetailsDTO userdetails); 

        DataTable GetAllUser();

        string SocialloginInfo(UserDetailsDTO userDetails);
    }
}
