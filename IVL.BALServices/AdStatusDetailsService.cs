﻿using Service.contract;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;

namespace Service.Implement.Implement
{
    public class AdStatusDetailsService : IAdStatusDetailsService, IDisposable
    {

        AdStatusDetailsBAL AdStatusDetailsBal = new AdStatusDetailsBAL();

        public AdStatusDetailsService()
        {

        }

        public AdStatusDetailsService(AdStatusDetailsBAL AdStatusDetailsBal)
        {
            this.AdStatusDetailsBal = AdStatusDetailsBal;
        }

        public bool CheckAdStatusDetailsExits(long AdStatusDetailsName)
        {
            //return AdStatusDetailsBal.CheckAdStatusDetailsExits(AdStatusDetailsName);
            return false;
        }

        public bool DeleteAdStatusDetails(int AdStatusDetailsID)
        {
            return AdStatusDetailsBal.DeleteAdStatusDetails(AdStatusDetailsID);
        }
        public bool UpdateAdStatusDetails(int AdStatusDetailsID, AdStatusDetailsDTO AdStatusDetails)
        {
            return AdStatusDetailsBal.UpdateAdStatusDetails(AdStatusDetailsID, AdStatusDetails);
        }
        public List<AdStatusDetailsDTO> GetAllAdStatusDetails()
        {
            return AdStatusDetailsBal.GetAllAdStatusDetails();
        }

        //public AdStatusDetailsDTO GetAdStatusDetailsById(int AdStatusDetailsID)
        //{
        //    return AdStatusDetailsBal.GetAdStatusDetailsById(AdStatusDetailsID);
        //}

        public bool InsertAdStatusDetails(AdStatusDetailsDTO AdStatusDetails)
        {
            return AdStatusDetailsBal.InsertAdStatusDetails(AdStatusDetails);
        }
        public void Dispose()
        {
            if (AdStatusDetailsBal != null)
            {
                this.AdStatusDetailsBal.Dispose();
            }
        }
    }
}
