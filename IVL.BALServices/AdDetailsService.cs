﻿using Service.contract;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using System.Data;

namespace Service.Implement.Implement
{
    public class AdDetailsService : IAdDetailsService, IDisposable
    {

        AdDetailsBAL AdDetailsBal = new AdDetailsBAL();

        public AdDetailsService()
        {

        }

        public AdDetailsService(AdDetailsBAL AdDetailsBal)
        {
            this.AdDetailsBal = AdDetailsBal;
        }

        public bool CheckAdDetailsExits(string Title, long CategoryId, long AdId)
        {
            return AdDetailsBal.CheckAdDetailsExits(Title, CategoryId,AdId);
        }

        public bool AdStatus(long AdId,string remarks,string Action, long CreatedBy)
        {
            return AdDetailsBal.AdStatus(AdId, remarks,Action, CreatedBy);
        }
        public bool UpdateAdDetails(long AdId, AdDetailsDTO AdDetails)
        {
            return AdDetailsBal.UpdateAdDetails(AdId, AdDetails);
        }
        public List<AdDetailsDTO> GetAllAdDetails(long loggedInUserId, string loggedInRole)
        {
            return AdDetailsBal.GetAllAdDetails(loggedInUserId, loggedInRole);
        }

        public DataTable GetAllAdsDetails(long loggedInUserId, string loggedInRole, string categoryName)
        {
            return AdDetailsBal.GetAllAdsDetails(loggedInUserId, loggedInRole, categoryName);
        }
        public List<AdDetailsDTO> GetAllJobDetails(long loggedInUserId, string loggedInRole)
        {
            return AdDetailsBal.GetAllJobDetails(loggedInUserId, loggedInRole);
        }
        public List<AdDetailsDTO> GetAdsAdListBycategoryId(long categoryId)
        {
            return AdDetailsBal.GetAdsAdListBycategoryId(categoryId);
        }
        public List<AdDetailsDTO> GetAdsAdListBySubcategoryId(long SubcategoryId)
        {
            return AdDetailsBal.GetAdsAdListBySubcategoryId(SubcategoryId);
        }
        public AdDetailsDTO GetAdDetailsById(long AdId)
        {
            return AdDetailsBal.GetAdDetailsById(AdId);
        }

        public bool InsertAdDetails(AdDetailsDTO AdDetails)
        {
            return AdDetailsBal.InsertAdDetails(AdDetails);

        }
        public void Dispose()
        {
            if (AdDetailsBal != null)
            {
                this.AdDetailsBal.Dispose();
            }
        }
    }
}
