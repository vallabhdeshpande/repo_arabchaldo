﻿using Service.contract;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using System.Data;

namespace Service.Implement
{
    public class BuySellDetailsService : IBuySellDetailsService, IDisposable
    {
        BuySellDetailsBAL BuySellDetailsBal = new BuySellDetailsBAL();

        public BuySellDetailsService()
        {

        }

        public BuySellDetailsService(BuySellDetailsBAL BuySellDetailsBal)
        {
            this.BuySellDetailsBal = BuySellDetailsBal;
        }
        public DataTable GetAllBuySellDetails(long loggedInUserId, string loggedInRole)
        {
            return BuySellDetailsBal.GetAllBuySellDetails(loggedInUserId, loggedInRole);
        }
        public bool InsertBuySellDetails(BuySellDetailsDTO buysellDetails)
        {
            return BuySellDetailsBal.InsertbuysellDetails(buysellDetails);

        }
        public BuySellDetailsDTO GetbuySellDetailsById(long buysellId)
        {
            return BuySellDetailsBal.GetbuySellDetailsById(buysellId);
        }
        public bool UpdatebuySellDetails(long buySellId, BuySellDetailsDTO buysellDetails)
        {
            return BuySellDetailsBal.UpdatebuySellDetails(buySellId, buysellDetails);
        }
        public bool buySellAdStatus(long buySellId, string remarks, string Action, long CreatedBy)
        {
            return BuySellDetailsBal.buySellAdStatus(buySellId, remarks, Action, CreatedBy);
        }

        public List<BuySellDetailsDTO> GetAllBuySellsDetails(long loggedInUserId, string loggedInRole)
        {
            return BuySellDetailsBal.GetAllBuySellsDetails(loggedInUserId, loggedInRole);
        }
        public void Dispose()
        {
            if (BuySellDetailsBal != null)
            {
                this.BuySellDetailsBal.Dispose();
            }
        }
    }

}
