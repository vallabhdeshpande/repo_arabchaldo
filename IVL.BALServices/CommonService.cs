﻿using Service.contract;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using System.Data;

namespace Service.Implement
{
    public class CommonService: ICommonService, IDisposable
    {
        CommonBAL CommonBal = new CommonBAL();
        public CommonService()
        {

        }

        public CommonService(CommonBAL CommonBal)
        {
            this.CommonBal = CommonBal;
        }

        public DataTable FillRoleDDL(string roleName)
        {
            return CommonBal.FillRoleDDL(roleName);
        }

        public DataTable FillUserDDL()
        {
            return CommonBal.FillUserDDL();
        }
        public DataTable FillCountryDDL()
        {
            return CommonBal.FillCountryDDL();
        }
        public DataTable FillStateDDLByCountryId(long CountryId)
        {
            return CommonBal.FillStateDDLByCountryId(CountryId);
        }

       
        public DataTable FillCityDDLByStateId(long StateId)
        {
            return CommonBal.FillCityDDLByStateId(StateId);
        }

        public DataTable FillBUCityDDLByStateId(long StateId)
        {
            return CommonBal.FillBUCityDDLByStateId(StateId);
        }

        public DataTable FillCategoryDDL()
        {
            return CommonBal.FillCategoryDDL();
        }
        public DataTable FillSubCategoryDDLByCategoryId(long CategoryId)
        {
            return CommonBal.FillSubCategoryDDLByCategoryId(CategoryId);
        }

        public DataTable FillUserByRoleDDL(long RoleId)
        {
            return CommonBal.FillUserByRoleDDL(RoleId);
        }

        public DataTable DisplayWorkFlow(long Id, long PostTypeId)
        {
            return CommonBal.DisplayWorkFlow(Id, PostTypeId);
        }
        public DataTable FillPopularCategory()
        {
            return CommonBal.FillPopularCategory();
        }
        public DataTable FillPopularEventsCategory()
        {
            return CommonBal.FillPopularEventsCategory();
        }

        public DataTable FillPopularJobSubCategory()
        {
            return CommonBal.FillPopularJobSubCategory();
        }
        public DataTable JobCategoryDDL()
        {
            return CommonBal.JobCategoryDDL();
        }
        public void Dispose()
        {
            if (CommonBal != null)
            {
                this.CommonBal.Dispose();
            }
        }

        #region DropDowns

        #endregion


    }
}
