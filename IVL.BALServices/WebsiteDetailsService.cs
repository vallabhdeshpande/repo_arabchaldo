﻿using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.contract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Service.Implement.Implement
{
    public class WebsiteDetailsService : IWebsiteDetailsService, IDisposable
    {
        WebsiteDetailsBAL WebsiteDetailsBal = new WebsiteDetailsBAL();

        public WebsiteDetailsService()
        {

        }
        public WebsiteDetailsService(WebsiteDetailsBAL WebsiteDetailsBal)
        {
            this.WebsiteDetailsBal = WebsiteDetailsBal;
        }
        public DataTable GetAllAdDetails(string imgurl)
        {
            return WebsiteDetailsBal.GetAllAdDetails(imgurl);
        }
        public DataTable GetAllJobDetails(string imgurl)
        {
            return WebsiteDetailsBal.GetAllJobDetails(imgurl);
        }
        public List<AdDetailsDTO> GetAdsAdListBycategoryId(long categoryId)
        {
            return WebsiteDetailsBal.GetAdsAdListBycategoryId(categoryId);
        }
        public List<AdDetailsDTO> GetAdsAdListBySubcategoryId(long SubcategoryId)
        {
            return WebsiteDetailsBal.GetAdsAdListBySubcategoryId(SubcategoryId);
        }
        public AdDetailsDTO GetAdDetailsById(long AdId)
        {
            return WebsiteDetailsBal.GetAdDetailsById(AdId);
        }
        public BannerDetailsDTO GetBannerById(long id)
        {
            return this.WebsiteDetailsBal.GetBannerById(id);
        }
        public DataTable GetAllBanner(string imgurl) 
        {
            return this.WebsiteDetailsBal.GetAllBanner(imgurl);
        }
        public DataTable getPremiumBannerList(string imgurl) 
        {
            return this.WebsiteDetailsBal.getPremiumBannerList(imgurl);
        }
    public DataTable GetAllBlog()
        {
            return this.WebsiteDetailsBal.GetAllBlog();
        }

        public BlogDetailsDTO GetBlogById(long id)
        {
            return this.WebsiteDetailsBal.GetBlogById(id);
        }
        public EventDetailsDTO GetEventById(long id)
        {
            return this.WebsiteDetailsBal.GetEventById(id);
        }
        public DataTable GetEventsByCategoryId(long cateoryId)
        {
            return this.WebsiteDetailsBal.GetEventsByCategoryId(cateoryId);
        }
        public List<EventDetailsDTO> GetAllEvent()
        {
            return this.WebsiteDetailsBal.GetAllEvent();
        }

        public bool SendEmail(EmailSenderDetailsDTO EmailDetails)
        {
            return this.WebsiteDetailsBal.SendEmail(EmailDetails);
        }

        public bool SendAcknowledgement(EmailSenderDetailsDTO EmailDetails)
        {
            return this.WebsiteDetailsBal.SendAcknowledgement(EmailDetails);
        }

        public DataTable GetAllCityData()
        {
            return this.WebsiteDetailsBal.GetAllCityData();
        }

        public DataTable GetAllCategoryData()
        {
            return this.WebsiteDetailsBal.GetAllCategoryData();
        }

        public DataTable GetAdListBySearchCriteria(string searchCriteria)
        {
            return this.WebsiteDetailsBal.GetAdListBySearchCriteria(searchCriteria);
        }

        public DataTable GetAboutUSData()
        {
            return this.WebsiteDetailsBal.GetAboutUSData();
        }
        public DataTable GetContactUSData()
        {
            return this.WebsiteDetailsBal.GetContactUSData();
        }
        public DataTable GetPrivacyPolicyData()
        {
            return this.WebsiteDetailsBal.GetPrivacyPolicyData();
        }
        public DataTable GetTermsofUseData()
        {
            return this.WebsiteDetailsBal.GetTermsofUseData();
        }
        public DataTable GetFAQData()
        {
            return this.WebsiteDetailsBal.GetFAQData();
        }
        public DataTable GetDisclaimerData()
        {
            return this.WebsiteDetailsBal.GetDisclaimerData();
        }
        public DataTable GetAllBuysellData(string imgurl)
        {
            return this.WebsiteDetailsBal.GetAllBuySellData(imgurl);
        }
        public DataTable GetAllBuysellRecentData(string imgurl)
        {
            return this.WebsiteDetailsBal.GetAllBuySellRecentData(imgurl);
        }
        public BuySellDetailsDTO GetBuysellById(long id)
        {
            return this.WebsiteDetailsBal.GetBuysellById(id);
        }


        public DataTable GetAllSmallAdDetails(string imgurl)
        {
            return WebsiteDetailsBal.GetAllSmallAdDetails(imgurl);
        }
        
        public SmallAdDetailsDTO GetSmallAdDetailsById(long SmallAdId)
        {
            return WebsiteDetailsBal.GetSmallAdDetailsById(SmallAdId);
        }

        public DataTable GetCategorySearchDDL()
        {
            return this.WebsiteDetailsBal.GetCategorySearchDDL();
        }

        public DataTable GetCitySearchDDL()
        {
            return this.WebsiteDetailsBal.GetCitySearchDDL();
        }
        public void Dispose()
        {
            if (WebsiteDetailsBal != null)
            {
                this.WebsiteDetailsBal.Dispose();
            }
        }
    }
}
