﻿using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.DAL.Interfaces;
using Service.contract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Service.Implement
{
    public class DashBoardService: IDashBoardService, IDisposable
    {
        DashboardBAL dashboardBAL = new DashboardBAL();

        public DataTable GetotalCount()
        {
            return dashboardBAL.GetotalCount();
        }

        public DataTable GetBargraphdata(long year)
        {
            return dashboardBAL.GetBargraphdata(year);
        }
        public DataTable GeBUtotalCount(long UserId)
        {
            return dashboardBAL.GeBUtotalCount(UserId);
        }
        public void Dispose()
        {
            if (dashboardBAL != null)
            {
                this.dashboardBAL.Dispose();
            }
        }
    }
}
