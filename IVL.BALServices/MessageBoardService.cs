﻿using Service.contract;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using System.Data;
namespace Service.Implement.Implement
{
    public class MessageBoardService : IMessageBoardService, IDisposable
    {
        MessageBoardBAL MessageBoardBal = new MessageBoardBAL();

        public MessageBoardService()
        {

        }

        public MessageBoardService(MessageBoardBAL MessageBoardBal)
        {
            this.MessageBoardBal = MessageBoardBal;
        }

        public bool CheckMessageBoardExits(string Name,long CategoryId)
        {
            return MessageBoardBal.CheckMessageBoardExits(Name, CategoryId);
        }

        public bool DeleteMessageBoard(long MessageBoardID)
        {
            return MessageBoardBal.DeleteMessageBoard(MessageBoardID);
        }

        public bool DeleteMessageBoardDetails(long MessageBoardID)
        {
            return MessageBoardBal.DeleteMessageBoardDetails(MessageBoardID);
        }

        public bool UpdateMessageBoard(long MessageBoardID, MessageBoardDTO MessageBoard)
        {
            return MessageBoardBal.UpdateMessageBoard(MessageBoardID, MessageBoard);
        }
        public DataTable GetAllMessageBoard()
        {
            return MessageBoardBal.GetAllMessageBoard();
        }
        

        public MessageBoardDTO GetMessageBoardById(long MessageBoardID)
        {
            return MessageBoardBal.GetMessageBoardById(MessageBoardID);
        }

        
        public bool InsertMessageBoard(MessageBoardDTO MessageBoard)
        {
            return MessageBoardBal.InsertMessageBoard(MessageBoard);
        }
        public void Dispose()
        {
            if (MessageBoardBal != null)
            {
                this.MessageBoardBal.Dispose();
            }
        }

        public DataTable FillMessageBoardList()
        {
            return MessageBoardBal.FillMessageBoardList();
        }

        // Message Board Details
       public List<MessageBoardDetailsDTO> GetMessageBoardReplyByMessageBoardId(long messageBoardId)
        {
            return MessageBoardBal.GetMessageBoardReplyByMessageBoardId(messageBoardId);

        }
       public bool InsertMessageBoardDetails(MessageBoardDetailsDTO messageBoardDetails)
        {
            return MessageBoardBal.InsertMessageBoardDetails(messageBoardDetails);

        }
        public DataTable FillMessageBoardReplyList(long MessageBoardId)
        {
            return MessageBoardBal.FillMessageBoardReplyList(MessageBoardId);
        }
    }
}
