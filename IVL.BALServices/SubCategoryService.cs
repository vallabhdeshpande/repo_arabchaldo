﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.contract;

namespace Service.Implement.Implement
{
   public class SubCategoryService: ISubCategoryService, IDisposable
    {

    SubCategoryBAL SubCategoryBal = new SubCategoryBAL();

    public SubCategoryService()
    {

    }

    public SubCategoryService(SubCategoryBAL SubCategoryBal)
    {
        this.SubCategoryBal = SubCategoryBal;
    }

    public bool CheckSubCategoryExits(string SubCategoryName, long CategoryId)
    {
        return SubCategoryBal.CheckSubCategoryExits(SubCategoryName, CategoryId);
    }

    public bool DeleteSubCategory(int SubCategoryID)
    {
        return SubCategoryBal.DeleteSubCategory(SubCategoryID);
    }
    public bool UpdateSubCategory(int SubCategoryID, SubCategoryDTO SubCategoryDetails)
    {
        return SubCategoryBal.UpdateSubCategory(SubCategoryID, SubCategoryDetails);
    }
    public List<SubCategoryDTO> GetAllSubCategory()
    {
        return SubCategoryBal.GetAllSubCategory();
    }

    public SubCategoryDTO GetSubCategorybyId(int SubCategoryID)
    {
        return SubCategoryBal.GetSubCategoryById(SubCategoryID);
    }

    public bool InsertSubCategory(SubCategoryDTO SubCategoryDetails)
    {
        return SubCategoryBal.InsertSubCategory(SubCategoryDetails);
    }
    public void Dispose()
    {
        if (SubCategoryBal != null)
        {
            this.SubCategoryBal.Dispose();
        }
    }


        public DataTable FillSubcategoryList()
        {
            return SubCategoryBal.FillSubcategoryList();
        }
    }
}
