﻿using Service.contract;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using System.Data;

namespace Service.Implement.Implement
{
    public class CityService : ICityService, IDisposable
    {

        CityBAL CityBal = new CityBAL();

        public CityService()
        {

        }

        public CityService(CityBAL CityBal)
        {
            this.CityBal = CityBal;
        }

        public bool CheckCityExits(string CityName, long StateId,long CityId)
        {
            return CityBal.CheckCityExits(CityName, StateId,CityId);
        }

        public bool DeleteCity(int CityID)
        {
            return CityBal.DeleteCity(CityID);
        }
        public bool UpdateCity(int CityID, CityDTO City)
        {
            return CityBal.UpdateCity(CityID, City);
        }
        public List<CityDTO> GetAllCity()
        {
            return CityBal.GetAllCity();
        }
        public List<CityDTO> GetCityWithPagination(string paginationFilter)
        {
           return CityBal.GetCityWithPagination(paginationFilter);
            
        }

        public CityDTO GetCityById(int CityID)
        {
            return CityBal.GetCityById(CityID);
        }

        public bool InsertCity(CityDTO City)
        {
            return CityBal.InsertCity(City);
        }
        public void Dispose()
        {
            if (CityBal != null)
            {
                this.CityBal.Dispose();
            }
        }

        public DataTable FillCityList()
        {
            return CityBal.FillCityList();
        }
    }
}
