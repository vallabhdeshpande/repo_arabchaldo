﻿using System;
using System.Collections.Generic;
using System.Text;
using Service.contract;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using System.Data;

namespace Service.Implement
{
    public class EventDetailsService : IEventDetailsService, IDisposable
    {
        private readonly EventDetailsBAL _eventDetailsBAL = new EventDetailsBAL();

        public EventDetailsService()
        {

        }

        public EventDetailsService(EventDetailsBAL eventDetailsBAL)
        {
            this._eventDetailsBAL = eventDetailsBAL;
        }

        public DataTable GetAllEvent(long loggedInUserId, string loggedInRole)
        {
            return this._eventDetailsBAL.GetAllEvent(loggedInUserId,loggedInRole);
        }

        public bool Insert(EventDetailsDTO eventDetails)
        {
            return this._eventDetailsBAL.AddEvent(eventDetails);
        }

        public bool Update(long id, EventDetailsDTO eventDetails)
        {
            return this._eventDetailsBAL.UpdateEvent(id, eventDetails);
        }

        public bool Delete(long eventId)
        {
            return this._eventDetailsBAL.DeleteEvent(eventId);
        }

        public bool EventStatus(long EventId, string remarks, string Action, long CreatedBy)
        {
            return this._eventDetailsBAL.EventStatus(EventId, remarks, Action,  CreatedBy);
        }

        public EventDetailsDTO GetEventById(long id)
        {
            return this._eventDetailsBAL.GetEventById(id);
        }
        public List<EventDetailsDTO> GetEventsByCategoryId(long cateoryId)
        {
            return this._eventDetailsBAL.GetEventsByCategoryId(cateoryId);
        }
        public void Dispose()
        {
            if (_eventDetailsBAL != null)
            {
                this._eventDetailsBAL.Dispose();
            }
        }
    }
}
