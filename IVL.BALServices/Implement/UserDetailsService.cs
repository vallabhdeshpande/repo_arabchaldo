﻿using System;
using System.Collections.Generic;
using System.Text;
using Service.contract;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using System.Data;

namespace Service.Implement.Implement
{
  public  class UserDetailsService : IUserDetailsService, IDisposable
    {
        UserDetailsBal UserDetailsBal = new UserDetailsBal();

    public UserDetailsService()
    {

    }

    public UserDetailsService(UserDetailsBal UserDetailsBal)
    {
        this.UserDetailsBal = UserDetailsBal;
    }

    public bool CheckUserExits(string EmailID)
    {
            return UserDetailsBal.CheckUserExits(EmailID);
    }

    public bool DeleteUser(long ID)
    {
        return UserDetailsBal.DeleteUser(ID);
    }
        public bool UpdateUsers(long ID,UserDetailsDTO userdetails)
        {
            return UserDetailsBal.UpdateUsers(ID, userdetails);
        }
        public DataTable GetAllUser()
    {
            return UserDetailsBal.GetAllUser();
    }

    public UserDetailsDTO GetUserbyId(long id)
    {
            return UserDetailsBal.GeUserbyId(id);
    }

    public bool InsertUser(UserDetailsDTO UserDetails)
    {
        return UserDetailsBal.InsertUser(UserDetails);
    }
        public string SocialloginInfo(UserDetailsDTO UserDetails)
        {
            return UserDetailsBal.SocialloginInfo(UserDetails);
        }
        public void Dispose()
    {
        if (UserDetailsBal != null)
        {
            this.UserDetailsBal.Dispose();
        }
    }
}
}


