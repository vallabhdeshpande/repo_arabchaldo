﻿using Service.contract;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using System.Data;

namespace Service.Implement.Implement
{
    public class ManageCategoryService : IManageCategoryService, IDisposable
    {

        ManageCategoryBAL ManageCategoryBal = new ManageCategoryBAL();

        public ManageCategoryService()
        {

        }

        public ManageCategoryService(ManageCategoryBAL ManageCategoryBal)
        {
            this.ManageCategoryBal = ManageCategoryBal;
        }

        public bool CheckCategoryExits(string CategoryName)
        {
            return ManageCategoryBal.CheckCategoryExits(CategoryName);
        }

        public bool DeleteCategory(int CategoryID)
        {
            return ManageCategoryBal.DeleteCategory(CategoryID);
        }
        public bool UpdateCategory(int CategoryID, CategoryDTO CategoryDetails)
        {
            return ManageCategoryBal.UpdateCategory(CategoryID, CategoryDetails);
        }
        public List<CategoryDTO> GetAllCategory()
        {
            return ManageCategoryBal.GetAllCategory();
        }

        public CategoryDTO GetCategorybyId(int CategoryID)
        {
            return ManageCategoryBal.GetCategorybyId(CategoryID);
        }

        public bool InsertCategory(CategoryDTO CategoryDetails)
        {
            return ManageCategoryBal.InsertCategory(CategoryDetails);
        }
        public void Dispose()
        {
            if (ManageCategoryBal != null)
            {
                this.ManageCategoryBal.Dispose();
            }
        }

        public DataTable FillCategoryList()
        {
            return ManageCategoryBal.FillCategoryList();
        }
    }
}
