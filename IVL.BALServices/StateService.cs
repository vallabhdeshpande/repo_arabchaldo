﻿using Service.contract;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using System.Data;

namespace Service.Implement.Implement
{
    public class StateService : IStateService, IDisposable
    {

        StateBAL StateBal = new StateBAL();

        public StateService()
        {

        }

        public StateService(StateBAL StateBal)
        {
            this.StateBal = StateBal;
        }

        public bool CheckStateExits(string StateName, long StateId)
        {
            return StateBal.CheckStateExits(StateName, StateId);
        }

        public bool DeleteState(int StateID)
        {
            return StateBal.DeleteState(StateID);
        }
        public bool UpdateState(int StateID, StateDTO State)
        {
            return StateBal.UpdateState(StateID, State);
        }
        public List<StateDTO> GetAllState()
        {
            return StateBal.GetAllState();
        }

        public StateDTO GetStateById(int StateID)
        {
            return StateBal.GetStateById(StateID);
        }

        public bool InsertState(StateDTO State)
        {
            return StateBal.InsertState(State);
        }
        public void Dispose()
        {
            if (StateBal != null)
            {
                this.StateBal.Dispose();
            }
        }



        public DataTable FillStateList()
        {
            return StateBal.FillStateList();
        }
    }
}
