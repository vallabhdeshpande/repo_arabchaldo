﻿using Service.contract;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using System.Data;

namespace Service.Implement.Implement
{
    public class SmallAdDetailsService : ISmallAdDetailsService, IDisposable
    {

        SmallAdDetailsBAL SmallAdDetailsBal = new SmallAdDetailsBAL();

        public SmallAdDetailsService()
        {

        }

        public SmallAdDetailsService(SmallAdDetailsBAL SmallAdDetailsBal)
        {
            this.SmallAdDetailsBal = SmallAdDetailsBal;
        }

        public bool CheckSmallAdDetailsExits(string Title, long SmallAdId)
        {
            return SmallAdDetailsBal.CheckSmallAdDetailsExits(Title, SmallAdId);
        }

        public bool SmallAdStatus(long SmallAdId, string remarks, string Action, long CreatedBy)
        {
            return SmallAdDetailsBal.SmallAdStatus(SmallAdId, remarks, Action, CreatedBy);
        }
        public bool UpdateSmallAdDetails(long SmallAdId, SmallAdDetailsDTO SmallAdDetails)
        {
            return SmallAdDetailsBal.UpdateSmallAdDetails(SmallAdId, SmallAdDetails);
        }
        public List<SmallAdDetailsDTO> GetAllSmallAdDetails(long loggedInUserId, string loggedInRole)
        {
            return SmallAdDetailsBal.GetAllSmallAdDetails(loggedInUserId, loggedInRole);
        }

        public DataTable GetAllSmallAdsDetails(long loggedInUserId, string loggedInRole)
        {
            return SmallAdDetailsBal.GetAllSmallAdsDetails(loggedInUserId, loggedInRole);
        }
       
        public SmallAdDetailsDTO GetSmallAdDetailsById(long SmallAdId)
        {
            return SmallAdDetailsBal.GetSmallAdDetailsById(SmallAdId);
        }

        public bool InsertSmallAdDetails(SmallAdDetailsDTO SmallAdDetails)
        {
            return SmallAdDetailsBal.InsertSmallAdDetails(SmallAdDetails);

        }
        public void Dispose()
        {
            if (SmallAdDetailsBal != null)
            {
                this.SmallAdDetailsBal.Dispose();
            }
        }
    }
}
