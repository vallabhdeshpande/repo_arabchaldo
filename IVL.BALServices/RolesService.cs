﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.contract;

namespace Service.Implement
{
    public class RolesService : IRolesService, IDisposable
    {

        RolesBAL RolesBAL = new RolesBAL();

        public RolesService()
        {

        }

        public RolesService(RolesBAL RolesBAL)
        {
            this.RolesBAL = RolesBAL;
        }

        public List<RolesDTO> GetAllUserRole()
        {
            return RolesBAL.GetAllUserRole();
        }
        public void Dispose()
        {
            if (RolesBAL != null)
            {
                this.RolesBAL.Dispose();
            }
        }
    }
}
