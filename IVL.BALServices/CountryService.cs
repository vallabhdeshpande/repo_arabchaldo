﻿using Service.contract;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;

namespace Service.Implement.Implement
{
    public class CountryService : ICountryService, IDisposable
    {

        CountryBAL CountryBal = new CountryBAL();

        public CountryService()
        {

        }

        public CountryService(CountryBAL CountryBal)
        {
            this.CountryBal = CountryBal;
        }

        public bool CheckCountryExits(string CountryName)
        {
            return CountryBal.CheckCountryExits(CountryName);
        }

        public bool DeleteCountry(int CountryID)
        {
            return CountryBal.DeleteCountry(CountryID);
        }
        public bool UpdateCountry(int CountryID, CountryDTO Country)
        {
            return CountryBal.UpdateCountry(CountryID, Country);
        }
        public List<CountryDTO> GetAllCountry()
        {
            return CountryBal.GetAllCountry();
        }

        public CountryDTO GetCountryById(int CountryID)
        {
            return CountryBal.GetCountryById(CountryID);
        }

        public bool InsertCountry(CountryDTO Country)
        {
            return CountryBal.InsertCountry(Country);
        }
        public void Dispose()
        {
            if (CountryBal != null)
            {
                this.CountryBal.Dispose();
            }
        }
    }
}
