﻿using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using IVL.ArabChaldo.DAL.Models;
using Service.contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Implement
{
    public class ConfigurationService : IConfigurationService, IDisposable
    {
        ConfigurationBAL ConfigDetailsBal = new ConfigurationBAL();

        public ConfigurationService()
        {

        }

        public ConfigurationService(ConfigurationBAL ConfigDetailsBal)
        {
            this.ConfigDetailsBal = ConfigDetailsBal;
        }


        public bool DeleteConfig(int ID)
        {
            return ConfigDetailsBal.DeleteConfig(ID);
        }
        public bool UpdateConfig(int ID, ConfigurationDTO configdetails)
        {
            return ConfigDetailsBal.UpdateConfig(ID, configdetails);
        }
        public List<ConfigurationDTO> GetAllConfigDetails()
        {
            return ConfigDetailsBal.GetAllConfigDetails();
        }


        public void Dispose()
        {
            if (ConfigDetailsBal != null)
            {
                this.ConfigDetailsBal.Dispose();
            }
        }
    }
}

