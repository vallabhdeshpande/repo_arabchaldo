﻿using Service.contract;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using System.Data;

namespace Service.Implement
{
    public class LoginService:ILoginService, IDisposable
    {
        LoginDetailsBAL Loginbal = new LoginDetailsBAL();

        public LoginService()
        {

        }

        public LoginService(LoginDetailsBAL Loginbal)
        {
            this.Loginbal = Loginbal;
        }

        public LoginDetailsDTO GetLogin(string UserName, string Password)
        {
            return Loginbal.GetLogin(UserName, Password);
        }

        public string GetLoginInfo(string UserName, string Password)
        {
            return Loginbal.GetLoginInfo(UserName, Password);
        }

        public LoginDetailsDTO GetLoginByUserID(long userId)
        {
            return Loginbal.GetLoginByUserID(userId);
        }
        public bool ResetPassword(int userId, LoginDetailsDTO loginDetails)
        {
            return Loginbal.ResetPassword(userId, loginDetails);
        }

        public bool ActivateUserAccount(string info)
        {
            return Loginbal.ActivateUserAccount(info);
        }

        public DataTable GetUserInfoByEmail(string email)
        {
            return Loginbal.GetUserInfoByEmail(email);
        }
        public void Dispose()
        {
            if (Loginbal != null)
            {
                this.Loginbal.Dispose();
            }
        }
    }
}
