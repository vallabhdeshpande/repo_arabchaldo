﻿
using System;
using System.Collections.Generic;
using System.Text;
using Service.contract;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using System.Data;

namespace Service.Implement
{
    public class BlogDetailsService : IBlogDetailsService, IDisposable
    {
     BlogDetailsBAL _blogDetailsBAL = new BlogDetailsBAL();

        public BlogDetailsService()
        {

        }

        public BlogDetailsService(BlogDetailsBAL blogDetailsBAL)
        {
            this._blogDetailsBAL = blogDetailsBAL;
        }

        public DataTable GetAllBlog(long loggedInUserId, string loggedInRole)
        {
            return this._blogDetailsBAL.GetAllBlog(loggedInUserId, loggedInRole);
        }

        public bool Insert(BlogDetailsDTO blogDetails)
        {
            return this._blogDetailsBAL.AddBlog(blogDetails);
        }

        public bool Update(long id, BlogDetailsDTO blogDetails)
        {
            return this._blogDetailsBAL.UpdateBlog(id, blogDetails);
        }

        public bool Delete(long blogId)
        {
            return this._blogDetailsBAL.DeleteBlog(blogId);
        }

        public bool BlogStatus(long BlogId, string remarks, string Action, long CreatedBy)
        {
            return this._blogDetailsBAL.BlogStatus(BlogId, remarks, Action, CreatedBy);
        }

        public BlogDetailsDTO GetBlogById(long id)
        {
            return this._blogDetailsBAL.GetBlogById(id);
        }

        public void Dispose()
        {
            if (_blogDetailsBAL != null)
            {
                this._blogDetailsBAL.Dispose();
            }
        }
    }
}
