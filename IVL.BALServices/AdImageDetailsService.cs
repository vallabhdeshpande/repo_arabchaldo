﻿using Service.contract;
using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;

namespace Service.Implement.Implement
{
    public class AdImageDetailsService : IAdImageDetailsService, IDisposable
    {

        AdImageDetailsBAL AdImageDetailsBal = new AdImageDetailsBAL();

        public AdImageDetailsService()
        {

        }

        public AdImageDetailsService(AdImageDetailsBAL AdImageDetailsBal)
        {
            this.AdImageDetailsBal = AdImageDetailsBal;
        }

        public bool CheckAdImageDetailsExits(string AdImageDetailsName)
        {
            return AdImageDetailsBal.CheckAdImageDetailsExits(AdImageDetailsName);
        }

        public bool DeleteAdImageDetails(int AdImageDetailsID)
        {
            return AdImageDetailsBal.DeleteAdImageDetails(AdImageDetailsID);
        }
        public bool UpdateAdImageDetails(int AdImageDetailsID, AdImageDetailsDTO AdImageDetails)
        {
            return AdImageDetailsBal.UpdateAdImageDetails(AdImageDetailsID, AdImageDetails);
        }
        public List<AdImageDetailsDTO> GetAllAdImageDetails()
        {
            return AdImageDetailsBal.GetAllAdImageDetails();
        }

        public AdImageDetailsDTO GetAdImageDetailsById(int AdImageDetailsID)
        {
            return AdImageDetailsBal.GetAdImageDetailsById(AdImageDetailsID);
        }

        public bool InsertAdImageDetails(AdImageDetailsDTO AdImageDetails)
        {
            return AdImageDetailsBal.InsertAdImageDetails(AdImageDetails);
        }
        public void Dispose()
        {
            if (AdImageDetailsBal != null)
            {
                this.AdImageDetailsBal.Dispose();
            }
        }
    }
}
