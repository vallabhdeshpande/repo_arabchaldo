﻿using System;
using System.Collections.Generic;
using System.Text;
using IVL.ArabChaldo.CommonDTO.Models;
using Service.contract;
using IVL.ArabChaldo.BAL;
using System.Data;

namespace Service.Implement
{
    public class PaymentPlanService : IPaymentPlanService, IDisposable
    {
        private readonly PaymentPlanBAL _paymentPlanBAL = new PaymentPlanBAL();

        public PaymentPlanService()
        {

        }

        public PaymentPlanService(PaymentPlanBAL paymentPlanBAL)
        {
            this._paymentPlanBAL = paymentPlanBAL;
        }

        public bool Delete(long planId)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public DataTable GetAllPlan()
        {
            return _paymentPlanBAL.getAllPlan();            
        }

        public List<PaymentPlanDTO> GetPlanListForUser(int userId, string location)
        {
            return this._paymentPlanBAL.GetPlanListForUser(userId, location);
        }
        public PaymentPlanDTO GetPlanId(int planId)
        {
            PaymentPlanDTO paymentPlanDTO = new PaymentPlanDTO();
            paymentPlanDTO = this._paymentPlanBAL.getPlanIdDetails(planId);
            return paymentPlanDTO;
        }

        public string InsertPlan(PaymentPlanDTO paymentPlanDetails)
        {
            //PaymentPlanBAL paymentPlanBAL = new PaymentPlanBAL();
            string isSuccess = this._paymentPlanBAL.InsertPlan(paymentPlanDetails);
            return isSuccess;
        }

        public void UpdatePlanDetails(long id, PaymentPlanDTO paymentPlan)
        {
            this._paymentPlanBAL.UpdatePlanDetails(id, paymentPlan);
        }

        public string StripePaymentConfirmation(string tokenResoponse)
        {
            string paymentResponse = this._paymentPlanBAL.StripePaymentConfirmation(tokenResoponse);
            return paymentResponse;
        }

        public bool OfflinePaymentConfirmation(string OfflinePayment)
        {
            bool OfflineResponse = this._paymentPlanBAL.OfflinePaymentConfirmation(OfflinePayment);
            return true;
        }
    }
}
