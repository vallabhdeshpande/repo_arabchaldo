﻿using System;
using System.Collections.Generic;
using System.Text;
using Service.contract;
using IVL.ArabChaldo.BAL;
using IVL.ArabChaldo.CommonDTO.Models;
using System.Data;

namespace Service.Implement
{
    public class BannerDetailsService : IBannerDetailsService, IDisposable
    {
        private readonly BannerDetailsBAL _bannerDetailsBAL = new BannerDetailsBAL();

        public BannerDetailsService()
        {

        }

        public BannerDetailsService(BannerDetailsBAL bannerDetailsBAL)
        {
            this._bannerDetailsBAL = bannerDetailsBAL;
        }
                
        public DataTable GetAllBanner(int userId)
        {
            return this._bannerDetailsBAL.GetAllBanner(userId);
        }
                
        public bool Insert(BannerDetailsDTO bannerDetails)
        {
            return this._bannerDetailsBAL.AddBanner(bannerDetails);
        }

        public bool Update(long id, BannerDetailsDTO bannerDetails)
        {
            return this._bannerDetailsBAL.UpdateBanner(id, bannerDetails);
        }        

        public bool Delete(long bannerId)
        {
            return this._bannerDetailsBAL.DeleteBanner(bannerId);
        }

        public bool BannerStatus(long bannerId, string remarks, string Action, long CreatedBy)
        {
            return this._bannerDetailsBAL.BannerStatus(bannerId, remarks, Action, CreatedBy);
        }

        public BannerDetailsDTO GetBannerById(long id)
        {
            return this._bannerDetailsBAL.GetBannerById(id);
        }

        public void Dispose()
        {
            if (_bannerDetailsBAL != null)
            {
                this._bannerDetailsBAL.Dispose();
            }
        }

        public DataTable FillBannerList()
        {
            return _bannerDetailsBAL.FillBannerList();
        }
    }
}
